<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Customer Search</title>

		<style type="text/css">
<!--

body {
	background-color: transparent;
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	-webkit-tap-highlight-color:rgba(0,0,0,0);
}

.style4 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #8896AD;
}
.style6 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 24px;
	font-weight: bold;
	color: #8896AD;
}
.style8 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 15px;
	font-weight: bold;
	color: #07142a;
}
.creditnumber {
	font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #3d4f6d;
}
.nametext {
	font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
}
.nametext_grey {
	font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #777777;
}
.subtitles {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #5A75A0;
}
-->
		</style>
	</head>

<?php 
	require_once(dirname(__FILE__) . "/../comconnect.php"); 
	$mode = reqvar("mode","recent_signup");
	$cm = reqvar("cm");
	$tab_name = reqvar("tab_name");
	$where_cond = "";
	$and_cond = "";
	$message = "";
	
	if(reqvar("loc_id")) $loc_id = reqvar("loc_id");
	else if(sessvar_isset("loc_id")) $loc_id = sessvar("loc_id");
	set_sessvar("loc_id", $loc_id);
	
	$set_eventid = reqvar("eventid","");
	if($set_eventid!="")  
	{
		$eventid = $set_eventid;
		set_sessvar("eventid",$eventid);
	}
	
	$group_queue = "";
	if(reqvar("group_queue")) $group_queue = reqvar("group_queue");
	else if(sessvar_isset("group_queue")) $group_queue = sessvar("group_queue");
	set_sessvar("group_queue", $group_queue);
	if ($group_queue == "all") $group_queue = "";
	$group_queue_filter = "";
	if ($group_queue != "") $group_queue_filter = " AND `event_id` = '$group_queue'";
	
	$use_csr = reqvar("use_csr");					
	$use_pool = reqvar("pool","recent");
	$use_credits = sessvar("use_credits");
	
	if($cm)
	{
		$cmd = "_DO:cmd=".reqvar("cmd")."&racer_name=[fullname]&racer_id=[id]&MorAaSU=[MorAaSU]&MorA=[MorA]";
	}
	else
	{
		if ($tab_name == "Customers") {
			$cmd = "_DO:cmd=load_url&c_name=customer_info&f_name=lavukart/customer_info.php?racer_id=[id]";
		} else {
			if( ($tab_name=="Schedule") || ($tab_name == "New Schedule") )
			{
				$eventid = sessvar("eventid");
				if($eventid)
				{					
					if ($use_credits != "No") {
						$where_cond = " where `credits` > 0";
						$and_cond = " and `credits` > 0";
					}
				}
				else
				{
					$message = "No Event Selected";
				}
			}
			$cmd = "_DO:cmd=close_overlay&c_name=customers&f_name=lavukart/customers.php?mode=assign&racer_name=[fullname]&racer_id=[id]";
		}
	}
	
	$copy_from = reqvar("copy_from","");
	if($copy_from!="")
	{
		$mode = "copy_racers";
		$use_pool = "copy_racers";
		$pool_title = "Copy Racers";
	}
	
	$rlist = array();
	if($mode=="recent_signup")
	{
		if($use_pool=="qualifier")
		{			
			$event_query = lavu_query("SELECT * from `lk_events` LEFT JOIN `lk_event_types` ON `lk_events`.`type`=`lk_event_types`.`id` where `lk_events`.`id`='[1]'",$eventid);
			if(mysqli_num_rows($event_query))
			{
				$event_read = mysqli_fetch_assoc($event_query);
				$event_ts = $event_read['ts'];
				$debug = "";
				
				if($event_read['title']=="Qualifier") $look_for_q = "Practice"; else $look_for_q = "Qualifier";
				$pool_title = $look_for_q;
				
				$customer_ids_used = array();
				$rlist = array();
				$qual_query = lavu_query("select `lk_customers`.`id` as customerid from `lk_customers` LEFT JOIN `lk_race_results` ON `lk_customers`.`id`=`lk_race_results`.`customerid` LEFT JOIN `lk_events` ON `lk_race_results`.`eventid`=`lk_events`.`id` LEFT JOIN `lk_event_types` ON `lk_events`.`type`=`lk_event_types`.`id` WHERE `lk_event_types`.`title` LIKE '".$look_for_q."' and `lk_events`.`ms_end`!='' and `lk_events`.`ts`<'[1]' and `lk_events`.`ts`>'[2]'",$event_ts,($event_ts - (60 * 60 * 2)));
				while($qual_read = mysqli_fetch_assoc($qual_query))
				{
					$cus_query = lavu_query("select * from `lk_customers` where `id`='[1]'",$qual_read['customerid']);
					if(mysqli_num_rows($cus_query))
					{
						$cus_read = mysqli_fetch_assoc($cus_query);
						if(!isset($customer_ids_used[$cus_read['id']]))
						{
							$cus_read['pre_placement'] = 888;
							$cus_read['pre_placed'] = false;
							$cus_read['customerid'] = $cus_read['id'];
							$rlist[] = $cus_read;
							
							$customer_ids_used[$cus_read['id']] = true;
						}
					}
				}
				
				for($rr=0; $rr<count($rlist); $rr++)
				{
					$results_query = lavu_query("select * from `lk_race_results` LEFT JOIN `lk_customers` ON `lk_race_results`.`customerid`=`lk_customers`.`id` LEFT JOIN `lk_events` ON `lk_race_results`.`eventid`=`lk_events`.`id` where `lk_race_results`.`customerid`='[1]' and `lk_events`.`ms_end`!='' and `lk_events`.`ts`<'[2]' and `lk_events`.`ts`>'[3]' order by `lk_events`.`ts` desc limit 1",$rlist[$rr]['customerid'],$event_ts,($event_ts * 1 - (60 * 60 * 12)));
					if(mysqli_num_rows($results_query))
					{
						$results_read = mysqli_fetch_assoc($results_query);
						$rlist[$rr]['pre_placement'] = $results_read['bestlap'];//place'];
						$rlist[$rr]['pre_placed'] = true;
						$debug .= "\n " . $rlist[$rr]['customerid'] . " " . $results_read['f_name'] . " " . $results_read['l_name'] . " index:$rr" . " = value:" . $results_read['place'];
					}
				}
									
				$r2_list = $rlist;
				$rlist = array();
				while(count($rlist) < count($r2_list))
				{
					$lowest = 99999;
					$lowest_index = "";
					for($n=0; $n<count($r2_list); $n++)
					{
						$r2num = $r2_list[$n]['pre_placement'];
						if($r2num!=98989 && ($r2num < $lowest))
						{
							$lowest = $r2num;
							$lowest_index = $n;
						}
					}
					$rlist[] = $r2_list[$lowest_index];
					$r2_list[$lowest_index]['pre_placement'] = 98989;
					$debug .= "\n add to list: index:" . $lowest_index . " value:$lowest)";
				}
					//if(sessvar("loc_id")==9)
						//mail("corey@meyerwind.com","placement",$debug,"From: info@poslavu.com");
			}
		}
		else if($use_pool=="csr")
		{
			$use_csr_parts = explode("|",$use_csr);
			$use_csr_id = $use_csr_parts[0];
			if(count($use_csr_parts) > 1)
				$use_csr_name = $use_csr_parts[1];
			else $use_csr_name = "CSR " . $use_csr_id;
			
			$mintime = 0;
			//echo "query: " . "select * from `lk_customers` where `date_created`>='$mintime'" . "<br>";
			$cust_query = lavu_query("select * from `lk_customers` where `locationid` = '".$loc_id."'$group_queue_filter AND `last_activity`>='".date("Y-m-d H:i:s",current_ts(-60))."' AND `last_activity`<='".date("Y-m-d H:i:s",current_ts(+2400))."'".$and_cond);// where `date_created`>='$mintime'");
		}
		
		if(count($rlist) < 1 && ($use_pool=="recent" || $use_pool=="qualifier"))
		{
			$use_pool = "recent";
			$mintime = 0;
			//echo "query: " . "select * from `lk_customers` where `date_created`>='$mintime'" . "<br>";
			if($group_queue_filter!="")
				$cust_query = lavu_query("select * from `lk_customers` where `locationid` = '".$loc_id."'$group_queue_filter ".$and_cond." order by `last_activity` desc");// where `date_created`>='$mintime'");
			else
				$cust_query = lavu_query("select * from `lk_customers` where `locationid` = '".$loc_id."'$group_queue_filter AND `last_activity`>='".date("Y-m-d H:i:s",current_ts(-60))."' AND `last_activity`<='".date("Y-m-d H:i:s",current_ts(+2400))."'".$and_cond." order by `last_activity` desc");// where `date_created`>='$mintime'");
		}
	}
	else if($mode=="copy_racers")
	{
		$copy_from_date = reqvar("copy_from_date");
		$copy_from_parts = explode("-",$copy_from);
		$copy_from_trackid = $copy_from_parts[0];
		$copy_from_time = $copy_from_parts[1];
		
		$rlist = array();
		$event_query = lavu_query("select * from `lk_events` where `date`='[1]' and `time`='[2]' and `trackid`='[3]'",$copy_from_date,$copy_from_time,$copy_from_trackid);
		if(mysqli_num_rows($event_query))
		{
			$event_read = mysqli_fetch_assoc($event_query);
			$copy_from_eventid = $event_read['id'];
			$sched_query = lavu_query("select * from `lk_event_schedules` where `eventid`='[1]'",$copy_from_eventid);
			while($sched_read = mysqli_fetch_assoc($sched_query))
			{
				$sched_customerid = $sched_read['customerid'];
				$cust_query = lavu_query("select * from `lk_customers` where `id`='[1]'".$and_cond,$sched_customerid);
				if(mysqli_num_rows($cust_query))
				{
					$cust_read = mysqli_fetch_assoc($cust_query);
					$rlist[] = $cust_read;
				}
			}
			$cust_query = false;
		}
		if(count($rlist) < 1 && $message=="") $message .= "No Racers Available to Copy";
	}
	else if($mode=="search")
	{
		$search_term = reqvar("search_term");
		//$cust_query = lavu_query("select * from `lk_customers` where (`f_name` LIKE TRIM('%".$search_term."%') or `l_name` LIKE TRIM('%".$search_term."%') or `email` LIKE TRIM('%".$search_term."%') or `racer_name` LIKE TRIM('%".$search_term."%') or concat(TRIM(`f_name`),' ',TRIM(`l_name`)) LIKE TRIM('%".$search_term."%') )".$and_cond." order by `f_name` asc, `l_name` asc limit 36");
		//$join_action_log_code = "LEFT JOIN `lk_action_log` ON `lk_customers`.`id` = `lk_action_log`.`customer_id`";
		$cust_query = lavu_query("select * from `lk_customers` where `locationid`='".$loc_id."'$group_queue_filter and (`racer_name` LIKE TRIM('%".$search_term."%') or `f_name` LIKE TRIM('%".$search_term."%') or `l_name` LIKE TRIM('%".$search_term."%') or concat(TRIM(`f_name`),' ',TRIM(`l_name`)) LIKE TRIM('%".$search_term."%') )".$and_cond." order by TRIM(`f_name`) LIKE TRIM('".$search_term."') desc, `f_name` asc, `l_name` asc limit 100");
		//$message = "searching for $search_term";
	}
	
	$csr_id_list = array();
	
	if($cust_query && count($rlist) < 1)
	{
		while($cust_read = mysqli_fetch_assoc($cust_query))
		{
			$include_cust = true;
			
			$csr_match = false;
			$action_query = lavu_query("select `user_id` from `lk_action_log` where `customer_id`='[1]' and `loc_id`='[2]' order by id desc limit 1",$cust_read['id'],$loc_id);
			if(mysqli_num_rows($action_query))
			{
				$action_read = mysqli_fetch_assoc($action_query);
				$csr_id = $action_read['user_id'];
				if($csr_id==$use_csr_id)
					$csr_match = true;
				if(is_numeric($csr_id))
					$csr_id_list[$csr_id] = 1;
			}
			if($use_pool=="csr" && !$csr_match)
			{
				$include_cust = false;
			}
			
			if($include_cust)
				$rlist[] = $cust_read;
		}
	}
	
	$csr_info_list = array();
	foreach($csr_id_list as $csr_key => $csr_val)
	{
		$csr_query = lavu_query("select * from `users` where `id`='[1]'",$csr_key);
		if(mysqli_num_rows($csr_query))
		{
			$csr_read = mysqli_fetch_assoc($csr_query);
			$csr_info_list[] = $csr_read;
		}
	}
		
	$showing_racer_id = 0;
	if(reqvar("showing_racer_id")) $showing_racer_id = reqvar("showing_racer_id");
	else if(sessvar_isset("showing_racer_id")) $showing_racer_id = sessvar("showing_racer_id");
	
	$jscode = "";
?>
	<body>
	
    <?php

			$group_event_picker = "";
			if ($group_queue != "") {
				$get_event_info = lavu_query("SELECT `id`, `title`, `event_date`, `event_time` FROM `lk_group_events` WHERE `id` = '[1]'", $group_queue);
				$event_info = mysqli_fetch_assoc($get_event_info);
				echo "<table><td width='40'>&nbsp;</td><td width='330' align='center'><span class='style8'><font color='#777777'>EVENT:</font> ".$event_info['title']." (".timize($event_info['event_time']).")</span></td></table>";
			} else {
				$today = date("Y-m-d");
				if (isset($location_info) && ($location_info['timezone'] != "")) {
					$today = localize_date(date("Y-m-d H:i:s"), $location_info['timezone']);
				}
				$check_events = lavu_query("SELECT `id`, `title`, `event_date`, `event_time` FROM `lk_group_events` WHERE `event_date` = '$today' AND `cancelled` != '1' AND `_deleted` != '1' AND `loc_id` = '[1]' ORDER BY `event_date` ASC, (`event_time` * 1) ASC", $loc_id);
				if (@mysqli_num_rows($check_events) > 0) {
					$group_event_picker = "<select name='group_event_list' onchange='window.location = \"_DO:cmd=load_url&c_name=customer_search&f_name=lavukart/customer_search.php?group_queue=\" + this.value'>";
					$group_event_picker .= "<option value=''>--- List by Group Event ---</option>";				
					while ($event_info = mysqli_fetch_assoc($check_events)) {
						$ddd = "";
						if (strlen($event_info['title']) > 25) $ddd = "...";
						$group_event_picker .= "<option value='".$event_info['id']."'>".substr($event_info['title'], 0, 25).$ddd." (".timize($event_info['event_time']).")</option>";
					}
					$group_event_picker .= "</select> &nbsp;";
				}
			}
			
			$view_all_code = "<td align='center'><input type='button' onclick='window.location = \"_DO:cmd=load_url&c_name=customer_search&f_name=lavukart/customer_search.php?pool=recent&group_queue=all\"' value='View Active Customers'></td>";

			if($use_pool=="qualifier")
			{
				echo "<table><td width='40'>&nbsp;</td><td width='230' align='center'><span class='style8'>$pool_title Racers</span></td>$view_all_code</table>";
			}
			else if($use_pool=="csr")
			{
				if ($group_event_picker != "") {
					echo "<table><tr><td align='center' width='400px'>$group_event_picker</td></tr></table>";
				}
				echo "<table><td width='40'>&nbsp;</td><td width='230' align='center'><span class='style8'><font color='#777777'>CSR:</font> $use_csr_name</span></td>$view_all_code</table>";
			}
			else if($use_pool=="copy_racers")
			{
				echo "<table><td width='40'>&nbsp;</td><td width='230' align='center'><span class='style8'>Copy Racers</span></td>$view_all_code</table>";
			}
			else
			{
				$use_view_all_code = "";
				if ($group_queue != "") {
					$use_view_all_code = $view_all_code;
				}
				echo "<table><td align='right' width='400px'>";				
				echo $group_event_picker;
				echo "<select name='csr_list' onchange='window.location = \"_DO:cmd=load_url&c_name=customer_search&f_name=lavukart/customer_search.php?pool=csr&use_csr=\" + this.value'>";
				echo "<option value=''>--- List by CSR ---</option>";
				for($i=0; $i<count($csr_info_list); $i++)
				{
					$csr_id = $csr_info_list[$i]['id'];
					$csr_name = $csr_info_list[$i]['f_name'] . " " . $csr_info_list[$i]['l_name'];
					echo "<option value='$csr_id|$csr_name'>$csr_name e</option>";
				}
				echo "</select> ";
				//echo "<input type='button' onclick='window.location = \"_DO:cmd=load_url&c_name=customer_search&f_name=lavukart/customer_search.php?pool=recent\"' value='List by CSR'>";
				echo "</td>$use_view_all_code</table>";
			}
			
		?>
		<table width="426" border="0" cellspacing=0 cellpadding=0>
        	<?php
			if($message!="")
			{
				echo "<td align='center'><font style='font-size:24px; color:#999999; font-family:Verdana,Arial'>$message</font></td>";
			}
			else
			{
			
				if ($eventid && !isset($event_read)) {
					$get_event_info = lavu_query("SELECT `date`, `groupid`, `heat` FROM `lk_events` WHERE `id` = '[1]'", $eventid);
					if (mysqli_num_rows($get_event_info) > 0) $event_read = mysqli_fetch_assoc($get_event_info);
				}
			
				$min_rows = 12;
				$rowcount = 0;
				$row_selected = 0;
				for($rr=0; $rr<count($rlist); $rr++)
				{
					$cust_read = $rlist[$rr];
				//while($cust_read = mysqli_fetch_assoc($cust_query))
				//{
					$rowcount++;
					$fullname = trim($cust_read['f_name'] . " " . $cust_read['l_name']);
					$racer_id = $cust_read['id'];
					$racer_name = $cust_read['racer_name'];
					$birth_date = $cust_read['birth_date'];
					$birth_date1 = $birth_date;
					$membership_expiration = $cust_read['membership_expiration'];
					$date_created = $cust_read['date_created'];
					$bdates = explode("-",$birth_date);
					if(count($bdates) > 2)
					{
						$byear = ($bdates[0] % 100);
						if($byear < 10) $byear = "0" . ($byear * 1);
						$birth_date = $bdates[1] . "/" . $bdates[2] . "/" . $byear;
					}
					else $birth_date = "";
					$row_cmd = str_replace("[fullname]",str_replace("'","%27",$fullname),str_replace("[id]",$racer_id,$cmd));
					$row_cmd = str_replace("[MorAaSU]", $cust_read['minor_or_adult_at_signup'], $row_cmd);
					$row_cmd = str_replace("[MorA]", $cust_read['minor_or_adult'], $row_cmd);
					if (($cm) && ($cust_read['confirmed_by_attendant'] == "0")) {
						//$row_cmd = "_DO:cmd=confirm_signature&racer_id=".$racer_id."&racer_name=".str_replace("'","%27",$fullname)."&MorA=".$cust_read['minor_or_adult']."&MorAaSU=".$cust_read['minor_or_adult_at_signup'];
						$row_cmd = str_replace("cmd=".reqvar("cmd"), "cmd=confirm_signature", $row_cmd);
					}
					$credits = $cust_read['credits'];
					$use_bg = "images/tab_bg2.png";
					if (($racer_id == $showing_racer_id) && ($tab_name == "Customers")) {
						$row_selected = $rowcount;
						$use_bg = "images/tab_bg2_lit.png";
					}
					$change_select = "";
					if ($tab_name == "Customers") {
						$change_select = " if(row_selected) document.getElementById(\"customer_row_\" + row_selected).style.background = \"URL(images/tab_bg2.png)\"; row_selected = \"".$rowcount."\"; document.getElementById(\"customer_row_\" + row_selected).style.background = \"URL(images/tab_bg2_lit.png)\"";
					}
					$nametext_class = "nametext";
					$td_onclick = "location = \"".$row_cmd."\"; ".$change_select;
					if ( ($tab_name == "Schedule") || ($tab_name == "New Schedule") ) {
						$check_schedule = lavu_query("SELECT * FROM `lk_event_schedules` WHERE `eventid` = '".$eventid."' AND `customerid` = '".$racer_id."'");
						if (@mysqli_num_rows($check_schedule) > 0) {
							$nametext_class = "nametext_grey";
							$td_onclick = "";
						}
					}
					
					$scheduled_count = "";
					if ($eventid) {
						$check_schedule = lavu_query("SELECT `lk_events`.`groupid` AS `groupid`, `lk_events`.`heat` AS `heat` FROM `lk_event_schedules` LEFT JOIN `lk_events` ON `lk_events`.`id` = `lk_event_schedules`.`eventid` WHERE `lk_event_schedules`.`customerid` = '[1]' AND `lk_events`.`date` = '[2]'", $cust_read['id'], $event_read['date']);
						$result_count = mysqli_num_rows($check_schedule);
						if ($result_count > 0) {
							$scheduled_count = "<span style='font-size:10px;'>(<font color='#006633'>$result_count</font>";
							$this_round_count = 0;
							while ($sched_info = mysqli_fetch_assoc($check_schedule)) {
								if ($sched_info['groupid']==$event_read['groupid'] && $sched_info['heat']==$event_read['heat'])
									$this_round_count++;
							}
							if ($this_round_count > 0)
								$scheduled_count .= "<font color='#000000'>·</font><font color='#990000'>$this_round_count</font>";
							$scheduled_count .= ") </span>";
						}
					}
					
					?>
					 <tr>
                        <td height="46"><table height="46" border="0" cellpadding="0" cellspacing="0">
                          <tr>
                          	<td width="10"></td>
                            <td width="34" align="center" valign="middle">
                            <?php
															if ( ($tab_name == "Schedule") || ($tab_name == "New Schedule") || $cm) {
																$display = "inline";
																if ($nametext_class == "nametext_grey") {
																	$display = "none"; 
																}
                            		echo "<input type='checkbox' style='width:30px; height:30px; display:".$display."' id='checkbox_$racer_id'>";
                            	} else {
																echo "&nbsp;";
															}
															$jscode .= "if(document.getElementById('checkbox_$racer_id').checked) { ";
															$jscode .= "  if(setval!='') setval += ','; ";
															$jscode .= "  setval += '$racer_id'; ";
															if ($cm) {
																$jscode .= "  if(racerInfo!='') racerInfo += '|*|'; ";
																$jscode .= "  racerInfo += '$racer_id|o|".str_replace("'","%27",$fullname)."|o|".$cust_read['confirmed_by_attendant']."|o|".$cust_read['minor_or_adult_at_signup']."|o|".$cust_read['minor_or_adult']."'; ";
															}
															$jscode .= "} ";
														?>
                            </td>
                            <td id="customer_row_<?php echo $rowcount; ?>" width="313" style="background:URL(<?php echo $use_bg; ?>);" ><table width="313" border="0" cellspacing="0" cellpadding="8">
                              <tr>
                                <td class="<?php echo $nametext_class; ?>" onclick='<?php echo $td_onclick; ?>'>
									<?php 
										if($birth_date1!="" && $birth_date1 > date("Y-m-d",mktime(0,0,0,date("m"),date("d"),date("Y") - 18)))
											$cust_is_minor = true;
										else
											$cust_is_minor = false;
										
										if ($scheduled_count != "") echo $scheduled_count;
										echo $fullname;
										if($racer_name!="") echo " <font style='font-size:10px; color:#000066'>($racer_name)</font>";
										if($birth_date!="") 
										{
											if($cust_is_minor)
												echo " <font style='font-size:10px; color:#bb0000'>$birth_date</font>";
											else
												echo " <font style='font-size:10px; color:#006600'>$birth_date</font>";
										}
									?>
								</td>
                              </tr>
                            </table></td>
							<?php
								if($date_created >= date("Y-m-d 00:00:00"))
									$cust_new = true;
								else
									$cust_new = false;
									$drawM = "null";
								if($membership_expiration!="" && $membership_expiration >= date("Y-m-d") && $membership_expration <= "9999-99-99")
									{
											$mem = true; 
											$bgf = "images/btn_credit2.png";
											$drawM="blue";
									}
								else 	
									{
										$mem = false;
										$bgf = "images/btn_credit.png";
										
										$drawM="no";
										
										if($membership_expiration!="" && $membership_expiration < date("Y-m-d")){
												$drawM = "red";
										}//if
										
									}
							?>
                            <td width="52" align="left" valign="top" background="<?php echo $bgf?>"><table width="50" height="46" border="0" cellspacing="0" cellpadding="6">
                              <tr>
                                <td align="center" valign="middle" class="creditnumber">
									<?php
										echo $credits;
										if($cust_new) echo "<font style='font-size:8px; color:#008800'>n</font>";
										if($mem){
												 echo "<font style='font-size:8px; color:#000088'>M</font>";
										}//if
										
										if(!$mem){
												 
												 if ($drawM=="no"){
															echo "<font style='font-size:8px; color:#000088'></font>";//this doesn't do anything
												 }//if
												 if ($drawM=="red"){
												 			echo "<font style='font-size:8px; color:#bb0000'>M</font>";
												 }//if
											}//if
										
										//if($cust_is_minor) echo "<font style='font-size:8px; color:#880000'>m</font>";
									?>
								</td>
                              </tr>
                            </table></td>
                          </tr>
                        </table></td>
                      </tr>
					<?php
				}
				for($i=$rowcount; $i<$min_rows; $i++)
				{
					?>
					 <tr>
                        <td height="46"><table height="46" border="0" cellpadding="0" cellspacing="0">
                          <tr>
                          	<td width="10">&nbsp;</td>
                            <td width="34" align="center" valign="middle">&nbsp;</td>
                            <td width="313" background="images/tab_bg2.png" ><table width="313" border="0" cellspacing="0" cellpadding="8">
                              <tr>
                                <td class="nametext">&nbsp;</td>
                              </tr>
                            </table></td>
                            <td width="52" align="left" valign="top" background="images/btn_credit.png"><table width="50" height="46" border="0" cellspacing="0" cellpadding="6">
                              <tr>
                                <td align="center" valign="middle" class="creditnumber">&nbsp;</td>
                              </tr>
                            </table></td>
                          </tr>
                        </table></td>
                      </tr>
					<?php
				}
			}
			?>
      <!--<tr>
        <td height="46" background="images/tab.png"><a onclick='location = "_DO:cmd=cancel";'>Custom Modifier Cancel Button Example</a></td>
      </tr>-->
    </table>
	<script language='javascript'>
		function submit_checked()
		{
			setval = "";
			racerInfo = "";
			<?php echo $jscode; ?>
			<?php if ($cm) { ?>
				window.location = "_DO:cmd=assign_multiple_racers&racer_info=" + racerInfo;
			<?php } else { ?>
				window.location = "_DO:cmd=close_overlay&c_name=customers&f_name=lavukart/customers.php?mode=assign_multiple&racer_ids=" + setval;
			<?php } ?>
		}
		
		row_selected = <?php echo $row_selected; ?>;
	</script>
	
	</body>
</html>
