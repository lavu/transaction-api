<?php
			require_once(dirname(__FILE__) . "/../comconnect.php");
			
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Untitled Document</title>
</head>

<body>


<?php

	$locationid = sessvar("loc_id");
	$serviceID = $_POST['serviceID']; //the unique id of the service
	
	
	
	/* the list menu says kart number, but the values are the kart id*/
	$kartID = $_POST['KartID'];
	
	$kartNumberResults = lavu_query("SELECT number FROM lk_karts WHERE id=\"[1]\"", $kartID);
	$kartNumberRows = mysqli_fetch_assoc($kartNumberResults);
	$kartNumber = $kartNumberRows['number'];
	
	
	/*the lk_service table stores value in service_type as serviceID-NameOfService, so we must format this now */
	$serviceTypeID = $_POST['serviceTypeID'];
	$getServiceTypeNameResults = lavu_query("SELECT * FROM lk_service_types WHERE locationid=\"[1]\" AND id=\"[2]\"", $locationid, $serviceTypeID);
	$getServiceTypeNameRows = mysqli_fetch_assoc($getServiceTypeNameResults);
	$serviceName = $getServiceTypeNameRows['name'];
	$serviceTypeDescription = $getServiceTypeNameRows['description'];
	$serviceTypeManHours = $getServiceTypeNameRows['man_hours'];
	
	
	$serviceTypePartsStr = $getServiceTypeNameRows['parts'];
	$serviceTypeArray = explode(",", $serviceTypePartsStr);
	$storeFormattedPartsList;
	$partCost = 0;
	for ($i=0; $i<count($serviceTypeArray);$i++){
				
				$curArray = $serviceTypeArray[$i];
				$curPieces = explode("@", $curArray);
				$curID = $curPieces[0];
				$curQuantity = $curPieces[1];
				
				
				//get part name
				$resultsGetPartName = lavu_query("SELECT * FROM lk_parts WHERE id=\"[1]\" AND locationid=\"[2]\"", $curID, $locationid);
				$getPartNameRows = mysqli_fetch_assoc($resultsGetPartName);
				$curPartName = $getPartNameRows['name'];
				$curPartCost = $getPartNameRows['cost'];
				
				$partCost = $partCost + ($curPartCost * $curQuantity);
				
				$strIntoArray = $curPartName . "@" . $curID . "@" . $curQuantity; /* we just changed from partIDxQuantity,partIDxQuantity... to partNamexpartIDxQuantity,partNamexpartIDxQuantity...etc */
				$storeFormattedPartsList[$i] = $strIntoArray;
				
	}//for
	
	$serviceTypePartsStr = implode(",", $storeFormattedPartsList);
	
	$serviceTypeSubmit2DB = $serviceTypeID . "-" . $serviceName;//properly formatted
	
	
	//$description; //description is not updated in the lk_service table since it cannot be modified by user
	$notes = $_POST['notes'];
	//$parts; // parts is not updated in the lk_service table since it cannot be modified by user
	$dateDueMonth = $_POST['dateDueMonth'];
	/* If single digit we need a "0" in front to make sure there are two digits*/
	if (strlen($dateDueMonth) == 1){
				$dateDueMonth = "0" . $dateDueMonth;
	}//if
	
	
	$dateDueDay = $_POST['dateDueDay'];
	
	/* If single digit we need a "0" in front to make sure there are two digits*/
	if (strlen($dateDueDay) == 1){
				$dateDueDay = "0" . $dateDueDay;
	}//if
	
	
	$dateDueYear = $_POST['dateDueYear'];
	$dateDue = $dateDueYear . "-" . $dateDueMonth . "-" . $dateDueDay;
	//now get the time (H:i:s) from database
	$dateDueResults = lavu_query("SELECT date_due FROM lk_service WHERE locationid=\"[1]\" AND id=\"[2]\"", $locationid, $serviceTypeID);
	$dateDueRows = mysqli_fetch_assoc($dateDueResults);
	$dateDueString = $dateDueRows['date_due'];
	$dateDueStringArray = explode(" ", $dateDueString);
	$dateDueTime = $dateDueStringArray[1]; //$dateDueStringArray[0] is the date we are about to overwrite


	
	$dateDueSubmitToDB = $dateDue . " " . $dateDueTime; //this goes to the lk_service table
	
	//$dateAssigned; //dateAssigned is not updated in the lk_service table since it cannot be modified by user
	$priority = $_POST['Priority'];
	//$manHours; //manHours is not updated in the lk_service table since it cannot be modified by user
	
	$submittedBy = $_POST['submittedBy'];
	
	$dateAssigned = date("Y-m-d H:i:s"); /* this is what date (year-month-day hour:min:second) this was submitted, so for our purposes, now */
	
	
	

	

	/* Lavu query only can handle five inputs at a time ? */
	$results1 = lavu_query("INSERT INTO lk_service (kart_id, service_type, notes, date_due, locationid) VALUES(\"[1]\", \"[2]\", \"[3]\", \"[4]\", \"[5]\")", $kartID, $serviceTypeSubmit2DB, $notes, $dateDueSubmitToDB, $locationid);
	
	//get the service id from the entry we just inserted into lk_service
	$getServiceIDResults = lavu_query("SELECT id FROM lk_service WHERE date_due=\"[1]\" AND locationid=\"[2]\" AND notes=\"[3]\" AND kart_id=\"[4]\"", $dateDueSubmitToDB, $locationid, $notes, $kartID);
	$getServiceIDRows = mysqli_fetch_assoc($getServiceIDResults);
	$serviceID = $getServiceIDRows['id'];
	
	$results2 = lavu_query("UPDATE lk_service SET priority='[1]', submitted_by='[2]', datetime='[3]'  WHERE id=\"[4]\" AND locationid=\"[5]\"", $priority, $submittedBy, $dateAssigned, $serviceID, $locationid);
	
	$results3 = lavu_query("UPDATE lk_service SET description='[1]', man_hours='[2]', parts='[3]'  WHERE id=\"[4]\" AND locationid=\"[5]\"", $serviceTypeDescription, $serviceTypeManHours, $serviceTypePartsStr, $serviceID, $locationid);
	
	$results4 = lavu_query("UPDATE lk_service SET part_cost='[1]', kart_number=\"[2]\" WHERE id=\"[3]\" AND locationid=\"[4]\"", $partCost, $kartNumber, $serviceID, $locationid);
	
	
	/* If nothing was updated $numberOfUpdatedRows will be 0 */
	if ($numberOfUpdatedRows == 0){
		/*	echo "<script type=\"text/javascript\">window.location = '_DO:cmd=alert&title=Update Failed&message=No changes were made.';</script>";*/
	}//if

?>
