<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Untitled Document</title>

		<style type="text/css">
<!--

body {
	background-color: transparent;
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	-webkit-tap-highlight-color:rgba(0,0,0,0);
}

.style4 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #8896AD;
}
.style6 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 24px;
	font-weight: bold;
	color: #8896AD;
}
.style8 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 15px;
	font-weight: bold;
	color: #07142a;
}
.creditnumber {
	font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #3d4f6d;
}
.nametext {
	font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
}
.subtitles {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 16px;
	font-weight: bold;
	color: #5A75A0;
}
.subtitles_dark {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 16px;
	font-weight: bold;
	color: #2f3e55;
}
.info_text1 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 16px;
	color: #233756;
}
.info_text2 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 15px;
	color: #5A75A0;
}
.info_text3 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 15px;
	color: #233756;
}
-->
		</style>
	</head>

<?php 
	require_once(dirname(__FILE__) . "/../comconnect.php"); 
	
	function reformat_date($date,$type) {
		$datetime_array = explode(" ", $date);
		$date_array = explode("-", $datetime_array[0]);
		$time_array = explode(":", $datetime_array[1]);
	
		if ($type == 1) {
			return date("g:i:s A \o\\n F jS, Y", mktime((int)$time_array[0], (int)$time_array[1], (int)$time_array[2], (int)$date_array[1], (int)$date_array[2], (int)$date_array[0]));
		} else if ($type == 2) {
			return date("Y-m-d g:i A", mktime((int)$time_array[0], (int)$time_array[1], (int)$time_array[2], (int)$date_array[1], (int)$date_array[2], (int)$date_array[0]));
		}
	}
	
	function get_field_info($table, $get_field, $known_field, $known_value) {	
		$get_info = lavu_query("SELECT `[1]` FROM `[2]` WHERE `[3]` = '[4]'", $get_field, $table, $known_field, $known_value);
		if (mysqli_num_rows($get_info) > 0) {
			$extract_info = mysqli_fetch_array($get_info);
			return $extract_info[$get_field];
		} else {
			return "";
		}
	}

	$creation_date = "";
	$confirmed_by = "Unconfirmed";
	$get_customer_info = lavu_query("SELECT * FROM `lk_customers` WHERE `id` = '[1]'", $_POST['racer_id']);
	if (mysqli_num_rows($get_customer_info) > 0) {
		$extract_c = mysqli_fetch_array($get_customer_info);
		$creation_date = reformat_date($extract_c['date_created'],1);
		if ($extract_c['confirmed_by_attendant'] == "") {
			$confirmed_by = "Speedsheet Transfer";
		} else if ($extract_c['confirmed_by_attendant'] != "0") {
			$confirmed_by = get_field_info("users", "f_name", "id", $extract_c['confirmed_by_attendant'])." ".get_field_info("users", "l_name", "id", $extract_c['confirmed_by_attendant']);
		}
	}
	
	$pos_history = "";
	$total_credits_purchased = 0;
	$check_pos_history = lavu_query("SELECT `locations`.`title` as pos_location, `lk_action_log`.`action` as action, `lk_action_log`.`time` as time, `lk_action_log`.`order_id` as order_id, `lk_action_log`.`credits` as credits, `lk_action_log`.`user_id` as user_id FROM `lk_action_log` LEFT JOIN `locations` ON `locations`.`id` = `lk_action_log`.`loc_id` WHERE `customer_id` = '[1]' ORDER BY `lk_action_log`.`time` DESC", $_POST['racer_id']);
	if (mysqli_num_rows($check_pos_history) > 0) {
		while ($extract_p = mysqli_fetch_array($check_pos_history)) {
			$display_order_id = "";
			if ($extract_p['order_id'] != "0") {
				$display_order_id = " - Order #".$extract_p['order_id'];
			}
			$attendant = "";
			if (($extract_p['user_id'] != "") && ($extract_p['user_id'] != "0")) {
				$attendant = "<td align='right' width='100'><span class='info_text2'>&nbsp;&nbsp;Attendant:</span></td><td align='left'><span class='info_text3'>".get_field_info("users", "f_name", "id", $extract_p['user_id'])." ".get_field_info("users", "l_name", "id", $extract_p['user_id'])."</span></td>";
			}
			
			$pos_history .= "<tr>
				<td width='532' height='60' align='center' style='background:URL(images/tab_bg_race.png)'>
					<table width='512' cellspacing='0' cellpadding='2'>
						<tr><td align='center' colspan='5'><span class='info_text2'>".reformat_date($extract_p['time'], 2)." - ".$extract_p['pos_location'].$display_order_id."</span></td></tr>
						<tr>
							<td align='center'>
								<table cellspacing='0' cellpadding='2'>
									<tr><td align='left'><span class='info_text3'>".$extract_p['action']."</span></td>".$attendant."</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>";
			$total_credits_purchased = ($total_credits_purchased + $extract_p['credits']);
		}
	} else {
		$pos_history = "<tr><td width='532' height='60' align='center' style='background:URL(images/tab_bg_race.png)'><span class=\"info_text1\">This person has no POS transactions on record since the SpeedSheet transfer.</span></td></tr>";
	}

	$race_history = "";
	$total_credits_used = 0;
	$check_race_history = lavu_query("SELECT `lk_events`.`ts` as event_ts, `lk_event_schedules`.`eventid` as `eventid`, `lk_event_schedules`.`customerid` as `customerid`, `locations`.`title` as event_location, `lk_event_types`.`title` as event_title, `lk_karts`.`number` as kart_number FROM `lk_event_schedules` LEFT JOIN `lk_events` ON `lk_events`.`id` = `lk_event_schedules`.`eventid` LEFT JOIN `lk_karts` ON `lk_karts`.`id` = `lk_event_schedules`.`kartid` LEFT JOIN `lk_event_types` ON `lk_event_types`.`id` = `lk_events`.`type` LEFT JOIN `locations` ON `locations`.`id` = `lk_events`.`locationid` WHERE `customerid` = '[1]' ORDER BY `lk_events`.`ts` DESC", $_POST['racer_id']);
	if (mysqli_num_rows($check_race_history) > 0) {
		while ($extract_r = mysqli_fetch_array($check_race_history)) {
			$race_result_query = lavu_query("select * from `lk_race_results` where `eventid`='[1]' and `customerid`='[2]'",$extract_r['eventid'],$extract_r['customerid']);
			if(mysqli_num_rows($race_result_query))
			{
				$race_result_read = mysqli_fetch_assoc($race_result_query);
				$set_ranking = $race_result_read['ranking'];
				$set_bestlap = $race_result_read['bestlap'];
			}
			else
			{
				$set_ranking = "";
				$set_bestlap = "";
			}
			
			$race_history .= "<tr>
				<td width='532' height='60' align='center' style='background:URL(images/tab_bg_race.png)'>
					<table width='512' cellspacing='0' cellpadding='2'>
						<tr><td align='center' colspan='6'><span class='info_text2'>".date("Y-m-d g:i A", ($extract_r['event_ts'] * 1))." - ".$extract_r['event_location']." - ".$extract_r['event_title']."</span></td></tr>
						<tr><td align='right' width='82'><span class='info_text2'><nobr>Best Lap:</nobr></span></td><td align='left' width='90'><span class='info_text3'>".$set_bestlap."</span></td><td align='right' width='80'><span class='info_text2'>&nbsp;&nbsp;Ranking:</span></td><td align='left' width='90'><span class='info_text3'>".$set_ranking."</span></td><td align='right' width='80'><span class='info_text2'>&nbsp;&nbsp;Kart:</span></td><td align='left' width='90'><span class='info_text3'>".$extract_r['kart_number']."</span></td></tr>
					</table>
				</td>
			</tr>";
			$total_credits_used++;
		}
	} else {
		$race_history = "<tr><td width='532' height='60' align='center' style='background:URL(images/tab_bg_race.png)'><span class=\"info_text1\">This person has not participated in any races since the SpeedSheet transfer.</span></td></tr>";
	}
?>
	<body>
		<table width="542" cellspacing="0" cellpadding="0">
			<tr>
				<td align="center">
					<table width="532" border="0" cellspacing="0" cellpadding="0">
						<tr><td>&nbsp;</td></tr>
						<?php
							if ($extract_c['membership'] != "") {
								$today = date("Y-m-d", time());
								if ($today > $extract_c['membership_expiration']) {
									$expires = "Expired ".$extract_c['membership_expiration'];
								} else if ($today == $extract_c['membership_expiration']) {
									$expires = "Today";
								} else {
									$expires = $extract_c['membership_expiration'];
								}
								echo "<tr>
									<td align='center'>
										<table cellspacing='3' cellpadding='3'>
											<tr>
												<td align='left' valign='middle'><span class='info_text3'><b>".$extract_c['membership']." - </b></span></td>
												<td align='right' valign='middle'><span class='info_text2'><b>Expires:</b></span></td>
												<td align='left' valign='middle'><span class='info_text3'><b>".$expires."</b></span></td>
											</tr>
										</table>
									</td>
								</tr>";
							}
						?>
						<tr>
							<td align="center">
								<table cellspacing="3" cellpadding="3">
									<tr>
										<td align="right" valign="middle"><span class="subtitles">Account Created:</span></td>
										<td align="left" valign="middle"><span class="info_text1"><?php echo $creation_date; ?></span></td>
									</tr>
								</table>
								<table cellspacing="3" cellpadding="3">
									<tr>
										<td align="right" valign="middle"><span class="subtitles">Confirmed by:</span></td>
										<td align="left" valign="middle"><span class="info_text1"><?php echo $confirmed_by; ?></span></td>
									</tr>
								</table>
							</td>
						</tr>
						<tr><td>&nbsp;</td></td>
						<tr>
							<td align="center">
								<table cellspacing="3" cellpadding="3">
									<tr>
										<td align='center' colspan='4'>
											<table cellspacing='3' cellpadding='3'>
												<tr>
													<td align="right" valign="middle"><span class="info_text2"><b>Visits:</b></span></td>
													<td align="left" valign="middle"><span class="info_text3"><?php echo $extract_c['total_visits']; ?></span></td>
													<td align="right" valign="middle"><span class="info_text2"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Races Raced:</b></span></td>
													<td align="left" valign="middle"><span class="info_text3"><?php echo $extract_c['total_races']; ?></span></td>
												</tr>
											</table>
										</td>
									</tr>
									<tr>
										<td align="right" valign="middle"><span class="info_text2"><b>Purchased Credits:</b></span></td>
										<td align="left" valign="middle"><span class="info_text3"><?php echo $total_credits_purchased; ?></span></td>
										<td align="right" valign="middle"><span class="info_text2"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Used Credits:</b></span></td>
										<td align="left" valign="middle"><span class="info_text3"><?php echo $total_credits_used; ?></span></td>
									</tr>
									<tr>
										<td align='center' colspan='4'>
											<table cellspacing='3' cellpadding='3'>
												<tr>
													<td align="right" valign="middle"><span class="info_text2"><b>Remaining Credits:</b></span></td>
													<td align="left" valign="middle"><span class="info_text3"><?php echo $extract_c['credits']; ?></span></td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr><td>&nbsp;</td></tr>
						<tr id="POS_row">
							<td align="center">
								<table width="532" cellspacing="0" cellpadding="0">
									<tr>
										<td align="center">
											<table width="532" cellspacing="0" cellpadding="0">
												<tr>
													<td align="left"><span class="subtitles_dark">&nbsp;&nbsp;POS Transactions</span></td>
													<td align="right" onClick="document.getElementById('POS_row').style.display = 'none'; document.getElementById('races_row').style.display = 'inline';"><span class="subtitles">Races&nbsp;&nbsp;</span></td>
												</tr>
											</table>
										</td>
									</tr>
									<tr><td><hr color="#5A75A0"></td></tr>
									<tr>
										<td align="center">
											<table cellspacing="0" cellpadding="0">
												<?php echo $pos_history; ?>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr id="races_row" style="display:none;">
							<td align="center">
								<table width="532" cellspacing="0" cellpadding="0">
									<tr>
										<td align="center">
											<table width="532" cellspacing="0" cellpadding="0">
												<tr>
													<td align="left" onClick="document.getElementById('races_row').style.display = 'none'; document.getElementById('POS_row').style.display = 'inline';"><span class="subtitles">&nbsp;&nbsp;POS Transactions</span></td>
													<td align="right"><span class="subtitles_dark">Races&nbsp;&nbsp;</span></td>
												</tr>
											</table>
										</td>
									</tr>
									<tr><td><hr color="#5A75A0"></td></tr>
									<tr>
										<td align="center">
											<table cellspacing="0" cellpadding="0">
												<?php echo $race_history; ?>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr><td>&nbsp;</td></tr>
					</table>
				</td>
			</tr>
	  </table>
	</body>
</html>
