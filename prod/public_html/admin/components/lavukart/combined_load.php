<?
	require_once(dirname(__FILE__) . "/../comconnect.php");

	$combined_html = "";
	
	$post_vars = "";
	$post_keys = array_keys($_POST);
	foreach ($post_keys as $key) {
		if ($post_vars != "") {
			$post_vars .= "&";
		}
		if ($key == "cc") {
			$post_vars .= "cc=".$company_code_full;
		} else {
			$post_vars .= $key."=".$_POST[$key];
		}
	}

	$get_component_layout = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`component_layouts` WHERE `id` = '".$_POST['layout_id']."'");
	if (@mysqli_num_rows($get_component_layout) > 0) {
		$layout_info = mysqli_fetch_assoc($get_component_layout);
		if (trim($layout_info['c_ids']) != "") {
			$combined_html = $layout_info['c_ids'];
			$component_ids = explode(",", trim($layout_info['c_ids']));
			$load_good = true;
			foreach ($component_ids as $c_id) {
				$get_components = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`components` WHERE `id` = '".$c_id."'");
				if (@mysqli_num_rows($get_components) > 0) {
					$component_info = mysqli_fetch_assoc($get_components);
					$combined_html .= "<******>";

					$ch = curl_init("http://admin.poslavu.com/components/".$component_info['filename']);
					curl_setopt($ch, CURLOPT_HEADER, 0);
					curl_setopt($ch, CURLOPT_POST, 1);
					curl_setopt($ch, CURLOPT_POSTFIELDS, $post_vars."&comp_name=".$component_info['name']);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					$combined_html .= curl_exec($ch);
					curl_close($ch);
	
				} else {
					$load_good = false;
					break;
				}
			}
			if ($load_good == true) {
				echo $combined_html;
			} else {
				echo "Error - Missing Component";
			}
			
		} else {
			echo "Error - Missing Component IDs";
		}
		
	} else {
		echo "Error - Unknown Component Layout";
	}
?>