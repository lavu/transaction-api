<?php
	require_once("../comconnect.php");
	
	$server_id = 0;
	if(reqvar("server_id")) $server_id = reqvar("server_id");
	else if(sessvar_isset("server_id")) $server_id = sessvar("server_id");
	set_sessvar("server_id",$server_id);

	$loc_id = 0;
	if(reqvar("loc_id")) $loc_id = reqvar("loc_id");
	else if(sessvar_isset("loc_id")) $loc_id = sessvar("loc_id");
	set_sessvar("loc_id", $loc_id);

	$track_id = 0;
	if(reqvar("track_id")) $track_id = reqvar("track_id");
	else if(sessvar_isset("track_id")) $track_id = sessvar("track_id");
	set_sessvar("track_id", $track_id);

	if ($track_id == 0) {
		$get_a_track_id = lavu_query("SELECT * FROM `lk_tracks` WHERE `locationid` = '[1]' AND `_deleted` != '1' ORDER BY `_order`, `id` ASC LIMIT 1", $loc_id);
		if (mysqli_num_rows($get_a_track_id) > 0) {
			$track_info = mysqli_fetch_array($get_a_track_id);
			$track_id = $track_info['id'];
			set_sessvar("track_id", $track_id);			
		}
	} else {
		$get_track_info = lavu_query("SELECT * FROM `lk_tracks` WHERE `id` = '[1]'", $track_id);
		if (mysqli_num_rows($get_track_info) > 0) {
			$track_info = mysqli_fetch_array($get_track_info);
		}
	}

	$year = reqvar("year",date("Y"));
	$month = reqvar("month",date("m"));
	$day = reqvar("day",date("d"));
	
	$pyear = $year;
	$pmonth = $month - 1;
	if($pmonth < 1) {$pmonth = 12;$pyear--;}
	$nyear = $year;
	$nmonth = $month + 1;
	if($nmonth > 12) {$nmonth = 1;$nyear++;}
	
	function create_calendar($year,$month,$day)
	{
		$js_cal_click = "cal_click";
		$calwidth = 51;
		$calheight = 48;
		$allow_past = true;

		$set_year = $year;
		$set_month = $month;
		$set_day = $day;
		
		$str .= "<script language='javascript'>";
		$str .= "function $js_cal_click(year, month, day) { ";
		$str .= "window.location = \"inner_calendar.php?year=\" + year + \"&month=\" + month + \"&day=\" + day; ";
		$str .= "} ";
		$str .= "</script>";
		
		$str .= "<div id='calendar'>";
		$str .= "<table cellspacing='0'>";
		$r=1;$day=1;
		while($r<=6)
		{
			$str .= "<tr>";
			if($r==1)
			{
				$c=date("w",mktime(0, 0, 0, $month, 1, $year))+1;
				$x=1;
				while($x<$c)
				{
				  $str .= "<td width=$calwidth height=$calheight>&nbsp;</td>";
				  $x=$x+1;
				}
			}
			else $c=1;
			
			while($c<=7)
			{
				if($day<10) $day="0".$day;
				$datecheck = $year."-".$month."-".$day;
				
				if((($r+$c)%2)==0) 
					$sbg="#9fd4f0";
				else 
					$sbg="#5bb4e2";
					
				if(checkdate($month,$day,$year))
				{						
					$dayoftheweek = date("w",mktime(0,0,0,$month,$day,$year)) * 1 + 1;
	
					$msg = date("l",mktime(0,0,0,$month,$day,$year));
					$clr = "000000";
					
					if($allow_past || date("Y-m-d") <= date("Y-m-d",mktime(0,0,0,$month,$day,$year)))
						$allow = true;
					else
					{
						$allow = false;
						$clr = "666699";
					}
					
					$datenum = ($month * 100) + $day;
					$cal_js .= "cal_colors[$datenum] = '$sbg'; ";
					if($month==$set_month && $year==$set_year && $day==$set_day)
					{
						$extra_style_code = "background: URL(images/btn_cal_day.png); background-position:center center; background-repeat:no-repeat; ";
						//$extra_style_code = "border:solid 2px black; ";
						$sbg = "#00ff00";
						$cal_js .="selected_datenum = '$datenum'; ";
					}
					else $extra_style_code = "background: URL(images/btn_cal_day_lit.png); background-position:center center; background-repeat:no-repeat; ";
					
					$str .= "<td id='".$fieldname."_date_$datecheck' class='cal' title='$msg' alt='$msg'";
					$str .= " style='cursor:pointer; ";
					$str .= $extra_style_code;
					$str .= "'";
					if($allow)
						$str .= " onclick='$js_cal_click($year, $month, $day)'";
					$str .= " width=$calwidth height=$calheight align='center' valign='center'>";
					$str .= "<font color='#$clr'>";
					$str .= ($day*1);
					$str .= "</font>";
					$str .= "</td>";						
				}
				else $str .= "<td width=$calwidth height=$calheight>&nbsp;</td>";
	
				$c++;$day++;
			}
			$str .= "</tr>";
			$r++;
		}
		$str .= "</table>";
		$str .= "</div>";
		return $str;
	}	
?>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Untitled Document</title>
		<style type="text/css">
<!--
.caltext {
	font-family: "Trebuchet MS", Arial, Helvetica, sans-serifVerdana, Geneva, sans-serif;
	font-size: 16px;
	font-weight: bold;
	color: #154473;
}
.cal_title {
	font-family: "Trebuchet MS", Arial, Helvetica, sans-serifVerdana, Geneva, sans-serif;
	font-size: 21px;
	font-weight: bold;
	color: #FFFFFF;
}
.available_time {
	font-family: "Trebuchet MS", Arial, Helvetica, sans-serifVerdana, Geneva, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #000099;
	width:68;
	height:38;
	background:URL(images/btn_cal_time.png);
	background-repeat:no-repeat;
}
.unavailable_time {
	font-family: "Trebuchet MS", Arial, Helvetica, sans-serifVerdana, Geneva, sans-serif;
	font-size: 14px;
	color: #999999;
	width:68;
	height:38;
	background:URL(images/btn_cal_time_gray.png);
	background-repeat:no-repeat;
}
-->
		</style>
	</head>

	<body style='margin:0px'>
		<table width="423" height="374" border="0" cellspacing="0" cellpadding="0">
			<tr><td height="7" colspan="5"></td></tr>
		  <tr>
				<td width="3" height="40">&nbsp;</td>
				<td width="120" height="40" align="center" valign="middle" onClick="window.location = '<?php echo "inner_calendar.php?year=$pyear&month=$pmonth&day=1"; ?>'" style="background:URL(images/btn_cal_prev.png); background-repeat:no-repeat;">&nbsp;</td>
				<td width="163" height="40" align="center"><span class="cal_title"><?php echo date("M Y",mktime(0,0,0,$month,$day,$year)); ?></td>
				<td width="120" height="40" align="center" valign="middle" onClick="window.location = '<?php echo "inner_calendar.php?year=$nyear&month=$nmonth&day=1"; ?>'" style="background:URL(images/btn_cal_next.png); background-repeat:no-repeat;">&nbsp;</td>
				<td width="17" height="40">&nbsp;</td>
			</tr>
			<tr><td colspan="5" align="center" valign="top" style="padding:5px 0px 0px 0px;"><?php echo create_calendar($year,$month,$day); ?></td></tr>
			<tr>
				<td colspan="5" align="center" valign="top" style="padding:11px 0px 0px 0px">
					<table cellspacing='1' cellpadding='0'>
						<?php
							$used_times = array();
							$check_events = lavu_query("SELECT * FROM `lk_group_events` WHERE `event_date` = '$year-$month-$day' AND `cancelled` != '1' AND `_deleted` != '1' AND `loc_id` = '$loc_id'");
							if (mysqli_num_rows($check_events) > 0) {
								while ($extract = mysqli_fetch_array($check_events)) {
									$event_time = explode(":", $extract['event_time']);
									$event_hour = $event_time[0];
									$event_minutes = $event_time[1];
									$event_ts = mktime($event_hour, $event_minutes, 0, $month, $day, $year);
									$half_hours = ($extract['duration'] / 30);
									$used_times[] = date("G:i", $event_ts);
									for ($hh = 0; $hh < ($half_hours - 1); $hh++) {
										$event_ts = ($event_ts + 1800);
										if (!in_array(date("G:i", $event_ts), $used_times)) {
											$used_times[] = date("G:i", $event_ts);
										}
									}
								}
							}
						
							$col = 1;
							for ($ts = mktime(($track_info['time_start'] / 100), ($track_info['time_start'] % 100), 0, $month, $day, $year); $ts < mktime(($track_info['time_end'] / 100), ($track_info['time_end'] % 100), 0, $month, $day, $year); ($ts += 1800)) {
								$time = date("G:i", $ts);
								if ($col == 1) {
									echo "<tr>";
								}
								if (in_array($time, $used_times)) {
									$time_cell_class = "unavailable_time";
									$time_cell_onclick = "";
								} else {
									$time_cell_class = "available_time";
									$time_cell_onclick = " onclick='parent.location = \"group_event_input.php?mode=get_info&year=$year&month=$month&day=$day&time=$time\"'";
								}
								echo "<td class='$time_cell_class' align='center' valign='middle'".$time_cell_onclick.">".date("g:ia", $ts)."</td>";
								if ($col == 6) {
									echo "</tr>";
									$col = 0;
								}
								$col++;
							}
							
						?>
					</table>
				</td>
			</tr>
		</table>
	</body>
</html>
