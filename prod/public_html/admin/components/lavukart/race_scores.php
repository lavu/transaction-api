<?php
  	require_once(dirname(__FILE__) . "/../comconnect.php");
			
	$eventid = reqvar("eventid");
	$trackid = reqvar("trackid");
	$mode = reqvar("mode");
	
	$tsnow = date("YmdHis");
	
	$event_request = $eventid;
	
	if(!$trackid && $eventid)
	{
		$ev_query = lavu_query("select * from `lk_events` where `id`='[1]'",$eventid);
		if(mysqli_num_rows($ev_query))
		{
			$ev_read = mysqli_fetch_assoc($ev_query);
			$trackid = $ev_read['trackid'];
			$locationid = $ev_read['locationid'];
		}
	}
	
	// monitoring
	$qvals = array();
	$qvals['ipaddress'] = $_SERVER['REMOTE_ADDR'];
	$qvals['date'] = date("Y-m-d");
	$qvals['time'] = date("H:i:s");
	$qvals['ts'] = time();
	$qvals['ccname'] = (isset($company_code))?$company_code:"";
	$qvals['locationid'] = (isset($locationid))?$locationid:"";
	$qvals['mode'] = $mode;
	$qvals['eventid'] = $event_request;
	$qvals['trackid'] = $trackid;
	$qvals['details'] = "";
	$detail_update = "";
	
	$rcpt = reqvar("rcpt");
	if($rcpt)
	{
		$qvals['details'] = $_GET['rcpt'];
		$detail_update = ", `details`='[details]'";
	}
	
	/*$monitor_query = lavu_query("select * from `lk_monitor` where `ipaddress`='[ipaddress]' and `mode`='[mode]' and `ccname`='[ccname]' and `locationid`='[locationid]' and `trackid`='[trackid]' order by `ts` desc",$qvals);
	if(mysqli_num_rows($monitor_query))
	{
		$monitor_read = mysqli_fetch_assoc($monitor_query);
		$qvals['monitorid'] = $monitor_read['id'];
		if(isset($monitor_read['reload']) && $monitor_read['reload']!="")
		{
			if($monitor_read['reload'] > time() - 300)
				echo "&reload_swf=".$monitor_read['reload']."&";
		}
		lavu_query("update `lk_monitor` set `date`='[date]', `time`='[time]', `ts`='[ts]', `eventid`='[eventid]'".$detail_update." where `id`='[monitorid]'",$qvals);
	}
	else
	{
		lavu_query("insert into `lk_monitor` (`ipaddress`,`date`,`time`,`ts`,`ccname`,`lname`,`locationid`,`mode`,`eventid`,`trackid`,`details`) values ('[ipaddress]','[date]','[time]','[ts]','[ccname]','[lname]','[locationid]','[mode]','[eventid]','[trackid]','[details]')",$qvals);
	}*/
	// end monitoring
	
	if($eventid=="current_pit")
	{
		$fin_query = lavu_query("select * from `lk_events` where `ms_end`!='' and `trackid`='[1]' order by `ts` desc",$trackid);
		if(mysqli_num_rows($fin_query))
		{
			$fin_read = mysqli_fetch_assoc($fin_query);
			$finid = $fin_read['id'];
			
			$next_query = lavu_query("select * from `lk_events` where `trackid`='[1]' and `ts`>'[2]' and `ts`>='".current_ts(-360)."' and `ms_start`='' order by `ts` asc limit 1",$trackid,$fin_read['ts']);
			if(mysqli_num_rows($next_query))
			{
				$next_read = mysqli_fetch_assoc($next_query);
				$eventid = $next_read['id'];
			}
			else $eventid = "current";
			//mail("corey@poslavu.com","eventid: $eventid","eventid: $eventid trackid: $trackid ts > ".$find_read['ts']." ts > " . current_ts(-60),"From: debug@poslavu.com");
		}
		else $eventid = "current";
	}
	
	if($eventid=="current" || $eventid=="active")
	{
		$active_query = lavu_query("select * from `lk_events` where `ms_start`!='' and `ms_end`='' and `trackid`='[1]'",$trackid);
		if(mysqli_num_rows($active_query))
		{
			$active_read = mysqli_fetch_assoc($active_query);
			$eventid = $active_read['id'];
		}
	}
	
	if($eventid=="last")
	{
		$active_query = lavu_query("select * from `lk_events` where `ms_start`!='' and `ms_end`!='' and `trackid`='[1]' order by `ms_end` desc",$trackid);
		if(mysqli_num_rows($active_query))
		{
			$active_read = mysqli_fetch_assoc($active_query);
			$eventid = $active_read['id'];
		}
		//echo "eventid: $eventid <br>";
	}
	
	if($mode=="kart_assignment")
	{
		$kart_count = 0;
		$sched_query = lavu_query("select `f_name`,`l_name`,`racer_name`,`lk_event_schedules`.`id` as `id`,`lk_karts`.`number` as `number` from `lk_event_schedules` LEFT JOIN `lk_customers` ON `lk_event_schedules`.`customerid`=`lk_customers`.`id`  LEFT JOIN `lk_karts` ON `lk_event_schedules`.`kartid`=`lk_karts`.`id` where `lk_event_schedules`.`eventid`='[1]'",$eventid);
		while($sched_read = mysqli_fetch_assoc($sched_query))
		{
			$racer_name = $sched_read['racer_name'];
			$abbrev_name = trim(ucfirst($sched_read['f_name']) . " " . ucfirst(substr(trim($sched_read['l_name']),0,1)));
			if(trim($racer_name)=="")
				$racer_name = $abbrev_name;
			$racer_number = $sched_read['number'];
			$kart_count++;
			echo "<br>&kart_name".$kart_count."=".$abbrev_name."&";
			echo "<br>&kart_number".$kart_count."=".$racer_number."&";
		}
		echo "<br>&kart_count=".$kart_count."&";
	}
	else if($mode=="upcoming")
	{
		$year = date("Y");
		$month = date("m");
		$day = date("d");
		$hour = date("H");
		$minute = date("m");
		$mints = mktime($hour - 2,$minute,0,$month,$day,$year);
		$maxts = mktime($hour + 8,$minute,0,$month,$day,$year);
		
		$event_count = 0;
		$show_event_names = array("","Next Race","2nd Upcoming Race","3rd Upcoming Race","in 4 races","in 5 races","in 6 races");
		$event_query = lavu_query("select `lk_events`.`ts` as `ts`, `lk_events`.`time` as time, `lk_events`.`id` as id, `lk_event_types`.`title` as `title` from `lk_events` left join `lk_event_types` on lk_events.type = lk_event_types.id where `ts`>='$mints' and `ts`<='$maxts' and `lk_events`.`ms_start`='' and `lk_events`.`trackid`='[1]' order by `ts` asc limit 6",$trackid);
		while($event_read = mysqli_fetch_assoc($event_query))
		{
			$eventid = $event_read['id'];
			$event_count++;
			echo "<br>&event".$event_count."_name=" . $show_event_names[$event_count] . "&";//date("g:i a",$event_read['ts'])."&";
			
			$racer_count = 0;
			$sched_query = lavu_query("select * from `lk_event_schedules` left join `lk_customers` on `lk_event_schedules`.`customerid`=`lk_customers`.`id` where `eventid`='$eventid'");
			while($sched_read = mysqli_fetch_assoc($sched_query))
			{
				$racer_count++;
				$racer_name = $sched_read['racer_name'];
				$abbrev_name = trim(ucfirst($sched_read['f_name']) . " " . ucfirst(substr(trim($sched_read['l_name']),0,1)));
				if(trim($racer_name)=="")
					$racer_name = $abbrev_name;
				echo "<br>&event".$event_count."_racer".$racer_count."=" . $abbrev_name . "&";
			}
			echo "<br>&event".$event_count."_racer_count=" . $racer_count . "&";
		}
		echo "<br>&event_count=" . $event_count . "&";
	}
	else
	{
		if($eventid)
		{
			$display = "";
			$data = "";
			
			$event_title = "";
			$total_laps = 0;
			$score_by = "";
			$event_query = lavu_query("select * from `lk_events` where `id`='[1]'",$eventid);
			if(mysqli_num_rows($event_query))
			{
				$event_read = mysqli_fetch_assoc($event_query);
				$event_title = display_time($event_read['time']);
				$event_type = $event_read['type'];
				$event_date = $event_read['date'];
				
				$type_query = lavu_query("select * from `lk_event_types` where `id`='[1]'",$event_type);
				if(mysqli_num_rows($type_query))
				{
					$type_read = mysqli_fetch_assoc($type_query);
					$total_laps = $type_read['laps'];
					$score_by = $type_read['score_by'];
					$event_type_title = $type_read['title'];
					if($score_by==0) $event_race_format = "Best Lap";
					else if($score_by==1) $event_race_format = "Position"; 
					else $event_race_format = "";
					
					$event_title = $event_type_title;
				}
				
				$track_query = lavu_query("select * from `lk_tracks` where `id`='[1]'",$trackid);
				if(mysqli_num_rows($track_query))
				{
					$track_read = mysqli_fetch_assoc($track_query);
					$track_name = $track_read['title'];
					$track_sec_int = $track_read['seconds_intermediate'];
					$track_sec_adv = $track_read['seconds_advanced'];
				}
			}
			$data .= "&event_title=$event_title&";
			$data .= "&total_laps=$total_laps&";
			
			$track_first_pass = "";
			$fastest_lap_all = "";
			$sched_query = lavu_query("select `kartid`,`f_name`,`l_name`,`racer_name`,`lk_event_schedules`.`id` as `id`,`lk_customers`.`id` as `custid`,`lk_karts`.`number` as `number` from `lk_event_schedules` LEFT JOIN `lk_customers` ON `lk_event_schedules`.`customerid`=`lk_customers`.`id`  LEFT JOIN `lk_karts` ON `lk_event_schedules`.`kartid`=`lk_karts`.`id` where `lk_karts`.`number`!='' and `lk_event_schedules`.`eventid`='[1]'",$eventid);
			if(mysqli_num_rows($sched_query))
			{
				$racer_info = array();
				$lap_first_pass = array();
				$all_laps = array();
				$highest_lap = 0;
				
				$lap_grid = array();
				while($sched_read = mysqli_fetch_assoc($sched_query))
				{
					$schedid = $sched_read['id'];
					
					$racer_name = $sched_read['racer_name'];
					if(trim($racer_name)=="")
						$racer_name = trim($sched_read['f_name'] . " " . $sched_read['l_name']);
						
					$racer_info[$schedid]['name'] = $racer_name;
					$racer_info[$schedid]['custid'] = $sched_read['custid'];
					$racer_info[$schedid]['number'] = $sched_read['number'];
					$racer_info[$schedid]['kartid'] = $sched_read['kartid'];
					$racer_info[$schedid]['schedid'] = $schedid;
					
					$all_laps[$schedid] = array();
					
					$lap_grid_info = array();
					$lap_grid_info['racer_name'] = $racer_name;
					$lap_grid_info['number'] = $sched_read['number'];
					$lap_grid_info['custid'] = $sched_read['custid'];
					$lap_grid_info['schedid'] = $schedid;
					$lap_grid_info['laps'] = array();
					
					$r_lap = 0;
					$r_bestlap = 0;
					$r_lastlap = 0;
					$r_gap = 0;
					$r_passed = 0; 
					$r_first_pass = "";
					$r_last_ts = "";
					$lap_query = lavu_query("select * from `lk_laps` where `scheduleid`='[1]' order by `ms_this_lap` asc",$sched_read['id']);
					while($lap_read = mysqli_fetch_assoc($lap_query))
					{
						if(is_numeric($lap_read['pass']) && $lap_read['pass'] < 999 /*&& $r_lap < $total_laps + 1*/)
						{
							/*while($lap < $lap_read['pass'])
							{
								echo "<td>-</td>";
								$lap++;
							}
							echo "<td>".$lap_read['ms_duration']."</td>";*/
							
							if($r_lap > 0)
							{
								if($lap_read['ms_duration'] > 10)
								{
									$r_passed = $lap_read['ms_this_lap'];
									$r_lastlap = $lap_read['ms_duration'];
									$r_ts = (isset($lap_read['ts']))?$lap_read['ts']:"";
									
									if($r_last_ts=="" || $r_ts > $r_last_ts) $r_last_ts = $r_ts;
									if(($r_bestlap==0 || $r_bestlap * 1 > $r_lastlap * 1) && $r_lastlap > 0) 
									{
										//if(isset($_GET['test'])) echo "schedid: " . $sched_read['id'] . "." . $r_lap . " - " . $r_bestlap . ">".$r_lastlap."<br>";
										$r_bestlap = $r_lastlap;
									}
									$all_laps[$schedid][] = $r_lastlap;
									
									$lap_grid_info['laps'][] = $r_lastlap;
									$r_lap++;
								}
							}
							else
							{
								$racer_info[$schedid]['first_pass'] = $r_first_pass;
								$r_lap++;
							}
							if(!isset($lap_first_pass[$r_lap]) || $lap_first_pass[$r_lap] > $r_passed) $lap_first_pass[$r_lap] = $r_passed;
							if($r_first_pass=="") $r_first_pass = $r_passed;
							//echo "<br>fl:" . $r_lastlap . " fp:" . $r_first_pass;
						}
					}
					//echo "<br>best: $r_bestlap last: $r_lastlap";
					
					$racer_info[$schedid]['lap'] = $r_lap;
					$racer_info[$schedid]['showlap'] = $r_lap;
					if($r_lap > $total_laps) $racer_info[$schedid]['showlap'] = "*";
					$racer_info[$schedid]['bestlap'] = number_format($r_bestlap,3,".",",");
					$racer_info[$schedid]['lastlap'] = number_format($r_lastlap,3,".",",");
					$racer_info[$schedid]['passed'] = $r_passed;
					$racer_info[$schedid]['lastts'] = $r_last_ts;
					//$racer_info[$schedid]['schedid'] = $schedid;
					
					if($fastest_lap_all=="" || ($r_bestlap < $fastest_lap_all && $r_bestlap > 0))
						$fastest_lap_all = $r_bestlap;
					
					if($track_first_pass=="" || $track_first_pass > $r_passed) $track_first_pass = $r_passed;
					if($r_lap > $highest_lap) $highest_lap = $r_lap;
					
					$lap_grid[] = $lap_grid_info;
				}
				
				$display .= "<style>";
				$display .= "  body, table {font-size:14px;} ";
				$display .= "</style>";
				
				/*for($n=0; $n<=$highest_lap; $n++)
				{
					if(isset($lap_first_pass[$n]))
					{
						$display .= "<br>" . $n . ": " . $lap_first_pass[$n];
					}
				}*/
				
				$display .= "<table cellpadding=7>";
				$display .= "<tr>";
				$display .= "<td>Racer</td><td>Number</td><td>Lap</td><td>Best Lap</td><td>Last Lap</td><td>Gap</td><td>Pass $track_first_pass</td>";
				$display .= "</tr>";
				
				$n = 0;
				$ordered_racers = array();
				foreach($racer_info as $key=>$rinfo)
				{
					$setlap = $rinfo['lap'];
					
					if($setlap > 1)
					{
						//mail("corey@meyerwind.com",$score_by,$score_by,"From:info@poslavu.com");
						if($score_by==1)//$score_by=="Position" || $score_by==7)
						{
							if(isset($lap_first_pass[$setlap]))
								$setgap = number_format($rinfo['passed'] - $lap_first_pass[$setlap],3,".",",");
							$showgap = $setgap;
							//if($setlap < $highest_lap + 1)
								//$showgap = "-" . ($highest_lap - $setlap);
						}
						else
						{
							if($fastest_lap_all > 0)
								$setgap = number_format($rinfo['bestlap'] - $fastest_lap_all,3,".",",");
							$showgap = $setgap;
						}
						//$setgap = number_format($rinfo['passed'] - $lap_first_pass[$setlap],3,".",",");
						//if($setgap >= -0.001 && $setgap <= 0.001) $setgap = 0.000;
					}
					else $setgap = "";
					
					$rinfo['gap'] = $setgap;// - number_format(($rinfo['first_pass'] - $track_first_pass),3,".",",");
					$rinfo['showgap'] = $showgap;
					$placed = false;
					$ordracers = $ordered_racers;
					$ordered_racers = array();
					for($i=0; $i<count($ordracers); $i++)
					{
						//if(!$placed && ($rinfo['lap'] > $ordracers[$i]['lap'] || ($rinfo['lap']==$ordracers[$i]['lap'] && $rinfo['lastlap'] < $ordracers[$i]['lastlap'])))
						if(!$placed && ($rinfo['gap'] < $ordracers[$i]['gap'] || $ordracers[$i]['gap']=="") && $setlap > 1)
						{
							$ordered_racers[] = $rinfo;
							$placed = true;
						}
						$ordered_racers[] = $ordracers[$i];
					}
					if(!$placed) 
					{
						$ordered_racers[] = $rinfo;
					}
				}
				
				$placed_pos = 0;
				$rinfo_pos = array();	
				foreach($ordered_racers as $rinfo)
				{
					$placed_pos++;
					if(($rinfo['gap']>=-0.001 && $rinfo['gap']<=0.001) || $rinfo['gap']==0) $rinfo['gap'] = "";
					$display .= "<tr>";
					$display .= "<td>" . $rinfo['name'] . "</td>";
					$display .= "<td>" . $rinfo['number'] . "</td>";
					$display .= "<td>" . $rinfo['lap'] . "</td>";
					$display .= "<td>" . $rinfo['bestlap'] . "</td>";
					$display .= "<td>" . $rinfo['lastlap'] . "</td>";
					$display .= "<td>" . $rinfo['gap'] . "</td>";
					$display .= "<td>" . $rinfo['passed'] . "</td>";
					$display .= "</tr>";
					
					$n++;
					$data .= "&name_$n=".$rinfo['name'];
					$data .= "&number_$n=".$rinfo['number'];
					$data .= "&lap_$n=".$rinfo['lap'];
					$data .= "&showlap_$n=".$rinfo['showlap'];
					$data .= "&bestlap_$n=".$rinfo['bestlap'];
					$data .= "&lastlap_$n=".$rinfo['lastlap'];
					$data .= "&gap_$n=".$rinfo['gap'];
					$data .= "&showgap_$n=".$rinfo['showgap'];
					
					$r_vars = array();
					$r_vars['eventid'] = $eventid;
					$r_vars['customerid'] = $rinfo['custid'];
					//$r_vars['ms_duration'] = $rinfo['bestlap'];
					$r_vars['bestlap'] = $rinfo['bestlap'];
					$r_vars['lastlap'] = $rinfo['lastlap'];
					$r_vars['trackid'] = $trackid;
					$r_vars['date'] = $event_date;
					$r_vars['place'] = $placed_pos;
					$r_vars['passes'] = $rinfo['lap'];
					
					$set_rank = "";
					if(isset($track_sec_int) && $track_sec_int!="" && $track_sec_int > 0)
					{
						$set_rank = 1;
						if($rinfo['bestlap'] <= $track_sec_int) 
						{
							$set_rank = 2;
							if(isset($track_sec_adv) && $track_sec_adv!="" && $track_sec_adv > 0)
							{
								if($rinfo['bestlap'] <= $track_sec_adv)
									$set_rank = 3;
							}
						}
					}
					
					$r_vars['rank'] = $set_rank;
					$rr_query = lavu_query("select * from `lk_race_results` where `eventid`='[1]' and `customerid`='[2]'",$eventid,$rinfo['custid']);
					if(mysqli_num_rows($rr_query))
					{
						$rr_read = mysqli_fetch_assoc($rr_query);
						$r_vars['id'] = $rr_read['id'];
						if(isset($_GET['test']))
						{
							echo $rinfo['name'] . " set bestlap = " . $rinfo['bestlap'] . ", rank = " . $set_rank . " for id " . $rr_read['id'] . "<br>";
						}
						lavu_query("update `lk_race_results` set `bestlap`='[bestlap]',`lastlap`='[lastlap]',`trackid`='[trackid]',`date`='[date]',`ranking`='[rank]',`place`='[place]',`passes`='[passes]' where `id`='[id]'",$r_vars);
					}
					else
					{
						//if(isset($_GET['test']))
						//{
						//	echo $rinfo['name'] . " set bestlap = " . $rinfo['bestlap'] . ", rank = " . $set_rank . " for id " . $rr_read['id'] . "<br>";
						//}
						lavu_query("insert into `lk_race_results` (`eventid`,`customerid`,`bestlap`,`lastlap`,`trackid`,`date`,`ranking`,`place`,`passes`) values ('[eventid]','[customerid]','[bestlap]','[lastlap]','[trackid]','[date]','[rank]','[place]','[passes]')",$r_vars);
					}
					
					//mail("corey@poslavu.com","test",$rinfo['lastts'] . " " . $tsnow);
					if(isset($rinfo['lastts']) && $rinfo['lastts'] > (time() - 5))
						$data .= "&highlight_$n=1";
					else
						$data .= "&highlight_$n=0";
					
					if($rinfo['lap'] > $highest_lap) $highest_lap = $rinfo['lap'];
					$rinfo_pos[$rinfo['kartid']] = $placed_pos;
				}
				if($highest_lap==0) $highest_lap = 1;
				$laps_left = $total_laps - ($highest_lap - 1);
				if($laps_left < 0) $laps_left = 0;
				$data = "highest_lap=$highest_lap&laps_left=$laps_left&count=$n" . $data;
				$display .= "</table>";
				
				$display .= "<br>cc=" . $company_code_full;
				$display .= "<br>loc_id=" . reqvar("loc_id");
				$display .= "<br>eventid=" . reqvar("eventid");
			}
			else
			{
				$display .= "Nobody Scheduled";
				$data .= "&count=0&";
			}
			$data = "server_time=".date("h:i:s")."&".$data;
			
			if($mode=="data")
			{
				echo $data;
			}
			else if($mode=="results_graph")
			{
				
				require_once(dirname(__FILE__) . "/results_graph.php");
			}
			else if($mode=="results")
			{
				if(isset($_GET['test'])) echo "rank: $set_rank<br>";
				require_once(dirname(__FILE__) . "/results_sheet.php");
			}
			else if($mode=="info")
			{
				$icols = array();
				$iheader = "";
				
				$imax = 0;
				//echo "<table>";
				for($i=0; $i<count($lap_grid); $i++)
				{
					//echo "<tr>";
					//
					$iheader .= "<td><u>" . $lap_grid[$i]['racer_name'] ." (".$lap_grid[$i]['number'].")</u></td>";
					$irow = array();
					for($n=0; $n<count($lap_grid[$i]['laps']); $n++)
					{
						$iextra_style = "";
						if($lap_grid[$i]['laps'][$n] * 1 > 40)
							$iextra_style = " font-weight:bold";
						$irow[] = "<td bgcolor='#dddddd' style='border:solid 1px #777777;".$iextra_style."'>".number_format($lap_grid[$i]['laps'][$n],1,".",",")."</td>";
						if($n > $imax) $imax = $n;
					}
					$icols[] = $irow;
					//echo "</tr>";
				}
				//echo "</table>";
				
				echo "<table>";
				echo "<tr>";
				echo "<td>&nbsp;</td>";
				echo $iheader;
				echo "</tr>";
				echo "<tr>";
				echo "<td valign='top' align='center'>";
				echo "<table>";
				for($n=0; $n<=$imax; $n++)
				{
					echo "<tr><td style='border:solid 1px #777777' align='center'>" . ($n + 1) . "</td></tr>";
				}
				echo "</table>";
				echo "</td>";
				for($n=0; $n<count($icols); $n++)
				{
					echo "<td valign='top' align='center'>";
					echo "<table>";
					for($x=0; $x<count($icols[$n]); $x++)
					{
						echo "<tr>" . $icols[$n][$x] . "</tr>";
					}
					echo "</table>";
					echo "</td>";
				}
				echo "</tr>";
				echo "</table>";
			}
			else
			{
				echo $display;
			}
		}
	}
?>
