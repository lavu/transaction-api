<?php
	require_once(dirname(__FILE__) . "/../comconnect.php");
	//ini_set("display_errors","1");
	if(reqvar("tab_name") == "Group Events") {
		$parent_component = "group_events";
		$parent_component_file = "lavukart/group_events.php";
	} else if(reqvar("tab_name") == "Pit") {
		$parent_component = "events_overlay_wrapper;events";
		$parent_component_file = "lavukart/events_overlay_wrapper.php;lavukart/events.php";
	} else {
		$parent_component = "events_wrapper;events";
		$parent_component_file = "lavukart/events_wrapper.php;lavukart/events.php";
	}
?>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<style type="text/css">
<!--

body, table {
	font-family:Verdana, Arial, Helvetica, sans-serif;
}

.caltext {
	font-family: "Trebuchet MS", Arial, Helvetica, sans-serifVerdana, Geneva, sans-serif;
	font-size: 16px;
	font-weight: bold;
	color: #154473;
}
-->
</style>
</head>

<body style='margin:0px'>
<table width="423" height="374" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="20" height="40">&nbsp;</td>
    <td width="120" align="center" valign="middle">&nbsp;</td>
    <td width="134" onClick="window.location = '_DO:cmd=close_overlay'">&nbsp;</td>
    <td width="120" align="center" valign="middle">&nbsp;</td>'
    <td width="21">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="5" align="center" valign="top">
		<?php
			$locid = reqvar("locid");
			if($locid)
			{
	        	echo "<span style='color:#ffffff'>Select Track</b>:</font>";
				echo "<br><br>";
				echo "<table cellspacing=4 cellpadding=12>";
				$track_query = lavu_query("select * from `lk_tracks` order by `_order` asc");
				while($track_read = mysqli_fetch_assoc($track_query))
				{
					echo "<tr><td style='border:solid 1px #777777; padding-left:40px; padding-right:40px' bgcolor='#ffffff' onclick='window.location = \"_DO:cmd=close_overlay&c_name=".$parent_component."&f_name=".$parent_component_file."?mode=set_track&set_trackid=".$track_read['id']."&set_tracktitle=" . $track_read['title'] . "\"'>" . $track_read['title'] . "</td></tr>";
				}
				echo "</table>";
			}
			else
			{
				echo "Error: No Location Sent";
			}
		?>
    </td>
  </tr>
</table>
</body>

