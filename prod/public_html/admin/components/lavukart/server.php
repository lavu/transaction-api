<?php
  	require_once("../comconnect.php");
	
	function find_duration($time_end,$ts_end,$time_start,$ts_start,$set_pauses)
	{
		$duration = $time_end - $time_start;
		$pauses = trim($set_pauses); //----------------- subtract pauses
		if($pauses!="")
		{
			$pause_parts = explode(" ",$pauses);
			for($p=0; $p<count($pause_parts); $p++)
			{
				$pause_info = trim($pause_parts[$p]);
				$pause_info = explode("=",$pause_info);
				if(count($pause_info) > 1)
				{
					$pause_duration = $pause_info[1];
					$pause_time = explode("-",$pause_info[0]);
					if(count($pause_time)>1)
					{
						$pause_start = $pause_time[1];
						$pause_end = $pause_time[0];
						
						if($ts_end >= $pause_end && $ts_start <= $pause_start)
						{
							$duration -= $pause_duration;
						}
					}
				}
			}
		}
		return $duration;
	}
	
	if(isset($_GET['list_transponders']))
	{
		$eventid = $_GET['eventid'];
		$trackid = $_GET['trackid'];
		if($eventid=="current" || $eventid=="active")
		{
			$active_query = lavu_query("select * from `lk_events` where `ms_start`!='' and `ms_end`='' and `trackid`='[1]'",$trackid);
			if(mysqli_num_rows($active_query))
			{
				$active_read = mysqli_fetch_assoc($active_query);
				$eventid = $active_read['id'];
			}
		}
		$sched_query = lavu_query("select * from `lk_event_schedules` where `eventid`='[1]'",$eventid);
		while($sched_read = mysqli_fetch_assoc($sched_query))
		{
			lavu_query("delete from `lk_laps` where `scheduleid`='[1]'",$sched_read['id']);
		}
		
		$kcount = 0;
		$kart_query = lavu_query("SELECT `lk_event_schedules`.`id` as `scheduleid`, `lk_karts`.`transponderid`, `lk_karts`.`number` FROM `lk_karts` LEFT JOIN `lk_event_schedules` ON `lk_karts`.`id`=`lk_event_schedules`.`kartid` LEFT JOIN `lk_events` ON `lk_event_schedules`.`eventid`=`lk_events`.`id` where `lk_events`.`id`='[1]'",$eventid);
		while($kart_read = mysqli_fetch_assoc($kart_query))
		{
			$kcount++;
			echo "&kart".$kcount."=".$kart_read['transponderid']."&<br>";
		}
		echo "&kart_count=".$kcount."&";
	}
	else if(isset($_GET['transponderid']))
	{
		$transponderid = $_GET['transponderid'];
		$msts = $_GET['msts'];
		$setlap = 0;
		$last_lap = 0;
		$duration = 0;
		
		$response_code = "";
		$response = "";
		//lavu_select_db("poslavu_DEVKART_db");
		//$kart_query = lavu_query("select * from `lk_karts` LEFT JOIN `lk_event_schedules` ON `lk_event_schedules`.`kartid`=`lk_karts`.`id`
		$kart_query = lavu_query("SELECT `lk_event_schedules`.`id` as `scheduleid`, `lk_events`.`pauses` as `pauses`, `lk_events`.`id` as `eventid`, `lk_event_types`.`laps` as `laps` FROM `lk_karts` LEFT JOIN `lk_event_schedules` ON `lk_karts`.`id`=`lk_event_schedules`.`kartid` LEFT JOIN `lk_events` ON `lk_event_schedules`.`eventid`=`lk_events`.`id` LEFT JOIN `lk_event_types` ON `lk_event_types`.`id`=`lk_events`.`type` where `transponderid`='[1]' and `lk_events`.`ms_start`!='' and `lk_events`.`ms_end`=''",$transponderid);
		if(mysqli_num_rows($kart_query))
		{
			while($kart_read = mysqli_fetch_assoc($kart_query))
			{
				$reorder = false;
				$scheduleid = $kart_read['scheduleid'];
				$last_lap_query = lavu_query("select * from `lk_laps` where `scheduleid`='$scheduleid' order by `ts` desc limit 1");
				if(mysqli_num_rows($last_lap_query))
				{
					$last_lap_read = mysqli_fetch_assoc($last_lap_query);
					$setlap = $last_lap_read['pass'] + 1;
					$last_lap = $last_lap_read['ms_this_lap'];
					
					$duration = find_duration($msts,time(),$last_lap,$last_lap_read['ts'],$kart_read['pauses']);
					//$duration = $msts - $last_lap;				
					if($duration < 0)
					{
						$reorder = true;
						$duration = 0;
					}
					//if($duration < -43200) $duration = $duration * 1 + 86400;
					//else if($duration < 0) $duration = $duration * 1 + 4294.967295; 
				}
				else
				{
					$setlap = 0;
					$last_lap = 0;
					$duration = 0;
				}
								
				$vars = array();
				$vars['scheduleid'] = $scheduleid;
				$vars['ms_this_lap'] = $msts;
				$vars['ms_last_lap'] = $last_lap;
				$vars['ms_duration'] = $duration;
				$vars['pass'] = $setlap;
				$vars['ts'] = time();//date("YmdHis");
				$dup_query = lavu_query("select * from `lk_laps` where `scheduleid`='[scheduleid]' and `ms_this_lap`='[ms_this_lap]'",$vars);
				if(mysqli_num_rows($dup_query) < 1)
				{
					if($duration > 10 || $setlap <=0)
					{
						lavu_query("insert into `lk_laps` (`scheduleid`,`ms_this_lap`,`ms_last_lap`,`ms_duration`,`pass`,`ts`) values ('[scheduleid]','[ms_this_lap]','[ms_last_lap]','[ms_duration]','[pass]','[ts]')",$vars);
						
						if($setlap==$kart_read['laps'])
						{
							$minlap_completed = $setlap;
							//$fin_query = lavu_query("select distinct `scheduleid` from `lk_laps` where `pass`='[1]'",$setlap);
							$sch_query = lavu_query("select * from `lk_event_schedules` where `eventid`='[1]'",$kart_read['eventid']);
							while($sch_read = mysqli_fetch_assoc($sch_query))
							{
								$slp_query = lavu_query("select * from `lk_laps` where `scheduleid`='[1]' order by (`pass` * 1) desc limit 1",$sch_read['id']);
								if(mysqli_num_rows($slp_query))
								{
									$slp_read = mysqli_fetch_assoc($slp_query);
									$slp_pass = $slp_read['pass'];
								}
								else
								{
									$slp_pass = 0;
								}
								if($slp_pass < $minlap_completed)
									$minlap_completed = $slp_pass;
							}
							if($minlap_completed==$setlap)
							{
								$cls_query = lavu_query("select * from `lk_events` where `id`='[1]'",$lart_read['eventid']);
								if(mysqli_num_rows($cls_query))
								{
									$cls_read = mysqli_fetch_assoc($cls_query);
									mail("cp_notifications@lavu.com","final laps","final laps: pass " . $setlap,"From: finallaps@poslavu.com");
								}
							}
							//else mail("corey@meyerwind.com","min laps completed: $minlap_completed","final laps: pass " . $setlap,"From: finallaps@poslavu.com");
						}
						
						if(!$reorder)
						{
							$response_code = "passed";
							$response = "passed lap $setlap";
						}
						else
						{
							$response_code = "passed - reordering";
							$response = "passed lap $setlap - reordering";
							
							$last_lap_ms = 0;
							$last_lap_ts = 0;
							$lap_num = 0;
							$lap_query = lavu_query("select * from `lk_laps` where `scheduleid`='[scheduleid]' order by `ms_this_lap` asc",$vars);
							while($lap_read = mysqli_fetch_assoc($lap_query))
							{
								$lap_ms = $lap_read['ms_this_lap'];
								$lap_ts = $lap_read['ts'];
								if($last_lap_ms==0) $duration = 0;
								else 
								{
									$duration = find_duration($lap_ms,$lap_ts,$last_lap_ms,$last_lap_ts,$kart_read['pauses']);
									//$duration = $lap_ms - $last_lap_ms;								
									//$duration = subtract_pauses($duration,time(),$kart_read['pauses']);
								}
								
								$uvars = array();
								$uvars['ms_last_lap'] = $last_lap_ms;
								$uvars['ms_duration'] = $duration;
								$uvars['pass'] = $lap_num;
								$uvars['lapid'] = $lap_read['id'];
								lavu_query("update `lk_laps` set `ms_last_lap`='[ms_last_lap]', `ms_duration`='[ms_duration]', `pass`='[pass]' where id='[lapid]'",$uvars);
								//mail("corey@meyerwind.com","success: $duration","success","From: laps@poslavu.com");
								
								$lap_num++;
								$last_lap_ms = $lap_ms;
								$last_lap_ts = $lap_ts;
							}
						}
					}
					else
					{
						$response_code = "short";
						$response = "too short for lap $setlap";
					}
				}
				else
				{
					$response_code = "duplicate";
					$response = "received duplicate for lap $setlap";
				}
				//echo "insert into `lk_laps` (`scheduleid`,`ms_this_lap`,`ms_last_lap`,`ms_duration`) values ('[scheduleid]','[ms_this_lap]','[ms_last_lap]','[ms_duration]')";
				//echo "<br>" . $scheduleid;
				//echo "<br>" . $msts;
				//echo "<br>" . $last_lap;
				//echo "<br>" . $duration;
			}
		}
		else
		{
			$response_code = "not_scheduled";
			$response = "transponder not scheduled";
			//echo "SELECT `lk_event_schedules`.`id` as `scheduleid` FROM `lk_karts` LEFT JOIN `lk_event_schedules` ON `lk_karts`.`id`=`lk_event_schedules`.`kartid` LEFT JOIN `lk_events` ON `lk_event_schedules`.`eventid`=`lk_events`.`id` where `eventid`='115' and `transponderid`='$transponderid' and `lk_events`.`ms_start`!='' and `lk_events`.`ms_end`=''";
		}
		
		//mail("corey@poslavu.com","Transponder $transponderid","Pass: $msts","From:info@polepositionraceway.com");
		echo "1|".$response_code."|".$response."|".$transponderid."|".$msts."|lap:".$setlap."|duration:".$duration;
		exit();
	}
?>
