<?php
	//error_log ("The results graph is: ".print_r(get_defined_vars(), 1));
	if(isset($_GET['graphtype']) && $_GET['graphtype']=="new" && false)
	{		 
		//error_log('new graph typpe..');
		// create image
		$image = imagecreatetruecolor(500, 330);
		 
		// allocate some solors
		$white    = imagecolorallocate($image, 0xFF, 0xFF, 0xFF);
		$gray1	  = imagecolorallocate($image, 0xb6, 0xb6, 0xb8);
		$gray2    = imagecolorallocate($image, 0xD8, 0xD9, 0xDa);
		$gray3    = imagecolorallocate($image, 0xB6, 0xB6, 0xB6);
		$darkgray = imagecolorallocate($image, 0x90, 0x90, 0x90);
		$navy     = imagecolorallocate($image, 0x00, 0x00, 0x80);
		$darknavy = imagecolorallocate($image, 0x00, 0x00, 0x50);
		$red      = imagecolorallocate($image, 0xFF, 0x00, 0x00);
		$darkred  = imagecolorallocate($image, 0x90, 0x00, 0x00);
	
		// prepare data
		if(isset($total_laps) && $total_laps > 0 && $total_laps < 999)
		{
			$showsched = $_GET['showsched'];
			$lap_titles = array();
			for($i=0; $i<$total_laps; $i++)
			{
				$lap_titles[] = ($i + 1);
			}
			$self_index = 0;
			$points = array();
			$scount = 0;
			$avg = array();
			$avg_count = array();
			for($i=0; $i<$total_laps; $i++)
			{
				//$avg[$i] = 0;
				$avg_count[$i] = 0;
			}
			foreach($all_laps as $schedid => $laps)
			{
				$graph_laps = array();
				for($i=0; $i<$total_laps; $i++)
				{
					if(isset($laps[$i]))
					{
						$graph_laps[] = $laps[$i];
						$avg[$i] += $laps[$i];
						$avg_count[$i] ++;
					}
				}
				if($schedid==$showsched || $scount==0)
				{
					$points[$scount] = array();
					if($schedid==$showsched)
					{
						$points[$scount]['weight'] = 3;
						$points[$scount]['color'] = $red;
						if($scount==1)
							$points[$scount]['legend'] = "Yours (Best)";
						else
							$points[$scount]['legend'] = "Yours";
						$race_place = $scount;
						$self_index = $scount;
					}
					else
					{
						$points[$scount]['weight'] = 1;
						$points[$scount]['color'] = $navy;
						$points[$scount]['legend'] = "Best";
					}
					//$p1->SetLegend("");
					$points[$scount]['points'] = $graph_laps;
					$scount++;
					//echo "<td>" . $laps[$i] . "</td>";
				}
			}
			
			for($i=0; $i<$total_laps; $i++)
			{
				if($avg_count[$i] > 0)
					$avg[$i] = $avg[$i] / $avg_count[$i];
			}
			$points[$scount]['weight'] = 1;
			$points[$scount]['color'] = $darkgrey;
			$points[$scount]['legend'] = "Avg";
			$points[$scount]['points'] = $avg;
			$scount++;
					 
			// make the 3D effect
			/*for ($i = 60; $i > 50; $i--) {
			   imagefilledarc($image, 50, $i, 100, 50, 0, 45, $darknavy, IMG_ARC_PIE);
			   imagefilledarc($image, 50, $i, 100, 50, 45, 75 , $darkgray, IMG_ARC_PIE);
			   imagefilledarc($image, 50, $i, 100, 50, 75, 360 , $darkred, IMG_ARC_PIE);
			}
			 
			imagefilledarc($image, 50, 50, 100, 50, 0, 45, $navy, IMG_ARC_PIE);
			imagefilledarc($image, 50, 50, 100, 50, 45, 75 , $gray, IMG_ARC_PIE);
			imagefilledarc($image, 50, 50, 100, 50, 75, 360 , $red, IMG_ARC_PIE);*/
			
			// fill the background
			//imagefilledrectangle($image, 0, 0, 249, 249, $bg);
			
			// draw a polygon
			/*$values = array(
						40,  50,  // Point 1 (x, y)
						20,  240, // Point 2 (x, y)
						60,  60,  // Point 3 (x, y)
						240, 20,  // Point 4 (x, y)
						50,  40,  // Point 5 (x, y)
						10,  10   // Point 6 (x, y)
						);*/
			//imagefilledpolygon($image, $values, 6, $blue);
			
			
			function imagelinethick($image, $x1, $y1, $x2, $y2, $color, $thick = 1)
			{
				/* this way it works well only for orthogonal lines
				imagesetthickness($image, $thick);
				return imageline($image, $x1, $y1, $x2, $y2, $color);
				*/
				if ($thick == 1) {
					return imageline($image, $x1, $y1, $x2, $y2, $color);
				}
				$t = $thick / 2 - 0.5;
				if ($x1 == $x2 || $y1 == $y2) {
					return imagefilledrectangle($image, round(min($x1, $x2) - $t), round(min($y1, $y2) - $t), round(max($x1, $x2) + $t), round(max($y1, $y2) + $t), $color);
				}
				$k = ($y2 - $y1) / ($x2 - $x1); //y = kx + q
				$a = $t / sqrt(1 + pow($k, 2));
				$points = array(
					round($x1 - (1+$k)*$a), round($y1 + (1-$k)*$a),
					round($x1 - (1-$k)*$a), round($y1 - (1+$k)*$a),
					round($x2 + (1+$k)*$a), round($y2 - (1-$k)*$a),
					round($x2 + (1-$k)*$a), round($y2 + (1+$k)*$a),
				);
				imagefilledpolygon($image, $points, 4, $color);
				return imagepolygon($image, $points, 4, $color);
			} 
			
			// draw racesheet
			//-------------------------------------------------------------------//
			// white background 
			imagefilltoborder($image,0,0,$white,$white);
			
			// configuration variables
			$rows = 12;
			$cols = $total_laps;//14;
			$cellwidth = 400 / $cols;
			if($cellwidth > 60) $cellwidth = 60;
			$cellheight = 20 * (12 / $rows);
			$chartx = 30;
			$charty = 30;
			$fontsize = 4;
			
			function chart_rectangle($image,$x1,$y1,$x2,$y2,$c)
			{
				global $chartx;
				global $charty;
				imagefilledrectangle($image, $chartx + $x1, $charty + $y1, $chartx + $x2, $charty + $y2, $c);
			}
			function chart_line($image,$x1,$y1,$x2,$y2,$c,$w)
			{
				global $chartx;
				global $charty;
				imagelinethick($image, $chartx + $x1, $charty + $y1, $chartx + $x2, $charty + $y2, $c, $w);
			}
			function draw_string($image,$x,$y,$string,$font_size,$c,$align="left")
			{
				//$font_size = 5;
				$width=imagefontwidth($font_size)*strlen($string);
				$height=imagefontheight($font_size)*2;
				$len=strlen($string);
				
				for($i=0;$i<$len;$i++)
				{
					$xpos=$x + $i*imagefontwidth($font_size);
					if($align=="center") $xpos -= ($width / 2);
					else if($align=="right") $xpos -= $width;
					$ypos=$y;//rand(0,imagefontheight($font_size));
					if($align=="middle") $ypos -= ($height / 4);
					imagechar($image,$font_size,$xpos,$ypos,$string,$c);
					$string = substr($string,1);    
					
				}
			}
			
			// find highest and lowest points
			$lowest = "";
			$highest = "";
			//$track_sec_int = $track_read['seconds_intermediate'];
			//$track_sec_adv = $track_read['seconds_advanced'];		
			$lowest = $track_sec_adv - 7;
			$highest = $track_sec_int + 7;
			
			// setup vertical grey background bars    track_sec_adv = 22 track_sec_int = 24 lowest - 21 highest - 29 diff - 8 (diff:8 - (highest:29 - track_sec_int:22))
			$diff = $highest - $lowest;
			$mult = ($rows * $cellheight) / $diff;
			
			$gyx = $cols * $cellwidth; 
			$gy1 = floor(($highest - $track_sec_int) * $mult);
			$gy2 = floor(($diff - ($track_sec_adv - $lowest)) * $mult);
			$gy3 = $rows * $cellheight;
			
			chart_rectangle($image, 0, 0, $gyx, $gy1, $gray1);
			chart_rectangle($image, 0, $gy1, $gyx, $gy2, $gray2);
			chart_rectangle($image, 0, $gy2, $gyx, $gy3, $white);
			 
			// draw rectangle grid
			for($r=0; $r<$rows; $r++)
			{
				for($c=0; $c<$cols; $c++)
				{
					$x1 = $c * $cellwidth;
					$x2 = ($c + 1) * $cellwidth;
					$y1 = $r * $cellheight;
					$y2 = ($r + 1) * $cellheight;
					chart_line($image, $x1, $y1, $x2, $y1, $darkgray, 2);
					chart_line($image, $x2, $y2, $x1, $y2, $darkgray, 2);
					if($cols< 20 || $c==$cols-1)
						chart_line($image, $x2, $y1, $x2, $y2, $darkgray, 2);
					if($cols < 20 || $c==0)
						chart_line($image, $x1, $y2, $x1, $y1, $darkgray, 2);
				}
			}
						
			// define data
			$datalist = array();
			$datacolors = array();
			$dataweight = array();
			for($x=0; $x<count($points); $x++)
			{
				$datalist[] = $points[$x]['points'];
				$datacolors[] = $points[$x]['color'];
				$dataweight[] = $points[$x]['weight'];
				//$pout = "";
				//for($y=0; $y<count($points[$x]['points']); $y++) $pout .= "\n" . $y . " - " . $points[$x]['points'][$y];
				//mail("corey@poslavu.com","datalist","count: " . count($datalist) . $pout, "From: info@poslavu.com");
			}
			//$datalist[] = array(26.45,26.14,26.55,26.34,25.54,26.53,26.14,26.24,25.18,26.56,26.34,26.32,26.43,26.23);
			//$datalist[] = array(27.45,27.14,27.55,25.34,28.54,28.53,26.14,27.24,25.88,27.56,27.04,27.62,28.43,28.03);
			
			//$datacolors = array($navy,$red,$darkred);
			 
			$diff = $highest - $lowest;
			$mult = ($rows * $cellheight) / $diff;
			$labels = array();//38,36,34,32);
			for($n=$lowest; $n<=$highest; $n+=2)
				$labels[] = $n;
				
			for($n=0; $n<count($labels); $n++)
			{
				$val = $labels[$n];
				$val_diff = $highest - $val;
				draw_string($image,$chartx + ($cols * $cellwidth) + 10,$charty + floor($val_diff * $mult),$val,$fontsize,$darkgrey,"middle");
			}
			/*for($i=0; $i<count($datalist); $i++)
			{
				$data = $datalist[$i];
				for($n=0; $n<count($data); $n++)
				{
					$val = $data[$n];
					if($lowest=="" || $val < $lowest) $lowest = $val;
					if($highest=="" || $val > $highest) $highest = $val;
				}
			}*/
			
			$debug = "";
			if($lowest!="" && $highest!="")
			{
				for($i=0; $i<count($datalist); $i++)
				{
					$data = $datalist[$i];
					$dc = $datacolors[$i];
					$dweight = $dataweight[$i];
					$lastx = "";
					$lasty = "";
					for($n=0; $n<count($data); $n++)
					{
						$val = $data[$n];
						$val_diff = $highest - $val;
						$val_x = ($n + 1) * $cellwidth;
						$val_y = floor($val_diff * $mult);
						if($n==0)
						{
							$lastx = $n * $cellwidth;
							$lasty = $val_y;
						}
						chart_line($image, $lastx, $lasty, $val_x, $val_y, $dc, $dweight);
						//$debug .= $lastx . "/" . $lasty . "_" . $val_x . "/" . floor(($val_diff * $mult)) . " ";
						$lastx = $val_x;
						$lasty = $val_y;
						
						if($i==$self_index)
						{
							$c = $n + 1;
							$show_val = number_format($val,"2",":","");
							$yoffset = 15;
							if(($c % 2)==0)
								$yoffset = $yoffset + ($fontsize * 5);
							if($cols < 20)
								draw_string($image,$chartx + ($c * $cellwidth),(($rows + 1) * $cellheight) + $yoffset,$show_val,$fontsize,$darkgrey,"center");
						}
					}
				}
			}
			 
			imagefilledrectangle($image, 0, 0, $chartx + 400, $charty - 1, $white); 
			$colstep=1;
			if($cols > 20) $colstep = 10;
			for($c=1; $c<=$cols; $c+=$colstep)
			{
				draw_string($image,$chartx + ($c * $cellwidth),10,$c,$fontsize,$darkgrey,"center");
			}
			//imagelinethick($image, 0, 0, 200, 200, $navy, 6);
			// flush image
			header('Content-type: image/png');
			imagepng($image);
			imagedestroy($image);
		}
	}
	else
	{
		//error_log('not new graph typpe..');
		$lap_titles = array();
		for($i=0; $i<$total_laps; $i++)
		{
			$lap_titles[] = ($i + 1);
		}
		
		//echo "results";
		//exit();
		require_once ('/home/poslavu/public_html/3rd_party/jpgraph/src/jpgraph.php');
		require_once ('/home/poslavu/public_html/3rd_party/jpgraph/src/jpgraph_line.php');
		require_once ('/home/poslavu/public_html/3rd_party/jpgraph/src/jpgraph_bar.php');
		
		$showsched = $_GET['showsched'];
		
		// New graph with a background image and drop shadow
		$graph = new Graph(750,300);
		//$graph->SetBackgroundImage("/home/poslavu/public_html/3rd_party/jpgraph/src/Examples/tiger_bkg.png",BGIMG_FILLFRAME);
		//$graph->SetShadow();
		
		// Use an integer X-scale
		$graph->SetScale("textlin");
		
		// Set title and subtitle
		$graph->title->Set("Race Results");
		
		// Use built in font
		$graph->title->SetFont(FF_FONT1,FS_BOLD);
		
		// Make the margin around the plot a little bit bigger
		// then default
		$graph->img->SetMargin(40,140,40,80);	
		
		// Slightly adjust the legend from it's default position in the
		// top right corner to middle right side
		//$graph->legend->Pos(0.05,0.5,"right","center");
		
		// Display every 10:th datalabel
		//$graph->xaxis->SetTextTickInterval(6);
		//$graph->xaxis->SetTextLabelInterval(2);
		//$graph->xaxis->SetTickLabels($lap_titles);
		//$graph->xaxis->SetLabelAngle(90);
		
		// Create a red line plot
		$scount = 0;
		$avg = array();
		$avg_count = array();
		//if( isset($_GET['nialls_testing']))
			//error_log( print_r($all_laps,1));
			//error_log(print_r($total_laps,1));

		for($i=0; $i<$total_laps; $i++)
		{
			//$avg[$i] = 0;
			$avg_count[$i] = 0;
		}
		
		$points = array();
		foreach($all_laps as $schedid => $laps)
		{
			if( $total_laps > count($laps) ){
				while(count($laps)< $total_laps)
					$laps[]= 0;
			}

			$scount ++;
			$graph_laps = array();
			for($i=0; $i<$total_laps; $i++)
			{
				if(isset($laps[$i]))
				{
					$graph_laps[] = $laps[$i];
					$avg[$i] += $laps[$i];
					$avg_count[$i] ++;
				}
			}
			if($schedid==$showsched || $scount==1)
			{
				$points[$scount] = new LinePlot($graph_laps);
				if($schedid==$showsched)
				{
					$points[$scount]->SetWeight(3);
					$points[$scount]->SetColor("#880000");
					if($scount==1)
						$points[$scount]->SetLegend("Your Laps (Best)");
					else
						$points[$scount]->SetLegend("Your Laps");
					$race_place = $scount;
				}
				else
				{
					$points[$scount]->SetWeight(1);
					$points[$scount]->SetColor("#000088");
					$points[$scount]->SetLegend("Best");
				}
				//$p1->SetLegend("");
				$graph->Add($points[$scount]);
				//echo "<td>" . $laps[$i] . "</td>";
			}
		}
		
		for($i=0; $i<$total_laps; $i++)
		{
			if($avg_count[$i] > 0)
				$avg[$i] = $avg[$i] / $avg_count[$i];
		}
		
		$p1 = new LinePlot($avg);
		$p1->SetColor("#008800");
		$p1->SetWeight(1);
		$p1->SetLegend("Average");
		$graph->Add($p1);
			
		// Finally output the  image
		$graph->Stroke();
	}
?>
