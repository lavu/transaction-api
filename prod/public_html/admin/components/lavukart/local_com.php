<?php
//error_log("local_com non-dev");
session_start();
require_once(dirname(__FILE__) . "/../comconnect.php");
$requri = $_SERVER['REQUEST_URI'];
$uriparts = explode("lavukart/",$requri);
if(count($uriparts) > 1) {
   $uripage = $uriparts[1];
   $uriparts = explode("?",$uripage);
   $uripage = $uriparts[0];
   if(count($uriparts) > 1) {
     $urivars = $uriparts[1];
   }
   else $urivars = "";
if($uripage =="calendar.php") {
//--------------------------------------------------- calendar.php ----------------------------------------------//
?>

<?php
	//require_once(dirname(__FILE__) . "/../comconnect.php");
	$year = reqvar("year",date("Y"));
	$month = reqvar("month",date("m"));
	$day = reqvar("day",date("d"));
	
	$pyear = $year;
	$pmonth = $month - 1;
	if($pmonth < 1) {$pmonth = 12;$pyear--;}
	$nyear = $year;
	$nmonth = $month + 1;
	if($nmonth > 12) {$nmonth = 1;$nyear++;}
	
	$tab_name = reqvar("tab_name");
	if(isset($_GET['tab_name'])) $tab_name = $_GET['tab_name'];
	if ($tab_name == "Group Events") {
		$parent_component = "group_events";
		$parent_component_file = "lavukart/group_events.php";
	} else if ($tab_name == "new_pit") {
		$parent_component = "pit_schedule;pit_schedule_wrapper";
		$parent_component_file = "lavukart/pit_schedule.php;lavukart/pit_schedule_wrapper.php";
	} else {
		/*if($company_code=="pole_position_raceway" && date("Y-m-d") < "2011-01-14")
		{
			$parent_component = "events";
			$parent_component_file = "lavukart/events.php";
		}
		else
		{*/
			$parent_component = "events;events_wrapper";
			$parent_component_file = "lavukart/events.php;lavukart/events_wrapper.php";
		//}
		//mail("corey@meyerwind.com","cc: $company_code $parent_component","cc","From:info@poslavu.com");
	}
	
	function create_calendar($year,$month,$day)
	{
		global $parent_component;
		global $parent_component_file;
	
		$js_cal_click = "cal_click";
		$calwidth = 51;
		$calheight = 48;
		$allow_past = true;

		$fieldname = "cal_cell";		
		$str = "";
		$cal_js = "";

		$str .= "<script language='javascript'>";
		$str .= "function $js_cal_click(year, month, day) { ";
		$str .= "window.location = \"_DO:cmd=close_overlay&f_name=".$parent_component_file."&c_name=".$parent_component."?year=\" + year + \"&month=\" + month + \"&day=\" + day; ";
		$str .= "} ";
		$str .= "</script>";
		
		$str .= "<div id='calendar'>";
		$str .= "<table cellpadding=2><td class='caltext'>".date("M Y",mktime(0,0,0,$month,1,$year))."</td></table>";
		$str .= "<table cellspacing=0>";
		$r=1;$day=1;
		while($r<=6)
		{
			$str .= "<tr>";
			if($r==1)
			{
				$c=date("w",mktime(0, 0, 0, $month, 1, $year))+1;
				$x=1;
				while($x<$c)
				{
				  $str .= "<td width=$calwidth height=$calheight>&nbsp;</td>";
				  $x=$x+1;
				}
			}
			else $c=1;
			
			while($c<=7)
			{
				if($day<10) $day="0".$day;
				$datecheck = $year."-".$month."-".$day;
				
				if((($r+$c)%2)==0) 
					$sbg="#9fd4f0";
				else 
					$sbg="#5bb4e2";
					
				if(checkdate($month,$day,$year))
				{						
					$dayoftheweek = date("w",mktime(0,0,0,$month,$day,$year)) * 1 + 1;
	
					$msg = date("l",mktime(0,0,0,$month,$day,$year));
					$clr = "000000";
					
					if($allow_past || date("Y-m-d") <= date("Y-m-d",mktime(0,0,0,$month,$day,$year)))
						$allow = true;
					else
					{
						$allow = false;
						$clr = "666699";
					}
					
					$datenum = ($month * 100) + $day;
					$cal_js .= "cal_colors[$datenum] = '$sbg'; ";
					//if($month==$set_month && $year==$set_year && $day==$set_day)
					if($month==date("m") && $year==date("Y") && $day==date("d"))
					{
						$extra_style_code = "background: URL(images/btn_cal_day.png); background-position:center center; background-repeat:no-repeat; ";
						//$extra_style_code = "border:solid 2px black; ";
						$sbg = "#00ff00";
						$cal_js .="selected_datenum = '$datenum'; ";
					}
					else $extra_style_code = "background: URL(images/btn_cal_day_lit.png); background-position:center center; background-repeat:no-repeat; ";

					
					$str .= "<td id='".$fieldname."_date_$datecheck' class='cal' title='$msg' alt='$msg'";
					$str .= " style='cursor:pointer; ";
					$str .= $extra_style_code;
					$str .= "'";
					if($allow)
						$str .= " onclick='$js_cal_click($year, $month, $day)'";
					$str .= " width=$calwidth height=$calheight align='center' valign='center'>";
					$str .= "<font color='#$clr'>";
					$str .= ($day*1);
					$str .= "</font>";
					$str .= "</td>";						
				}
				else $str .= "<td width=$calwidth height=$calheight>&nbsp;</td>";
	
				$c++;$day++;
			}
			$str .= "</tr>";
			$r++;
		}
		$str .= "</table>";
		$str .= "</div>";
		return $str;
	}	
?>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<style type="text/css">
<!--

.caltext {
	font-family: "Trebuchet MS", Arial, Helvetica, sans-serifVerdana, Geneva, sans-serif;
	font-size: 16px;
	font-weight: bold;
	color: #154473;
}
-->
</style>
</head>

<body style='margin:0px'>
<table width="423" height="374" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="20" height="40">&nbsp;</td>
    <td width="120" align="center" valign="middle" onClick="window.location = '<?php echo "calendar.php?year=$pyear&month=$pmonth&day=$day&tab_name=$tab_name"; ?>'">&nbsp;</td>
    <td width="134" onClick="window.location = '_DO:cmd=close_overlay'">&nbsp;</td>
    <td width="120" align="center" valign="middle" onClick="window.location = '<?php echo "calendar.php?year=$nyear&month=$nmonth&day=$day&tab_name=$tab_name"; ?>'">&nbsp;</td>
    <td width="21">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="5" align="center" valign="middle">
		<?php
        	echo create_calendar($year,$month,$day);
		?>
    </td>
  </tr>
</table>
</body>


<?php

} else if($uripage =="calendar_wrapper.php") {
//--------------------------------------------------- calendar_wrapper.php ----------------------------------------------//
?>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<style type="text/css">
<!--

.caltext {
	font-family: "Trebuchet MS", Arial, Helvetica, sans-serifVerdana, Geneva, sans-serif;
	font-size: 16px;
	font-weight: bold;
	color: #154473;
}
-->
</style>
</head>

<body style='margin:0px'>
<table width="423" height="374" border="0" cellspacing="0" cellpadding="0" background="images/win_overlay_calendar_full.png">
  <tr>
    <td width="20" height="40">&nbsp;</td>
    <td width="120" align="center" valign="middle"><img src="images/btn_cal_prev.png" width="120" height="40"></td>
    <td width="134">&nbsp;</td>
    <td width="120" align="center" valign="middle"><img src="images/btn_cal_next.png" width="120" height="40"></td>
    <td width="21">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="5" align="center" valign="middle">&nbsp;</td>
  </tr>
</table>
</body>
</html>

<?php

} else if($uripage =="car_assignments.php") {
//--------------------------------------------------- car_assignments.php ----------------------------------------------//
?>

<html>
	<head>
		<title>Untitled Document</title>
		<style type="text/css">
<!--
body {
	background-color: transparent;
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}

.carnumber {	font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #FFF;
}
.creditnumber {	font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #3d4f6d;
}
.nametext {	font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
}
.gridposition {	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #5A75A0;
}
.icontext {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 11px;
	font-weight: bold;
	color: #5A75A0;
}
.subtitles {	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #5A75A0;
}
.title1 {	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 18px;
	font-weight: bold;
	color: #5A75A0;
}
-->
		</style>
		<script language="javascript">
		
			function assign_kart(customerid) {
			
				window.location = "_DO:cmd=load_url&c_name=car_assignments&f_name=lavukart/car_assignments.php?assignid=" + customerid + "&position=" + row_selected;
			}
		</script>
	</head>

<?php
	//require_once(dirname(__FILE__) . "/../comconnect.php");
	ini_set("display_errors","1");
	
	$eventid = 0;
	if(reqvar("eventid")) $eventid = reqvar("eventid");
	else if(sessvar_isset("eventid")) $eventid = sessvar("eventid");
	set_sessvar("eventid",$eventid);

	$lane_number = 1;
	if(reqvar("lane_number")) $lane_number = reqvar("lane_number");
	else if(sessvar_isset("lane_number")) $lane_number = sessvar("lane_number");
	set_sessvar("lane_number",$lane_number);
	
	$lane_calc = ($lane_number % 8);	
	$kart_img = "car.png";	
	switch ($lane_calc) {
		 case 1 : $kart_img = "car1.png"; break;
		 case 2 : $kart_img = "car2.png"; break;
		 case 3 : $kart_img = "car3.png"; break;
		 case 4 : $kart_img = "car4.png"; break;
		 case 5 : $kart_img = "car6.png"; break;
		 case 6 : $kart_img = "car7.png"; break;
		 case 7 : $kart_img = "car8.png"; break;
		 case 8 : $kart_img = "car9.png"; break;
	}

	$row_selected = 1;

	if (isset($_POST['assignid'])) {		
		$get_kart_id = lavu_query("SELECT * FROM `lk_karts` WHERE `lane` = '".$lane_number."' AND `locationid` = '".$_POST['loc_id']."' AND `position` = '".$_POST['position']."'");
		if (@lysql_num_rows($get_kart_id) > 0) {
			$extract_kid = lysql_fetch_array($get_kart_id);
			$reset_kart = lavu_query("UPDATE `lk_event_schedules` SET `kartid` = '' WHERE `eventid` = '".$eventid."' AND `kartid` = '".$extract_kid['id']."'");
			$assign_racer = lavu_query("UPDATE `lk_event_schedules` SET `kartid` = '".$extract_kid['id']."' WHERE `eventid` = '".$eventid."' AND `customerid` = '".$_POST['assignid']."'");
		}
		$row_selected = ($_POST['position'] + 1);
	}

	$get_last_kart = lavu_query("SELECT `position` as `position` FROM `lk_karts` WHERE `locationid` = '".$_POST['loc_id']."' AND `lane` = '".$lane_number."' ORDER BY `position` DESC LIMIT 1");
	if (@lysql_num_rows($get_last_kart) > 0) {
		$extract_lk = lysql_fetch_array($get_last_kart);
		$last_row = $extract_lk['position'];
	} else {
		$last_row = 0;
	}
	if (isset($_POST['rs'])) { $row_selected = $_POST['rs']; }
	if ($row_selected > $last_row) { $row_selected = 1; }	
?>

	<body>
		<table width="395" border="0" cellpadding="0" cellspacing="0">
			<?php
				$rowcount = 0;
				$get_karts = lavu_query("SELECT * FROM `lk_karts` WHERE `locationid` = '".$_POST['loc_id']."' AND `lane` = '".$lane_number."' ORDER BY `position` * 1 ASC");
				while ($extract_k = lysql_fetch_array($get_karts)) {
					$rowcount++;
					if($extract_k['position']=="")
					{
						lavu_query("update `lk_karts` set `position`='$rowcount' where `id`='".$extract_k['id']."'");
					}
					$get_assignment = lavu_query("SELECT `lk_customers`.`f_name` as f_name, `lk_customers`.`l_name` as l_name, `lk_event_schedules`.`customerid` as customerid FROM `lk_event_schedules` LEFT JOIN `lk_customers` ON `lk_event_schedules`.`customerid` = `lk_customers`.`id` WHERE `lk_event_schedules`.`kartid` = '".$extract_k['id']."' AND `lk_event_schedules`.`eventid` = '".$eventid."'");
					$f_name = "";
					$l_name = "";
					$use_bg = "images/tab_bg2.png";
					if ($rowcount == $row_selected) {
						$use_bg = "images/tab_bg2_lit.png";
					}
					if (@lysql_num_rows($get_assignment) > 0) {
						$extract_a = lysql_fetch_array($get_assignment);
						$f_name = $extract_a['f_name'];
						$l_name = $extract_a['l_name'];
					}
					echo "<tr>
						<td height='46' align='left' valign='top'>
							<table width='395' height='46' border='0' cellpadding='0' cellspacing='0'>
								<tr>
									<td width='30' align='center' valign='middle' class='subtitles'><span class='gridposition'>".$extract_k['position']."</span></td>
									<td id='kart_row_".$rowcount."' width='313' align='left' valign='middle' style='background: URL(".$use_bg.");' onclick='if(row_selected) document.getElementById(\"kart_row_\" + row_selected).style.background = \"URL(images/tab_bg2.png)\"; row_selected = \"".$rowcount."\"; this.style.background = \"URL(images/tab_bg2_lit.png)\";'>
										<table width='313' border='0' cellspacing='0' cellpadding='8'>
											<tr><td class='nametext'>".$f_name." ".$l_name."</td></tr>
										</table>
									</td>
									<td width='52' align='left' valign='top' background='images/".$kart_img."' >
										<table width='44' height='36' border='0' cellspacing='0' cellpadding='6'>
											<tr><td align='center' valign='bottom' class='carnumber'>".$extract_k['number']."</td></tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>";
				}
			?>
		</table>
		<script language='javascript'>
			row_selected = <?php echo $row_selected?>;
			
			<?php
				if (isset($_POST['assignid'])) {
					echo "window.location = '_DO:cmd=load_url&c_name=racers_for_this_race&f_name=lavukart/racers_for_this_race.php?rs=".$row_selected."';";
				} else {
					echo "window.location = '_DO:cmd=send_js&c_name=racers_for_this_race&js=assign_rs(".$row_selected.")';";
				}
			?>
		</script>
	</body>
</html>

<?php

} else if($uripage =="car_assignments_wrapper.php") {
//--------------------------------------------------- car_assignments_wrapper.php ----------------------------------------------//
?>

<html>
	<head>
		<title>Untitled Document</title>
		<style type="text/css">
<!--
body {
	background-color: transparent;
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}

.carnumber {	font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #FFF;
}
.creditnumber {	font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #3d4f6d;
}
.nametext {	font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
}
.gridposition {	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #5A75A0;
}
.icontext {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 11px;
	font-weight: bold;
	color: #5A75A0;
}
.subtitles {	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #5A75A0;
}
.title1 {	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 18px;
	font-weight: bold;
	color: #5A75A0;
}
-->
		</style>
	</head>

<?php
	//require_once(dirname(__FILE__) . "/../comconnect.php");
	
	$eventid = 0;
	if(reqvar("eventid")) $eventid = reqvar("eventid");
	else if(sessvar_isset("eventid")) $eventid = sessvar("eventid");
	set_sessvar("eventid",$eventid);
	
	$print_event = reqvar("print");
	if($print_event)
	{
		if(strpos($company_code,"jersey")!==false) $ploc = "p2r_new_jersey%20-%20NYJC";
		else if(strpos($company_code,"corona")!==false) $ploc = "p2r_corona%20-%20P2R%20Corona";
		else if(strpos($company_code,"DEV")!==false) $ploc = "DEVKART%20-%20Dev%20East%20Side";
		else {
			if(reqvar("loc_id")==9)
				$ploc = "pole_position_raceway%20-%20Murrieta";
			else
				$ploc = "pole_position_raceway%20-%20Oklahoma";
		}
		
		$track_query = lavu_query("SELECT `lk_tracks`.`title` as `track_title` from `lk_events` LEFT JOIN `lk_tracks` ON `lk_events`.`trackid`=`lk_tracks`.`id` WHERE `lk_events`.`id`='[1]'",$eventid);
		if(lysql_num_rows($track_query))
		{
			$track_read = lysql_fetch_assoc($track_query);
			$track_title = $track_read['track_title'];
		}
		else $track_title = "";
		
		$print_to = "printer1";
		if(strpos($track_title,"2")!==false || strpos(strtolower($track_title),"second")!==false) $print_to = "printer2";
		else if(strpos($track_title,"3")!==false) $print_to = "printer3";
		else if(strpos($track_title,"4")!==false) $print_to = "printer4";
		
		//mail("corey@poslavu.com","print event","$company_code \n $ploc \nprint to: $print_to \ntrack: $track_title","From: info@poslavu.com");
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $server_url_path . "/print/index.php?location=$ploc&print_to=".$print_to."&print=".$print_event);
			//curl_setopt($ch, CURLOPT_VERBOSE, 1);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_POST, 1);
			//curl_setopt($ch, CURLOPT_POSTFIELDS, $nvpreq);
			$httpResponse = curl_exec($ch);
	}

	$lane_number = 1;
	if(reqvar("lane_number")) $lane_number = reqvar("lane_number");
	else if(sessvar_isset("lane_number")) $lane_number = sessvar("lane_number");
	set_sessvar("lane_number",$lane_number);

	$race_title = "";
	$trackid = "";

	$get_race_info = lavu_query("SELECT `lk_event_types`.`title` as `title`, `lk_events`.`trackid` as `trackid` FROM `lk_events` LEFT JOIN `lk_event_types` ON `lk_events`.`type` = `lk_event_types`.`id` WHERE `lk_events`.`id` = '".$eventid."'");
	if (@lysql_num_rows($get_race_info) > 0) {
		$extract = lysql_fetch_array($get_race_info);
		$race_title = $extract['title'];
		$trackid = $extract['trackid'];
	}
	
	$startrace = reqvar("startrace");
	if($startrace && $startrace==$eventid)
	{
		$start_success = lavu_query("update `lk_events` set `ms_start`='".date("Y-m-d H:i:s")."' where `id`='[1]'",$startrace);
	}
	$endrace = reqvar("endrace");
	if($endrace)
	{
		lavu_query("update `lk_events` set `ms_end`='".date("Y-m-d H:i:s")."' where `id`='[1]'",$endrace);
	}
	$continue_race = reqvar("continue_race");
	if($continue_race)
	{
		function time_to_ts($time)
		{
			$parts = explode(" ",$time);
			$date_parts = explode("-",$parts[0]);
			$time_parts = explode(":",$parts[1]);
			$ts = mktime($time_parts[0],$time_parts[1],$time_parts[2],$date_parts[1],$date_parts[2],$date_parts[0]);
			return $ts;
		}
		
		$e_query = lavu_query("SELECT * from `lk_events` where `id`='[1]'",$continue_race);
		if(lysql_num_rows($e_query))
		{
			$e_read = lysql_fetch_assoc($e_query);
			
			$ts_now = time();
			$ts_end = time_to_ts($e_read['ms_end']);
			$ts_span = $ts_now - $ts_end;
			$pause_data = $ts_now . "-" . $ts_end . "=" . $ts_span;
			lavu_query("update `lk_events` set `ms_end`='', `pauses`=CONCAT(`pauses`,'$pause_data ') where `id`='[1]'",$continue_race);
		}
	}
	
	$race_active = false;
	$active_query = lavu_query("select * from `lk_events` where `ms_start`!='' and (`ms_end`='' or `ms_end` IS NULL) and `trackid`='[1]'",$trackid);
	if(lysql_num_rows($active_query))
	{
		$active_read = lysql_fetch_assoc($active_query);
		$active_raceid = $active_read['id'];
		$race_active = true;
	}
	
	$event_exists = false;
	$event_active = false;
	$event_query = lavu_query("select * from `lk_events` where `id`='[1]'",$eventid);
	if(lysql_num_rows($event_query))
	{
		$event_exists = true;
		$event_active = true;
		$event_read = lysql_fetch_assoc($event_query);
		if($event_read['ms_end']!="")
			$event_active = false;
	}
?>

	<body>
		<table width="474" height="598" border="0" cellpadding="0" cellspacing="0" >
			<tr>
				<td height="40" colspan="2">
					<table width="474" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="79">&nbsp;</td>
							<td width="343" align="center" valign="middle" class="title1"><?php if($eventid > 0) echo $race_title." - Lane ".$lane_number; ?></td>
							<td width="52">&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td width="79" height="536" align="left" valign="top">
					<table width="79" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="11">&nbsp;</td>
							<td height="26">&nbsp;</td>
						</tr>
						<tr>
							<td width="11">&nbsp;</td>
							<td>
								<table width="68" border="0" cellpadding="0" cellspacing="0">
									<tr><td height="32" align="center" valign="middle" class="subtitles"><span class="icontext">RACES</span></td></tr>
									<tr><td align="center" valign="middle"><a onclick='location = "_DO:cmd=create_overlay&f_name=lavukart/events_overlay_wrapper.php;lavukart/events.php&c_name=events_overlay_wrapper;events&dims=263,90,494,598;348,130,420,540&scroll=0;1?test1=1&test2=2";'><img src="images/btn_races.png" width="68" height="68" /></a></td></tr>
								</table>
							</td>
						</tr>
						<?php if($event_exists) { ?>
						<tr>
							<td width="11">&nbsp;</td>
							<td height="110" align="left" valign="bottom">
								<table width="68" border="0" cellpadding="0" cellspacing="0">
									<tr><td height="32" align="center" valign="middle" class="subtitles"><a onclick='location = "_DO:cmd=show_pit_setup";'><img src="images/btn_grid.png" width="68" height="68" /></a></td></tr>
									<!--$ <tr><td height="32" align="center" valign="middle" class="subtitles"><a onclick='location = "_DO:cmd=create_overlay&f_name=lavukart/pit_grid_overlay_wrapper.php;lavukart/pit_grid.php&c_name=pit_grid_overlay_wrapper;pit_grid&dims=275,90,474,598;302,130,420,540&scroll=0;1?test1=1&test2=2";'><img src="images/btn_grid.png" width="68" height="68" /></a></td></tr> $-->
									<tr><td height="32" align="center" valign="middle"><span class="icontext">GRID</span></span></td></tr>
								</table>
							</td>
						</tr>
						<?php } ?>
						<?php if($event_active) {
									if($race_active) { 
						?>
						<tr>
							<td width="11">&nbsp;</td>
							<td height="110" align="left" valign="bottom">
								<table width="68" border="0" cellpadding="0" cellspacing="0">
									<tr><td height="32" align="center" valign="middle" class="subtitles">
                                    <a onclick='location = "_DO:cmd=load_url&c_name=car_assignments_wrapper&f_name=lavukart/car_assignments_wrapper.php?endrace=<?php echo $active_raceid?>";'><img src="images/btn_endrace.png" width="68" height="68" /></a></td></tr>
									<!--$ <tr><td height="32" align="center" valign="middle" class="subtitles"><a onclick='location = "_DO:cmd=create_overlay&f_name=lavukart/pit_grid_overlay_wrapper.php;lavukart/pit_grid.php&c_name=pit_grid_overlay_wrapper;pit_grid&dims=275,90,474,598;302,130,420,540&scroll=0;1?test1=1&test2=2";'><img src="images/btn_grid.png" width="68" height="68" /></a></td></tr> $-->
									<tr><td height="32" align="center" valign="middle"><span class="icontext">STOP</span></span></td></tr>
								</table>
							</td>
						</tr>
						<?php 		} else { ?>
						<tr>
							<td width="11">&nbsp;</td>
							<td height="110" align="left" valign="bottom">
								<table width="68" border="0" cellpadding="0" cellspacing="0">
									<tr><td height="32" align="center" valign="middle" class="subtitles"><a onclick='location = "_DO:cmd=load_url&c_name=car_assignments_wrapper&f_name=lavukart/car_assignments_wrapper.php?startrace=<?php echo $eventid?>";'><img src="images/btn_startrace.png" width="68" height="68" /></a></td></tr>
									<!--$ <tr><td height="32" align="center" valign="middle" class="subtitles"><a onclick='location = "_DO:cmd=create_overlay&f_name=lavukart/pit_grid_overlay_wrapper.php;lavukart/pit_grid.php&c_name=pit_grid_overlay_wrapper;pit_grid&dims=275,90,474,598;302,130,420,540&scroll=0;1?test1=1&test2=2";'><img src="images/btn_grid.png" width="68" height="68" /></a></td></tr> $-->
									<tr><td height="32" align="center" valign="middle"><span class="icontext">START</span></span></td></tr>
								</table>
							</td>
						</tr>
						<?php } } else if(!$race_active && $event_exists) { ?>
						<tr>
							<td width="11">&nbsp;</td>
							<td height="110" align="left" valign="bottom">
								<table width="68" border="0" cellpadding="0" cellspacing="0">
									<tr><td height="32" align="center" valign="middle" class="subtitles">
										<a onclick='location = "_DO:cmd=load_url&c_name=car_assignments_wrapper&f_name=lavukart/car_assignments_wrapper.php?continue_race=<?php echo $eventid?>";'><img src="images/btn_startrace.png" width="68" height="68" /></a>
								</td></tr>
									<!--$ <tr><td height="32" align="center" valign="middle" class="subtitles"><a onclick='location = "_DO:cmd=create_overlay&f_name=lavukart/pit_grid_overlay_wrapper.php;lavukart/pit_grid.php&c_name=pit_grid_overlay_wrapper;pit_grid&dims=275,90,474,598;302,130,420,540&scroll=0;1?test1=1&test2=2";'><img src="images/btn_grid.png" width="68" height="68" /></a></td></tr> $-->
									<tr><td height="32" align="center" valign="middle"><span class="icontext">CONTINUE</span></span></td></tr>
								</table>
							</td>
						</tr>
						<?php } ?>
						<?php if($event_exists) { ?>
						<tr>
							<td width="11">&nbsp;</td>
							<td height="110" align="left" valign="bottom">
								<table width="68" border="0" cellpadding="0" cellspacing="0">
									<tr><td height="32" align="center" valign="middle" class="subtitles"><a onclick='location = "_DO:cmd=load_url&c_name=car_assignments_wrapper&f_name=lavukart/car_assignments_wrapper.php?print=<?php echo $eventid?>";'><img src="images/btn_printer.png" width="68" height="68" /></a></td></tr>
									<!--$ <tr><td height="32" align="center" valign="middle" class="subtitles"><a onclick='location = "_DO:cmd=create_overlay&f_name=lavukart/pit_grid_overlay_wrapper.php;lavukart/pit_grid.php&c_name=pit_grid_overlay_wrapper;pit_grid&dims=275,90,474,598;302,130,420,540&scroll=0;1?test1=1&test2=2";'><img src="images/btn_grid.png" width="68" height="68" /></a></td></tr> $-->
									<tr><td height="32" align="center" valign="middle"><span class="icontext">PRINT RESULTS</span></span></td></tr>
								</table>
							</td>
						</tr>
						<?php } ?>
					</table>
				</td>
				<td align="left" valign="top">&nbsp;</td>
			</tr>
		</table>
	</body>
	<?php if($print_event) { ?>
	<script language="javascript">
		alert("Printing...");
	</script>
	<?php } ?>
</html>
<?php

} else if($uripage =="combined_load.php") {
//--------------------------------------------------- combined_load.php ----------------------------------------------//
?>

<?php
	//require_once(dirname(__FILE__) . "/../comconnect.php");

	$combined_html = "";
	
	$post_vars = "";
	$post_keys = array_keys($_POST);
	foreach ($post_keys as $key) {
		if ($post_vars != "") {
			$post_vars .= "&";
		}
		if ($key == "cc") {
			$post_vars .= "cc=".$company_code_full;
		} else {
			$post_vars .= $key."=".$_POST[$key];
		}
	}

	$get_component_layout = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`component_layouts` WHERE `id` = '".$_POST['layout_id']."'");
	if (@lysql_num_rows($get_component_layout) > 0) {
		$layout_info = lysql_fetch_assoc($get_component_layout);
		if (trim($layout_info['c_ids']) != "") {
			$combined_html = $layout_info['c_ids'];
			$component_ids = explode(",", trim($layout_info['c_ids']));
			$load_good = true;
			foreach ($component_ids as $c_id) {
				$get_components = mlavu_query("SELECT * FROM `poslavu_MAIN_db`.`components` WHERE `id` = '".$c_id."'");
				if (@lysql_num_rows($get_components) > 0) {
					$component_info = lysql_fetch_assoc($get_components);
					$combined_html .= "<******>";

					$ch = curl_init("http://admin.poslavu.com/components/".$component_info['filename']);
					curl_setopt($ch, CURLOPT_HEADER, 0);
					curl_setopt($ch, CURLOPT_POST, 1);
					curl_setopt($ch, CURLOPT_POSTFIELDS, $post_vars."&comp_name=".$component_info['name']);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					$combined_html .= curl_exec($ch);
					curl_close($ch);
	
				} else {
					$load_good = false;
					break;
				}
			}
			if ($load_good == true) {
				echo $combined_html;
			} else {
				echo "Error - Missing Component";
			}
			
		} else {
			echo "Error - Missing Component IDs";
		}
		
	} else {
		echo "Error - Unknown Component Layout";
	}
?>
<?php

} else if($uripage =="customer_history.php") {
//--------------------------------------------------- customer_history.php ----------------------------------------------//
?>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Untitled Document</title>

		<style type="text/css">
<!--

body {
	background-color: transparent;
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}

.style4 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #8896AD;
}
.style6 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 24px;
	font-weight: bold;
	color: #8896AD;
}
.style8 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 15px;
	font-weight: bold;
	color: #07142a;
}
.creditnumber {
	font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #3d4f6d;
}
.nametext {
	font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
}
.subtitles {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 16px;
	font-weight: bold;
	color: #5A75A0;
}
.subtitles_dark {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 16px;
	font-weight: bold;
	color: #2f3e55;
}
.info_text1 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 16px;
	color: #233756;
}
.info_text2 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 15px;
	color: #5A75A0;
}
.info_text3 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 15px;
	color: #233756;
}
-->
		</style>
	</head>

<?php 
	//require_once(dirname(__FILE__) . "/../comconnect.php"); 
	
	function reformat_date($date,$type) {
		$datetime_array = explode(" ", $date);
		$date_array = explode("-", $datetime_array[0]);
		$time_array = explode(":", $datetime_array[1]);
	
		if ($type == 1) {
			return date("g:i:s A \o\\n F jS, Y", mktime((int)$time_array[0], (int)$time_array[1], (int)$time_array[2], (int)$date_array[1], (int)$date_array[2], (int)$date_array[0]));
		} else if ($type == 2) {
			return date("Y-m-d g:i A", mktime((int)$time_array[0], (int)$time_array[1], (int)$time_array[2], (int)$date_array[1], (int)$date_array[2], (int)$date_array[0]));
		}
	}
	
	function get_field_info($table, $get_field, $known_field, $known_value) {	
		$get_info = lavu_query("SELECT `[1]` FROM `[2]` WHERE `[3]` = '[4]'", $get_field, $table, $known_field, $known_value);
		if (lysql_num_rows($get_info) > 0) {
			$extract_info = lysql_fetch_array($get_info);
			return $extract_info[$get_field];
		} else {
			return "";
		}
	}

	$creation_date = "";
	$confirmed_by = "Unconfirmed";
	$get_customer_info = lavu_query("SELECT * FROM `lk_customers` WHERE `id` = '[1]'", $_POST['racer_id']);
	if (lysql_num_rows($get_customer_info) > 0) {
		$extract_c = lysql_fetch_array($get_customer_info);
		$creation_date = reformat_date($extract_c['date_created'],1);
		if ($extract_c['confirmed_by_attendant'] == "") {
			$confirmed_by = "Speedsheet Transfer";
		} else if ($extract_c['confirmed_by_attendant'] != "0") {
			$confirmed_by = get_field_info("users", "f_name", "id", $extract_c['confirmed_by_attendant'])." ".get_field_info("users", "l_name", "id", $extract_c['confirmed_by_attendant']);
		}
	}
	
	$pos_history = "";
	$total_credits_purchased = 0;
	$check_pos_history = lavu_query("SELECT `locations`.`title` as pos_location, `lk_action_log`.`action` as action, `lk_action_log`.`time` as time, `lk_action_log`.`order_id` as order_id, `lk_action_log`.`credits` as credits, `lk_action_log`.`user_id` as user_id FROM `lk_action_log` LEFT JOIN `locations` ON `locations`.`id` = `lk_action_log`.`loc_id` WHERE `customer_id` = '[1]' ORDER BY `lk_action_log`.`time` DESC", $_POST['racer_id']);
	if (lysql_num_rows($check_pos_history) > 0) {
		while ($extract_p = lysql_fetch_array($check_pos_history)) {
			$display_order_id = "";
			if ($extract_p['order_id'] != "0") {
				$display_order_id = " - Order #".$extract_p['order_id'];
			}
			$attendant = "";
			if (($extract_p['user_id'] != "") && ($extract_p['user_id'] != "0")) {
				$attendant = "<td align='right' width='100'><span class='info_text2'>&nbsp;&nbsp;Attendant:</span></td><td align='left'><span class='info_text3'>".get_field_info("users", "f_name", "id", $extract_p['user_id'])." ".get_field_info("users", "l_name", "id", $extract_p['user_id'])."</span></td>";
			}
			
			$pos_history .= "<tr>
				<td width='532' height='60' align='center' style='background:URL(images/tab_bg_race.png)'>
					<table width='512' cellspacing='0' cellpadding='2'>
						<tr><td align='center' colspan='5'><span class='info_text2'>".reformat_date($extract_p['time'], 2)." - ".$extract_p['pos_location'].$display_order_id."</span></td></tr>
						<tr>
							<td align='center'>
								<table cellspacing='0' cellpadding='2'>
									<tr><td align='left'><span class='info_text3'>".$extract_p['action']."</span></td>".$attendant."</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>";
			$total_credits_purchased = ($total_credits_purchased + $extract_p['credits']);
		}
	} else {
		$pos_history = "<tr><td width='532' height='60' align='center' style='background:URL(images/tab_bg_race.png)'><span class=\"info_text1\">This person has no POS transactions on record since the SpeedSheet transfer.</span></td></tr>";
	}

	$race_history = "";
	$total_credits_used = 0;
	$check_race_history = lavu_query("SELECT `lk_events`.`ts` as event_ts, `locations`.`title` as event_location, `lk_event_types`.`title` as event_title, `lk_karts`.`number` as kart_number FROM `lk_event_schedules` LEFT JOIN `lk_events` ON `lk_events`.`id` = `lk_event_schedules`.`eventid` LEFT JOIN `lk_karts` ON `lk_karts`.`id` = `lk_event_schedules`.`kartid` LEFT JOIN `lk_event_types` ON `lk_event_types`.`id` = `lk_events`.`type` LEFT JOIN `locations` ON `locations`.`id` = `lk_events`.`locationid` WHERE `customerid` = '[1]' ORDER BY `lk_events`.`ts` DESC", $_POST['racer_id']);
	if (lysql_num_rows($check_race_history) > 0) {
		while ($extract_r = lysql_fetch_array($check_race_history)) {
			$race_history .= "<tr>
				<td width='532' height='60' align='center' style='background:URL(images/tab_bg_race.png)'>
					<table width='512' cellspacing='0' cellpadding='2'>
						<tr><td align='center' colspan='6'><span class='info_text2'>".date("Y-m-d g:i A", $extract_r['event_ts'])." - ".$extract_r['event_location']." - ".$extract_r['event_title']."</span></td></tr>
						<tr><td align='right' width='82'><span class='info_text2'>Place:</span></td><td align='left' width='90'><span class='info_text3'>???</span></td><td align='right' width='80'><span class='info_text2'>&nbsp;&nbsp;Time:</span></td><td align='left' width='90'><span class='info_text3'>??m ??s</span></td><td align='right' width='80'><span class='info_text2'>&nbsp;&nbsp;Kart:</span></td><td align='left' width='90'><span class='info_text3'>".$extract_r['kart_number']."</span></td></tr>
					</table>
				</td>
			</tr>";
			$total_credits_used++;
		}
	} else {
		$race_history = "<tr><td width='532' height='60' align='center' style='background:URL(images/tab_bg_race.png)'><span class=\"info_text1\">This person has not participated in any races since the SpeedSheet transfer.</span></td></tr>";
	}
?>
	<body>
		<table width="542" cellspacing="0" cellpadding="0">
			<tr>
				<td align="center">
					<table width="532" border="0" cellspacing="0" cellpadding="0">
						<tr><td>&nbsp;</td></tr>
						<?php
							if ($extract_c['membership'] != "") {
								$today = date("Y-m-d", time());
								if ($today > $extract_c['membership_expiration']) {
									$expires = "Expired ".$extract_c['membership_expiration'];
								} else if ($today == $extract_c['membership_expiration']) {
									$expires = "Today";
								} else {
									$expires = $extract_c['membership_expiration'];
								}
								echo "<tr>
									<td align='center'>
										<table cellspacing='3' cellpadding='3'>
											<tr>
												<td align='left' valign='middle'><span class='info_text3'><b>".$extract_c['membership']." - </b></span></td>
												<td align='right' valign='middle'><span class='info_text2'><b>Expires:</b></span></td>
												<td align='left' valign='middle'><span class='info_text3'><b>".$expires."</b></span></td>
											</tr>
										</table>
									</td>
								</tr>";
							}
						?>
						<tr>
							<td align="center">
								<table cellspacing="3" cellpadding="3">
									<tr>
										<td align="right" valign="middle"><span class="subtitles">Account Created:</span></td>
										<td align="left" valign="middle"><span class="info_text1"><?php echo $creation_date; ?></span></td>
									</tr>
								</table>
								<table cellspacing="3" cellpadding="3">
									<tr>
										<td align="right" valign="middle"><span class="subtitles">Confirmed by:</span></td>
										<td align="left" valign="middle"><span class="info_text1"><?php echo $confirmed_by; ?></span></td>
									</tr>
								</table>
							</td>
						</tr>
						<tr><td>&nbsp;</td></td>
						<tr>
							<td align="center">
								<table cellspacing="3" cellpadding="3">
									<tr>
										<td align='center' colspan='4'>
											<table cellspacing='3' cellpadding='3'>
												<tr>
													<td align="right" valign="middle"><span class="info_text2"><b>Visits:</b></span></td>
													<td align="left" valign="middle"><span class="info_text3"><?php echo $extract_c['total_visits']; ?></span></td>
													<td align="right" valign="middle"><span class="info_text2"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Races Raced:</b></span></td>
													<td align="left" valign="middle"><span class="info_text3"><?php echo $extract_c['total_races']; ?></span></td>
												</tr>
											</table>
										</td>
									</tr>
									<tr>
										<td align="right" valign="middle"><span class="info_text2"><b>Purchased Credits:</b></span></td>
										<td align="left" valign="middle"><span class="info_text3"><?php echo $total_credits_purchased; ?></span></td>
										<td align="right" valign="middle"><span class="info_text2"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Used Credits:</b></span></td>
										<td align="left" valign="middle"><span class="info_text3"><?php echo $total_credits_used; ?></span></td>
									</tr>
									<tr>
										<td align='center' colspan='4'>
											<table cellspacing='3' cellpadding='3'>
												<tr>
													<td align="right" valign="middle"><span class="info_text2"><b>Remaining Credits:</b></span></td>
													<td align="left" valign="middle"><span class="info_text3"><?php echo $extract_c['credits']; ?></span></td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr><td>&nbsp;</td></tr>
						<tr id="POS_row">
							<td align="center">
								<table width="532" cellspacing="0" cellpadding="0">
									<tr>
										<td align="center">
											<table width="532" cellspacing="0" cellpadding="0">
												<tr>
													<td align="left"><span class="subtitles_dark">&nbsp;&nbsp;POS Transactions</span></td>
													<td align="right" onClick="document.getElementById('POS_row').style.display = 'none'; document.getElementById('races_row').style.display = 'inline';"><span class="subtitles">Races&nbsp;&nbsp;</span></td>
												</tr>
											</table>
										</td>
									</tr>
									<tr><td><hr color="#5A75A0"></td></tr>
									<tr>
										<td align="center">
											<table cellspacing="0" cellpadding="0">
												<?php echo $pos_history; ?>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr id="races_row" style="display:none;">
							<td align="center">
								<table width="532" cellspacing="0" cellpadding="0">
									<tr>
										<td align="center">
											<table width="532" cellspacing="0" cellpadding="0">
												<tr>
													<td align="left" onClick="document.getElementById('races_row').style.display = 'none'; document.getElementById('POS_row').style.display = 'inline';"><span class="subtitles">&nbsp;&nbsp;POS Transactions</span></td>
													<td align="right"><span class="subtitles_dark">Races&nbsp;&nbsp;</span></td>
												</tr>
											</table>
										</td>
									</tr>
									<tr><td><hr color="#5A75A0"></td></tr>
									<tr>
										<td align="center">
											<table cellspacing="0" cellpadding="0">
												<?php echo $race_history; ?>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr><td>&nbsp;</td></tr>
					</table>
				</td>
			</tr>
	  </table>
	</body>
</html>

<?php

} else if($uripage =="customer_history_overlay_wrapper.php") {
//--------------------------------------------------- customer_history_overlay_wrapper.php ----------------------------------------------//
?>

<?php
	//require_once(dirname(__FILE__) . "/../comconnect.php"); 
	$tab_name = reqvar("tab_name");
?>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Untitled Document</title>

		<style type="text/css">
<!--
body {
	background-color: transparent;
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}

.style4 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #8896AD;
}
.style6 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 24px;
	font-weight: bold;
	color: #8896AD;
}
.style8 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 15px;
	font-weight: bold;
	color: #07142a;
}

.racer_name {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 20px;
	color: #d4dae4;
}
-->
		</style>
	</head>
	
<?php
	$racer_name = "";
	$get_customer_info = lavu_query("SELECT * FROM `lk_customers` WHERE `id` = '[1]'", $_POST['racer_id']);
	if (lysql_num_rows($get_customer_info) > 0) {
		$extract = lysql_fetch_array($get_customer_info);
		$racer_name = $extract['f_name']." ".$extract['l_name'];
	}
?>

	<body>
		<table width="574" height="549" border="0" cellpadding="0" cellspacing="0" style="background:URL(images/win_overlay_no_btns_wide.png); background-repeat:no-repeat; background-position:top center;">
			<tr>
				<td width="26" height="53">&nbsp;</td>
				<td width="522" height="53">
					<table width="522" border="0" cellpadding="6" cellspacing="0">
						<tr>
							<td width="63" height="47" align="center" style="background:URL(images/btn_soft.png); background-repeat:no-repeat; background-position:center;" onclick='location = "_DO:cmd=close_overlay";'><span class="style6"> X </span></td>
              <td width="459" height="47" align="center"><span class="racer_name"><b><?php echo $racer_name; ?></b></span></td>
						</tr>
					</table>
				</td>
				<td width="26" height="53">&nbsp;</td>
			</tr>
			<tr><td width="574" height="496" colspan="3">&nbsp;</td></tr>
		</table>
	</body>
</html>

<?php

} else if($uripage =="customer_info.php") {
//--------------------------------------------------- customer_info.php ----------------------------------------------//
?>

<html>
	<head>
		<title>Untitled Document</title>
		<style type="text/css">
<!--
body {
	background-color: transparent;
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}

.creditnumber {
	font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #3d4f6d;
}
.gridposition {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #5A75A0;
}
.icontext {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 11px;
	font-weight: bold;
	color: #5A75A0;
}
.nametext {
	font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
}
.subtitles {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #5A75A0;
}
.bg_signature_top {
	background-image:URL(images/bg_signature_top.png);
	background-repeat:no-repeat;
	width:400px;
	height:9px;
}
.bg_signature_mid {
	background-image:URL(images/bg_signature_mid.png);
	background-repeat:repeat-y;
	width:400px;
}
.bg_signature_bottom {
	background-image:URL(images/bg_signature_bottom.png);
	background-repeat:no-repeat;
	width:400px;
	height:9px;
}
-->
		</style>
	</head>

<?php
	//require_once(dirname(__FILE__) . "/../comconnect.php");
	
	$racer_id = 0;
	if(reqvar("racer_id")) $racer_id = reqvar("racer_id");
	else if(sessvar_isset("showing_racer_id")) $racer_id = sessvar("showing_racer_id");
	set_sessvar("showing_racer_id",$racer_id);

	/*$server_id = 0;
	if(reqvar("server_id")) $server_id = reqvar("server_id");
	else if(sessvar_isset("server_id")) $server_id = sessvar("server_id");
	set_sessvar("server_id",$server_id);

	$loc_id = 0;
	if(reqvar("loc_id")) $loc_id = reqvar("loc_id");
	else if(sessvar_isset("loc_id")) $loc_id = sessvar("loc_id");
	set_sessvar("loc_id", $loc_id);

	$dataname = "";
	if(reqvar("dn")) $dataname = reqvar("dn");
	else if(sessvar_isset("dataname")) $dataname = sessvar("dataname");
	set_sessvar("dataname", $dataname);*/
	
	$info_form = array();
	$info_form[] = array("First Name","f_name");
	$info_form[] = array("Last Name","l_name");
	$info_form[] = array("Racer Name","racer_name");
	$info_form[] = array("Membership","membership");
	$info_form[] = array("Expiration","membership_expiration");
	$info_form[] = array("Date of Birth","birth_date");
	$info_form[] = array("Email","email");
	$info_form[] = array("Postal Code","postal_code");
	$info_form[] = array("State","state");
	$info_form[] = array("Country","country");
	$info_form[] = array("License","license_exp");
	$info_form[] = array("Source","source");
	$info_form[] = array("Notes","notes");
	if($access_level * 1 >= 2)
		$info_form[] = array("Credits","credits");
	
	if (isset($_POST['save'])) {
	  $vals = array();
		$save_info = "SET ";
		for ($c = 0; $c < count($info_form); $c++) {
		  $iname = $info_form[$c][1];
		  $vals[$iname] = $_POST[$iname];
			$save_info .= "`$iname` = '[$iname]'";
			if ($c < (count($info_form) - 1)) {
				$save_info .= ", ";
			}
		}
		
		$update_customer = lavu_query("UPDATE `lk_customers` ".$save_info." WHERE `id` = '".$racer_id."'", $vals);
	}

?>
	<body>
		<table width="460" border="0" cellspacing="0" cellpadding="0">
			<?php
				$get_customer_info = lavu_query("SELECT * FROM `lk_customers` WHERE `id` = '".$racer_id."'");
				if (lysql_num_rows($get_customer_info) > 0) {
					$extract = lysql_fetch_array($get_customer_info);
					echo "<form name='info_form' action='customer_info.php' method='POST'>";
					foreach ($info_form as $info) {
						if($info[1]=="notes")
						{
							$valign = "top";
							$element_height = 92;
							$element_bg = "images/tab_bg2_tall.png";
							$form_element = "<textarea name='".$info[1]."' class='nametext' style='border:none; background: URL(win_overlay.png); width: 290px' rows='4' onchange='document.getElementById(\"update_btn\").style.display = \"inline\";'>" . $extract[$info[1]] . "</textarea>";
						}
						else
						{
							$valign = "middle";
							$element_height = 46;
							$element_bg = "images/tab_bg2.png";
							$form_element = "<input type='text' name='".$info[1]."' class='nametext' style='border:none; background: URL(win_overlay.png);' size='30' value=\"".str_replace("\"","&quot;",$extract[$info[1]])."\" onchange='document.getElementById(\"update_btn\").style.display = \"inline\";'>";
						}
					
						echo "<tr>
							<td width='147' height='$element_height' valign='$valign'>
								<table width='147' border='0' cellspacing='0' cellpadding='8'>
									<tr><td align='right' class='subtitles'>".$info[0]."</td></tr>
								</table>
							</td>
							<td width='313' background='$element_bg'>
								<table width='313' border='0' cellspacing='0' cellpadding='8'>
									<tr><td class='nametext'>";
									echo $form_element;
									echo "</td></tr>
								</table>
							</td>
						</tr>";
					}
					echo "<tr><td>&nbsp;</td></tr>
						<tr><td align='center' valign='middle' colspan='2'><img src='images/btn_history.png' width='139px' onclick='window.location = \"_DO:cmd=create_overlay&f_name=lavukart/customer_history_overlay_wrapper.php;lavukart/customer_history.php&c_name=customer_history_overlay_wrapper;customer_history&dims=250,100,574,549;266,158,542,480&scroll=0;1?racer_id=".$racer_id."\";';> &nbsp;&nbsp;&nbsp;&nbsp; <img id='update_btn' src='images/btn_update.png' width='120px' onclick='document.info_form.submit();' style='display:none'></td></tr>
						<input type='hidden' name='save' value='1'>
						<input type='hidden' name='racer_id' value='".$_POST['racer_id']."'>
						<tr><td>&nbsp;</td></tr>
					</form>";
					
					if (reqvar("confirm_signature")) {
	
						$update_customer = lavu_query("UPDATE `lk_customers` SET `confirmed_by_attendant` = '".$server_id."' WHERE `id` = '".$racer_id."'");
						$signature = "Signature";
						if ($extract['minor_or_adult_at_signup'] == "minor") {
							$signature = "Signatures";
						}
						$lk_log_this = lysql_query("INSERT INTO `lk_action_log` (`action`, `customer_id`, `order_id`, `time`, `user_id`, `credits`, `loc_id`) VALUES ('".$signature." Confirmed', '".$racer_id."', '0', now(), '".$server_id."', '0', '".$loc_id."')");
					}
					
					if (is_file("companies/".$dataname."/signatures/signature_".$racer_id.".jpg")) {
						$display_signature = "<img src='companies/".$dataname."/signatures/signature_".$racer_id.".jpg' width='380'>";
					} else {
						$display_signature = "<span class='subtitles'>No signature image on record.</span>";
					}
					echo "<tr>
						<td align='center' colspan='2'>
							<table cellspacing='0' cellpadding='0'>
								<tr>
									<td width='50'>&nbsp;</td>
									<td width='400'>
										<table width='400' cellspacing='0' cellpadding='0'>
											<tr><td class='bg_signature_top'></td></tr>
											<tr><td class='bg_signature_mid' align='center'>".$display_signature."</td></tr>
											<tr><td class='bg_signature_bottom'></td></tr>
										</table>
									</td>									
								</tr>";

					if (($extract['confirmed_by_attendant'] == "") && (!reqvar("confirm_signature"))) {
						echo "<tr><td>&nbsp;</td></tr>
						<tr><td width='50'>&nbsp;</td><td align='center' width='400'><img src='images/btn_confirm_signature.png' width='342' onclick='window.location = \"customer_info.php?confirm_signature=1\";'></td></tr>";
					}
					echo "<tr><td>&nbsp;</td></tr>
					<tr><td align='center' colspan='2'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a onclick='window.location = \"_DO:cmd=update_signature&refresh_comp=customer_info&customer_id=".$racer_id."&minor_or_adult=".$extract['minor_or_adult_at_signup']."\";'><img src='images/btn_update_signature.png' width='331px'></a></td></tr>";
								
					echo "</table>
						</td>
					</tr>";
					
				} else {
					echo "<tr><td align='center'><font style='font-size:24px; color:#999999; font-family:Verdana,Arial'><br>No record selected...</font></td></tr>";
				}
			?>
			<tr><td>&nbsp;</td></tr>
		</table>
		<?php
			if (isset($_POST['save'])) {
				echo "<script language='javascript'>location = '_DO:cmd=alert&title=Update Successful';</script>";
			}
			if (reqvar("confirm_signature")) {
				if ($extract['minor_or_adult_at_signup'] == "minor") {
					$signature = "Signatures";
				} else {
					$signature = "Signature";
				}
				echo "<script language='javascript'>location = '_DO:cmd=alert&title=".$signature." Confirmed';</script>";
			}
		?>
</body>
</html>

<?php

} else if($uripage =="customer_info_wrapper.php") {
//--------------------------------------------------- customer_info_wrapper.php ----------------------------------------------//
?>

<html>
	<head>
		<title>Untitled Document</title>
		<style type="text/css">
<!--
body {
	background-color: transparent;
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}

.creditnumber {font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #3d4f6d;
}
.gridposition {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #5A75A0;
}
.icontext {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 11px;
	font-weight: bold;
	color: #5A75A0;
}
.nametext {font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
}
.subtitles {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #5A75A0;
}
-->
		</style>
	</head>

	<body>
		<table width="474" height="598" border="0" cellpadding="0" cellspacing="0" >
			<tr>
				<td width="9">&nbsp;</td>
				<td align="center" valign="top">
					<table width="451" height="598" border="0" cellspacing="0" cellpadding="0">
						<tr><td height="23"><img src="images/line_top.png" width="451" height="23"></td></tr>
						<tr><td height="552" align="left" valign="top">&nbsp;</td></tr>
						<tr><td height="23"><img src="images/line_bot.png" width="451" height="23"></td></tr>
					</table>
				</td>
      </tr>
    </table>
	</body>
</html>

<?php

} else if($uripage =="customer_search.php") {
//--------------------------------------------------- customer_search.php ----------------------------------------------//
?>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Untitled Document</title>

		<style type="text/css">
<!--

body {
	background-color: transparent;
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}

.style4 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #8896AD;
}
.style6 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 24px;
	font-weight: bold;
	color: #8896AD;
}
.style8 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 15px;
	font-weight: bold;
	color: #07142a;
}
.creditnumber {
	font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #3d4f6d;
}
.nametext {
	font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
}
.nametext_grey {
	font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #777777;
}
.subtitles {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #5A75A0;
}
-->
		</style>
	</head>

<?php 
	//require_once(dirname(__FILE__) . "/../comconnect.php"); 
	$mode = reqvar("mode","recent_signup");
	$cm = reqvar("cm");
	$tab_name = reqvar("tab_name");
	$where_cond = "";
	$and_cond = "";
	$message = "";
	
	if(reqvar("loc_id")) $loc_id = reqvar("loc_id");
	else if(sessvar_isset("loc_id")) $loc_id = sessvar("loc_id");
	set_sessvar("loc_id", $loc_id);
	
	if($cm)
	{
		$cmd = "_DO:cmd=".reqvar("cmd")."&racer_name=[fullname]&racer_id=[id]";
	}
	else
	{
		if ($tab_name == "Customers") {
			$cmd = "_DO:cmd=load_url&c_name=customer_info&f_name=lavukart/customer_info.php?racer_id=[id]";
		} else {
			$cmd = "_DO:cmd=close_overlay&c_name=customers&f_name=lavukart/customers.php?mode=assign&racer_name=[fullname]&racer_id=[id]";
			if($tab_name=="Schedule")
			{
				$eventid = sessvar("eventid");
				if($eventid=="remove") $eventid = false;
				
				if($eventid)
				{
					$where_cond = " where `credits` > 0";
					$and_cond = " and `credits` > 0";
						
					$use_pool = reqvar("pool","qualifier");
				}
				else
				{
					$message = "No Event Selected";
				}
			}
		}
	}

	$rlist = array();
	if($mode=="recent_signup")
	{
		if($use_pool=="qualifier")
		{			
			$event_query = lavu_query("SELECT * from `lk_events` LEFT JOIN `lk_event_types` ON `lk_events`.`type`=`lk_event_types`.`id` where `lk_events`.`id`='[1]'",$eventid);
			if(lysql_num_rows($event_query))
			{
				$event_read = lysql_fetch_assoc($event_query);
				$event_ts = $event_read['ts'];
				$debug = "";
				
				if($event_read['title']=="Qualifier") $look_for_q = "Practice"; else $look_for_q = "Qualifier";
				$pool_title = $look_for_q;
				
				$customer_ids_used = array();
				$rlist = array();
				$qual_query = lavu_query("select `lk_customers`.`id` as customerid from `lk_customers` LEFT JOIN `lk_race_results` ON `lk_customers`.`id`=`lk_race_results`.`customerid` LEFT JOIN `lk_events` ON `lk_race_results`.`eventid`=`lk_events`.`id` LEFT JOIN `lk_event_types` ON `lk_events`.`type`=`lk_event_types`.`id` WHERE `lk_event_types`.`title` LIKE '".$look_for_q."' and `lk_events`.`ms_end`!='' and `lk_events`.`ts`<'[1]' and `lk_events`.`ts`>'[2]'",$event_ts,($event_ts - (60 * 60 * 2)));
				while($qual_read = lysql_fetch_assoc($qual_query))
				{
					$cus_query = lavu_query("select * from `lk_customers` where `id`='[1]'",$qual_read['customerid']);
					if(lysql_num_rows($cus_query))
					{
						$cus_read = lysql_fetch_assoc($cus_query);
						if(!isset($customer_ids_used[$cus_read['id']]))
						{
							$cus_read['pre_placement'] = 888;
							$cus_read['pre_placed'] = false;
							$cus_read['customerid'] = $cus_read['id'];
							$rlist[] = $cus_read;
							
							$customer_ids_used[$cus_read['id']] = true;
						}
					}
				}
				
				for($rr=0; $rr<count($rlist); $rr++)
				{
					$results_query = lavu_query("select * from `lk_race_results` LEFT JOIN `lk_customers` ON `lk_race_results`.`customerid`=`lk_customers`.`id` LEFT JOIN `lk_events` ON `lk_race_results`.`eventid`=`lk_events`.`id` where `lk_race_results`.`customerid`='[1]' and `lk_events`.`ms_end`!='' and `lk_events`.`ts`<'[2]' and `lk_events`.`ts`>'[3]' order by `lk_events`.`ts` desc limit 1",$rlist[$rr]['customerid'],$event_ts,($event_ts * 1 - (60 * 60 * 12)));
					if(lysql_num_rows($results_query))
					{
						$results_read = lysql_fetch_assoc($results_query);
						$rlist[$rr]['pre_placement'] = $results_read['bestlap'];//place'];
						$rlist[$rr]['pre_placed'] = true;
						$debug .= "\n " . $rlist[$rr]['customerid'] . " " . $results_read['f_name'] . " " . $results_read['l_name'] . " index:$rr" . " = value:" . $results_read['place'];
					}
				}
									
				$r2_list = $rlist;
				$rlist = array();
				while(count($rlist) < count($r2_list))
				{
					$lowest = 99999;
					$lowest_index = "";
					for($n=0; $n<count($r2_list); $n++)
					{
						$r2num = $r2_list[$n]['pre_placement'];
						if($r2num!=98989 && ($r2num < $lowest))
						{
							$lowest = $r2num;
							$lowest_index = $n;
						}
					}
					$rlist[] = $r2_list[$lowest_index];
					$r2_list[$lowest_index]['pre_placement'] = 98989;
					$debug .= "\n add to list: index:" . $lowest_index . " value:$lowest)";
				}
					//if(sessvar("loc_id")==9)
						//mail("corey@meyerwind.com","placement",$debug,"From: info@poslavu.com");
			}
		}
		if(count($rlist) < 1)
		{
			$use_pool = "recent";
			$mintime = 0;
			//echo "query: " . "select * from `lk_customers` where `date_created`>='$mintime'" . "<br>";
			$cust_query = lavu_query("select * from `lk_customers` where `locationid` = '".$loc_id."' AND `last_activity`>='".date("Y-m-d H:i:s",current_ts(-60))."' AND `last_activity`<='".date("Y-m-d H:i:s",current_ts(+2400))."'".$and_cond);// where `date_created`>='$mintime'");
		}
	}
	else if($mode=="search")
	{
		$search_term = reqvar("search_term");
		//$cust_query = lavu_query("select * from `lk_customers` where (`f_name` LIKE TRIM('%".$search_term."%') or `l_name` LIKE TRIM('%".$search_term."%') or `email` LIKE TRIM('%".$search_term."%') or `racer_name` LIKE TRIM('%".$search_term."%') or concat(TRIM(`f_name`),' ',TRIM(`l_name`)) LIKE TRIM('%".$search_term."%') )".$and_cond." order by `f_name` asc, `l_name` asc limit 36");
		$cust_query = lavu_query("select * from `lk_customers` where `locationid`='".$loc_id."' and (`racer_name` LIKE TRIM('%".$search_term."%') or `f_name` LIKE TRIM('%".$search_term."%') or `l_name` LIKE TRIM('%".$search_term."%') or concat(TRIM(`f_name`),' ',TRIM(`l_name`)) LIKE TRIM('%".$search_term."%') )".$and_cond." order by TRIM(`f_name`) LIKE TRIM('".$search_term."') desc, `f_name` asc, `l_name` asc limit 100");
		//$message = "searching for $search_term";
	}
	
	if($cust_query && count($rlist) < 1)
	{
		while($cust_read = lysql_fetch_assoc($cust_query))
		{
			$rlist[] = $cust_read;
		}
	}
		
	$showing_racer_id = 0;
	if(reqvar("showing_racer_id")) $showing_racer_id = reqvar("showing_racer_id");
	else if(sessvar_isset("showing_racer_id")) $showing_racer_id = sessvar("showing_racer_id");
	
	$jscode = "";
?>
	<body>
    	<?php
			if($use_pool=="qualifier")
			{
				echo "<table><td width='40'>&nbsp;</td><td width='240' align='center'><span class='style8'>$pool_title Racers</span></td><td><input type='button' onclick='window.location = \"_DO:cmd=load_url&c_name=customer_search&f_name=lavukart/customer_search.php?pool=recent\"' value='View Recent'></td></table>";
			}
		?>
		<table width="426" border="0" cellspacing=0 cellpadding=0>
        	<?php
			if($message!="")
			{
				echo "<td align='center'><font style='font-size:24px; color:#999999; font-family:Verdana,Arial'>$message</font></td>";
			}
			else
			{
				$min_rows = 12;
				$rowcount = 0;
				$row_selected = 0;
				for($rr=0; $rr<count($rlist); $rr++)
				{
					$cust_read = $rlist[$rr];
				//while($cust_read = lysql_fetch_assoc($cust_query))
				//{
					$rowcount++;
					$fullname = trim($cust_read['f_name'] . " " . $cust_read['l_name']);
					$racer_id = $cust_read['id'];
					$racer_name = $cust_read['racer_name'];
					$birth_date = $cust_read['birth_date'];
					$birth_date1 = $birth_date;
					$membership_expiration = $cust_read['membership_expiration'];
					$date_created = $cust_read['date_created'];
					$bdates = explode("-",$birth_date);
					if(count($bdates) > 2)
					{
						$byear = ($bdates[0] % 100);
						if($byear < 10) $byear = "0" . ($byear * 1);
						$birth_date = $bdates[1] . "/" . $bdates[2] . "/" . $byear;
					}
					else $birth_date = "";
					$row_cmd = str_replace("[fullname]",str_replace("'","%27",$fullname),str_replace("[id]",$racer_id,$cmd));
					//if (($cm) && ($cust_read['confirmed_by_attendant'] == "0") && ($loc_id != 10)) {
					if (($cm) && ($cust_read['confirmed_by_attendant'] == "0")) {
						$row_cmd = "_DO:cmd=confirm_signature&racer_id=".$racer_id."&racer_name=".str_replace("'","%27",$fullname)."&MorA=".$cust_read['minor_or_adult_at_signup'];
					}
					$credits = $cust_read['credits'];
					$use_bg = "images/tab_bg2.png";
					if (($racer_id == $showing_racer_id) && ($tab_name == "Customers")) {
						$row_selected = $rowcount;
						$use_bg = "images/tab_bg2_lit.png";
					}
					$change_select = "";
					if ($tab_name == "Customers") {
						$change_select = " if(row_selected) document.getElementById(\"customer_row_\" + row_selected).style.background = \"URL(images/tab_bg2.png)\"; row_selected = \"".$rowcount."\"; document.getElementById(\"customer_row_\" + row_selected).style.background = \"URL(images/tab_bg2_lit.png)\"";
					}
					$nametext_class = "nametext";
					$td_onclick = "location = \"".$row_cmd."\"; ".$change_select;
					if ($tab_name == "Schedule") {
						$check_schedule = lavu_query("SELECT * FROM `lk_event_schedules` WHERE `eventid` = '".$eventid."' AND `customerid` = '".$racer_id."'");
						if (@lysql_num_rows($check_schedule) > 0) {
							$nametext_class = "nametext_grey";
							$td_onclick = "";
						}
					}
					?>
					 <tr>
                        <td height="46"><table height="46" border="0" cellpadding="0" cellspacing="0">
                          <tr>
                          	<td width="10">&nbsp;</td>
                            <td width="34" align="center" valign="middle">
                            <?php
															if ($tab_name == "Schedule") {
																$display = "inline";
																if ($nametext_class == "nametext_grey") {
																	$display = "none"; 
																}
                            		echo "<input type='checkbox' style='width:30px; height:30px; display:".$display."' id='checkbox_$racer_id'>";
                            	} else {
																echo "&nbsp;";
															}
															$jscode .= "if(document.getElementById('checkbox_$racer_id').checked) { ";
															$jscode .= "  if(setval!='') setval += ','; ";
															$jscode .= "  setval += '$racer_id'; ";
															$jscode .= "} ";
														?>
                            </td>
                            <td id="customer_row_<?php echo $rowcount; ?>" width="313" style="background:URL(<?php echo $use_bg; ?>);" ><table width="313" border="0" cellspacing="0" cellpadding="8">
                              <tr>
                                <td class="<?php echo $nametext_class; ?>" onclick='<?php echo $td_onclick; ?>'>
									<?php 
										if($birth_date1!="" && $birth_date1 > date("Y-m-d",mktime(0,0,0,date("m"),date("d"),date("Y") - 18)))
											$cust_is_minor = true;
										else
											$cust_is_minor = false;
											
										echo $fullname;
										if($racer_name!="") echo " <font style='font-size:10px; color:#000066'>($racer_name)</font>";
										if($birth_date!="") 
										{
											if($cust_is_minor)
												echo " <font style='font-size:10px; color:#bb0000'>$birth_date</font>";
											else
												echo " <font style='font-size:10px; color:#006600'>$birth_date</font>";
										}
									?>
								</td>
                              </tr>
                            </table></td>
							<?php
								if($date_created >= date("Y-m-d 00:00:00"))
									$cust_new = true;
								else
									$cust_new = false;
								if($membership_expiration!="" && $membership_expiration >= date("Y-m-d") && $membership_expration <= "9999-99-99")
									{$mem = true; $bgf = "images/btn_credit2.png";}
								else 	
									{$mem = false;$bgf = "images/btn_credit.png";}
							?>
                            <td width="52" align="left" valign="top" background="<?php echo $bgf?>"><table width="50" height="46" border="0" cellspacing="0" cellpadding="6">
                              <tr>
                                <td align="center" valign="middle" class="creditnumber">
									<?php
										echo $credits;
										if($cust_new) echo "<font style='font-size:8px; color:#008800'>n</font>";
										if($mem) echo "<font style='font-size:8px; color:#000088'>M</font>";
										//if($cust_is_minor) echo "<font style='font-size:8px; color:#880000'>m</font>";
									?>
								</td>
                              </tr>
                            </table></td>
                          </tr>
                        </table></td>
                      </tr>
					<?php
				}
				for($i=$rowcount; $i<$min_rows; $i++)
				{
					?>
					 <tr>
                        <td height="46"><table height="46" border="0" cellpadding="0" cellspacing="0">
                          <tr>
                          	<td width="10">&nbsp;</td>
                            <td width="34" align="center" valign="middle">&nbsp;</td>
                            <td width="313" background="images/tab_bg2.png" ><table width="313" border="0" cellspacing="0" cellpadding="8">
                              <tr>
                                <td class="nametext">&nbsp;</td>
                              </tr>
                            </table></td>
                            <td width="52" align="left" valign="top" background="images/btn_credit.png"><table width="50" height="46" border="0" cellspacing="0" cellpadding="6">
                              <tr>
                                <td align="center" valign="middle" class="creditnumber">&nbsp;</td>
                              </tr>
                            </table></td>
                          </tr>
                        </table></td>
                      </tr>
					<?php
				}
			}
			?>
      <!--<tr>
        <td height="46" background="images/tab.png"><a onclick='location = "_DO:cmd=cancel";'>Custom Modifier Cancel Button Example</a></td>
      </tr>-->
    </table>
	<script language='javascript'>
		function submit_checked()
		{
			setval = "";
			<?php echo $jscode; ?>
			window.location = "_DO:cmd=close_overlay&c_name=customers&f_name=lavukart/customers.php?mode=assign_multiple&racer_ids=" + setval;
		}
		
		row_selected = <?php echo $row_selected; ?>;
	</script>
	</body>
</html>

<?php

} else if($uripage =="customer_search_overlay_wrapper.php") {
//--------------------------------------------------- customer_search_overlay_wrapper.php ----------------------------------------------//
?>

<?php
	//require_once(dirname(__FILE__) . "/../comconnect.php"); 
	$tab_name = reqvar("tab_name");
?>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Untitled Document</title>

		<style type="text/css">
<!--
body {
	background-color: transparent;
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}

.style4 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #8896AD;
}
.style6 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 24px;
	font-weight: bold;
	color: #8896AD;
}
.style8 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 15px;
	font-weight: bold;
	color: #07142a;
}
-->
		</style>
	</head>

<?php
	////require_once("../comconnect.php");
	if(reqvar("cm"))
	{
		$cancel_cmd = "cancel";
		$extra_vars = "&cm=1&cmd=".reqvar("cmd");
	}
	else
	{
		$cancel_cmd = "close_overlay";
		$extra_vars = "";
	}
	
	if($tab_name=="Schedule")
	{
		$bg = "images/win_overlay_3btn.png";
	}
	else
	{
		$bg = "images/win_overlay_2btn_short.png";
	}
?>
	<body>
		<script language="javascript">
          function isEnterKey(evt)
          {
              var charCode = (evt.which)?evt.which:event.keyCode
              if (charCode == 13)
                return true
              else
                return false
          }
            function process_form_key(fld,evt) {
                if (isEnterKey(evt)) {
                    setval = document.getElementById("search_term").value; if(setval!="SEARCH" && setval!="") location = "_DO:cmd=load_url&c_name=customer_search&f_name=lavukart/customer_search.php?mode=search<?php echo $extra_vars; ?>&search_term=" + setval + "&abc=123";
                    return false;
                }
                else return true;
            }
        </script>
		<table width="474" height="598" border="0" cellpadding="0" cellspacing="0" style="background:URL(<?php echo $bg?>); background-repeat:no-repeat; background-position:top center;">
			<tr>
				<td width="26" height="53">&nbsp;</td>
				<td width="422" height="53">
					<table width="428" border="0" cellpadding="6" cellspacing="0">
						<tr>
                        	<td width="46" align="center" onclick='location = "_DO:cmd=<?php echo $cancel_cmd?>";'><span class="style6"> X </span></td>
                            <td width="20">&nbsp;</td>
							<td width="320" align="left" valign="middle"><input id='search_term' class='style4' value='SEARCH' style='border:none; background: URL(win_overlay.png); width:270px' onKeyDown='return process_form_key(this, event)' onfocus='if(this.value=="SEARCH") this.value = "";' onblur='if(this.value=="") this.value = "SEARCH"'></td>
							<td width="36" align="center"><a onclick='setval = document.getElementById("search_term").value; if(setval!="SEARCH" && setval!="") location = "_DO:cmd=load_url&c_name=customer_search&f_name=lavukart/customer_search.php?mode=search<?php echo $extra_vars; ?>&search_term=" + setval + "&abc=123";'><span class="style6"> GO </span></a></td>
						</tr>
					</table>
				</td>
				<td width="26" height="53">&nbsp;</td>
			</tr>
			<tr>
				<td width="26" height="536">&nbsp;</td>
				<td width="422" height="536" align="right" valign="bottom">
                	<?php
                		if($tab_name=="Schedule")
							echo "<img src='images/btn_add_plus.png' border='0' onclick='window.location = \"_DO:cmd=send_js&c_name=customer_search&js=submit_checked()\"' />";
						else
							echo "&nbsp;";
					?>
                </td>
				<td width="26" height="536">&nbsp;</td>
			</tr>
			<tr>
				<td width="26" height="10"></td>
				<td width="422" height="10"></td>
				<td width="26" height="10"></td>
			</tr>
		</table>
	</body>
</html>

<?php

} else if($uripage =="customer_search_wrapper.php") {
//--------------------------------------------------- customer_search_wrapper.php ----------------------------------------------//
?>

<html>
	<head>
		<title>Untitled Document</title>
<style type="text/css">
<!--
body {
	background-color: transparent;
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}



.creditnumber {font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #3d4f6d;
}
.gridposition {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #5A75A0;
}
.icontext {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 11px;
	font-weight: bold;
	color: #5A75A0;
}
.nametext {font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
}
.subtitles {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #5A75A0;
}
.style4 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #8896AD;
}
.style6 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 24px;
	font-weight: bold;
	color: #8896AD;
}
-->
</style>
	</head>

<?php
	//require_once(dirname(__FILE__) . "/../comconnect.php");

?>

	<body>
    <script language="javascript">
	  function isEnterKey(evt)
	  {
		  var charCode = (evt.which)?evt.which:event.keyCode
		  if (charCode == 13)
			return true
		  else
			return false
	  }
		function process_form_key(fld,evt) {
      		if (isEnterKey(evt)) {
				setval = document.getElementById("search_term").value; if(setval!="SEARCH" && setval!="") location = "_DO:cmd=load_url&c_name=customer_search&f_name=lavukart/customer_search.php?mode=search<?php echo $extra_vars; ?>&search_term=" + setval + "&abc=123";
				return false;
			}
			else return true;
		}
	</script>
		<table width="474" height="592" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="474" height="63" colspan="3" background="images/search_bar.png">
					<table width="474" cellspacing="0" cellpadding="0">
						<tr>
							<td width="20" height="50">&nbsp;</td>
							<td width="428" height="50">
								<table width="428" border="0" cellpadding="6" cellspacing="0">
									<tr>
										<td width="12" onclick='document.getElementById("search_term").value = ""'><font color='white'><b>X</b></td>
										<td width="20">&nbsp;</td>
										<td width="320" align="left" valign="middle"><input id='search_term' class='style4' value='SEARCH' style='border:none; background: URL(win_overlay.png); width:270px' onKeyDown='return process_form_key(this, event)' onfocus='if(this.value=="SEARCH") this.value = "";' onblur='if(this.value=="") this.value = "SEARCH"'></td>
										<td width="36" align="center"><a onclick='setval = document.getElementById("search_term").value; if(setval!="SEARCH" && setval!="") location = "_DO:cmd=load_url&c_name=customer_search&f_name=lavukart/customer_search.php?mode=search<?php echo $extra_vars; ?>&search_term=" + setval + "&abc=123";'><span class="style6"> GO </span></a></td>
										<td width="40">&nbsp;</td>
									</tr>
								</table>
							</td>
							<td width="26" height="50">&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td width="59" height="506">&nbsp;</td>
				<td width="395" height="506">&nbsp;</td>
				<td width="20" height="506">&nbsp;</td>
			</tr>
			<tr>
				<td width="59" height="23">&nbsp;</td>
				<td width="395" height="23" background="images/line_bot.png"></td>
				<td width="20" height="23">&nbsp;</td>
			</tr>
		</table>
	</body>
</html>
<?php

} else if($uripage =="customers.php") {
//--------------------------------------------------- customers.php ----------------------------------------------//
?>

<html>
	<head>
		<title>Untitled Document</title>

<style type="text/css">
<!--
body {
	background-color: transparent;
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}



.creditnumber {
	font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #3d4f6d;
}
.groupnumber {
	font-family: Verdana, Geneva, sans-serif;
	font-size: 16px;
	font-weight: bold;
	color: #30613a;
}
.gridposition {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #5A75A0;
}
.icontext {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 11px;
	font-weight: bold;
	color: #5A75A0;
}
.nametext {
	font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
}
.subtitles {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #5A75A0;
}
-->
</style>
	</head>

	<?php
      //require_once(dirname(__FILE__) . "/../comconnect.php");
			
			if(reqvar("loc_id")) $loc_id = reqvar("loc_id");
			else if(sessvar_isset("loc_id")) $loc_id = sessvar("loc_id");
			set_sessvar("loc_id", $loc_id);

			$max_group = 4;
			/*$max_group = 1;
			$get_karts = lavu_query("SELECT `position` FROM `lk_karts` WHERE `locationid` = '".sessvar("loc_id")."'");
			if (lysql_num_rows($get_karts) > 0) {
				while ($extract_k = lysql_fetch_array($get_karts)) {
					if ((floor((int)$extract_k['position'] / 2) > $max_group) && (floor((int)$extract_k['position'] / 2) <= 3)) {
						$max_group = floor((int)$extract_k['position'] / 2);
					}
				}
			}*/
  ?>

	<body>
    	<?php
			$mode = reqvar("mode");
			
			if(reqvar("eventid")) $eventid = reqvar("eventid");
			else if(sessvar_isset("eventid")) $eventid = sessvar("eventid");
			else $eventid = false;
			
			set_sessvar("eventid",$eventid);
			
			if($eventid=="remove") $eventid = false;
			
			if($mode=="assign" || $mode=="assign_multiple" || $mode=="remove")
			{		
				if($mode=="assign_multiple")
				{
					$racer_ids = reqvar("racer_ids");
					$racers = explode(",",$racer_ids);
				}
				else
				{
					$racer_name = reqvar("racer_name");
					$racer_id = reqvar("racer_id");
					$racers = array($racer_id);
				}
				
				for($i=0; $i<count($racers); $i++)
				{
					$racer_id = $racers[$i];
					$sched_query = lavu_query("select * from `lk_event_schedules` where `eventid`='$eventid' and `customerid`='$racer_id'");
					if(lysql_num_rows($sched_query) < 1)
					{
						if($mode=="assign" || $mode=="assign_multiple")
						{
							lavu_query("insert into `lk_event_schedules` (`eventid`,`customerid`,`group`) values ('$eventid','$racer_id','1')");
							lavu_query("update `lk_customers` set `credits` = `credits` - 1 where `id`='$racer_id'");
						}
					}
					else
					{
						if($mode=="remove")
						{
							lavu_query("delete from `lk_event_schedules` where `eventid`='$eventid' and `customerid`='$racer_id'");
							lavu_query("update `lk_customers` set `credits` = `credits` + 1 where `id`='$racer_id'");
						}
					}
				}
			} else if ($mode == "set_group") {
				//echo "<br>UPDATE `lk_event_schedules` SET `group` = '".reqvar("new_group")."' WHERE `customerid` = '".reqvar("customer_id")."' AND `eventid` = '".$eventid."'";
		 		lavu_query("UPDATE `lk_event_schedules` SET `group` = '".reqvar("new_group")."' WHERE `customerid` = '".reqvar("customer_id")."' AND `eventid` = '".$eventid."'");
			}
		?>
		<table width="395" border="0" cellpadding="0" cellspacing="0">
		<?php
            if($eventid)
            {
				//require_once(dirname(__FILE__) . "/lk_functions.php");
				$minrows = 12;
				$maxrows = get_slots_for_event($eventid);//10;
				$rowcount = 0;
				$overbooked = 0;
				
				if($maxrows < $minrows)
					$minrows = $maxrows;
				
				//echo "<br>select * from `lk_event_schedules` left join `lk_customers` on `lk_event_schedules`.`customerid`=`lk_customers`.`id` where `eventid`='$eventid'<br>";
				$sched_query = lavu_query("select * from `lk_event_schedules` left join `lk_customers` on `lk_event_schedules`.`customerid`=`lk_customers`.`id` where `eventid`='$eventid'");
				while($sched_read = lysql_fetch_assoc($sched_query))
				{
					$rowcount++;
					$fullname = trim($sched_read['f_name'] . " " . $sched_read['l_name']);
					$credits = $sched_read['credits'];
					$customerid = $sched_read['id'];
					$next_group = (((int)$sched_read['group'] % $max_group) + 1);
					if($credits=="") $credits = 0;
					
					$membership_expiration = $sched_read['membership_expiration'];
					$date_created = $sched_read['date_created'];
					if($date_created >= date("Y-m-d 00:00:00"))
						$cust_new = true;
					else
						$cust_new = false;
					if($membership_expiration!="" && $membership_expiration >= date("Y-m-d") && $membership_expration <= "9999-99-99")
						{$mem = true; $bgf = "images/btn_credit2.png";}
					else 	
						{$mem = false;$bgf = "images/btn_credit.png";}
						
					$birth_date = $sched_read['birth_date'];
					$birth_date1 = $birth_date;
					$bdates = explode("-",$birth_date);
					if(count($bdates) > 2)
					{
						$byear = ($bdates[0] % 100);
						if($byear < 10) $byear = "0" . ($byear * 1);
						$birth_date = $bdates[1] . "/" . $bdates[2] . "/" . $byear;
					}
					else $birth_date = "";
					if($birth_date1!="" && $birth_date1 > date("Y-m-d",mktime(0,0,0,date("m"),date("d"),date("Y") - 18)))
						$cust_is_minor = true;
					else
						$cust_is_minor = false;
						
					if($rowcount > $maxrows && $overbooked==0)
					{
						echo "<tr><td bgcolor='#880000' style='font-size:2px'>&nbsp;</td></tr>";
						$overbooked++;
					}
					?>
					<tr>
						<td height="46" align="left" valign="top"><table width="395" height="46" border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td width="30" align="center" valign="middle" class="subtitles"><a href="customers.php?mode=remove&racer_name=<?php echo $fullname?>&racer_id=<?php echo $customerid?>"><img src="images/btn_x_30.png" width="30" height="41" border="0"/></a></td>
								<td width="51" align="center" valign="middle" style="background:URL(images/group_color_bg<?php echo $sched_read['group']; ?>.png);" onClick="this.style.backgroundImage = 'URL(images/group_color_bg<?php echo $next_group; ?>.png)'; window.location = 'customers.php?mode=set_group&new_group=<?php echo $next_group; ?>&customer_id=<?php echo $customerid; ?>';">
									<table width="51" border="0" cellspacing="0" cellpadding="0">
										<tr><td align="center" valign="middle" class="groupnumber" >&nbsp;</td></tr>
									</table>
								</td>
								<td width="262" align="left" valign="middle" background="images/tab_bg_262.png" >
									<table width="262" border="0" cellspacing="0" cellpadding="8">
										<tr><td class="nametext">
											&nbsp;
											<?php 
												echo $fullname; 
												if($birth_date!="") 
												{
													if($cust_is_minor)
														echo " <font style='font-size:10px; color:#bb0000'>$birth_date</font>";
													else
														echo "";//" <font style='font-size:10px; color:#006600'>$birth_date</font>";
												}
											?>
										</td></tr>
									</table>
								</td>
								<td width="52" align="left" valign="top" background="images/btn_credit.png" >
									<table width="50" height="46" border="0" cellspacing="0" cellpadding="6">
										<tr><td align="center" valign="middle" class="creditnumber">
										<?php
											echo $credits;
											if($cust_new) echo "<font style='font-size:8px; color:#008800'>n</font>";
											if($mem) echo "<font style='font-size:8px; color:#000088'>M</font>";
										?>
										</td></tr>
									</table>
								</td>
							</tr>
						</table></td>
					</tr>
					<?php
				}
				for($i=$rowcount; $i<$minrows; $i++)
				{
				?>
						<tr>
							<td height="46">
								<table width="395" height="46" border="0" cellpadding="0" cellspacing="0">
									<tr>
										<td width="30" align="center" valign="middle"><img src="images/btn_x_30.png" width="30" height="41" /></span></td>
										<td width="51" align="center" valign="middle" background="images/tab_bg_51.png">&nbsp;</td>
										<td width="262" background="images/tab_bg_262.png" >
											<table width="262" border="0" cellspacing="0" cellpadding="8">
												<tr><td class="nametext">&nbsp;</td></tr>
											</table>
										</td>
										<td width="52" align="left" valign="top" background="images/btn_credit.png">
											<table width="50" height="46" border="0" cellspacing="0" cellpadding="6">
												<tr><td align="center" valign="middle" class="creditnumber">&nbsp;</td></tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					<?php
				}
			}
	?>
		</table>
	</body>
</html>
<?php

} else if($uripage =="customers_wrapper.php") {
//--------------------------------------------------- customers_wrapper.php ----------------------------------------------//
?>

<html>
	<head>
		<title>Untitled Document</title>
<style type="text/css">
<!--
body {
	background-color: transparent;
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}



.creditnumber {font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #3d4f6d;
}
.gridposition {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #5A75A0;
}
.icontext {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 11px;
	font-weight: bold;
	color: #5A75A0;
}
.nametext {font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
}
.subtitles {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #5A75A0;
}
-->
</style>
	</head>

	<body>
	  	<table width="474" height="598" border="0" cellpadding="0" cellspacing="0" >
    		<tr>
      			<td width="79" height="536" align="left" valign="top">
					<table width="79" border="0" cellpadding="0" cellspacing="0">
          				<tr><td height="26">&nbsp;</td></tr>
            			<tr>
            				<td height="180" align="center" valign="bottom">
								<table width="68" border="0" cellpadding="0" cellspacing="0">
									<tr><td height="32" align="center" valign="middle" class="subtitles"><a href="_DO:cmd=create_overlay&f_name=lavukart/customer_search_overlay_wrapper.php;lavukart/customer_search.php&c_name=customer_search_overlay_wrapper;customer_search&dims=300,100,474,598;320,158,426,480&scroll=0;1"><img src="images/btn_add.png" width="66" height="66" border="0"/></a></td></tr>
									<tr><td height="32" align="center" valign="middle"><span class="icontext">ADD</span></span></td></tr>
								</table>
							</td>
						</tr>
						<tr>
							<td height="110" align="center" valign="bottom">
								<table width="68" border="0" cellpadding="0" cellspacing="0">
									<tr><td height="32" align="center" valign="middle" class="subtitles"><a onClick="if(confirm('Are you sure you want to remove the selected event?')) location = '_DO:cmd=load_url&c_name=events;customers&f_name=lavukart/events.php;lavukart/customers.php?mode=remove_event&eventid=remove&abc=123'"><img src="images/btn_remove_trash.png" width="66" height="66" border="0"/></a></td></tr>
									<tr><td height="32" align="center" valign="middle"><span class="icontext">REMOVE</span></span></td></tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
				<td align="left" valign="top">
					<table width="395" height="598" border="0" cellspacing="0" cellpadding="0">
						<tr><td height="23" background="images/line_top.png"></td></tr>
						<tr><td height="552" align="left" valign="top">&nbsp;</td></tr>
						<tr><td height="23" background="images/line_bot.png"></td></tr>
					</table>
				</td>
			</tr>
		</table>
	</body>
</html>
<?php

} else if($uripage =="events.php") {
//--------------------------------------------------- events.php ----------------------------------------------//
?>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Untitled Document</title>

		<style type="text/css">
<!--
body {
	background-color: transparent;
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}

.creditnumber {font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #3d4f6d;
}
.nametext {font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
}
.active_nametext {font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #00aa00;
}
.finished_nametext {font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color:#999999;
}
.subtitles {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #5A75A0;
}
.style411 {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #5A75A0;
}
.gridposition {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #5A75A0;
}
.icontext {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 11px;
	font-weight: bold;
	color: #5A75A0;
}
.subtitles1 {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-weight: bold;
	color: #5A75A0;
}
.title1 {	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 18px;
	font-weight: bold;
	color: #5A75A0;
}
-->
		</style>
		<script language="javascript">
			function findTopPos(objid) 
			{
				obj = document.getElementById(objid);
				var curleft = curtop = 0;
				if (obj.offsetParent) 
				{
					do 
					{
						curleft += obj.offsetLeft;
						curtop += obj.offsetTop;
					} while (obj = obj.offsetParent);
					return curtop;
				}
				else return 0;
			}			
		</script>
	</head>

<?php
	//require_once(dirname(__FILE__) . "/../comconnect.php");
	//ini_set("display_errors","1");
	/*if(reqvar("eventid")) $eventid = reqvar("eventid");
	else if(sessvar_isset("eventid")) $eventid = sessvar("eventid");
	set_sessvar("eventid",$eventid);*/
	$eventid = sessvar("eventid");
	$row_selected = 0;
	
	$set_scrolltop = reqvar("set_scrolltop");
	$mode = reqvar("mode");
	if($mode=="add_event")
	{
		$set_date = reqvar("event_date");
		$t = reqvar("event_time");
		$set_type = reqvar("event_type");
		$set_locationid = reqvar("event_locationid");
		$set_trackid = reqvar("event_trackid");
		
		$dparts = explode("-",$set_date);
		$set_year = $dparts[0];
		$set_month = $dparts[1];
		$set_day = $dparts[2];
		$set_min = $t % 100;
		$set_hour = (($t - $set_min) / 100);
		$set_time = $t;
		$set_ts = mktime($set_hour,$set_min,0,$set_month,$set_day,$set_year);

		$success = lavu_query("insert into `lk_events` (`locationid`,`type`,`date`,`time`,`ts`,`trackid`) values ('$set_locationid','$set_type','$set_date','$set_time','$set_ts','$set_trackid')");
		if(!$success) echo "failed to create new event<br>";
		//echo "insert into `lk_events` (`locationid`,`type`,`date`,`time`,`ts`,`trackid`) values ('$set_locationid','$set_type','$set_date','$set_time','$set_ts','$set_trackid')";
	}
	else if($mode=="move_event")
	{
		$mtvars = array();
		$mtvars['date'] = reqvar("moveto_date");
		$mtvars['time'] = reqvar("moveto_slot");
		$mtvars['id'] = $eventid;//reqvar("eventid");

		$t = $mtvars['time'];
		$dparts = explode("-",$mtvars['date']);
		$set_year = $dparts[0];
		$set_month = $dparts[1];
		$set_day = $dparts[2];
		$set_min = $t % 100;
		$set_hour = (($t - $set_min) / 100);
		$set_time = $t;
		$set_ts = mktime($set_hour * 1,$set_min * 1,0,$set_month * 1,$set_day * 1,$set_year * 1);
		$mtvars['ts'] = $set_ts;
		
		$trackid = sessvar("trackid");
		$mtvars['trackid'] = $trackid;
		$track_query = lavu_query("select * from `lk_tracks` where id='[1]'",$trackid);
		if(lysql_num_rows($track_query))
		{
			$track_read = lysql_fetch_assoc($track_query);
			$time_span = $track_read['time_interval'];
			
			$move_events_queue = array();
			$slot_clear = false;
			$ctime = $mtvars['time'];
			$cdate = $mtvars['date'];
			
			$set_min = $ctime % 100;
			$set_hour = (($ctime - $set_min) / 100);
			$cts = mktime($set_hour * 1,$set_min * 1,0,$set_month * 1,$set_day * 1,$set_year * 1);
			$loopcount = 0;
						
			while($slot_clear==false && $loopcount < 300)
			{			
				$e_query = lavu_query("select * from `lk_events` where `trackid`='[1]' and `ts`='[2]' and `id`!='[3]'",$trackid,$cts,$eventid);
				if(lysql_num_rows($e_query))
				{
					$ctime = time_advance($ctime,$time_span);
					$set_min = $ctime % 100;
					$set_hour = (($ctime - $set_min) / 100);
					$cts = mktime($set_hour * 1,$set_min * 1,0,$set_month * 1,$set_day * 1,$set_year * 1);
					while($e_read = lysql_fetch_assoc($e_query))
					{
						$move_events_queue[] = array($e_read['id'],$cdate,$ctime,$cts);
					}
				}
				else
				{
					$slot_clear = true;
				}
				$loopcount++;
			}
			for($n=0; $n<count($move_events_queue); $n++)
			{
				$cvars['id'] = $move_events_queue[$n][0];
				$cvars['date'] = $move_events_queue[$n][1];
				$cvars['time'] = $move_events_queue[$n][2];
				$cvars['ts'] = $move_events_queue[$n][3];
				//$cvars['trackid'] = 
				//mail("corey@poslavu.com","forward: " . $move_events_queue[$n][0],"date: " . $move_events_queue[$n][1] . " time: " . $move_events_queue[$n][2] . " ts: " . $move_events_queue[$n][3],"From: info@poslavu.com");
				lavu_query("update `lk_events` set `date`='[date]', `time`='[time]', `ts`='[ts]' where `id`='[id]",$cvars);
			}
			
			lavu_query("update `lk_events` set `date`='[date]', `time`='[time]', `ts`='[ts]', `trackid`='[trackid]' where `id`='[id]'",$mtvars);
		}
	}
	else if($mode=="remove_event")
	{
		if($eventid!="remove")
		{
			$event_query = lavu_query("select * from `lk_events` where id='[1]'",$eventid);
			if(lysql_num_rows($event_query))
			{
				$event_read = lysql_fetch_assoc($event_query);
				$date_parts = explode("-",$event_read['date']);
				if(count($date_parts) > 2)
				{
					$year = $date_parts[0];
					$month = $date_parts[1];
					$day = $date_parts[2];
				}
				lavu_query("delete from `lk_events` where `id`='[1]'",$eventid);
			}
		}
	}
	else if($mode=="set_track")
	{
		$trackid = reqvar("set_trackid");
		set_sessvar("trackid",$trackid);
	}
	
	if(sessvar_isset("trackid"))
		$trackid = sessvar("trackid");
	else
		$trackid = false;
	$anchor_now = "page_body";
?>

	<body id="page_body" onLoad="body_loaded()">
		<table width="395" height="535" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td height="535" align="left" valign="top">
					<table width="382" border="0" cellspacing="0" cellpadding="0">
             <?php
							$minrows = 10;
							$rowcount = 0;
							
							if(postvar("day")==false && $eventid)
							{
								$event_query = lavu_query("select * from `lk_events` where id='[1]'",$eventid);
								if(lysql_num_rows($event_query))
								{
									$event_read = lysql_fetch_assoc($event_query);
									$date_parts = explode("-",$event_read['date']);
									if(count($date_parts) > 2)
									{
										$year = $date_parts[0];
										$month = $date_parts[1];
										$day = $date_parts[2];
									}
								}
								else
								{
									$year = postvar("year",date("Y"));
									$month = postvar("month",date("m"));
									$day = postvar("day",date("d"));
								}
							}
							else
							{
								$year = postvar("year",date("Y"));
								$month = postvar("month",date("m"));
								$day = postvar("day",date("d"));
							}
							if($month < 10) $month = "0" . ($month * 1);
							if($day < 10) $day = "0" . ($day * 1);
							$fulldate = $year . "-" . $month . "-" . $day;
							//echo $fulldate;

							function create_timeslot_row($date,$slot,$trackid,$locationid,$eventid=false)
							{
								?>
                                <tr>
                                    <td height="46" id="timeslot_<?php echo $slot?>"><table width="79" border="0" cellspacing="0" cellpadding="8">
                                        <tr>
                                            <td class="subtitles1"><nobr>
											<?php
												if($eventid)
												{
													echo "<a onclick='if(confirm(\"Are you sure you want to move this event?\")) location = \"_DO:cmd=load_url&c_name=events&f_name=lavukart/events.php?mode=move_event&eventid=".$eventid."&moveto_date=".$date."&moveto_slot=".$slot."&set_scrolltop=\" + window.pageYOffset + \"&abc=123\"; '>".display_time($slot)."</a>"; 
												}
												else
												{
													echo display_time($slot); 
												}
											?>
											</nobr></td> 
                                        </tr>
                                    </table></td>
                                    <td width="313" align="left" valign="middle" background="images/tab_bg2.png" onClick="window.location = '_DO:cmd=create_overlay&f_name=lavukart/newevent_wrapper.php;lavukart/newevent.php&c_name=newevent_wrapper;newevent&scroll=0;1&dims=112,200,423,384;112,242,423,327?date=<?php echo $date; ?>&time=<?php echo $slot; ?>&trackid=<?php echo $trackid; ?>&locid=<?php echo $locationid; ?>&scrolltop=' + window.pageYOffset"><table width="313" border="0" cellspacing="0" cellpadding="8">
                                        <tr>
                                            <td class="nametext">&nbsp;</td>
                                        </tr>
                                    </table></td>
                                </tr>
                                <?php
							}
							
							$locationid = reqvar("loc_id");
							$closest_time = 0;
							$timeslot_selected = 0;
							
							if($trackid)
							{
								$track_query = lavu_query("select * from `lk_tracks` where id='[1]' and `locationid`='[2]'",$trackid,$locationid);
								if(!lysql_num_rows($track_query))
									$trackid = false;
							}
							
							if(!$trackid)
							{
								$track_query = lavu_query("select * from `lk_tracks` where `locationid`='$locationid' and `_deleted`!='1' order by id asc limit 1");
							}
							if(!lysql_num_rows($track_query))
							{
								echo "Error: No Tracks exist for this locationid " . $locationid;
								exit();
							}
							
							$track_read = lysql_fetch_assoc($track_query);
							$time_start = $track_read['time_start'];
							$time_end = $track_read['time_end'];
							$time_span = $track_read['time_interval'];
							$trackid = $track_read['id'];
							$track_title = $track_read['title'];
							set_sessvar("trackid",$trackid);
							
							if(!is_numeric($time_start)) $time_start = 1100;
							if(!is_numeric($time_end)) $time_end = 2200;
														
							//echo "<tr><td><td align='center' class='subtitles1'><b>$month/$day/$year</b></td></tr>";
							/*if($year==date("Y") && $month==date("m") && $day==date("d"))
							{
								$mints = current_ts(-60); //mktime($hour - 1,$minute,0,$month,$day,$year);
							}
							else*/
								$mints = mktime(0,0,0,$month*1,$day*1,$year*1);
							$maxts = mktime(24,0,0,$month*1,$day*1,$year*1);
							
							$current_time = $time_start;
							function time_advance($time, $add)
							{
								$time_hours = $time - ($time % 100);
								$time_minutes = $time % 100;
								$add_hours = $add - ($add % 100);
								$add_minutes = $add % 100;
								$hours = $time_hours + $add_hours;
								$minutes = $time_minutes + $add_minutes;
								if($minutes >= 60)
								{
									$hours += 100;
									$minutes -= 60;
								}
								return $hours + $minutes;
							}
					
							$event_query = lavu_query("select `lk_events`.`ts` as `ts`, `lk_events`.`time` as time, `lk_events`.`id` as id, `lk_events`.`ms_start` as `ms_start`, `lk_events`.`ms_end` as `ms_end`, `lk_event_types`.`title` as `title`, `lk_event_types`.`slots` as `slots` from `lk_events` left join `lk_event_types` on lk_events.type = lk_event_types.id where `trackid`='[1]' and `ts`>='$mints' and `ts`<='$maxts' order by `ts` asc",$trackid);
							while($event_read = lysql_fetch_assoc($event_query))
							{
								
								while($current_time < $event_read['time'])
								{
									$rowcount++;
									if($current_time >= date("Hi",$mints))
									{
										create_timeslot_row($fulldate,$current_time,$trackid,$locationid,$eventid);
										if(abs($current_time - date("Hi")) < abs($current_time - $closest_time))
											$closest_time = $current_time;
									}
									$current_time = time_advance($current_time,$time_span);
								}
								if($current_time < time_advance($event_read['time'],$time_span))
									$current_time = time_advance($current_time,$time_span);
								$rowcount++;
								
								if(abs($event_read['time'] - date("Hi")) < abs($event_read['time'] - $closest_time))
									$closest_time = $event_read['time'];
								?>
								<tr>
									<td width="69" height="46" id="timeslot_<?php echo $event_read['time'];?>">
										<table width="69" border="0" cellspacing="0" cellpadding="8">
											<tr><td class="subtitles1"><nobr>
											<?php
												/*if($eventid)
												{
													echo "<a onclick='if(confirm(\"Are you sure you want to move this event?\")) location = \"_DO:cmd=load_url&c_name=events&f_name=lavukart/events.php?mode=move_event&eventid=".$eventid."&moveto_date=".$fulldate."&moveto_slot=".$event_read['time']."&abc=123\"; '>".display_time($event_read['time'])."</a>"; 
												}
												else
												{ */
													echo display_time($event_read['time']); 
												//}
											?>
											</nobr></td></tr>
										</table>
									</td>
                                    <?php
										$start_style = "";
										$use_bg = "images/tab_bg2.png";
										if($eventid==$event_read['id'])
										{
											$use_bg = "images/tab_bg2_lit.png";
											$row_selected = $rowcount;
											$timeslot_selected = $event_read['time'];
										}
										
										if ($_POST['tab_name'] == "Schedule") {
											$comp_cmd = "location = \"_DO:cmd=load_url&c_name=customers&f_name=lavukart/customers.php?mode=schedule&eventid=".$event_read['id']."&abc=123\";";
										} else if ($_POST['tab_name'] == "Pit") {
											$comp_cmd = "location = \"_DO:cmd=close_overlay&c_name=car_assignments_wrapper;car_assignments;racers_for_this_race&f_name=lavukart/car_assignments_wrapper.php;lavukart/car_assignments.php;lavukart/racers_for_this_race.php?eventid=".$event_read['id']."&abc=123\";";
										} else {
											$comp_cmd = "";
										}
										$tdclass = "nametext";
										if($event_read['ms_start']!="" && $event_read['ms_end']=="")
										{
											$tdclass = "active_nametext";
										}
										else if($event_read['ms_end']!="")
										{
											$tdclass = "finished_nametext";
										}
									?>
									<td width="313" id="event_row_<?php echo $rowcount?>" style="background: URL(<?php echo $use_bg?>);" onclick='if(row_selected) document.getElementById("event_row_" + row_selected).style.background = "URL(images/tab_bg2.png)"; row_selected = "<?php echo $rowcount?>"; this.style.background = "URL(images/tab_bg2_lit.png)";<?php echo $comp_cmd; ?>'>
										<table width="313" border="0" cellspacing="0" cellpadding="8">
											<tr><td class="<?php echo $tdclass;?>">
												<?php echo ucfirst($event_read['title']); ?>
                                                <?php 
													echo "&nbsp;(";
													$sched_query = lavu_query("select count(*) as sched_count from `lk_event_schedules` where `eventid`='[1]' and _deleted!='1'",$event_read['id']);
													if(lysql_num_rows($sched_query))
													{
														$sched_read = lysql_fetch_assoc($sched_query);
														$sched_count = $sched_read['sched_count'];
													}
													else $sched_count = 0;
													echo $sched_count . "/";
													if($event_read['slots']=="") echo "10"; else echo $event_read['slots'];
													echo ")";
												?>
                                            </td></tr>
										</table>
									</td>
								</tr>
                                <?php
							}
							while($current_time < $time_end)
							{
								$rowcount++;
								if($current_time >= date("Hi",$mints))
									create_timeslot_row($fulldate,$current_time,$trackid,$locationid,$eventid);
								$current_time = time_advance($current_time,$time_span);
							}
							$rowcount++;
							?>
					</table>
					<script language='javascript'>row_selected = <?php echo $row_selected?>;</script>
				</td>
			</tr>
		</table>
		<script language="javascript">
			function body_loaded() {
				<?php 
					if($timeslot_selected > 0) {
						$anchor_now = "timeslot_".$timeslot_selected;
					}
					else if($closest_time > 0) {
						$anchor_now = "timeslot_".$closest_time;
					}
					if(!$set_scrolltop || $set_scrolltop=="")
					{
						$set_scrolltop = "(findTopPos('$anchor_now') - 40)";
					}
					//if(strpos($company_code,"DEV")!==false) {echo "alert(\"pos: \" + $set_scrolltop); ";}
					echo "window.scrollTo(0,$set_scrolltop); ";
				?>
				document.getElementById("page_body").style.visibility = "visible";
			}
		</script>
	</body>
</html>
<?php

} else if($uripage =="events_overlay_wrapper.php") {
//--------------------------------------------------- events_overlay_wrapper.php ----------------------------------------------//
?>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Untitled Document</title>

		<style type="text/css">
<!--
body {
	background-color: transparent;
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}

.style4 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #8896AD;
}
.style6 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 24px;
	font-weight: bold;
	color: #8896AD;
}
.style8 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 15px;
	font-weight: bold;
	color: #07142a;
}
.nametext {font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
}
.subtitles1 {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #5A75A0;
}
.title1 {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 18px;
	font-weight: bold;
	color: #5A75A0;
}
.icontext {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 11px;
	font-weight: bold;
	color: #5A75A0;
}
.subtitles {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #5A75A0;
}
-->
		</style>
	</head>

<?php	
	//require_once(dirname(__FILE__) . "/../comconnect.php");
	
	$mode = reqvar("mode");
	$locationid = reqvar("loc_id");
	$eventid = sessvar("eventid");
	
	if($mode=="set_track")
	{
		$trackid = reqvar("set_trackid");
	}
	else
	{
		if(sessvar_isset("trackid"))
			$trackid = sessvar("trackid");
		else
			$trackid = false;
	}
	if($trackid)
	{
		$track_query = lavu_query("select * from `lk_tracks` where id='[1]' and `locationid`='[2]'",$trackid,$locationid);
		if(!lysql_num_rows($track_query))
			$trackid = false;
	}
	
	if(!$trackid)
	{
		$track_query = lavu_query("select * from `lk_tracks` where `locationid`='$locationid' and `_deleted`!='1' order by id asc limit 1");
	}
	if(!lysql_num_rows($track_query))
	{
		$track_title = "&nbsp;";
	}
	else
	{	
		$track_read = lysql_fetch_assoc($track_query);
		$time_start = $track_read['time_start'];
		$time_end = $track_read['time_end'];
		$time_span = $track_read['time_interval'];
		$trackid = $track_read['id'];
		$track_title = $track_read['title'];
	}
	
	$year = reqvar("year");
	$month = reqvar("month");
	$day = reqvar("day");
	
	if(!$year && $eventid)
	{
		$event_query = lavu_query("select * from `lk_events` where id='[1]'",$eventid);
		if(lysql_num_rows($event_query))
		{
			$event_read = lysql_fetch_assoc($event_query);
			$set_date = $event_read['date'];
			$date_parts = explode("-",$set_date);
			if(count($date_parts) > 2)
			{
				$year = $date_parts[0];
				$month = $date_parts[1];
				$day = $date_parts[2];
			}
		}
	}
	if(!$year) 
	{
		$year = date("Y");
		$month = date("m");
		$day = date("d");
	}
	$date_ts = mktime(0,0,0,$month,$day,$year);
	$show_date = date("M d",$date_ts);
?>

	<body>
		<table width="494" height="598" border="0" cellpadding="0" cellspacing="0" background="images/win_overlay_events_bg.png">
			<tr><td height="4"></td></tr>
			<tr>
				<td width="494" align="center" valign="top">
					<table width="464" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td width="76" rowspan="2" align="center" valign="top">
								<table width="68" border="0" cellpadding="0" cellspacing="0">
									<tr><td height="80" align="center" valign="middle" class="subtitles"><img src="images/btn_x.png" border="0" onClick="window.location = '_DO:cmd=close_overlay'" /></td></tr>
									<tr><td height="32" align="center" valign="middle" class="subtitles"><a href="_DO:cmd=create_overlay&f_name=lavukart/calendar_wrapper.php;lavukart/calendar.php&c_name=calendar_wrapper;calendar&dims=112,200,423,374;112,200,423,374"><img src="images/btn_calendar.png" width="68" height="68" /></a></td></tr>
									<tr><td height="32" align="center" valign="middle"><span class="icontext">CALENDAR</span></span></td></tr>
									<tr><td height="30" align="center" valign="middle" class="subtitles">&nbsp;</td></tr>
									<tr><td height="32" align="center" valign="middle" class="subtitles"><a href="_DO:cmd=create_overlay&f_name=lavukart/newevent_wrapper.php;lavukart/select_track.php&c_name=select_track_wrapper;select_track&dims=112,200,423,374;112,200,423,374?locid=<?php echo $_REQUEST['loc_id']; ?>"><img src="images/btn_select_track.png" width="68" height="68" /></a></td></tr>
									<tr><td height="32" align="center" valign="middle"><span class="icontext">TRACKS</span></span></td></tr>
								</table>
							</td>
							<td width="12">&nbsp;</td>
							<td width="376">
								<table width="382" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td width="119" height="46">
											<table width="119" border="0" cellspacing="0" cellpadding="8">
												<tr><td width="119" class="title1"><?php echo "<b>".$show_date."</b>"?></td></tr>
											</table>
										</td>
										<td width="223" >
											<table width="223" border="0" cellpadding="8" cellspacing="0">
												<tr><td align="center" class="title1"><font color="#4A5580"><?php echo "<b>".$track_title."</b>"?></font></td></tr>
											</table>
										</td>
										<td width="80">&nbsp;</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</body>
</html>

<?php

} else if($uripage =="events_wrapper.php") {
//--------------------------------------------------- events_wrapper.php ----------------------------------------------//
?>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Untitled Document</title>

		<style type="text/css">
<!--
body {
	background-color: transparent;
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}

.creditnumber {font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #3d4f6d;
}
.nametext {font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
}
.subtitles {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #5A75A0;
}
.style411 {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #5A75A0;
}
.gridposition {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #5A75A0;
}
.icontext {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 11px;
	font-weight: bold;
	color: #5A75A0;
}
.subtitles1 {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #5A75A0;
}
.title1 {	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 18px;
	font-weight: bold;
	color: #5A75A0;
}
-->
		</style>
	</head>
	
<?php	
	//require_once(dirname(__FILE__) . "/../comconnect.php");
	
	$mode = reqvar("mode");
	$locationid = reqvar("loc_id");
	$eventid = sessvar("eventid");
	
	if($mode=="set_track")
	{
		$trackid = reqvar("set_trackid");
	}
	else
	{
		if(sessvar_isset("trackid"))
			$trackid = sessvar("trackid");
		else
			$trackid = false;
	}
	if($trackid)
	{
		$track_query = lavu_query("select * from `lk_tracks` where id='[1]' and `locationid`='[2]'",$trackid,$locationid);
		if(!lysql_num_rows($track_query))
			$trackid = false;
	}
	
	if(!$trackid)
	{
		$track_query = lavu_query("select * from `lk_tracks` where `locationid`='$locationid' and `_deleted`!='1' order by id asc limit 1");
	}
	if(!lysql_num_rows($track_query))
	{
		$track_title = "&nbsp;";
	}
	else
	{	
		$track_read = lysql_fetch_assoc($track_query);
		$time_start = $track_read['time_start'];
		$time_end = $track_read['time_end'];
		$time_span = $track_read['time_interval'];
		$trackid = $track_read['id'];
		$track_title = $track_read['title'];
	}
	
	$year = reqvar("year");
	$month = reqvar("month");
	$day = reqvar("day");
	
	if(!$year && $eventid)
	{
		$event_query = lavu_query("select * from `lk_events` where id='[1]'",$eventid);
		if(lysql_num_rows($event_query))
		{
			$event_read = lysql_fetch_assoc($event_query);
			$set_date = $event_read['date'];
			$date_parts = explode("-",$set_date);
			if(count($date_parts) > 2)
			{
				$year = $date_parts[0];
				$month = $date_parts[1];
				$day = $date_parts[2];
			}
		}
	}
	if(!$year) 
	{
		$year = date("Y");
		$month = date("m");
		$day = date("d");
	}
	$date_ts = mktime(0,0,0,$month,$day,$year);
	$show_date = date("M d",$date_ts);
?>

	<body>
		<table width="474" height="598" border="0" cellpadding="0" cellspacing="0" >
			<tr>
				<td width="77" height="536" align="center" valign="top">
					<table width="68" border="0" cellpadding="0" cellspacing="0">
						<tr><td height="120" align="center" valign="middle" class="subtitles">&nbsp;</td></tr>
						<tr><td height="32" align="center" valign="middle" class="subtitles"><a href="_DO:cmd=create_overlay&f_name=lavukart/calendar_wrapper.php;lavukart/calendar.php&c_name=calendar_wrapper;calendar&dims=112,200,423,374;112,200,423,374"><img src="images/btn_calendar.png" width="68" height="68" border="0"/></a></td></tr>
						<tr><td height="32" align="center" valign="middle"><span class="icontext">CALENDAR</span></span></td></tr>
						<tr><td height="20" align="center" valign="middle" class="subtitles">&nbsp;</td></tr>
						<tr><td height="32" align="center" valign="middle" class="subtitles"><a href="_DO:cmd=create_overlay&f_name=lavukart/newevent_wrapper.php;lavukart/select_track.php&c_name=select_track_wrapper;select_track&dims=112,200,423,374;112,200,423,374?locid=<?php echo $_REQUEST['loc_id']; ?>"><img src="images/btn_select_track.png" width="68" height="68" border="0"/></a></td></tr>
						<tr><td height="32" align="center" valign="middle"><span class="icontext">TRACKS</span></span></td></tr>
					</table>
				</td>
				<td align="left" valign="top">
					<table width="395" height="598" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td height="40">
								<table width="392" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td width="119" height="46">
											<table width="119" border="0" cellspacing="0" cellpadding="8">
												<tr><td width="119" class="title1"><?php echo "<b>".$show_date."</b>"?></td></tr>
											</table>
										</td>
										<td width="233" >
											<table width="233" border="0" cellpadding="8" cellspacing="0">
												<tr><td align="center" class="title1"><font color="#4A5580"><?php echo "<b>".$track_title."</b>"?></font></td></tr>
											</table>
										</td>
										<td width="80">&nbsp;</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td height="535" align="left" valign="top">
								<table cellspacing="0" cellpadding="0">
									<tr>
										<td><img src="images/line.png" width="1" height="535"></td>
									</tr>
								</table>
							</td>
						</tr>
						<tr><td height="23" background="images/line_bot.png"></td></tr>
					</table>
				</td>
			</tr>
		</table>
	</body>
</html>
<?php

} else if($uripage =="group_event_info.php") {
//--------------------------------------------------- group_event_info.php ----------------------------------------------//
?>

<html>
	<head>
		<title>Group Event Info</title>
		<style type="text/css">
<!--
body {
	background-color: transparent;
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}

.creditnumber {
	font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #3d4f6d;
}
.gridposition {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #5A75A0;
}
.icontext {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 11px;
	font-weight: bold;
	color: #5A75A0;
}
.nametext {
	font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
}
.title {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 16px;
	font-weight: bold;
	color: #5A75A0;
}
.subtitles {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #5A75A0;
}
.bg_signature_top {
	background-image:URL(images/bg_signature_top.png);
	background-repeat:no-repeat;
	width:400px;
	height:9px;
}
.bg_signature_mid {
	background-image:URL(images/bg_signature_mid.png);
	background-repeat:repeat-y;
	width:400px;
}
.bg_signature_bottom {
	background-image:URL(images/bg_signature_bottom.png);
	background-repeat:no-repeat;
	width:400px;
	height:9px;
}
-->
		</style>
		<script language="javascript">
		
			function cancel_or_delete(CorD,group_event_id,event_date,selected_row) {
			
				window.location = "group_event_info.php?CorD=" + CorD + "&group_event_id=" + group_event_id + "&event_date=" + event_date + "&selected_row=" + selected_row;
			}
					
		</script>
	</head>

<?php
	//require_once("../comconnect.php");
	
	$group_event_id = 0;
	if(reqvar("group_event_id")) $group_event_id = reqvar("group_event_id");
	else if(sessvar_isset("showing_group_event_id")) $group_event_id = sessvar("showing_group_event_id");
	set_sessvar("showing_group_event_id", $group_event_id);

	$server_id = 0;
	if(reqvar("server_id")) $server_id = reqvar("server_id");
	else if(sessvar_isset("server_id")) $server_id = sessvar("server_id");
	set_sessvar("server_id",$server_id);

	$loc_id = 0;
	if(reqvar("loc_id")) $loc_id = reqvar("loc_id");
	else if(sessvar_isset("loc_id")) $loc_id = sessvar("loc_id");
	set_sessvar("loc_id", $loc_id);

	$dataname = "";
	if(reqvar("dn")) $dataname = reqvar("dn");
	else if(sessvar_isset("dataname")) $dataname = sessvar("dataname");
	set_sessvar("dataname", $dataname);
	
	$event_date = reqvar("event_date");
	$event_time = reqvar("event_time");
	
	$info_form = array();
	$info_form[] = array("Title", "title");
	$info_form[] = array("Duration", "duration");
	$info_form[] = array("Name", "cust_name");
	$info_form[] = array("Phone", "cust_phone");
	$info_form[] = array("Email", "cust_email");
	
	if (isset($_POST['save'])) {
		$info_form_save = array();
		$info_form_save = $info_form;
		$info_form_save[] = array("","item_id"); 
		$info_form_save[] = array("","notes"); 
		if ($group_event_id == "NEW") {
			$info_form_save[] = array("", "event_date"); 
			$info_form_save[] = array("", "event_time");
			$info_form_save[] = array("", "server_id");
			$info_form_save[] = array("", "loc_id");
			$info_form_save[] = array("", "track_id");
		}
		
	  $vals = array();
		$save_fields = "";
		$save_values = "";
		$update_fields = "SET ";
		for ($c = 0; $c < count($info_form_save); $c++) {
	  	$iname = $info_form_save[$c][1];
	  	$vals[$iname] = $_POST[$iname];
			$save_fields .= "`$iname`";
			$save_values .= "'[$iname]'";
			$update_fields .= "`$iname` = '[$iname]'";
			if ($c < (count($info_form_save) - 1)) {
				$save_fields .= ", ";
				$save_values .= ", ";
				$update_fields .= ", ";
			}
		}
	
		if ($group_event_id == "NEW") {
			$create_group_event = lavu_query("INSERT INTO `lk_group_events` (".$save_fields.", `created`) VALUES (".$save_values.", now())", $vals);
			$group_event_id = lavu_insert_id();
			$new_group_event = true;
		} else {
			$update_group_event = lavu_query("UPDATE `lk_group_events` ".$update_fields." WHERE `id` = '".$group_event_id."'", $vals);
		}
	}
	
	if (reqvar("CorD")) {
		
		$CorD = reqvar("CorD");
		if ($CorD == 1) {
			$cancel_group_event = lavu_query("UPDATE `lk_group_events` SET `cancelled` = '1' WHERE `id` = '[1]'", $group_event_id);
			$lk_log_this = lavu_query("INSERT INTO `lk_action_log` (`action`, `time`, `user_id`, `loc_id`, `group_event_id`) VALUES ('Deleted group event', now(), '[1]', '[2]', '[3]')", $server_id, $loc_id, $group_event_id);
		} else if ($CorD == 2) {
			$delete_group_event = lavu_query("UPDATE `lk_group_events` SET `_deleted` = '1' WHERE `id` = '[1]'", $group_event_id);
			$lk_log_this = lavu_query("INSERT INTO `lk_action_log` (`action`, `time`, `user_id`, `loc_id`, `group_event_id`) VALUES ('Cancelled group event', now(), '[1]', '[2]', '[3]')", $server_id, $loc_id, $group_event_id);
		}
		$group_event_id = "NEW";
		set_sessvar("showing_group_event_id", $group_event_id);
	}

?>
	<body>
		<table width="440" border="0" cellspacing="0" cellpadding="0">
			<?php			
				$get_group_event_info = lavu_query("SELECT * FROM `lk_group_events` WHERE `id` = '".$group_event_id."'");
				if ((lysql_num_rows($get_group_event_info) > 0) || (($group_event_id == "NEW") && ($event_date != ""))) {
					if (lysql_num_rows($get_group_event_info) > 0) {
						$extract = lysql_fetch_array($get_group_event_info);
					}
					if ($group_event_id == "NEW") {
						$form_title = "New Group Event: ";
						$show_event_time = $event_time;
					} else {
						$form_title = "Group Event: ";
						$show_event_time = str_replace(":","",$extract['event_time']);
					}
					echo "<tr><td colspan='2' class='title' align='center' style='padding:10px 0px 10px 0px'>".$form_title;
					if(isset($event_date) && $event_date!="")
						echo display_date($event_date) . " " . display_time($show_event_time);

					$loc_query = lavu_query("select * from `locations` where `id`='[1]'",$loc_id);
					$loc_read = lysql_fetch_assoc($loc_query);
					$location_menu = $loc_read['menu_id'];
					echo "</tr>
					<form name='info_form' action='group_event_info.php' method='POST'>
						<tr>
							<td width='127' height='46' valign='middle'>
								<table width='127' border='0' cellspacing='0' cellpadding='8'>
									<tr><td align='right' class='subtitles'>Event Type</td></tr>
								</table>
							</td>
							<td width='313' style='background:URL(images/tab_bg2.png); background-repeat:no-repeat;'>
								<table width='313' border='0' cellspacing='0' cellpadding='8'>
									<tr>
										<td class='nametext'>
											<select name='item_id' class='nametext' style='border:none; background: URL(win_overlay.png);'>";
					$get_group_event_items = lavu_query("SELECT * from `menu_items` where `menu_items`.`_deleted` = '0' AND `menu_items`.`hidden_value2` = 'group event' ORDER BY `menu_items`.`name` ASC");//`menu_items`.`id` as id, `menu_items`.`item_id` as `item_id`, `menu_items`.`name` as name FROM `menu_items` LEFT JOIN `menu_categories` ON `menu_items`.`category_id`=`menu_categories`.`id` LEFT JOIN `menu_groups` ON `menu_categories`.`group_id`=`menu_groups`.`id` WHERE `menu_groups`.`menu_id`='$location_menu' AND `menu_items`.`_deleted` = '0' AND `menu_items`.`hidden_value2` = 'group event' ORDER BY `menu_items`.`name` ASC");
					if (lysql_num_rows($get_group_event_items) > 0) {
						while ($extract_gei = lysql_fetch_array($get_group_event_items)) {
							$selected = "";
							if ($extract_gei['id'] == $extract['item_id']) {
								$selected = " selected";
							}
							echo "<option value='".$extract_gei['id']."'".$selected.">".$extract_gei['name']."</option>";
						}
					} else {
						echo "<option value='0'>Error loading group event items...</option>";
					}
					echo "</select>
										</td>
									</tr>
								</table>
							</td>
						</tr>";

					foreach ($info_form as $info) {
						echo "<tr>
							<td width='127' height='46'>
								<table width='127' border='0' cellspacing='0' cellpadding='8'>
									<tr><td align='right' class='subtitles'>".$info[0]."</td></tr>
								</table>
							</td>
							<td width='313' style='background:URL(images/tab_bg2.png); background-repeat:no-repeat;'>
								<table width='313' border='0' cellspacing='0' cellpadding='8'>
									<tr><td class='nametext'><input type=\"text\" name=\"".$info[1]."\" class=\"nametext\" style=\"border:none; background: URL(win_overlay.png);\" size=\"30\" value=\"".(isset($extract[$info[1]])?$extract[$info[1]]:"")."\" onchange=\"document.getElementById('update_btn').style.display = 'inline';\"></td></tr>
								</table>
							</td>
						</tr>";
					}
					echo "<tr>
							<td width='127' height='150' valign='top'>
								<table width='127' border='0' cellspacing='0' cellpadding='8'>
									<tr><td align='right' class='subtitles'>Notes</td></tr>
								</table>
							</td>
							<td width='313' style='background:URL(images/tab_bg2_area.png); background-repeat:no-repeat;'>
								<table width='313' border='0' cellspacing='0' cellpadding='8'>
									<tr><td class='nametext'><textarea name=\"notes\" class=\"nametext\" style=\"border:none; background: URL(win_overlay.png);\" rows=\"7\" cols=\"30\" onchange=\"document.getElementById('update_btn').style.display = 'inline';\" />".(isset($extract['notes'])?$extract['notes']:"")."</textarea></td></tr>
								</table>
							</td>
						</tr>
						<tr><td>&nbsp;</td></tr>";
					$submit_button = "btn_update.png";
					$submit_display = " style='display:none'";
					if ($group_event_id == "NEW") {
						$submit_button = "btn_submit.png";
						$submit_display = "";
						echo "<input type='hidden' name='loc_id' value='".$loc_id."'>
						<input type='hidden' name='track_id' value='".sessvar("trackid")."'>
						";
					}
					if (!strstr($event_time, ":")) {
						$event_time = substr($event_time, 0, 2).":".substr($event_time, 2, 2);
					}
					echo "<tr>
							<td align='center' valign='middle' colspan='2'>
								<table cellspacing='0' cellpadding='0'>
									<tr id='update_btn'".$submit_display.">
										<td align='center' valign='middle' colspan='2'>
											<table cellspacing='0' cellpadding='0' width='430px'>
												<tr><td align='center'><img src='images/".$submit_button."' width='120px' onclick='document.info_form.submit();'></td></tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<input type='hidden' name='event_date' value='".$event_date."'>
						<input type='hidden' name='event_time' value='".$event_time."'>
						<input type='hidden' name='server_id' value='".$server_id."'>
						<input type='hidden' name='save' value='1'>
						<input type='hidden' name='group_event_id' value='".$group_event_id."'>
						<input type='hidden' name='selected_row' value='".reqvar("selected_row")."'>
						<tr><td>&nbsp;</td></tr>";
					if ($group_event_id != "NEW") { ?>
						<tr><td align='center' colspan='2'><a href='_DO:cmd=confirm&title=Cancel Group Event&message=cancel this group event&c_name=group_event_info&js_cmd=cancel_or_delete("1","<?php echo $group_event_id; ?>","<?php echo $event_date; ?>","<?php echo reqvar("selected_row"); ?>")'><img src='images/btn_cancel.png' height='30px' border='0'></a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href='_DO:cmd=confirm&title=Delete Group Event&message=delete this group event&c_name=group_event_info&js_cmd=cancel_or_delete("2","<?php echo $group_event_id; ?>","<?php echo $event_date; ?>","<?php echo reqvar("selected_row"); ?>")'><img src='images/btn_delete.png' height='30px' border='0'></a></td></tr>
						<tr><td>&nbsp;</td></tr>
					<?php }
					echo "</form>";
					
					/*if (is_file("companies/".$dataname."/signatures/signature_".$racer_id.".jpg")) {
						$display_signature = "<img src='companies/".$dataname."/signatures/signature_".$racer_id.".jpg' width='380'>";
					} else {
						$display_signature = "<span class='subtitles'>No signature image on record.</span>";
					}
					echo "<tr>
						<td align='center' colspan='2'>
							<table cellspacing='0' cellpadding='0'>
								<tr>
									<td width='50'>&nbsp;</td>
									<td width='400'>
										<table width='400' cellspacing='0' cellpadding='0'>
											<tr><td class='bg_signature_top'></td></tr>
											<tr><td class='bg_signature_mid' align='center'>".$display_signature."</td></tr>
											<tr><td class='bg_signature_bottom'></td></tr>
										</table>
									</td>									
								</tr>
							</table>
						</td>
					</tr>";*/
					
				} else {
					echo "<tr><td align='center'><font style='font-size:24px; color:#999999; font-family:Verdana,Arial'><br>No record selected...</font></td></tr>";
				}
			?>
			<tr><td>&nbsp;</td></tr>
		</table>
		<?php
			if (isset($_POST['save'])) {
				$date_parts = explode("-", $event_date);
				if ($new_group_event) {
					echo "<script language='javascript'>location = '_DO:cmd=alert&title=Group Event Created&c_name=group_events&f_name=lavukart/group_events.php?year=".$date_parts[0]."&month=".$date_parts[1]."&day=".$date_parts[2]."&selected_row=".reqvar("selected_row")."'</script>";
				} else {
					echo "<script language='javascript'>location = '_DO:cmd=alert&title=Update Successful&c_name=group_events&f_name=lavukart/group_events.php?year=".$date_parts[0]."&month=".$date_parts[1]."&day=".$date_parts[2]."&selected_row=".reqvar("selected_row")."';</script>";
				}
			}
			if (reqvar("CorD")) {
				$date_parts = explode("-", $event_date);
				$CorD = reqvar("CorD");
				if ($CorD == 1) {
					echo "<script language='javascript'>location = '_DO:cmd=alert&title=Group Event Cancelled&c_name=group_events&f_name=lavukart/group_events.php?year=".$date_parts[0]."&month=".$date_parts[1]."&day=".$date_parts[2]."&selected_row=".reqvar("selected_row")."'</script>";
				} else if ($CorD == 2) {
					echo "<script language='javascript'>location = '_DO:cmd=alert&title=Group Event Deleted&c_name=group_events&f_name=lavukart/group_events.php?year=".$date_parts[0]."&month=".$date_parts[1]."&day=".$date_parts[2]."&selected_row=".reqvar("selected_row")."';</script>";
				}
			}
		?>
</body>
</html>

<?php

} else if($uripage =="group_event_info_wrapper.php") {
//--------------------------------------------------- group_event_info_wrapper.php ----------------------------------------------//
?>

<html>
	<head>
		<title>Group Event Info Wrapper</title>
		<style type="text/css">
<!--
body {
	background-color: transparent;
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}

.creditnumber {font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #3d4f6d;
}
.gridposition {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #5A75A0;
}
.icontext {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 11px;
	font-weight: bold;
	color: #5A75A0;
}
.nametext {font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
}
.subtitles {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #5A75A0;
}
-->
		</style>
	</head>

	<body>
		<table width="474" height="598" border="0" cellpadding="0" cellspacing="0" >
			<tr>
				<td width="9">&nbsp;</td>
				<td align="center" valign="top">
					<table width="451" height="598" border="0" cellspacing="0" cellpadding="0">
						<tr><td height="23"><img src="images/line_top.png" width="451" height="23"></td></tr>
						<tr><td height="552" align="left" valign="top">&nbsp;</td></tr>
						<tr><td height="23"><img src="images/line_bot.png" width="451" height="23"></td></tr>
					</table>
				</td>
      </tr>
    </table>
	</body>
</html>

<?php

} else if($uripage =="group_event_input.php") {
//--------------------------------------------------- group_event_input.php ----------------------------------------------//
?>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Untitled Document</title>

		<style type="text/css">
<!--

body {
	background-color: transparent;
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}

.style4 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #8896AD;
}
.style6 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 24px;
	font-weight: bold;
	color: #8896AD;
}
.style8 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 15px;
	font-weight: bold;
	color: #07142a;
}
.creditnumber {
	font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #3d4f6d;
}
.nametext {
	font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
}
.nametext_grey {
	font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #777777;
}
.subtitles {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #5A75A0;
}
.info_text1 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 16px;
	color: #233756;
}

-->
		</style>
		<script language="javascript">
			var event_date = "";
			var event_time = "";
		</script>
	</head>

<?php 
	//require_once(dirname(__FILE__) . "/../comconnect.php"); 
	$cm = reqvar("cm");
	$tab_name = reqvar("tab_name");
	$mode= reqvar("mode");
	
	$loc_id = 0;
	if(reqvar("loc_id")) $loc_id = reqvar("loc_id");
	else if(sessvar_isset("loc_id")) $loc_id = sessvar("loc_id");
	set_sessvar("loc_id", $loc_id);
	
	$server_id = 0;
	if(reqvar("server_id")) $server_id = reqvar("server_id");
	else if(sessvar_isset("server_id")) $server_id = sessvar("server_id");
	set_sessvar("server_id",$server_id);

	$track_id = 0;
	if(reqvar("track_id")) $track_id = reqvar("track_id");
	else if(sessvar_isset("track_id")) $track_id = sessvar("track_id");
	set_sessvar("track_id", $track_id);

	$info_form = array();
	$info_form[] = array("Title","title");
	$info_form[] = array("Name","cust_name");
	$info_form[] = array("Phone","cust_phone");
	$info_form[] = array("Email","cust_email");

	$year = reqvar("year");
	$month = reqvar("month");
	$day = reqvar("day");
	$time = reqvar("time");
	$time_array = explode(":", $time);

	if ($mode == "") {
	
		$display = "<tr><td height='8'></td></tr>
		<tr><td width='423' height='349' align='center' style='background:URL(images/inner_cal_bg.png); background-repeat:no-repeat;'><iframe frameborder='0' scrolling='no' width='418' height='550' src='inner_calendar.php' allowtransparency='true'></iframe></td></tr>";

	} else if ($mode == "get_info") {
			
		$display = "<tr><td align='center' style='padding:10px 0px 8px 0px;'><span class='subtitles'>Date:</span> <span class='info_text1'>$year-$month-$day</span> &nbsp;&nbsp; <span class='subtitles'>Time:</span> <span class='info_text1'>".date("g:ia", mktime($time_array[0], $time_array[1], 0, $month, $day, $year))."</span></td></tr>
		<tr>
			<td align='center'>
				<form name='info_form' action='group_event_input.php' method='post'>
					<input type='hidden' name='mode' value='submit'>
					<input type='hidden' name='event_date' value='$year-$month-$day'>
					<input type='hidden' name='event_time' value='$time'>
					<table width='425' cellspacing='0' cellpadding='0'>";
		foreach ($info_form as $info) {
			$display .= "<tr>
							<td width='100' height='46'>
								<table width='100' border='0' cellspacing='0' cellpadding='8'>
									<tr><td align='right' class='subtitles'>".$info[0]."</td></tr>
								</table>
							</td>
							<td width='313' style='background:URL(images/tab_bg2.png); background-repeat:no-repeat;'>
								<table width='313' border='0' cellspacing='0' cellpadding='8'>
									<tr><td class='nametext'><input type='text' name='".$info[1]."' class='nametext' style='border:none; background: URL(win_overlay.png);' size='30'></td></tr>
								</table>
							</td>
						</tr>";
		}
		$display .= "<tr>
							<td width='100' height='150' valign='top'>
								<table width='100' border='0' cellspacing='0' cellpadding='8'>
									<tr><td align='right' class='subtitles'>Notes</td></tr>
								</table>
							</td>
							<td width='313' style='background:URL(images/tab_bg2_area.png); background-repeat:no-repeat;'>
								<table width='313' border='0' cellspacing='0' cellpadding='8'>
									<tr><td class='nametext'><textarea name='notes' class='nametext' style='border:none; background: URL(win_overlay.png);' rows='7' cols='30' /></textarea></td></tr>
								</table>
							</td>
						</tr>
						<tr><td align='center' valign='middle' colspan='2' style='padding:20px 0px 20px 0px'><img src='images/btn_submit.png' width='120px' onclick='document.info_form.submit();'></td></tr>
					</table>
				</form>
			</td>
		</tr>";

	} else if ($mode == "submit") {
	
		$create_event = lysql_query("INSERT INTO `lk_group_events` (`title`, `event_date`, `event_time`, `duration`, `cust_name`, `cust_phone`, `cust_email`, `notes`, `loc_id`, `server_id`, `created`, `track_id`, `checked_out`) VALUES ('".str_replace("'", "''", $_POST['title'])."', '".$_POST['event_date']."', '".$_POST['event_time']."', '30', '".str_replace("'", "''", $_POST['cust_name'])."', '".$_POST['cust_phone']."', '".$_POST['cust_email']."', '".str_replace("'", "''", $_POST['notes'])."', '".$loc_id."', '".$server_id."', now(), '".$track_id."', '0')");
		$new_event_id = lysql_insert_id();

		$lk_log_this = lysql_query("INSERT INTO `lk_action_log` (`action`, `customer_id`, `order_id`, `time`, `user_id`, `credits`, `loc_id`, `group_event_id`) VALUES ('Group Event Scheduled: ".str_replace("'", "''", $_POST['title'])."', '0', '0', now(), '".$server_id."', '0', '".$loc_id."', '".$new_event_id."')");
		
		$time_array = explode(":", $_POST['event_time']);
			
		header("Location: _DO:cmd=event_info&customer_name=".$_POST['cust_name']."&event_id=".$new_event_id."&event_date=".$_POST['event_date']."&event_time=".date("g:00a", mktime($time_array[0], $time_array[1], 0, $month, $day, $year)));
		exit;
	}

?>
	<body>
		<table width="426" border="0" cellspacing="0" cellpadding="0">
			<?php echo $display; ?>
		</table>
	</body>
</html>

<?php

} else if($uripage =="group_event_input_overlay_wrapper.php") {
//--------------------------------------------------- group_event_input_overlay_wrapper.php ----------------------------------------------//
?>

<?php
	//require_once(dirname(__FILE__) . "/../comconnect.php"); 
	$tab_name = reqvar("tab_name");
?>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Untitled Document</title>

		<style type="text/css">
<!--
body {
	background-color: transparent;
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}

.style4 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #8896AD;
}
.style6 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 24px;
	font-weight: bold;
	color: #8896AD;
}
.style8 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 15px;
	font-weight: bold;
	color: #07142a;
}
.overlay_title {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 20px;
	color: #d4dae4;
}

-->
		</style>
	</head>

<?php
	//require_once("../comconnect.php");

	$cancel_cmd = "cancel";
	$extra_vars = "&cm=1&cmd=".reqvar("cmd");
	$bg = "images/win_overlay_no_btns.png";
?>
	<body>
		<table width="474" height="598" border="0" cellpadding="0" cellspacing="0" style="background:URL(<?php echo $bg?>); background-repeat:no-repeat; background-position:top center;">
			<tr>
				<td width="26" height="53">&nbsp;</td>
				<td width="422" height="53">
					<table cellspacing="0" cellspacing="0">
						<tr>
							<td width="63" height="47" align="center" style="background:URL(images/btn_soft.png); background-repeat:no-repeat;" onClick="location = '_DO:cmd=<?php echo $cancel_cmd?>';"><span class="style6">X</span></td>
							<td width="359" align="center" valign="middle"><span class='overlay_title'>Group Event Info</span></td>
						</tr>
					</table>
				</td>
				<td width="26" height="53">&nbsp;</td>
			</tr>
			<tr>
				<td width="26" height="536">&nbsp;</td>
				<td width="422" height="536">&nbsp;</td>
				<td width="26" height="536">&nbsp;</td>
			</tr>
			<tr>
				<td width="26" height="10"></td>
				<td width="422" height="10"></td>
				<td width="26" height="10"></td>
			</tr>
		</table>
	</body>
</html>

<?php

} else if($uripage =="group_events.php") {
//--------------------------------------------------- group_events.php ----------------------------------------------//
?>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Group Events</title>

		<style type="text/css">
<!--
body {
	background-color: transparent;
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}

.creditnumber {font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #3d4f6d;
}
.nametext {font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
}
.active_nametext {font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #00aa00;
}
.finished_nametext {font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color:#999999;
}
.subtitles {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #5A75A0;
}
.style411 {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #5A75A0;
}
.gridposition {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #5A75A0;
}
.icontext {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 11px;
	font-weight: bold;
	color: #5A75A0;
}
.subtitles1 {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-weight: bold;
	color: #5A75A0;
}
.title1 {	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 18px;
	font-weight: bold;
	color: #5A75A0;
}
-->
		</style>
	</head>

<?php
	//require_once("../comconnect.php");
	//ini_set("display_errors","1");
	/*if(reqvar("eventid")) $eventid = reqvar("eventid");
	else if(sessvar_isset("eventid")) $eventid = sessvar("eventid");
	set_sessvar("eventid",$eventid);*/
	$group_event_id = sessvar("group_event_id");
	
	$row_selected = reqvar("selected_row", 0);
	
	$set_scrolltop = reqvar("set_scrolltop");
	$mode = reqvar("mode");
	if($mode=="add_event")
	{
		$set_date = reqvar("event_date");
		$t = reqvar("event_time");
		$set_type = reqvar("event_type");
		$set_locationid = reqvar("event_locationid");
		$set_trackid = reqvar("event_trackid");
		
		$dparts = explode("-",$set_date);
		$set_year = $dparts[0];
		$set_month = $dparts[1];
		$set_day = $dparts[2];
		$set_min = $t % 100;
		$set_hour = (($t - $set_min) / 100);
		$set_time = $t;
		$set_ts = mktime($set_hour,$set_min,0,$set_month,$set_day,$set_year);

		lavu_query("insert into `lk_events` (`locationid`,`type`,`date`,`time`,`ts`,`trackid`) values ('$set_locationid','$set_type','$set_date','$set_time','$set_ts','$set_trackid')");
		//echo "insert into `lk_events` (`locationid`,`type`,`date`,`time`,`ts`,`trackid`) values ('$set_locationid','$set_type','$set_date','$set_time','$set_ts','$set_trackid')";
	}
	else if($mode=="move_event")
	{
		$mtvars = array();
		$mtvars['date'] = reqvar("moveto_date");
		$mtvars['time'] = reqvar("moveto_slot");
		$mtvars['id'] = $eventid;//reqvar("eventid");

		$t = $mtvars['time'];
		$dparts = explode("-",$mtvars['date']);
		$set_year = $dparts[0];
		$set_month = $dparts[1];
		$set_day = $dparts[2];
		$set_min = $t % 100;
		$set_hour = (($t - $set_min) / 100);
		$set_time = $t;
		$set_ts = mktime($set_hour * 1,$set_min * 1,0,$set_month * 1,$set_day * 1,$set_year * 1);
		$mtvars['ts'] = $set_ts;
		
		$trackid = sessvar("trackid");
		$track_query = lavu_query("select * from `lk_tracks` where id='[1]'",$trackid);
		if(lysql_num_rows($track_query))
		{
			$track_read = lysql_fetch_assoc($track_query);
			$time_span = $track_read['time_interval'];
			
			$move_events_queue = array();
			$slot_clear = false;
			$ctime = $mtvars['time'];
			$cdate = $mtvars['date'];
			
			$set_min = $ctime % 100;
			$set_hour = (($ctime - $set_min) / 100);
			$cts = mktime($set_hour * 1,$set_min * 1,0,$set_month * 1,$set_day * 1,$set_year * 1);
			$loopcount = 0;
						
			while($slot_clear==false && $loopcount < 300)
			{			
				$e_query = lavu_query("select * from `lk_events` where `trackid`='[1]' and `ts`='[2]' and `id`!='[3]'",$trackid,$cts,$eventid);
				if(lysql_num_rows($e_query))
				{
					$ctime = time_advance($ctime,$time_span);
					$set_min = $ctime % 100;
					$set_hour = (($ctime - $set_min) / 100);
					$cts = mktime($set_hour * 1,$set_min * 1,0,$set_month * 1,$set_day * 1,$set_year * 1);
					while($e_read = lysql_fetch_assoc($e_query))
					{
						$move_events_queue[] = array($e_read['id'],$cdate,$ctime,$cts);
					}
				}
				else
				{
					$slot_clear = true;
				}
				$loopcount++;
			}
			for($n=0; $n<count($move_events_queue); $n++)
			{
				$cvars['id'] = $move_events_queue[$n][0];
				$cvars['date'] = $move_events_queue[$n][1];
				$cvars['time'] = $move_events_queue[$n][2];
				$cvars['ts'] = $move_events_queue[$n][3];
				//mail("corey@poslavu.com","forward: " . $move_events_queue[$n][0],"date: " . $move_events_queue[$n][1] . " time: " . $move_events_queue[$n][2] . " ts: " . $move_events_queue[$n][3],"From: info@poslavu.com");
				lavu_query("update `lk_events` set `date`='[date]', `time`='[time]', `ts`='[ts]' where `id`='[id]",$cvars);
			}
			
			lavu_query("update `lk_events` set `date`='[date]', `time`='[time]', `ts`='[ts]' where `id`='[id]'",$mtvars);
		}
	}
	else if($mode=="remove_event")
	{
		$event_query = lavu_query("select * from `lk_events` where id='[1]'",$eventid);
		if(lysql_num_rows($event_query))
		{
			$event_read = lysql_fetch_assoc($event_query);
			$date_parts = explode("-",$event_read['date']);
			if(count($date_parts) > 2)
			{
				$year = $date_parts[0];
				$month = $date_parts[1];
				$day = $date_parts[2];
			}
			lavu_query("delete from `lk_events` where `id`='[1]'",$eventid);
		}
	}
	else if($mode=="set_track")
	{
		$trackid = reqvar("set_trackid");
		set_sessvar("trackid", $trackid);
	}
	
	if(sessvar_isset("trackid"))
		$trackid = sessvar("trackid");
	else
		$trackid = false;
?>

	<body>
		<table width="395" height="535" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td height="535" align="left" valign="top">
					<table width="382" border="0" cellspacing="0" cellpadding="0">
             <?php
							$minrows = 10;
							$rowcount = 0;
							
							if(postvar("day")==false && $group_event_id)
							{
								$event_query = lavu_query("SELECT * FROM `lk_group_events` WHERE id = '[1]' AND `cancelled` != '1' AND `_deleted` != '1'", $group_event_id);
								if(lysql_num_rows($event_query))
								{
									$event_read = lysql_fetch_assoc($event_query);
									$date_parts = explode("-",$event_read['event_date']);
									if(count($date_parts) > 2)
									{
										$year = $date_parts[0];
										$month = $date_parts[1];
										$day = $date_parts[2];
									}
								}
								else
								{
									$year = postvar("year",date("Y"));
									$month = postvar("month",date("m"));
									$day = postvar("day",date("d"));
								}
							}
							else
							{
								$year = postvar("year",date("Y"));
								$month = postvar("month",date("m"));
								$day = postvar("day",date("d"));
							}
							if($month < 10) $month = "0" . ($month * 1);
							if($day < 10) $day = "0" . ($day * 1);
							$fulldate = $year . "-" . $month . "-" . $day;
							$fulldate_no_lead = $year . "-" . ($month * 1) . "-" . ($day * 1);
							//echo $fulldate;

							function create_timeslot_row($rowcount, $date, $slot, $trackid ,$locationid, $group_event_id = false, $unavailable = false)
							{
								$tab_bg = "tab_bg2.png";
								$onclick = " onclick=\"if(row_selected) document.getElementById('event_row_' + row_selected).style.background = 'URL(images/tab_bg2.png)'; row_selected = '".$rowcount."'; this.style.background = 'URL(images/tab_bg2_lit.png)'; window.location = '_DO:cmd=load_url&c_name=group_event_info&f_name=lavukart/group_event_info.php?group_event_id=NEW&event_date=".$date."&event_time=".$slot."&selected_row=".$rowcount."&abc=123';\"";
								$display_unavailable = "";
							
								if ($unavailable) {
									$tab_bg = "tab_bg2_gray.png";
									$onclick = "";
									$display_unavailable = "<font color='#999999'>Unavailable</font>";
								}
								?>
								<tr>
									<td height="46">
										<table width="79" border="0" cellspacing="0" cellpadding="8">
											<tr>
												<td class="subtitles1"><nobr>
													<?php
														if ($group_event_id)
														{
															echo "<a onclick='if(confirm(\"Are you sure you want to move this event?\")) location = \"_DO:cmd=load_url&c_name=group_events&f_name=lavukart/group_events.php?mode=move_event&group_event_id=".$group_event_id."&moveto_date=".$date."&moveto_slot=".$slot."&abc=123\"; '>".display_time($slot)."</a>"; 
														}
														else
														{
															echo display_time($slot);
														}
													?>
												</nobr></td> 
											</tr>
										</table>
									</td>
									<td width="313" id="event_row_<?php echo $rowcount; ?>" style="background: URL(images/<?php echo $tab_bg; ?>);"<?php echo $onclick; ?>>
										<table width="313" border="0" cellspacing="0" cellpadding="8">
											<td class="nametext"><?php echo $display_unavailable; ?></td>
										</table>
									</td>
								</tr>
								<?php
							}
							
							$locationid = reqvar("loc_id");
							
							if ($trackid)
							{
								$track_query = lavu_query("select * from `lk_tracks` where id='[1]' and `locationid`='[2]'", $trackid, $locationid);
								if(!lysql_num_rows($track_query))
									$trackid = false;
							}
							
							if(!$trackid)
							{
								$track_query = lavu_query("select * from `lk_tracks` where `locationid`='$locationid' and `_deleted`!='1' order by id asc limit 1");
							}
							if(!lysql_num_rows($track_query))
							{
								echo "Error: No Tracks exist for this location";
								exit();
							}  
							
							$track_read = lysql_fetch_assoc($track_query);
							$time_start = $track_read['time_start'];
							$time_end = $track_read['time_end'];
							$time_span = 30;
							$trackid = $track_read['id'];
							$track_title = $track_read['title'];
							set_sessvar("trackid",$trackid);
							
							if(!is_numeric($time_start)) $time_start = 1000;
							if(!is_numeric($time_end)) $time_end = 2200;
														
							echo "<tr><td><td align='center' class='subtitles1'><b>$month/$day/$year</b> - <b>$track_title</b></td></tr>";
							//if($year==date("Y") && $month==date("m") && $day==date("d"))
							//{
								//$mints = current_ts(-60); //mktime($hour - 1,$minute,0,$month,$day,$year);
							//}
							//else
								$mints = mktime(0,0,0,$month*1,$day*1,$year*1);
							$maxts = mktime(24,0,0,$month*1,$day*1,$year*1);
							
							$current_time = $time_start;
							function time_advance($time, $add)
							{
								$time_hours = $time - ($time % 100);
								$time_minutes = $time % 100;
								$add_hours = $add - ($add % 100);
								$add_minutes = $add % 100;
								$hours = $time_hours + $add_hours;
								$minutes = $time_minutes + $add_minutes;
								if($minutes >= 60)
								{
									$hours += 100;
									$minutes -= 60;
								}
								return $hours + $minutes;
							}
	
							$last_event_slots = 0;
							//echo "SELECT * FROM `lk_group_events` WHERE `loc_id` = '$locationid' AND `track_id` = '$trackid' AND `event_date` = '$fulldate' AND `cancelled` != '1' AND `_deleted` != '1' ORDER BY `event_time` ASC<br>";
							//echo $locationid . "<br>" . $trackid . "<br>" . $fulldate;
							$event_query = lavu_query("SELECT * FROM `lk_group_events` WHERE `loc_id` = '[1]' AND `track_id` = '[2]' AND (`event_date` = '[3]' or `event_date`='[4]') AND `cancelled` != '1' AND `_deleted` != '1' ORDER BY (REPLACE(`event_time`,':','') * 1) ASC", $locationid, $trackid, $fulldate, $fulldate_no_lead);
							//$event_query = lavu_query("SELECT * FROM `lk_group_events` WHERE `loc_id` = '[1]' AND `event_date` = '[3]' AND `cancelled` != '1' AND `_deleted` != '1' ORDER BY `event_time` ASC", $locationid, $trackid, $fulldate);
							while($event_read = lysql_fetch_assoc($event_query))
							{
								//echo "event found<br>";
								while($current_time < str_replace(":", "", $event_read['event_time']))
								{
									if ($last_event_slots > 0) {
										$last_event_slots--;
										$unavailable = true;
									} else {
										$unavailable = false;
									}
									$rowcount++;
									if($current_time >= date("Hi",$mints)) {
										create_timeslot_row($rowcount, $fulldate, $current_time, $trackid, $locationid, $group_event_id, $unavailable);
									}
									$current_time = time_advance($current_time, $time_span);
								}
								if($current_time < time_advance(str_replace(":", "", $event_read['event_time']), $time_span))
									$current_time = time_advance($current_time, $time_span);
								$rowcount++;

								$unavailable = false;
								?>
								<tr>
									<td width="69" height="46">
										<table width="69" border="0" cellspacing="0" cellpadding="8">
											<tr><td class="subtitles1"><nobr>
											<?php
												/*if($eventid)
												{
													echo "<a onclick='if(confirm(\"Are you sure you want to move this event?\")) location = \"_DO:cmd=load_url&c_name=events&f_name=lavukart/events.php?mode=move_event&eventid=".$eventid."&moveto_date=".$fulldate."&moveto_slot=".$event_read['time']."&abc=123\"; '>".display_time($event_read['time'])."</a>"; 
												}
												else
												{ */
													echo display_time(str_replace(":", "", $event_read['event_time'])); 
												//}
											?>
											</nobr></td></tr>
										</table>
									</td>
									<?php
										$start_style = "";
										$use_bg = "images/tab_bg2.png";
										if (($group_event_id == $event_read['id']) || ($row_selected == $rowcount))
										{
											$use_bg = "images/tab_bg2_lit.png";
											$row_selected = $rowcount;
										}
										
										$comp_cmd = "location = \"_DO:cmd=load_url&c_name=group_event_info&f_name=lavukart/group_event_info.php?group_event_id=".$event_read['id']."&event_date=".$fulldate."&selected_row=".$rowcount."&abc=123\";";
										$tdclass = "nametext";
										$last_event_slots = (($event_read['duration'] / 30) - 1);
									?>
									<td width="313" id="event_row_<?php echo $rowcount; ?>" style="background: URL(<?php echo $use_bg; ?>);" onclick='if(row_selected) document.getElementById("event_row_" + row_selected).style.background = "URL(images/tab_bg2.png)"; row_selected = "<?php echo $rowcount?>"; this.style.background = "URL(images/tab_bg2_lit.png)";<?php echo $comp_cmd; ?>'>
										<table width="313" border="0" cellspacing="0" cellpadding="8">
											<tr><td class="<?php echo $tdclass; ?>"><?php echo ucfirst($event_read['title']); ?></td></tr>
										</table>
									</td>
								</tr>
                                <?php
							}
							while($current_time < $time_end)
							{
								if ($last_event_slots > 0) {
									$last_event_slots--;
									$unavailable = true;
								} else {
									$unavailable = false;
								}
								$rowcount++;
								if($current_time >= date("Hi",$mints))
									create_timeslot_row($rowcount, $fulldate, $current_time, $trackid, $locationid, $group_event_id, $unavailable);
								$current_time = time_advance($current_time, $time_span);
							}
							$rowcount++;
							?>
					</table>
					<script language='javascript'>row_selected = <?php echo $row_selected?>;</script>
				</td>
			</tr>
		</table>
		<?php if($set_scrolltop && $set_scrolltop!="")
			echo "<script language='javascript'>window.scrollTo(0,$set_scrolltop);</script>";
		?>
	</body>
</html>
<?php

} else if($uripage =="group_events_wrapper.php") {
//--------------------------------------------------- group_events_wrapper.php ----------------------------------------------//
?>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Group Events Wrapper</title>

		<style type="text/css">
<!--
body {
	background-color: transparent;
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}

.creditnumber {font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #3d4f6d;
}
.nametext {font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
}
.subtitles {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #5A75A0;
}
.style411 {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #5A75A0;
}
.gridposition {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #5A75A0;
}
.icontext {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 11px;
	font-weight: bold;
	color: #5A75A0;
}
.subtitles1 {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #5A75A0;
}
.title1 {	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 18px;
	font-weight: bold;
	color: #5A75A0;
}
-->
		</style>
	</head>

	<body>
		<table width="474" height="598" border="0" cellpadding="0" cellspacing="0" >
			<tr>
				<td width="77" height="536" align="center" valign="top">
					<table width="68" border="0" cellpadding="0" cellspacing="0">
						<tr><td height="120" align="center" valign="middle" class="subtitles">&nbsp;</td></tr>
						<tr><td height="32" align="center" valign="middle" class="subtitles"><a href="_DO:cmd=create_overlay&f_name=lavukart/calendar_wrapper.php;lavukart/calendar.php&c_name=calendar_wrapper;calendar&dims=112,200,423,374;112,200,423,374"><img src="images/btn_calendar.png" width="68" height="68" border="0"/></a></td></tr>
						<tr><td height="32" align="center" valign="middle"><span class="icontext">CALENDAR</span></span></td></tr>
					</table>
				</td>
				<td align="left" valign="top">
					<table width="395" height="598" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td height="40">
								<table width="392" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td width="79" height="46">
											<table width="79" border="0" cellspacing="0" cellpadding="8">
												<tr><td width="79" class="title1">TIME</td></tr>
											</table>
										</td>
										<td width="313" >
											<table width="313" border="0" cellpadding="8" cellspacing="0">
												<tr><td align="center" class="title1">EVENT</td></tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td height="535" align="left" valign="top">
								<table cellspacing="0" cellpadding="0">
									<tr>
										<td><img src="images/line.png" width="1" height="535"></td>
									</tr>
								</table>
							</td>
						</tr>
						<tr><td height="23" background="images/line_bot.png"></td></tr>
					</table>
				</td>
			</tr>
		</table>
	</body>
</html>
<?php

} else if($uripage =="index.php") {
//--------------------------------------------------- index.php ----------------------------------------------//
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
</body>
</html>

<?php

} else if($uripage =="inner_calendar.php") {
//--------------------------------------------------- inner_calendar.php ----------------------------------------------//
?>

<?php
	//require_once("../comconnect.php");
	
	$server_id = 0;
	if(reqvar("server_id")) $server_id = reqvar("server_id");
	else if(sessvar_isset("server_id")) $server_id = sessvar("server_id");
	set_sessvar("server_id",$server_id);

	$loc_id = 0;
	if(reqvar("loc_id")) $loc_id = reqvar("loc_id");
	else if(sessvar_isset("loc_id")) $loc_id = sessvar("loc_id");
	set_sessvar("loc_id", $loc_id);

	$track_id = 0;
	if(reqvar("track_id")) $track_id = reqvar("track_id");
	else if(sessvar_isset("track_id")) $track_id = sessvar("track_id");
	set_sessvar("track_id", $track_id);

	if ($track_id == 0) {
		$get_a_track_id = lavu_query("SELECT * FROM `lk_tracks` WHERE `locationid` = '[1]' AND `_deleted` != '1' ORDER BY `_order`, `id` ASC LIMIT 1", $loc_id);
		if (lysql_num_rows($get_a_track_id) > 0) {
			$track_info = lysql_fetch_array($get_a_track_id);
			$track_id = $track_info['id'];
			set_sessvar("track_id", $track_id);			
		}
	} else {
		$get_track_info = lavu_query("SELECT * FROM `lk_tracks` WHERE `id` = '[1]'", $track_id);
		if (lysql_num_rows($get_track_info) > 0) {
			$track_info = lysql_fetch_array($get_track_info);
		}
	}

	$year = reqvar("year",date("Y"));
	$month = reqvar("month",date("m"));
	$day = reqvar("day",date("d"));
	
	$pyear = $year;
	$pmonth = $month - 1;
	if($pmonth < 1) {$pmonth = 12;$pyear--;}
	$nyear = $year;
	$nmonth = $month + 1;
	if($nmonth > 12) {$nmonth = 1;$nyear++;}
	
	function create_calendar($year,$month,$day)
	{
		$js_cal_click = "cal_click";
		$calwidth = 51;
		$calheight = 48;
		$allow_past = true;

		$set_year = $year;
		$set_month = $month;
		$set_day = $day;
		
		$str .= "<script language='javascript'>";
		$str .= "function $js_cal_click(year, month, day) { ";
		$str .= "window.location = \"inner_calendar.php?year=\" + year + \"&month=\" + month + \"&day=\" + day; ";
		$str .= "} ";
		$str .= "</script>";
		
		$str .= "<div id='calendar'>";
		$str .= "<table cellspacing='0'>";
		$r=1;$day=1;
		while($r<=6)
		{
			$str .= "<tr>";
			if($r==1)
			{
				$c=date("w",mktime(0, 0, 0, $month, 1, $year))+1;
				$x=1;
				while($x<$c)
				{
				  $str .= "<td width=$calwidth height=$calheight>&nbsp;</td>";
				  $x=$x+1;
				}
			}
			else $c=1;
			
			while($c<=7)
			{
				if($day<10) $day="0".$day;
				$datecheck = $year."-".$month."-".$day;
				
				if((($r+$c)%2)==0) 
					$sbg="#9fd4f0";
				else 
					$sbg="#5bb4e2";
					
				if(checkdate($month,$day,$year))
				{						
					$dayoftheweek = date("w",mktime(0,0,0,$month,$day,$year)) * 1 + 1;
	
					$msg = date("l",mktime(0,0,0,$month,$day,$year));
					$clr = "000000";
					
					if($allow_past || date("Y-m-d") <= date("Y-m-d",mktime(0,0,0,$month,$day,$year)))
						$allow = true;
					else
					{
						$allow = false;
						$clr = "666699";
					}
					
					$datenum = ($month * 100) + $day;
					$cal_js .= "cal_colors[$datenum] = '$sbg'; ";
					if($month==$set_month && $year==$set_year && $day==$set_day)
					{
						$extra_style_code = "background: URL(images/btn_cal_day.png); background-position:center center; background-repeat:no-repeat; ";
						//$extra_style_code = "border:solid 2px black; ";
						$sbg = "#00ff00";
						$cal_js .="selected_datenum = '$datenum'; ";
					}
					else $extra_style_code = "background: URL(images/btn_cal_day_lit.png); background-position:center center; background-repeat:no-repeat; ";
					
					$str .= "<td id='".$fieldname."_date_$datecheck' class='cal' title='$msg' alt='$msg'";
					$str .= " style='cursor:pointer; ";
					$str .= $extra_style_code;
					$str .= "'";
					if($allow)
						$str .= " onclick='$js_cal_click($year, $month, $day)'";
					$str .= " width=$calwidth height=$calheight align='center' valign='center'>";
					$str .= "<font color='#$clr'>";
					$str .= ($day*1);
					$str .= "</font>";
					$str .= "</td>";						
				}
				else $str .= "<td width=$calwidth height=$calheight>&nbsp;</td>";
	
				$c++;$day++;
			}
			$str .= "</tr>";
			$r++;
		}
		$str .= "</table>";
		$str .= "</div>";
		return $str;
	}	
?>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Untitled Document</title>
		<style type="text/css">
<!--
.caltext {
	font-family: "Trebuchet MS", Arial, Helvetica, sans-serifVerdana, Geneva, sans-serif;
	font-size: 16px;
	font-weight: bold;
	color: #154473;
}
.cal_title {
	font-family: "Trebuchet MS", Arial, Helvetica, sans-serifVerdana, Geneva, sans-serif;
	font-size: 21px;
	font-weight: bold;
	color: #FFFFFF;
}
.available_time {
	font-family: "Trebuchet MS", Arial, Helvetica, sans-serifVerdana, Geneva, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #000099;
	width:68;
	height:38;
	background:URL(images/btn_cal_time.png);
	background-repeat:no-repeat;
}
.unavailable_time {
	font-family: "Trebuchet MS", Arial, Helvetica, sans-serifVerdana, Geneva, sans-serif;
	font-size: 14px;
	color: #999999;
	width:68;
	height:38;
	background:URL(images/btn_cal_time_gray.png);
	background-repeat:no-repeat;
}
-->
		</style>
	</head>

	<body style='margin:0px'>
		<table width="423" height="374" border="0" cellspacing="0" cellpadding="0">
			<tr><td height="7" colspan="5"></td></tr>
		  <tr>
				<td width="3" height="40">&nbsp;</td>
				<td width="120" height="40" align="center" valign="middle" onClick="window.location = '<?php echo "inner_calendar.php?year=$pyear&month=$pmonth&day=1"; ?>'" style="background:URL(images/btn_cal_prev.png); background-repeat:no-repeat;">&nbsp;</td>
				<td width="163" height="40" align="center"><span class="cal_title"><?php echo date("M Y",mktime(0,0,0,$month,$day,$year)); ?></td>
				<td width="120" height="40" align="center" valign="middle" onClick="window.location = '<?php echo "inner_calendar.php?year=$nyear&month=$nmonth&day=1"; ?>'" style="background:URL(images/btn_cal_next.png); background-repeat:no-repeat;">&nbsp;</td>
				<td width="17" height="40">&nbsp;</td>
			</tr>
			<tr><td colspan="5" align="center" valign="top" style="padding:5px 0px 0px 0px;"><?php echo create_calendar($year,$month,$day); ?></td></tr>
			<tr>
				<td colspan="5" align="center" valign="top" style="padding:11px 0px 0px 0px">
					<table cellspacing='1' cellpadding='0'>
						<?php
							$used_times = array();
							$check_events = lysql_query("SELECT * FROM `lk_group_events` WHERE `event_date` = '$year-$month-$day' AND `cancelled` != '1' AND `_deleted` != '1' AND `loc_id` = '$loc_id'");
							if (lysql_num_rows($check_events) > 0) {
								while ($extract = lysql_fetch_array($check_events)) {
									$event_time = explode(":", $extract['event_time']);
									$event_hour = $event_time[0];
									$event_minutes = $event_time[1];
									$event_ts = mktime($event_hour, $event_minutes, 0, $month, $day, $year);
									$half_hours = ($extract['duration'] / 30);
									$used_times[] = date("G:i", $event_ts);
									for ($hh = 0; $hh < ($half_hours - 1); $hh++) {
										$event_ts = ($event_ts + 1800);
										if (!in_array(date("G:i", $event_ts), $used_times)) {
											$used_times[] = date("G:i", $event_ts);
										}
									}
								}
							}
						
							$col = 1;
							for ($ts = mktime(($track_info['time_start'] / 100), ($track_info['time_start'] % 100), 0, $month, $day, $year); $ts < mktime(($track_info['time_end'] / 100), ($track_info['time_end'] % 100), 0, $month, $day, $year); ($ts += 1800)) {
								$time = date("G:i", $ts);
								if ($col == 1) {
									echo "<tr>";
								}
								if (in_array($time, $used_times)) {
									$time_cell_class = "unavailable_time";
									$time_cell_onclick = "";
								} else {
									$time_cell_class = "available_time";
									$time_cell_onclick = " onclick='parent.location = \"group_event_input.php?mode=get_info&year=$year&month=$month&day=$day&time=$time\"'";
								}
								echo "<td class='$time_cell_class' align='center' valign='middle'".$time_cell_onclick.">".date("g:ia", $ts)."</td>";
								if ($col == 6) {
									echo "</tr>";
									$col = 0;
								}
								$col++;
							}
							
						?>
					</table>
				</td>
			</tr>
		</table>
	</body>
</html>
<?php

} else if($uripage =="lavukart_pit.php") {
//--------------------------------------------------- lavukart_pit.php ----------------------------------------------//
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Untitled Document</title>
<style type="text/css">
<!--
.style1 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-weight: bold;
	font-size: 20px;
	color: #0D2249;
}
.style2 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-weight: bold;
	font-size: 22px;
	color: #FFFFFF;
}
.style3 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 18px;
	font-weight: bold;
	color: #999999;
}
-->
</style>
</head>

<body>
<table width="1024" height="613" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td height="613" align="center" valign="top"><table width="984" height="613" border="0" cellpadding="0" cellspacing="0" background="images/pitwin.png">
      <tr>
        <td height="6" align="center">&nbsp;</td>
      </tr>
      <tr>
        <td height="41" align="center">&nbsp;</td>
      </tr>
      <tr>
        <td height="44" align="center"><table width="928" height="44" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="102"><span class="style1">RACERS</span></td>
            <td width="422" align="center" valign="middle"><table width="402" border="0" cellspacing="0" cellpadding="3">
              <tr>
                <td><span class="style3">SEARCH</span></td>
              </tr>
            </table></td>
            <td width="74" align="center" valign="middle" class="style1">GO</td>
            <td width="331">&nbsp;</td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td height="19" align="center">&nbsp;</td>
      </tr>
      <tr>
        <td height="429" align="center"><table width="924" height="428" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="77" height="107" align="center" valign="middle"><table width="73" height="51" border="0" cellpadding="0" cellspacing="0" background="images/car_red.png">
              <tr>
                <td align="center" valign="middle"><span class="style2">12</span></td>
              </tr>
            </table></td>
            <td width="77" height="107" align="center" valign="middle"><table width="73" height="51" border="0" cellpadding="0" cellspacing="0" background="images/car_red.png">
              <tr>
                <td align="center" valign="middle"><span class="style2">11</span></td>
              </tr>
            </table></td>
            <td width="77" height="107" align="center" valign="middle"><table width="73" height="51" border="0" cellpadding="0" cellspacing="0" background="images/car_red.png">
              <tr>
                <td align="center" valign="middle"><span class="style2">10</span></td>
              </tr>
            </table></td>
            <td width="77" height="107" align="center" valign="middle"><table width="73" height="51" border="0" cellpadding="0" cellspacing="0" background="images/car_red.png">
              <tr>
                <td align="center" valign="middle"><span class="style2">9</span></td>
              </tr>
            </table></td>
            <td width="77" height="107" align="center" valign="middle"><table width="73" height="51" border="0" cellpadding="0" cellspacing="0" background="images/car_red.png">
              <tr>
                <td align="center" valign="middle"><span class="style2">8</span></td>
              </tr>
            </table></td>
            <td width="77" height="107" align="center" valign="middle"><table width="73" height="51" border="0" cellpadding="0" cellspacing="0" background="images/car_red.png">
              <tr>
                <td align="center" valign="middle"><span class="style2">7</span></td>
              </tr>
            </table></td>
            <td width="77" height="107" align="center" valign="middle"><table width="73" height="51" border="0" cellpadding="0" cellspacing="0" background="images/car_red.png">
              <tr>
                <td align="center" valign="middle"><span class="style2">6</span></td>
              </tr>
            </table></td>
            <td width="77" height="107" align="center" valign="middle"><table width="73" height="51" border="0" cellpadding="0" cellspacing="0" background="images/car_red.png">
              <tr>
                <td align="center" valign="middle"><span class="style2">5</span></td>
              </tr>
            </table></td>
            <td width="77" height="107" align="center" valign="middle"><table width="73" height="51" border="0" cellpadding="0" cellspacing="0" background="images/car_red.png">
              <tr>
                <td align="center" valign="middle"><span class="style2">4</span></td>
              </tr>
            </table></td>
            <td width="77" height="107" align="center" valign="middle"><table width="73" height="51" border="0" cellpadding="0" cellspacing="0" background="images/car_red.png">
              <tr>
                <td align="center" valign="middle"><span class="style2">3</span></td>
              </tr>
            </table></td>
            <td width="77" height="107" align="center" valign="middle"><table width="73" height="51" border="0" cellpadding="0" cellspacing="0" background="images/car_red.png">
              <tr>
                <td align="center" valign="middle"><span class="style2">2</span></td>
              </tr>
            </table></td>
            <td width="77" height="107" align="center" valign="middle"><table width="73" height="51" border="0" cellpadding="0" cellspacing="0" background="images/car_red.png">
              <tr>
                <td align="center" valign="middle"><span class="style2">1</span></td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td width="77" height="107" align="center" valign="middle">&nbsp;</td>
            <td width="77" height="107" align="center" valign="middle">&nbsp;</td>
            <td width="77" height="107" align="center" valign="middle">&nbsp;</td>
            <td width="77" height="107" align="center" valign="middle">&nbsp;</td>
            <td width="77" height="107" align="center" valign="middle">&nbsp;</td>
            <td width="77" height="107" align="center" valign="middle"><table width="73" height="51" border="0" cellpadding="0" cellspacing="0" background="images/car_blue.png">
              <tr>
                <td align="center" valign="middle"><span class="style2">7</span></td>
              </tr>
            </table></td>
            <td width="77" height="107" align="center" valign="middle"><table width="73" height="51" border="0" cellpadding="0" cellspacing="0" background="images/car_blue.png">
              <tr>
                <td align="center" valign="middle"><span class="style2">6</span></td>
              </tr>
            </table></td>
            <td width="77" height="107" align="center" valign="middle"><table width="73" height="51" border="0" cellpadding="0" cellspacing="0" background="images/car_blue.png">
              <tr>
                <td align="center" valign="middle"><span class="style2">5</span></td>
              </tr>
            </table></td>
            <td width="77" height="107" align="center" valign="middle"><table width="73" height="51" border="0" cellpadding="0" cellspacing="0" background="images/car_blue.png">
              <tr>
                <td align="center" valign="middle"><span class="style2">4</span></td>
              </tr>
            </table></td>
            <td width="77" height="107" align="center" valign="middle"><table width="73" height="51" border="0" cellpadding="0" cellspacing="0" background="images/car_blue.png">
              <tr>
                <td align="center" valign="middle"><span class="style2">3</span></td>
              </tr>
            </table></td>
            <td width="77" height="107" align="center" valign="middle"><table width="73" height="51" border="0" cellpadding="0" cellspacing="0" background="images/car_blue.png">
              <tr>
                <td align="center" valign="middle"><span class="style2">2</span></td>
              </tr>
            </table></td>
            <td width="77" height="107" align="center" valign="middle"><table width="73" height="51" border="0" cellpadding="0" cellspacing="0" background="images/car_blue.png">
              <tr>
                <td align="center" valign="middle"><span class="style2">1</span></td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td width="77" height="107" align="center" valign="middle">&nbsp;</td>
            <td width="77" height="107" align="center" valign="middle">&nbsp;</td>
            <td width="77" height="107" align="center" valign="middle">&nbsp;</td>
            <td width="77" height="107" align="center" valign="middle">&nbsp;</td>
            <td width="77" height="107" align="center" valign="middle">&nbsp;</td>
            <td width="77" height="107" align="center" valign="middle">&nbsp;</td>
            <td width="77" height="107" align="center" valign="middle"><table width="73" height="51" border="0" cellpadding="0" cellspacing="0" background="images/car_gold.png">
              <tr>
                <td align="center" valign="middle"><span class="style2">15</span></td>
              </tr>
            </table></td>
            <td width="77" height="107" align="center" valign="middle"><table width="73" height="51" border="0" cellpadding="0" cellspacing="0" background="images/car_black.png">
              <tr>
                <td align="center" valign="middle"><span class="style2">2</span></td>
              </tr>
            </table></td>
            <td width="77" height="107" align="center" valign="middle"><table width="73" height="51" border="0" cellpadding="0" cellspacing="0" background="images/car_white.png">
              <tr>
                <td align="center" valign="middle"><span class="style2">37</span></td>
              </tr>
            </table></td>
            <td width="77" height="107" align="center" valign="middle"><table width="73" height="51" border="0" cellpadding="0" cellspacing="0" background="images/car_red.png">
              <tr>
                <td align="center" valign="middle"><span class="style2">12</span></td>
              </tr>
            </table></td>
            <td width="77" height="107" align="center" valign="middle"><table width="73" height="51" border="0" cellpadding="0" cellspacing="0" background="images/car_red.png">
              <tr>
                <td align="center" valign="middle"><span class="style2">2</span></td>
              </tr>
            </table></td>
            <td width="77" height="107" align="center" valign="middle"><table width="73" height="51" border="0" cellpadding="0" cellspacing="0" background="images/car_red.png">
              <tr>
                <td align="center" valign="middle"><span class="style2">5</span></td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td width="77" height="107" align="center" valign="middle">&nbsp;</td>
            <td width="77" height="107" align="center" valign="middle">&nbsp;</td>
            <td width="77" height="107" align="center" valign="middle"><table width="73" height="51" border="0" cellpadding="0" cellspacing="0" background="images/car_red.png">
              <tr>
                <td align="center" valign="middle"><span class="style2">10</span></td>
              </tr>
            </table></td>
            <td width="77" height="107" align="center" valign="middle"><table width="73" height="51" border="0" cellpadding="0" cellspacing="0" background="images/car_red.png">
              <tr>
                <td align="center" valign="middle"><span class="style2">8</span></td>
              </tr>
            </table></td>
            <td width="77" height="107" align="center" valign="middle"><table width="73" height="51" border="0" cellpadding="0" cellspacing="0" background="images/car_red.png">
              <tr>
                <td align="center" valign="middle"><span class="style2">6</span></td>
              </tr>
            </table></td>
            <td width="77" height="107" align="center" valign="middle"><table width="73" height="51" border="0" cellpadding="0" cellspacing="0" background="images/car_red.png">
              <tr>
                <td align="center" valign="middle"><span class="style2">13</span></td>
              </tr>
            </table></td>
            <td width="77" height="107" align="center" valign="middle"><table width="73" height="51" border="0" cellpadding="0" cellspacing="0" background="images/car_red.png">
              <tr>
                <td align="center" valign="middle"><span class="style2">12</span></td>
              </tr>
            </table></td>
            <td width="77" height="107" align="center" valign="middle"><table width="73" height="51" border="0" cellpadding="0" cellspacing="0" background="images/car_red.png">
              <tr>
                <td align="center" valign="middle"><span class="style2">9</span></td>
              </tr>
            </table></td>
            <td width="77" height="107" align="center" valign="middle"><table width="73" height="51" border="0" cellpadding="0" cellspacing="0" background="images/car_red.png">
              <tr>
                <td align="center" valign="middle"><span class="style2">4</span></td>
              </tr>
            </table></td>
            <td width="77" height="107" align="center" valign="middle"><table width="73" height="51" border="0" cellpadding="0" cellspacing="0" background="images/car_red.png">
              <tr>
                <td align="center" valign="middle"><span class="style2">37</span></td>
              </tr>
            </table></td>
            <td width="77" height="107" align="center" valign="middle"><table width="73" height="51" border="0" cellpadding="0" cellspacing="0" background="images/car_red.png">
              <tr>
                <td align="center" valign="middle"><span class="style2">17</span></td>
              </tr>
            </table></td>
            <td width="77" height="107" align="center" valign="middle"><table width="73" height="51" border="0" cellpadding="0" cellspacing="0" background="images/car_red.png">
              <tr>
                <td align="center" valign="middle"><span class="style2">15</span></td>
              </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td height="39" align="center"><table width="928" height="39" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td align="center" valign="bottom"><img src="images/scrolldrag.png" alt="drag" width="110" height="34" /></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td height="20" align="center">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
</body>
</html>

<?php

} else if($uripage =="lk_functions.php") {
//--------------------------------------------------- lk_functions.php ----------------------------------------------//
?>

<?php
	function get_slots_for_event($eventid)
	{
		$slots = 10;
		$ev_query = lavu_query("select * from `lk_events` where `id`='[1]'",$eventid);
		if(lysql_num_rows($ev_query))
		{
			$ev_read = lysql_fetch_assoc($ev_query);
			$event_type = $ev_read['type'];
			$type_query = lavu_query("select * from `lk_event_types` where `id`='[1]'",$event_type);
			if(lysql_num_rows($type_query))
			{
				$type_read = lysql_fetch_assoc($type_query);
				if(isset($type_read['slots']))
				{
					$set_slots = $type_read['slots'];
					if($set_slots!="" && is_numeric($set_slots))
						$slots = $set_slots;
				}
			}
		}
		return $slots;
	}
?>

<?php

} else if($uripage =="newevent.php") {
//--------------------------------------------------- newevent.php ----------------------------------------------//
?>

<?php
	//require_once(dirname(__FILE__) . "/../comconnect.php");
	
	$mode = urlvar("mode");
	if($mode=="add_event")
	{
	}
?>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<style type="text/css">
<!--

body, table {
	font-family:Verdana, Arial, Helvetica, sans-serif;
}

.caltext {
	font-family: "Trebuchet MS", Arial, Helvetica, sans-serifVerdana, Geneva, sans-serif;
	font-size: 16px;
	font-weight: bold;
	color: #154473;
}
-->
</style>
</head>

<body style='margin:0px'>
<table width="423" height="334" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="5" align="center" valign="top">
		<?php
			$date = reqvar("date");
			$time = reqvar("time");
			$locid = reqvar("locid");
			$trackid = reqvar("trackid");
			
			$scrolltop = reqvar("scrolltop");
			
			$date_parts = explode("-",$date);
			$year = $date_parts[0];
			$month = $date_parts[1];
			$day = $date_parts[2];
			
			if($date && $time)
			{
	        	echo "&nbsp;<br><span style='color:#ffffff'>New Event For <b>" . display_date($date) . " - " . display_time($time) . "</b>:</font>";
				echo "<br><br>";
				echo "<table cellspacing=4 cellpadding=12>";
				echo "<tr>";
				$col = 1;
				$type_query = lavu_query("select * from `lk_event_types` where `locationid`='[1]' order by `_order` asc",$locid);
				while($type_read = lysql_fetch_assoc($type_query))
				{
					echo "<td style='border:solid 1px #777777; padding-left:40px; padding-right:40px' bgcolor='#ffffff' onclick='window.location = \"_DO:cmd=close_overlay&c_name=events&f_name=lavukart/events.php?mode=add_event&event_date=$date&event_time=$time&event_type=".$type_read['id']."&event_locationid=$locid&event_trackid=$trackid&year=$year&month=$month&day=$day&set_scrolltop=$scrolltop\"'>" . $type_read['title'] . "</td>";
					$col++;
					if($col>2) {echo "</tr><tr>"; $col = 1;}
				}
				echo "</tr>";
				echo "</table>";
			}
			else
			{
				echo "Error: No Date/Time Sent";
			}
		?>
    </td>
  </tr>
</table>
</body>


<?php

} else if($uripage =="newevent_wrapper.php") {
//--------------------------------------------------- newevent_wrapper.php ----------------------------------------------//
?>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<style type="text/css">
<!--

.caltext {
	font-family: "Trebuchet MS", Arial, Helvetica, sans-serifVerdana, Geneva, sans-serif;
	font-size: 16px;
	font-weight: bold;
	color: #154473;
}
-->
</style>
</head>

<body style='margin:0px'>
<table width="423" height="374" border="0" cellspacing="0" cellpadding="0" background="images/win_overlay_newevent_full.png">
  <tr>
    <td width="20" height="40">&nbsp;</td>
    <td width="120" align="center" valign="middle"><!--<img src="images/btn_cal_prev.png" width="120" height="40">-->&nbsp;</td>
    <td width="134" onClick="window.location = '_DO:cmd=close_overlay'">&nbsp;</td>
    <td width="120" align="center" valign="middle"><!--<img src="images/btn_cal_next.png" width="120" height="40">-->&nbsp;</td>
    <td width="21">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="5" align="center" valign="middle">&nbsp;</td>
  </tr>
</table>
</body>
</html>

<?php

} else if($uripage =="overlay.php") {
//--------------------------------------------------- overlay.php ----------------------------------------------//
?>

<html>
<body>
<table width='490px' cellpadding='10'>
<tr><td align='center'><b>Test Component</b></td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td align='center'>Racers Right</td></tr>
<tr><td>&nbsp;</td></tr>
<tr>
	<td align='left' bgcolor='#FFFFFF'>
		<font color='#0000FF'><b>RECEIVED DATA:</b><br>
		<?php
			echo "<br><a onclick='location = \"_DO:cmd=close_overlay\"'>Close</a>";
			echo "<br>";
			echo "<br>component name: ".$_POST['comp_name'];
			echo "<br>company code: ".$_POST['cc'];
			echo "<br>data name: ".$_POST['dn'];
			echo "<br>location id: ".$_POST['loc_id'];
			echo "<br>server id: ".$_POST['server_id'];
			echo "<br>server name: ".$_POST['server_name'];
			echo "<br>tab name: ".$_POST['tab_name'];
			echo "<br><br>test1: ".$_POST['test1'];
			echo "<br>test2: ".$_POST['test2'];
		?>
	</td>
</tr>
</table>
</body>
</html>
<?php

} else if($uripage =="overlay_header.php") {
//--------------------------------------------------- overlay_header.php ----------------------------------------------//
?>

<html>
<body>
<table width='490px' cellpadding='10'>
<tr><td align='center'><b>Test Component</b></td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td align='center'>Racers Right</td></tr>
<tr><td>&nbsp;</td></tr>
<tr>
	<td align='left' bgcolor='#FFFFFF'>
		<font color='#0000FF'><b>RECEIVED DATA:</b><br>
		<?php
			echo "<br>component name: ".$_POST['comp_name'];
			echo "<br>company code: ".$_POST['cc'];
			echo "<br>data name: ".$_POST['dn'];
			echo "<br>location id: ".$_POST['loc_id'];
			echo "<br>server id: ".$_POST['server_id'];
			echo "<br>server name: ".$_POST['server_name'];
			echo "<br>tab name: ".$_POST['tab_name'];
			echo "<br><br>test1: ".$_POST['test1'];
			echo "<br>test2: ".$_POST['test2'];
		?>
	</td>
</tr>
</table>
</body>
</html>
<?php

} else if($uripage =="pit_grid.php") {
//--------------------------------------------------- pit_grid.php ----------------------------------------------//
?>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Untitled Document</title>
		<style type="text/css">
<!--
body {
	background-color: transparent;
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}

.style4 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #8896AD;
}
.style6 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 24px;
	font-weight: bold;
	color: #8896AD;
}
.style8 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 15px;
	font-weight: bold;
	color: #07142a;
}
.nametext {font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
}
.subtitles1 {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #5A75A0;
}
.titlebutton {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #5ecd53;
}
.title1 {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 18px;
	font-weight: bold;
	color: #5A75A0;
}
.carnumber {font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #FFF;
}
-->
		</style>
	</head>

	<body>
		<table width="420" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td align="center">
					<table width="360" border="0" cellspacing="0" cellpadding="0">
						<tr><td align="center"><a onclick='location = "_DO:cmd=close_overlay";'>CLOSE</a></td></tr>
						<tr>
							<td align="center">
								<table width="52" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="52" height="46" align="left" valign="top" background="images/car1.png"><table width="40" height="36" border="0" cellspacing="0" cellpadding="6">
                      <tr>
                        <td align="center" valign="bottom" class="carnumber">24</td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td height="46" align="left" valign="top" background="images/car1.png"><table width="40" height="36" border="0" cellspacing="0" cellpadding="6">
                      <tr>
                        <td align="center" valign="bottom" class="carnumber">24</td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td height="46" align="left" valign="top" background="images/car1.png"><table width="40" height="36" border="0" cellspacing="0" cellpadding="6">
                      <tr>
                        <td align="center" valign="bottom" class="carnumber">24</td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td height="46" align="left" valign="top" background="images/car1.png"><table width="40" height="36" border="0" cellspacing="0" cellpadding="6">
                      <tr>
                        <td align="center" valign="bottom" class="carnumber">24</td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td height="46" align="left" valign="top" background="images/car1.png"><table width="40" height="36" border="0" cellspacing="0" cellpadding="6">
                      <tr>
                        <td align="center" valign="bottom" class="carnumber">24</td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td height="46" align="left" valign="top" background="images/car1.png"><table width="40" height="36" border="0" cellspacing="0" cellpadding="6">
                      <tr>
                        <td align="center" valign="bottom" class="carnumber">24</td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td height="46" align="left" valign="top" background="images/car1.png"><table width="40" height="36" border="0" cellspacing="0" cellpadding="6">
                      <tr>
                        <td align="center" valign="bottom" class="carnumber">24</td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td height="46" align="left" valign="top" background="images/car1.png"><table width="40" height="36" border="0" cellspacing="0" cellpadding="6">
                      <tr>
                        <td align="center" valign="bottom" class="carnumber">24</td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td height="46" align="left" valign="top" background="images/car1.png"><table width="40" height="36" border="0" cellspacing="0" cellpadding="6">
                      <tr>
                        <td align="center" valign="bottom" class="carnumber">24</td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td height="46" align="left" valign="top" background="images/car1.png"><table width="40" height="36" border="0" cellspacing="0" cellpadding="6">
                      <tr>
                        <td align="center" valign="bottom" class="carnumber">24</td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td height="46" align="left" valign="top" background="images/car1.png"><table width="40" height="36" border="0" cellspacing="0" cellpadding="6">
                      <tr>
                        <td align="center" valign="bottom" class="carnumber">24</td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td height="46" align="left" valign="top" background="images/car1.png"><table width="40" height="36" border="0" cellspacing="0" cellpadding="6">
                      <tr>
                        <td align="center" valign="bottom" class="carnumber">24</td>
                      </tr>
                    </table></td>
                  </tr>
                </table></td>
                <td align="center"><table width="52" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="52" height="46" align="left" valign="top" background="images/car2.png"><table width="40" height="36" border="0" cellspacing="0" cellpadding="6">
                      <tr>
                        <td align="center" valign="bottom" class="carnumber">24</td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td height="46" align="left" valign="top" background="images/car2.png"><table width="40" height="36" border="0" cellspacing="0" cellpadding="6">
                      <tr>
                        <td align="center" valign="bottom" class="carnumber">24</td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td height="46" align="left" valign="top" background="images/car2.png"><table width="40" height="36" border="0" cellspacing="0" cellpadding="6">
                      <tr>
                        <td align="center" valign="bottom" class="carnumber">24</td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td height="46" align="left" valign="top" background="images/car2.png"><table width="40" height="36" border="0" cellspacing="0" cellpadding="6">
                      <tr>
                        <td align="center" valign="bottom" class="carnumber">24</td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td height="46" align="left" valign="top" background="images/car2.png"><table width="40" height="36" border="0" cellspacing="0" cellpadding="6">
                      <tr>
                        <td align="center" valign="bottom" class="carnumber">24</td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td height="46" align="left" valign="top" background="images/car2.png"><table width="40" height="36" border="0" cellspacing="0" cellpadding="6">
                      <tr>
                        <td align="center" valign="bottom" class="carnumber">24</td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td height="46" align="left" valign="top" background="images/car2.png"><table width="40" height="36" border="0" cellspacing="0" cellpadding="6">
                      <tr>
                        <td align="center" valign="bottom" class="carnumber">24</td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td height="46" align="left" valign="top" background="images/car2.png"><table width="40" height="36" border="0" cellspacing="0" cellpadding="6">
                      <tr>
                        <td align="center" valign="bottom" class="carnumber">24</td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td height="46" align="left" valign="top" background="images/car2.png"><table width="40" height="36" border="0" cellspacing="0" cellpadding="6">
                      <tr>
                        <td align="center" valign="bottom" class="carnumber">24</td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td height="46" align="left" valign="top" background="images/car2.png"><table width="40" height="36" border="0" cellspacing="0" cellpadding="6">
                      <tr>
                        <td align="center" valign="bottom" class="carnumber">24</td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td height="46" align="left" valign="top" background="images/car2.png"><table width="40" height="36" border="0" cellspacing="0" cellpadding="6">
                      <tr>
                        <td align="center" valign="bottom" class="carnumber">24</td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td height="46" align="left" valign="top" background="images/car2.png"><table width="40" height="36" border="0" cellspacing="0" cellpadding="6">
                      <tr>
                        <td align="center" valign="bottom" class="carnumber">24</td>
                      </tr>
                    </table></td>
                  </tr>
                </table></td>
                <td align="center"><table width="52" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="52" height="46" align="left" valign="top" background="images/car3.png"><table width="40" height="36" border="0" cellspacing="0" cellpadding="6">
                      <tr>
                        <td align="center" valign="bottom" class="carnumber">24</td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td height="46" align="left" valign="top" background="images/car3.png"><table width="40" height="36" border="0" cellspacing="0" cellpadding="6">
                      <tr>
                        <td align="center" valign="bottom" class="carnumber">24</td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td height="46" align="left" valign="top" background="images/car3.png"><table width="40" height="36" border="0" cellspacing="0" cellpadding="6">
                      <tr>
                        <td align="center" valign="bottom" class="carnumber">24</td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td height="46" align="left" valign="top" background="images/car3.png"><table width="40" height="36" border="0" cellspacing="0" cellpadding="6">
                      <tr>
                        <td align="center" valign="bottom" class="carnumber">24</td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td height="46" align="left" valign="top" background="images/car3.png"><table width="40" height="36" border="0" cellspacing="0" cellpadding="6">
                      <tr>
                        <td align="center" valign="bottom" class="carnumber">24</td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td height="46" align="left" valign="top" background="images/car3.png"><table width="40" height="36" border="0" cellspacing="0" cellpadding="6">
                      <tr>
                        <td align="center" valign="bottom" class="carnumber">24</td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td height="46" align="left" valign="top" background="images/car3.png"><table width="40" height="36" border="0" cellspacing="0" cellpadding="6">
                      <tr>
                        <td align="center" valign="bottom" class="carnumber">24</td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td height="46" align="left" valign="top" background="images/car3.png"><table width="40" height="36" border="0" cellspacing="0" cellpadding="6">
                      <tr>
                        <td align="center" valign="bottom" class="carnumber">24</td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td height="46" align="left" valign="top" background="images/car3.png"><table width="40" height="36" border="0" cellspacing="0" cellpadding="6">
                      <tr>
                        <td align="center" valign="bottom" class="carnumber">24</td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td height="46" align="left" valign="top" background="images/car3.png"><table width="40" height="36" border="0" cellspacing="0" cellpadding="6">
                      <tr>
                        <td align="center" valign="bottom" class="carnumber">24</td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td height="46" align="left" valign="top" background="images/car3.png"><table width="40" height="36" border="0" cellspacing="0" cellpadding="6">
                      <tr>
                        <td align="center" valign="bottom" class="carnumber">24</td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td height="46" align="left" valign="top" background="images/car3.png"><table width="40" height="36" border="0" cellspacing="0" cellpadding="6">
                      <tr>
                        <td align="center" valign="bottom" class="carnumber">24</td>
                      </tr>
                    </table></td>
                  </tr>
                </table></td>
                <td align="center"><table width="52" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="52" height="46" align="left" valign="top" background="images/car4.png"><table width="40" height="36" border="0" cellspacing="0" cellpadding="6">
                      <tr>
                        <td align="center" valign="bottom" class="carnumber">24</td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td height="46" align="left" valign="top" background="images/car4.png"><table width="40" height="36" border="0" cellspacing="0" cellpadding="6">
                      <tr>
                        <td align="center" valign="bottom" class="carnumber">24</td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td height="46" align="left" valign="top" background="images/car4.png"><table width="40" height="36" border="0" cellspacing="0" cellpadding="6">
                      <tr>
                        <td align="center" valign="bottom" class="carnumber">24</td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td height="46" align="left" valign="top" background="images/car4.png"><table width="40" height="36" border="0" cellspacing="0" cellpadding="6">
                      <tr>
                        <td align="center" valign="bottom" class="carnumber">24</td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td height="46" align="left" valign="top" background="images/car4.png"><table width="40" height="36" border="0" cellspacing="0" cellpadding="6">
                      <tr>
                        <td align="center" valign="bottom" class="carnumber">24</td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td height="46" align="left" valign="top" background="images/car4.png"><table width="40" height="36" border="0" cellspacing="0" cellpadding="6">
                      <tr>
                        <td align="center" valign="bottom" class="carnumber">24</td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td height="46" align="left" valign="top" background="images/car4.png"><table width="40" height="36" border="0" cellspacing="0" cellpadding="6">
                      <tr>
                        <td align="center" valign="bottom" class="carnumber">24</td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td height="46" align="left" valign="top" background="images/car4.png"><table width="40" height="36" border="0" cellspacing="0" cellpadding="6">
                      <tr>
                        <td align="center" valign="bottom" class="carnumber">24</td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td height="46" align="left" valign="top" background="images/car4.png"><table width="40" height="36" border="0" cellspacing="0" cellpadding="6">
                      <tr>
                        <td align="center" valign="bottom" class="carnumber">24</td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td height="46" align="left" valign="top" background="images/car4.png"><table width="40" height="36" border="0" cellspacing="0" cellpadding="6">
                      <tr>
                        <td align="center" valign="bottom" class="carnumber">24</td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td height="46" align="left" valign="top" background="images/car4.png"><table width="40" height="36" border="0" cellspacing="0" cellpadding="6">
                      <tr>
                        <td align="center" valign="bottom" class="carnumber">24</td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td height="46" align="left" valign="top" background="images/car4.png"><table width="40" height="36" border="0" cellspacing="0" cellpadding="6">
                      <tr>
                        <td align="center" valign="bottom" class="carnumber">24</td>
                      </tr>
                    </table></td>
                  </tr>
                </table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</body>
</html>

<?php

} else if($uripage =="pit_grid_overlay_wrapper.php") {
//--------------------------------------------------- pit_grid_overlay_wrapper.php ----------------------------------------------//
?>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Untitled Document</title>
		<style type="text/css">
<!--
body {
	background-color: transparent;
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}

.style4 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #8896AD;
}
.style6 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 24px;
	font-weight: bold;
	color: #8896AD;
}
.style8 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 15px;
	font-weight: bold;
	color: #07142a;
}
.nametext {font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
}
.subtitles1 {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #5A75A0;
}
.titlebutton {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #5ecd53;
}
.title1 {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 18px;
	font-weight: bold;
	color: #5A75A0;
}
.carnumber {font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #FFF;
}
-->
		</style>
	</head>

	<body>
		<table width="474" height="598" border="0" cellpadding="0" cellspacing="0" background="images/win_overlay_topbar.png">
  		<tr>
    		<td width="26" height="10"></td>
    		<td width="422" height="10"></td>
    		<td width="26" height="10"></td>
  		</tr>
  		<tr>
    		<td width="26" height="536"></td>
    		<td width="422" height="536" align="center" valign="top">
					<table width="420" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td height="26" align="center">
								<table width="360" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td width="90" align="center" class="titlebutton">LANE 1</td>
										<td width="90" align="center" class="titlebutton">LANE2</td>
										<td align="center" class="titlebutton">LANE 3</td>
										<td align="center" class="titlebutton">LANE 4</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr><td align="center">&nbsp;</td></tr>
					</table>
				</td>
			</tr>
		</table>
	</body>
</html>

<?php

} else if($uripage =="pit_karts.php") {
//--------------------------------------------------- pit_karts.php ----------------------------------------------//
?>

<html>
	<head>
		<title>Untitled Document</title>
		<style type="text/css">
<!--
body {
	background-color: transparent;
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}

.carnumber {	font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #FFF;
}
.creditnumber {	font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #3d4f6d;
}
.nametext {	font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
}
.nametext_grey {	font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #777777;
}
.gridposition {	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #5A75A0;
}
.icontext {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 11px;
	font-weight: bold;
	color: #5A75A0;
}
.subtitles {	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #5A75A0;
}
.title1 {	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 18px;
	font-weight: bold;
	color: #5A75A0;
}
-->
		</style>
	</head>

<?php
	//require_once(dirname(__FILE__) . "/../comconnect.php");
	
?>

	<body>
		<table border="0" cellpadding="0" cellspacing="0"><tr>
			<?php
				//require_once(dirname(__FILE__) . "/lk_functions.php");
				$max_cols = 7;
				$col = 1;
				
				if(reqvar("loc_id")) $loc_id = reqvar("loc_id");
				else if(sessvar_isset("loc_id")) $loc_id = sessvar("loc_id");
				set_sessvar("loc_id", $loc_id);
			
				$max_group = 1;
				$kart_query = lavu_query("SELECT * FROM `lk_karts` WHERE `locationid` = '".sessvar("loc_id")."' order by (`number` * 1) asc");
				while($kart_read = lysql_fetch_assoc($kart_query))
				{
					$nametext_class = "nametext_grey";
											
					$lane_calc = ($kart_read['lane'] % 6);	
					$kart_img = "car.png";	
					switch ($lane_calc) {
						 case 1 : $kart_img = "car1.png"; break;
						 case 2 : $kart_img = "car2.png"; break;
						 case 3 : $kart_img = "car3.png"; break;
						 case 4 : $kart_img = "car4.png"; break;
						 case 5 : $kart_img = "car6.png"; break;
						 case 6 : $kart_img = "car7.png"; break;
						 case 7 : $kart_img = "car8.png"; break;
						 case 8 : $kart_img = "car9.png"; break;
					}
					//$next_group = (((int)$kart_read['race_group'] % $max_group) + 1);

					echo "<td style='width:58px; height:42px' align='left' valign='top' class='subtitles'>
						<table width='44' height='36' border='0' cellspacing='0' cellpadding='6' background='images/".$kart_img."'>
							<tr><td align='center' valign='bottom' class='carnumber' onclick='window.location = \"_DO:cmd=send_js&c_name=pit_racers&js=assign_row_selected(".$kart_read['id'].")\";'>".$kart_read['number']."</td></tr>
						</table>
					</td>";
					$col++;
					if($col > $max_cols)
					{
						echo "</tr><tr>";
						$col = 1;
					}
				}
			?>
       </tr></table>
	</body>
</html>
<?php

} else if($uripage =="pit_karts_wrapper.php") {
//--------------------------------------------------- pit_karts_wrapper.php ----------------------------------------------//
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
</body>
</html>

<?php

} else if($uripage =="pit_racers.php") {
//--------------------------------------------------- pit_racers.php ----------------------------------------------//
?>

<html>
	<head>
		<title>Untitled Document</title>
		<style type="text/css">
<!--
body {
	background-color: transparent;
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}

.carnumber {	font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #FFF;
}
.creditnumber {	font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #3d4f6d;
}
.nametext {	font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
}
.nametext_grey {	font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #777777;
}
.gridposition {	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #5A75A0;
}
.icontext {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 11px;
	font-weight: bold;
	color: #5A75A0;
}
.subtitles {	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #5A75A0;
}
.title1 {	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 18px;
	font-weight: bold;
	color: #5A75A0;
}
-->
		</style>
		<script language="javascript">
		
			<?php
				$row_selected = 1;
				if (isset($_POST['row_selected'])) {
					$row_selected = $_POST['row_selected'];
				}
				$js_schedule_ids = "";
				$js_next_row_select = "";
				echo "row_selected = '".$row_selected."'; ";
			?>
		
			function deassign_kart(rowid) {
				window.location = "_DO:cmd=load_url&c_name=pit_racers&f_name=lavukart/pit_racers.php?deassign_kart=" + schedule_ids[rowid] + "&row_selected=" + row_selected;
			}
			
			function assign_row_selected(kartid) {
				window.location = "_DO:cmd=load_url&c_name=pit_racers&f_name=lavukart/pit_racers.php?assign_kart=" + kartid + "&set_scheduleid=" + schedule_ids[row_selected] + "&row_selected=next&row_on=" + row_selected + "&next_row_number=" + next_row_select[row_selected];
			}
		</script>
	</head>

<?php
	//require_once(dirname(__FILE__) . "/../comconnect.php");
	
	$eventid = 0;
	if(reqvar("eventid")) $eventid = reqvar("eventid");
	else if(sessvar_isset("eventid")) $eventid = sessvar("eventid");
	set_sessvar("eventid",$eventid);
	
	if(reqvar("loc_id")) $loc_id = reqvar("loc_id");
	else if(sessvar_isset("loc_id")) $loc_id = sessvar("loc_id");
	set_sessvar("loc_id", $loc_id);

	$max_group = 1;
	$get_karts = lavu_query("SELECT `position` as `position` FROM `lk_karts` WHERE `locationid` = '".sessvar("loc_id")."'");
	if (lysql_num_rows($get_karts) > 0) {
		while ($extract_k = lysql_fetch_array($get_karts)) {
			if ((floor((int)$extract_k['position'] / 2) > $max_group) && (floor((int)$extract_k['position'] / 2) <= 3)) {
				$max_group = floor((int)$extract_k['position'] / 2);
			}
		}
	}

	if(isset($_POST['assign_kart']))
	{
		$current_query = lavu_query("SELECT * FROM `lk_event_schedules` where `eventid`='[1]' and `kartid`='[2]'",$eventid,$_POST['assign_kart']);
		if(lysql_num_rows($current_query))
		{
			lavu_query("UPDATE `lk_event_schedules` SET `kartid`='' where `eventid`='[1]' and `kartid`='[2]'",$eventid,$_POST['assign_kart']);
		}
		lavu_query("UPDATE `lk_event_schedules` SET `kartid`='[1]' where `id`='[2]'",$_POST['assign_kart'],$_POST['set_scheduleid']);
	}
	if (isset($_POST['deassign_kart'])) 
	{
		lavu_query("UPDATE `lk_event_schedules` SET `kartid`='' where `id`='[1]'",$_POST['deassign_kart']);
		//$deassign_racer = lavu_query("UPDATE `lk_event_schedules` SET `kartid` = '' WHERE `eventid` = '".$eventid."' AND `customerid` = '".$_POST['deassignid']."'");
	}
	
	$mode = reqvar("mode");
	
	if ($mode == "set_group") {
		//echo "here"; 
		//mail("richard@greenkeyconcepts.com","deBug","UPDATE `lk_event_schedules` SET `group` = '".reqvar("new_group")."' WHERE `customerid` = '".reqvar("customer_id")."' AND `eventid` = '".$eventid."'");
		lavu_query("UPDATE `lk_event_schedules` SET `group` = '".reqvar("new_group")."' WHERE `customerid` = '".reqvar("customer_id")."' AND `eventid` = '".$eventid."'");
	}

?>

	<body>
		<table width="417" border="0" cellpadding="0" cellspacing="0">
			<?php
				//require_once(dirname(__FILE__) . "/lk_functions.php");
				$minrows = 12;
				$maxrows = get_slots_for_event($eventid);//10;
				$rowcount = 0;			
				$overbooked = 0;
				if($maxrows < $minrows)
					$minrows = $maxrows;

				$rlist = array();
				$get_racers = lavu_query("SELECT `lk_customers`.`membership_expiration` as `membership_expiration`, `lk_customers`.`date_created` as `date_created`, `lk_event_schedules`.`customerid` as customerid, `lk_event_schedules`.`kartid` as kartid, `lk_event_schedules`.`id` as event_schedule_id, `lk_event_schedules`.`group` as race_group, `lk_customers`.`f_name` as f_name, `lk_customers`.`l_name` as l_name, `lk_customers`.`credits` as credits, `lk_customers`.`birth_date` as birth_date, `lk_karts`.`lane` as kartlane, `lk_karts`.`number` as kartnumber FROM `lk_event_schedules` LEFT JOIN `lk_customers` ON `lk_event_schedules`.`customerid` = `lk_customers`.`id` LEFT JOIN `lk_karts` ON `lk_event_schedules`.`kartid` = `lk_karts`.`id` WHERE `eventid` = '".$eventid."'");
				while ($extract_r = lysql_fetch_assoc($get_racers)) {
					$extract_r['pre_placement'] = 888;
					$extract_r['pre_placed'] = false;
					$rlist[] = $extract_r;
				}
				
				$event_query = lavu_query("SELECT * from `lk_events` where `id`='[1]'",$eventid);
				if(lysql_num_rows($event_query))
				{
					$event_read = lysql_fetch_assoc($event_query);
					$event_ts = $event_read['ts'];
					$debug = "";
					
					for($rr=0; $rr<count($rlist); $rr++)
					{
						//$debug .= "\n" . "select * from `lk_race_results` LEFT JOIN `lk_events` ON `lk_race_results`.`eventid`=`lk_events`.`id` where `lk_race_results`.`customerid`='".$rlist[$rr]['customerid']."' and `lk_events`.`ms_end`!='' and `lk_events`.`ts`>'".($event_ts * 1 - (60 * 60 * 12))."' order by `ts` desc limit 1";
						
						$results_query = lavu_query("select * from `lk_race_results` LEFT JOIN `lk_customers` ON `lk_race_results`.`customerid`=`lk_customers`.`id` LEFT JOIN `lk_events` ON `lk_race_results`.`eventid`=`lk_events`.`id` where `lk_race_results`.`customerid`='[1]' and `lk_events`.`ms_end`!='' and `lk_events`.`ts`<'[2]' and `lk_events`.`ts`>'[3]' order by `lk_events`.`ts` desc limit 1",$rlist[$rr]['customerid'],$event_ts,($event_ts * 1 - (60 * 60 * 12)));
						if(lysql_num_rows($results_query))
						{
							$results_read = lysql_fetch_assoc($results_query);
							$rlist[$rr]['pre_placement'] = $results_read['bestlap'];//place'];
							$rlist[$rr]['pre_placed'] = true;
							$debug .= "\n " . $rlist[$rr]['customerid'] . " " . $results_read['f_name'] . " " . $results_read['l_name'] . " index:$rr" . " = value:" . $results_read['place'];
						}
					}
										
					$r2_list = $rlist;
					$rlist = array();
					while(count($rlist) < count($r2_list))
					{
						$lowest = 99999;
						$lowest_index = "";
						for($n=0; $n<count($r2_list); $n++)
						{
							$r2num = $r2_list[$n]['pre_placement'];
							if($r2num!=98989 && ($r2num < $lowest))
							{
								$lowest = $r2num;
								$lowest_index = $n;
							}
						}
						$rlist[] = $r2_list[$lowest_index];
						$r2_list[$lowest_index]['pre_placement'] = 98989;
						$debug .= "\n add to list: index:" . $lowest_index . " value:$lowest)";
					}
					//if(sessvar("loc_id")==9)
						//mail("corey@meyerwind.com","placement",$debug,"From: info@poslavu.com");
				}
				
				//$get_racers = lavu_query("SELECT `lk_customers`.`membership_expiration` as `membership_expiration`, `lk_customers`.`date_created` as `date_created`, `lk_event_schedules`.`customerid` as customerid, `lk_event_schedules`.`kartid` as kartid, `lk_event_schedules`.`group` as race_group, `lk_customers`.`f_name` as f_name, `lk_customers`.`l_name` as l_name, `lk_customers`.`credits` as credits, `lk_customers`.`birth_date` as birth_date, `lk_karts`.`lane` as kartlane, `lk_karts`.`number` as kartnumber FROM `lk_event_schedules` LEFT JOIN `lk_customers` ON `lk_event_schedules`.`customerid` = `lk_customers`.`id` LEFT JOIN `lk_karts` ON `lk_event_schedules`.`kartid` = `lk_karts`.`id` WHERE `eventid` = '".$eventid."'");
				//while ($extract_r = lysql_fetch_assoc($get_racers)) {
				
				if($row_selected=="next")
				{
					if(isset($_POST['row_on']))
					{
						$row_on = $_POST['row_on'];
						
						if($row_on==count($rlist)) $row_selected = 1;
						else $row_selected = $row_on * 1 + 1;
						
						$empty_found = false;
						for($rr=$row_on; $rr <= count($rlist); $rr++)
						{
							if(!$empty_found)
							{
								$extract_r = $rlist[$rr - 1];
								if (($extract_r['kartid'] == "") || ($extract_r['kartid'] == "0")) {
									$empty_found = true;
									$row_selected = $rr;
								}
								else {
								}
							}
						}
						for($rr=1; $rr < $row_on; $rr++)
						{
							if(!$empty_found)
							{
								$extract_r = $rlist[$rr - 1];
								if (($extract_r['kartid'] == "") || ($extract_r['kartid'] == "0")) {
									$empty_found = true;
									$row_selected = $rr;
								}
								else {
								}
							}
						}
					}
				}
				
				$rowcount = 0;
				$rows_filled = array();
				for($rr=0; $rr<count($rlist); $rr++)
				{
					$extract_r = $rlist[$rr];
				
					$birth_date = $extract_r['birth_date'];
					$birth_date1 = $birth_date;
					$bdates = explode("-",$birth_date);
					if(count($bdates) > 2)
					{
						$byear = ($bdates[0] % 100);
						if($byear < 10) $byear = "0" . ($byear * 1);
						$birth_date = $bdates[1] . "/" . $bdates[2] . "/" . $byear;
					}
					else $birth_date = "";
					if($birth_date1!="" && $birth_date1 > date("Y-m-d",mktime(0,0,0,date("m"),date("d"),date("Y") - 18)))
						$cust_is_minor = true;
					else
						$cust_is_minor = false;
											
					$rowcount++;
					if($rr==count($rlist)-1)
						$next_rowcount = 1;
					else
						$next_rowcount = $rowcount + 1;
					$js_next_row_select .= "next_row_select[$rowcount] = $next_rowcount; ";
					
					if($rowcount > $maxrows && $overbooked==0)
					{
						echo "<tr><td bgcolor='#880000' style='font-size:2px'>&nbsp;</td></tr>";
						$overbooked++;
					}
					echo "<tr>
						<td height='46' align='center' valign='top'>
							<table width='417' height='46' border='0' cellpadding='0' cellspacing='0'>
								<tr>";
					if (($extract_r['kartid'] == "") || ($extract_r['kartid'] == "0")) {
						$nametext_class = "nametext";
						echo "<td width='52' align='center' valign='middle' class='subtitles'>&nbsp;</td>";
						$rows_filled[$rowcount] = false;
					} else {
						$nametext_class = "nametext_grey";
						
						$rows_filled[$rowcount] = true;						
												
						$lane_calc = ($extract_r['kartlane'] % 8);	
						$kart_img = "car.png";	
						switch ($lane_calc) {
							 case 1 : $kart_img = "car1.png"; break;
							 case 2 : $kart_img = "car2.png"; break;
							 case 3 : $kart_img = "car3.png"; break;
							 case 4 : $kart_img = "car4.png"; break;
							 case 5 : $kart_img = "car6.png"; break;
							 case 6 : $kart_img = "car7.png"; break;
							 case 7 : $kart_img = "car8.png"; break;
							 case 8 : $kart_img = "car9.png"; break;
						}
						$next_group = (((int)$extract_r['race_group'] % $max_group) + 1);

						echo "<td width='52' align='left' valign='top' class='subtitles' background='images/".$kart_img."' onclick='deassign_kart($rowcount);'>
							<table width='44' height='36' border='0' cellspacing='0' cellpadding='6'>
								<tr><td align='center' valign='bottom' class='carnumber'>".$extract_r['kartnumber']."</td></tr>
							</table>
						</td>";
					}
					$js_schedule_ids .= "schedule_ids[$rowcount] = '".$extract_r['event_schedule_id']."'; ";
					if($row_selected==$rowcount) $use_img = "tab_short_bg2_lit.png"; else $use_img = "tab_short_bg2.png";
					echo "<td width='51' align='center' valign='middle' style='background:URL(images/group_color_bg".$extract_r['race_group'].".png);' onClick='this.style.backgroundImage = \"URL(images/group_color_bg".$next_group.".png)\"; window.location = \"pit_racers.php?mode=set_group&new_group=".$next_group."&customer_id=".$extract_r['customerid']."\";'>
										<table width='51' border='0' cellspacing='0' cellpadding='0'>
											<tr><td align='center' valign='middle'>&nbsp;</td></tr>
										</table>
									</td>
									<td id='kart_row_".$rowcount."' width='263' align='left' valign='middle' background='images/$use_img' onclick='if(row_selected) document.getElementById(\"kart_row_\" + row_selected).style.background = \"URL(images/tab_short_bg2.png)\"; row_selected = \"".$rowcount."\"; this.style.background = \"URL(images/tab_short_bg2_lit.png)\";'>
										<table width='263' border='0' cellspacing='0' cellpadding='8'>
											<tr><td class='".$nametext_class."'>";
											echo $extract_r['f_name']." ".$extract_r['l_name'];
											if($birth_date!="") 
											{
												if($cust_is_minor)
													echo " <font style='font-size:10px; color:#bb0000'>$birth_date</font>";
												else
													echo "";//" <font style='font-size:10px; color:#006600'>$birth_date</font>";
											}
											echo "</td></tr>
										</table>
									</td>
									<td width='52' align='left' valign='top' background='images/btn_credit.png' >
										<table width='50' height='46' border='0' cellspacing='0' cellpadding='6'>
											<tr><td align='center' valign='middle' class='creditnumber'>";
											
											$membership_expiration = $extract_r['membership_expiration'];
											$date_created = $extract_r['date_created'];
											if($date_created >= date("Y-m-d 00:00:00"))
												$cust_new = true;
											else
												$cust_new = false;
											if($membership_expiration!="" && $membership_expiration >= date("Y-m-d") && $membership_expration <= "9999-99-99")
												{$mem = true; $bgf = "images/btn_credit2.png";}
											else 	
												{$mem = false;$bgf = "images/btn_credit.png";}
											
											echo $extract_r['credits'];
											if($cust_new) echo "<font style='font-size:8px; color:#008800'>n</font>";
											if($mem) echo "<font style='font-size:8px; color:#000088'>M</font>";
											
											echo "</td></tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>";
				}
				for($r = $rowcount; $r < $minrows; $r++) {
					echo "<tr>
						<td height='46' align='center' valign='top'>
							<table width='417' height='46' border='0' cellpadding='0' cellspacing='0'>
								<tr>
									<td width='52' align='center' valign='middle' class='subtitles'>&nbsp;</td>
									<td width='51' align='center' valign='middle' background='images/tab_bg_51.png'>&nbsp;</td>
									<td width='262' align='left' valign='middle' background='images/tab_bg_262.png' >
										<table width='262' border='0' cellspacing='0' cellpadding='8'>
											<tr><td class='nametext'>&nbsp;</td></tr>
										</table>
									</td>
									<td width='52' align='left' valign='top' background='images/btn_credit.png' >
										<table width='50' height='46' border='0' cellspacing='0' cellpadding='6'>
											<tr><td align='center' valign='middle' class='creditnumber'>&nbsp;</td></tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>";
				}
			?>
    </table>
		<script language='javascript'>
			<?php
				echo "schedule_ids = new Array();\n" . $js_schedule_ids . "\n";
				echo "next_row_select = new Array();\n" . $js_next_row_select . "\n";
			?>
			row_selected = <?php echo $row_selected?>;
		</script>
	</body>
</html>
<?php

} else if($uripage =="pit_racers_wrapper.php") {
//--------------------------------------------------- pit_racers_wrapper.php ----------------------------------------------//
?>

<html>
	<head>
		<title>Untitled Document</title>
		<style type="text/css">
<!--
body {
	background-color: transparent;
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}

.carnumber {	font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #FFF;
}
.creditnumber {	font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #3d4f6d;
}
.nametext {	font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
}
.gridposition {	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #5A75A0;
}
.icontext {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 11px;
	font-weight: bold;
	color: #5A75A0;
}
.subtitles {	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #5A75A0;
}
.title1 {	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 18px;
	font-weight: bold;
	color: #5A75A0;
}
-->
		</style>
	</head>

<?php
	//require_once(dirname(__FILE__) . "/../comconnect.php");
	
	$eventid = 0;
	if(reqvar("eventid")) $eventid = reqvar("eventid");
	else if(sessvar_isset("eventid")) $eventid = sessvar("eventid");
	set_sessvar("eventid",$eventid);
	
	$print_event = reqvar("print");
	if($print_event)
	{
		if(strpos($company_code,"jersey")) $ploc = "p2r_new_jersey%20-%20NYJC";
		else if(strpos($company_code,"corona")!==false) $ploc = "p2r_corona%20-%20P2R%20Corona";
		else if(strpos($company_code,"DEV")!==false) $ploc = "DEVKART%20-%20Dev%20East%20Side";
		else {
			if(reqvar("loc_id")==9)
				$ploc = "pole_position_raceway%20-%20Murrieta";
			else
				$ploc = "pole_position_raceway%20-%20Oklahoma";
		}
		
		$track_query = lavu_query("SELECT `lk_tracks`.`title` as `track_title` from `lk_events` LEFT JOIN `lk_tracks` ON `lk_events`.`trackid`=`lk_tracks`.`id` WHERE `lk_events`.`id`='[1]'",$eventid);
		if(lysql_num_rows($track_query))
		{
			$track_read = lysql_fetch_assoc($track_query);
			$track_title = $track_read['track_title'];
		}
		else $track_title = "";
		
		$print_to = "printer1";
		if(strpos($track_title,"2")!==false || strpos(strtolower($track_title),"second")!==false) $print_to = "printer2";
		else if(strpos($track_title,"3")!==false) $print_to = "printer3";
		else if(strpos($track_title,"4")!==false) $print_to = "printer4";
		
		//mail("corey@poslavu.com","print event","$company_code \n $ploc \nprint to: $print_to \ntrack: $track_title","From: info@poslavu.com");
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $server_url_path . "/print/index.php?location=$ploc&print_to=".$print_to."&print=".$print_event);
			//curl_setopt($ch, CURLOPT_VERBOSE, 1);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_POST, 1);
			//curl_setopt($ch, CURLOPT_POSTFIELDS, $nvpreq);
			$httpResponse = curl_exec($ch);
			echo "<script language='javascript'>alert('Printing...');</script>";
	}

	$lane_number = 1;
	if(reqvar("lane_number")) $lane_number = reqvar("lane_number");
	else if(sessvar_isset("lane_number")) $lane_number = sessvar("lane_number");
	set_sessvar("lane_number",$lane_number);

	$race_title = "";
	$race_time = "";
	$trackid = "";

	$get_race_info = lavu_query("SELECT `lk_event_types`.`title` as `title`, `lk_events`.`time` as `time`, `lk_events`.`trackid` as `trackid` FROM `lk_events` LEFT JOIN `lk_event_types` ON `lk_events`.`type` = `lk_event_types`.`id` WHERE `lk_events`.`id` = '".$eventid."'");
	if (@lysql_num_rows($get_race_info) > 0) {
		$extract = lysql_fetch_array($get_race_info);
		$race_title = $extract['title'];
		$race_time = $extract['time'];
		$trackid = $extract['trackid'];
	}
	
	$startrace = reqvar("startrace");
	if($startrace && $startrace==$eventid)
	{
		lavu_query("update `lk_events` set `ms_start`='".date("Y-m-d H:i:s")."' where `id`='[1]'",$startrace);
	}
	$endrace = reqvar("endrace");
	if($endrace)
	{
		lavu_query("update `lk_events` set `ms_end`='".date("Y-m-d H:i:s")."' where `id`='[1]'",$endrace);
	}
	$continue_race = reqvar("continue_race");
	if($continue_race)
	{
		function time_to_ts($time)
		{
			$parts = explode(" ",$time);
			$date_parts = explode("-",$parts[0]);
			$time_parts = explode(":",$parts[1]);
			$ts = mktime($time_parts[0],$time_parts[1],$time_parts[2],$date_parts[1],$date_parts[2],$date_parts[0]);
			return $ts;
		}
		
		$e_query = lavu_query("SELECT * from `lk_events` where `id`='[1]'",$continue_race);
		if(lysql_num_rows($e_query))
		{
			$e_read = lysql_fetch_assoc($e_query);
			
			$ts_now = time();
			$ts_end = time_to_ts($e_read['ms_end']);
			$ts_span = $ts_now - $ts_end;
			$pause_data = $ts_now . "-" . $ts_end . "=" . $ts_span;
			lavu_query("update `lk_events` set `ms_end`='', `pauses`=CONCAT(`pauses`,'$pause_data ') where `id`='[1]'",$continue_race);
		}
	}
	
	$race_active = false;
	$active_query = lavu_query("select * from `lk_events` where `ms_start`!='' and `ms_end`='' and `trackid`='[1]'",$trackid);
	if(lysql_num_rows($active_query))
	{
		$active_read = lysql_fetch_assoc($active_query);
		$active_raceid = $active_read['id'];
		$race_active = true;
	}
	
	$event_exists = false;
	$event_active = false;
	$event_query = lavu_query("select * from `lk_events` where `id`='[1]'",$eventid);
	if(lysql_num_rows($event_query))
	{
		$event_exists = true;
		$event_active = true;
		$event_read = lysql_fetch_assoc($event_query);
		if($event_read['ms_end']!="")
			$event_active = false;
	}
?>

	<body>
		<table width="474" height="598" border="0" cellpadding="0" cellspacing="0" >
			<tr>
				<td width="79" height="486" align="left" valign="top">
					<table width="79" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="11">&nbsp;</td>
							<td height="26">&nbsp;</td>
						</tr>
						<tr>
							<td width="11">&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td width="11">&nbsp;</td>
							<td height="110" align="left" valign="bottom"></td>
						</tr>
					</table>
				</td>
				<td align="left" valign="top">
					<table width="395" height="508" border="0" cellspacing="0" cellpadding="0">
						<tr><td height="23" background="images/line_top.png"></td></tr>
						<tr><td height="482" align="left" valign="top">&nbsp;</td></tr>
						<tr><td height="8" background="images/line_bot.png"></td></tr>
					</table>
				</td>
			</tr>
            <tr>
            	<td height="50" colspan='2' align='right' valign="top">

					
                    <table>
                    	<tr>
							<td valign="top" class="title1" align='right'><?php if($eventid > 0) echo $race_title . "<br>" . display_time($race_time); ?></td>
                            <td width='11'>&nbsp;</td>
						<?php if($event_active) {
									if($race_active) { 
						?>
							<td width="11">&nbsp;</td>
							<td align="left">
								<table width="68" border="0" cellpadding="0" cellspacing="0">
									<tr><td height="32" align="center" valign="top" class="subtitles">
                                    <a onclick='location = "_DO:cmd=load_url&c_name=pit_racers_wrapper&f_name=lavukart/pit_racers_wrapper.php?endrace=<?php echo $active_raceid?>";'><img src="images/btn_endrace.png" width="68" height="68" /></a></td></tr>
									<!--$ <tr><td height="32" align="center" valign="middle" class="subtitles"><a onclick='location = "_DO:cmd=create_overlay&f_name=lavukart/pit_grid_overlay_wrapper.php;lavukart/pit_grid.php&c_name=pit_grid_overlay_wrapper;pit_grid&dims=275,90,474,598;302,130,420,540&scroll=0;1?test1=1&test2=2";'><img src="images/btn_grid.png" width="68" height="68" /></a></td></tr> $-->
									<!--<tr><td height="32" align="center" valign="middle"><span class="icontext">STOP</span></span></td></tr>-->
								</table>
							</td>
						<?php 		} else { ?>
							<td width="11">&nbsp;</td>
							<td align="left">
								<table width="68" border="0" cellpadding="0" cellspacing="0">
									<tr><td height="32" align="center" valign="top" class="subtitles"><a onclick='location = "_DO:cmd=load_url&c_name=pit_racers_wrapper&f_name=lavukart/pit_racers_wrapper.php?startrace=<?php echo $eventid?>";'><img src="images/btn_startrace.png" width="68" height="68" /></a></td></tr>
									<!--$ <tr><td height="32" align="center" valign="middle" class="subtitles"><a onclick='location = "_DO:cmd=create_overlay&f_name=lavukart/pit_grid_overlay_wrapper.php;lavukart/pit_grid.php&c_name=pit_grid_overlay_wrapper;pit_grid&dims=275,90,474,598;302,130,420,540&scroll=0;1?test1=1&test2=2";'><img src="images/btn_grid.png" width="68" height="68" /></a></td></tr> $-->
									<!--<tr><td height="32" align="center" valign="middle"><span class="icontext">START</span></span></td></tr>-->
								</table>
							</td>
						<?php } } else if(!$race_active && $event_exists) { ?>
							<td width="11">&nbsp;</td>
							<td align="left">
								<table width="68" border="0" cellpadding="0" cellspacing="0">
									<tr><td height="32" align="center" valign="top" class="subtitles">
										<a onclick='location = "_DO:cmd=load_url&c_name=pit_racers_wrapper&f_name=lavukart/pit_racers_wrapper.php?continue_race=<?php echo $eventid?>";'><img src="images/btn_continue_race.png" width="68" height="68" /></a>
								</td></tr>
									<!--$ <tr><td height="32" align="center" valign="middle" class="subtitles"><a onclick='location = "_DO:cmd=create_overlay&f_name=lavukart/pit_grid_overlay_wrapper.php;lavukart/pit_grid.php&c_name=pit_grid_overlay_wrapper;pit_grid&dims=275,90,474,598;302,130,420,540&scroll=0;1?test1=1&test2=2";'><img src="images/btn_grid.png" width="68" height="68" /></a></td></tr> $-->
									<!--<tr><td height="32" align="center" valign="middle"><span class="icontext">CONTINUE</span></span></td></tr>-->
								</table>
							</td>
						<?php } ?>
						<?php if($event_exists) { ?>
							<td width="11">&nbsp;</td>
							<td align="left">
								<table width="68" border="0" cellpadding="0" cellspacing="0">
									<tr><td height="32" align="center" valign="top" class="subtitles"><a onclick='location = "_DO:cmd=load_url&c_name=pit_racers_wrapper&f_name=lavukart/pit_racers_wrapper.php?print=<?php echo $eventid?>";'><img src="images/btn_printer.png" width="68" height="68" /></a></td></tr>
									<!--$ <tr><td height="32" align="center" valign="middle" class="subtitles"><a onclick='location = "_DO:cmd=create_overlay&f_name=lavukart/pit_grid_overlay_wrapper.php;lavukart/pit_grid.php&c_name=pit_grid_overlay_wrapper;pit_grid&dims=275,90,474,598;302,130,420,540&scroll=0;1?test1=1&test2=2";'><img src="images/btn_grid.png" width="68" height="68" /></a></td></tr> $-->
									<!--<tr><td height="32" align="center" valign="middle"><span class="icontext">PRINT RESULTS</span></span></td></tr>-->
								</table>
							</td>
						<?php } ?>
                       	<td width='7'>&nbsp;</td>
					</table>



                </td>
            </tr>
		</table>
	</body>
</html>
<?php

} else if($uripage =="pit_schedule.php") {
//--------------------------------------------------- pit_schedule.php ----------------------------------------------//
?>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Untitled Document</title>

		<style type="text/css">
<!--
body {
	background-color: transparent;
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}

.creditnumber {font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #3d4f6d;
}
.nametext {font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
}
.active_nametext {font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #00aa00;
}
.finished_nametext {font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color:#999999;
}
.subtitles {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #5A75A0;
}
.style411 {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #5A75A0;
}
.gridposition {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #5A75A0;
}
.icontext {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 11px;
	font-weight: bold;
	color: #5A75A0;
}
.subtitles1 {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-weight: bold;
	color: #5A75A0;
}
.title1 {	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 18px;
	font-weight: bold;
	color: #5A75A0;
}
-->
		</style>
		<script language="javascript">
			function findTopPos(objid) 
			{
				obj = document.getElementById(objid);
				var curleft = curtop = 0;
				if (obj.offsetParent) 
				{
					do 
					{
						curleft += obj.offsetLeft;
						curtop += obj.offsetTop;
					} while (obj = obj.offsetParent);
					return curtop;
				}
				else return 0;
			}			
		</script>
	</head>

<?php
	//require_once(dirname(__FILE__) . "/../comconnect.php");
	//ini_set("display_errors","1");
	/*if(reqvar("eventid")) $eventid = reqvar("eventid");
	else if(sessvar_isset("eventid")) $eventid = sessvar("eventid");
	set_sessvar("eventid",$eventid);*/
	$eventid = sessvar("eventid");
	$row_selected = 0;
	$set_scrolltop = reqvar("set_scrolltop");
	$mode = reqvar("mode");
	if($mode=="add_event")
	{
		$set_date = reqvar("event_date");
		$t = reqvar("event_time");
		$set_type = reqvar("event_type");
		$set_locationid = reqvar("event_locationid");
		$set_trackid = reqvar("event_trackid");
		
		$dparts = explode("-",$set_date);
		$set_year = $dparts[0];
		$set_month = $dparts[1];
		$set_day = $dparts[2];
		$set_min = $t % 100;
		$set_hour = (($t - $set_min) / 100);
		$set_time = $t;
		$set_ts = mktime($set_hour,$set_min,0,$set_month,$set_day,$set_year);

		lavu_query("insert into `lk_events` (`locationid`,`type`,`date`,`time`,`ts`,`trackid`) values ('$set_locationid','$set_type','$set_date','$set_time','$set_ts','$set_trackid')");
		//echo "insert into `lk_events` (`locationid`,`type`,`date`,`time`,`ts`,`trackid`) values ('$set_locationid','$set_type','$set_date','$set_time','$set_ts','$set_trackid')";
	}
	else if($mode=="move_event")
	{
		$mtvars = array();
		$mtvars['date'] = reqvar("moveto_date");
		$mtvars['time'] = reqvar("moveto_slot");
		$mtvars['id'] = $eventid;//reqvar("eventid");

		$t = $mtvars['time'];
		$dparts = explode("-",$mtvars['date']);
		$set_year = $dparts[0];
		$set_month = $dparts[1];
		$set_day = $dparts[2];
		$set_min = $t % 100;
		$set_hour = (($t - $set_min) / 100);
		$set_time = $t;
		$set_ts = mktime($set_hour * 1,$set_min * 1,0,$set_month * 1,$set_day * 1,$set_year * 1);
		$mtvars['ts'] = $set_ts;
		
		$trackid = sessvar("trackid");
		$mtvars['trackid'] = $trackid;
		$track_query = lavu_query("select * from `lk_tracks` where id='[1]'",$trackid);
		if(lysql_num_rows($track_query))
		{
			$track_read = lysql_fetch_assoc($track_query);
			$time_span = $track_read['time_interval'];
			
			$move_events_queue = array();
			$slot_clear = false;
			$ctime = $mtvars['time'];
			$cdate = $mtvars['date'];
			
			$set_min = $ctime % 100;
			$set_hour = (($ctime - $set_min) / 100);
			$cts = mktime($set_hour * 1,$set_min * 1,0,$set_month * 1,$set_day * 1,$set_year * 1);
			$loopcount = 0;
						
			while($slot_clear==false && $loopcount < 300)
			{			
				$e_query = lavu_query("select * from `lk_events` where `trackid`='[1]' and `ts`='[2]' and `id`!='[3]'",$trackid,$cts,$eventid);
				if(lysql_num_rows($e_query))
				{
					$ctime = time_advance($ctime,$time_span);
					$set_min = $ctime % 100;
					$set_hour = (($ctime - $set_min) / 100);
					$cts = mktime($set_hour * 1,$set_min * 1,0,$set_month * 1,$set_day * 1,$set_year * 1);
					while($e_read = lysql_fetch_assoc($e_query))
					{
						$move_events_queue[] = array($e_read['id'],$cdate,$ctime,$cts);
					}
				}
				else
				{
					$slot_clear = true;
				}
				$loopcount++;
			}
			for($n=0; $n<count($move_events_queue); $n++)
			{
				$cvars['id'] = $move_events_queue[$n][0];
				$cvars['date'] = $move_events_queue[$n][1];
				$cvars['time'] = $move_events_queue[$n][2];
				$cvars['ts'] = $move_events_queue[$n][3];
				//$cvars['trackid'] = 
				//mail("corey@poslavu.com","forward: " . $move_events_queue[$n][0],"date: " . $move_events_queue[$n][1] . " time: " . $move_events_queue[$n][2] . " ts: " . $move_events_queue[$n][3],"From: info@poslavu.com");
				lavu_query("update `lk_events` set `date`='[date]', `time`='[time]', `ts`='[ts]' where `id`='[id]",$cvars);
			}
			
			lavu_query("update `lk_events` set `date`='[date]', `time`='[time]', `ts`='[ts]', `trackid`='[trackid]' where `id`='[id]'",$mtvars);
		}
	}
	else if($mode=="remove_event")
	{
		$event_query = lavu_query("select * from `lk_events` where id='[1]'",$eventid);
		if(lysql_num_rows($event_query))
		{
			$event_read = lysql_fetch_assoc($event_query);
			$date_parts = explode("-",$event_read['date']);
			if(count($date_parts) > 2)
			{
				$year = $date_parts[0];
				$month = $date_parts[1];
				$day = $date_parts[2];
			}
			lavu_query("delete from `lk_events` where `id`='[1]'",$eventid);
		}
	}
	else if($mode=="set_track")
	{
		$trackid = reqvar("set_trackid");
		set_sessvar("trackid",$trackid);
	}
	
	if(sessvar_isset("trackid"))
		$trackid = sessvar("trackid");
	else
		$trackid = false;
	$anchor_now = "page_body";
?>

	<body id="page_body" onLoad="body_loaded()">
		<table width="295" height="535" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td height="535" align="left" valign="top">
					<table width="282" border="0" cellspacing="0" cellpadding="0">
             <?php
							$minrows = 10;
							$rowcount = 0;
							
							if(postvar("day")==false && $eventid)
							{
								$event_query = lavu_query("select * from `lk_events` where id='[1]'",$eventid);
								if(lysql_num_rows($event_query))
								{
									$event_read = lysql_fetch_assoc($event_query);
									$date_parts = explode("-",$event_read['date']);
									if(count($date_parts) > 2)
									{
										$year = $date_parts[0];
										$month = $date_parts[1];
										$day = $date_parts[2];
									}
								}
								else
								{
									$year = postvar("year",date("Y"));
									$month = postvar("month",date("m"));
									$day = postvar("day",date("d"));
								}
							}
							else
							{
								$year = postvar("year",date("Y"));
								$month = postvar("month",date("m"));
								$day = postvar("day",date("d"));
							}
							if($month < 10) $month = "0" . ($month * 1);
							if($day < 10) $day = "0" . ($day * 1);
							$fulldate = $year . "-" . $month . "-" . $day;
							//echo $fulldate;

							function create_timeslot_row($date,$slot,$trackid,$locationid,$eventid=false)
							{
								?>
                                <tr>
                                    <td height="46" id="timeslot_<?php echo $slot?>"><table width="79" border="0" cellspacing="0" cellpadding="8">
                                        <tr>
                                            <td class="subtitles1"><nobr>
											<?php
												echo display_time($slot); 
											?>
											</nobr></td> 
                                        </tr>
                                    </table></td>
                                    <td width="263" align="left" valign="middle" background="images/tab_short_bg2.png"><table width="263" border="0" cellspacing="0" cellpadding="8">
                                        <tr>
                                            <td class="nametext">&nbsp;</td>
                                        </tr>
                                    </table></td>
                                </tr>
                                <?php
							}
							
							$locationid = reqvar("loc_id");
							$closest_time = 0;
							$timeslot_selected = 0;
							
							if($trackid)
							{
								$track_query = lavu_query("select * from `lk_tracks` where id='[1]' and `locationid`='[2]'",$trackid,$locationid);
								if(!lysql_num_rows($track_query))
									$trackid = false;
							}
							
							if(!$trackid)
							{
								$track_query = lavu_query("select * from `lk_tracks` where `locationid`='$locationid' and `_deleted`!='1' order by id asc limit 1");
							}
							if(!lysql_num_rows($track_query))
							{
								echo "Error: No Tracks exist for this location";
								exit();
							}
							
							$track_read = lysql_fetch_assoc($track_query);
							$time_start = $track_read['time_start'];
							$time_end = $track_read['time_end'];
							$time_span = $track_read['time_interval'];
							$trackid = $track_read['id'];
							$track_title = $track_read['title'];
							set_sessvar("trackid",$trackid);
							
							if(!is_numeric($time_start)) $time_start = 1100;
							if(!is_numeric($time_end)) $time_end = 2200;
														
							//echo "<tr><td><td align='center' class='subtitles1'><b>$month/$day/$year</b></td></tr>";
							/*if($year==date("Y") && $month==date("m") && $day==date("d"))
							{
								$mints = current_ts(-60); //mktime($hour - 1,$minute,0,$month,$day,$year);
							}
							else*/
								$mints = mktime(0,0,0,$month*1,$day*1,$year*1);
							$maxts = mktime(24,0,0,$month*1,$day*1,$year*1);
							
							$current_time = $time_start;
							function time_advance($time, $add)
							{
								$time_hours = $time - ($time % 100);
								$time_minutes = $time % 100;
								$add_hours = $add - ($add % 100);
								$add_minutes = $add % 100;
								$hours = $time_hours + $add_hours;
								$minutes = $time_minutes + $add_minutes;
								if($minutes >= 60)
								{
									$hours += 100;
									$minutes -= 60;
								}
								return $hours + $minutes;
							}
					
							$event_query = lavu_query("select `lk_events`.`ts` as `ts`, `lk_events`.`time` as time, `lk_events`.`id` as id, `lk_events`.`ms_start` as `ms_start`, `lk_events`.`ms_end` as `ms_end`, `lk_event_types`.`title` as `title`, `lk_event_types`.`slots` as `slots` from `lk_events` left join `lk_event_types` on lk_events.type = lk_event_types.id where `trackid`='[1]' and `ts`>='$mints' and `ts`<='$maxts' order by `ts` asc",$trackid);
							while($event_read = lysql_fetch_assoc($event_query))
							{
								
								while($current_time < $event_read['time'])
								{
									$rowcount++;
									if($current_time >= date("Hi",$mints))
									{
										create_timeslot_row($fulldate,$current_time,$trackid,$locationid,$eventid);
										if(abs($current_time - date("Hi")) < abs($current_time - $closest_time))
											$closest_time = $current_time;
									}
									$current_time = time_advance($current_time,$time_span);
								}
								if($current_time < time_advance($event_read['time'],$time_span))
									$current_time = time_advance($current_time,$time_span);
								$rowcount++;
								
								if(abs($event_read['time'] - date("Hi")) < abs($event_read['time'] - $closest_time))
									$closest_time = $event_read['time'];
								?>
								<tr>
									<td width="69" height="46" id="timeslot_<?php echo $event_read['time'];?>">
										<table width="69" border="0" cellspacing="0" cellpadding="8">
											<tr><td class="subtitles1"><nobr>
											<?php
												/*if($eventid)
												{
													echo "<a onclick='if(confirm(\"Are you sure you want to move this event?\")) location = \"_DO:cmd=load_url&c_name=events&f_name=lavukart/events.php?mode=move_event&eventid=".$eventid."&moveto_date=".$fulldate."&moveto_slot=".$event_read['time']."&abc=123\"; '>".display_time($event_read['time'])."</a>"; 
												}
												else
												{ */
													echo display_time($event_read['time']); 
												//}
											?>
											</nobr></td></tr>
										</table>
									</td>
                                    <?php
										$start_style = "";
										$use_bg = "images/tab_short_bg2.png";
										if($eventid==$event_read['id'])
										{
											$use_bg = "images/tab_short_bg2_lit.png";
											$row_selected = $rowcount;
											$timeslot_selected = $event_read['time'];
										}
										
										$comp_cmd = "location = \"_DO:cmd=load_url&c_name=pit_racers;pit_racers_wrapper&f_name=lavukart/pit_racers.php;lavukart/pit_racers_wrapper.php?mode=schedule&eventid=".$event_read['id']."&abc=123\";";
										$tdclass = "nametext";
										if($event_read['ms_start']!="" && $event_read['ms_end']=="")
										{
											$tdclass = "active_nametext";
										}
										else if($event_read['ms_end']!="")
										{
											$tdclass = "finished_nametext";
										}
									?>
									<td width="263" id="event_row_<?php echo $rowcount?>" style="background: URL(<?php echo $use_bg?>);" onclick='if(row_selected) document.getElementById("event_row_" + row_selected).style.background = "URL(images/tab_short_bg2.png)"; row_selected = "<?php echo $rowcount?>"; this.style.background = "URL(images/tab_short_bg2_lit.png)";<?php echo $comp_cmd; ?>'>
										<table width="263" border="0" cellspacing="0" cellpadding="8">
											<tr><td class="<?php echo $tdclass;?>">
												<?php echo ucfirst($event_read['title']); ?>
                                                <?php 
													echo "&nbsp;(";
													$sched_query = lavu_query("select count(*) as sched_count from `lk_event_schedules` where `eventid`='[1]' and _deleted!='1'",$event_read['id']);
													if(lysql_num_rows($sched_query))
													{
														$sched_read = lysql_fetch_assoc($sched_query);
														$sched_count = $sched_read['sched_count'];
													}
													else $sched_count = 0;
													echo $sched_count . "/";
													if($event_read['slots']=="") echo "10"; else echo $event_read['slots'];
													echo ")";
												?>
                                            </td></tr>
										</table>
									</td>
								</tr>
                                <?php
							}
							while($current_time < $time_end)
							{
								$rowcount++;
								if($current_time >= date("Hi",$mints))
									create_timeslot_row($fulldate,$current_time,$trackid,$locationid,$eventid);
								$current_time = time_advance($current_time,$time_span);
							}
							$rowcount++;
							?>
					</table>
					<script language='javascript'>row_selected = <?php echo $row_selected?>;</script>
				</td>
			</tr>
		</table>
		<script language="javascript">
			function body_loaded() {
				<?php 
					if($timeslot_selected > 0) {
						$anchor_now = "timeslot_".$timeslot_selected;
					}
					else if($closest_time > 0) {
						$anchor_now = "timeslot_".$closest_time;
					}
					if(!$set_scrolltop || $set_scrolltop=="")
					{
						$set_scrolltop = "(findTopPos('$anchor_now') - 40)";
					}
					//if(strpos($company_code,"DEV")!==false) {echo "alert(\"pos: \" + $set_scrolltop); ";}
					echo "window.scrollTo(0,$set_scrolltop); ";
				?>
				document.getElementById("page_body").style.visibility = "visible";
			}
		</script>
	</body>
</html>
<?php

} else if($uripage =="pit_schedule_wrapper.php") {
//--------------------------------------------------- pit_schedule_wrapper.php ----------------------------------------------//
?>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Untitled Document</title>

		<style type="text/css">
<!--
body {
	background-color: transparent;
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}

.creditnumber {font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #3d4f6d;
}
.nametext {font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
}
.subtitles {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #5A75A0;
}
.style411 {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #5A75A0;
}
.gridposition {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #5A75A0;
}
.icontext {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 11px;
	font-weight: bold;
	color: #5A75A0;
}
.subtitles1 {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #5A75A0;
}
.title1 {	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 18px;
	font-weight: bold;
	color: #5A75A0;
}
-->
		</style>
	</head>
	
<?php	
	//require_once(dirname(__FILE__) . "/../comconnect.php");
	
	$mode = reqvar("mode");
	$locationid = reqvar("loc_id");
	$eventid = sessvar("eventid");
	
	if($mode=="set_track")
	{
		$trackid = reqvar("set_trackid");
	}
	else
	{
		if(sessvar_isset("trackid"))
			$trackid = sessvar("trackid");
		else
			$trackid = false;
	}
	if($trackid)
	{
		$track_query = lavu_query("select * from `lk_tracks` where id='[1]' and `locationid`='[2]'",$trackid,$locationid);
		if(!lysql_num_rows($track_query))
			$trackid = false;
	}
	
	if(!$trackid)
	{
		$track_query = lavu_query("select * from `lk_tracks` where `locationid`='$locationid' and `_deleted`!='1' order by id asc limit 1");
	}
	if(!lysql_num_rows($track_query))
	{
		$track_title = "&nbsp;";
	}
	else
	{	
		$track_read = lysql_fetch_assoc($track_query);
		$time_start = $track_read['time_start'];
		$time_end = $track_read['time_end'];
		$time_span = $track_read['time_interval'];
		$trackid = $track_read['id'];
		$track_title = $track_read['title'];
	}
	
	$year = reqvar("year");
	$month = reqvar("month");
	$day = reqvar("day");
	
	if(!$year && $eventid)
	{
		$event_query = lavu_query("select * from `lk_events` where id='[1]'",$eventid);
		if(lysql_num_rows($event_query))
		{
			$event_read = lysql_fetch_assoc($event_query);
			$set_date = $event_read['date'];
			$date_parts = explode("-",$set_date);
			if(count($date_parts) > 2)
			{
				$year = $date_parts[0];
				$month = $date_parts[1];
				$day = $date_parts[2];
			}
		}
	}
	if(!$year) 
	{
		$year = date("Y");
		$month = date("m");
		$day = date("d");
	}
	$date_ts = mktime(0,0,0,$month,$day,$year);
	$show_date = date("M d",$date_ts);
	$cal_code = "_DO:cmd=create_overlay&f_name=lavukart/calendar_wrapper.php;lavukart/calendar.php&c_name=calendar_wrapper;calendar&dims=112,200,423,374;112,200,423,374?tab_name=new_pit";
?>

	<body>
		<table width="474" height="398" border="0" cellpadding="0" cellspacing="0" >
			<tr>
				<td width="77" height="336" align="right" valign="top">
					<table><tr><td height="42" valign="bottom"><a href="<?php echo $cal_code?>"><img src="images/btn_calendar.png" width="34" height="34" border="0" /></a></td></tr></table>
                    <!--
					<table width="68" border="0" cellpadding="0" cellspacing="0">
						<tr><td height="120" align="center" valign="middle" class="subtitles">&nbsp;</td></tr>
						<tr><td height="32" align="center" valign="middle" class="subtitles"><a href="_DO:cmd=create_overlay&f_name=lavukart/calendar_wrapper.php;lavukart/calendar.php&c_name=calendar_wrapper;calendar&dims=112,200,423,374;112,200,423,374"><img src="images/btn_calendar.png" width="68" height="68" border="0"/></a></td></tr>
						<tr><td height="32" align="center" valign="middle"><span class="icontext">CALENDAR</span></span></td></tr>
						<tr><td height="20" align="center" valign="middle" class="subtitles">&nbsp;</td></tr>
						<tr><td height="32" align="center" valign="middle" class="subtitles"><a href="_DO:cmd=create_overlay&f_name=lavukart/newevent_wrapper.php;lavukart/select_track.php&c_name=select_track_wrapper;select_track&dims=112,200,423,374;112,200,423,374?locid=<?php echo $_REQUEST['loc_id']; ?>"><img src="images/btn_select_track.png" width="68" height="68" border="0"/></a></td></tr>
						<tr><td height="32" align="center" valign="middle"><span class="icontext">TRACKS</span></span></td></tr>
					</table>
                    -->
                    &nbsp;
				</td>
				<td align="left" valign="top">
					<table width="395" height="398" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td height="40">
								<table width="392" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td width="119" height="46">
											<table width="119" border="0" cellspacing="0" cellpadding="8">
												<tr><td width="119" class="title1" onClick="window.location = '<?php echo $cal_code?>'"><?php echo "<b>".$show_date."</b>"?></td></tr>
											</table>
										</td>
										<td width="233" >
											<table width="233" border="0" cellpadding="8" cellspacing="0">
												<tr><td align="center" class="title1"><font color="#4A5580"><?php echo "<b>".$track_title."</b>"?></font></td></tr>
											</table>
										</td>
										<td width="80">&nbsp;</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td height="335" align="left" valign="top">
								<table cellspacing="0" cellpadding="0">
									<tr>
										<td><img src="images/line.png" width="1" height="535"></td>
									</tr>
								</table>
							</td>
						</tr>
						<tr><td height="23" background="images/line_bot.png"></td></tr>
					</table>
				</td>
			</tr>
		</table>
	</body>
</html>
<?php

} else if($uripage =="print_sheet.php") {
//--------------------------------------------------- print_sheet.php ----------------------------------------------//
?>

<?php
	////require_once(dirname(__FILE__) . "/../comconnect.php");
	
	if(isset($_GET['sent'])) $eventid = $_GET['sent'];
	if(isset($_GET['data'])) $eventid = $_GET['data'];
	
	if($eventid) 
	{
		$_GET['mode'] = "results";
		if(!isset($_GET['cc']))
		{
			$_GET['cc'] = "pole_position_raceway_key_113838";
			$_GET['loc_id'] = 10;
			$_GET['trackid'] = 2;
		}
		$_GET['eventid'] = $eventid;
		require_once("race_scores.php");
	}
?>

<?php

} else if($uripage =="race_scores.php") {
//--------------------------------------------------- race_scores.php ----------------------------------------------//
?>

<?php
  	//require_once("../comconnect.php");
			
	$eventid = reqvar("eventid");
	$trackid = reqvar("trackid");
	$mode = reqvar("mode");
	
	$tsnow = date("YmdHis");
	
	$event_request = $eventid;
	
	if(!$trackid && $eventid)
	{
		$ev_query = lavu_query("select * from `lk_events` where `id`='[1]'",$eventid);
		if(lysql_num_rows($ev_query))
		{
			$ev_read = lysql_fetch_assoc($ev_query);
			$trackid = $ev_read['trackid'];
			$locationid = $ev_read['locationid'];
		}
	}
	
	// monitoring
	$qvals = array();
	$qvals['ipaddress'] = $_SERVER['REMOTE_ADDR'];
	$qvals['date'] = date("Y-m-d");
	$qvals['time'] = date("H:i:s");
	$qvals['ts'] = time();
	$qvals['ccname'] = (isset($company_code))?$company_code:"";
	$qvals['locationid'] = (isset($locationid))?$locationid:"";
	$qvals['mode'] = $mode;
	$qvals['eventid'] = $event_request;
	$qvals['trackid'] = $trackid;
	$qvals['details'] = "";
	$detail_update = "";
	
	$rcpt = reqvar("rcpt");
	if($rcpt)
	{
		$qvals['details'] = $_GET['rcpt'];
		$detail_update = ", `details`='[details]'";
	}
	
	$monitor_query = lavu_query("select * from `lk_monitor` where `ipaddress`='[ipaddress]' and `mode`='[mode]' and `ccname`='[ccname]' and `locationid`='[locationid]' and `trackid`='[trackid]' order by `ts` desc",$qvals);
	if(lysql_num_rows($monitor_query))
	{
		$monitor_read = lysql_fetch_assoc($monitor_query);
		$qvals['monitorid'] = $monitor_read['id'];
		if(isset($monitor_read['reload']) && $monitor_read['reload']!="")
		{
			if($monitor_read['reload'] > time() - 300)
				echo "&reload_swf=".$monitor_read['reload']."&";
		}
		lavu_query("update `lk_monitor` set `date`='[date]', `time`='[time]', `ts`='[ts]', `eventid`='[eventid]'".$detail_update." where `id`='[monitorid]'",$qvals);
	}
	else
	{
		lavu_query("insert into `lk_monitor` (`ipaddress`,`date`,`time`,`ts`,`ccname`,`lname`,`locationid`,`mode`,`eventid`,`trackid`,`details`) values ('[ipaddress]','[date]','[time]','[ts]','[ccname]','[lname]','[locationid]','[mode]','[eventid]','[trackid]','[details]')",$qvals);
	}
	// end monitoring
	
	if($eventid=="current_pit")
	{
		$fin_query = lavu_query("select * from `lk_events` where `ms_end`!='' and `trackid`='[1]' order by `ts` desc",$trackid);
		if(lysql_num_rows($fin_query))
		{
			$fin_read = lysql_fetch_assoc($fin_query);
			$finid = $fin_read['id'];
			
			$next_query = lavu_query("select * from `lk_events` where `trackid`='[1]' and `ts`>'[2]' and `ts`>='".current_ts(-360)."' and `ms_start`='' order by `ts` asc limit 1",$trackid,$fin_read['ts']);
			if(lysql_num_rows($next_query))
			{
				$next_read = lysql_fetch_assoc($next_query);
				$eventid = $next_read['id'];
			}
			else $eventid = "current";
			//mail("corey@poslavu.com","eventid: $eventid","eventid: $eventid trackid: $trackid ts > ".$find_read['ts']." ts > " . current_ts(-60),"From: debug@poslavu.com");
		}
		else $eventid = "current";
	}
	
	if($eventid=="current" || $eventid=="active")
	{
		$active_query = lavu_query("select * from `lk_events` where `ms_start`!='' and (`ms_end`='' or `ms_end` IS NULL) and `trackid`='[1]'",$trackid);
		if(lysql_num_rows($active_query))
		{
			$active_read = lysql_fetch_assoc($active_query);
			$eventid = $active_read['id'];
		}
	}
	
	if($eventid=="last")
	{
		$active_query = lavu_query("select * from `lk_events` where `ms_start`!='' and `ms_end`!='' and `trackid`='[1]' order by `ms_end` desc",$trackid);
		if(lysql_num_rows($active_query))
		{
			$active_read = lysql_fetch_assoc($active_query);
			$eventid = $active_read['id'];
		}
		//echo "eventid: $eventid <br>";
	}
	
	if($mode=="kart_assignment")
	{
		$kart_count = 0;
		$sched_query = lavu_query("select `lk_customers`.`f_name` as `f_name`,`lk_customers`.`l_name` as `l_name`,`lk_customers`.`racer_name` as `racer_name`,`lk_event_schedules`.`id` as `id`,`lk_karts`.`number` as `number` from `lk_event_schedules` LEFT JOIN `lk_customers` ON `lk_event_schedules`.`customerid`=`lk_customers`.`id`  LEFT JOIN `lk_karts` ON `lk_event_schedules`.`kartid`=`lk_karts`.`id` where `lk_event_schedules`.`eventid`='[1]'",$eventid);
		while($sched_read = lysql_fetch_assoc($sched_query))
		{
			$racer_name = $sched_read['racer_name'];
			$abbrev_name = trim(ucfirst($sched_read['f_name']) . " " . ucfirst(substr(trim($sched_read['l_name']),0,1)));
			if(trim($racer_name)=="")
				$racer_name = $abbrev_name;
			$racer_number = $sched_read['number'];
			$kart_count++;
			echo "<br>&kart_name".$kart_count."=".$abbrev_name."&";
			echo "<br>&kart_number".$kart_count."=".$racer_number."&";
		}
		echo "<br>&kart_count=".$kart_count."&";
	}
	else if($mode=="upcoming")
	{
		$year = date("Y");
		$month = date("m");
		$day = date("d");
		$hour = date("H");
		$minute = date("m");
		$mints = mktime($hour - 2,$minute,0,$month,$day,$year);
		$maxts = mktime($hour + 8,$minute,0,$month,$day,$year);
		
		$event_count = 0;
		$show_event_names = array("","Next Race","2nd Upcoming Race","3rd Upcoming Race","in 4 races","in 5 races","in 6 races");
		$event_query = lavu_query("select `lk_events`.`ts` as `ts`, `lk_events`.`time` as time, `lk_events`.`id` as id, `lk_event_types`.`title` as `title` from `lk_events` left join `lk_event_types` on lk_events.type = lk_event_types.id where `ts`>='$mints' and `ts`<='$maxts' and `lk_events`.`ms_start`='' and `lk_events`.`trackid`='[1]' order by `ts` asc limit 6",$trackid);
		while($event_read = lysql_fetch_assoc($event_query))
		{
			$eventid = $event_read['id'];
			$event_count++;
			echo "<br>&event".$event_count."_name=" . $show_event_names[$event_count] . "&";//date("g:i a",$event_read['ts'])."&";
			
			$racer_count = 0;
			$sched_query = lavu_query("select * from `lk_event_schedules` left join `lk_customers` on `lk_event_schedules`.`customerid`=`lk_customers`.`id` where `eventid`='$eventid'");
			while($sched_read = lysql_fetch_assoc($sched_query))
			{
				$racer_count++;
				$racer_name = $sched_read['racer_name'];
				$abbrev_name = trim(ucfirst($sched_read['f_name']) . " " . ucfirst(substr(trim($sched_read['l_name']),0,1)));
				if(trim($racer_name)=="")
					$racer_name = $abbrev_name;
				echo "<br>&event".$event_count."_racer".$racer_count."=" . $abbrev_name . "&";
			}
			echo "<br>&event".$event_count."_racer_count=" . $racer_count . "&";
		}
		echo "<br>&event_count=" . $event_count . "&";
	}
	else
	{
		if($eventid)
		{
			$display = "";
			$data = "";
			
			$event_title = "";
			$total_laps = 0;
			$score_by = "";
			$event_query = lavu_query("select * from `lk_events` where `id`='[1]'",$eventid);
			if(lysql_num_rows($event_query))
			{
				$event_read = lysql_fetch_assoc($event_query);
				$event_title = display_time($event_read['time']);
				$event_type = $event_read['type'];
				$event_date = $event_read['date'];
				
				$type_query = lavu_query("select * from `lk_event_types` where `id`='[1]'",$event_type);
				if(lysql_num_rows($type_query))
				{
					$type_read = lysql_fetch_assoc($type_query);
					$total_laps = $type_read['laps'];
					$score_by = $type_read['score_by'];
					$event_type_title = $type_read['title'];
					if($score_by==0) $event_race_format = "Best Lap";
					else if($score_by==1) $event_race_format = "Position"; 
					else $event_race_format = "";
					
					$event_title = $event_type_title;
				}
				
				$track_query = lavu_query("select * from `lk_tracks` where `id`='[1]'",$trackid);
				if(lysql_num_rows($track_query))
				{
					$track_read = lysql_fetch_assoc($track_query);
					$track_name = $track_read['title'];
					$track_sec_int = $track_read['seconds_intermediate'];
					$track_sec_adv = $track_read['seconds_advanced'];
				}
			}
			$data .= "&event_title=$event_title&";
			$data .= "&total_laps=$total_laps&";
			
			$track_first_pass = "";
			$fastest_lap_all = "";
			$sched_query = lavu_query("select `lk_event_schedules`.`kartid` as `kartid`,`lk_customers`.`f_name` as `f_name`,`lk_customers`.`l_name` as `l_name`,`lk_customers`.`racer_name` as `racer_name`,`lk_event_schedules`.`id` as `id`,`lk_customers`.`id` as `custid`,`lk_karts`.`number` as `number` from `lk_event_schedules` LEFT JOIN `lk_customers` ON `lk_event_schedules`.`customerid`=`lk_customers`.`id`  LEFT JOIN `lk_karts` ON `lk_event_schedules`.`kartid`=`lk_karts`.`id` where `lk_karts`.`number`!='' and `lk_event_schedules`.`eventid`='[1]'",$eventid);
			if(lysql_num_rows($sched_query))
			{
				$racer_info = array();
				$lap_first_pass = array();
				$all_laps = array();
				$highest_lap = 0;
				
				$lap_grid = array();
				while($sched_read = lysql_fetch_assoc($sched_query))
				{
					$schedid = $sched_read['id'];
					
					$racer_name = $sched_read['racer_name'];
					if(trim($racer_name)=="")
						$racer_name = trim($sched_read['f_name'] . " " . $sched_read['l_name']);
						
					$racer_info[$schedid]['name'] = $racer_name;
					$racer_info[$schedid]['custid'] = $sched_read['custid'];
					$racer_info[$schedid]['number'] = $sched_read['number'];
					$racer_info[$schedid]['kartid'] = $sched_read['kartid'];
					$racer_info[$schedid]['schedid'] = $schedid;
					
					$all_laps[$schedid] = array();
					
					$lap_grid_info = array();
					$lap_grid_info['racer_name'] = $racer_name;
					$lap_grid_info['number'] = $sched_read['number'];
					$lap_grid_info['custid'] = $sched_read['custid'];
					$lap_grid_info['schedid'] = $schedid;
					$lap_grid_info['laps'] = array();
					
					$r_lap = 0;
					$r_bestlap = 0;
					$r_lastlap = 0;
					$r_gap = 0;
					$r_passed = 0; 
					$r_first_pass = "";
					$r_last_ts = "";
					$lap_query = lavu_query("select * from `lk_laps` where `scheduleid`='[1]' order by `ms_this_lap` asc",$sched_read['id']);
					while($lap_read = lysql_fetch_assoc($lap_query))
					{
						if(is_numeric($lap_read['pass']) && $lap_read['pass'] < 999 && $r_lap < $total_laps + 1)
						{
							/*while($lap < $lap_read['pass'])
							{
								echo "<td>-</td>";
								$lap++;
							}
							echo "<td>".$lap_read['ms_duration']."</td>";*/
							
							if($r_lap > 0)
							{
								if($lap_read['ms_duration'] > 10)
								{
									$r_passed = $lap_read['ms_this_lap'];
									$r_lastlap = $lap_read['ms_duration'];
									$r_ts = (isset($lap_read['ts']))?$lap_read['ts']:"";
									
									if($r_last_ts=="" || $r_ts > $r_last_ts) $r_last_ts = $r_ts;
									if(($r_bestlap==0 || $r_bestlap * 1 > $r_lastlap * 1) && $r_lastlap > 0) 
									{
										//if(isset($_GET['test'])) echo "schedid: " . $sched_read['id'] . "." . $r_lap . " - " . $r_bestlap . ">".$r_lastlap."<br>";
										$r_bestlap = $r_lastlap;
									}
									$all_laps[$schedid][] = $r_lastlap;
									
									$lap_grid_info['laps'][] = $r_lastlap;
									$r_lap++;
								}
							}
							else
							{
								$racer_info[$schedid]['first_pass'] = $r_first_pass;
								$r_lap++;
							}
							if(!isset($lap_first_pass[$r_lap]) || $lap_first_pass[$r_lap] > $r_passed) $lap_first_pass[$r_lap] = $r_passed;
							if($r_first_pass=="") $r_first_pass = $r_passed;
							//echo "<br>fl:" . $r_lastlap . " fp:" . $r_first_pass;
						}
					}
					//echo "<br>best: $r_bestlap last: $r_lastlap";
					
					$racer_info[$schedid]['lap'] = $r_lap;
					$racer_info[$schedid]['showlap'] = $r_lap;
					if($r_lap > $total_laps) $racer_info[$schedid]['showlap'] = "*";
					$racer_info[$schedid]['bestlap'] = number_format($r_bestlap,3,".",",");
					$racer_info[$schedid]['lastlap'] = number_format($r_lastlap,3,".",",");
					$racer_info[$schedid]['passed'] = $r_passed;
					$racer_info[$schedid]['lastts'] = $r_last_ts;
					//$racer_info[$schedid]['schedid'] = $schedid;
					
					if($fastest_lap_all=="" || ($r_bestlap < $fastest_lap_all && $r_bestlap > 0))
						$fastest_lap_all = $r_bestlap;
					
					if($track_first_pass=="" || $track_first_pass > $r_passed) $track_first_pass = $r_passed;
					if($r_lap > $highest_lap) $highest_lap = $r_lap;
					
					$lap_grid[] = $lap_grid_info;
				}
				
				$display .= "<style>";
				$display .= "  body, table {font-size:14px;} ";
				$display .= "</style>";
				
				/*for($n=0; $n<=$highest_lap; $n++)
				{
					if(isset($lap_first_pass[$n]))
					{
						$display .= "<br>" . $n . ": " . $lap_first_pass[$n];
					}
				}*/
				
				$display .= "<table cellpadding=7>";
				$display .= "<tr>";
				$display .= "<td>Racer</td><td>Number</td><td>Lap</td><td>Best Lap</td><td>Last Lap</td><td>Gap</td><td>Pass $track_first_pass</td>";
				$display .= "</tr>";
				
				$n = 0;
				$ordered_racers = array();
				foreach($racer_info as $key=>$rinfo)
				{
					$setlap = $rinfo['lap'];
					
					if($setlap > 1)
					{
						//mail("corey@meyerwind.com",$score_by,$score_by,"From:info@poslavu.com");
						if($score_by==1)//$score_by=="Position" || $score_by==7)
						{
							if(isset($lap_first_pass[$setlap]))
								$setgap = number_format($rinfo['passed'] - $lap_first_pass[$setlap],3,".",",");
							$showgap = $setgap;
							//if($setlap < $highest_lap + 1)
								//$showgap = "-" . ($highest_lap - $setlap);
						}
						else
						{
							if($fastest_lap_all > 0)
								$setgap = number_format($rinfo['bestlap'] - $fastest_lap_all,3,".",",");
							$showgap = $setgap;
						}
						//$setgap = number_format($rinfo['passed'] - $lap_first_pass[$setlap],3,".",",");
						//if($setgap >= -0.001 && $setgap <= 0.001) $setgap = 0.000;
					}
					else {$setgap = "";$showgap = "";}
					
					$rinfo['gap'] = $setgap;// - number_format(($rinfo['first_pass'] - $track_first_pass),3,".",",");
					$rinfo['showgap'] = $showgap;
					$placed = false;
					$ordracers = $ordered_racers;
					$ordered_racers = array();
					for($i=0; $i<count($ordracers); $i++)
					{
						//if(!$placed && ($rinfo['lap'] > $ordracers[$i]['lap'] || ($rinfo['lap']==$ordracers[$i]['lap'] && $rinfo['lastlap'] < $ordracers[$i]['lastlap'])))
						if(!$placed && ($rinfo['gap'] < $ordracers[$i]['gap'] || $ordracers[$i]['gap']=="") && $setlap > 1)
						{
							$ordered_racers[] = $rinfo;
							$placed = true;
						}
						$ordered_racers[] = $ordracers[$i];
					}
					if(!$placed) 
					{
						$ordered_racers[] = $rinfo;
					}
				}
				
				$placed_pos = 0;
				$rinfo_pos = array();	
				foreach($ordered_racers as $rinfo)
				{
					$placed_pos++;
					if(($rinfo['gap']>=-0.001 && $rinfo['gap']<=0.001) || $rinfo['gap']==0) $rinfo['gap'] = "";
					$display .= "<tr>";
					$display .= "<td>" . $rinfo['name'] . "</td>";
					$display .= "<td>" . $rinfo['number'] . "</td>";
					$display .= "<td>" . $rinfo['lap'] . "</td>";
					$display .= "<td>" . $rinfo['bestlap'] . "</td>";
					$display .= "<td>" . $rinfo['lastlap'] . "</td>";
					$display .= "<td>" . $rinfo['gap'] . "</td>";
					$display .= "<td>" . $rinfo['passed'] . "</td>";
					$display .= "</tr>";
					
					$n++;
					$data .= "&name_$n=".$rinfo['name'];
					$data .= "&number_$n=".$rinfo['number'];
					$data .= "&lap_$n=".$rinfo['lap'];
					$data .= "&showlap_$n=".$rinfo['showlap'];
					$data .= "&bestlap_$n=".$rinfo['bestlap'];
					$data .= "&lastlap_$n=".$rinfo['lastlap'];
					$data .= "&gap_$n=".$rinfo['gap'];
					$data .= "&showgap_$n=".$rinfo['showgap'];
					
					$r_vars = array();
					$r_vars['eventid'] = $eventid;
					$r_vars['customerid'] = $rinfo['custid'];
					//$r_vars['ms_duration'] = $rinfo['bestlap'];
					$r_vars['bestlap'] = $rinfo['bestlap'];
					$r_vars['lastlap'] = $rinfo['lastlap'];
					$r_vars['trackid'] = $trackid;
					$r_vars['date'] = $event_date;
					$r_vars['place'] = $placed_pos;
					$r_vars['passes'] = $rinfo['lap'];
					
					$set_rank = "";
					if(isset($track_sec_int) && $track_sec_int!="" && $track_sec_int > 0)
					{
						$set_rank = 1;
						if($rinfo['bestlap'] <= $track_sec_int) 
						{
							$set_rank = 2;
							if(isset($track_sec_adv) && $track_sec_adv!="" && $track_sec_adv > 0)
							{
								if($rinfo['bestlap'] <= $track_sec_adv)
									$set_rank = 3;
							}
						}
					}
					
					$r_vars['rank'] = $set_rank;
					$rr_query = lavu_query("select * from `lk_race_results` where `eventid`='[1]' and `customerid`='[2]'",$eventid,$rinfo['custid']);
					if(lysql_num_rows($rr_query))
					{
						$rr_read = lysql_fetch_assoc($rr_query);
						$r_vars['id'] = $rr_read['id'];
						if(isset($_GET['test']))
						{
							echo $rinfo['name'] . " set bestlap = " . $rinfo['bestlap'] . ", rank = " . $set_rank . " for id " . $rr_read['id'] . "<br>";
						}
						lavu_query("update `lk_race_results` set `bestlap`='[bestlap]',`lastlap`='[lastlap]',`trackid`='[trackid]',`date`='[date]',`ranking`='[rank]',`place`='[place]',`passes`='[passes]' where `id`='[id]'",$r_vars);
					}
					else
					{
						lavu_query("insert into `lk_race_results` (`eventid`,`customerid`,`bestlap`,`lastlap`,`trackid`,`date`,`ranking`,`place`,`passes`) values ('[eventid]','[customerid]','[bestlap]','[lastlap]','[trackid]','[date]','[rank]','[place]','[passes]')",$r_vars);
					}
					
					//mail("corey@poslavu.com","test",$rinfo['lastts'] . " " . $tsnow);
					if(isset($rinfo['lastts']) && $rinfo['lastts'] > (time() - 5))
						$data .= "&highlight_$n=1";
					else
						$data .= "&highlight_$n=0";
					
					if($rinfo['lap'] > $highest_lap) $highest_lap = $rinfo['lap'];
					$rinfo_pos[$rinfo['kartid']] = $placed_pos;
				}
				if($highest_lap==0) $highest_lap = 1;
				$laps_left = $total_laps - ($highest_lap - 1);
				if($laps_left < 0) $laps_left = 0;
				$data = "highest_lap=$highest_lap&laps_left=$laps_left&count=$n" . $data;
				$display .= "</table>";
				
				$display .= "<br>cc=" . $company_code_full;
				$display .= "<br>loc_id=" . reqvar("loc_id");
				$display .= "<br>eventid=" . reqvar("eventid");
			}
			else
			{
				$display .= "Nobody Scheduled";
				$data .= "&count=0&";
			}
			$data = "server_time=".date("h:i:s")."&".$data;
			
			if($mode=="data")
			{
				echo $data;
			}
			else if($mode=="results_graph")
			{
				//echo "GRAPH";
				require_once(dirname(__FILE__) . "/results_graph.php");
			}
			else if($mode=="results")
			{
				if(isset($_GET['test'])) echo "rank: $set_rank<br>";
				require_once(dirname(__FILE__) . "/results_sheet.php");
			}
			else if($mode=="info")
			{
				$icols = array();
				$iheader = "";
				
				$imax = 0;
				//echo "<table>";
				for($i=0; $i<count($lap_grid); $i++)
				{
					//echo "<tr>";
					$iheader .= "<td><u>" . $lap_grid[$i]['racer_name'] . "</u></td>";
					$irow = array();
					for($n=0; $n<count($lap_grid[$i]['laps']); $n++)
					{
						$iextra_style = "";
						if($lap_grid[$i]['laps'][$n] * 1 > 40)
							$iextra_style = " font-weight:bold";
						$irow[] = "<td bgcolor='#dddddd' style='border:solid 1px #777777;".$iextra_style."'>".number_format($lap_grid[$i]['laps'][$n],1,".",",")."</td>";
						if($n > $imax) $imax = $n;
					}
					$icols[] = $irow;
					//echo "</tr>";
				}
				//echo "</table>";
				
				echo "<table>";
				echo "<tr>";
				echo "<td>&nbsp;</td>";
				echo $iheader;
				echo "</tr>";
				echo "<tr>";
				echo "<td valign='top' align='center'>";
				echo "<table>";
				for($n=0; $n<=$imax; $n++)
				{
					echo "<tr><td style='border:solid 1px #777777' align='center'>" . ($n + 1) . "</td></tr>";
				}
				echo "</table>";
				echo "</td>";
				for($n=0; $n<count($icols); $n++)
				{
					echo "<td valign='top' align='center'>";
					echo "<table>";
					for($x=0; $x<count($icols[$n]); $x++)
					{
						echo "<tr>" . $icols[$n][$x] . "</tr>";
					}
					echo "</table>";
					echo "</td>";
				}
				echo "</tr>";
				echo "</table>";
			}
			else
			{
				echo $display;
			}
		}
	}
?>

<?php

} else if($uripage =="race_scores2.php") {
//--------------------------------------------------- race_scores2.php ----------------------------------------------//
?>

<?php
  	//require_once("../comconnect.php");
			
	$eventid = reqvar("eventid");
	$trackid = reqvar("trackid");
	$mode = reqvar("mode");
	
	if($eventid=="current" || $eventid=="active")
	{
		$active_query = lavu_query("select * from `lk_events` where `ms_start`!='' and `ms_end`='' and `trackid`='[1]'",$trackid);
		if(lysql_num_rows($active_query))
		{
			$active_read = lysql_fetch_assoc($active_query);
			$eventid = $active_read['id'];
		}
	}
	
	if($mode=="kart_assignment")
	{
		$kart_count = 0;
		$sched_query = lavu_query("select `f_name`,`l_name`,`racer_name`,`lk_event_schedules`.`id` as `id`,`lk_karts`.`number` as `number` from `lk_event_schedules` LEFT JOIN `lk_customers` ON `lk_event_schedules`.`customerid`=`lk_customers`.`id`  LEFT JOIN `lk_karts` ON `lk_event_schedules`.`kartid`=`lk_karts`.`id` where `lk_event_schedules`.`eventid`='[1]'",$eventid);
		while($sched_read = lysql_fetch_assoc($sched_query))
		{
			$racer_name = $sched_read['racer_name'];
			if(trim($racer_name)=="")
				$racer_name = trim($sched_read['f_name'] . " " . $sched_read['l_name']);
			$racer_number = $sched_read['number'];
			$kart_count++;
			echo "<br>&kart_name".$kart_count."=".$racer_name."&";
			echo "<br>&kart_number".$kart_count."=".$racer_number."&";
		}
		echo "<br>&kart_count=".$kart_count."&";
	}
	else if($mode=="upcoming")
	{
		$year = date("Y");
		$month = date("m");
		$day = date("d");
		$hour = date("H");
		$minute = date("m");
		$mints = mktime($hour - 2,$minute,0,$month,$day,$year);
		$maxts = mktime($hour + 8,$minute,0,$month,$day,$year);
		
		$event_count = 0;
		$event_query = lavu_query("select `lk_events`.`ts` as `ts`, `lk_events`.`time` as time, `lk_events`.`id` as id, `lk_event_types`.`title` as `title` from `lk_events` left join `lk_event_types` on lk_events.type = lk_event_types.id where `ts`>='$mints' and `ts`<='$maxts' and `lk_events`.`ms_start`='' and `lk_events`.`trackid`='[1]' order by `ts` asc limit 6",$trackid);
		while($event_read = lysql_fetch_assoc($event_query))
		{
			$eventid = $event_read['id'];
			$event_count++;
			echo "<br>&event".$event_count."_name=" . date("g:i a",$event_read['ts'])."&";
			
			$racer_count = 0;
			$sched_query = lavu_query("select * from `lk_event_schedules` left join `lk_customers` on `lk_event_schedules`.`customerid`=`lk_customers`.`id` where `eventid`='$eventid'");
			while($sched_read = lysql_fetch_assoc($sched_query))
			{
				$racer_count++;
				$racer_name = $sched_read['racer_name'];
				if(trim($racer_name)=="")
					$racer_name = trim($sched_read['f_name'] . " " . $sched_read['l_name']);
				echo "<br>&event".$event_count."_racer".$racer_count."=" . $racer_name . "&";
			}
			echo "<br>&event".$event_count."_racer_count=" . $racer_count . "&";
		}
		echo "<br>&event_count=" . $event_count . "&";
	}
	else
	{
		if($eventid)
		{
			$display = "";
			$data = "";
			
			$event_title = "";
			$total_laps = 0;
			$event_query = lavu_query("select * from `lk_events` where `id`='[1]'",$eventid);
			if(lysql_num_rows($event_query))
			{
				$event_read = lysql_fetch_assoc($event_query);
				$event_title = display_time($event_read['time']);
				$event_type = $event_read['type'];
				
				$type_query = lavu_query("select * from `lk_event_types` where `id`='[1]'",$event_type);
				if(lysql_num_rows($type_query))
				{
					$type_read = lysql_fetch_assoc($type_query);
					$total_laps = $type_read['laps'];
				}
			}
			$data .= "&event_title=$event_title&";
			$data .= "&total_laps=$total_laps&";
			
			$track_first_pass = "";
			$sched_query = lavu_query("select `f_name`,`l_name`,`racer_name`,`lk_event_schedules`.`id` as `id`,`lk_karts`.`number` as `number` from `lk_event_schedules` LEFT JOIN `lk_customers` ON `lk_event_schedules`.`customerid`=`lk_customers`.`id`  LEFT JOIN `lk_karts` ON `lk_event_schedules`.`kartid`=`lk_karts`.`id` where `lk_karts`.`number`!='' and `lk_event_schedules`.`eventid`='[1]'",$eventid);
			if(lysql_num_rows($sched_query))
			{
				$racer_info = array();
				$lap_first_pass = array();
				$highest_lap = 0;
				
				while($sched_read = lysql_fetch_assoc($sched_query))
				{
					$schedid = $sched_read['id'];
					
					$racer_name = $sched_read['racer_name'];
					if(trim($racer_name)=="")
						$racer_name = trim($sched_read['f_name'] . " " . $sched_read['l_name']);
						
					$racer_info[$schedid]['name'] = $racer_name;
					$racer_info[$schedid]['number'] = $sched_read['number'];
					
					$r_lap = 0;
					$r_bestlap = 0;
					$r_lastlap = 0;
					$r_gap = 0;
					$r_passed = 0; 
					$r_first_pass = "";
					$lap_query = lavu_query("select * from `lk_laps` where `scheduleid`='[1]' order by `ms_this_lap` asc",$sched_read['id']);
					while($lap_read = lysql_fetch_assoc($lap_query))
					{
						if(is_numeric($lap_read['pass']) && $lap_read['pass'] < 999)
						{
							/*while($lap < $lap_read['pass'])
							{
								echo "<td>-</td>";
								$lap++;
							}
							echo "<td>".$lap_read['ms_duration']."</td>";*/
							
							if($r_lap > 0)
							{
								$r_passed = $lap_read['ms_this_lap'];
								$r_lastlap = $lap_read['ms_duration'];
								if($r_bestlap==0 || $r_bestlap > $r_lastlap) $r_bestlap = $r_lastlap;
							}
							else
							{
								$racer_info[$schedid]['first_pass'] = $r_first_pass;
							}
							$r_lap++;
							if(!isset($lap_first_pass[$r_lap]) || $lap_first_pass[$r_lap] > $r_passed) $lap_first_pass[$r_lap] = $r_passed;
							if($r_first_pass=="") $r_first_pass = $r_passed;
						}
					}
					
					$racer_info[$schedid]['lap'] = $r_lap;
					$racer_info[$schedid]['bestlap'] = number_format($r_bestlap,3,".",",");
					$racer_info[$schedid]['lastlap'] = number_format($r_lastlap,3,".",",");
					$racer_info[$schedid]['passed'] = $r_passed;
					
					if($track_first_pass=="" || $track_first_pass > $r_passed) $track_first_pass = $r_passed;
					if($r_lap > $highest_lap) $highest_lap = $r_lap;
				}
				
				$display .= "<style>";
				$display .= "  body, table {font-size:14px;} ";
				$display .= "</style>";
				
				for($n=0; $n<=$highest_lap; $n++)
				{
					if(isset($lap_first_pass[$n]))
					{
						$display .= "<br>" . $n . ": " . $lap_first_pass[$n];
					}
				}
				
				$display .= "<table cellpadding=7>";
				$display .= "<tr>";
				$display .= "<td>Racer</td><td>Number</td><td>Lap</td><td>Best Lap</td><td>Last Lap</td><td>Gap</td><td>Pass $track_first_pass</td>";
				$display .= "</tr>";
				
				$n = 0;
				$ordered_racers = array();
				foreach($racer_info as $key=>$rinfo)
				{
					$setlap = $rinfo['lap'];
					$setgap = number_format($rinfo['passed'] - $lap_first_pass[$setlap],3,".",",");
					$rinfo['gap'] = $setgap;// - number_format(($rinfo['first_pass'] - $track_first_pass),3,".",",");
					$placed = false;
					$ordracers = $ordered_racers;
					$ordered_racers = array();
					for($i=0; $i<count($ordracers); $i++)
					{
						//if(!$placed && ($rinfo['lap'] > $ordracers[$i]['lap'] || ($rinfo['lap']==$ordracers[$i]['lap'] && $rinfo['lastlap'] < $ordracers[$i]['lastlap'])))
						if(!$placed && ($rinfo['gap'] < $ordracers[$i]['gap'] && $rinfo['gap']!=""))
						{
							$ordered_racers[] = $rinfo;
							$placed = true;
						}
						$ordered_racers[] = $ordracers[$i];
					}
					if(!$placed) 
					{
						$ordered_racers[] = $rinfo;
					}
				}
					
				foreach($ordered_racers as $rinfo)
				{
					$display .= "<tr>";
					$display .= "<td>" . $rinfo['name'] . "</td>";
					$display .= "<td>" . $rinfo['number'] . "</td>";
					$display .= "<td>" . $rinfo['lap'] . "</td>";
					$display .= "<td>" . $rinfo['bestlap'] . "</td>";
					$display .= "<td>" . $rinfo['lastlap'] . "</td>";
					$display .= "<td>" . $rinfo['gap'] . "</td>";
					$display .= "<td>" . $rinfo['passed'] . "</td>";
					$display .= "</tr>";
					
					$n++;
					$data .= "&name_$n=".$rinfo['name'];
					$data .= "&number_$n=".$rinfo['number'];
					$data .= "&lap_$n=".$rinfo['lap'];
					$data .= "&bestlap_$n=".$rinfo['bestlap'];
					$data .= "&lastlap_$n=".$rinfo['lastlap'];
					$data .= "&gap_$n=".$rinfo['gap'];
					$data .= "&highlight_$n=1";
				}
				if($highest_lap==0) $highest_lap = 1;
				$laps_left = $total_laps - ($highest_lap - 1);
				if($laps_left < 0) $laps_left = 0;
				$data = "highest_lap=$highest_lap&laps_left=$laps_left&count=$n" . $data;
				$display .= "</table>";
				
				$display .= "<br>cc=" . $company_code_full;
				$display .= "<br>loc_id=" . reqvar("loc_id");
				$display .= "<br>eventid=" . reqvar("eventid");
			}
			else
			{
				$display .= "Nobody Scheduled";
				$data .= "&count=0&";
			}
			
			if($mode=="data")
			{
				echo $data;
			}
			else
			{
				echo $display;
			}
		}
	}
?>

<?php

} else if($uripage =="racers_for_this_race.php") {
//--------------------------------------------------- racers_for_this_race.php ----------------------------------------------//
?>

<html>
	<head>
		<title>Untitled Document</title>
		<style type="text/css">
<!--
body {
	background-color: transparent;
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}

.carnumber {	font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #FFF;
}
.creditnumber {	font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #3d4f6d;
}
.nametext {	font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
}
.nametext_grey {	font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #777777;
}
.gridposition {	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #5A75A0;
}
.icontext {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 11px;
	font-weight: bold;
	color: #5A75A0;
}
.subtitles {	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #5A75A0;
}
.title1 {	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 18px;
	font-weight: bold;
	color: #5A75A0;
}
-->
		</style>
		<script language="javascript">
		
			row_selected = 1;
			
			<?php
				if (isset($_POST['rs'])) {
					echo "row_selected = ".$_POST['rs'].";";
				}
			?>
		
			function deassign_kart(customerid) {
				
				window.location = "_DO:cmd=load_url&c_name=racers_for_this_race&f_name=lavukart/racers_for_this_race.php?deassignid=" + customerid +"&rs=" + row_selected;
			}
			
			function assign_rs(rs) {
				row_selected = rs;
			}
		</script>
	</head>

<?php
	//require_once(dirname(__FILE__) . "/../comconnect.php");
	
	$eventid = 0;
	if(reqvar("eventid")) $eventid = reqvar("eventid");
	else if(sessvar_isset("eventid")) $eventid = sessvar("eventid");
	set_sessvar("eventid",$eventid);
	
	if(reqvar("loc_id")) $loc_id = reqvar("loc_id");
	else if(sessvar_isset("loc_id")) $loc_id = sessvar("loc_id");
	set_sessvar("loc_id", $loc_id);

	$max_group = 1;
	$get_karts = lavu_query("SELECT `position` as `position` FROM `lk_karts` WHERE `locationid` = '".sessvar("loc_id")."'");
	if (lysql_num_rows($get_karts) > 0) {
		while ($extract_k = lysql_fetch_array($get_karts)) {
			if ((floor((int)$extract_k['position'] / 2) > $max_group) && (floor((int)$extract_k['position'] / 2) <= 3)) {
				$max_group = floor((int)$extract_k['position'] / 2);
			}
		}
	}

	if (isset($_POST['deassignid'])) {
		$deassign_racer = lavu_query("UPDATE `lk_event_schedules` SET `kartid` = '' WHERE `eventid` = '".$eventid."' AND `customerid` = '".$_POST['deassignid']."'");
	}
	
	$mode = reqvar("mode");
	
	if ($mode == "set_group") {
		//echo "here"; 
		//mail("richard@greenkeyconcepts.com","deBug","UPDATE `lk_event_schedules` SET `group` = '".reqvar("new_group")."' WHERE `customerid` = '".reqvar("customer_id")."' AND `eventid` = '".$eventid."'");
		lavu_query("UPDATE `lk_event_schedules` SET `group` = '".reqvar("new_group")."' WHERE `customerid` = '".reqvar("customer_id")."' AND `eventid` = '".$eventid."'");
	}

?>

	<body>
		<table width="417" border="0" cellpadding="0" cellspacing="0">
			<?php
				//require_once(dirname(__FILE__) . "/lk_functions.php");
				$minrows = 12;
				$maxrows = get_slots_for_event($eventid);//10;
				$rowcount = 0;			
				$overbooked = 0;
				if($maxrows < $minrows)
					$minrows = $maxrows;

				$rlist = array();
				$get_racers = lavu_query("SELECT `lk_customers`.`membership_expiration` as `membership_expiration`, `lk_customers`.`date_created` as `date_created`, `lk_event_schedules`.`customerid` as customerid, `lk_event_schedules`.`kartid` as kartid, `lk_event_schedules`.`group` as race_group, `lk_customers`.`f_name` as f_name, `lk_customers`.`l_name` as l_name, `lk_customers`.`credits` as credits, `lk_customers`.`birth_date` as birth_date, `lk_karts`.`lane` as kartlane, `lk_karts`.`number` as kartnumber FROM `lk_event_schedules` LEFT JOIN `lk_customers` ON `lk_event_schedules`.`customerid` = `lk_customers`.`id` LEFT JOIN `lk_karts` ON `lk_event_schedules`.`kartid` = `lk_karts`.`id` WHERE `eventid` = '".$eventid."'");
				while ($extract_r = lysql_fetch_assoc($get_racers)) {
					$extract_r['pre_placement'] = 888;
					$extract_r['pre_placed'] = false;
					$rlist[] = $extract_r;
				}
				
				$event_query = lavu_query("SELECT * from `lk_events` where `id`='[1]'",$eventid);
				if(lysql_num_rows($event_query))
				{
					$event_read = lysql_fetch_assoc($event_query);
					$event_ts = $event_read['ts'];
					$debug = "";
					
					for($rr=0; $rr<count($rlist); $rr++)
					{
						//$debug .= "\n" . "select * from `lk_race_results` LEFT JOIN `lk_events` ON `lk_race_results`.`eventid`=`lk_events`.`id` where `lk_race_results`.`customerid`='".$rlist[$rr]['customerid']."' and `lk_events`.`ms_end`!='' and `lk_events`.`ts`>'".($event_ts * 1 - (60 * 60 * 12))."' order by `ts` desc limit 1";
						
						$results_query = lavu_query("select * from `lk_race_results` LEFT JOIN `lk_customers` ON `lk_race_results`.`customerid`=`lk_customers`.`id` LEFT JOIN `lk_events` ON `lk_race_results`.`eventid`=`lk_events`.`id` where `lk_race_results`.`customerid`='[1]' and `lk_events`.`ms_end`!='' and `lk_events`.`ts`<'[2]' and `lk_events`.`ts`>'[3]' order by `lk_events`.`ts` desc limit 1",$rlist[$rr]['customerid'],$event_ts,($event_ts * 1 - (60 * 60 * 12)));
						if(lysql_num_rows($results_query))
						{
							$results_read = lysql_fetch_assoc($results_query);
							$rlist[$rr]['pre_placement'] = $results_read['bestlap'];//place'];
							$rlist[$rr]['pre_placed'] = true;
							$debug .= "\n " . $rlist[$rr]['customerid'] . " " . $results_read['f_name'] . " " . $results_read['l_name'] . " index:$rr" . " = value:" . $results_read['place'];
						}
					}
										
					$r2_list = $rlist;
					$rlist = array();
					while(count($rlist) < count($r2_list))
					{
						$lowest = 99999;
						$lowest_index = "";
						for($n=0; $n<count($r2_list); $n++)
						{
							$r2num = $r2_list[$n]['pre_placement'];
							if($r2num!=98989 && ($r2num < $lowest))
							{
								$lowest = $r2num;
								$lowest_index = $n;
							}
						}
						$rlist[] = $r2_list[$lowest_index];
						$r2_list[$lowest_index]['pre_placement'] = 98989;
						$debug .= "\n add to list: index:" . $lowest_index . " value:$lowest)";
					}
					//if(sessvar("loc_id")==9)
						//mail("corey@meyerwind.com","placement",$debug,"From: info@poslavu.com");
				}
				
				//$get_racers = lavu_query("SELECT `lk_customers`.`membership_expiration` as `membership_expiration`, `lk_customers`.`date_created` as `date_created`, `lk_event_schedules`.`customerid` as customerid, `lk_event_schedules`.`kartid` as kartid, `lk_event_schedules`.`group` as race_group, `lk_customers`.`f_name` as f_name, `lk_customers`.`l_name` as l_name, `lk_customers`.`credits` as credits, `lk_customers`.`birth_date` as birth_date, `lk_karts`.`lane` as kartlane, `lk_karts`.`number` as kartnumber FROM `lk_event_schedules` LEFT JOIN `lk_customers` ON `lk_event_schedules`.`customerid` = `lk_customers`.`id` LEFT JOIN `lk_karts` ON `lk_event_schedules`.`kartid` = `lk_karts`.`id` WHERE `eventid` = '".$eventid."'");
				//while ($extract_r = lysql_fetch_assoc($get_racers)) {
				
				for($rr=0; $rr<count($rlist); $rr++)
				{
					$extract_r = $rlist[$rr];
				
					$birth_date = $extract_r['birth_date'];
					$birth_date1 = $birth_date;
					$bdates = explode("-",$birth_date);
					if(count($bdates) > 2)
					{
						$byear = ($bdates[0] % 100);
						if($byear < 10) $byear = "0" . ($byear * 1);
						$birth_date = $bdates[1] . "/" . $bdates[2] . "/" . $byear;
					}
					else $birth_date = "";
					if($birth_date1!="" && $birth_date1 > date("Y-m-d",mktime(0,0,0,date("m"),date("d"),date("Y") - 18)))
						$cust_is_minor = true;
					else
						$cust_is_minor = false;
											
					$rowcount++;
					if($rowcount > $maxrows && $overbooked==0)
					{
						echo "<tr><td bgcolor='#880000' style='font-size:2px'>&nbsp;</td></tr>";
						$overbooked++;
					}
					echo "<tr>
						<td height='46' align='center' valign='top'>
							<table width='417' height='46' border='0' cellpadding='0' cellspacing='0'>
								<tr>";
					if (($extract_r['kartid'] == "") || ($extract_r['kartid'] == "0")) {
						$nametext_class = "nametext";
						echo "<td width='52' align='center' valign='middle' class='subtitles'>&nbsp;</td>";
					} else {
						$nametext_class = "nametext_grey";
												
						$lane_calc = ($extract_r['kartlane'] % 8);	
						$kart_img = "car.png";	
						switch ($lane_calc) {
							 case 1 : $kart_img = "car1.png"; break;
							 case 2 : $kart_img = "car2.png"; break;
							 case 3 : $kart_img = "car3.png"; break;
							 case 4 : $kart_img = "car4.png"; break;
							 case 5 : $kart_img = "car6.png"; break;
							 case 6 : $kart_img = "car7.png"; break;
							 case 7 : $kart_img = "car8.png"; break;
							 case 8 : $kart_img = "car9.png"; break;
						}
						$next_group = (((int)$extract_r['race_group'] % $max_group) + 1);

						echo "<td width='52' align='left' valign='top' class='subtitles' background='images/".$kart_img."' onclick='deassign_kart(".$extract_r['customerid'].");'>
							<table width='44' height='36' border='0' cellspacing='0' cellpadding='6'>
								<tr><td align='center' valign='bottom' class='carnumber'>".$extract_r['kartnumber']."</td></tr>
							</table>
						</td>";
					}
					echo "<td width='51' align='center' valign='middle' style='background:URL(images/group_color_bg".$extract_r['race_group'].".png);' onClick='this.style.backgroundImage = \"URL(images/group_color_bg".$next_group.".png)\"; window.location = \"racers_for_this_race.php?mode=set_group&new_group=".$next_group."&customer_id=".$extract_r['customerid']."\";'>
										<table width='51' border='0' cellspacing='0' cellpadding='0'>
											<tr><td align='center' valign='middle'>&nbsp;</td></tr>
										</table>
									</td>
									<td width='262' align='left' valign='middle' background='images/tab_bg_262.png' onclick='location = \"_DO:cmd=send_js&c_name=car_assignments&js=assign_kart(".$extract_r['customerid'].");\";'>
										<table width='262' border='0' cellspacing='0' cellpadding='8'>
											<tr><td class='".$nametext_class."'>";
											echo $extract_r['f_name']." ".$extract_r['l_name'];
											if($birth_date!="") 
											{
												if($cust_is_minor)
													echo " <font style='font-size:10px; color:#bb0000'>$birth_date</font>";
												else
													echo "";//" <font style='font-size:10px; color:#006600'>$birth_date</font>";
											}
											echo "</td></tr>
										</table>
									</td>
									<td width='52' align='left' valign='top' background='images/btn_credit.png' >
										<table width='50' height='46' border='0' cellspacing='0' cellpadding='6'>
											<tr><td align='center' valign='middle' class='creditnumber'>";
											
											$membership_expiration = $extract_r['membership_expiration'];
											$date_created = $extract_r['date_created'];
											if($date_created >= date("Y-m-d 00:00:00"))
												$cust_new = true;
											else
												$cust_new = false;
											if($membership_expiration!="" && $membership_expiration >= date("Y-m-d") && $membership_expration <= "9999-99-99")
												{$mem = true; $bgf = "images/btn_credit2.png";}
											else 	
												{$mem = false;$bgf = "images/btn_credit.png";}
											
											echo $extract_r['credits'];
											if($cust_new) echo "<font style='font-size:8px; color:#008800'>n</font>";
											if($mem) echo "<font style='font-size:8px; color:#000088'>M</font>";
											
											echo "</td></tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>";
				}
				for($r = $rowcount; $r < $minrows; $r++) {
					echo "<tr>
						<td height='46' align='center' valign='top'>
							<table width='417' height='46' border='0' cellpadding='0' cellspacing='0'>
								<tr>
									<td width='52' align='center' valign='middle' class='subtitles'>&nbsp;</td>
									<td width='51' align='center' valign='middle' background='images/tab_bg_51.png'>&nbsp;</td>
									<td width='262' align='left' valign='middle' background='images/tab_bg_262.png' >
										<table width='262' border='0' cellspacing='0' cellpadding='8'>
											<tr><td class='nametext'>&nbsp;</td></tr>
										</table>
									</td>
									<td width='52' align='left' valign='top' background='images/btn_credit.png' >
										<table width='50' height='46' border='0' cellspacing='0' cellpadding='6'>
											<tr><td align='center' valign='middle' class='creditnumber'>&nbsp;</td></tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>";
				}
			?>
    </table>
		<script language='javascript'>
			<?php
				if (isset($_POST['deassignid'])) {
					echo "window.location = '_DO:cmd=load_url&c_name=car_assignments&f_name=lavukart/car_assignments.php?rs=".$_POST['rs']."';";
				}
			?>
		</script>
	</body>
</html>
<?php

} else if($uripage =="racers_for_this_race_wrapper.php") {
//--------------------------------------------------- racers_for_this_race_wrapper.php ----------------------------------------------//
?>

<html>
	<head>
		<title>Untitled Document</title>
		<style type="text/css">
<!--
body {
	background-color: transparent;
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}

.carnumber {	font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #FFF;
}
.creditnumber {	font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #3d4f6d;
}
.nametext {	font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
}
.gridposition {	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #5A75A0;
}
.icontext {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 11px;
	font-weight: bold;
	color: #5A75A0;
}
.subtitles {	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #5A75A0;
}
.title1 {	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 18px;
	font-weight: bold;
	color: #5A75A0;
}
-->
		</style>
	</head>

	<body>
		<table width="474" height="598" border="0" cellpadding="0" cellspacing="0" >
			<tr>
				<td width="79" height="536" align="left" valign="top">
					<table width="79" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="11">&nbsp;</td>
							<td height="26">&nbsp;</td>
						</tr>
						<tr>
							<td width="11">&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td width="11">&nbsp;</td>
							<td height="110" align="left" valign="bottom"></td>
						</tr>
					</table>
				</td>
				<td align="left" valign="top">
					<table width="395" height="598" border="0" cellspacing="0" cellpadding="0">
						<tr><td height="23" background="images/line_top.png"></td></tr>
						<tr><td height="552" align="left" valign="top">&nbsp;</td></tr>
						<tr><td height="23" background="images/line_bot.png"></td></tr>
					</table>
				</td>
			</tr>
		</table>
	</body>
</html>
<?php

} else if($uripage =="resize_image.php") {
//--------------------------------------------------- resize_image.php ----------------------------------------------//
?>

<?php
	class Resize_Image 
	{
		var $image_to_resize;
		var $new_width;
		var $new_height;
		var $ratio;
		var $new_ratio;
		var $new_image_name;
		var $save_folder;
		
		function resize()
		{
			if(!file_exists($this->image_to_resize))
			{
			  exit("File ".$this->image_to_resize." does not exist.");
			}
			
			$info = GetImageSize($this->image_to_resize);
			
			if(empty($info))
			{
			  exit("The file ".$this->image_to_resize." doesn't seem to be an image.");
			}
			
			$width = $info[0];
			$height = $info[1];
			$mime = $info['mime'];
					
			if($this->ratio)
			{
				if (isset($this->new_ratio))
				{
					$this->new_width = $width * $this->new_ratio;
					$this->new_height = $height * $this->new_ratio;
				}
				
				if (isset($this->new_width))
				{
					$factor = (float)$this->new_width / (float)$width;
					$this->new_height = $factor * $height;
				}
				else if (isset($this->new_height))
				{
					$factor = (float)$this->new_height / (float)$height;
					$this->new_width = $factor * $width;
				}
				else
				{
					exit("Width or Height is not set");
				}
				
				$type = substr(strrchr($mime, '/'), 1);
				
				switch ($type)
				{
					case 'jpeg':
						$image_create_func = 'ImageCreateFromJPEG';
						$image_save_func = 'ImageJPEG';
						$new_image_ext = 'jpg';
						break;
					
					case 'png':
						$image_create_func = 'ImageCreateFromPNG';
						$image_save_func = 'ImagePNG';
						$new_image_ext = 'png';
						break;
					
					case 'bmp':
						$image_create_func = 'ImageCreateFromBMP';
						$image_save_func = 'ImageBMP';
						$new_image_ext = 'bmp';
						break;
					
					case 'gif':
						$image_create_func = 'ImageCreateFromGIF';
						$image_save_func = 'ImageGIF';
						$new_image_ext = 'gif';
						break;
					
					case 'vnd.wap.wbmp':
						$image_create_func = 'ImageCreateFromWBMP';
						$image_save_func = 'ImageWBMP';
						$new_image_ext = 'bmp';
						break;
					
					case 'xbm':
						$image_create_func = 'ImageCreateFromXBM';
						$image_save_func = 'ImageXBM';
						$new_image_ext = 'xbm';
						break;
					
					default:
						$image_create_func = 'ImageCreateFromJPEG';
						$image_save_func = 'ImageJPEG';
						$new_image_ext = 'jpg';
				}
				
				// New Image
				$white    = imagecolorallocate($image, 0xFF, 0xFF, 0xFF);
				$image_c = ImageCreateTrueColor($this->new_width, $this->new_height);
				
				// allow transparency
				imagealphablending($image_c, false);
				imagesavealpha($image_c,true);
				$transparent = imagecolorallocatealpha($image_c, 255, 255, 255, 127);
				imagefilledrectangle($image_c, 0, 0, $this->new_width, $this->new_height, $transparent);
				imagecopyresampled($image_c, $im, 0, 0, 0, 0, $this->new_width, $this->new_height, $width, $height);
							
				$new_image = $image_create_func($this->image_to_resize);
			
				ImageCopyResampled($image_c, $new_image, 0, 0, 0, 0, $this->new_width, $this->new_height, $width, $height);
		
				if($this->save_folder)
				{
				   if($this->new_image_name)
				   {
					$new_name = $this->new_image_name.'.'.$new_image_ext;
				   }
				   else
				   {
					$new_name = $this->new_thumb_name( basename($this->image_to_resize) ).'_resized.'.$new_image_ext;
				   }
		
					$save_path = $this->save_folder.$new_name;
				}
				else
				{
					/* Show the image without saving it to a folder */
				   header("Content-Type: ".$mime);
		
				   $image_save_func($image_c);
		
				   $save_path = '';
				}
		
				$process = $image_save_func($image_c, $save_path);
		
				return array('result' => $process, 'new_file_path' => $save_path);
		
			}
		}
	
		function new_thumb_name($filename)
		{
			$string = trim($filename);
			$string = strtolower($string);
			$string = trim(ereg_replace("[^ A-Za-z0-9_]", " ", $string));
			$string = ereg_replace("[ tnr]+", "_", $string);
			$string = str_replace(" ", '_', $string);
			$string = ereg_replace("[ _]+", "_", $string);
		
			return $string;
		}
	}
	
	if(isset($_GET['image']) && isset($_GET['ratio']))
	{
		$file = $_GET['image'];
		$ratio = $_GET['ratio'];
		if(!is_numeric($ratio))
			$ratio = 1;
		
		$img = new Resize_Image;
		$img->image_to_resize = $file;
		$img->new_ratio = $ratio;
		$img->ratio = true;
		$img->new_image_name = false;
		$img->save_folder = false;
		$img->resize();
	}
?>
<?php

} else if($uripage =="result_sheet.php") {
//--------------------------------------------------- result_sheet.php ----------------------------------------------//
?>

<style>
body, table {
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size:12px;
}
.titles {
	font-size:14px;
}
</style>

<table align="center" style="table-layout:fixed">
	<tr>
		<td colspan='4' align='center'>
			<table>
				<td class="titles">
					<nobr>Name: [fullname]</nobr>
					<br><nobr>Racer Name: [racer name]</nobr>
				</td>
				<td style="width:120px" class="titles">&nbsp;&nbsp;&nbsp;&nbsp;</td>
				<td>
					<nobr>Placed [placed]</nobr>
					<br><nobr>Date: [race date]</nobr>
				</td>
			</table>
			<hr />
		</td>
		<td>
	</tr>
	<tr valign="top">
		<td valign="top" style="width:120px">		
			<nobr><b>Race Results</b></nobr>
			<br>[all best times]			
		</td>
		<td>&nbsp;&nbsp;</td>
		<td style="width:120px">
			<b><nobr>Your Laps</nobr></b>
			<br>[lap times]
		</td>
		<td valign="top">
			<img src="[chart]" />
		</td>
	</tr>
    <tr><td colspan="3" height="460"></td></tr>
</table>
<?php

} else if($uripage =="results_graph.php") {
//--------------------------------------------------- results_graph.php ----------------------------------------------//
?>

<?php
	if(isset($_GET['graphtype']) && $_GET['graphtype']=="new")
	{		 
		// create image
		$image = imagecreatetruecolor(500, 330);
		 
		// allocate some solors
		$white    = imagecolorallocate($image, 0xFF, 0xFF, 0xFF);
		$gray1	  = imagecolorallocate($image, 0xb6, 0xb6, 0xb8);
		$gray2    = imagecolorallocate($image, 0xD8, 0xD9, 0xDa);
		$gray3    = imagecolorallocate($image, 0xB6, 0xB6, 0xB6);
		$darkgray = imagecolorallocate($image, 0x90, 0x90, 0x90);
		$navy     = imagecolorallocate($image, 0x00, 0x00, 0x80);
		$darknavy = imagecolorallocate($image, 0x00, 0x00, 0x50);
		$red      = imagecolorallocate($image, 0xFF, 0x00, 0x00);
		$darkred  = imagecolorallocate($image, 0x90, 0x00, 0x00);
	
		// prepare data
		if(isset($total_laps) && $total_laps > 0 && $total_laps < 999)
		{
			$showsched = $_GET['showsched'];
			$lap_titles = array();
			for($i=0; $i<$total_laps; $i++)
			{
				$lap_titles[] = ($i + 1);
			}
			$self_index = 0;
			$points = array();
			$scount = 0;
			$avg = array();
			$avg_count = array();
			for($i=0; $i<$total_laps; $i++)
			{
				//$avg[$i] = 0;
				$avg_count[$i] = 0;
			}
			foreach($all_laps as $schedid => $laps)
			{
				$graph_laps = array();
				for($i=0; $i<$total_laps; $i++)
				{
					if(isset($laps[$i]))
					{
						$graph_laps[] = $laps[$i];
						$avg[$i] += $laps[$i];
						$avg_count[$i] ++;
					}
				}
				if($schedid==$showsched || $scount==0)
				{
					$points[$scount] = array();
					if($schedid==$showsched)
					{
						$points[$scount]['weight'] = 3;
						$points[$scount]['color'] = $red;
						if($scount==1)
							$points[$scount]['legend'] = "Yours (Best)";
						else
							$points[$scount]['legend'] = "Yours";
						$race_place = $scount;
						$self_index = $scount;
					}
					else
					{
						$points[$scount]['weight'] = 1;
						$points[$scount]['color'] = $navy;
						$points[$scount]['legend'] = "Best";
					}
					//$p1->SetLegend("");
					$points[$scount]['points'] = $graph_laps;
					$scount++;
					//echo "<td>" . $laps[$i] . "</td>";
				}
			}
			
			for($i=0; $i<$total_laps; $i++)
			{
				if($avg_count[$i] > 0)
					$avg[$i] = $avg[$i] / $avg_count[$i];
			}
			$points[$scount]['weight'] = 1;
			$points[$scount]['color'] = $darkgrey;
			$points[$scount]['legend'] = "Avg";
			$points[$scount]['points'] = $avg;
			$scount++;
					 
			// make the 3D effect
			/*for ($i = 60; $i > 50; $i--) {
			   imagefilledarc($image, 50, $i, 100, 50, 0, 45, $darknavy, IMG_ARC_PIE);
			   imagefilledarc($image, 50, $i, 100, 50, 45, 75 , $darkgray, IMG_ARC_PIE);
			   imagefilledarc($image, 50, $i, 100, 50, 75, 360 , $darkred, IMG_ARC_PIE);
			}
			 
			imagefilledarc($image, 50, 50, 100, 50, 0, 45, $navy, IMG_ARC_PIE);
			imagefilledarc($image, 50, 50, 100, 50, 45, 75 , $gray, IMG_ARC_PIE);
			imagefilledarc($image, 50, 50, 100, 50, 75, 360 , $red, IMG_ARC_PIE);*/
			
			// fill the background
			//imagefilledrectangle($image, 0, 0, 249, 249, $bg);
			
			// draw a polygon
			/*$values = array(
						40,  50,  // Point 1 (x, y)
						20,  240, // Point 2 (x, y)
						60,  60,  // Point 3 (x, y)
						240, 20,  // Point 4 (x, y)
						50,  40,  // Point 5 (x, y)
						10,  10   // Point 6 (x, y)
						);*/
			//imagefilledpolygon($image, $values, 6, $blue);
			
			
			function imagelinethick($image, $x1, $y1, $x2, $y2, $color, $thick = 1)
			{
				/* this way it works well only for orthogonal lines
				imagesetthickness($image, $thick);
				return imageline($image, $x1, $y1, $x2, $y2, $color);
				*/
				if ($thick == 1) {
					return imageline($image, $x1, $y1, $x2, $y2, $color);
				}
				$t = $thick / 2 - 0.5;
				if ($x1 == $x2 || $y1 == $y2) {
					return imagefilledrectangle($image, round(min($x1, $x2) - $t), round(min($y1, $y2) - $t), round(max($x1, $x2) + $t), round(max($y1, $y2) + $t), $color);
				}
				$k = ($y2 - $y1) / ($x2 - $x1); //y = kx + q
				$a = $t / sqrt(1 + pow($k, 2));
				$points = array(
					round($x1 - (1+$k)*$a), round($y1 + (1-$k)*$a),
					round($x1 - (1-$k)*$a), round($y1 - (1+$k)*$a),
					round($x2 + (1+$k)*$a), round($y2 - (1-$k)*$a),
					round($x2 + (1-$k)*$a), round($y2 + (1+$k)*$a),
				);
				imagefilledpolygon($image, $points, 4, $color);
				return imagepolygon($image, $points, 4, $color);
			} 
			
			// draw racesheet
			//-------------------------------------------------------------------//
			// white background 
			imagefilltoborder($image,0,0,$white,$white);
			
			// configuration variables
			$rows = 12;
			$cols = $total_laps;//14;
			$cellwidth = 400 / $cols;
			if($cellwidth > 60) $cellwidth = 60;
			$cellheight = 20 * (12 / $rows);
			$chartx = 30;
			$charty = 30;
			$fontsize = 4;
			
			function chart_rectangle($image,$x1,$y1,$x2,$y2,$c)
			{
				global $chartx;
				global $charty;
				imagefilledrectangle($image, $chartx + $x1, $charty + $y1, $chartx + $x2, $charty + $y2, $c);
			}
			function chart_line($image,$x1,$y1,$x2,$y2,$c,$w)
			{
				global $chartx;
				global $charty;
				imagelinethick($image, $chartx + $x1, $charty + $y1, $chartx + $x2, $charty + $y2, $c, $w);
			}
			function draw_string($image,$x,$y,$string,$font_size,$c,$align="left")
			{
				//$font_size = 5;
				$width=imagefontwidth($font_size)*strlen($string);
				$height=imagefontheight($font_size)*2;
				$len=strlen($string);
				
				for($i=0;$i<$len;$i++)
				{
					$xpos=$x + $i*imagefontwidth($font_size);
					if($align=="center") $xpos -= ($width / 2);
					else if($align=="right") $xpos -= $width;
					$ypos=$y;//rand(0,imagefontheight($font_size));
					if($align=="middle") $ypos -= ($height / 4);
					imagechar($image,$font_size,$xpos,$ypos,$string,$c);
					$string = substr($string,1);    
					
				}
			}
			
			// find highest and lowest points
			$lowest = "";
			$highest = "";
			//$track_sec_int = $track_read['seconds_intermediate'];
			//$track_sec_adv = $track_read['seconds_advanced'];		
			$lowest = $track_sec_adv - 3;
			$highest = $track_sec_int + 7;
			
			// setup vertical grey background bars    track_sec_adv = 22 track_sec_int = 24 lowest - 21 highest - 29 diff - 8 (diff:8 - (highest:29 - track_sec_int:22))
			$diff = $highest - $lowest;
			$mult = ($rows * $cellheight) / $diff;
			
			$gyx = $cols * $cellwidth; 
			$gy1 = floor(($highest - $track_sec_int) * $mult);
			$gy2 = floor(($diff - ($track_sec_adv - $lowest)) * $mult);
			$gy3 = $rows * $cellheight;
			
			chart_rectangle($image, 0, 0, $gyx, $gy1, $gray1);
			chart_rectangle($image, 0, $gy1, $gyx, $gy2, $gray2);
			chart_rectangle($image, 0, $gy2, $gyx, $gy3, $white);
			 
			// draw rectangle grid
			for($r=0; $r<$rows; $r++)
			{
				for($c=0; $c<$cols; $c++)
				{
					$x1 = $c * $cellwidth;
					$x2 = ($c + 1) * $cellwidth;
					$y1 = $r * $cellheight;
					$y2 = ($r + 1) * $cellheight;
					chart_line($image, $x1, $y1, $x2, $y1, $darkgray, 2);
					chart_line($image, $x2, $y2, $x1, $y2, $darkgray, 2);
					if($cols< 20 || $c==$cols-1)
						chart_line($image, $x2, $y1, $x2, $y2, $darkgray, 2);
					if($cols < 20 || $c==0)
						chart_line($image, $x1, $y2, $x1, $y1, $darkgray, 2);
				}
			}
						
			// define data
			$datalist = array();
			$datacolors = array();
			$dataweight = array();
			for($x=0; $x<count($points); $x++)
			{
				$datalist[] = $points[$x]['points'];
				$datacolors[] = $points[$x]['color'];
				$dataweight[] = $points[$x]['weight'];
				//$pout = "";
				//for($y=0; $y<count($points[$x]['points']); $y++) $pout .= "\n" . $y . " - " . $points[$x]['points'][$y];
				//mail("corey@poslavu.com","datalist","count: " . count($datalist) . $pout, "From: info@poslavu.com");
			}
			//$datalist[] = array(26.45,26.14,26.55,26.34,25.54,26.53,26.14,26.24,25.18,26.56,26.34,26.32,26.43,26.23);
			//$datalist[] = array(27.45,27.14,27.55,25.34,28.54,28.53,26.14,27.24,25.88,27.56,27.04,27.62,28.43,28.03);
			
			//$datacolors = array($navy,$red,$darkred);
			 
			$diff = $highest - $lowest;
			$mult = ($rows * $cellheight) / $diff;
			$labels = array();//38,36,34,32);
			for($n=$lowest; $n<=$highest; $n+=2)
				$labels[] = $n;
				
			for($n=0; $n<count($labels); $n++)
			{
				$val = $labels[$n];
				$val_diff = $highest - $val;
				draw_string($image,$chartx + ($cols * $cellwidth) + 10,$charty + floor($val_diff * $mult),$val,$fontsize,$darkgrey,"middle");
			}
			/*for($i=0; $i<count($datalist); $i++)
			{
				$data = $datalist[$i];
				for($n=0; $n<count($data); $n++)
				{
					$val = $data[$n];
					if($lowest=="" || $val < $lowest) $lowest = $val;
					if($highest=="" || $val > $highest) $highest = $val;
				}
			}*/
			
			$debug = "";
			if($lowest!="" && $highest!="")
			{
				for($i=0; $i<count($datalist); $i++)
				{
					$data = $datalist[$i];
					$dc = $datacolors[$i];
					$dweight = $dataweight[$i];
					$lastx = "";
					$lasty = "";
					for($n=0; $n<count($data); $n++)
					{
						$val = $data[$n];
						$val_diff = $highest - $val;
						$val_x = ($n + 1) * $cellwidth;
						$val_y = floor($val_diff * $mult);
						if($n==0)
						{
							$lastx = $n * $cellwidth;
							$lasty = $val_y;
						}
						chart_line($image, $lastx, $lasty, $val_x, $val_y, $dc, $dweight);
						//$debug .= $lastx . "/" . $lasty . "_" . $val_x . "/" . floor(($val_diff * $mult)) . " ";
						$lastx = $val_x;
						$lasty = $val_y;
						
						if($i==$self_index)
						{
							$c = $n + 1;
							$show_val = number_format($val,"2",":","");
							$yoffset = 15;
							if(($c % 2)==0)
								$yoffset = $yoffset + ($fontsize * 5);
							if($cols < 20)
								draw_string($image,$chartx + ($c * $cellwidth),(($rows + 1) * $cellheight) + $yoffset,$show_val,$fontsize,$darkgrey,"center");
						}
					}
				}
			}
			 
			imagefilledrectangle($image, 0, 0, $chartx + 400, $charty - 1, $white); 
			$colstep=1;
			if($cols > 20) $colstep = 10;
			for($c=1; $c<=$cols; $c+=$colstep)
			{
				draw_string($image,$chartx + ($c * $cellwidth),10,$c,$fontsize,$darkgrey,"center");
			}
			//imagelinethick($image, 0, 0, 200, 200, $navy, 6);
			// flush image
			header('Content-type: image/png');
			imagepng($image);
			imagedestroy($image);
		}
	}
	else
	{
		$lap_titles = array();
		for($i=0; $i<$total_laps; $i++)
		{
			$lap_titles[] = ($i + 1);
		}
		
		//echo "results";
		//exit();
		require_once ('/home/poslavu/public_html/3rd_party/jpgraph/src/jpgraph.php');
		require_once ('/home/poslavu/public_html/3rd_party/jpgraph/src/jpgraph_line.php');
		require_once ('/home/poslavu/public_html/3rd_party/jpgraph/src/jpgraph_bar.php');
		
		$showsched = $_GET['showsched'];
		
		// New graph with a background image and drop shadow
		$graph = new Graph(450,300);
		//$graph->SetBackgroundImage("/home/poslavu/public_html/3rd_party/jpgraph/src/Examples/tiger_bkg.png",BGIMG_FILLFRAME);
		//$graph->SetShadow();
		
		// Use an integer X-scale
		$graph->SetScale("textlin");
		
		// Set title and subtitle
		$graph->title->Set("Race Results");
		
		// Use built in font
		$graph->title->SetFont(FF_FONT1,FS_BOLD);
		
		// Make the margin around the plot a little bit bigger
		// then default
		$graph->img->SetMargin(40,140,40,80);	
		
		// Slightly adjust the legend from it's default position in the
		// top right corner to middle right side
		//$graph->legend->Pos(0.05,0.5,"right","center");
		
		// Display every 10:th datalabel
		//$graph->xaxis->SetTextTickInterval(6);
		//$graph->xaxis->SetTextLabelInterval(2);
		//$graph->xaxis->SetTickLabels($lap_titles);
		//$graph->xaxis->SetLabelAngle(90);
		
		// Create a red line plot
		$scount = 0;
		$avg = array();
		$avg_count = array();
		for($i=0; $i<$total_laps; $i++)
		{
			//$avg[$i] = 0;
			$avg_count[$i] = 0;
		}
		
		$points = array();
		foreach($all_laps as $schedid => $laps)
		{
			$scount ++;
			$graph_laps = array();
			for($i=0; $i<$total_laps; $i++)
			{
				if(isset($laps[$i]))
				{
					$graph_laps[] = $laps[$i];
					$avg[$i] += $laps[$i];
					$avg_count[$i] ++;
				}
			}
			if($schedid==$showsched || $scount==1)
			{
				$points[$scount] = new LinePlot($graph_laps);
				if($schedid==$showsched)
				{
					$points[$scount]->SetWeight(3);
					$points[$scount]->SetColor("#880000");
					if($scount==1)
						$points[$scount]->SetLegend("Your Laps (Best)");
					else
						$points[$scount]->SetLegend("Your Laps");
					$race_place = $scount;
				}
				else
				{
					$points[$scount]->SetWeight(1);
					$points[$scount]->SetColor("#000088");
					$points[$scount]->SetLegend("Best");
				}
				//$p1->SetLegend("");
				$graph->Add($points[$scount]);
				//echo "<td>" . $laps[$i] . "</td>";
			}
		}
		
		for($i=0; $i<$total_laps; $i++)
		{
			if($avg_count[$i] > 0)
				$avg[$i] = $avg[$i] / $avg_count[$i];
		}
		
		$p1 = new LinePlot($avg);
		$p1->SetColor("#008800");
		$p1->SetWeight(1);
		$p1->SetLegend("Average");
		$graph->Add($p1);
			
		// Finally output the  image
		$graph->Stroke();
	}
?>

<?php

} else if($uripage =="results_sheet.php") {
//--------------------------------------------------- results_sheet.php ----------------------------------------------//
?>

<?php
	$use_new_sheet = true;
	//if(strpos($company_code,"jersey"))
		//$use_new_sheet = true;
		
	if(isset($trackid))
	{
		$track_query = lavu_query("select * from `lk_tracks` where `id`='[1]'",$trackid);
		if(lysql_num_rows($track_query))
		{
			$track_read = lysql_fetch_assoc($track_query);
			$track_name = $track_read['title'];
			$mult_percent = $track_read['print_ratio'];
			if($mult_percent=="old") $use_new_sheet = false;
		}
	}
		
	if($use_new_sheet)//isset($_GET['test']) && $_GET['test']==1)
	{
		if(isset($_GET['test']) && is_numeric($_GET['test']))
		{
			$display_multiplier = $_GET['test'];
			$lookup_multiplier = false;
		}
		else
		{
			$display_multiplier = 1;
			$lookup_multiplier = true;
		}
		if(isset($_GET['eventid']))
		{
			$event_query = lavu_query("select * from `lk_events` where `id`='[1]'",$_GET['eventid']);
			if(lysql_num_rows($event_query))
			{
				$event_read = lysql_fetch_assoc($event_query);
				$trackid = $event_read['trackid'];
				$_GET['trackid'] = $trackid;
			}
		}
		if(isset($trackid))
		{
			$track_query = lavu_query("select * from `lk_tracks` where `id`='[1]'",$trackid);
			if(lysql_num_rows($track_query))
			{
				$track_read = lysql_fetch_assoc($track_query);
				$track_name = $track_read['title'];
				$mult_percent = $track_read['print_ratio'];
				$mult_percent = trim(str_replace("%","",$mult_percent));
				if(is_numeric($mult_percent) && $lookup_multiplier)
				{
					$display_multiplier = $mult_percent / 100;
					if(isset($_GET['test2'])) echo "multiplier: " . $display_multiplier . " percentage: " . $mult_percent;
				}
			}
		}
	
		function mult_css_properties($code,$tags,$mult)
		{
			return mult_tag_properties($code,$tags,$mult,":");
		}
		
		function mult_tag_properties($code,$tags,$mult,$sep="=")
		{
			$tags = explode(",",$tags);
			for($t=0; $t<count($tags); $t++)
			{
				$tag = trim($tags[$t]);
				if($tag!="")
				{
					$code = str_replace($tag . " ".$sep,$tag . $sep,$code);
					$code_parts = explode($tag . $sep,$code);
					$setcode = $code_parts[0];
					for($i=1; $i<count($code_parts); $i++)
					{
						$str = $code_parts[$i];
						$append = $str;
						if(strlen($str) > 0)
						{
							$firstchar = substr(trim($str),0,1);
							if($firstchar=="'" || $firstchar=='"')
							{
								$str_parts = explode($firstchar,$str);
								$setnum = trim($str_parts[1]);
								if(is_numeric($setnum))
								{
									$append = $str_parts[0] . $firstchar . ($setnum * $mult) . $fistchar;
									for($n=2;$n<count($str_parts);$n++)
									{
										$append .= $firstchar . $str_parts[$n];
									}
								}
							}
							else if(is_numeric($firstchar))
							{
								$x=0;
								$setnum = "";
								$ws = "";
								while($x<strlen($str) && (is_numeric($str[$x]) || ($str[$x]==" " && trim($setnum==""))))
								{
									if($str[$x]==" ") $ws .= " ";
									$setnum .= $str[$x];
									$x++;
								}
								if(is_numeric(trim($setnum)))
								{
									$append = $ws . (trim($setnum) * $mult) . substr($str,$x);
								}
							}
						}
						$setcode .= $tag . $sep . $append;
					}
					$code = $setcode;
				}
			}
			$code = str_replace("ratio=R","ratio=".$mult,$code);
			return $code;
		}
	
		$fp = fopen("results_sheet_template2.php","r");
		$sheet_template = fread($fp,filesize("results_sheet_template2.php"));
		fclose($fp);
		
		$sheet_parts = explode("<!--startsheet-->",$sheet_template);
		if(count($sheet_parts) > 1)
		{
			$sheet_start = $sheet_parts[0];
			$sheet_parts = explode("<!--endsheet-->",$sheet_parts[1]);
			$sheet_mid = $sheet_parts[0];
			if(count($sheet_parts) > 1)
			{
				$sheet_end = $sheet_parts[1];
			}
			else $sheet_end = "";
		}
		else
		{
			$sheet_start = "";
			$sheet_mid = $sheet_parts[0];
			$sheet_end = "";
			echo "Invalid sheet type";
			exit();
		}
		$sheet_template = $sheet_mid;
		
		$display_racers = array();
		foreach($all_laps as $schedid => $laps)
		{
			$rkart = $racer_info[$schedid]['kartid'];
			$rplace = $rinfo_pos[$rkart];
			$display_racers[$rplace] = array($racer_info[$schedid]['name'],$racer_info[$schedid]['bestlap']);
			//$all_best_times .= "<nobr>".$racer_info[$schedid]['name'] . ": " . $racer_info[$schedid]['bestlap'] . "</nobr><br>";
		}
		
		$best_lap_ofweek = array(array(0,0));
		$week_ago = date("Y-m-d",mktime(0,0,0,date("m"),date("d")-7,date("Y")));
		$today = date("Y-m-d");
		$best_query = lavu_query("select * from `lk_race_results` where `date`>='[1]' and `date`<='$today' and `trackid`='[2]' and `bestlap`>18 order by (`bestlap` * 1) asc limit 25",$week_ago,$trackid);
		$best_count = 0;
		while($best_read = lysql_fetch_assoc($best_query))
		{
			$cust_query = lavu_query("select * from `lk_customers` where `id`='[1]'",$best_read['customerid']);
			if(lysql_num_rows($cust_query))
			{
				$best_count++;
				$cust_read = lysql_fetch_assoc($cust_query);
				$best_lap_ofweek[] = array(trim($cust_read['f_name'] . " " . $cust_read['l_name']),$best_read['bestlap']);
			}
		}
		
		echo mult_css_properties($sheet_start,"font-size",$display_multiplier);
		//echo $sheet_start;
		$scount = 0;
		//foreach($all_laps as $schedid => $laps)
		//{
			//$sched_query = lavu_query("select * from `lk_event_schedules` LEFT JOIN `lk_customers` ON `lk_event_schedules`.`customerid`=`lk_customers`.`id` LEFT JOIN `lk_events` ON `lk_event_schedules`.`eventid`=`lk_events`.`id` where `lk_event_schedules`.`id`='[1]'",$schedid);
		foreach($ordered_racers as $rinfo)
		{
			$scount++;
			$schedid = $rinfo['schedid'];
			$sched_query = lavu_query("select * from `lk_event_schedules` LEFT JOIN `lk_customers` ON `lk_event_schedules`.`customerid`=`lk_customers`.`id` LEFT JOIN `lk_events` ON `lk_event_schedules`.`eventid`=`lk_events`.`id` where `lk_event_schedules`.`id`='[1]'",$schedid);
			if(lysql_num_rows($sched_query))
			{
				$sched_read = lysql_fetch_assoc($sched_query);
				$sheet = $sheet_template;
				
				$rr_query = lavu_query("select * from `lk_race_results` where `eventid`='[1]' and `customerid`='[2]'",$sched_read['eventid'],$sched_read['customerid']);
				if(lysql_num_rows($rr_query))
				{
					$rr_read = lysql_fetch_assoc($rr_query);
					$set_rank = $rr_read['ranking'];
				}
				
				$laps = $all_laps[$schedid];
				$placed_pos = $rinfo_pos[$sched_read['kartid']];
				
				if($placed_pos > 10 && $placed_pos < 20) $att = "th";
				else if($placed_pos % 10 == 1) $att = "st";
				else if($placed_pos % 10 == 2) $att = "nd";
				else if($placed_pos % 10 == 3) $att = "rd";
				else $att = "th";
				
				$lap_times = "";
				for($n=0; $n<count($laps); $n++)
				{
					if(isset($laps[$n]))
					{
						$lap_times .= "<nobr>".($n + 1) . ": " . $laps[$n] . "</nobr><br>";
					}
				}
				
				$edate_parts = explode("-",$event_date);
				$show_event_date = $edate_parts[1] . "/" . $edate_parts[2] . "/" . $edate_parts[0];
				
				$show_rank = "";
				$show_tip = "";
				if(isset($set_rank))
				{
					if($set_rank==1) 
					{
						$show_rank = "Amateur";
						$show_tip = "Follow a faster driver to learn their race lines, braking points and overall driving technique. Maintaining momentum is the key to going faster.";
					}
					else if($set_rank==2) 
					{
						$show_rank = "Intermediate";
						$show_tip = "Consider being smoother with your steering. Try not to over steer in the corners or make sharp, abrupt turns. Try to also be smoother on the brakes.";
					}
					else if($set_rank==3) 
					{
						$show_rank = "Expert";
						$show_tip = "Consider using less brakes and trying to get back into the throttle earlier while simultaneously trying to minimize sliding too much through turns.";
					}
				}
				$visit_query = lavu_query("select distinct `date` from `lk_race_results` where `customerid`='[1]'",$sched_read['customerid']);
				$show_visits = lysql_num_rows($visit_query);
				$total_races_query = lavu_query("select * from `lk_race_results` where `customerid`='[1]'",$sched_read['customerid']);
				$show_total_races = lysql_num_rows($total_races_query);
				$exp_query = lavu_query("select * from `lk_race_results` where `customerid`='[1]' and `ranking`='3'",$sched_read['customerid']);
				$show_exp_count = lysql_num_rows($exp_query);
				$int_query = lavu_query("select * from `lk_race_results` where `customerid`='[1]' and `ranking`='2'",$sched_read['customerid']);
				$show_int_count = lysql_num_rows($int_query);
				$ama_query = lavu_query("select * from `lk_race_results` where `customerid`='[1]' and `ranking`='1'",$sched_read['customerid']);
				$show_ama_count = lysql_num_rows($ama_query);
												
				$tr = array();
				$tr[] = array("fullname",trim($sched_read['f_name'] . " " . $sched_read['l_name']));
				$tr[] = array("racer name",$sched_read['racer_name']);						
				$tr[] = array("race name",$event_type_title);
				$tr[] = array("race format",$event_race_format);
				$tr[] = array("date",$show_event_date);
				$tr[] = array("visits",$show_visits);
				$tr[] = array("rank",$show_rank);
				$tr[] = array("track name",$track_name);
				$tr[] = array("all best times",$all_best_times);
				$tr[] = array("lap times",$lap_times);
				$tr[] = array("placed",$placed_pos);
				$tr[] = array("race date",display_date($sched_read['date']));
				
				$tr[] = array("tip","To improve you should...
$show_tip ");
				$tr[] = array("chart","/components/lavukart/race_scores.php?mode=results_graph&cc=".$company_code . "_key_".$company_code_key."&loc_id=".$_GET['loc_id']."&trackid=".$_GET['trackid']."&eventid=".$_GET['eventid']."&showsched=$schedid");
				$tr[] = array("chart_new","/components/lavukart/race_scores.php?mode=results_graph&cc=".$company_code . "_key_".$company_code_key."&loc_id=".$_GET['loc_id']."&trackid=".$_GET['trackid']."&eventid=".$_GET['eventid']."&showsched=$schedid&graphtype=new");
				
				$tr[] = array("exp_count",$show_exp_count);
				$tr[] = array("int_count",$show_int_count);
				$tr[] = array("ama_count",$show_ama_count);
				$tr[] = array("total_races",$show_total_races);
				
				for($n=1; $n<=14; $n++)
				{
					if(isset($display_racers[$n]) && is_array($display_racers[$n]))
					{
						$tr[] = array("racer".$n,$display_racers[$n][0]);
						$tr[] = array("rtime".$n,$display_racers[$n][1]);
						$tr[] = array("p".$n,$n);
					}
					else
					{
						$tr[] = array("racer".$n,"&nbsp;");
						$tr[] = array("rtime".$n,"&nbsp;");
						$tr[] = array("p".$n,"");
					}
				}
				for($n=1; $n<=5; $n++)
				{
					if(isset($best_lap_ofweek[$n]) && is_array($best_lap_ofweek[$n]))
					{
						$tr[] = array("blracer".$n,$best_lap_ofweek[$n][0]);
						$tr[] = array("bltime".$n,$best_lap_ofweek[$n][1]);
						$tr[] = array("bl".$n,$n);
					}
					else
					{
						$tr[] = array("blracer".$n,"&nbsp;");
						$tr[] = array("bltime".$n,"&nbsp;");
						$tr[] = array("bl".$n,"");
					}
				}
				$cups = array(0,4,4,4,4);
				for($n=1; $n<=4; $n++)
				{
					for($m=1; $m<=5; $m++)
					{
						if($cups[$n]>=$m)
							$tr[] = array("t".$n."_".$m,"<img src=\"images/resultsheet/cup.png\" width=\"20\" height=\"18\">");
						else
							$tr[] = array("t".$n."_".$m,"&nbsp;");
					}
				}
				for($n=0; $n<count($tr); $n++)
				{
					$sheet = str_replace("[".$tr[$n][0]."]",$tr[$n][1],$sheet);
				}
				
				if($scount < count($ordered_racers)) echo "<div style='page-break-after:always'>"; else echo "<div>";
				//echo $sheet;
				echo mult_tag_properties($sheet,"width,height",$display_multiplier);
				echo "</div>";
			}
		}		
		echo $sheet_end;
	}
	else
	{
		$fp = fopen("results_sheet_template.php","r");
		$sheet_template = fread($fp,filesize("results_sheet_template.php"));
		fclose($fp);
	
		$all_best_times = "";
		foreach($all_laps as $schedid => $laps)
		{
			$all_best_times .= "<nobr>".$racer_info[$schedid]['name'] . ": " . $racer_info[$schedid]['bestlap'] . "</nobr><br>";
		}
		
		$scount = 0;
		foreach($all_laps as $schedid => $laps)
		{
			$scount++;
			$sched_query = lavu_query("select * from `lk_event_schedules` LEFT JOIN `lk_customers` ON `lk_event_schedules`.`customerid`=`lk_customers`.`id` LEFT JOIN `lk_events` ON `lk_event_schedules`.`eventid`=`lk_events`.`id` where `lk_event_schedules`.`id`='[1]'",$schedid);
			if(lysql_num_rows($sched_query))
			{
				$sched_read = lysql_fetch_assoc($sched_query);
				$sheet = $sheet_template;
				
				$placed_pos = $rinfo_pos[$sched_read['kartid']];
				
				if($placed_pos > 10 && $placed_pos < 20) $att = "th";
				else if($placed_pos % 10 == 1) $att = "st";
				else if($placed_pos % 10 == 2) $att = "nd";
				else if($placed_pos % 10 == 3) $att = "rd";
				else $att = "th";
				
				$lap_times = "";
				for($n=0; $n<count($laps); $n++)
				{
					if(isset($laps[$n]))
					{
						$lap_times .= "<nobr>".($n + 1) . ": " . $laps[$n] . "</nobr><br>";
					}
				}
				
				$tr = array();
				$tr[] = array("fullname",trim($sched_read['f_name'] . " " . $sched_read['l_name']));
				$tr[] = array("racer name",$sched_read['racer_name']);						
				$tr[] = array("all best times",$all_best_times);
				$tr[] = array("lap times",$lap_times);
				$tr[] = array("placed",$placed_pos . $att);
				$tr[] = array("race date",display_date($sched_read['date']));
				$tr[] = array("chart","/components/lavukart/race_scores.php?mode=results_graph&cc=".$company_code . "_key_".$company_code_key."&loc_id=".$_GET['loc_id']."&trackid=".$_GET['trackid']."&eventid=".$_GET['eventid']."&showsched=$schedid");
				
				for($n=0; $n<count($tr); $n++)
				{
					$sheet = str_replace("[".$tr[$n][0]."]",$tr[$n][1],$sheet);
				}
				
				if($scount > 1) echo "<div style='page-break-after:always'></div>";
				echo $sheet;
			}
		}
	}	
?>
<?php

} else if($uripage =="results_sheet_template.php") {
//--------------------------------------------------- results_sheet_template.php ----------------------------------------------//
?>

<style>
body, table {
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size:12px;
}
.titles {
	font-size:14px;
}
</style>

<table align="center" style="table-layout:fixed">
	<tr>
		<td colspan='4' align='center'>
			<table>
				<td class="titles">
					<nobr>Name: [fullname]</nobr>
					<br><nobr>Racer Name: [racer name]</nobr>
				</td>
				<td style="width:120px" class="titles">&nbsp;&nbsp;&nbsp;&nbsp;</td>
				<td>
					<nobr>Placed [placed]</nobr>
					<br><nobr>Date: [race date]</nobr>
				</td>
			</table>
			<hr />
		</td>
		<td>
	</tr>
	<tr valign="top">
		<td valign="top" style="width:120px">		
			<nobr><b>Race Results</b></nobr>
			<br>[all best times]			
		</td>
		<td>&nbsp;&nbsp;</td>
		<td style="width:120px">
			<b><nobr>Your Laps</nobr></b>
			<br>[lap times]
		</td>
		<td valign="top">
			<img src="[chart]" />
		</td>
	</tr>
    <tr><td colspan="3" height="460"></td></tr>
</table>
<?php

} else if($uripage =="results_sheet_template2.php") {
//--------------------------------------------------- results_sheet_template2.php ----------------------------------------------//
?>


<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Kart Lavu : Print Results</title>
<style type="text/css">
<!--
.titletext {
	font-family: Arial, Helvetica, sans-serif;
	font-weight: 700;
	font-size: 16px;
	color: #000;
}

.pagetext {
	font-family:  Arial, Helvetica, sans-serif;
	font-weight: 500;
	font-size: 13px;
	color: #666;
}
.pagetext_red {
	font-family:  Arial, Helvetica, sans-serif;
	font-weight: bold;
	font-size: 13px;
	color: #ee2d32;
}
.pagetext_blue {
	font-family:  Arial, Helvetica, sans-serif;
	font-weight: bold;
	font-size: 13px;
	color: #3653a4;
}

.helptext {
	font-family: Arial, Helvetica, sans-serif;
	font-weight: 700;
	font-size: 11px;
	color: #000;
}
.titlesub {
	font-family: Arial, Helvetica, sans-serif;
	font-weight: 500;
	font-size: 14px;
	font-style:italic;
	color: #666;
}

-->
</style>
</head>

<body>
<!--startsheet-->
<table width="1020" height="660" border="0" align="center" cellpadding="0" cellspacing="0" id="pagetext">
  <tr>
    <td height="51" colspan="2" align="left" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="left" valign="top">
      <table width="960" height="555" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="460" align="left" valign="top"><table width="460" height="555" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td height="132" align="left" valign="top">&nbsp;</td>
            </tr>
            <tr>
              <td align="left" valign="top"><table width="460" height="139" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="60" align="left" valign="middle"  class="titletext">&nbsp;</td>
                  <td width="400" align="left" valign="middle"  class="titletext"><table width="400" border="0" cellpadding="0" cellspacing="0" class="pagetext">
                    <tr>
                      <td align="left" class="titletext">Driver Name</td>
					  <td class="titletext">[fullname] <font style='font-size:10px'>([racer name])</font></td>
                      <td width="142" rowspan="3"><img src="images/resultsheet/number[placed].png" width="142" height="50"></td>
                    </tr>
                    <tr>
                      <td width="100" align="right">Date:</td>
                      <td>[date]</td>
                    </tr>
                    <tr>
                      <td align="right">Visits:</td>
                      <td>[visits]</td>
                    </tr>
                    <tr>
                      <td align="right">Rank:</td>
                      <td colspan="2">[rank]</td>
                    </tr>
                    <tr>
                      <td align="right">Race Name:</td>
                      <td colspan="2">[race name]</td>
                    </tr>
                    <tr>
                      <td align="right">Race Format:</td>
                      <td colspan="2">[race format]</td>
                    </tr>
                    <tr>
                      <td align="right">Track Name:</td>
                      <td colspan="2">[track name]</td>
                    </tr>
                  </table></td>
                </tr>
                </table></td>
            </tr>
            <tr>
              <td height="156" align="left" valign="top"><table width="460" height="156" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td height="22" colspan="4" align="left" valign="bottom" background="resize_image.php?image=images/resultsheet/flagline.png&ratio=R" class="titletext"><table width="400" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="60"></td>
                      <td align="left" valign="bottom" class="titletext">Race Results</td>
                    </tr>
                  </table></td>
                </tr>
                <tr>
                  <td width="60" align="left" valign="top"></td>
                  <td width="196" align="left" valign="top"><table width="196" border="0" cellpadding="0" cellspacing="0" class="pagetext" >
                    <tr>
                      <td width="12" align="left" valign="middle" class="pagetext_red">[p1]</td>
                      <td width="148" align="left" valign="middle" class="pagetext_red">[racer1]</td>
                      <td width="36" align="center" valign="middle" class="pagetext_red">[rtime1]</td>
                    </tr>
                    <tr>
                      <td width="12" align="left" valign="middle" class="pagetext_blue">[p2]</td>
                      <td align="left" valign="middle" class="pagetext_blue">[racer2]</td>
                      <td align="center" valign="middle" class="pagetext_blue">[rtime2]</td>
                    </tr>
                    <tr>
                      <td width="12" align="left" valign="middle">[p3]</td>
                      <td align="left" valign="middle">[racer3]</td>
                      <td align="center" valign="middle">[rtime3]</td>
                    </tr>
                    <tr>
                      <td width="12" align="left" valign="middle">[p4]</td>
                      <td align="left" valign="middle">[racer4]</td>
                      <td align="center" valign="middle">[rtime4]</td>
                    </tr>
                    <tr>
                      <td width="12" align="left" valign="middle">[p5]</td>
                      <td align="left" valign="middle">[racer5]</td>
                      <td align="center" valign="middle">[rtime5]</td>
                    </tr>
                    <tr>
                      <td width="12" align="left" valign="middle">[p6]</td>
                      <td align="left" valign="middle">[racer6]</td>
                      <td align="center" valign="middle">[rtime6]</td>
                    </tr>
                    <tr>
                      <td width="12" align="left" valign="middle">[p7]</td>
                      <td align="left" valign="middle">[racer7]</td>
                      <td align="center" valign="middle">[rtime7]</td>
                    </tr>
                  </table></td>
                  <td width="8" align="left" valign="top"></td>
                  <td width="196" align="left" valign="top"><table width="196" border="0" cellpadding="0" cellspacing="0" class="pagetext" >
                    <tr>
                      <td width="16">[p8]</td>
                      <td width="144" align="left" valign="middle">[racer8]</td>
                      <td width="36" align="center" valign="middle">[rtime8]</td>
                    </tr>
                    <tr>
                      <td width="16">[p9]</td>
                      <td align="left" valign="middle">[racer9]</td>
                      <td width="36" align="center" valign="middle">[rtime9]</td>
                    </tr>
                    <tr>
                      <td width="16">[p10]</td>
                      <td align="left" valign="middle">[racer10]</td>
                      <td width="36" align="center" valign="middle">[rtime10]</td>
                    </tr>
                    <tr>
                      <td width="16">[p11]</td>
                      <td align="left" valign="middle">[racer11]</td>
                      <td width="36" align="center" valign="middle">[rtime11]</td>
                    </tr>
                    <tr>
                      <td width="16">[p12]</td>
                      <td align="left" valign="middle">[racer12]</td>
                      <td width="36" align="center" valign="middle">[rtime12]</td>
                    </tr>
                    <tr>
                      <td width="16">[p13]</td>
                      <td align="left" valign="middle">[racer13]</td>
                      <td width="36" align="center" valign="middle">[rtime13]</td>
                    </tr>
                    <tr>
                      <td width="16">[p14]</td>
                      <td align="left" valign="middle">[racer14]</td>
                      <td width="36" align="center" valign="middle">[rtime14]</td>
                    </tr>
                  </table></td>
                </tr>
              </table></td>
            </tr>
            <tr>
              <td height="128" align="left" valign="top"><table width="460" height="128" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td height="22" colspan="4" align="left" valign="bottom" background="resize_image.php?image=images/resultsheet/flagline.png&ratio=R" class="titletext"><table width="400" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="60">&nbsp;</td>
                      <td class="titletext">Best Lap This Week</td>
                    </tr>
                  </table></td>
                </tr>
                <tr>
                  <td width="60" align="left" valign="top">&nbsp;</td>
                  <td width="196" align="left" valign="middle"><table width="196" border="0" cellpadding="0" cellspacing="0" class="pagetext">
                    <tr>
                      <td width="20">[bl1]</td>
                      <td>[blracer1]</td>
                      <td align="right" valign="middle">[bltime1]</td>
                    </tr>
                    <tr>
                      <td width="20">[bl2]</td>
                      <td>[blracer2]</td>
                      <td align="right" valign="middle">[bltime2]</td>
                    </tr>
                    <tr>
                      <td width="20">[bl3]</td>
                      <td>[blracer3]</td>
                      <td align="right" valign="middle">[bltime3]</td>
                    </tr>
                    <tr>
                      <td width="20">[bl4]</td>
                      <td>[blracer4]</td>
                      <td align="right" valign="middle">[bltime4]</td>
                    </tr>
                    <tr>
                      <td width="20">[bl5]</td>
                      <td>[blracer5]</td>
                      <td align="right" valign="middle">[bltime5]</td>
                    </tr>
                    </table></td>
                  <td width="8" align="left" valign="top"></td>
                  <td width="196" align="center" valign="middle" background=""><img src="images/resultsheet/speedsheet.png" width="174" height="80"></td>
                </tr>
              </table></td>
            </tr>
          </table></td>
          <td width="5">&nbsp;</td>
          <td width="495" align="left" valign="top"><table width="495" height="555" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td height="317" align="left" valign="top"><img src="[chart_new]" width="500" height="330"></td>
              </tr>
            <tr>
              <td height="131" align="left" valign="top"><table width="495" height="139" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td height="30" colspan="4" align="left" valign="middle" class="titletext"><table width="495" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="20">&nbsp;</td>
                      <td align="center" class="titlesub">Race Information</td>
                    </tr>
                  </table></td>
                </tr>
                <tr>
                  <td width="20" align="left" valign="top"></td>
                  <td width="196" align="left" valign="top"><table width="196" border="0" cellpadding="2" cellspacing="1" class="pagetext" bgcolor="#b1b1b1">
                    <tr>
                      <td width="28" height="23" bgcolor="#6d6e71">&nbsp;</td>
                      <td align="center" bgcolor="#FFFFFF">Amateur</td>
                      <td align="center" valign="middle" bgcolor="#FFFFFF">[ama_count]</td>
                    </tr>
                    <tr>
                      <td height="23" bgcolor="#b2b3b6">&nbsp;</td>
                      <td align="center" bgcolor="#FFFFFF">Intermediate</td>
                      <td align="center" valign="middle" bgcolor="#FFFFFF">[int_count]</td>
                    </tr>
                    <tr bgcolor="#FFFFFF">
                      <td height="23">&nbsp;</td>
                      <td align="center">Expert</td>
                      <td align="center" valign="middle">[exp_count]</td>
                    </tr>
                    <tr bgcolor="#FFFFFF">
                      <td height="23" colspan="2" align="center">Total Races</td>
                      <td align="center" valign="middle">[total_races]</td>
                    </tr>
                  </table></td>
                  <td width="8" align="left" valign="top"></td>
                  <td width="271" align="left" valign="top"><table width="252" border="0" cellpadding="2" cellspacing="1" class="pagetext" bgcolor="#b1b1b1">
                    <tr>
                      <td width="119" height="23" align="center" bgcolor="#FFFFFF">Speed</td>
                      <td width="20" align="center" valign="middle" bgcolor="#FFFFFF">[t1_1]</td>
                      <td width="20" align="center" valign="middle" bgcolor="#FFFFFF">[t1_2]</td>
                      <td width="20" align="center" valign="middle" bgcolor="#FFFFFF">[t1_3]</td>
                      <td width="20" align="center" valign="middle" bgcolor="#FFFFFF">[t1_4]</td>
                      <td width="20" align="center" valign="middle" bgcolor="#FFFFFF">[t1_5]</td>
                    </tr>
                    <tr>
                      <td height="23" align="center" bgcolor="#FFFFFF">Consistancy</td>
                      <td width="20" align="center" valign="middle" bgcolor="#FFFFFF">[t2_1]</td>
                      <td width="20" align="center" valign="middle" bgcolor="#FFFFFF">[t2_2]</td>
                      <td width="20" align="center" valign="middle" bgcolor="#FFFFFF">[t2_3]</td>
                      <td width="20" align="center" valign="middle" bgcolor="#FFFFFF">[t2_4]</td>
                      <td width="20" align="center" valign="middle" bgcolor="#FFFFFF">[t2_5]</td>
                    </tr>
                    <tr>
                      <td height="23" align="center" bgcolor="#FFFFFF">Passing</td>
                      <td width="20" align="center" valign="middle" bgcolor="#FFFFFF">[t3_1]</td>
                      <td width="20" align="center" valign="middle" bgcolor="#FFFFFF">[t3_2]</td>
                      <td width="20" align="center" valign="middle" bgcolor="#FFFFFF">[t3_3]</td>
                      <td width="20" align="center" valign="middle" bgcolor="#FFFFFF">[t3_4]</td>
                      <td width="20" align="center" valign="middle" bgcolor="#FFFFFF">[t3_5]</td>
                    </tr>
                    <tr>
                      <td height="23" align="center" bgcolor="#FFFFFF">Betterment</td>
                      <td width="20" align="center" valign="middle" bgcolor="#FFFFFF">[t4_1]</td>
                      <td width="20" align="center" valign="middle" bgcolor="#FFFFFF">[t4_2]</td>
                      <td width="20" align="center" valign="middle" bgcolor="#FFFFFF">[t4_3]</td>
                      <td width="20" align="center" valign="middle" bgcolor="#FFFFFF">[t4_4]</td>
                      <td width="20" align="center" valign="middle" bgcolor="#FFFFFF">[t4_5]</td>
                    </tr>
                  </table></td>
                </tr>
              </table></td>
              </tr>
            <tr>
              <td height="99" align="center" valign="middle"><table width="460" height="73
            " border="0" cellpadding="8" cellspacing="0" background="resize_image.php?image=images/resultsheet/tipbox.png&ratio=R" class="pagetext">
                <tr>
                  <td align="left" valign="middle"><p class="helptext">[tip]</p></td>
                  </tr>
                </table></td>
              </tr>
          </table></td>
        </tr>
    </table></td>
    <td width="60">&nbsp;</td>
  </tr>
  <tr>
    <td height="54" colspan="2" align="left" valign="top">&nbsp;</td>
  </tr>
</table>
<!--endsheet-->
</body>
</html>

<?php

} else if($uripage =="scoreboard.php") {
//--------------------------------------------------- scoreboard.php ----------------------------------------------//
?>

<html>
	<head>
		<title>Scoreboard</title>

<style type="text/css">
<!--
body {
	background-color: transparent;
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
.toptitles {
	font-family: Verdana, Geneva, sans-serif;
	font-size: 10px;
	font-weight: bold;
	color: #0D2960;
}

.carnumber {
	font-family: Verdana, Geneva, sans-serif;
	font-size: 16px;
	font-weight: bold;
	color: #FFF;
}
.creditnumber {	font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #3d4f6d;
}
.nametext {	font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
}
.gridposition {	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #5A75A0;
}
.icontext {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 11px;
	font-weight: bold;
	color: #5A75A0;
}
.subtitles {	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #5A75A0;
}
.title1 {	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 18px;
	font-weight: bold;
	color: #5A75A0;
}
.subtitles1 {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #5A75A0;
}
-->
</style>
	</head>

	<?php
  	//require_once(dirname(__FILE__) . "/../comconnect.php");
			
		if(reqvar("loc_id")) $loc_id = reqvar("loc_id");
		else if(sessvar_isset("loc_id")) $loc_id = sessvar("loc_id");
		set_sessvar("loc_id", $loc_id);
		
		$trackid = sessvar("trackid");
		if(!$trackid)
		{
			$track_query = lavu_query("select * from `lk_tracks` where `locationid`='$loc_id' and `_deleted`!='1' order by id asc limit 1");
			if(lysql_num_rows($track_query))
			{
				$track_read = lysql_fetch_assoc($track_query);
				$trackid = $track_read['id'];
				set_sessvar("trackid",$trackid);
			}
		}
  ?>

	<script type="text/javascript">
	function trim(stringToTrim) {
		return stringToTrim.replace(/^\s+|\s+$/g,"");
	}
	function setrp(n,key,val)
	{
		//alert(key + "_" + n + " = " + val[key + "_" + n]);
		document.getElementById(key + "_" + n).innerHTML = val[key + "_" + n];
	}
	function clearrp(n,key)
	{
		document.getElementById(key + "_" + n).innerHTML = "&nbsp;";
	}
	load_dots = ".";
	rnd = Math.floor(Math.random()*1111);
	function loadXMLDoc(set_eventid)
	{
		load_dots = load_dots + ".";
		if(load_dots.length > 8) load_dots = ".";
		//document.getElementById("show_server_time").innerHTML = load_dots;
	if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  xmlhttp=new XMLHttpRequest();
	  }
	else
	  {// code for IE6, IE5
	  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
	xmlhttp.onreadystatechange=function()
	  {
	  if (xmlhttp.readyState==4 && xmlhttp.status==200)
		{
			data = xmlhttp.responseText;
			data_split = data.split("&");
			vars = new Array();
			vars['laps_left'] = "";
			vars['server_time'] = "";
			for(i=0; i<data_split.length; i++)
			{
				data_part = trim(data_split[i]);
				if(data_part!="")
				{
					data_parts = data_part.split("=");
					if(data_parts.length > 1)
					{
						data_key = data_parts[0];
						data_val = data_parts[1];
						vars[data_key] = data_val;
					}
				}
			}
			document.getElementById("lap_status").innerHTML = "LAPS LEFT: <font style='font-size:16px'>" + vars['laps_left'] + "</font>";
			document.getElementById("show_server_time").innerHTML = vars['server_time'];
			
			if(vars['count'])
			{
				for(i=1; i<=vars['count']; i++)
				{
					setrp(i,"number",vars);
					setrp(i,"name",vars);
					setrp(i,"lastlap",vars);
					setrp(i,"bestlap",vars);
					setrp(i,"gap",vars);
					setrp(i,"showlap",vars);
				}
				for(i=vars['count'] + 1; i<=12; i++)
				{
					clearrp(i,"number");
					clearrp(i,"name");
					clearrp(i,"lastlap");
					clearrp(i,"bestlap");
					clearrp(i,"gap");
					clearrp(i,"showlap");
				}
			}
		}
	  }
	  rnd++;
	xmlhttp.open("GET","race_scores.php?cc=<?php echo $company_code_full?>&loc_id=<?php echo $loc_id?>&trackid=<?php echo $trackid?>&eventid=active&rnd=<?php echo date("His");?>" + rnd + "&mode=data",true);
	xmlhttp.send();
	}
	</script>

	<body>
		<table width="964" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td height="42" align="center" class="title1">&nbsp;</td>
            <td align="left" valign="bottom" ><table width="900" border="0" cellspacing="0" cellpadding="8">
              <tr>
                <td width="32" class="toptitles">KART</td>
                <td width="246" class="toptitles">
                	<table cellspacing=0 cellpadding=0 width='100%'><td width='50%' class='toptitles'>RACER NAME</td><td width='50%' class='toptitles' id='show_server_time'>&nbsp;</td></table>
                   </td>
                <td width="200" class="toptitles" id="lap_status">LAPS: </td>
                <td width="100" align="center" class="toptitles">LAST LAP</td>
                <td width="100" align="center" class="toptitles">FASTEST LAP</td>
                <td width="100" align="center" class="toptitles">GAP TIME</td>
                <td width="25" align="right" class="toptitles">LAP</td>
              </tr>
            </table></td>
          </tr>
          <?php 
		  	for($i=1; $i<=12; $i++) { ?>
          <tr>
            <td width="50" height="46" align="center" class="title1"><?php echo $i?></td>
            <td width="614" background="images/tab_wide_bg.png"><table width="900" height="44" border="0" cellspacing="0" cellpadding="8">
              <tr>
                <td width="32" align="center" valign="bottom" class="nametext"><span class="carnumber" id="number_<?php echo $i?>">&nbsp;</span></td>
                <td width="450" class="nametext" id="name_<?php echo $i?>">&nbsp;</td>
                <td width="100" class="nametext" id="lastlap_<?php echo $i?>">&nbsp;</td>
                <td width="100" class="nametext" id="bestlap_<?php echo $i?>">&nbsp;</td>
                <td width="100" class="nametext" id="gap_<?php echo $i?>">&nbsp;</td>
                <td width="21" align="center" valign="middle" class="nametext" id="showlap_<?php echo $i?>">&nbsp;</td>
              </tr>
            </table></td>
          </tr>
          <?php } ?>
          <tr>
            <td height="4" align="center" valign="middle"></td>
            <td  height="4"></td>
          </tr>
        </table>
	</body>
	<script language='javascript'>loadXMLDoc();</script>
	<script language='javascript'>setInterval(function(){loadXMLDoc()},1000);</script>
</html>
<?php

} else if($uripage =="scoreboard_wrapper.php") {
//--------------------------------------------------- scoreboard_wrapper.php ----------------------------------------------//
?>

<html>
	<head>
		<title>Scoreboard Wrapper</title>

<style type="text/css">
<!--
body {
	background-color: transparent;
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
-->
</style>
	</head>

	<?php
	
		ini_set("display_errors","1");
  	//require_once(dirname(__FILE__) . "/../comconnect.php");
			
		if(reqvar("loc_id")) $loc_id = reqvar("loc_id");
		else if(sessvar_isset("loc_id")) $loc_id = sessvar("loc_id");
		set_sessvar("loc_id", $loc_id);
  ?>

	<body>
		<table width='760' cellspacing='0' cellpadding='0'>
			<tr><td height='555' align='center' valign='top'>&nbsp;</td></tr>
		</table>
	</body>
</html>
<?php

} else if($uripage =="select_grid.php") {
//--------------------------------------------------- select_grid.php ----------------------------------------------//
?>

<?php
	//require_once(dirname(__FILE__) . "/../comconnect.php");
	//ini_set("display_errors","1");
	/*if(reqvar("tab_name") == "Group Events") {
		$parent_component = "group_events";
		$parent_component_file = "lavukart/group_events.php";
	} else if(reqvar("tab_name") == "Pit") {
		$parent_component = "events_overlay_wrapper;events";
		$parent_component_file = "lavukart/events_overlay_wrapper.php;lavukart/events.php";
	} else {
		$parent_component = "events_wrapper;events";
		$parent_component_file = "lavukart/events_wrapper.php;lavukart/events.php";
	}*/
	$parent_component = "car_assignments_wrapper;car_assignments";
	$parent_component_file = "lavukart/car_assignments_wrapper.php;lavukart/car_assignments.php";
?>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<style type="text/css">
<!--

body, table {
	font-family:Verdana, Arial, Helvetica, sans-serif;
}

.caltext {
	font-family: "Trebuchet MS", Arial, Helvetica, sans-serifVerdana, Geneva, sans-serif;
	font-size: 16px;
	font-weight: bold;
	color: #154473;
}
-->
</style>
</head>

<body style='margin:0px'>
<table width="423" height="374" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="20" height="40">&nbsp;</td>
    <td width="120" align="center" valign="middle">&nbsp;</td>
    <td width="134" onClick="window.location = '_DO:cmd=close_overlay'">&nbsp;</td>
    <td width="120" align="center" valign="middle">&nbsp;</td>'
    <td width="21">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="5" align="center" valign="top">
		<?php
			$locid = reqvar("locid");
			if($locid)
			{
	        	echo "<span style='color:#ffffff'>Select Lane</b>:</font>";
				echo "<br><br>";
				echo "<table cellspacing=4 cellpadding=12>";
				echo "<tr>";
				for($i=1; $i<=6; $i++)
				{
					if($i==4) echo "</tr><tr>";
					echo "<td style='border:solid 1px #777777; padding-left:40px; padding-right:40px' bgcolor='#ffffff' onclick='window.location = \"_DO:cmd=close_overlay&c_name=".$parent_component."&f_name=".$parent_component_file."?lane_number=".$i."\"'>" . $i . "</td>";
				}
				echo "</tr>";
				echo "</table>";
			}
			else
			{
				echo "Error: No Location Sent";
			}
		?>
    </td>
  </tr>
</table>
</body>


<?php

} else if($uripage =="select_track.php") {
//--------------------------------------------------- select_track.php ----------------------------------------------//
?>

<?php
	//require_once(dirname(__FILE__) . "/../comconnect.php");
	//ini_set("display_errors","1");
	if(reqvar("tab_name") == "Group Events") {
		$parent_component = "group_events";
		$parent_component_file = "lavukart/group_events.php";
	} else if(reqvar("tab_name") == "Pit") {
		$parent_component = "events_overlay_wrapper;events";
		$parent_component_file = "lavukart/events_overlay_wrapper.php;lavukart/events.php";
	} else {
		$parent_component = "events_wrapper;events";
		$parent_component_file = "lavukart/events_wrapper.php;lavukart/events.php";
	}
?>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<style type="text/css">
<!--

body, table {
	font-family:Verdana, Arial, Helvetica, sans-serif;
}

.caltext {
	font-family: "Trebuchet MS", Arial, Helvetica, sans-serifVerdana, Geneva, sans-serif;
	font-size: 16px;
	font-weight: bold;
	color: #154473;
}
-->
</style>
</head>

<body style='margin:0px'>
<table width="423" height="374" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="20" height="40">&nbsp;</td>
    <td width="120" align="center" valign="middle">&nbsp;</td>
    <td width="134" onClick="window.location = '_DO:cmd=close_overlay'">&nbsp;</td>
    <td width="120" align="center" valign="middle">&nbsp;</td>'
    <td width="21">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="5" align="center" valign="top">
		<?php
			$locid = reqvar("locid");
			if($locid)
			{
	        	echo "<span style='color:#ffffff'>Select Track</b>:</font>";
				echo "<br><br>";
				echo "<table cellspacing=4 cellpadding=12>";
				$track_query = lavu_query("select * from `lk_tracks` where `locationid`='[1]' order by `_order` asc",$loc_id);
				while($track_read = lysql_fetch_assoc($track_query))
				{
					echo "<tr><td style='border:solid 1px #777777; padding-left:40px; padding-right:40px' bgcolor='#ffffff' onclick='window.location = \"_DO:cmd=close_overlay&c_name=".$parent_component."&f_name=".$parent_component_file."?mode=set_track&set_trackid=".$track_read['id']."&set_tracktitle=" . $track_read['title'] . "\"'>" . $track_read['title'] . "</td></tr>";
				}
				echo "</table>";
			}
			else
			{
				echo "Error: No Location Sent";
			}
		?>
    </td>
  </tr>
</table>
</body>


<?php

} else if($uripage =="server.php") {
//--------------------------------------------------- server.php ----------------------------------------------//
?>

<?php
  	//require_once("../comconnect.php");
	
	function find_duration($time_end,$ts_end,$time_start,$ts_start,$set_pauses)
	{
		$duration = $time_end - $time_start;
		$pauses = trim($set_pauses); //----------------- subtract pauses
		if($pauses!="")
		{
			$pause_parts = explode(" ",$pauses);
			for($p=0; $p<count($pause_parts); $p++)
			{
				$pause_info = trim($pause_parts[$p]);
				$pause_info = explode("=",$pause_info);
				if(count($pause_info) > 1)
				{
					$pause_duration = $pause_info[1];
					$pause_time = explode("-",$pause_info[0]);
					if(count($pause_time)>1)
					{
						$pause_start = $pause_time[1];
						$pause_end = $pause_time[0];
						
						if($ts_end >= $pause_end && $ts_start <= $pause_start)
						{
							$duration -= $pause_duration;
						}
					}
				}
			}
		}
		return $duration;
	}
	
	if(isset($_GET['list_transponders']))
	{
		$eventid = $_GET['eventid'];
		$trackid = $_GET['trackid'];
		if($eventid=="current" || $eventid=="active")
		{
			$active_query = lavu_query("select * from `lk_events` where `ms_start`!='' and `ms_end`='' and `trackid`='[1]'",$trackid);
			if(lysql_num_rows($active_query))
			{
				$active_read = lysql_fetch_assoc($active_query);
				$eventid = $active_read['id'];
			}
		}
		$sched_query = lavu_query("select * from `lk_event_schedules` where `eventid`='[1]'",$eventid);
		while($sched_read = lysql_fetch_assoc($sched_query))
		{
			lavu_query("delete from `lk_laps` where `scheduleid`='[1]'",$sched_read['id']);
		}
		
		$kcount = 0;
		$kart_query = lavu_query("SELECT `lk_event_schedules`.`id` as `scheduleid`, `lk_karts`.`transponderid`, `lk_karts`.`number` FROM `lk_karts` LEFT JOIN `lk_event_schedules` ON `lk_karts`.`id`=`lk_event_schedules`.`kartid` LEFT JOIN `lk_events` ON `lk_event_schedules`.`eventid`=`lk_events`.`id` where `lk_events`.`id`='[1]'",$eventid);
		while($kart_read = lysql_fetch_assoc($kart_query))
		{
			$kcount++;
			echo "&kart".$kcount."=".$kart_read['transponderid']."&<br>";
		}
		echo "&kart_count=".$kcount."&";
	}
	else if(isset($_GET['transponderid']))
	{
		$transponderid = $_GET['transponderid'];
		$msts = $_GET['msts'];
		$setlap = 0;
		$last_lap = 0;
		$duration = 0;
		
		$response_code = "";
		$response = "";
		//lavu_select_db("poslavu_DEVKART_db");
		//$kart_query = lavu_query("select * from `lk_karts` LEFT JOIN `lk_event_schedules` ON `lk_event_schedules`.`kartid`=`lk_karts`.`id`
		$kart_query = lavu_query("SELECT `lk_event_schedules`.`id` as `scheduleid`, `lk_events`.`pauses` as `pauses`, `lk_events`.`id` as `eventid`, `lk_event_types`.`laps` as `laps` FROM `lk_karts` LEFT JOIN `lk_event_schedules` ON `lk_karts`.`id`=`lk_event_schedules`.`kartid` LEFT JOIN `lk_events` ON `lk_event_schedules`.`eventid`=`lk_events`.`id` LEFT JOIN `lk_event_types` ON `lk_event_types`.`id`=`lk_events`.`type` where `transponderid`='[1]' and `lk_events`.`ms_start`!='' and `lk_events`.`ms_end`=''",$transponderid);
		if(lysql_num_rows($kart_query))
		{
			while($kart_read = lysql_fetch_assoc($kart_query))
			{
				$reorder = false;
				$scheduleid = $kart_read['scheduleid'];
				$last_lap_query = lavu_query("select * from `lk_laps` where `scheduleid`='$scheduleid' order by `ts` desc limit 1");
				if(lysql_num_rows($last_lap_query))
				{
					$last_lap_read = lysql_fetch_assoc($last_lap_query);
					$setlap = $last_lap_read['pass'] + 1;
					$last_lap = $last_lap_read['ms_this_lap'];
					
					$duration = find_duration($msts,time(),$last_lap,$last_lap_read['ts'],$kart_read['pauses']);
					//$duration = $msts - $last_lap;				
					if($duration < 0)
					{
						$reorder = true;
						$duration = 0;
					}
					//if($duration < -43200) $duration = $duration * 1 + 86400;
					//else if($duration < 0) $duration = $duration * 1 + 4294.967295; 
				}
				else
				{
					$setlap = 0;
					$last_lap = 0;
					$duration = 0;
				}
								
				$vars = array();
				$vars['scheduleid'] = $scheduleid;
				$vars['ms_this_lap'] = $msts;
				$vars['ms_last_lap'] = $last_lap;
				$vars['ms_duration'] = $duration;
				$vars['pass'] = $setlap;
				$vars['ts'] = time();//date("YmdHis");
				$dup_query = lavu_query("select * from `lk_laps` where `scheduleid`='[scheduleid]' and `ms_this_lap`='[ms_this_lap]'",$vars);
				if(lysql_num_rows($dup_query) < 1)
				{
					if($duration > 10 || $setlap <=0)
					{
						lavu_query("insert into `lk_laps` (`scheduleid`,`ms_this_lap`,`ms_last_lap`,`ms_duration`,`pass`,`ts`) values ('[scheduleid]','[ms_this_lap]','[ms_last_lap]','[ms_duration]','[pass]','[ts]')",$vars);
						
						if($setlap==$kart_read['laps'])
						{
							$minlap_completed = $setlap;
							//$fin_query = lavu_query("select distinct `scheduleid` from `lk_laps` where `pass`='[1]'",$setlap);
							$sch_query = lavu_query("select * from `lk_event_schedules` where `eventid`='[1]'",$kart_read['eventid']);
							while($sch_read = lysql_fetch_assoc($sch_query))
							{
								$slp_query = lavu_query("select * from `lk_laps` where `scheduleid`='[1]' order by (`pass` * 1) desc limit 1",$sch_read['id']);
								if(lysql_num_rows($slp_query))
								{
									$slp_read = lysql_fetch_assoc($slp_query);
									$slp_pass = $slp_read['pass'];
								}
								else
								{
									$slp_pass = 0;
								}
								if($slp_pass < $minlap_completed)
									$minlap_completed = $slp_pass;
							}
							if($minlap_completed==$setlap)
							{
								$cls_query = lavu_query("select * from `lk_events` where `id`='[1]'",$kart_read['eventide']);
								if(lysql_num_rows($cls_query))
								{
									$cls_read = lysql_fetch_assoc($cls_query);
									mail("cp_notifications@lavu.com","final laps","final laps: pass " . $setlap,"From: finallaps@poslavu.com");
								}
							}
							//else mail("corey@meyerwind.com","min laps completed: $minlap_completed","final laps: pass " . $setlap,"From: finallaps@poslavu.com");
						}
						
						if(!$reorder)
						{
							$response_code = "passed";
							$response = "passed lap $setlap";
						}
						else
						{
							$response_code = "passed - reordering";
							$response = "passed lap $setlap - reordering";
							
							$last_lap_ms = 0;
							$last_lap_ts = 0;
							$lap_num = 0;
							$lap_query = lavu_query("select * from `lk_laps` where `scheduleid`='[scheduleid]' order by `ms_this_lap` asc",$vars);
							while($lap_read = lysql_fetch_assoc($lap_query))
							{
								$lap_ms = $lap_read['ms_this_lap'];
								$lap_ts = $lap_read['ts'];
								if($last_lap_ms==0) $duration = 0;
								else 
								{
									$duration = find_duration($lap_ms,$lap_ts,$last_lap_ms,$last_lap_ts,$kart_read['pauses']);
									//$duration = $lap_ms - $last_lap_ms;								
									//$duration = subtract_pauses($duration,time(),$kart_read['pauses']);
								}
								
								$uvars = array();
								$uvars['ms_last_lap'] = $last_lap_ms;
								$uvars['ms_duration'] = $duration;
								$uvars['pass'] = $lap_num;
								$uvars['lapid'] = $lap_read['id'];
								lavu_query("update `lk_laps` set `ms_last_lap`='[ms_last_lap]', `ms_duration`='[ms_duration]', `pass`='[pass]' where id='[lapid]'",$uvars);
								//mail("corey@meyerwind.com","success: $duration","success","From: laps@poslavu.com");
								
								$lap_num++;
								$last_lap_ms = $lap_ms;
								$last_lap_ts = $lap_ts;
							}
						}
					}
					else
					{
						$response_code = "short";
						$response = "too short for lap $setlap";
					}
				}
				else
				{
					$response_code = "duplicate";
					$response = "received duplicate for lap $setlap";
				}
				//echo "insert into `lk_laps` (`scheduleid`,`ms_this_lap`,`ms_last_lap`,`ms_duration`) values ('[scheduleid]','[ms_this_lap]','[ms_last_lap]','[ms_duration]')";
				//echo "<br>" . $scheduleid;
				//echo "<br>" . $msts;
				//echo "<br>" . $last_lap;
				//echo "<br>" . $duration;
			}
		}
		else
		{
			$response_code = "not_scheduled";
			$response = "transponder not scheduled";
			//echo "SELECT `lk_event_schedules`.`id` as `scheduleid` FROM `lk_karts` LEFT JOIN `lk_event_schedules` ON `lk_karts`.`id`=`lk_event_schedules`.`kartid` LEFT JOIN `lk_events` ON `lk_event_schedules`.`eventid`=`lk_events`.`id` where `eventid`='115' and `transponderid`='$transponderid' and `lk_events`.`ms_start`!='' and `lk_events`.`ms_end`=''";
		}
		
		//mail("corey@poslavu.com","Transponder $transponderid","Pass: $msts\n"."1|".$response_code."|".$response."|".$transponderid."|".$msts."|lap:".$setlap."|duration:".$duration,"From:info@polepositionraceway.com");
		echo "1|".$response_code."|".$response."|".$transponderid."|".$msts."|lap:".$setlap."|duration:".$duration;
		exit();
	}
?>

<?php

} else if($uripage =="test_component.php") {
//--------------------------------------------------- test_component.php ----------------------------------------------//
?>

<html>
<body>
<table width='490px' cellpadding='10'>
<tr><td align='center'><b>Test Component</b></td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td align='center'>Racers Right</td></tr>
<tr><td>&nbsp;</td></tr>
<tr>
	<td align='left' bgcolor='#FFFFFF'>
		<font color='#0000FF'><b>RECEIVED DATA:</b><br>
		<?php
			echo "<br>component name: ".$_POST['comp_name'];
			echo "<br>company code: ".$_POST['cc'];
			echo "<br>data name: ".$_POST['dn'];
			echo "<br>location id: ".$_POST['loc_id'];
			echo "<br>server id: ".$_POST['server_id'];
			echo "<br>server name: ".$_POST['server_name'];
			echo "<br>tab name: ".$_POST['tab_name'];
			echo "<br><br>test1: ".$_POST['test1'];
			echo "<br>test2: ".$_POST['test2'];
		?>
	</td>
</tr>
</table>
</body>
</html>
<?php

} else if($uripage =="test_component2.php") {
//--------------------------------------------------- test_component2.php ----------------------------------------------//
?>

<html>
<body>
<table width='490px' cellpadding='10'>
<tr><td align='center'><b>Test Component 2</b></td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td align='center'>Pit</td></tr>
<tr><td>&nbsp;</td></tr>
<tr>
	<td align='left' bgcolor='#FFFFFF'>
		<font color='#0000FF'><b>RECEIVED DATA:</b><br>
		<?php
			echo "<br>component name: ".$_POST['comp_name'];
			echo "<br>company code: ".$_POST['cc'];
			echo "<br>data name: ".$_POST['dn'];
			echo "<br>location id: ".$_POST['loc_id'];
			echo "<br>server id: ".$_POST['server_id'];
			echo "<br>server name: ".$_POST['server_name'];
		?>
	</td>
</tr>
</table>
</body>
</html>
<?php

} }

exit();
?>
