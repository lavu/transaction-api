<style>
body, table {
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size:12px;
}
.titles {
	font-size:14px;
}
</style>

<table align="center" style="table-layout:fixed">
	<tr>
		<td colspan='4' align='center'>
			<table>
				<td class="titles">
					<nobr>Name: [fullname]</nobr>
					<br><nobr>Racer Name: [racer name]</nobr>
				</td>
				<td style="width:120px" class="titles">&nbsp;&nbsp;&nbsp;&nbsp;</td>
				<td>
					<nobr>Placed [placed]</nobr>
					<br><nobr>Date: [race date]</nobr>
				</td>
			</table>
			<hr />
		</td>
		<td>
	</tr>
	<tr valign="top">
		<td valign="top" style="width:120px">		
			<nobr><b>Race Results</b></nobr>
			<br>[all best times]			
		</td>
		<td>&nbsp;&nbsp;</td>
		<td style="width:120px">
			<b><nobr>Your Laps</nobr></b>
			<br>[lap times]
		</td>
		<td valign="top">
			<img src="[chart]" />
		</td>
	</tr>
    <tr><td colspan="3" height="460"></td></tr>
</table>