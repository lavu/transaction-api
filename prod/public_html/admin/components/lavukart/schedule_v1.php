<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Untitled Document</title>

		<style type="text/css">
<!--
body {
	background-color: transparent;
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}

.creditnumber {font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #3d4f6d;
}
.nametext {font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
}
.active_nametext {font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #00aa00;
}
.finished_nametext {font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color:#999999;
}
.subtitles {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #5A75A0;
}
.style411 {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #5A75A0;
}
.gridposition {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #5A75A0;
}
.icontext {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 11px;
	font-weight: bold;
	color: #5A75A0;
}
.subtitles1 {font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-weight: bold;
	color: #5A75A0;
}
.title1 {	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 18px;
	font-weight: bold;
	color: #5A75A0;
}
-->
		</style>
		<script language="javascript">
			function findTopPos(objid) 
			{
				obj = document.getElementById(objid);
				var curleft = curtop = 0;
				if (obj.offsetParent) 
				{
					do 
					{
						curleft += obj.offsetLeft;
						curtop += obj.offsetTop;
					} while (obj = obj.offsetParent);
					return curtop;
				}
				else return 0;
			}			
		</script>
	</head>

<?php
	require_once(dirname(__FILE__) . "/../comconnect.php");
	//ini_set("display_errors","1");
	/*if(reqvar("eventid")) $eventid = reqvar("eventid");
	else if(sessvar_isset("eventid")) $eventid = sessvar("eventid");
	set_sessvar("eventid",$eventid);*/
	$eventid = sessvar("eventid");
	$row_selected = 0;
	
	$set_scrolltop = reqvar("set_scrolltop");
	$mode = reqvar("mode");
	if($mode=="add_event")
	{
		$set_date = reqvar("event_date");
		$t = reqvar("event_time");
		$set_type = reqvar("event_type");
		$set_locationid = reqvar("event_locationid");
		$set_trackid = reqvar("event_trackid");
		
		$dparts = explode("-",$set_date);
		$set_year = $dparts[0];
		$set_month = $dparts[1];
		$set_day = $dparts[2];
		$set_min = $t % 100;
		$set_hour = (($t - $set_min) / 100);
		$set_time = $t;
		$set_ts = mktime($set_hour,$set_min,0,$set_month,$set_day,$set_year);

		lavu_query("insert into `lk_events` (`locationid`,`type`,`date`,`time`,`ts`,`trackid`) values ('$set_locationid','$set_type','$set_date','$set_time','$set_ts','$set_trackid')");
		//echo "insert into `lk_events` (`locationid`,`type`,`date`,`time`,`ts`,`trackid`) values ('$set_locationid','$set_type','$set_date','$set_time','$set_ts','$set_trackid')";
	}
	else if($mode=="move_event")
	{
		$mtvars = array();
		$mtvars['date'] = reqvar("moveto_date");
		$mtvars['time'] = reqvar("moveto_slot");
		$mtvars['id'] = $eventid;//reqvar("eventid");

		$t = $mtvars['time'];
		$dparts = explode("-",$mtvars['date']);
		$set_year = $dparts[0];
		$set_month = $dparts[1];
		$set_day = $dparts[2];
		$set_min = $t % 100;
		$set_hour = (($t - $set_min) / 100);
		$set_time = $t;
		$set_ts = mktime($set_hour * 1,$set_min * 1,0,$set_month * 1,$set_day * 1,$set_year * 1);
		$mtvars['ts'] = $set_ts;
		
		$trackid = sessvar("trackid");
		$mtvars['trackid'] = $trackid;
		$track_query = lavu_query("select * from `lk_tracks` where id='[1]'",$trackid);
		if(mysqli_num_rows($track_query))
		{
			$track_read = mysqli_fetch_assoc($track_query);
			$time_span = $track_read['time_interval'];
			
			$move_events_queue = array();
			$slot_clear = false;
			$ctime = $mtvars['time'];
			$cdate = $mtvars['date'];
			
			$set_min = $ctime % 100;
			$set_hour = (($ctime - $set_min) / 100);
			$cts = mktime($set_hour * 1,$set_min * 1,0,$set_month * 1,$set_day * 1,$set_year * 1);
			$loopcount = 0;
						
			while($slot_clear==false && $loopcount < 300)
			{			
				$e_query = lavu_query("select * from `lk_events` where `trackid`='[1]' and `ts`='[2]' and `id`!='[3]'",$trackid,$cts,$eventid);
				if(mysqli_num_rows($e_query))
				{
					$ctime = time_advance($ctime,$time_span);
					$set_min = $ctime % 100;
					$set_hour = (($ctime - $set_min) / 100);
					$cts = mktime($set_hour * 1,$set_min * 1,0,$set_month * 1,$set_day * 1,$set_year * 1);
					while($e_read = mysqli_fetch_assoc($e_query))
					{
						$move_events_queue[] = array($e_read['id'],$cdate,$ctime,$cts);
					}
				}
				else
				{
					$slot_clear = true;
				}
				$loopcount++;
			}
			for($n=0; $n<count($move_events_queue); $n++)
			{
				$cvars['id'] = $move_events_queue[$n][0];
				$cvars['date'] = $move_events_queue[$n][1];
				$cvars['time'] = $move_events_queue[$n][2];
				$cvars['ts'] = $move_events_queue[$n][3];
				//$cvars['trackid'] = 
				//mail("corey@poslavu.com","forward: " . $move_events_queue[$n][0],"date: " . $move_events_queue[$n][1] . " time: " . $move_events_queue[$n][2] . " ts: " . $move_events_queue[$n][3],"From: info@poslavu.com");
				lavu_query("update `lk_events` set `date`='[date]', `time`='[time]', `ts`='[ts]' where `id`='[id]",$cvars);
			}
			
			lavu_query("update `lk_events` set `date`='[date]', `time`='[time]', `ts`='[ts]', `trackid`='[trackid]' where `id`='[id]'",$mtvars);
		}
	}
	else if($mode=="remove_event")
	{
		$event_query = lavu_query("select * from `lk_events` where id='[1]'",$eventid);
		if(mysqli_num_rows($event_query))
		{
			$event_read = mysqli_fetch_assoc($event_query);
			$date_parts = explode("-",$event_read['date']);
			if(count($date_parts) > 2)
			{
				$year = $date_parts[0];
				$month = $date_parts[1];
				$day = $date_parts[2];
			}
			lavu_query("delete from `lk_events` where `id`='[1]'",$eventid);
		}
	}
	else if($mode=="set_track")
	{
		$trackid = reqvar("set_trackid");
		set_sessvar("trackid",$trackid);
	}
	
	if(sessvar_isset("trackid"))
		$trackid = sessvar("trackid");
	else
		$trackid = false;
	$anchor_now = "page_body";
?>

	<body id="page_body" onLoad="body_loaded()">
		<table width="395" height="535" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td height="535" align="left" valign="top">
					<table border="0" cellspacing="0" cellpadding="0">
             <?php
							$minrows = 10;
							$rowcount = 0;
							
							if(postvar("day")==false && $eventid)
							{
								$event_query = lavu_query("select * from `lk_events` where id='[1]'",$eventid);
								if(mysqli_num_rows($event_query))
								{
									$event_read = mysqli_fetch_assoc($event_query);
									$date_parts = explode("-",$event_read['date']);
									if(count($date_parts) > 2)
									{
										$year = $date_parts[0];
										$month = $date_parts[1];
										$day = $date_parts[2];
									}
								}
								else
								{
									$year = postvar("year",date("Y"));
									$month = postvar("month",date("m"));
									$day = postvar("day",date("d"));
								}
							}
							else
							{
								$year = postvar("year",date("Y"));
								$month = postvar("month",date("m"));
								$day = postvar("day",date("d"));
							}
							if($month < 10) $month = "0" . ($month * 1);
							if($day < 10) $day = "0" . ($day * 1);
							$fulldate = $year . "-" . $month . "-" . $day;
							//echo $fulldate;

							function create_timeslot_row($date,$slot,$trackid,$locationid,$eventid=false,$contents=false,$rowcount=0)
							{
								?>
                                <tr>
                                    <td height="46" id="timeslot_<?php echo $slot?>"><table width="79" border="0" cellspacing="0" cellpadding="8">
                                        <tr>
                                            <td class="subtitles1"><nobr>
											<?php
												if($eventid)
												{
													echo "<a onclick='if(confirm(\"Are you sure you want to move this event?\")) location = \"_DO:cmd=load_url&c_name=schedule&f_name=lavukart/schedule.php?mode=move_event&eventid=".$eventid."&moveto_date=".$date."&moveto_slot=".$slot."&set_scrolltop=\" + window.pageYOffset + \"&parentcom=schedule&abc=123\"; '>".display_time($slot)."</a>"; 
												}
												else
												{
													echo display_time($slot);
												}
											?>
											</nobr></td>
                                        </tr>
                                    </table></td>
                                    <?php
										for($i=0; $i<count($contents); $i++)
										{
											if($contents[$i]=="")
											{
											?>
                                            <td width="51" align="left" valign="middle" background="images/tab_bg_51.png" onClick="window.location = '_DO:cmd=create_overlay&f_name=lavukart/newevent_wrapper.php;lavukart/newevent.php&c_name=newevent_wrapper;newevent&scroll=0;1&dims=112,200,423,384;112,242,423,327?date=<?php echo $date; ?>&time=<?php echo $slot; ?>&trackid=<?php echo $trackid; ?>&locid=<?php echo $locationid; ?>&parentcom=schedule&scrolltop=' + window.pageYOffset"><table width="51" border="0" cellspacing="0" cellpadding="8">
                                                <tr>
                                                    <td class="nametext" align="center" valign="middle">&nbsp;
                                                    	
                                                    </td>
                                                </tr>
                                            </table></td>
                                            <?php
											}
											else if(is_array($contents[$i]) && count($contents[$i])==2)
											{
												$tab_part = $contents[$i][0];
												
												$tab_image = "tab_bg_51.png";
												if($tab_part=="T") $tab_image = "tab_bg_51_top.png";
												else if($tab_part=="M") $tab_image = "tab_bg_51_mid.png";
												else if($tab_part=="B") $tab_image = "tab_bg_51_bot.png";
												
												?>
												<td width="51" align="left" valign="middle" background="images/<?php echo $tab_image; ?>" onClick="window.location = '_DO:cmd=create_overlay&f_name=lavukart/newevent_wrapper.php;lavukart/newevent.php&c_name=newevent_wrapper;newevent&scroll=0;1&dims=112,200,423,384;112,242,423,327?date=<?php echo $date; ?>&time=<?php echo $slot; ?>&trackid=<?php echo $trackid; ?>&locid=<?php echo $locationid; ?>&parentcom=schedule&scrolltop=' + window.pageYOffset"><table width="51" border="0" cellspacing="0" cellpadding="8">
													<tr>
														<td class="nametext" align="center" valign="middle">
															<?php echo "&nbsp;"; ?>
														</td>
													</tr>
												</table></td>
												<?php
											}
											else
											{
												$event_read = $contents[$i];
												
												$start_style = "";
												$use_bg = "images/tab_bg_51.png";
												if($eventid==$event_read['id'])
												{
													$use_bg = "images/tab_bg_51_lit.png";
													$row_selected = $rowcount;
													$timeslot_selected = $event_read['time'];
												}
		
												if ($_POST['tab_name'] == "Schedule" || $_POST['tab_name'] == "New Schedule") {
													$comp_cmd = "location = \"_DO:cmd=load_url&c_name=customers&f_name=lavukart/customers.php?mode=schedule&eventid=".$event_read['id']."&abc=123\";";
												} else if ($_POST['tab_name'] == "Pit") {
													$comp_cmd = "location = \"_DO:cmd=close_overlay&c_name=car_assignments_wrapper;car_assignments;racers_for_this_race&f_name=lavukart/car_assignments_wrapper.php;lavukart/car_assignments.php;lavukart/racers_for_this_race.php?eventid=".$event_read['id']."&abc=123\";";
												} else {
													$comp_cmd = "";
												}
												$tdclass = "nametext";
												if($event_read['ms_start']!="" && $event_read['ms_end']=="")
												{
													$tdclass = "active_nametext";
												}
												else if($event_read['ms_end']!="")
												{
													$tdclass = "finished_nametext";
												}
												?>
												<td width="51" id="event_row_<?php echo $rowcount?>" style="background: URL(<?php echo $use_bg?>);" onclick='if(row_selected) document.getElementById("event_row_" + row_selected).style.background = "URL(images/tab_bg_51.png)"; row_selected = "<?php echo $rowcount?>"; this.style.background = "URL(images/tab_bg_51_lit.png)";<?php echo $comp_cmd; ?>'>
													<table width="51" border="0" cellspacing="0" cellpadding="8">
														<tr><td class="<?php echo $tdclass;?>"><?php echo ucfirst(substr($event_read['title'],0,3)); ?></td></tr>
													</table>
												</td>
                                            	<?php
											}
										}
										?>
                                </tr>
                                <?php
							}
							
							function time_advance($time, $add)
							{
								$time_hours = $time - ($time % 100);
								$time_minutes = $time % 100;
								$add_hours = $add - ($add % 100);
								$add_minutes = $add % 100;
								$hours = $time_hours + $add_hours;
								$minutes = $time_minutes + $add_minutes;
								if($minutes >= 60)
								{
									$hours += 100;
									$minutes -= 60;
								}
								return $hours + $minutes;
							}
							
							$locationid = reqvar("loc_id");
							$closest_time = 0;
							$timeslot_selected = 0;
							
							/*if($trackid)
							{
								$track_query = lavu_query("select * from `lk_tracks` where id='[1]' and `locationid`='[2]'",$trackid,$locationid);
								if(!mysqli_num_rows($track_query))
									$trackid = false;
								set_sessvar("trackid",$trackid);
							}
							
							if(!$trackid)
							{*/
								$track_query = lavu_query("select * from `lk_tracks` where `locationid`='$locationid' and `_deleted`!='1' order by id asc");
							//}
							if(!mysqli_num_rows($track_query))
							{
								echo "Error: No Tracks exist for this location";
								exit();
							}
							
							$track_title = "All";							
							$show_multiple_schedules = true;
							if($show_multiple_schedules)
							{
								$time_start = "";
								$time_end = "";
								$time_span = "";
								$sched_info = array();
								$track_list = array();
								while($track_read = mysqli_fetch_assoc($track_query))
								{
									if($time_start=="" || $track_read['time_start'] < $time_start) $time_start = $track_read['time_start'];
									if($time_end=="" || $track_read['time_end'] > $time_end) $time_end = $track_read['time_end'];
									if($time_span=="" || $track_read['time_interval'] < $time_span) $time_span = $track_read['time_interval'];
									
									$trackid = $track_read['id'];
									$track_list[] = $trackid;
									//echo "found track: $trackid <br>";
									//$track_title = $track_read['title'];
								}
								if(!is_numeric($time_start)) $time_start = 1100;
								if(!is_numeric($time_end)) $time_end = 2200;

								$mints = mktime(0,0,0,$month*1,$day*1,$year*1);
								$maxts = mktime(24,0,0,$month*1,$day*1,$year*1);
								
								$event_query = lavu_query("select `lk_events`.`trackid` as `trackid`, `lk_events`.`ts` as `ts`, `lk_events`.`time` as time, `lk_events`.`id` as id, `lk_events`.`ms_start` as `ms_start`, `lk_events`.`ms_end` as `ms_end`, `lk_event_types`.`title` as `title` from `lk_events` left join `lk_event_types` on lk_events.type = lk_event_types.id where `ts`>='$mints' and `ts`<='$maxts' order by `ts` asc");
								while($event_read = mysqli_fetch_assoc($event_query))
								{
									$event_trackid = $event_read['trackid'];
									$event_time = $event_read['time'];
									if(!isset($sched_info[$event_trackid])) $sched_info[$event_trackid] = array();
									
									//echo $event_trackid . "-" . $event_time . "<br>";
									$sched_info[$event_trackid][$event_time] = $event_read;
								}
								$sched_info['room'] = array();
								$sched_info['room'][$time_start + $time_span * 0] = array("T",0);
								$sched_info['room'][$time_start + $time_span * 1] = array("M",0);
								$sched_info['room'][$time_start + $time_span * 2] = array("T",0);
								$sched_info['room'][$time_start + $time_span * 3] = array("T",0);
								$sched_info['room'][$time_start + $time_span * 4] = array("M",0);
								$sched_info['room'][$time_start + $time_span * 5] = array("M",0);
								$sched_info['room'][$time_start + $time_span * 6] = array("M",0);
								$sched_info['room'][$time_start + $time_span * 7] = array("B",0);
								
								$current_time = $time_start;
								while($current_time < $time_end)
								{
									$rowcount++;
									if($current_time >= date("Hi",$mints))
									{
										$row_info = array();
										for($t=0; $t<count($track_list); $t++)
										{
											$tid = $track_list[$t];
											if(isset($sched_info[$tid][$current_time]))
											{
												$slot_info = $sched_info[$tid][$current_time];
												$row_info[] = $slot_info;
											}
											else $row_info[] = "";
										}
										if(isset($sched_info['room'][$current_time]))
										{
											$slot_info = $sched_info['room'][$current_time];
											$row_info[] = $slot_info;
										}
										create_timeslot_row($fulldate,$current_time,$trackid,$locationid,$eventid,$row_info,$rowcount);
									}
									$current_time = time_advance($current_time,$time_span);
								}
								$rowcount++;
							}
							else
							{
								$track_read = mysqli_fetch_assoc($track_query);
								$time_start = $track_read['time_start'];
								$time_end = $track_read['time_end'];
								$time_span = $track_read['time_interval'];
								$trackid = $track_read['id'];
								$track_title = $track_read['title'];
								set_sessvar("trackid",$trackid);
								
								if(!is_numeric($time_start)) $time_start = 1100;
								if(!is_numeric($time_end)) $time_end = 2200;
															
								//echo "<tr><td><td align='center' class='subtitles1'><b>$month/$day/$year</b></td></tr>";
								/*if($year==date("Y") && $month==date("m") && $day==date("d"))
								{
									$mints = current_ts(-60); //mktime($hour - 1,$minute,0,$month,$day,$year);
								}
								else*/
									$mints = mktime(0,0,0,$month*1,$day*1,$year*1);
								$maxts = mktime(24,0,0,$month*1,$day*1,$year*1);
								
								$current_time = $time_start;
						
								$event_query = lavu_query("select `lk_events`.`ts` as `ts`, `lk_events`.`time` as time, `lk_events`.`id` as id, `lk_events`.`ms_start` as `ms_start`, `lk_events`.`ms_end` as `ms_end`, `lk_event_types`.`title` as `title` from `lk_events` left join `lk_event_types` on lk_events.type = lk_event_types.id where `trackid`='[1]' and `ts`>='$mints' and `ts`<='$maxts' order by `ts` asc",$trackid);
								while($event_read = mysqli_fetch_assoc($event_query))
								{
									
									while($current_time < $event_read['time'])
									{
										$rowcount++;
										if($current_time >= date("Hi",$mints))
										{
											create_timeslot_row($fulldate,$current_time,$trackid,$locationid,$eventid);
											if(abs($current_time - date("Hi")) < abs($current_time - $closest_time))
												$closest_time = $current_time;
										}
										$current_time = time_advance($current_time,$time_span);
									}
									if($current_time < time_advance($event_read['time'],$time_span))
										$current_time = time_advance($current_time,$time_span);
									$rowcount++;
									
									if(abs($event_read['time'] - date("Hi")) < abs($event_read['time'] - $closest_time))
										$closest_time = $event_read['time'];
									?>
									<tr>
										<td width="69" height="46" id="timeslot_<?php echo $event_read['time'];?>">
											<table width="69" border="0" cellspacing="0" cellpadding="8">
												<tr><td class="subtitles1"><nobr>
												<?php
													/*if($eventid)
													{
														echo "<a onclick='if(confirm(\"Are you sure you want to move this event?\")) location = \"_DO:cmd=load_url&c_name=events&f_name=lavukart/events.php?mode=move_event&eventid=".$eventid."&moveto_date=".$fulldate."&moveto_slot=".$event_read['time']."&abc=123\"; '>".display_time($event_read['time'])."</a>"; 
													}
													else
													{ */
														echo display_time($event_read['time']); 
													//}
												?>
												</nobr></td></tr>
											</table>
										</td>
										<?php
											$start_style = "";
											$use_bg = "images/tab_bg_51.png";
											if($eventid==$event_read['id'])
											{
												$use_bg = "images/tab_bg_51_lit.png";
												$row_selected = $rowcount;
												$timeslot_selected = $event_read['time'];
											}
	
											if ($_POST['tab_name'] == "Schedule" || $_POST['tab_name'] == "New Schedule") {
												$comp_cmd = "location = \"_DO:cmd=load_url&c_name=customers&f_name=lavukart/customers.php?mode=schedule&eventid=".$event_read['id']."&abc=123\";";
											} else if ($_POST['tab_name'] == "Pit") {
												$comp_cmd = "location = \"_DO:cmd=close_overlay&c_name=car_assignments_wrapper;car_assignments;racers_for_this_race&f_name=lavukart/car_assignments_wrapper.php;lavukart/car_assignments.php;lavukart/racers_for_this_race.php?eventid=".$event_read['id']."&abc=123\";";
											} else {
												$comp_cmd = "";
											}
											$tdclass = "nametext";
											if($event_read['ms_start']!="" && $event_read['ms_end']=="")
											{
												$tdclass = "active_nametext";
											}
											else if($event_read['ms_end']!="")
											{
												$tdclass = "finished_nametext";
											}
										?>
										<td width="51" id="event_row_<?php echo $rowcount?>" style="background: URL(<?php echo $use_bg?>);" onclick='if(row_selected) document.getElementById("event_row_" + row_selected).style.background = "URL(images/tab_bg_51.png)"; row_selected = "<?php echo $rowcount?>"; this.style.background = "URL(images/tab_bg_51_lit.png)";<?php echo $comp_cmd; ?>'>
											<table width="51" border="0" cellspacing="0" cellpadding="8">
												<tr><td class="<?php echo $tdclass;?>"><?php echo ucfirst(substr($event_read['title'],0,3)); ?></td></tr>
											</table>
										</td>
									</tr>
									<?php
								}
								while($current_time < $time_end)
								{
									$rowcount++;
									if($current_time >= date("Hi",$mints))
										create_timeslot_row($fulldate,$current_time,$trackid,$locationid,$eventid);
									$current_time = time_advance($current_time,$time_span);
								}
								$rowcount++;
							}
							?>
					</table>
					<script language='javascript'>row_selected = <?php echo $row_selected?>;</script>
				</td>
			</tr>
		</table>
		<script language="javascript">
			function body_loaded() {
				<?php 
					if($timeslot_selected > 0) {
						$anchor_now = "timeslot_".$timeslot_selected;
					}
					else if($closest_time > 0) {
						$anchor_now = "timeslot_".$closest_time;
					}
					if(!$set_scrolltop || $set_scrolltop=="")
					{
						$set_scrolltop = "(findTopPos('$anchor_now') - 40)";
					}
					//if(strpos($company_code,"DEV")!==false) {echo "alert(\"pos: \" + $set_scrolltop); ";}
					echo "window.scrollTo(0,$set_scrolltop); ";
				?>
				document.getElementById("page_body").style.visibility = "visible";
			}
		</script>
	</body>
</html>