<?php
				// this form is used in service_types_info.php and service_types_addServiceType.php, but they call different processing scripts
				//INCLUDE 'service_formInclude_2.php';
				
				
//echo "<h3>Add service to kart</h3>";				

			$serviceId = $_POST['id'];
			$_SESSION['serviceID'] = $serviceId;
			
			$locationid = sessvar("loc_id");
			
			$results = lavu_query("SELECT * FROM lk_service WHERE id=\"[1]\" AND locationid=\"[2]\"", $serviceId, $locationid);
				
			$rows = mysqli_fetch_assoc($results);	
						
							
							
							
							$submittedBy  = $rows['submitted_by'];//which employee entered this, they may be questioned later about details
							$servicedBy  = $rows['serviced_by'];//who performed the service
							$description = $rows['description'];
							$notes = $rows['notes'];//optional notes
							$manHours = $rows['man_hours'];//man hours from lk_service_types table
						
	
	echo "<div class=\"leftForm\"><label>Kart &#35;</label></div>";
					
						$kartID = $rows['kart_id'];
						$resultsGetKartNumber = lavu_query("SELECT number FROM lk_karts WHERE id=\"[1]\" AND locationid=\"[2]\"", $kartID, $locationid);
						$kartNumberRows = mysqli_fetch_assoc($resultsGetKartNumber);
						$kartNumber = $kartNumberRows['number'];
	
	echo "<div class=\"rightForm\"><select name=\"KartID\"><option value=\"0\"></option>";
						
					
						$kartResults = lavu_query("SELECT * FROM lk_karts WHERE locationid=\"[1]\" ORDER BY number*1", $locationid);
				
						while ($rowKarts = mysqli_fetch_assoc($kartResults)){
								
								$curKartID = $rowKarts['id'];
								$curKartNumber = $rowKarts['number'];
								
								if ($curKartID == $kartID){
											echo "<option value=\"$curKartID\" selected=\"selected\">$curKartNumber</option>";
								}//if
								
								if ($curKartID != $kartID){
											echo "<option value=\"$curKartID\">$curKartNumber</option>";
								}//if
								
						}//while
					
				
	echo "</select></div>";
	
	echo "<div style=\"clear:both;\">&nbsp;</div>
				<div style=\"clear:both;\">&nbsp;</div>
	
	
				<div class=\"leftForm\"><label>Service Type</label></div>";
	
	
	//get the service_type id for this service
	$serviceTypeString = $rows['service_type'];
	$serviceArray = explode("-",$serviceTypeString);
	$serviceID = $serviceArray[0];
	
	
	
	echo "<div class=\"rightForm\"><select name=\"serviceTypeID\" style=\"width:300px;\"><option value=\"0\"></option>";
						
			
						
	$locationid = sessvar("loc_id");
	$serviceTypesList = lavu_query("SELECT * FROM lk_service_types WHERE locationid=\"[1]\" ORDER BY name", $locationid);
				
					
					
	
	while ($rowServiceTypesList = mysqli_fetch_assoc($serviceTypesList)){
								
				$serviceTypeID = $rowServiceTypesList['id'];
				$serviceTypeName = $rowServiceTypesList['name'];
								
				
								
				if ($serviceID != $serviceTypeID){
							echo "<option value=\"$serviceTypeID\">$serviceTypeName</option>";
				}//if
								
				if ($serviceID == $serviceTypeID){
							echo "<option value=\"$serviceTypeID\" selected=\"selected\">$serviceTypeName</option>";
				}//if
								
	}//while
					
				
	echo "</select></div>";
				
	echo "<div style=\"clear:both;\"></div><br />";
				
				
				
			
				
	
								
								
								
								
								
	
				
				
				
				
				
	
	
	
	
	echo "<div class=\"leftForm\"><label>Notes</label></div>";
	
				echo "<div class=\"rightForm\"><textarea name=\"notes\" cols=\"33\" rows=\"10\">$notes</textarea></div>";
	
	echo "<div style=\"clear:both;\">&nbsp;</div>";
	
	
	
	
	
	
		
	echo "<div class=\"leftForm\">
				<label>Date Due</label>
				</div>
	";
	
										
	$dateTimeStr = $rows['date_due'];
	$dateYMDArray = explode(" ", $dateTimeStr);
	$dateArray = $dateYMDArray[0]; //this is the date yyyy/mm/dd. $datePieces[1] is the time of day it was submitted
	$dateYMDPieces = explode("-", $dateArray);
	$yearAssigned = $dateYMDPieces[0];
	$monthAssigned = $dateYMDPieces[1];
	$dayAssigned = $dateYMDPieces[2];
	
	echo "<div class=\"rightForm\" style=\"margin-left:20px;\">";
				
	echo "<select name=\"dateDueMonth\">
							<option value=\"0\"></option>";
										
							for ($month=1; $month<=12; $month++){
										
										if ($monthAssigned == $month){
													echo "<option value=\"$month\" selected=\"selected\">$month</option>";
										}//if
										
										if ($monthAssigned != $month){
													echo "<option value=\"$month\">$month</option>";
										}//if
										
							}//for
					
				
	echo "</select>";
	
				
				
	echo "<select name=\"dateDueDay\">
							<option value=\"0\"></option>";
										
							for ($day=1; $day<=31; $day++){
										
										if ($dayAssigned == $day){
													echo "<option value=\"$day\" selected=\"selected\">$day</option>";
										}//if
										
										if ($dayAssigned != $day){
													echo "<option value=\"$day\">$day</option>";
										}//if
							}//for
					
				
	echo "</select>";
				
				
				
				
	echo "<select name=\"dateDueYear\">
							<option value=\"0\"></option>";
										
							for ($year=2011; $year<=2015; $year++){
								
										if ($yearAssigned == $year){
													echo "<option value=\"$year\" selected=\"selected\">$year</option>";
										}//if
										
										if ($yearAssigned != $year){
													echo "<option value=\"$year\">$year</option>";
										}//if
										
							}//for
					
				
	echo "</select>";
	
	echo "<label>&nbsp; mm-dd-yyyy</label>";
	
	echo "</div>";
	
	
	echo "<div style=\"clear:both;\">&nbsp;</div>";
	
	
	
	
	
	
	
	
	echo "<div class=\"leftForm\"><label>Priority</label></div>
	";
	
	$priority = $rows['priority'];
										
	
	echo "<div class=\"rightForm\">
				
							<select name=\"Priority\">
										<option value=\"0\"></option>";
							
							
										$maxPriorityLevel = 5; //max priority is 5, min is 1
										for ($i=1; $i<=$maxPriorityLevel; $i++){
										
													if ($priority == $i){
																echo "<option value=\"$i\" selected=\"selected\">$i</option>";
													}//if
										
													if ($priority != $i){
																echo "<option value=\"$i\">$i</option>";
													}//if
										}//for
				
				
				
				
				echo "</select>";
				echo "<label>1 is low, 5 is high<label>";
	
	echo "</div>";
	
	
	echo "<div style=\"clear:both;\">&nbsp;</div>";
	
	
	
	

	
	
	
	echo "<div class=\"leftForm\"><label>Submitted by</label></div>
	
				<div class=\"rightForm\">
							<select name=\"submittedBy\">
										<option value=\"0\"></option>
	";
						
	$locationid = sessvar("loc_id");
	$listOfEmployees = lavu_query("SELECT * FROM users WHERE loc_id=\"[1]\"", $locationid);
				
					
					
	$storePartNamesAndIDs; //associateive array -> I DON'T THINK I NEED THIS ANYMORE
					
					while ($rowListOfEmployees = mysqli_fetch_assoc($listOfEmployees)){
								
								$employeeID = $rowListOfEmployees['id'];
								$employeeFirstName = $rowListOfEmployees['f_name'];
								$employeeLastName = $rowListOfEmployees['l_name'];
								
								/* store ALL parts and IDs in text field so javascript function getPartName() can use this as a database to get the part name from the part id */
								$storePartNamesAndIDs[$serviceTypeID] = $serviceTypeName;//DO I NEED THIS ANYMORE?
								
								if ($submittedBy == $employeeID){
											echo "<option value=\"$employeeID\" selected=\"selected\">$employeeFirstName $employeeLastName</option>";
								}//if
								
								if ($submittedBy != $employeeID){
											echo "<option value=\"$employeeID\">$employeeFirstName $employeeLastName</option>";
								}//if
								
					}//while
					
				
	echo "</select></div>";
				
	echo "<div style=\"clear:both;\"></div>";
	echo "<div style=\"clear:both;\">&nbsp;</div>";
	
	
	
	
	
	
	
	
	
	
	
	
	
	
?>
	<br/>
	<!--<img onClick="service_types_updatePartsDB();" src="images/btn_submit.png" width="100" height="33" alt="update">-->
		<input type="submit" class="submitButton" value="" onClick="">
	</form>
				
				

	<div style=\"clear:both;\"></div>
	</div>