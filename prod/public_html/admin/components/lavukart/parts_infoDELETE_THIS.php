<?php
			require_once(dirname(__FILE__) . "/../comconnect.php");
			require_once(dirname(__FILE__) . "/./part_functions.php");
			session_start();
?>


<?php
		$var = reqvar("loadRightSide");
		if ($var != "true"){
				exit();			
		}//if
		?>
	


<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Garage Info</title>

		<style type="text/css">
<!--
body {
	background-color: transparent;
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	-webkit-tap-highlight-color:rgba(0,0,0,0);
	font-family:Verdana, Geneva, sans-serif;
	color:#333;
}

.leftForm{
	float:left;
	width:75px;
}

.rightForm{
	float:left;
	width:300px;
}

.submitButton{
	background-image:url(images/btn_update_100x33.png);
	background-repeat: no-repeat;
	width:100px;
	border:none;
	height:33px;
}

input{
	background-image:url(images/tab_bg_262.png);
	background-repeat:no-repeat;
	background-color:transparent;
	border:none;
	width:262px;
	height:46px;
}

label{
	color: #5A75A0;
	font-family:Verdana, Geneva, sans-serif;
	font-size:10pt;
	font-weight: bold;
}

.centerCell{
	width:412px;
	
	background-image:url(images/tab_v1_mid.png);
	background-repeat:repeat-y;
	
	height:auto;
	
}

.topCap{
	background-image:url(images/tab_v1_top_612.png);
	background-repeat:no-repeat;
	height:9px;
}


.bottomCap{
	background-image:url(images/tab_v1_bot_612.png);
	background-repeat:no-repeat;
	height:9px;
}

-->
		</style>
		
<script type="text/javascript" src="parts_pit_forms.js"></script>		

<script type="text/javascript">

function parts_addPartScript(id){
					window.location = "_DO:cmd=create_overlay&f_name=lavukart/parts_addPart.php&c_name=overlay_comp&dims=175,100,474,549?EditOrAdd=edit&id=" + id;
}//parts_addPartScript()

function changeBackgroundImage(id){
			/* function changes background image to 'lit' or 'unlit' depending on what is appropriate, the id input parameter is the id assigned to each <table></table> element. it is the same as the part id since all parts have unique 	          IDs*/
			
			var lastRowLit = document.getElementById('lastTRLit').value;
			
			/* If a row is lit this hidden field will contain a value, the value is the id of the last selected <tr></tr> */
			if (lastRowLit != ""){
					
					/* change back to 'unlit' image */
						var topCapID = "topCap" + lastRowLit;
					
						document.getElementById(topCapID).style.backgroundImage="url('images/tab_v1_top_612.png')";
			
						document.getElementById(lastRowLit).style.backgroundImage="url('images/tab_v1_mid_612.png')";
			
						var botCapID = "botCap" + lastRowLit;
						document.getElementById(botCapID).style.backgroundImage="url('images/tab_v1_bot_612.png')";
		 
			}//if
			
			/* Change background image of current selected table row to the 'lit' image */
			var topCapID = "topCap" + id;
			document.getElementById(topCapID).style.backgroundImage="url('images/tab_v1_toplit_612.png')";
			
			document.getElementById(id).style.backgroundImage="url('images/tab_v1_midlit_612.png')";
			
			var botCapID = "botCap" + id;
			document.getElementById(botCapID).style.backgroundImage="url('images/tab_v1_botlit_612.png')";
		
			/* store id in hidden text field so we can switch that row off later */
			document.getElementById('lastTRLit').value = id; 
			
	}//changeBackgroundImage()


function showAlphaBetizer(){
				/* Pop up the overlay */
				window.location = "_DO:cmd=create_overlay&f_name=lavukart/parts_alphabetizer.php;lavukart/parts_alphabetizer.php&c_name=parts_parts_addPartWrapper;parts_alphabetizer&dims=645,0,123,874;645,0,123,874";
	
		}//parts_addPartScript()





	function parts_updatePartsDB(){
				window.location = "_DO:cmd=load_url&c_name=parts_list_wrapper&f_name=lavukart/parts_updatePart.php?id=" + id;
	}//parts_updatePartsDB()
	

</script>

	</head>
	
	
	
	<body>
	<form action="parts_updatePart.php" method="post" name="parts_addPartForm" id="parts_updatePartForm" onSubmit="return validateAddForm();">
	
	
	<?php
		echo "<input type=\"hidden\" name=\"lastTRLit\" id=\"lastTRLit\">"; /*Store the id of the last <tr></tr> element selected so we can change the background image back to unlit */
		
	$locationid = sessvar("loc_id");
	$category = reqvar("category");
	
	if ($category != "All Parts by Name"){
			/* only make the alphabetizer (AZ icon) available when parts are organzized alphabetically by name. Otherwise, hide it */
				echo "<script type='text/javascript'>window.location='_DO:cmd=send_js&c_name=parts_info_wrapper&js=hideAZIcon()';</script>";
	}//if
	
	if ($category == "All Parts by ID"){
		
				$result = lavu_query("SELECT * FROM lk_parts WHERE locationid=\"[1]\"", $locationid);
	}//if
	
	if ( ($category == "All Parts by Name") || (!isset($category)) ){
				$result = lavu_query("SELECT * FROM lk_parts WHERE locationid=\"[1]\" ORDER BY name", $locationid);
				echo "<script type='text/javascript'>window.location='_DO:cmd=send_js&c_name=parts_info_wrapper&js=displayAZIcon()';</script>";
	}//if
	
	echo "<a name=\"Number\"></a>";
	$lastLetter = chr(96);
	
	while ($rows = mysqli_fetch_assoc($result)){
				
				
		
						//$category = $categoriesArray[$i];
						$id = $rows['id'];
					
		
						$topCapID = "topCap" . $id;
						$botCapID = "botCap" . $id;
						$name = $rows['name'];
						$custom_part_id = $rows['custom_part_id'];
						echo "<div class=\"topCap\" id=\"$topCapID\"></div>";

						
							
						echo "<div id=\"$id\" onclick=\"parts_addPartScript('$id'); changeBackgroundImage('$id');\" style=\"background-image:url(images/tab_v1_mid_612.png); background-repeat:repeat-y; width:612px; height:auto; \">";	
					
						
							
							
							
							if (isAlphaCharacter($name{0})){
									$lastLetter = insertAnchor($name,$lastLetter);
							}//if
							
							echo "<div style=\"width:600px;  padding-left:5px; font-size:10pt;\"><div style='font-weight:600; float:left; width:100px;'>".strtoupper($custom_part_id)."</div><div style='float:left; width:440px; font-weight:600; color:#555;'>$name </div><div style='clear:both;'></div></div>";
			
							echo "<div style=\"clear:both;\"></div>";
							echo "</div>";
					
						echo "<div class=\"bottomCap\" id=\"$botCapID\"></div>";
				}//while
	
	
?>	
<br/>
	<!--<img onClick="parts_updatePartsDB();" src="images/btn_submit.png" width="100" height="33" alt="update">-->
		<!--<input type="submit" class="submitButton" value="" onClick="parts_updatePartsDB();">-->
	</form>
	
	
	
	
	</body>
</html>

<?php



?>