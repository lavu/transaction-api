<?php
	require_once(dirname(__FILE__) . "/../comconnect.php");
	require_once(dirname(__FILE__) . "/./part_functions.php");
?>
	
	
	
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<style type="text/css">


body {
	-webkit-tap-highlight-color:rgba(0,0,0,0);
}

.main{
	background-image:url(images/win_overlay_no_btns.png);
	background-repeat:no-repeat;
	height:549px;
	padding-top:5px;
}

.closeOverlay{
	width:120px;
	height:36px;
	background-image:url(images/close_btn_g.png);
	background-repeat:no-repeat;
	
	
	margin-left:177px;
}

.leftForm{
	float:left;
	width:75px;
}

.rightForm{
	float:left;
	width:300px;
}


form{
		margin-left:40px;
}

.submitButton{
	background-image:url(images/btn_submit_100x33.png);
	background-repeat: no-repeat;
	width: 100px;
	height: 33px;
	border:none;
}

input{
	background-image:url(images/tab_bg_262.png);
	background-repeat:no-repeat;
	background-color:transparent;
	border:none;
	width:262px;
	height:46px;
}

</style>

<script type="text/javascript" src="parts_pit_forms1.js"></script>		

<script type="text/javascript">

function shouldNewCategoryBeShown(){
		var category = document.getElementById("Category").value;
		if (category == "New Category"){
				document.getElementsByName("NewCategoryField").item(0).style.display = "block";
		}//if
		
		if (category != "New Category"){
				document.getElementById("NewCategory").value = "";
				document.getElementsByName("NewCategoryField").item(0).style.display = "none";
		}//if
		
}//shouldNewCategoryBeShown()


function closeparts_addPartOverlay(){
	
			window.location = "_DO:cmd=close_overlay&c_name=parts_info_wrapper";
	
}//closeparts_addPartOverlay()

</script>
<link rel="stylesheet" href="parts_form_select_element2.css" type="text/css">		
</head>

<body style='margin:0px'>
 
 <div class="main">
 
 <div class="closeOverlay" onClick="closeparts_addPartOverlay();">
     
</div><!--closeOverlay-->

<div style="clear:both;">&nbsp;</div>

<form action="parts_updatePart3.php" method="post" name="parts_addPartForm" id="parts_addPartForm" onSubmit="return validateAddForm();">
<?php
$id = reqvar("id");
$_SESSION['partID'] = $id;
$editOrAdd = reqvar("EditOrAdd");

//echo "editOrAdd: $editOrAdd<br><br>";
$locationid = sessvar("loc_id");
echo "<input type='hidden' name='loc_id' value='$locationid'/>";
$results = lavu_query("SELECT * FROM lk_parts WHERE id=\"[1]\" AND locationid=\"[2]\"", $id, $locationid);
		
$rows = mysqli_fetch_assoc($results);	
						
$partName = $rows['name'];
$category = $rows['category'];

$description = $rows['description'];
$quantity = $rows['qty'];
$costPerPart = $rows['cost'];
$custom_part_id = $rows['custom_part_id'];
echo "<input type=\"hidden\" name=\"editOrAdd\" value=\"$editOrAdd\" />";
echo "<input type=\"hidden\" name=\"partID\" value=\"$id\" />";

echo "
	<div class=\"leftForm\">
				<label>Name</label>
	</div>
	
	<div class=\"rightForm\">
				<input type=\"text\" name=\"name\" value=\"" . showQuote($partName) . "\" />
	</div>
	
	<div style=\"clear:both;\">&nbsp;</div>
	
	
	<div class=\"leftForm\">
				<label>Part ID</label>
	</div>
	
	<div class=\"rightForm\">
				<input type=\"text\" name=\"custom_part_id\" value=\"" . showQuote($custom_part_id) . "\" />
	</div>
	
	<div style=\"clear:both;\">&nbsp;</div>
	";
	
	/*
	<div class=\"leftForm\">
				<label>Category</label>
	</div>
	
	<div class=\"rightForm\">
				<select name=\"Category\" id=\"Category\" class=\"styled-select\" onChange=\"shouldNewCategoryBeShown()\">
						<option value=\"\"></option>
						<option value=\"New Category\">New Category</option>";
							
						//	$getCategoriesResult = lavu_query("SELECT value3 FROM config WHERE location=\"[1]\" AND setting=\"categories\"",$locationid);
						//	$categoryRows = mysqli_fetch_assoc($getCategoriesResult);
						//	$categoryVals = $categoryRows['value3'];
						//	$categoryArray = explode(",", $categoryVals);
						//	sort($categoryArray);
							
							$resGetCategories = lavu_query("SELECT * FROM lk_categories WHERE locationid='[1]' ORDER BY Category",$locationid);
							$categoryArray = array();
							$catIndex = 0;
							while ($getCatRow = mysqli_fetch_assoc($resGetCategories)){
										$categoryArray[$catIndex] = $getCatRow['Category'];
										$catIndex++;
							}//while
							for ($i=0; $i<count($categoryArray);$i++){
									$curCategory = $categoryArray[$i];
									if ($category == $curCategory){
											echo "<option value=\"$curCategory\" selected>$curCategory</option>";
									}//if
									if ($category != $curCategory){
											echo "<option value=\"$curCategory\">$curCategory</option>";
									}//if
							}//for
						
				echo "
				</select>
	</div>
	*/
	echo"
	<span name=\"NewCategoryField\" style=\"display:none;\">
	<div class=\"leftForm\">
				<label>New Category <span style=\"color:red\">*</span></label>
	</div>
	
	<div class=\"rightForm\">
				<input type=\"text\" name=\"NewCategory\" id=\"NewCategory\" />
	</div>
	
	<div style=\"clear:both;\">&nbsp;</div>
	</span>
	
	<div style=\"clear:both;\">&nbsp;</div>
	
	<div class=\"leftForm\">
				<label>Desc.</label>
	</div>
	
	<div class=\"rightForm\">
				<input type=\"Description\" name=\"Description\" value=\"" . showQuote($description) . "\"/>
	</div>
	
	
	<div style=\"clear:both;\">&nbsp;</div>
	
	<div class=\"leftForm\">
				<label>Qty.</label>
	</div>
	
	<div class=\"rightForm\">
				<input type=\"text\" name=\"quantity\" value=\"" . showQuote($quantity) . "\" />
	</div>
	
	<div style=\"clear:both;\">&nbsp;</div>
	
	<div class=\"leftForm\">
				<label>Cost per part</label>
	</div>
	
	<div class=\"rightForm\">
				<input type=\"text\" name=\"CostPerPart\" value=\"" . showQuote($costPerPart) . "\"  />
	</div>
	
	
	<div style=\"clear:both;\">&nbsp;</div>
	
	</tr>
	";

?>
	<div style="clear:both;">&nbsp;</div>
	<input name="hi" type="submit" class="submitButton" value="">
	<!--<img src="images/btn_submit.png" width="100" height="33" onClick="submitAddForm();" alt="Submit">-->
</form>
<div style="clear:both;">&nbsp;</div>
</div><!--main-->



</body>

