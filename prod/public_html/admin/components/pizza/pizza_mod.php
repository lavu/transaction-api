<?php

//error_log("pizza_mod: ".json_encode($_REQUEST));

	//This code is set to be running on admin (in-app) as well as ltg.  Different includes based on which server.
	require_once(dirname(__FILE__).'/../comconnect.php');
	require_once(dirname(__FILE__).'/pizza_mod_shared_funcs.php');

	$seq = 1;
	$opacity = "0.95";
	$tab_opacity = "1";
	$tab_text_color_closed = '#333';
	$tab_bg_color_closed = 'rgba(187,187,187,{$tab_opacity})';
	$tab_shadow_color_closed = '#aaa';
	$tab_text_color_open = '#333';
	$tab_font_size = 13;
	$tab_text_top = 15;
	$tab_bg_color_open = "rgba(60,60,60,{$tab_opacity})";
	$tab_shadow_color_open = '#aaa';
	$tab_radius = '5px';
	$tab_spacing = 5;
	$tab_text_color = "#ccc";
	$cap_height = 35;
	$cap_padding = 0;
	$modifier_bg_color_unselected = "rgba(255,255,255,{$opacity})";
	$modifier_bg_color_selected = "rgba(240,240,240,{$opacity})";
	$seperator_color = '#eee';
	$cat_height = 52;
	$page_break_font_size = 20;
	$i_num_columns = 2;

	$previously_set_modifiers = reqvar("previous_mods", "[]");
?>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Pizza Toppings</title>

		<style type="text/css">
body {
	background-color: transparent;
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	-webkit-tap-highlight-color: rgba(0,0,0,0);
	-webkit-user-select: none;
}

div {
	-webkit-user-select: none;
}
span {
	-webkit-user-select: none;
}
select {
	-webkit-user-select: none;
}
font {
	-webkit-user-select: none;
}
img {
	-webkit-user-select: none;
}
table {
	-webkit-user-select: none;
}
td {
	-webkit-user-select: none;
}
tr {
	-webkit-user-select: none;
}

.style4 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #8896AD;
}
.style6 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 24px;
	font-weight: bold;
	color: #8896AD;
}
.style8 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 15px;
	font-weight: bold;
	color: #07142a;
}
.forced_modifier_list_container {
	font-family: Verdana, Geneva, sans-serif;
	font-weight: bold;
}
.category_tab {
	color: <?php echo $tab_text_color_closed; ?>;
	height: <?php echo $cat_height; ?>px;
	/*background:URL("images/tab_dark_allround.png");*/
	background-color: <?php echo $tab_shadow_color_closed; ?>;
	background-repeat: no-repeat;
	background-position: top center;
	border-radius: <?php echo $tab_radius; ?>;
	overflow: hidden;
	border: 0px solid none;
	box-shadow: 0px 40px 10px 0px <?php echo $tab_bg_color_closed; ?> inset;
	margin-top: <?php echo $cap_height; ?>px;
	box-sizing: border-box;
}
.category_tab.pressed {
	color: <?php echo $tab_text_color_open; ?>;
}
.category_tab.expanded {
	/*background:URL("images/tab_dark_topround.png");*/
	background-color: <?php echo $tab_bg_color_open; ?>;
	border-radius: 0;
	border-top-left-radius: <?php echo $tab_radius; ?>;
	border-top-right-radius: <?php echo $tab_radius; ?>;
	color: <?php echo $tab_text_color; ?>;
}
.category_tab_contents {
	position: relative;
	padding-left: 15px;
	font-size: <?php echo $tab_font_size; ?>px;
	top: <?php echo $tab_text_top; ?>px;
}
.category_extra_text {
	font-weight: normal;
}
.modifier_list {
	padding: 0;
	margin: 0;
}
.modifer_list_cap {
	/*background: URL('images/tab_dark_cap_bottomround_10h.png');*/
	background-color: <?php echo $tab_shadow_color_open; ?>;
	background-repeat: no-repeat;
	background-position: top center;
	height: <?php echo $cap_height; ?>px;
	border-radius: 0;
	border-bottom-left-radius: <?php echo $tab_radius; ?>;
	border-bottom-right-radius: <?php echo $tab_radius; ?>;
	box-shadow: 0px 0px <?php echo min((int)$tab_radius, $cap_height); ?>px 0px <?php echo $tab_bg_color_open; ?> inset;
}
.modifier_tab {
	position: relative;
	font-size: 13px;
	height: <?php echo $cat_height; ?>px;
	/*background: URL('images/tab_light_noround_framed.png');*/
	background-color: <?php echo $modifier_bg_color_unselected; ?>;
	background-repeat: no-repeat;
	background-position: top center;
	border: 1px solid <?php echo $tab_bg_color_open; ?>;
	border-bottom: 0px solid none;
	border-top: 1px solid <?php echo $seperator_color; ?>;
	box-sizing: border-box;
}
.modifier_tab.selected {
	/*background: URL('images/tab_light_noround_lit_framed.png');*/
	background-color: <?php echo $modifier_bg_color_selected; ?>;
	background-repeat: no-repeat;
	background-position: top center;
	border: 1px solid <?php echo $tab_bg_color_open; ?>;
	border-bottom: 0px solid none;
	border-top: 1px solid <?php echo $seperator_color; ?>;
}
.modifier_text {
	padding: 15px;
	color: #333;
	position: absolute;
	left: 0px;
}
.modifier_options {
	padding-right: 15px;
	position: absolute;
	right: 0px;
	margin-top: 7px;
}
.modifier_price {
	color: #aaa;
}
.page_break {
	display: table;
	width: 100%;
	height: <?php echo $cat_height; ?>;
	border: 2px solid black;
	border-radius: <?php echo $tab_radius; ?>;
	background-color: <?php echo $tab_bg_color_open; ?>;
	color: <?php echo $tab_text_color; ?>;
	font-size: <?php echo $page_break_font_size; ?>px;
	font-weight: bold;
	text-align: center;
	vertical-align: middle;
	margin-top: <?php echo $cap_height; ?>px;
}
.page_break font {
	position: relative;
	top: <?php echo ($cat_height/2 - $page_break_font_size/2 - 3); ?>px;
}
.column {
	float: left;
	min-height: 10px;
}
.column_spacer {
	float: left;
	min-height: 10px;
}
.column_container {
	display: table;
	margin: 0 auto;
}
.category_container {
}
.portions {
	margin: -6px 0px 0px 0px;
	width: 30px;
	height: 40px;
	display: inline-block;
	padding: 0px;
}
.portions.left {
	background: URL('/components/pizza/images/i_pizza_left.png')           no-repeat 50% 50%;
	background-size: 25px 25px;
}
.portions.left.selected {
	background: URL('/components/pizza/images/i_pizza_left_selected.png')  no-repeat 50% 50%;
	background-size: 25px 25px;
}
.portions.right {
	background: URL('/components/pizza/images/i_pizza_right.png')          no-repeat 50% 50%;
	background-size: 25px 25px;
}
.portions.right.selected {
	background: URL('/components/pizza/images/i_pizza_right_selected.png') no-repeat 50% 50%;
	background-size: 25px 25px;
}
.portions.whole {
	background: URL('/components/pizza/images/i_pizza_whole.png')          no-repeat 50% 50%;
	background-size: 25px 25px;
}
.portions.whole.selected {
	background: URL('/components/pizza/images/i_pizza_whole_selected.png') no-repeat 50% 50%;
	background-size: 25px 25px;
}
.choice_tab {
 	color: <?php echo $tab_text_color_closed; ?>;
 	height: 20px;
 	background-color: <?php echo $tab_shadow_color_closed; ?>;
 	background-repeat: no-repeat;
 	background-position: top center;
 	overflow: hidden;
 	border: 0px solid none;
 	box-shadow: 0px 40px 10px 0px <?php echo $tab_bg_color_closed; ?> inset;
 	margin-top: 0px;
 	box-sizing: border-box;
 	text-align: right;
 }
 .choice_tab_contents {
 	position: relative;
 	padding-right: 15px;
 	font-size: <?php echo $tab_font_size; ?>px;
 	top: 0px;
 }
 .choice_tab.expanded {
 	/*background:URL("images/tab_dark_topround.png");*/
 	background-color: <?php echo $tab_bg_color_open; ?>;
 	border-radius: 0;
 	color: <?php echo $tab_text_color; ?>;
 }
 .soldout {
	text-decoration: line-through;
	color: #aaa;
 }

/*.portions.selected {
	border: 2px solid black;
	border-radius: 20px;
	padding: 0px;
}*/
		</style>
		<script type="text/javascript" src="/manage/js/jquery/js/jquery-1.9.0.js"></script>
		<?php echo $s_shared_javascript; ?>
		<script type="text/javascript">

			function tryParseJSON(jsonString) {
				try {
					var o = JSON.parse(jsonString);
					if (o && typeof o === "object" && o !== null) {
							return o;
					}
				}
				catch (e) { }

				return false;
			};

			function validStringFromObject(obj, key, def)
			{
				if (!def)
				{
					def = "";
				}

				if (typeof obj !== "object")
				{
					return def;
				}

				if (typeof key !== "string")
				{
					return def;
				}

				var string = obj[key];

				if (typeof string === "undefined")
				{
					return def;
				}

				if (string == null)
				{
					return def;
				}

				return string.toString();
			}

			function validString(string)
			{
				if (typeof string === "undefined")
				{
					return "";
				}

				if (string == null)
				{
					return "";
				}

				return string.toString();
			}

			var previouslySetModifiers_str = '<?php echo str_replace('\"', '\\\"', $previously_set_modifiers); ?>';

			var previouslySetModifiers = tryParseJSON(previouslySetModifiers_str);
			var processingPreviouslySetModifiers = false;
			var modifierBatchForWrapper = [];

			window.setTimeout(function(){ processPreviouslySetModifiers(); }, 250);

			function processPreviouslySetModifiers()
			{
				if (!previouslySetModifiers)
				{
					return;
				}

				if (previouslySetModifiers.constructor !== Array)
				{
					return;
				}

				processingPreviouslySetModifiers = true;
				modifierBatchForWrapper = [];

				for (var psm = 0; psm < previouslySetModifiers.length; psm++)
				{
					var this_mod = previouslySetModifiers[psm];
					var this_mod_id = validStringFromObject(this_mod, "mod_id");
					var this_mod_price = validStringFromObject(this_mod, "price");
					if (this_mod_id.length == 0)
					{
						continue;
					}

					var this_mod_portion = "whole";
					var this_mod_heaviness = "normal";

					if ("custom_mod_info" in this_mod)
					{
						var custom_mod_info_str = validStringFromObject(this_mod, "custom_mod_info");
						var custom_mod_info = tryParseJSON(custom_mod_info_str);
						if (custom_mod_info)
						{
							this_mod_portion = validStringFromObject(custom_mod_info, "portion", this_mod_portion);
							this_mod_heaviness = validStringFromObject(custom_mod_info, "heaviness", this_mod_heaviness);
						}

						if (this_mod_portion.length == 0)
						{
							this_mod_portion = "whole";
						}

						if (this_mod_heaviness.length == 0)
						{
							this_mod_heaviness = "normal";
						}
					}

					var modifier_tab = document.getElementById("modifier_" + this_mod_id);
					if (!modifier_tab)
					{
						continue;
					}

					var modifier_text = null;

					var modifier_tab_children = modifier_tab.children;
					for (var c = 0; c < modifier_tab_children.length; c++)
					{
						var mt_child = modifier_tab_children[c];
						if (mt_child.classList.contains("modifier_text"))
						{
							modifier_text = mt_child;
							continue;
						}

						if (mt_child.classList.contains("modifier_options"))
						{
							var modifier_options_children = mt_child.children;
							var mo_children_count = modifier_options_children.length;
							if (mo_children_count == 1)
							{
								toggleOnOffFMODRow(this_mod_id);
							}
							else
							{
								for (var moc = 0; moc < modifier_options_children.length; moc++)
								{
									var mo_child = modifier_options_children[moc];
									if (mo_child.classList.contains(this_mod_portion))
									{
										var button_span = mo_child;

										selectPortion( $( $(button_span).children('[type=radio]')[0] ) );

										break;
									}
								}

								if (modifier_text && (this_mod_heaviness != "normal"))
								{
									applyPreselectedHeaviness(modifier_text, this_mod_heaviness, this_mod_price);
								}
							}

							break;
						}
					}
				}

				if (modifierBatchForWrapper.length > 0)
				{
					setTimeout(function()
					{
						window.location = "_DO:cmd=send_js&c_name=pizza_mod_wrapper&js=" + encodeURIComponent( "addModifierBatch(" + JSON.stringify(modifierBatchForWrapper) + ")");
					}, 150);
				}

				processingPreviouslySetModifiers = false;
			}

			function toTitleCase(str)
			{
				return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
			}

			function applyPreselectedHeaviness(modifier_text, heaviness, price)
			{
				var title_id = modifier_text.id;
				var value_id = title_id + "_value";

				var jtitle_obj = $(modifier_text);
				var jmod_tab = getJModifierTab(jtitle_obj);

				var original_price = parseFloat(jmod_tab.find("input[name=original_modifier_price]").val());
				var heaviness_price = original_price;
				var heaviness_is_detour= false;
				if (heaviness == "extra")
				{
					heaviness_price = (original_price * 2);
				}
				else if (heaviness.match(/\((.*?)\)/))
				{
					heaviness_price = price;
					heaviness_is_detour = true;
				}

				var new_title = "";
				var mt_children = modifier_text.children;
				for (var mtc = 0; mtc < mt_children.length; mtc++)
				{
					var mt_child = mt_children[mtc];
					if (mt_child.classList.contains("modifier_title"))
					{
						if (heaviness_is_detour)
						{
							new_title = mt_child.innerHTML + " " + toTitleCase(heaviness);
						}
						else
						{
							new_title = toTitleCase(heaviness) + " " + mt_child.innerHTML;
						}
					}
				}

				var modifier_input = document.getElementById(value_id);
				if (modifier_input)
				{
					modifier_input.value = new_title + "|" + heaviness_price.toString();
				}

				// set the new title and cost
				jtitle_obj.find(".modifier_title").text(new_title);
				jmod_tab.find("input[name=heaviness_modifier_price]").val(heaviness_price);
				jtitle_obj.find(".modifier_price").text(calculatePriceByPortion(jmod_tab));
				jtitle_obj.find("font").show();

				var jportion = jmod_tab.find(".portions.selected");
				if (jportion.length == 0)
				{
					jportion = jmod_tab.find(".portions");
				}
				var jradio = jportion.find("input[type=radio]");
				var is_choice = jradio.hasClass('is_choice');

				updateCategoryTextByModTab(jmod_tab, is_choice);
				sendPortionToWrapper(jmod_tab, jradio, is_choice);
			}

			function errorLog(msg) {
				$.ajax({
					url: "/components/pizza/test.php",
					async: false,
					cache: false,
					data: { cmd:msg }
				});
			}

			function expandCategory(category_id) {
				var cat_id = 'category_'+category_id;
				var mods_id = 'catmods_'+category_id;
				var jcategory = $('#'+cat_id);
				var jmods = $('#'+mods_id);
				var expanded = jcategory.hasClass('expanded');

				if (expanded) {
					return;

					// collapse this category
					jcategory.removeClass('expanded');
				} else {

					// expand this category
					jcategory.addClass('expanded');

					// collapse the other categories
					var category_ids = [<?php
						$a_categories = get_categories();
						$a_cat_ids = array();
						foreach($a_categories as $a_category)
							$a_cat_ids[] = $a_category['id'];
						echo implode(',', $a_cat_ids);
						?>];
					for (var i = 0; i < category_ids.length; i++) {
						if (category_ids[i] == category_id)
							continue;
						var jcategory = $('#category_'+category_ids[i]);
						var is_expanded = jcategory.hasClass('expanded');
						if (is_expanded)
							expandCategory(category_ids[i]);
					}
				}
				expanded = !expanded;

				jmods.stop(true, true);
				if (expanded) {
					var full_height = jmods.children('input[name=full_height]').val();
					jmods.show();
					jmods.animate({ height: full_height }, 250);
				} else {
					jmods.animate({ height: 0 }, {
						duration: 250,
						complete: function() {
							jmods.hide();
						}
					});
				}
			}

			/**
			 * Find the parent modifier tab
			 * @param  jquery object jelement A child of the modifier tab, or the modifier tab
			 * @return jquery object          The modifier tab
			 */
			function getJModifierTab(jelement) {
				var retval = jelement;
				if (jelement.hasClass('modifier_tab'))
					return jelement;
				return getJModifierTab(jelement.parent());
			}

			/**
			 * Select or unselect the portion of the modifier row based on the clicked on radio button
			 * @param  jquery object jradio The radio button that was clicked on
			 * @return N/A                  No return value.
			 */
			function selectPortion(jradio) {

				// get properties about the radio button
				var radio_name = jradio.prop('name');
				var portion_buttons = $("input[name="+radio_name+"]");
				var jmod_tab = getJModifierTab(jradio);
				var is_choice = jradio.hasClass('is_choice');

				// set properties about the button
				if (jradio.prop('checked') && !is_choice) {
					jradio.prop('checked', false);
					jmod_tab.removeClass('selected');
		//alert('DESELECTING');
				} else {
					jradio.prop('checked', true);
					jmod_tab.addClass('selected');
		//alert('SELECTING');
				}

				// set the properties about the other buttons
				$.each(portion_buttons, function(k, html_radio) {

					// check or uncheck the radio button images
					jportion = $(html_radio);
					if (jportion.prop('checked')) {
						jportion.parent().addClass('selected');
					} else {
						jportion.parent().removeClass('selected');

						// unhighlight the tab if it's a choice, not a checklist
						if (jportion.hasClass('is_choice')) {
							var jmod_tab = getJModifierTab(jportion);
							jmod_tab.removeClass('selected');
						}
					}
				});

				// set the price
				var jmodElement = jmod_tab.find(".modifier_price");
				jmodElement.text(calculatePriceByPortion(jmod_tab));

				// set the count and the price in the pizza wrapper
				updateCategoryTextByModTab(jmod_tab, is_choice);
				sendPortionToWrapper(jmod_tab, jradio, is_choice);
			}

			/**
			 * Multiply the price, with the heaviness mod applied, by the portion modifier
			 * @param  jquery object jmodifier The modifier tab row (of the topping)
			 * @param  double        price     The price of the item, with the heaviness mod applied
			 * @return double                  The price of the item, with the heaviness mod and portion mod applied
			 */
			function calculatePriceByPortion(jmodifier) {
				var jheavinessPriceMod = jmodifier.find("input[name=heaviness_modifier_price]");
				var price = parseFloat(jheavinessPriceMod.val());
				var jportionSelected = jmodifier.find(".portions.selected");
				if (jportionSelected.length == 0)
					jportionSelected = jmodifier.find(".portions.whole");
				if (jportionSelected.length == 0)
					jportionSelected = jmodifier.find(".portions");
				var jpriceMod = jportionSelected.find("input[name=pricemod]");
				var fpriceMod = parseFloat(jpriceMod.val());
				return (price * fpriceMod).toFixed(2);
			}

			/**
			 * Tell the pizza_mod_wrapper about a modifier that was either added or removed
			 * @param  jquery object  jmod_tab  the modifier row
			 * @param  jquery object  jradio    the radio button that was pressed
			 * @param  Boolean        is_choice TRUE if it's a choice category, FALSE if it's a list category
			 * @return N/A                      No return value.
			 */
			function sendPortionToWrapper(jmod_tab, jradio, is_choice) {
				var modname = jmod_tab.find('input[name=original_modifier_title]').val();
				while (modname.indexOf("---double quote---") > -1) {
					modname = modname.replace("---double quote---", '"');
				}
				var portion = jradio.val();
				var is_choice = jradio.hasClass('is_choice');
				var list_id = jmod_tab.children('input[name=forced_modifier_list_id]').val();
				var mod_id = jmod_tab.children('input[name=forced_modifier_id]').val();
				var a_heaviness = getHeaviness(modname, jmod_tab);
				var heaviness_text = a_heaviness['title'];
				var cost = a_heaviness['cost'];
				var category_page = jmod_tab.children('input[name=category_page]').val();
				var category_order = jmod_tab.children('input[name=category_order]').val();
				var modifier_order = jmod_tab.children('input[name=forced_modifier_order]').val();
				var happyhours = jmod_tab.children('input[name=forced_modifier_happyhours]').val();
				var mod_seq = jmod_tab.children('input[name=forced_modifier_sequence]').val();

				//Added by Brian D.
				if(isPortionDisabled){
					portion = "";
				}

				var arguments = [
					modname,
					portion,
					is_choice,
					list_id,
					mod_id,
					heaviness_text,
					cost,
					category_page,
					category_order,
					modifier_order,
					happyhours,
					mod_seq
				];

				if (processingPreviouslySetModifiers)
				{
					modifierBatchForWrapper.push(arguments);
				}
				else
				{
					arguments[0] = "'" + arguments[0] + "'";
					arguments[1] = "'" + arguments[1] + "'";
					arguments[5] = "'" + arguments[5] + "'";
					arguments[10] = "'" + arguments[10] + "'";

					var arguments_str = arguments.join(', ');

					setTimeout(function() {
						if (jradio.prop('checked')) {
							var cmd = "_DO:cmd=send_js&c_name=pizza_mod_wrapper&js=" + encodeURIComponent( "addModifier("+arguments_str+")");
						} else {
							var cmd = "_DO:cmd=send_js&c_name=pizza_mod_wrapper&js=" + encodeURIComponent( "removeModifier("+arguments_str+")");
						}
						window.location = cmd;
					}, 150);
				}

				drawTotalCost();
			}

			/**
			 * Updates the text in the category header to reflect the choice (or quantity of choices)
			 *     and total cost of items in that category.
			 * @param  jquery object jmod_tab  The radio button from the modifier's row.
			 * @param  Boolean       is_choice TRUE if the category is a choice category, FALSE if it's a list category (eg TRUE for size, FALSE for toppings).
			 * @return N/A                     No return value
			 */
			function updateCategoryTextByModTab(jmod_tab, is_choice) {
				var jmods_selected = getModsSelected(jmod_tab);
				var category_cost = getCategoryCost(jmods_selected);
				if (is_choice) {
					var choice = getJTitle(jmod_tab);
					setModtabExtraText(jmod_tab, choice, drawPrice(category_cost));
				} else {
					var count = jmods_selected.length;
					setModtabExtraText(jmod_tab, 'x'+count, drawPrice(category_cost));
				}
			}

			/**
			 * Gets the cost of an item based on the dom value
			 * @param  dom object modifier The modifier row.
			 * @return integer             The cost of the item inside said row, or 0 if the item price can't be found.
			 */
			function getItemPrice(modifier) {
				var jpriceContainer = $(modifier).find("font.modifier_price");
				if (jpriceContainer.length == 0)
					return 0;
				return parseFloat(jpriceContainer.text());
			}

			/**
			 * Gets the cost of an item based on the dom value and the heaviness descriptor.
			 * @param  string     modname  The original modifier name, before heaviness mods have been applied.
			 * @param  dom object modifier The modifier row.
			 * @return integer             An object {title:string, cost:float}
			 */
			function getHeaviness(modname, modifier) {
				var jmodifier = $(modifier);
				var cost = getItemPrice(jmodifier);
				var jmodifierTitleNow = jmodifier.find(".modifier_title");
				var heavinessTitle = "normal";
				if (jmodifierTitleNow.length > 0) {
					heavinessTitle = $.trim(jmodifierTitleNow.text().replace(modname, "")).toLowerCase();
				}
				return {title:heavinessTitle, cost:cost};
			}

			/**
			 * A helper function of updateCategoryTextByModTab().
			 *     Calculates a category's cost according to the modifier(s) selected.
			 * @param  array jmods_selected An array of jquery objects.
			 * @return float                The total cost for the category.
			 */
			function getCategoryCost(jmods_selected) {
				var retval = 0;

				$.each(jmods_selected, function(k, mod) {
					retval += getItemPrice(mod);
				});

				return retval;
			}

			<?php echo draw_drawPrice_function(); ?>

			/**
			 * Find all selected sibling modifiers of a modifier.
			 * @param  jquery object jmod_tab The radio button from the modifier's row.
			 * @return array                  An array of jmod_tabs.
			 */
			function getModsSelected(jmod_tab) {
				var mods = jmod_tab.siblings();
				var jmods = [];
				var retval = [];

				// get all the mod tabs
				$.each(mods, function(k,html_mod) {
					jmods.push($(html_mod));
				});
				jmods.push(jmod_tab);

				// check if they have a selected radio button
				// and add them to the retval
				$.each(jmods, function(k, jmod) {
					radios = jmod.find('input[type=radio]:checked');
					for (var i = 0; i < radios.length; i++) {
						retval.push(jmod);
						break;
					}
				});

				return retval;
			}

			/**
			 * Since it takes so long to set the inner text of an element on a freaking iPad,
			 * this function was created which sets the text at a delayed time and the delay can
			 * be overwritten by subsequent calls with the same identifier.
			 * @param html dom     element    The element to set the innerText of.
			 * @param weakly typed identifier The identifier of the timed delay.
			 * @param string       text       The text to be set.
			 * @param integer      delay      The delay before setting the text, in milliseconds.
			 */
			function setText(element, identifier, text, delay) {

				// cancel the old timeout
				if (typeof(window.setTextDelays) == 'undefined')
					window.setTextDelays = {};
				var setTextDelays = window.setTextDelays;
				if (typeof(setTextDelays[identifier]))
					clearTimeout(setTextDelays[identifier]);

				if (!element || typeof element === "undefined")
				{
					return;
				}

				// create a new timeout
				setTextDelays[identifier] = setTimeout(function() {
					//element.innerText = text;
					element.innerHTML = '';
					element.appendChild(document.createTextNode(text));
				}, delay);
			}

			/**
			 * Set the text for a category tab's header.
			 * @param jquery object jmod_tab  The radio button from the modifier's row.
			 * @param string        primary   The primary description.
			 * @param string        secondary The secondary description (wrapped in parentheses).
			 */
			function setModtabExtraText(jmod_tab, primary, secondary) {
				var jname_container = jmod_tab.children('input[name=category_id]');
				var category_id = jname_container.val();
				var jcategory = $('#category_'+category_id);
				var jextra = $(jcategory.find('font.category_extra_text'));
				setText(jextra[0], 'cat_text_'+category_id, primary+' ('+secondary+')', 1700);
			}

			/**
			 * Get the title of the modifier row, without the heaviness description.
			 * @param  jquery object jmod_tab          The radio button from the modifier's row.
			 * @return string                          The title of the modifier row (eg '12 inch' from the category 'Size')
			 */
			function getJTitle(jmod_tab) {
				var jtitle = $(jmod_tab.find("input[name=original_modifier_title]")[0]);
				return jtitle.text();
			}

			/**
			 * Initialize the extra text for each category.
			 * @return N/A No return value
			 */
			function initializeText() {
				var modgroups = $('div[id^=catmods_]');

				$.each(modgroups, function(k, modgroup) {
					var jmod_tab = $( $(modgroup).children('div[id^=modifier_]')[0] );
					var jradio = jmod_tab.find('input[type=radio]');
					var is_choice = jradio.hasClass('is_choice');
					if (!is_choice)
						updateCategoryTextByModTab(jmod_tab, false);
				});
			}

			/**
			 * Select a radio choice for the modifier row.
			 * @param  jquery object jcontainer The modifier row.
			 * @param  string        s_name     Name of the modifier.
			 * @param  string        s_value    The portion to select (eg 'whole')
			 * @return N/A                      No return value
			 */
			function selectRadio(jcontainer, s_name, s_value)
			{
				// first check if there are any checked
				var checked = DomExplorer.findCheckedOption(jcontainer[0]);
				if (checked !== null) {
					selectPortion($(checked));
					return;
				}

				var inputs = jcontainer.find("input[name="+s_name+"]");
				for (var i = 0; i < inputs.length; i++) {
					if (inputs[i].value == s_value) {
						var jinput = $(inputs[i]);
						selectPortion(jinput);
					}
				}
			}

			/**
			 * Send the total cost of all categories to the mod_wrapper.
			 * @return N/A No return value.
			 */
			function drawTotalCost() {
				var categories = $("div[id^=category_]");
				var total_cost = <?php echo get_item_cost(); ?>;

				$.each(categories, function(i, category) {
					var extra_text = $(category).find('font.category_extra_text').text();
					if (extra_text != '') {
						var cost_text = extra_text.match(/\([0-9]*.?[0-9]*\)$/)[0];
						if (cost_text != '') {
							var cost = parseFloat(cost_text.split('(')[1].split(')')[0]);
							total_cost += cost;
						}
					}
				});

				window.location = "_DO:cmd=send_js&c_name=pizza_mod_wrapper&js=setTotalCost("+total_cost+")";
			}

			/**
			 * Resizes all of the columns on a page to fit appropriately.
			 * @param  int page The page number to resize things on.
			 * @return N/A      No return value.
			 */
			function sizeColumnsOnPage(page) {

				// get the page, columns, spacers, and other initial variables
				var jpage = $("#page_"+page);
				var jcolumns = jpage.children(".column");
				var jborders = jpage.children(".page_border");
				var jspacers = jpage.children(".column_spacer");
				var jcategoryContainers = jpage.find(".category_container");
				var jwindow = window.jwindow;
				var page_width = jwindow.width();

				// calculate the widths of everything
				var num_spacers = jborders.length*2+jspacers.length;
				var num_columns = jcolumns.length;
				var col_width = Math.min((page_width/num_columns - 10*(num_columns+2)), 426);
				var spacer_width = (page_width - num_columns*col_width) / num_spacers;
				if (num_spacers == 1) spacer_width -= 39;

				// assign widths
				$.each(jcolumns, function(k,v) {
					$(v).css({ 'min-height':'10px', display:'inline-table', 'vertical-align':'top', width:((col_width-2)+'px') });
				});
				$.each(jborders, function(k,v) {
					var v = $(v);
					v.css({ 'min-height':'10px', display:'inline-table', 'vertical-align':'top', width:(spacer_width*2+'px') });
					v.children().css({ width:(spacer_width+'px') });
				});
				$.each(jspacers, function(k,v) {
					$(v).css({ 'min-height':'10px', display:'inline-table', 'vertical-align':'top', width:((spacer_width-2)+'px') });
				});

				// assign heights
				jpage.css({ height:'' });
				$.each(jcategoryContainers, function(k,v) {
					v = $(v);
					var catTab = v.find(".category_tab");
					var modTabs = v.find(".modifier_tab");
					var modStackHeight = 0;
					var modTab = $(modTabs[0]);
					var modHeight = modTab.height() + parseInt(modTab.css('padding-top')) + parseInt(modTab.css('padding-bottom')) + parseInt(modTab.css('border-top')) + parseInt(modTab.css('border-bottom'));
					var catHeight = catTab.height() + parseInt(catTab.css('padding-top')) + parseInt(catTab.css('padding-bottom')) + parseInt(catTab.css('border-top')) + parseInt(catTab.css('border-bottom'));

					if (v.hasClass('multiColumn')) {
						modStackHeight = Math.ceil(modTabs.length/(parseInt(v.children('input.colSpan').val())));
					} else {
						modStackHeight = modTabs.length;
					}

					v.css({ height:(modStackHeight*modHeight+catHeight+'px') })
				});

				// size the last page
				if (page == <?php echo get_categories_max_page(); ?> && parseInt(jpage.height()) < 748)
					jpage.css({ height:'748px' });

				// remove the transparency
				jpage.css({ opacity:1 });
			}

			/**
			 * Ballances out the categories on a page as well as it can so that each column has the same height.
			 * @param  int page The page number to ballance the categories on.
			 * @return N/A      No return value.
			 */
			function ballanceCategories(page) {

				function findShortestColumn(col_data) {
					var shortest_height = 999999999;
					var shortest_column = 0;
					for (var i = 0; i < col_data; i++) {
						if (col_data[i].height < shortest_height) {
							shortest_height = col_data[i].height;
							shortest_column = i;
						}
					}
					return shortest_column;
				}

				// get the page, columns, spacers, and other initial variables
				var jpage = $("#page_"+page);
				var jcolumns = jpage.find(".column");
				var jspacers = jpage.find(".column_spacer");
				var jcategoryHeaders = jpage.find(".category_tab");

				// build an array of categories and some data about them
				var cat_data = [];
				var cat_id = 1;
				$.each(jcategoryHeaders, function(k,v) {
					var cat = $(v).parent();
					var height = cat.height()+parseInt(cat.css('padding-top'))+parseInt(cat.css('padding-bottom'))+parseInt(cat.css('margin-top'))+parseInt(cat.css('margin-bottom'));
					cat_data.push({ cat:cat, height:height, col:1, id:cat_id });
					cat_id++;
				});

				// figure out the appropriate column in which to put each category
				BruteForceBallancer.reset();
				for (var i = 0; i < jcolumns.length; i++)
					BruteForceBallancer.addColumn();
				for (var i = 0; i < cat_data.length; i++)
					BruteForceBallancer.addCategory(cat_data[i]);
				var col_data = BruteForceBallancer.ballanceColumns();

				// place the categories into the appropriate column
				for (var i in col_data) {
					var colnumber = i+1;
					var cats = [];
					for (var j in col_data[i].categories)
						cats.push(col_data[i].categories[j]);
					cats.sort(function(a,b) {
						return a.id - b.id;
					});
					for (var j = 0; j < cats.length; j++) {
						var cat = cats[j];
						if (cat.col != colnumber) {
							cat.cat.detach();
							$(jcolumns[i]).append(cat.cat);
						}
					}
				}
			}

			/**
			 * Used to merge the categories of two columns together and
			 * expand the columns across horizontal space that they both used to occupy.
			 * @param  integer page_num           The page that the columns should be on.
			 * @param  integer first_column_index The index of the first column, will be merged with first_column_index+1
			 * @return string                     one of 'success', 'page not found', 'columns not found', or 'categories do not allow multi-columns merges'
			 */
			function mergeColumns(page_num, first_column_index) {

				// find the page and verify that it exists
				var jpage = $("#page_"+page_num);
				if (jpage.length == 0)
					return 'page not found';
				var jcolumn_container = jpage.children(".column_container");

				// find the columns and verify that they exist
				var jcolumn1 = jcolumn_container.children(".column.column_index_"+first_column_index);
				var jcolumn2 = [];
				for (var second_column_index = first_column_index+1; second_column_index < 100; second_column_index++) {
					jcolumn2 = jcolumn_container.children(".column.column_index_"+second_column_index);
					if (jcolumn2.length > 0)
						break;
				}
				if (jcolumn1.length == 0 || jcolumn2.length == 0)
					return 'columns not found';

				// find the categories and verify that they allow multi-column merges
				var allowMultiColumn = false;
				var categories = jcolumn1.find(".category_tab");
				for (var i = 0; i < categories.length; i++) {
					if (categories[i].classList.contains('allowMultiColumn')) {
						allowMultiColumn = true;
						break;
					}
				}
				if (!allowMultiColumn)
					return 'categories do not allow multi-columns merges';

				// get the column spacer and the dimensions of the new columns and modifiers
				var jcolumn_spacer = jcolumn_container.children(".column_spacer.column_spacer_index_"+(second_column_index-1));
				var num_columns_spanned = second_column_index - first_column_index + 1;
				var new_column_width = jcolumn1.width()+jcolumn2.width()+(jcolumn_spacer.length == 0 ? 0 : jcolumn_spacer.width());
				var new_modifier_width = new_column_width / num_columns_spanned;

				// merge the containers from the second column into the first column
				var second_containers = jcolumn2.find(".category_container");
				$.each(second_containers, function(k,v) {
					v = $(v);
					v.detach;
					jcolumn1.append(v);
				});

				// resize the first column and all of its modifier tabs
				jcolumn1.css({ width:(new_column_width+'px') });
				var mod_tabs = jcolumn1.find(".modifier_tab");
				$.each(mod_tabs, function(k,v) {
					$(v).css({ float:'left', width:(new_modifier_width+'px') });
				});

				// record the number of columns
				// mark the categor containers as spanning multiple columns
				var jcat_containers = jcolumn1.find(".category_container");
				$.each(jcat_containers, function(k,v) {
					v = $(v);
					v.addClass("multiColumn");
					if (v.find("input.colSpan").length > 0)
						v.find("input.colSpan").val(num_columns_spanned);
					else
						v.append("<input type='hidden' class='colSpan' value='"+num_columns_spanned+"' />");
				})

				// remove the second column and the column spacer
				jcolumn2.remove();
				if (jcolumn_spacer.length > 0) jcolumn_spacer.remove();

				// add "spacer modifer tabs" to ballance out the categories
				$.each(jcat_containers, function(k,v) {
					var jmodifier_tabs = $(v).find(".modifier_tab");
					var diff = num_columns_spanned - (jmodifier_tabs.length % num_columns_spanned);
					if (diff % num_columns_spanned > 0) {
						var style = getStyleString(jmodifier_tabs[0]);
						var appendString = "";
						for (var i = 0; i < diff; i++) {
							appendString += "<div class='modifier_tab' style='"+style+"'></div>";
						}
						jcat_containers.find(".modifier_list").append(appendString);
					}
				});

				sizeColumnsOnPage(page_num);

				return 'success';
			}

			/**
			 * Gets the style of an element as a single string
			 * @param  dom object element The element to get the style of.
			 * @return string             A string representing the object's style. Eg "width:50px; height:20px;"
			 */
			function getStyleString(element) {
				var retval = "";
				var styles = element.style;
				for (var i = 0; i < styles.length; i++) {
					retval += styles[i]+":"+element.style[styles[i]]+"; ";
				}
				return retval;
			}

			/**
			 * Used to merge together columns on a page that has empty columns.
			 * The assumption is that columns are filled from lowest index to highest index.
			 * @param  integer page_num           The page that the columns should be on.
			 * @return string                     one of 'success', 'page not found', 'columns already ballanced'
			 */
			function checkForEmptyColumns(page_num) {

				// find the page and verify that it exists
				var jpage = $("#page_"+page_num);
				if (jpage.length == 0)
					return "page not found";

				// find the columns and category containers and verify that this page needs ballancing
				var jcolumns = jpage.find(".column");
				var last_full_column_index = 1;
				var num_empty_columns = 0;
				for (var i = 0; i < jcolumns.length; i++) {
					var jcolumn = $(jcolumns[i]);
					var jcolumnIndex = jcolumn.children("input[name=column_index]");
					var jcategoryContainers = jcolumn.find(".category_container");
					if (jcategoryContainers.length > 0) {
						last_full_column_index = parseInt(jcolumnIndex.val());
					} else {
						num_empty_columns++;
					}
				}

				// merge empty columns into the last column with categories in it
				for (i = 0; i < num_empty_columns; i++)
					mergeColumns(page_num, last_full_column_index);
			}

			// brute forces a ballanced solution for placing categories with a given height into columns
			// can be simplified to a problem of fitting an ordered number line into a set number of groups
			var BruteForceBallancer = {
				columns: [],
				categories: [],
				reset: function() {
					this.columns = [];
					this.categories = [];
				},
				addColumn: function() {
					this.columns.push({ height:0, categories:{} });
				},
				removeColumn: function(column_index) {
					delete this.columns[column_index];
				},
				addCategory: function(category) {
					this.categories.push(category);
				},
				addCategoryToColumn: function(category, column_index) {
					var col = this.columns[column_index];
					col.height += category.height;
					col.categories[category.id] = category;
				},
				ballanceColumns: function() {

					// get some initial values
					var cats = this.categories;
					var cols = this.columns;
					var numColumns = cols.length;
					var numberLine = [];
					for (var i = 0; i < cats.length; i++)
						numberLine.push(cats[i].height);

					// find the best split
					var optimalSplit = this.testSplit(numberLine, [], numColumns+1);
					var splits = optimalSplit.splits;

					// move the categories into the columns based on this split
					for (var i = 0; i < splits.length; i++) {
						for (var j = splits[i]; j < splits[i+1]; j++) {
							this.addCategoryToColumn(cats[j], i);
						}
					}

					return this.columns;
				},
				dup: function(array) {
					var dup = [];
					for (var i = 0; i < array.length; i++)
						dup.push(array[i]);
					return dup;
				},
				findSum: function(numberLine) {
					var total = 0;
					for (var i = 0; i < numberLine.length; i++)
						total += numberLine[i];
					return total;
				},
				findDeltaInColumnHeights: function(cols, avgColumnHeight) {
					var delta = 0;
					if (!cols)
						cols = this.columns;
					var avg = avgColumnHeight;
					var colHeights = [];
					for(var key in cols)
						colHeights.push(cols[key].height);
					for (var i = 0; i < cols.length; i++) {
						delta += Math.abs(cols[i].height - avg);
					}
					return delta;
				},
				testSplit: function(numberLine, curSplits, numSplits, avgColumnHeight) {

					// get some initial values
					var optimalSplit = null;
					if (!avgColumnHeight) {
						avgColumnHeight = this.findSum(numberLine) / (numSplits-1);
					}

					// build the splits
					if (curSplits.length < numSplits) {
						var lineLength = numberLine.length;
						var curSplitIndex = (curSplits.length > 0) ? curSplits[curSplits.length-1] : 0;
						for (var i = curSplitIndex; i <= lineLength; i++) {
							new_splits = this.dup(curSplits);
							new_splits.push(i);
							testOptimalSplit = this.testSplit(numberLine, new_splits, numSplits, avgColumnHeight);
							if (optimalSplit === null) {
								optimalSplit = testOptimalSplit;
							} else {
								if (optimalSplit.delta >= testOptimalSplit.delta) {
									optimalSplit = testOptimalSplit;
								}
							}
						}
						return optimalSplit;
					}

					// make sure the split is valid
					if (curSplits[0] != 0 || curSplits[curSplits.length-1] != numberLine.length)
						return { delta:9999999, splits:curSplits };

					// test the splits
					var testcols = [];
					for (var i = 0; i < curSplits.length-1; i++) {
						var height = 0;
						for (var j = curSplits[i]; j < curSplits[i+1]; j++) {
							height += numberLine[j];
						}
						testcols.push({ height:height });
					}
					var delta = this.findDeltaInColumnHeights(testcols, avgColumnHeight);
					optimalSplit = { delta:delta, splits:curSplits };
					return optimalSplit;
				}
			};

			PageTurner = {
				turnPage: function(nextPageNum) {

					// get some variables
					var currentPageNum = window.currentPage;
					var jcurrentPage = $("#page_"+currentPageNum);
					var jnextPage = $("#page_"+nextPageNum);
					//jcurrentPage.stop(true,true);
					//jnextPage.stop(true,true);
					//var fullPageWidth = parseInt(jcurrentPage.width());

					// create the animation for the current page
					//var currToLeft = (nextPageNum > currentPageNum ? -1 : 1)*fullPageWidth;
					//jcurrentPage.css({ position:'fixed', left:0 });
					//jcurrentPage.animate({ left:currToLeft }, 4000, 'linear', function(){jcurrentPage.hide();});
					jcurrentPage.hide();

					// create the animation for the next page
					//var nextFromLeft = -currToLeft;
					//jnextPage.css({ position:'fixed', left:nextFromLeft });
					jnextPage.show();
					//jnextPage.animate({ left:0 }, 4000, 'linear');

					// update the window vars
					window.currentPage = nextPageNum;
				}
			}

			DomExplorer = {
				findCheckedOption: function(modifierRowElement) {

					// find the options container and all of its spans
					var optionsContainer = this.findChildWithClass(modifierRowElement, 'modifier_options');
					var spans = optionsContainer.children;

					// go through each span and look for checked radio buttons
					var element = null;
					for (var i = 0; i < spans.length; i++) {
						var inputs = spans[i].children;
						if (typeof(inputs) == 'undefined' || inputs.length == 0)
							continue;
						for (var j = 0; j < inputs.length; j++) {
							if (inputs[j].type == 'radio' && inputs[j].checked) {
								element = inputs[j];
								break;
							}
						}
						if (element !== null)
							break;
					}

					return element;
				},
				findChildWithClass: function(parent, sclass) {
					var children = parent.children;
					for (var i = 0; i < children.length; i++)
						if (this.hasClass(children[i], sclass))
							return children[i];
					return null;
				},
				hasClass: function(element, sclass) {
					return element.classList.contains(sclass);
				}
			};

			function viewPizza() {
				var jpages = $("#pages");
				jpages.css({ opacity:0 });
			}

			function hidePizza() {
				var jpages = $("#pages");
				jpages.css({ opacity:1 });
			}

			// computes the current page that is being shown
			function computeCurrentPage() {

				// get some values
				var pageBreaks = $(".page_break");
				var jwindow = window.jwindow;
				var windowScroll = jwindow.scrollTop();
				var page = 0;

				// go through each pageBreak until one can be found that isn't smaller than scrollTop()
				for (var i = 0; i < pageBreaks.length; i++) {
					var offset = $(pageBreaks[i]).offset().top;
					if (offset > windowScroll+100)
						break;
					page++;
				}

				// update the page
				if (page != window.currentPage) {
					window.currentPage = page;
					window.location = "_DO:cmd=send_js&c_name=pizza_mod_wrapper&js=PageTurner.setPageMarker("+page+")";
				}

			}

			function toggleOnOffFMODRow(fmod_id)
			{
				console.log("toggleOnOffFMODRow: " + fmod_id);
				var radioInSpan = document.getElementById('radio_in_button_span_'+fmod_id);
				selectPortion( $(radioInSpan) );
			}

			/**
			 * Allows user to choose the heaviness, such as Light, Normal, or Extra
			 * @param object			title_obj
			 * @param Array:string		title_options
			 * @return N/A
			 *
			 * new param: options_source_type can be: 'default_heaviness' for original behaviour, 'none', or 'detour' (where clients can set their own)
			 *         if the options_source_type is none, then title_options should essentially be empty, so I guess may not have any affect here.
			 */
			function mod_title_touch(title_obj,title_options,options_source_type,fmod_id) {
			    if(options_source_type == 'none'){
			    	 toggleOnOffFMODRow(fmod_id);
    			     return;
			    }
			    //If options_source_type == detour, then: ForcedMod  [--] ForceModDetourOption | price
				var title_id = title_obj.id;
				var select_id = title_id + "_selector";
				var value_id = title_id + "_value";
				var opts = "";
				var jtitle_obj = $(title_obj);
				var jmod_tab = getJModifierTab(jtitle_obj);

				opts += "<select id='" + select_id + "''>";
				for(var n=0; n<title_options.length; n++) {
					var full_title_option_val = title_options[n];
					var title_parts = full_title_option_val.split('|');
					var show_title_option_val;
					if(options_source_type == 'detour'){
						var title_and_forced_mod = title_parts[0];
						var titleAndForcedModArr = title_and_forced_mod.split('[--]');
						//It has been made impossible to add any stying to the text.
						//Will work on that last.
						//show_title_option_val = titleAndForcedModArr[0] + '<span>' + titleAndForcedModArr[1] + '</span>';
						if(titleAndForcedModArr[1] != '')
							show_title_option_val = titleAndForcedModArr[0] + ' (' + titleAndForcedModArr[1] + ')';
						else
							show_title_option_val = titleAndForcedModArr[0];
						//Have to override
						full_title_option_val = show_title_option_val+'|'+title_parts[1];
					}else{
						show_title_option_val = title_parts[0];
					}
					//
					opts += "<option value='" + full_title_option_val + "'";
					if(document.getElementById(value_id).value==full_title_option_val) opts += " selected";
					opts += ">";
					opts += show_title_option_val;
					opts += " - " + (title_parts[1] * 1).toFixed(2);
					opts += "</option>";
				}
				opts += "</select>";

				// select the modifier tab if it isn't already selected
				if (!jmod_tab.hasClass('selected')) {
					var jmidRadio = jmod_tab.find(".portions.whole").find("input[type=radio]");
					selectPortion(jmidRadio);
				}

				jtitle_obj.find("font").hide();
				jtitle_obj.append(opts);

				if (options_source_type != 'none') {
					document.getElementById(select_id).focus();
				}
				var onchangeFunc = function() {
					mod_title_changed(title_id, value_id, $("#"+select_id).val());
					var jportion = jmod_tab.find(".portions.selected");
					if (jportion.length == 0)
						jportion = jmod_tab.find(".portions");
					var jradio = jportion.find("input[type=radio]");
					var is_choice = jradio.hasClass('is_choice');
					updateCategoryTextByModTab(jmod_tab, is_choice);
					sendPortionToWrapper(jmod_tab, jradio, is_choice);
				}

				$("#"+select_id).change(onchangeFunc);
				$("#"+select_id).blur(onchangeFunc);

				if (options_source_type == 'none') {
					mod_title_changed(title_id, value_id, $("#"+select_id).val());
				}
			}

			/**
			 * Draw selected modifier title after editing the heaviness, including the heaviness description
			 * @param string			title_id	Id for the container where the title and price are drawn
			 * @param string			value_id	A hidden field where the current title and price are stored
			 * @param string			new_value	Value to set
			 * @return N/A
			 */
			function mod_title_changed(title_id,value_id,new_value) {
				document.getElementById(value_id).value = new_value;

				// get the new title and cost
				var new_value_parts = new_value.split('|');
				var show_new_value = new_value_parts[0];
				var new_price = (new_value_parts[1] * 1).toFixed(2);

				// set the new title and cost, and remove the select
				var jtitleContainer = $("#"+title_id);
				var jmodifier = getJModifierTab(jtitleContainer);
				var jselect = jtitleContainer.find("select");
				jtitleContainer.find(".modifier_title").text(show_new_value);
				jmodifier.find("input[name=heaviness_modifier_price]").val(new_price);
				jtitleContainer.find(".modifier_price").text(calculatePriceByPortion(jmodifier));
				jtitleContainer.find("font").show();
				if (jselect.length > 0 && !jselect.hasClass("removed")) {
					jselect.addClass("removed");
					jselect.remove();
				}
			}

			/**
			 * Smoothly scrolls the window to the destination_scroll position with the given delay (don't pass in start_scroll or start_time).
			 * @param  integer           destination_scroll The position to scroll the window to.
			 * @param  integer           delay              The time it takes to scroll the window to the given position.
			 * @param  internal variable start_scroll       For internal use
			 * @param  internal variable start_time         For internal use
			 * @return N/A                                  No return value
			 */
			function smoothScrollPage(destination_scroll, delay, start_scroll, start_time) {

				// unset the previous timeout
				if (typeof(window.smoothScrollPageTimeout) !== 'undefined')
					clearTimeout(window.smoothScrollPageTimeout);

				// get some initial values
				var jwindow = window.jwindow;
				var curr_time = (new Date()).getTime();

				// find the starting values
				if (typeof(start_scroll) == 'undefined' || typeof(start_time) == 'undefined') {
					start_scroll = jwindow.scrollTop();
					start_time = curr_time;
				}

				// get the destination and current values
				var dest_time = start_time+delay;
				var progress = (curr_time-start_time)/delay;
				for (var i = 0; i < 3; i++)
					progress = Math.cos(progress*3.14159+3.14159)*0.5+0.5;
				progress = (curr_time > dest_time) ? 1 : progress;
				var curr_scroll = (destination_scroll - start_scroll)*progress + start_scroll;

				// scroll the page
				jwindow.scrollTop(curr_scroll);
				if (progress < 1)
					window.smoothScrollPageTimeout = setTimeout('smoothScrollPage('+destination_scroll+','+delay+','+start_scroll+','+start_time+')', 30);
			}

			// sets the page and scrolls to that pagebreak
			function setPage(pageNum) {

				window.currentPage = pageNum;
				var jpageBreak = $(".page_break.break_"+pageNum);

				smoothScrollPage(jpageBreak.offset().top, 300);
				//window.jwindow.scrollTop(jpageBreak.offset().top);
			}

			setTimeout(function() {
				$(document).ready(function() {

					var inputs = $("input[type=radio]");
					var choices = [];
					var categories = [];
					window.currentPage = 1;
					window.jwindow = $(window);

					var perform_auto_select = true;
					if (previouslySetModifiers)
					{
						if (previouslySetModifiers.constructor === Array)
						{
							if (previouslySetModifiers.length > 0)
							{
								perform_auto_select = false;
							}
						}
					}

					if (perform_auto_select)
					{
						// find each individual choice
						$.each(inputs, function(k, input) {
							var jinput = $(input);
							var input_name = jinput.prop('name');
							if (jinput.hasClass('is_choice')) {
								var in_choice = $.grep(choices, function(choice_name) {
									return (input_name == choice_name);
								});
								if (in_choice.length == 0)
									choices.push(input_name);
							}
						});

						// select one of each of the choices
						i_selection_timeout = 0;
						$.each(choices, function(k, choice_name) {
							var radios = $("input[name="+choice_name+"]");
							var jradio = $(radios[0]);
							setTimeout(function() {
								selectPortion(jradio);
							}, i_selection_timeout);
							i_selection_timeout += 200;
						});
					}

					for (var i = 1; i <= <?php echo get_categories_max_page(); ?>; i++) {
						// size the columns on each page appropraitly
						sizeColumnsOnPage(i);
						// ballance out the categories across the columns (vertically on each page)
						ballanceCategories(i);
						// check for any pages that have empty columns but some categories
						checkForEmptyColumns(i);
					}

					// expand the first category
					expandCategory(<?php
						$a_categories = get_categories();
						echo $a_categories[0]['id'];
					?>)

					// initialize the text for all categories
					initializeText();

					// track the page window
					window.jwindow.scroll(function() { computeCurrentPage(); });
				});
			}, 500);

			/**
 			 * Function used to check user selected forced modifier based on given choice limit
 			 */
			function checkChoiceCountMod() {
				var len = document.getElementsByClassName('fmodifier_choice').length;
				var addOrder = true;
 				for(i = 0; i < len; i++) {
 					var chooseLimit = $('#fmodifier_choice_'+i).val();
 					if (chooseLimit != 'undefined') {
 						var selLen = $("input[type='radio'].choiceRadio_"+i+":checked").length;
 						if (selLen != chooseLimit && chooseLimit > 0) {
							var title = 'Alert';
							var message = 'Required fields not selected\nSelect your options to continue';
							window.location = "_DO:cmd=alert&title=" + encodeURIComponent(title) + "&message=" + encodeURIComponent(message);
							addOrder = false;
							break;
 						}
 					}
				 }
				 if (addOrder) {
					window.location = "_DO:cmd=send_js&c_name=pizza_mod_wrapper&js=js=apply_mods()";
				 }
 			}
		</script>
	</head>

<?php
	$a_categories_by_page = array();
	for ($i = 1; $i <= get_categories_max_page(); $i++) {
		$s_forced_modifiers_categories = draw_categories($i);
		$a_categories_by_page[$i] = $s_forced_modifiers_categories;
	}

	/**
	 * Because the entire pizza is one "item", the item id and possibly the item cost should be passed through $_POST vars.
	 *     Get the item cost.
	 * @return float The cost of the item.
	 */
	function get_item_cost() {

		// return the price, if it's set
		return (float)$_POST['item_price'];

		// get the id of the menu item
		$item_id = $_POST['item_id'];
		$item_query = lavu_query("SELECT `price` FROM `menu_items` WHERE `id`='[id]'",
			array('id'=>$item_id));

		// get the cost of the item
		$item_cost = 0;
		if ($item_query !== FALSE && mysqli_num_rows($item_query) > 0) {
			$item_read = mysqli_fetch_assoc($item_query);
			$item_cost = (float)$item_read['price'];
		}

		return $item_cost;
	}

	/**
	 * Draws the categories that are to appear on the given page.
	 * @param  int $i_page The page number to draw.
	 * @return string      The HTML to draw for that category.
	 */
	function draw_categories($i_page) {

		// get the categories
		$a_forced_modifiers_categories = get_categories();

		//INSERTED BY BRIAN D. WE NOW NEED ALL DETOUR INFORMATION FOR ALL USED CATEGORIES
		global $forcedModifierInfoArr;
		$forcedModifierInfoArr = getAllReferencedDetoursFromCategoriesArr($a_forced_modifiers_categories);
		//END INSERT

		// create the categories and their mods
		$i_count = 0;
		$s_forced_modifiers_categories = '';
		foreach($a_forced_modifiers_categories as $a_category) {
			if ($a_category['page'] == $i_page) {
				$s_forced_modifiers_categories .= draw_category($a_category);
				$i_count++;
			}
		}

		// draw the javascript to perform price mods on portions
		$s_forced_modifiers_categories .= '
			<script type="text/javascript">
				portion_mods = {
					left: { title: "Left", value: "left", price_mod: 1 },
					right: { title: "Right", value: "right", price_mod: 1 },
					whole: { title: "Whole", value: "whole", price_mod: 1 },
				};
			</script>';

		return $s_forced_modifiers_categories;
	}

	function draw_category(&$a_category) {
		$s_category_id = $a_category['id'];
		$s_category_title = $a_category['title'];
		$s_category_type_option = $a_category['type_option'];
		$s_multi_column = ($a_category['multi-column'] > 0 ? "allowMultiColumn" : "");
		$s_retval = '';
		$s_retval .= '
			<div class="category_container" style="margin:0; padding:0; box-shadow:gray 2px 2px 8px 1px;">
				<div id="category_'.$s_category_id.'" class="category_tab expanded '.$s_multi_column.'" onmousedown="$(this).addClass(\'pressed\');" onmouseout="$(this).mouseup();" onmouseup="$(this).removeClass(\'pressed\');" ontouchstart="touchStart(event);" '.je('touchend').'="if (touchSuccess(event)) {expandCategory(\''.$s_category_id.'\');}">
					<font class="category_tab_contents">
						'.$s_category_title.'
						<font class="category_extra_text"></font>
					</font>
				</div>';
				if ($s_category_type_option > 0) {
					$s_retval .= '
						<div id="category_choice_'.$s_category_id.'" class="choice_tab expanded">
							<font class="choice_tab_contents">
								Choose '.$s_category_type_option.'
							</font>
						</div>';
				}
		$s_retval .= '<input type="hidden" value="'.$s_category_type_option.'" id="fmodifier_choice_'.$s_category_id.'" name="fmodifier_choice" class = "fmodifier_choice" />';
		$s_retval .= draw_mods($a_category);
		$s_retval .= '
			</div>';
		return $s_retval;
	}

	// Bastard array needs definition
	// From what I could track down $a_category is an entry in config, where setting=pizza_mods
	// `$a_category` has the following keys:
	// id -- defined in pizza_mod_shared_funcs.php func-getCategories()
	// --The rest are defined in pizza_mod_shared_funcs.php func-build_forced_modifier_categories.php (called by getCategories)
	// title
	// mods (forced modifiers)
	// choice (whether the forced modifier list was a 'choice' or not)
	// page (value8 of config where setting=pizza_mods)
	// multi-column - value9 of config where setting=pizza_mods
	// partial_serving_pricing ... ln 66 in pizza_mod_shared_funcs.php
	function draw_mods(&$a_category) {
		global $cap_height;
		global $cap_padding;
		global $cat_height;

		$s_category_id = $a_category['id'];
		$s_category_page = $a_category['page'];
		$s_category_order = $a_category['order'];
		$s_mods_id = 'catmods_'.$s_category_id;
		$a_mods = $a_category['mods'];

		//Edited by Brian D. now optin to disable portion.
		//original:
		//$b_portions = !$a_category['choice'];
		//new:
		$isDisablingPortions = getConfDisablePortionsSetting();
		$b_portions = !$a_category['choice'];
		if($isDisablingPortions){
		    //This is gross, but I have no alternative.
    		echo "<script type='text/javascript'> var isPortionDisabled = true;</script>";
		}else{
    		echo "<script type='text/javascript'> var isPortionDisabled = false;</script>";
		}
		//End modification.
		$a_heaviness_mods = array(
			array('title'=>'Light', 'value'=>'light', 'price_mod'=>1, 'default'=>FALSE),
			array('title'=>'Normal', 'value'=>'normal', 'price_mod'=>1, 'default'=>TRUE),
			array('title'=>'Extra', 'value'=>'extra', 'price_mod'=>2, 'default'=>FALSE),
		);

		// create the mods container
		$s_retval = '';
		$s_retval .= '
			<div id="'.$s_mods_id.'" class="modifier_list">
				<input type="hidden" name="full_height" value="'.(count($a_mods)*$cat_height+$cap_height+$cap_padding+5).'" />';

		//For each forced modifier in the category.
		global $seq;
		foreach($a_mods as $a_mod) {

			//Inserted by Brian D.
			// now we get the detour information for the current mod
			global $forcedModifierInfoArr;//Set in function drawCategories where the getCategories is called.
			$detourVal = $a_mod['detour'];
//error_log("Current forced_modifier: ". $a_mod['title'].' has detour:'.$a_mod['detour']);
			$forcedModsInThisForcedModDetourInOrder = array();
			if(strlen($detourVal) && $detourVal[0] == 'l'){
				$index = substr($detourVal, 1);
				$forcedModsInThisForcedModDetourInOrder = $forcedModifierInfoArr[$index];
			}
//error_log("Number of forced mods in detour:$detourVal is:".count($forcedModsInThisForcedModDetourInOrder));
			// End Insert.
			// get the mod data
			$s_mod_seq = $seq;  
			$s_mod_title = $a_mod['title'];
			$i_mod_order = (int)$a_mod['_order'];
			$s_mod_id = $a_mod['id'];
			$s_mod_list_id = $a_mod['list_id'];
			$a_decimal_data = get_decimal_data();
			$s_mod_cost = number_format((float)$a_mod['cost'], (int)$a_decimal_data['disable_decimal'], $a_decimal_data['decimal_char'], ',');
			$unavailable_until = $a_mod['unavailable_until'];
			$soldout = ($unavailable_until > 0) ? 'soldout' : '';
			//get Happyhours details.
			$s_happyhours = '';
			if ($a_mod['happyhours_id'] || $s_mod_list_id) {
				$s_happyhours = getHappyhoursInfo($a_mod['happyhours_id'], $s_mod_list_id);
			}
			// check if the portions options should be drawn
			if ($a_category['choice']) {
				$s_radio_name = 'mod_choice_'.$s_category_id;
				$s_selections = draw_hidden_radio($s_radio_name, 'whole', 'whole', 'is_choice', '1', $s_mod_id, '', $s_category_id, $soldout);
			}
			else if($isDisablingPortions && !empty($forcedModsInThisForcedModDetourInOrder)){// Added by Brian D.
				$s_radio_name = 'mod_checklist_'.$s_category_id.'_'.$s_mod_id;
				$s_selections = draw_hidden_radio($s_radio_name, 'whole', 'whole', 'is_checklist', '1', $s_mod_id, '', $s_category_id, $soldout);
			}
			else if($isDisablingPortions && empty($forcedModsInThisForcedModDetourInOrder)){// Added by Brian D.
				$s_radio_name = 'mod_checklist_'.$s_category_id.'_'.$s_mod_id;
				$s_selections = draw_hidden_radio($s_radio_name, 'whole', 'whole', 'is_checklist', '1', $s_mod_id, '', $s_category_id, $soldout);
			}
			else {
				$s_radio_name = 'mod_checklist_'.$s_category_id.'_'.$s_mod_id;
				$s_selections = draw_hidden_radio($s_radio_name, 'left', 'left', 'is_checklist', $a_category['partial_serving_pricing'], $s_mod_id, '', $s_category_id, $soldout).'
						'.draw_hidden_radio($s_radio_name, 'whole', 'whole', 'is_checklist', '1', $s_mod_id, '', $s_category_id, $soldout).'
						'.draw_hidden_radio($s_radio_name, 'right', 'right', 'is_checklist', $a_category['partial_serving_pricing'], $s_mod_id, '', $s_category_id, $soldout);
			}

			//  ontouchstart="touchStart(event);" '.je('touchend').'="if (touchSuccess(event)) { mod_title_touch(this,new Array(\"$s_mod_title\",\"Light $s_mod_title\",\"Extra $s_mod_title\")); }"

			// create each modifier
			$isDisablingDefaultHeaviness = getConfDisableDefaultHeaviness();
			$mod_select_id = trim('mod_select_'.$s_category_id.'-'.$s_mod_list_id.'-'.$s_mod_id);
			$i_text_height = 44;
			$i_text_top = ($cat_height - $i_text_height - 2)/2;

			if ($unavailable_until > 0) {
				$s_touchend_text = '';
			}
			else if ($a_category['choice']) {
				//echo "<script type='text/javascript'> alert('$s_radio_name'); </script>";
				$s_touchend_text = 'ontouchstart="touchStart(event);" '.je('touchend').'="if (touchSuccess(event)) { selectRadio($(this).parent(),\''.$s_radio_name.'\',\'whole\'); }"';
			} else {
				//Modified by Brian D.
				//Here we load the detour as the quantification options if it exists, else we load what was default.
				$s_touchend_text = '';
				if(!empty($forcedModsInThisForcedModDetourInOrder)){
					//\''.$s_mod_title.'|'.$s_mod_cost.'\',\'Light '.$s_mod_title.'|'.$s_mod_cost.'\',\'Extra '.$s_mod_title.'|'.($s_mod_cost * 2).'\')
					//We include the original as the first entry.
					$mod_title_touch_inArrayStr = "'$s_mod_title".'[--]'."|".rtrim($s_mod_cost, "0")."'";
					$i = 0; $count = count($forcedModsInThisForcedModDetourInOrder);
					foreach($forcedModsInThisForcedModDetourInOrder as $currForcedMod){
						$mod_title_touch_inArrayStr .= ',';
						$mod_title_touch_inArrayStr .= "'" . $s_mod_title . '[--]' . $currForcedMod['fm_title'] . '|' . $currForcedMod['fm_cost'] . "'";
						//if(++$i < $count)
					}
//error_log("JS array:".$mod_title_touch_inArrayStr);
					$s_touchend_text = 'ontouchstart="touchStart(event);" '.
					       je('touchend').'="if (touchSuccess(event)) { mod_title_touch(this,new Array('.
					       $mod_title_touch_inArrayStr.'),\'detour\',\''.$s_mod_id.'\'); }"';
				}else{
					$sourceArg = $isDisablingDefaultHeaviness ? 'none' : 'default_heaviness';
					if($isDisablingDefaultHeaviness){
    					$s_touchend_text = 'ontouchstart="touchStart(event);" '.
    					je('touchend').'="if (touchSuccess(event)) { mod_title_touch(this,new Array(\''.$s_mod_title.'|'.rtrim($s_mod_cost, "0").'\'),\''.$sourceArg.'\',\''.$s_mod_id.'\'); }"';
					}else{
    					$s_touchend_text = 'ontouchstart="touchStart(event);" '.
    					je('touchend').'="if (touchSuccess(event)) { mod_title_touch(this,new Array(\''.$s_mod_title.'|'.
    					rtrim($s_mod_cost, "0").'\',\'Light '.$s_mod_title.'|'.rtrim($s_mod_cost, "0").'\',\'Extra '.
    					$s_mod_title.'|'.rtrim((string)($s_mod_cost * 2), "0").'\'),\''.$sourceArg.'\',\''.$s_mod_id.'\'); }"';
					}

				}
				//$s_touchend_text = 'ontouchstart="touchStart(event);" '.je('touchend').'="if (touchSuccess(event)) { mod_title_touch(this,new Array(\''.$s_mod_title.'|'.$s_mod_cost.'\',\'Light '.$s_mod_title.'|'.$s_mod_cost.'\',\'Extra '.$s_mod_title.'|'.($s_mod_cost * 2).'\')); }"';
			}


			//toggleOnOffFMODRow("'.$s_mod_id.'")
			$touchEventOnOverlayTouchDiv = 'ontouchstart="touchStart(event);" '.je('touchend').'="if (touchSuccess(event, $( $(this).children(\'[type=radio]\')[0] ))) { toggleOnOffFMODRow(\''.$s_mod_id.'\'); }"';
			$styleOnOverlayTouchDiv = $a_category['choice'] ?
				"style='display:inline-block;position:absolute;left:0px;width:100%;height:".$i_text_height."px;'" :
				"style='display:inline-block;position:absolute;left:200px;width:190px;height:".$i_text_height."px;'";
			//$onclick = 'onclick="toggleOnOffFMODRow(\''.$s_mod_id.'\');"';
			$s_retval .= '
				<div id="modifier_'.$s_mod_id.'" class="modifier_tab" ">
					<input type="hidden" value="'.$s_mod_seq.'" name="forced_modifier_sequence" />
					<input type="hidden" value="'.$s_category_page.'" name="category_page" />
					<input type="hidden" value="'.$s_category_order.'" name="category_order" />
					<input type="hidden" value="'.$s_category_id.'" name="category_id" />
					<input type="hidden" value="'.$s_mod_list_id.'" name="forced_modifier_list_id" />
					<input type="hidden" value="'.$s_mod_id.'" name="forced_modifier_id" />
					<input type="hidden" value="'.$i_mod_order.'" name="forced_modifier_order" />
					<input type="hidden" value="'.$s_happyhours.'" name="forced_modifier_happyhours" />
					<input type="hidden" value="'.$s_mod_title.'|'.$s_mod_cost.'" name="'.$mod_select_id.'_value" id="'.$mod_select_id.'_value" />
					<div class="modifier_text" id="'.$mod_select_id.'" style="z-index:200; top:'.$i_text_top.'px; width:200px; height:'.$i_text_height.'px" '.$s_touchend_text.'>
						<input type="hidden" name="original_modifier_title" value="'.str_replace('"', '---double quote---', $s_mod_title).'" />
						<input type="hidden" name="original_modifier_price"  value="'.$s_mod_cost.'" />
						<input type="hidden" name="heaviness_modifier_price" value="'.$s_mod_cost.'" />
						<font class="modifier_title '.$soldout.'">'.$s_mod_title.'</font>
						<font class="modifier_price">'.$s_mod_cost.'</font>
					</div>'.

					"<div $styleOnOverlayTouchDiv $touchEventOnOverlayTouchDiv> </div>"
					.
					//<div class="click_me" style="position:absolute; top:'.$i_text_top.'px; left:0px; width:422px; height:'.$i_text_height.'px;" ontouchstart="touchStart(event);" '.je('touchend').'="if (touchSuccess(event)) { selectRadio($(this).parent(),\''.$s_radio_name.'\',\'whole\'); }">&nbsp;</div>
					'
					<div class="modifier_options" style="top:'.$i_text_top.'px;">
						'.$s_selections.'
					</div>
				</div>';

				$seq++;
		}
		//$s_retval .= '<div class="modifer_list_cap">&nbsp;</div>';
		$s_retval .= '
			</div>';

		return $s_retval;
	}

	function draw_hidden_radio($s_name, $s_value, $s_class, $s_extra_class, $s_price_mod, $s_mod_id, $additional_inline_javascript, $s_category_id = '', $soldout='') {
		$spanID = 'button_span_'.$s_mod_id;
		$radioID = 'radio_in_button_span_'.$s_mod_id;
		$s_extra_class = $s_extra_class . ' choiceRadio_'.$s_category_id;
		//$style = $fillWidth ? "style='width:100%; background-color:black;'" : "";
		//$style = "style=\"width:100%; background-color:black;\"";
		//$style = '';
		if ($soldout != '') {
			$draw_radio ='<span id="'.$spanID.'" class="portions '.$s_class.'">
							<input type="radio" style="display:none;" class="'.$s_extra_class.'"></input>
						  </span>';
		} else {
			$draw_radio = '<span id="'.$spanID.'" '.
		                  '" ontouchstart="touchStart(event);" '.
					         je('touchend').'="if (touchSuccess(event, $( $(this).children(\'[type=radio]\')[0] ))) { selectPortion(   $( $(this).children(\'[type=radio]\')[0] )   );'.
					         $additional_inline_javascript.
					         '}" class="portions '.$s_class.'">
						<input type="radio" id="'.$radioID.'" name="'.$s_name.'" value="'.$s_value.'" style="display:none;" class="'.$s_extra_class.'"></input>
						<input type="hidden" name="pricemod" value="'.$s_price_mod.'"></input>
					</span>';
				}
		return $draw_radio;
	}

	/**
	 * This function is used to get happyhours information.
	 * @param id $happyHoursId
	 * @return string happyhours details.
	 */
	function getHappyhoursInfo($happyHoursId, $modListId) {
		$data = "";
		if($happyHoursId == 0) {
			$force_query = lavu_query('SELECT happyhours_id FROM `forced_modifier_lists` WHERE `id` = "[1]" AND `_deleted` != "1"', $modListId);
			$force_row = mysqli_fetch_assoc($force_query);
			$happyHoursId = $force_row['happyhours_id'];
		}
		$query = lavu_query('SELECT start_time, end_time, weekdays, property, extended_properties  FROM `happyhours` WHERE `id` = "[1]" AND `_deleted` != "1"', $happyHoursId);
		$row = mysqli_fetch_assoc($query);
		if (mysqli_num_rows($query) > 0) {
			$data = $row[start_time].":".$row[end_time].":".$row[weekdays].":".$row[property].":".$row[extended_properties];
		}
		return $data;
	}
?>
	<body style="overflow-y:auto;">

	<div id="pages">
		<?php
		foreach($a_categories_by_page as $i_page=>$s_forced_modifiers_categories) {
		?>
		<div id="page_<?php echo $i_page; ?>" style="display:table; opacity:0; padding-bottom:10px;" class="forced_modifier_list_container">
			<div class="page_break break_<?php echo $i_page; ?>" style="width:100%; height:1px; border:none; padding:0; margin:0 auto;"></div>
			<div class="column_container">
				<div class="column column_index_1" style="width:426px;">
					<input type="hidden" name="column_index" value="1" /><?php echo $s_forced_modifiers_categories; ?>

				</div>
				<?php
				for ($i_column_index = 2; $i_column_index <= $i_num_columns; $i_column_index++) {
				?>
				<div class="column_spacer column_spacer_index_<?php echo $i_column_index-1; ?>" style="width:33px;">
					<input type="hidden" name="column_spacer_index" value="<?php echo $i_column_index-1; ?>" />
				</div>
				<div class="column column_index_<?php echo $i_column_index; ?>" style="width:426px; margin-right:0px; padding-right:0px;">
					<input type="hidden" name="column_index" value="<?php echo $i_column_index; ?>" />
				</div>
				<?php
				}
				?>
			</div>
		</div>
		<?php
		}
		?>
	</div>

	</body>
</html>
