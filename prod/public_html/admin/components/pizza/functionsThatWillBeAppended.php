<?php

	//Possible config settings.
	//Return true or false.
	function getConfBackgroundEmptyImageSetting(){
		static $value = null;
		if($value !== null)
			return $value;
		$result = lavu_query("SELECT * FROM `config` WHERE `setting`='build_your_own_background_image_empty'");
		if(mysqli_num_rows($result) == 0){
			$value = false;
		}
		else{
			$row = mysqli_fetch_assoc($result);
			$value = $row['value'];
		}
		return $value;
	}

	//Returns bool true or false.
	function getConfDisablePortionsSetting(){
		static $value = null;
		if($value !== null)
			return $value;
		$result = lavu_query("SELECT * FROM `config` WHERE `setting`='build_your_own_disable_portions'");
		if(mysqli_num_rows($result) == 0){
			$value = false;
		}
		else{
			$row = mysqli_fetch_assoc($result);
			$value = strtolower($row['value']) == 'true' ? true : false;
		}
		return $value;
	}
	function getConfDisableDefaultHeaviness(){
		static $value = null;
		if($value !== null)
			return $value;
		$result = lavu_query("SELECT * FROM `config` WHERE `setting`='build_your_own_disable_default_heavyness'");
		if(mysqli_num_rows($result) == 0){
			$value = false;
		}else{
			$row = mysqli_fetch_assoc($result);
			$value = strtolower($row['value']) == 'true' ? true : false;
		}
		return $value;
	}






	// Searches for any pertinent detour attached to an item and adds it to a 2D array
	// where the first key is the:        id of the force_modifier_lists row,
	// and the second key is the:         id of the force_modifer row, itself.
	function getAllReferencedDetoursFromCategoriesArr($all_categories, $useCaching = true){

		//We only allow this function to query once, and we send the cash back.
		static $cacheReturnVal = null;
		if($cacheReturnVal != null && $useCaching){
//error_log( 'Returning cached value' );
			return $cacheReturnVal;
		}

		//Extract just the `forced_modifiers` rows from the category object.
		$modifierRowsArr = array();
		foreach($all_categories as $currCategory){
			$modifierRowsArr[] = $currCategory['mods'];
		}

		//Extract all detours from the forced modifiers.
		$allDetourIDs = _getAllReferencedDetoursFromArrayOfForcedModifierRows($modifierRowsArr);//Used to build query.

		//Query would crash and be meaningless if there's no detours within reference.
		if(empty($allDetourIDs))
			return array();

		//We 2D array as such: [force_modifier_list_id][all forced_modifiers_in_this_list]
		//for each detour, and we return it.
		$forceModListsIdEmbedsItsForceMods = array();
		$joinedQueryRows = _getAllRows_forcedModifierLists_joinedToTheirForcedMods($allDetourIDs);
//error_log("Number of rows returned: ".count($joinedQueryRows));
		foreach($joinedQueryRows as $currRow){
			//We index by the forced_modifier_lists id.
			if(!isset($forceModListsIdEmbedsItsForceMods[$currRow['fml_id']]))
				$forceModListsIdEmbedsItsForceMods[$currRow['fml_id']] = array();
			$forceModListsIdEmbedsItsForceMods[$currRow['fml_id']][$currRow['fm_id']] = $currRow;
		}

		//Static caching of query...
		$cacheReturnVal = $forceModListsIdEmbedsItsForceMods;

//error_log("We have embedded array: " . print_r($forceModListsIdEmbedsItsForceMods,1));
		return $forceModListsIdEmbedsItsForceMods;
	}
	function _getAllReferencedDetoursFromArrayOfForcedModifierRows($modifierRowsArr){
		$allDetourIDs = array();
		foreach($modifierRowsArr as $currRowArr){
			foreach($currRowArr as $currRow){
				$currDetour = $currRow['detour'];
				if($currDetour[0] == 'l'){
					$forced_modifier_list_id = substr($currDetour, 1);
					$allDetourIDs[ $forced_modifier_list_id ] = ConnectionHub::getConn('rest')->escapeString( $forced_modifier_list_id );
				}
			}
		}
		return $allDetourIDs;
	}
	function _getAllRows_forcedModifierLists_joinedToTheirForcedMods($allDetourIDs){
		$inExpression = '(' . implode(',',$allDetourIDs) . ')';
		$queryStr = <<<MYSQL
			SELECT `forced_modifier_lists`.`title` AS fml_title,
				   `forced_modifiers`.`title` AS fm_title,
				   SUBSTRING(`forced_modifiers`.`detour`, 2) AS `fm_detour`,
				   `forced_modifier_lists`.`type` AS fml_type,
				   `forced_modifier_lists`.`id` AS fml_id,
				   `forced_modifiers`.`id` AS fm_id,
				   `forced_modifiers`.`cost` AS fm_cost,
				   `forced_modifiers`.`list_id` AS fm_list_id
			FROM `forced_modifier_lists`
			JOIN `forced_modifiers` ON `forced_modifier_lists`.`id`=SUBSTRING(`forced_modifiers`.`list_id`,1)
			WHERE `forced_modifier_lists`.`id` IN $inExpression
				  AND `forced_modifiers`.`_deleted`='0'
				  AND `forced_modifier_lists`.`_deleted`='0'
			ORDER BY `forced_modifier_lists`.`id`,`forced_modifiers`.`_order`
MYSQL;
		$result = lavu_query($queryStr);
		if(!$result){ error_log("MYSQL ERROR IN ".__FILE__." mysql_error: ".lavu_dberror()); }
		$allRowsArr = array();
		while($currRow = mysqli_fetch_assoc($result)){
			$allRowsArr[] = $currRow;
		}
		return $allRowsArr;
	}


?>
