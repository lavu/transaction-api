<?php
	$dont_draw_pizza_settings = TRUE;
	$dev_dir = strstr(__FILE__, '/dev/')?'/..':'';
	//error_log("IMPORT:" . $s_root_dir.'cp/areas/pizza_settings.php');
	require_once dirname(__FILE__) . $dev_dir . '/../../cp/areas/pizza_settings.php';

	// require_once($s_root_dir.'cp/areas/pizza_settings.php');

	function build_forced_modifier_categories() {

		// global settings
		global $locationid;
		global $build_forced_modifier_categories_returned_value;
		if (!isset($locationid))
			$locationid = 1;
		if (isset($build_forced_modifier_categories_returned_value) && isset($build_forced_modifier_categories_returned_value[$locationid]))
			return $build_forced_modifier_categories_returned_value[$locationid];

		// get the creator settings
		$s_tablename = '';
		$s_filter_by = '';
		$s_order_by = '';
		$i_creator_id = PizzaCreatorSettings::creator_name_to_id($_REQUEST['cmd']);
		PizzaCreatorSettings::set_forced_mods_selectors($i_creator_id, $s_tablename, $s_filter_by, $s_order_by);

		// load the data
		$a_retval = array();
		$a_configs = ConnectionHub::getConn('rest')->getDBAPI()->getAllInTable($s_tablename, NULL, FALSE, array('whereclause'=>"WHERE {$s_filter_by} AND `_deleted`='0'", 'orderbyclause'=>"ORDER BY `{$s_order_by}` ASC"));

		// build the categories
		foreach($a_configs as $a_config) {
			// get the title and index of the list
			$s_title = $a_config['value'];
			$i_list_index = count($a_retval);

			// get the forced modifier list
			$i_forced_mod_list_id = (int)$a_config['value2'];
			$a_forced_mod_lists = ConnectionHub::getConn('rest')->getDBAPI()->getAllInTable('forced_modifier_lists', array('id'=>$i_forced_mod_list_id), FALSE);
			$a_forced_mod_list = $a_forced_mod_lists[0];
			if ((int)$a_forced_mod_list['_deleted'] != 0) {
				lavu_query("UPDATE `config` SET `_deleted`='1' WHERE `setting`='pizza_mods' AND `value2`='[id]'", array('id'=>$i_forced_mod_list_id));
				if (ConnectionHub::getConn('rest')->affectedRows() > 0) {
					return build_forced_modifier_categories();
				}
			}

			// get the forced modifiers
			$a_forced_modifiers = ConnectionHub::getConn('rest')->getDBAPI()->getAllInTable('forced_modifiers', array('list_id'=>$i_forced_mod_list_id, '_deleted'=>0), FALSE, array( 'orderbyclause' => 'ORDER BY `_order` ASC' ));

			// create the new category in the list
			$a_retval[$i_list_index] = array();
			$a_retval[$i_list_index]['title'] = $s_title;
			$a_retval[$i_list_index]['type_option'] = $a_forced_mod_list['type_option'];
			$a_retval[$i_list_index]['mods'] = array();
			$a_retval[$i_list_index]['choice'] = ($a_forced_mod_list['type'] == 'choice');
			$a_retval[$i_list_index]['page'] = (int)$a_config['value8'];
			$a_retval[$i_list_index]['order'] = (int)$a_config['value6'];
			$a_retval[$i_list_index]['multi-column'] = (int)$a_config['value9'];
			$a_retval[$i_list_index]['partial_serving_pricing'] = ($a_config['value10'] == "") ? 1.00 : (float)$a_config['value10'];

			// insert each modifier into the new category
			foreach($a_forced_modifiers as $a_forced_modifier) {
				$a_retval[$i_list_index]['mods'][] = $a_forced_modifier;
			}
		}

		$build_forced_modifier_categories_returned_value[$locationid] = $a_retval;

		return $a_retval;
	}

	function get_decimal_data() {
		// in case the location data can't be found
		$a_badval = array('disable_decimal'=>'2', 'decimal_char'=>'.');

		// load the location data and return it, if found
		// otherwise, return the badval
		$s_location_id = (isset($_POST['loc_id'])) ? $_POST['loc_id'] : '1';
		$decimal_query = lavu_query("SELECT `disable_decimal`,`decimal_char` FROM `locations` WHERE `id`='[id]'",
			array('id'=>$s_location_id));
		if ($decimal_query === FALSE || mysqli_num_rows($decimal_query) == 0)
			return $a_badval;
		return mysqli_fetch_assoc($decimal_query);
	}

	function draw_drawPrice_function() {
		$a_decimal_data = get_decimal_data();
		$precision = $a_decimal_data['disable_decimal'];
		$decimal_char = $a_decimal_data['decimal_char'];

		return "
		function drawPrice(fval) {
			var precision = $precision;
			var decimal_char = '$decimal_char';
			return toFixed(fval, precision, decimal_char);
		}

		// from http://stackoverflow.com/questions/2221167/javascript-formatting-a-rounded-number-to-n-decimals/2909252#2909252
		function toFixed(value, precision, decimal_char) {
		    var precision = precision || 0,
		    neg = value < 0,
		    power = Math.pow(10, precision),
		    value = Math.round(value * power),
		    integral = String((neg ? Math.ceil : Math.floor)(value / power)),
		    fraction = String((neg ? -value : value) % power),
		    padding = new Array(Math.max(precision - fraction.length, 0) + 1).join('0');

		    return precision ? integral + decimal_char +  padding + fraction : integral;
		}";
	}

	/**
	 * Get the forced modifier categories that apply to the pizza creator.
	 * @return array An array of forced mods in the form
	 *     array(
	 *         array(
	 *             'title'=>string, 'mods'=>db row from forced modifiers, 'choice'=>boolean either choice or list, 'page'=>int),
	 *             ...
	 *         )
	 *     )
	 */
	function get_categories() {

		global $a_get_categories_return_value;
		if (isset($a_get_categories_return_value))
			return $a_get_categories_return_value;

		// get the forced modifiers
		$a_forced_modifiers_categories = build_forced_modifier_categories();

		// standardize the data
		foreach($a_forced_modifiers_categories as $s_category_id=>$a_category) {
			$a_category['page'] = max((int)$a_category['page'], 1);
			$a_forced_modifiers_categories[$s_category_id]['page'] = $a_category['page'];
		}

		// assign ids
		$i_max_page = 0;
		$a_page_data = array();
		foreach($a_forced_modifiers_categories as $s_category_id=>$a_category) {
			$a_forced_modifiers_categories[$s_category_id]['id'] = $s_category_id;

			// get page data
			$i_page = $a_category['page'];
			$i_max_page = max($i_page, $i_max_page);
			if (!isset($a_page_data[$i_page]))
				$a_page_data[$i_page] = array('categories'=>array());
			$a_page_data[$i_page]['categories'][] = $s_category_id;
		}

		// condense the pages
		for ($i = 1; $i <= $i_max_page; $i++) {

			// check if there are any categories in the table
			// if so, continue
			if (isset($a_page_data[$i]) && count($a_page_data[$i]['categories']) > 0)
				continue;

			// find the next page with categories
			// if there is no next page, we're done condensing
			$i_next_page = $i;
			foreach($a_page_data as $i_page=>$a_data) {
				if (count($a_data['categories']) > 0)
					$i_next_page = max($i_page, $i_next_page);
				if ($i_next_page > $i)
					break;
			}
			if ($i_next_page <= $i)
				break;

			// condense the categories from the next page onto this page
			foreach($a_page_data[$i_next_page]['categories'] as $k=>$s_category_id) {
				$a_page_data[$i]['categories'][] = $s_category_id;
				$a_forced_modifiers_categories[$s_category_id]['page'] = $i;
				unset($a_page_data[$i_next_page]['categories'][$k]);
			}
		}

		$a_get_categories_return_value = $a_forced_modifiers_categories;
		return $a_forced_modifiers_categories;
	}

	/**
	 * Retrieves the maximum page number of the categories.
	 * @return int The maximum page number of the categories.
	 */
	function get_categories_max_page() {

		// check for a cached value
		global $i_get_categories_max_page_retval;
		if (isset($i_get_categories_max_page_retval))
			return $i_get_categories_max_page_retval;

		// get the data
		$a_forced_modifiers_categories = get_categories();
		$i_max_page = 1;
		foreach($a_forced_modifiers_categories as $a_category)
			$i_max_page = max($a_category['page'], $i_max_page);

		$i_get_categories_max_page_retval = $i_max_page;
		return $i_max_page;
	}

	// touch javascript events
	function je($s_type) {
		global $b_dev_components;

		if ($b_dev_components) {
			return 'onclick';
		} else {
			return "on{$s_type}";
		}
	}

	ob_start();
	?>
	<script type='text/javascript'>
		/**
		 * A precursor to touchEnd.
		 *     NOTE: MUST BE CALLED if touchEnd is to be trusted.
		 * @param  a javscript event event The event that was fired.
		 * @param  int               i     The index of the touch event status.
		 * @return N/A                     No return value.
		 */
		function touchStart(event, i){
			if (typeof(i) == 'undefined')
				i = 0;

			_start_x_loc_old[i] = _start_x_loc[i];
			_start_y_loc_old[i] = _start_y_loc[i];
			_start_x_loc[i] = event.touches[0].clientX;
			_start_y_loc[i] = event.touches[0].clientY;
			var between = (new Date().getTime())-_time_start[i];
			_time_start[i] = new Date().getTime();
			_is_double_tap[i] =false;
			if(between < 250)
				if( (Math.abs(_start_x_loc[i]-_start_x_loc_old[i]) < 20 && Math.abs(_start_y_loc[i]-_start_y_loc_old[i]) < 20 ))
					_is_double_tap[i]=true;
		}

		/**
		 * Sets the last scroll time of the window to try and reduce false positives on clicks.
		 */
		function setLastScroll() {
			_last_scroll_datetime = (new Date()).getTime();
		}
		_last_scroll_datetime = 0;

		/**
		 * Checks if a touchend event is the result of a touch or a scroll.
		 *     NOTE: touchStart MUST BE CALLED if touchEnd is to be trusted.
		 * @param  a javscript event event The event that was fired.
		 * @param  int               i     The index of the touch event status.
		 * @return string One of 'tap', 'double tap', 'scroll', or 'tap and hold'
		 */
		function touchEnd(event, i){
			if (typeof(i) == 'undefined')
				i = 0;

			// initialize some values
			event.preventDefault();
			var istouch = false;
			var isdouble = false;
			var ishold = false;
			var isscroll = false;

			// do some calculations
			if( Math.abs(_start_x_loc[i]-event.changedTouches[0].clientX) < 15 &&
			    Math.abs(_start_y_loc[i]-event.changedTouches[0].clientY) < 15 &&
			    (new Date()).getTime() - _last_scroll_datetime > 200 ){
				istouch = true;
				if(_is_double_tap[i])
					isdouble = true;
			}else{
				isscroll = true;
			}
			if ((new Date().getTime()) - _time_start[i] > 1000)
				ishold = true;

			// return a value
			var retval = '';
			if (isscroll)
				retval = 'scroll';
			else if (ishold)
				retval = 'tap and hold';
			else if (isdouble)
				retval = 'double tap';
			else
				retval = 'tap';
			return retval;
		}
		_time_start = [];
		_start_x_loc = [];
		_start_y_loc = [];
		_start_x_loc_old = [];
		_start_y_loc_old = [];
		_is_double_tap = [];

		/**
		 * Always returns true if $b_dev_components (in sa_cp/index.php?mode=manage_customers&submode=general_tools&tool=edit_coms)
		 * @param  javascript event event The event created by a click or touchend
		 * @return boolean                TRUE if $b_dev_components or it's a tap, FALSE otherwise
		 */
		function touchSuccess(event, jradio = null) {
			<?php
				global $b_dev_components;
				if ($b_dev_components)
					echo "return true;";
			?>

			var touch_result = touchEnd(event);
			if (touch_result == 'tap') {
				return checkChoiceLimit(jradio);
			}
			return false;
		}

		setTimeout(function() {
			$(document).ready(function() {
				$(window).scroll(setLastScroll);
			});
		}, 500);
		/**
 		 * This function is used to check choice limit of option which is set on cp setting
 		 * @param  jradio javascript dom element
 		*/
 		function checkChoiceLimit(jradio) {
 			if (jradio != null) {
 				var radioName = jradio.prop('name');
 				var radioNameArray = radioName.split('_');
 				var catid = radioNameArray[2];
 				if (catid != '') {
 					var choiceLimit = $('#fmodifier_choice_'+catid).val();
 					var selLen = $("input[type='radio'].choiceRadio_"+catid+":checked").length;
					 var rchecked = jradio.prop("checked");
					 var is_checklist = $("input[type='radio'].choiceRadio_"+catid).hasClass('is_checklist');
 					if (!rchecked && (choiceLimit == selLen) && choiceLimit > 0 && is_checklist) {
 						return false;
 					} else {
                         return true;
                     }
 				}
 			}
 			return true;
 		}
	</script>
	<?php
	$s_shared_javascript = ob_get_contents();
	ob_end_clean();
    require_once(dirname(__FILE__).'/functionsThatWillBeAppended.php');
?>
