<?php
	require_once(dirname(__FILE__) . "/../comconnect.php"); 
	$tab_name = reqvar("tab_name");
	if(reqvar("source_area")) $source_area = reqvar("source_area");
	else $source_area = "customers";
	if(reqvar("order_id")) $order_id = reqvar("order_id");
	else $order_id = "";
?>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Customer Mod Wrapper</title>

		<style type="text/css">
<!--
body {
	background-color: transparent;
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	-webkit-tap-highlight-color:rgba(0,0,0,0);
}

.style4 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #8896AD;
}
.style6 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 24px;
	font-weight: bold;
	color: #8896AD;
}
.style8 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 15px;
	font-weight: bold;
	color: #07142a;
}
-->
		</style>
		
	
<?php
	//require_once("../comconnect.php");
	if(reqvar("cm"))
	{
		$cancel_cmd = "cancel";
		$extra_vars = "&cm=1&cmd=".reqvar("cmd");
	}
	else
	{
		$cancel_cmd = "close_overlay";
		$extra_vars = "";
	}
	
	if( ($tab_name=="Schedule") || ($tab_name == "New Schedule") )
	{
		$bg = "images/win_overlay_3btn.png";
	}
	else
	{
		$bg = "images/win_overlay_2btn_short.png";
	}
?>	
		
<script type="text/javascript">


function doSearch(){
		//alert("hello this day");
		setval = document.getElementById("search_term").value; if(setval!="SEARCH" && setval!="") location = "_DO:cmd=load_url&c_name=med_customer_mod&f_name=medical/med_customer_search.php?mode=search<?php echo $extra_vars; ?>&source_area=<?php echo $source_area?>&order_id=<?php echo $order_id?>&search_term=" + setval + "&abc=123";
		
		return false;
}//doSearch()

/*function doSearch(){
		
		
		setval = document.getElementById("search_term").value; 
	alert("setval: " + setval);
		if(setval!="SEARCH" && setval!=""){
			 cmd = "_DO:cmd=load_url&c_name=med_customer_search&f_name=medical/med_customer_search.php?mode=search<?php echo $extra_vars; ?>&search_term=" + setval + "&abc=123";
			 alert("cmd: " + cmd);
			 	
		}
		return false;
}//doSearch()
	*/
</script>
		
		
	</head>

	<body>
	
		<table width="474" height="598" border="0" cellpadding="0" cellspacing="0" style="background:URL(<?php echo $bg?>); background-repeat:no-repeat; background-position:top center;">
			<tr>
				<td width="26" height="53">&nbsp;</td>
				<td width="422" height="53">
					<table width="428" border="0" cellpadding="6" cellspacing="0">
						
					<form name="submittingForm" method="post" action="submitFormAction.php" onSubmit="return doSearch();">
						
						<tr>
                        	<td width="46" align="center" onclick='location = "_DO:cmd=<?php echo $cancel_cmd?>";'><span class="style6"> X </span></td>
                            <td width="20">&nbsp;</td>
							<td width="320" align="left" valign="middle"><input id='search_term' class='style4' value='SEARCH' style='border:none; background: URL(win_overlay.png); width:270px' onfocus='if(this.value=="SEARCH") this.value = "";' onblur='if(this.value=="") this.value = "SEARCH"'></td>
							<td width="36" align="center"><a onclick='setval = document.getElementById("search_term").value; if(setval!="SEARCH" && setval!="") location = "_DO:cmd=load_url&c_name=med_customer_mod&f_name=medical/med_customer_search.php?mode=search<?php echo $extra_vars; ?>&source_area=<?php echo $source_area?>&order_id=<?php echo $order_id?>&search_term=" + setval + "&abc=123";'><span class="style6"> GO </span></a></td>
						</tr>
					</form>
					</table>
				</td>
				<td width="26" height="53">&nbsp;</td>
			</tr>
			<tr>
				<td width="26" height="536">&nbsp;</td>
				<td width="422" height="536" align="right" valign="bottom">
                	<?php
                		if(($tab_name=="Schedule") || ($tab_name == "New Schedule") )
							echo "<img src='images/btn_add_plus.png' border='0' onclick='window.location = \"_DO:cmd=send_js&c_name=med_customer_search&js=submit_checked()\"' />";
						else
							echo "&nbsp;";
					?>
                </td>
				<td width="26" height="536">&nbsp;</td>
			</tr>
			<tr>
				<td width="26" height="10"></td>
				<td width="422" height="10"></td>
				<td width="26" height="10"></td>
			</tr>
		</table>
	</body>
</html>
