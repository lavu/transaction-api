<?php
session_start();

$removeThisID = $_SESSION['customerID'];

if (!isset($removeThisID)){
	
	echo "<script type=\"text/javascript\">alert(\"Select a customer to remove.\");</script>";
	
}//if

echo "<script type=\"text/javascript\">
		
		var answer = confirm(\"Are you sure you wish to remove this customer?\");
		
		if (answer){
			window.location = \"_DO:cmd=load_url&c_name=med_customer_info&f_name=medical/med_customer_removeCustomer2.php?\";
		}	

</script>";

?>