<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Customer Search</title>

		<style type="text/css">
<!--

body {
	background-color: transparent;
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	-webkit-tap-highlight-color:rgba(0,0,0,0);
}

.style4 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #8896AD;
}
.style6 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 24px;
	font-weight: bold;
	color: #8896AD;
}
.style8 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 15px;
	font-weight: bold;
	color: #07142a;
}
.creditnumber {
	font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #3d4f6d;
}
.nametext {
	font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
}
.nametext_grey {
	font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #777777;
}
.subtitles {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #5A75A0;
}
-->
		</style>
        

        
	</head>

<?php 
	require_once(dirname(__FILE__) . "/../comconnect.php"); 
	$mode = reqvar("mode","recent_signup");
	$tab_name = reqvar("tab_name");
	//echo "tab: " . $tab_name;
	if(reqvar("loc_id")) $loc_id = reqvar("loc_id");
	else if(sessvar_isset("loc_id")) $loc_id = sessvar("loc_id");
	set_sessvar("loc_id", $loc_id);	
	
	if(reqvar("source_area")) $source_area = reqvar("source_area");
	else $source_area = "customers";
	if(reqvar("order_id")) $order_id = reqvar("order_id");
	else $order_id = "";
		
	$customer_selected = reqvar("customer_id","");
	$auto_select_row = "";
	$set_last_activity = "0";
	
	if($customer_selected=="" && isset($_SESSION["customerID"]))
		$customer_selected = $_SESSION["customerID"];
	/*if($_SERVER['REMOTE_ADDR']=="74.95.18.90")
	{
		echo $customer_selected;
	}*/
	
	$rlist = array();
	if($mode=="recent_signup")
	{
		$cust_query = lavu_query("select * from `med_customers` where `last_activity`>='".date("Y-m-d H:i:s",current_ts(-180))."' AND `last_activity`<='".date("Y-m-d H:i:s",current_ts(+2400))."' order by `last_activity` desc");// where `date_created`>='$mintime'");
	}
	else if($mode=="search")
	{
		$search_term = reqvar("search_term");
		$cust_query = lavu_query("select * from `med_customers` where (`f_name` LIKE TRIM('%".$search_term."%') or `l_name` LIKE TRIM('%".$search_term."%') or concat(TRIM(`f_name`),' ',TRIM(`l_name`)) LIKE TRIM('%".$search_term."%') ) order by TRIM(`f_name`) LIKE TRIM('".$search_term."') desc, `f_name` asc, `l_name` asc limit 100");
	}
		
	$jscode = "";
?>
	<body>
	
   
    
    
    
    
    	
    <?php
			
			
		?>
		<table width="426" border="0" cellspacing=0 cellpadding=0>
        	<?php
			if($message!="")
			{
				echo "<td align='center'><font style='font-size:24px; color:#999999; font-family:Verdana,Arial'>$message</font></td>";
			}
			else
			{
				$min_rows = 12;
				$rowcount = 0;
				$row_selected = 0;
				$cmd = "_DO:cmd=load_url&c_name=med_customer_info&f_name=medical/med_customer_info.php?customer_id=[id]";
				if ($tab_name == "customModifier"){
					$cmd = "_DO:cmd=add_mod&mod_title=Customer:[fullname] ([id])&mod_price=0";
				}
				if(trim($source_area)=="events") {
					$cmd = "_DO:cmd=load_url&c_name=ers&f_name=ers/ers.php?customer_id=[id]&order_id=$order_id&updated=1_DO:cmd=close_overlay";
					$customer_selected = "";
				}
				//echo $cmd . "<br>";
				while($cust_read = mysqli_fetch_assoc($cust_query))
				{
					if($set_last_activity=="0")
					{
						$set_last_activity = $cust_read['last_activity'];
					}
					
					$rowcount++;
					$fullname = trim($cust_read['f_name'] . " " . $cust_read['l_name']);
					$row_cmd = str_replace("[id]",$cust_read['id'],$cmd);
					$row_cmd = str_replace("[fullname]", $fullname, $row_cmd);
					$change_select = " if(row_selected) document.getElementById(\"customer_row_\" + row_selected).style.background = \"URL(images/tab_bg2.png)\"; row_selected = \"".$rowcount."\"; document.getElementById(\"customer_row_\" + row_selected).style.background = \"URL(images/tab_bg2_lit.png)\"";
					$nametext_class = "nametext";
					$td_onclick = "location = \"".$row_cmd."\"; ".$change_select;
					$use_bg = "images/tab_bg2.png";
					$bgf = "images/btn_credit.png";
							
					/*if($_SERVER['REMOTE_ADDR']=="74.95.18.90")
					{
						echo "customer_selected: $customer_selected<br>cust_read_id: " . $cust_read['id'] . "<br>";
					}*/
					
					if($customer_selected==$cust_read['id'])
					{
						$use_bg = "images/tab_bg2_lit.png";
						$auto_select_row = $rowcount;
					}
					$sigdata = "&nbsp;";
					$rows = $cust_read;
					if(isset($rows) && isset($rows['waiver_expiration']))
					{
						$waiver_expiration = $rows['waiver_expiration'];
						$waiver_id = (isset($rows['field16']))?$rows['field16']:"";
						$wparts = explode(" ",$waiver_expiration);
						$wparts = explode("-",$wparts[0]);
						
						if(trim($waiver_expiration)!="")
						{
							if($waiver_expiration < date("Y-m-d H:i:s"))
							{
								$sigdata = "<img src='images/sig_red.png' width='24' />";
							}
							else if(substr($waiver_id,0,1)=="e")
							{
								$sigdata = "<img src='images/sig_blue.png' width='24' />";
							}
							else
							{
								$sigdata = "<img src='images/sig_green.png' width='24' />";
							}
						}
					}
					?>
					 <tr>
                        <td height="46"><table height="46" border="0" cellpadding="0" cellspacing="0">
                          <tr>
                          	<td width="10">&nbsp;</td>
                            <td width="34" align="center" valign="middle">&nbsp;
                            </td>
                            <td id="customer_row_<?php echo $rowcount; ?>" width="313" style="background:URL(<?php echo $use_bg; ?>);" ><table width="313" border="0" cellspacing="0" cellpadding="8">
                              <tr>
                                <td class="<?php echo $nametext_class; ?>" onclick='<?php echo $td_onclick; ?>'>
									<?php 
										echo $fullname;
									?>
								</td>
                              </tr>
                            </table></td>
                            <td width="52" align="left" valign="top" background="<?php echo $bgf?>"><table width="50" height="46" border="0" cellspacing="0" cellpadding="6">
                              <tr>
                                <td align="center" valign="middle" class="creditnumber">
                                	<?php
										echo $sigdata;
									?>
								</td>
                              </tr>
                            </table></td>
                          </tr>
                        </table></td>
                      </tr>
					<?php
				}
				for($i=$rowcount; $i<$min_rows; $i++)
				{
					?>
					 <tr>
                        <td height="46"><table height="46" border="0" cellpadding="0" cellspacing="0">
                          <tr>
                          	<td width="10">&nbsp;</td>
                            <td width="34" align="center" valign="middle">&nbsp;</td>
                            <td width="313" background="images/tab_bg2.png" ><table width="313" border="0" cellspacing="0" cellpadding="8">
                              <tr>
                                <td class="nametext">&nbsp;</td>
                              </tr>
                            </table></td>
                            <td width="52" align="left" valign="top" background="images/btn_credit.png"><table width="50" height="46" border="0" cellspacing="0" cellpadding="6">
                              <tr>
                                <td align="center" valign="middle" class="creditnumber">&nbsp;</td>
                              </tr>
                            </table></td>
                          </tr>
                        </table></td>
                      </tr>
					<?php
				}
			}
			?>
      <!--<tr>
        <td height="46" background="images/tab.png"><a onclick='location = "_DO:cmd=cancel";'>Custom Modifier Cancel Button Example</a></td>
      </tr>-->
    </table>
    
    
    
    
	<?php if($mode=="recent_signup") { ?>
		<script language="javascript" src="lib/ajax_loop.js"></script>    
        <script type="text/javascript">
            <?php 
				if($auto_select_row=="") echo "row_selected=0; "; 
				else echo "row_selected=\"$auto_select_row\"; "; 
				echo "last_activity = '$set_last_activity'; ";
			?>
            
            ajax_use_interval = 2000;
            set_ajax_url("med_get_response.php?cc=<?php echo $company_code_full?>&loc_id=<?php echo $loc_id?>&mode=recent_activity");
            function ajax_received(vars)
            {
				get_last_activity = vars['last_activity'];				
				if(get_last_activity!=last_activity)
				{
					window.location.reload();
				}
            }
            start_ajax_loop();
        </script>
	<?php } else { ?>
        <script type="text/javascript">
            <?php if($auto_select_row=="") echo "row_selected=0; "; else echo "row_selected=\"$auto_select_row\"; "; ?>
		</script>   
    <?php } ?>
	</body>
</html>
