<?php
	require_once(dirname(__FILE__) . "/../comconnect.php");
	require_once(dirname(__FILE__) . "/./med_functions.php");
	session_start();
	
	foreach($_REQUEST as $key => $val){
    	//echo $key . " = " . $val . "<br>";
    	if(isset($_REQUEST[$key])){
        	$_SESSION[$key] = $_REQUEST[$key];
    	}
	}
	
	if(!isset($_REQUEST['company_name'])) $_GET['company_name'] = $_SESSION['cc'];
	if(!isset($_REQUEST['cc']))           $_GET['cc']           = $_SESSION['cc'];
	if(!isset($_REQUEST['dn']))           $_GET['dn']           = $_SESSION['dn'];
	if(!isset($_REQUEST['loc_id']))       $_GET['loc_id']       = $_SESSION['loc_id'];
	if(!isset($_REQUEST['server_id']))    $_GET['server_id']    = $_SESSION['server_id'];
	if(!isset($_REQUEST['server_name']))  $_GET['server_name']  = $_SESSION['server_name'];
	if(!isset($_REQUEST['tab_name']))     $_GET['tab_name']     = $_SESSION['tab_name'];
	if(!isset($_REQUEST['customer_id']))  $_GET['customer_id']  = $_SESSION['customer_id'];
	
	$loc_id = sessvar("loc_id");
	if(reqvar("customer_id")) $customer_id = reqvar("customer_id"); 
	else if(isset($_SESSION['customerID'])) $customer_id = $_SESSION['customerID'];
	else
	{
    	exit();
	}
	$_SESSION['customerID'] = $customer_id;

	//foreach($_REQUEST as $key => $val){ echo $key . " = " . $val . "<br>"; }
?>

<html>
	<head>
		<title>Customer Info</title>

<?php
generateFormValidationJS($loc_id);
?>


<style type="text/css">

body {
	background-color: transparent;
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	-webkit-tap-highlight-color:rgba(0,0,0,0);
	font-family:Verdana, Geneva, sans-serif;
	color: #5A75A0;
}
/*
.gridposition {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #5A75A0;
}
*/

h2{
	font-family:Verdana, Geneva, sans-serif;
	
}
.leftForm{
	float:left;
	width:110px;
	margin-left:20px;
	text-align:right;
	margin-right:9px;
	padding-top:12px;
}


.leftForm1{
	float:left;
	width:100px;
	margin-left:20px;
	text-align:right;
	margin-right:9px;
	
}


.rightForm{
	float:left;
	width:300px;
}

.rightForm1{
	float:left;
	width:10px;
}


.breakLine{
	clear:both;
	height:1px;	
}



.updateForm{
	width:100px;
	height:33px;
	background-image:url(images/btn_update_100x33.png);
	border:0;
}

.myInput{
	background-image:url(images/tab_bg_262.png);
	background-repeat:no-repeat;
	background-color:transparent;
	border:none;
	width:262px;
	height:46px;
}

label{
	color: #5A75A0;
	font-family:Verdana, Geneva, sans-serif;
	font-size:10pt;
	font-weight: bold;
}

</style>

<script type="text/javascript">





</script>



	</head>
<body>


<h2>Customer Information</h2>
<?php
//echo "<p>Currently In development @" . date("H:i:s m-d-Y") . "</p>";

?>

<form action="med_customer_updateProcess.php" method="post" name="thisForm" onSubmit="return validateForm();">

<?php
			INCLUDE 'med_customer_formInclude.php';
?>

<br>

<div class="leftForm">
		
		
</div><!--leftForm-->

<div class="rightForm">
<input class="updateForm" style="float:left; margin-right:20px;" type="submit" name="submit" value="" />
		<?php
echo "<img src='images/btn_history.png' height=\"33px\" onclick='window.location = \"_DO:cmd=create_overlay&f_name=medical/customer_history_overlay_wrapper.php;medical/customer_history.php&c_name=customer_history_overlay_wrapper;customer_history&dims=250,100,574,549;266,158,542,480&scroll=0;1?customer_id=$customer_id\";' />";
//echo "<span style=\"color:#fff;\">customerID: $customer_id<br/></span>";

$in_dev = (strpos($_SERVER['REQUEST_URI'],"/dev/")!==false);
if($in_dev) $compath = "/dev/components";
else $compath = "/components";

$sigmsg = "";
$sign_term = "sign";
$border_color = "#99aabb";
if(isset($rows) && isset($rows['waiver_expiration']))
{
	$waiver_expiration = $rows['waiver_expiration'];
	$wparts = explode(" ",$waiver_expiration);
	$wparts = explode("-",$wparts[0]);
	
	if(trim($waiver_expiration)!="")
	{
		$sign_term = "re-sign";
		if($waiver_expiration < date("Y-m-d H:i:s"))
		{
			$sigmsg = "<br><font color='#880000'>Expired " . $wparts[1] . "/" . $wparts[2] . "/" . $wparts[0] . "</font>";
			$border_color = "#880000";
		}
		else
		{
			$sigmsg = "<br>Expires " . $wparts[1] . "/" . $wparts[2] . "/" . $wparts[0];
		}
	}
}

$com_query = lavu_query("select `component_package_code` from `locations` where `component_package_code`!=''");
if(mysqli_num_rows($com_query))
{
	$com_read = mysqli_fetch_assoc($com_query);
	$comcode = $com_read['component_package_code'];
}

if (in_array($comcode, array("ers", "hotel", "lasertag", "medical")))
{
	$signature_path = $compath . "/lavukart/companies/".$dataname."/signatures/signature_".$customer_id.".jpg";
	echo "<br><br>";
	echo "<table><tr><td valign='top'>";
	
	echo "<img src='images/doc.png' border='0' onclick='window.location = \"_DO:cmd=create_overlay&f_name=medical/customer_history_overlay_wrapper.php;medical/customer_read_waiver.php&c_name=customer_history_overlay_wrapper;customer_read_waiver&dims=250,100,574,549;266,158,542,480&scroll=0;1?customer_id=$customer_id&title_start=Waiver: (name) ($sign_term)\";'/>";
	
	echo "</td><td valign='middle'>";
	
	$root_signature_path = $_SERVER['DOCUMENT_ROOT'] . $signature_path;
		echo "<table style='border: solid 2px $border_color' bgcolor='#ffffff'><tr><td height='55' onclick='window.location = \"_DO:cmd=update_signature&refresh_comp=med_customer_info&customer_id=$customer_id&minor_or_adult=adult?updated=1\"'>";
		if (is_file($root_signature_path))
		{
			echo "<img src='$compath/lavukart/companies/".$dataname."/signatures/signature_".$customer_id.".jpg' width='160'>";
		}
		else
		{
			echo "&nbsp;&nbsp;no signature found&nbsp;&nbsp;";
			/*if($_SERVER['REMOTE_ADDR']=="74.95.18.90")
			{
				echo "<br>$root_signature_path<br>";
			}*/
		}
		echo "</td></tr></table>";
		echo $sigmsg;
		
	echo "</td></tr></table>";
}

/*if($_SERVER['REMOTE_ADDR']=="74.95.18.90")
{
	foreach($_REQUEST as $key => $val) echo $key . " = " . $val . "<br>";
}*/
if(!isset($_REQUEST['cc']))
{
	echo "<script language='javascript'>window.location = \"_DO:cmd=load_url&c_name=med_customer_search&f_name=medical/med_customer_search.php?customer_id=$customer_id\";</script>";
}
?>

</div><!--rightForm-->

<div class="breakLine"></div>
</form>
</body>
</html>


<?php

	function get_timestamp($dt) {
		$datetime_array = explode(" ", $dt);
		$date_array = explode("-", $datetime_array[0]);
		
		$ts = mktime(0, 0, 0, ($date_array[1]  * 1), ($date_array[2] * 1), ($date_array[0] * 1));
		return (int)$ts;
	}//get_timestamp()


?>