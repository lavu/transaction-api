<?php
	require_once(dirname(__FILE__) . "/../comconnect.php");
	require_once(dirname(__FILE__) . "/../comconnect_legacy.php");
	$cm = reqvar("cm"); 
	$tab_name = reqvar("tab_name");
	//$racer_id = 123;
	if(reqvar("customer_id")) $customer_id = reqvar("customer_id");
	else if (isset($_POST['mod_info'])) $customer_id = extractCustomerIDfromModInfo($_POST['mod_info'], "MEMBER:");
	if(reqvar("minor_or_adult")) $minor_or_adult = reqvar("minor_or_adult"); else $minor_or_adult = "adult";
	if(reqvar("source_area")) $source_area = reqvar("source_area"); else $source_area = "customers";
	$refresh_comp = (reqvar("refresh_comp"))?reqvar("refresh_comp"):"";
	//$racer_id = $customer_id;
	//echo "<span style=\"color:#fff;\">customer_id: $customer_id</span><br/>";
?>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Untitled Document</title>

		<style type="text/css">
<!--
body {
	background-color: transparent;
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	-webkit-tap-highlight-color:rgba(0,0,0,0);
}

.style4 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #8896AD;
}
.style6 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 24px;
	font-weight: bold;
	color: #8896AD;
}
.style8 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 15px;
	font-weight: bold;
	color: #07142a;
}

.racer_name {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 20px;
	color: #d4dae4;
}
-->
		</style>
		
		<script language='javascript'>
		
			function signature_updated(customer_id) {
			
				window.location = "customer_history_overlay_wrapper.php?signature_updated=1&customer_id=" + customer_id;
							
				return 1;
			}
		
		</script>
		
	</head>
	
<?php

	if (reqvar("signature_updated") == "1") {
	
		// update customer record to reflect submission of waiver signature
		
		echo "<script language='javascript'>window.location = \"_DO:cmd=done\"</script>";	
		exit();
	}
	
	$racer_name = "";
	
	$get_customer_info = lavu_query("SELECT * FROM `med_customers` WHERE `id` = '[1]'", $customer_id);
	//echo "<span style=\"color:#fff;\">SDFSDF: racer_name: $racer_name<br/>racer_id: ".$customer_id."<br/>";
	if (mysqli_num_rows($get_customer_info) > 0) {
		
		$extract = mysqli_fetch_array($get_customer_info);
		$racer_name = $extract['f_name']." ".$extract['l_name'];
		
	}
	
	$close_cmd = ($cm)?"cancel":"close_overlay";
?>

	<body>
		<table width="574" height="549" border="0" cellpadding="0" cellspacing="0" style="background:URL(images/win_overlay_no_btns_wide.png); background-repeat:no-repeat; background-position:top center;">
			<tr>
				<td width="26" height="53">&nbsp;</td>
				<td width="522" height="53">
					<table width="522" border="0" cellpadding="6" cellspacing="0">
						<tr>
							<td width="63" height="47" align="center" style="background:URL(images/btn_soft.png); background-repeat:no-repeat; background-position:center;" onclick='location = "_DO:cmd=<?php echo $close_cmd; ?>";'><span class="style6"> X </span></td>
              <td width="459" height="47" align="center"><span class="racer_name"><b>
			  			<?php 
							if(isset($_POST['title_start'])) 
							{
								$title_start = $_POST['title_start'];
								
								if(strpos($title_start,"(name)")!==false)
								{
									$title_start = str_replace("(name)",$racer_name,$title_start);
								}
								else $title_start .= " " . $racer_name;
								
								$show_signature_code = false;
								if(strpos($title_start,"(sign)")!==false)
								{
									$show_signature_code = true;
									$show_signature_term = "sign";
								}
								else if(strpos($title_start,"(re-sign)")!==false)
								{
									$show_signature_code = true;
									$show_signature_term = "re-sign";
								}
								
								//if($source_area=="events") $refresh_comp = "ers";
								//else $refresh_comp = "med_customer_info";
								
								if (empty($refresh_comp)) {
									if ($cm) {
										$refresh_comp = "wrapper&js=signature_updated($customer_id);";
										$js = "signature_updated($customer_id);";
									}
									else if($source_area=="events") $refresh_comp = "ers";
									else if($source_area=="guests") $refresh_comp = "guests";
									else $refresh_comp = "med_customer_info";
								}
								
								if($show_signature_code)
								{
									// When it's a contract being signed, we prepend an identifier string to the Customer ID to keep the call to the update_signature _DO cmd from overwriting the waiver signature.
									$customer_id_prepend = (substr($title_start, 0, 8) === "Contract") ? "contract_" : "";

									// Prevent post-signature javascript callbacks from being called when this is called by forced modifiers in the POS tab.
									if (!$cm && $refresh_comp == 'members_signins') $js = "loadMemberProfile({$customer_id})";

									if (!empty($js)) $js = "&js={$js}";

									$sig_cmd = "_DO:cmd=update_signature&refresh_comp=".$refresh_comp.$js."&customer_id=".$customer_id_prepend.$customer_id."&minor_or_adult=".$minor_or_adult;
									if (!$cm) $sig_cmd = "_DO:cmd=close_overlay".$sig_cmd."?updated=1";		
								
									$sig_click = "onclick='window.location = \"".$sig_cmd."\"'";
									$sign_code = "<table style='border:solid 4px #d4dae4' bgcolor='#ffffff' $sig_click><tr><td width='80' height='24' class='style4' align='center'>$show_signature_term</td></tr></table>";
									$title_start = str_replace("(sign)","",$title_start);
									$title_start = str_replace("(re-sign)","",$title_start);
								}
								
								echo "<table cellspacing=0 cellpadding=0><tr><td valign='middle'><span class='racer_name'><b>$title_start</b></span></td><td width='16'>&nbsp;</td><td valign='top'>$sign_code</td></tr></table>";
							}
							else
								echo $racer_name; 
						?></b></span></td>
						</tr>
					</table>
				</td>
				<td width="26" height="53">&nbsp;</td>
			</tr>
			<tr><td width="574" height="496" colspan="3">&nbsp;</td></tr>
		</table>
	</body>
</html>
