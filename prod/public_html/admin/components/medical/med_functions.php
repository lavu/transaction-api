<?php
require_once(dirname(__FILE__) . "/../comconnect.php");
?>

<?php

function populateForm($label, $type, $id, $customerID, $loc_id){
			/* Function takes in input from the config table, and prints the relevent form element to the screen */
			$valuesResult = lavu_query("SELECT * FROM config WHERE id=\"[1]\" AND location=\"[2]\" AND _deleted!=\"1\"", $id, $loc_id);
			$valuesRow = mysqli_fetch_assoc($valuesResult);
			$value2 = $valuesRow['value2'];//this is the field that stores customer information in the med_customers db
			$getCustInfoResult = lavu_query("SELECT $value2 FROM med_customers WHERE locationid=\"[1]\" AND id=\"[2]\"",$loc_id,$customerID);
			$getCustInfoRow = mysqli_fetch_assoc($getCustInfoResult);
			$value = $getCustInfoRow[$value2];
			
			$value4 = $valuesRow['value4'];//options
			$value5 = $valuesRow['value5'];
			$elementName = createFormElementName($label);
			//echo "$elementName<br>";
			if ($type == "list_menu"){
						
						if ($value4 == "useTool"){
									
									if ($value5 == "State"){
												
												State($value, $valuesRow['value']); //what should the input be?
												return;
									}//if
									
									if ($value5 == "Last Name"){
												//nothing needs to be done
												return;
									}//if
									
									if ($value5 == "First Name"){
												//nothing needs to be done
												return;
									}//if
									
						}//if
						
						if ($value4 != "useTool"){
												
												$printListMenu = "<select class=\"myInput\" name=\"$elementName\" id=\"$elementName\"><option value=\"\"></value>";
												
												$listMenuOptionsString = $value4;
												$listMenuOptionsArray = explode(",", $listMenuOptionsString);
												$matchedAValue = false;
												for ($i=0;$i<count($listMenuOptionsArray);$i++){
															
															$curVal = $listMenuOptionsArray[$i];
															$curLabel = createFormElementLabel($curVal);
															if ($curVal != $value){
																		$printListMenu .= "<option value=\"$curVal\">$curLabel</option>";
															}//if
															
															if ($curVal == $value){
																		$printListMenu .= "<option value=\"$curVal\" selected=\"selected\">$curLabel</option>";
																		$matchedAValue = true;
															}//if
															
												}//for
												
												if ($matchedAValue != true && ($customerID != "") ){
															$printListMenu .= "<option value=\"$value\" selected=\"selected\">$value</option>";			
												}//if
												
												$printListMenu .= "</select>";
												echo $printListMenu;
												
												return;
						
						}//if
						
						
			}//if - list_menu
			
			
			if ($type == "checkbox"){
					//mail ("ck@poslavu.com", "value: $value", "howdy");
					echo "<input type=\"checkbox\" name=\"$elementName\"  id=\"$elementName\"";
						if ($value == "on"){
								echo "checked";
						}//if
					echo " />";
					
			}//if
			
			if ($type == "input"){
				
					echo "
								<input type=\"text\" class=\"myInput\" value=\"$value\" name=\"$elementName\" id=\"$elementName\" />
					";
					
					return;
			}//if
			
			if ($type == "date") {
				$field_name = $elementName;
				$field_value = $value;
				$field_value_escaped = str_replace("\"","&quot;",$field_value);
				$select_style = "font-size:14px";
				$field_props = 0;
				$field_props2 = -100;
				
							$select_changecode = " onchange='document.getElementById(\"$field_name\").value = document.getElementById(\"year_$field_name\").value + \"-\" + document.getElementById(\"month_$field_name\").value + \"-\" + document.getElementById(\"day_$field_name\").value; document.getElementById(\"update_btn\").style.display = \"inline\";'";
							$valign = "middle";
							$element_height = 46;
							$element_bg = "images/tab_bg2.png";
							
							$form_element = "";
							
							$birthdate_parts = explode("-",$field_value);
							if($birthdate_parts > 2)
							{
								$bd_year = $birthdate_parts[0];
								$bd_month = $birthdate_parts[1];
								$bd_day = $birthdate_parts[2];
							}
							else
							{
								$bd_year = "";
								$bd_month = "";
								$bd_day = "";
							}
							$form_element .= "<table><tr><td>";
							
							//$element_height = 92; 
							//$form_element .= "<input type='text' name='$field_name' id='$field_name' value=\"$field_value_escaped\">";							
							$form_element .= "<input type='hidden' name='$field_name' id='$field_name' value=\"$field_value_escaped\">";
							
							$form_element .= "<select name='month_$field_name' style='$select_style' id='month_$field_name' $select_changecode>";
							$form_element .= "<option value=''></option>";
							for($month=1; $month <= 12; $month++)
							{
								$month_ts = mktime(0,0,0,$month,1,2000);
								$selected = ($month==$bd_month*1)?" selected":"";
								$form_element .= "<option value='".date("m",$month_ts)."' $selected>".date("M",$month_ts)."</option>";
							}
							$form_element .= "</select>";
							$form_element .= "</td><td>/</td><td>";
							$form_element .= "<select name='day_$field_name' style='$select_style' id='day_$field_name' $select_changecode>";
							$form_element .= "<option value=''></option>";
							for($day=1; $day <= 31; $day++)
							{
								if($day < 10) $day_leading_zero = "0" . ($day * 1);
								else $day_leading_zero = $day;
								$selected = ($day==$bd_day*1)?" selected":"";
								$form_element .= "<option value='$day_leading_zero' $selected>$day</option>";
							}
							$form_element .= "</select>";
							$form_element .= "</td><td>/</td><td>";
							$form_element .= "<select name='year_$field_name' style='$select_style' id='year_$field_name' $select_changecode>";
							$form_element .= "<option value=''></option>";
							for($year=date("Y") + $field_props; $year >= date("Y") + $field_props2; $year--)
							{
								$selected = ($year==$bd_year*1)?" selected":"";
								$form_element .= "<option value='$year' $selected>$year</option>";
							}
							$form_element .= "</select>";
							$form_element .= "</td></tr></table>";
							
							echo $form_element;
			}
			
			if ($type == "radio"){
					
					
					
					$valuesArray = explode(",", $value4);
					$formString;
			
					$radioGroupName = createFormElementLabel($label);
					$matchedAValue = false;
					for ($i=0;$i<count($valuesArray);$i++){
								$curVal = $valuesArray[$i];
								$curLabel = createFormElementLabel($curVal);
							echo "
							<div class=\"leftForm1\">
										<label>$curLabel</label>
							</div>";
							
							echo "<div class=\"rightForm1\">";
							
										if ($curVal == $value){			
													echo "<input value=\"$curVal\" type=\"radio\" name=\"$elementName\" id=\"$elementName\" checked=\"checked\"/>";
													$matchedAValue = true;
										}//if
										
										if ($curVal != $value){
													echo "<input value=\"$curVal\" type=\"radio\" name=\"$elementName\" id=\"$elementName\" />";
										}//if
										
							echo "</div>
							<div class=\"breakLine\"></div>
							";
							
					}//for
					
					if ($matchedAValue != true && ($customerID != "") ){
								echo "
										<div class=\"leftForm1\">
													<label>&nbsp;$value</label>
										</div>";
								echo "<div class=\"rightForm1\">
													<input value=\"$value\" type=\"radio\" name=\"$elementName\" id=\"$elementName\" checked=\"checked\"/>
											</div>";
					}//if
					
					return array("name"=>$elementName,"value"=>$value);
			}//if
			
			return array("name"=>$elementName,"value"=>$value);
}//populateForm()


function createFormElementLabel($label){
	/* function returns the label in the form that can be used for a form element name (eg, takes out spaces and replaces them with underscores). 
			For example, the label "On or Off" will be returned as "On_or_Off", so this can be used to gather $_POST[] data from forms. */
			
			$pieces = explode("_", $label);
			$label = implode(" ", $pieces);
			
			return $label; //finish this function.
	
	
}//createFormElementLabel()


function createFormElementName($label){
	/* Function replaces spaces with underscore so that way label names can be used as names form form elements*/
			$pieces = explode(" ", $label);
			$formElementName = implode("_",$pieces);
			return $formElementName;
			
}//makeElementName()


function State($selectedState, $uniqueName){
	
	$state = $selectedState;//just so i don't have to readjust variables
	
	$state_list = "AL:Alabama,
AK:Alaska,
AZ:Arizona,
AR:Arkansas,
AK:Alaska,
AZ:Arizona,
AR:Arkansas,
CA:California,
CO:Colorado, 
CT:Connecticut, 
DE:Delaware, 
DC:District Of Columbia, 
FL:Florida, 
GA:Georgia, 
HI:Hawaii, 
ID:Idaho, 
IL:Illinois, 
IN:Indiana, 
IA:Iowa, 
KS:Kansas, 
KY:Kentucky, 
LA:Louisiana, 
ME:Maine, 
MD:Maryland, 
MA:Massachusetts, 
MI:Michigan, 
MN:Minnesota, 
MS:Mississippi, 
MO:Missouri, 
MT:Montana, 
NE:Nebraska, 
NV:Nevada, 
NH:New Hampshire, 
NJ:New Jersey, 
NM:New Mexico, 
NY:New York, 
NC:North Carolina, 
ND:North Dakota, 
OH:Ohio, 
OK:Oklahoma, 
OR:Oregon, 
PA:Pennsylvania, 
RI:Rhode Island, 
SC:South Carolina, 
SD:South Dakota, 
TN:Tennessee, 
TX:Texas, 
UT:Utah, 
VT:Vermont, 
VA:Virginia, 
WA:Washington, 
WV:West Virginia, 
WI:Wisconsin, 
WY:Wyoming,
";
 
$states = explode(",",$state_list);
$state_options = "<select class=\"myInput\" id=\"$uniqueName\" name=\"$uniqueName\">";
$state_options .= "<option></option>";

for($i=0; $i<count($states); $i++){
	$current_state = explode(":",$states[$i]);
	if(count($current_state) > 1){
		$state_short = trim($current_state[0]);
		$state_name = trim($current_state[1]);
		if($state_short==$state){
					$selected_code = " selected";
		}//if
		
		$state_options .= "<option value=\"$state_short\"$selected_code>$state_name</option>";
		$selected_code = "";
	
	}//if
}//for

$state_options .= "</select>";

echo $state_options;
	
return;	
	
}//State()


function generateFormValidationJS($loc_id){
	/* This function generates a javascript that forces required fields to be filled in before the form is submitted*/
	
			$javascript = "<script type=\"text/javascript\">
			
			function validateForm(){
						//var thing = document.getElementById(\"First_Name\").value;
						//alert(thing);
						var formIsValid = true;
						var fillInTheseFields = \"\";
										";
			
			$requiredResult = lavu_query("SELECT * FROM config WHERE location=\"[1]\" AND setting=\"GetCustomerInfo\" AND value6=\"requiredField\" AND _deleted!=\"1\"", $loc_id);
			while ($requiredRows = mysqli_fetch_assoc($requiredResult)){
						$label = $requiredRows['value'];//The label name shown to the user next to the input area
						$type = $requiredRows['type'];//input/list_menu/radio
						$formElementName = createFormElementName($label);
						
						
						if ($type == "input"){
									$javascript .= "if (document.getElementById(\"$formElementName\").value ==\"\"){
																				formIsValid = false;
																				fillInTheseFields += \"\\n $label\";
													  			}//if
													 			 ";
						}//if
						
						
						if ($type == "list_menu"){
									$javascript .= "if (document.getElementById(\"$formElementName\").value ==\"\"){
																				formIsValid = false;
																				fillInTheseFields += \"\\n $label\";
													  			}//if
													 			 ";
						}//if
						
						if ($type == "checkbox"){
									$javascript .= "if (document.getElementById(\"$formElementName\").value ==\"\"){
																				formIsValid = false;
																				fillInTheseFields += \"\\n $label\";
													  			}//if
													 			 ";
						}//if
						
						
						if ($type == "radio"){
									$javascript .= "if (getCheckedRadioValue('$formElementName') == false){
																				formIsValid = false;
																				fillInTheseFields += \"\\n $label\";
													  			}//if
																	
													 			 ";
						}//if
						
						
			}//while
						
						
						
						$formElementName = createFormElementName($label);
						
		
			$javascript .= "
											if (formIsValid != true){
														alert(\"The following fields are required: \" + fillInTheseFields);
														return false;
											}//if
											
											return true; //if form is valid return true
											
									}//validateForm();";
			
			/* Add other javascripts here */
			$javascript .= "
												function getCheckedRadioValue(input){
													/* This function return the value of the selected radio button, given the name of the radio buttons. */
													radioGroupLength = thisForm.elements[input].length;
													for (i=0;i<radioGroupLength;i++){
																
																var curValIsChecked = thisForm.elements[input][i].checked;
																if (curValIsChecked == true){
																			curVal = thisForm.elements[input][i].value;
																			return curVal;
																}//if
													}//for
													
													//script should not get this far, if it does, nothing was selected
													return false;
													
												}//getCheckedRadioValue()
											";
			
			
		
			$javascript .= "</script>";
			
			echo $javascript;
			
}//generateFormValidationJS()


function clean($string){
		return preg_replace('/[^a-zA-Z0-9\s]/', '', $string); 
}//clean()


?>