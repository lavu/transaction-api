<?php
	require_once(dirname(__FILE__) . "/../comconnect.php");
	require_once(dirname(__FILE__) . "/./med_functions.php");
	$loc_id = sessvar("loc_id");
?>

<html>
	<head>
		<title>Customer Info</title>

<?php
generateFormValidationJS($loc_id);
?>

<style type="text/css">

body {
	background-color: transparent;
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	-webkit-tap-highlight-color:rgba(0,0,0,0);
	font-family:Verdana, Geneva, sans-serif;
	color: #5A75A0;
}
/*
.gridposition {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #5A75A0;
}
*/

.submitForm{
	width:100px;
	height:33px;
	background-image:url(images/btn_submit_100x33.png);
	border:0;
}

.leftForm{
	float:left;
	width:110px;
	margin-left:20px;
	text-align:right;
	margin-right:9px;
	padding-top:12px;
}

.leftForm1{
	float:left;
	width:100px;
	margin-left:20px;
	text-align:right;
	margin-right:9px;
	
}

.rightForm{
	float:left;
	width:300px;
}

.rightForm1{
	float:left;
	width:10px;
}

.breakLine{
	clear:both;
	height:1px;	
}

.myInput{
	background-image:url(images/tab_bg_262.png);
	background-repeat:no-repeat;
	background-color:transparent;
	border:none;
	width:262px;
	height:46px;
}

label{
	color: #5A75A0;
	font-family:Verdana, Geneva, sans-serif;
	font-size:10pt;
	font-weight: bold;
}

</style>

<script type="text/javascript">

</script>

	</head>
<body>


<h2>Add Customer</h2>


<form action="med_addCustomer_process.php" method="post" id="thisForm" name="thisForm" onSubmit="return validateForm();">

<?php
			INCLUDE 'med_customer_formInclude.php';
?>

<br>

<div class="leftForm">
		&nbsp;
</div><!--leftForm-->

<div class="rightForm">
		<input class="submitForm" type="submit" name="submit" value="" />
	</div><!--rightForm-->

<div class="breakLine"></div>
</form>
</body>
</html>


<?php

	function get_timestamp($dt) {
		$datetime_array = explode(" ", $dt);
		$date_array = explode("-", $datetime_array[0]);
		
		$ts = mktime(0, 0, 0, ($date_array[1]  * 1), ($date_array[2] * 1), ($date_array[0] * 1));
		return (int)$ts;
	}//get_timestamp()


?>