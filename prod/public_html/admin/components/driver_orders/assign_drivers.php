<?php

	require_once(dirname(__FILE__) . "/../comconnect.php");
	header('Content-Type: text/html; charset=utf-8');
	// For Api Based Addon: if($_GET['request_key']==md5("AG3f2oCjXCZ7EW4bOvxk"))
	if(isset($company_code))
	{
		if(!isset($_REQUEST['comp_name'])) $_REQUEST['comp_name'] = "assign_drivers";
		$comp_name = $_REQUEST['comp_name'];
		//$loc_id

		$ajax_request = (isset($_POST['ajax_request']))?$_POST['ajax_request']:"";
		if($ajax_request=="time")
		{
			echo "&time=".time()."&display_time=".date("h:i:s a")."&";
			exit();
		}
		else if($ajax_request=="claim")
		{
			$claim_list = $_POST['claim_list'];
			$claim_pin = $_POST['claim_pin'];
			$message = "";
			$server_query = lavu_query("select * from `users` where `PIN`='[1]' order by id desc limit 1",$claim_pin);
			if(mysqli_num_rows($server_query))
			{
				$server_read = mysqli_fetch_assoc($server_query);
				$server_id = $server_read['id'];
				$server_name = "driver: " . $server_read['f_name'];
				$message = $server_id . " " . $server_name . " ";

				$claim_orders = explode(",",$claim_list);
				for($c=0; $c<count($claim_orders); $c++)
				{
					$order_id = trim($claim_orders[$c]);
					if($order_id!="")
					{
						// Update Driver Here
						// Cannot set a driver if a driver is already assigned.
						lavu_query("update `orders` set `server`='[1]', `server_id`='[2]' where `order_id`='[3]' and `server` NOT LIKE 'driver:%'",$server_name,$server_id,$order_id);
					}
				}
			}

			echo "&message=".urlencode($message)."&";
			exit();
		}
		else if($ajax_request=="get_receipt_str"){
			//error_log("In part 'get_receipt_str'");
			require_once(dirname(__FILE__).'/get_order_details.php');
			//error_log("Exiting: 'get_receipt_str'.");
			exit();
		}
		else if($ajax_request=="orders")
		{
			$timezone = $_POST['timezone'];
			$min_datetime = $_POST['min_datetime'];
			$max_datetime = $_POST['max_datetime'];

			$str = "";
			$count = 0;
			$order_query = lavu_query("select * from `orders` where `opened`>='[1]' and `opened`<'[2]' and `original_id`!='' and `void`!='1' and `server` NOT LIKE 'driver:%'",$min_datetime,$max_datetime);
			while($order_read = mysqli_fetch_assoc($order_query))
			{
				$info_parts = explode("|*|",$order_read['original_id']);
				for($i=0; $i<count($info_parts); $i++)
				{
					$inner_parts = explode("|o|",$info_parts[$i]);
					if($inner_parts[0]==19)
					{
						$customer_name = $inner_parts[2];
						$customer_id = $inner_parts[3];
						$customer_json = json_decode($inner_parts[4]);
						if(isset($customer_json->deliveryInfo))
						{
							$dinfo = $customer_json->deliveryInfo;
							$dinfo_firstname = $dinfo->First_Name;
							$dinfo_lastname = $dinfo->Last_Name;
							$dinfo_phone = $dinfo->Phone_Number;
							$dinfo_is_delivering = $dinfo->is_delivering;
							$dinfo_delivery_time = $dinfo->delivery_time;
							$dinfo_customer_id = $dinfo->customer_ID;
							$dinfo_address = $dinfo->address->street;
							$dinfo_city = $dinfo->address->city;
							$dinfo_state = $dinfo->address->state;
							$dinfo_zip = $dinfo->address->zip;
							$dinfo_country = $dinfo->address->country;
							$dinfo_customer_address = $dinfo->Street_Address;
							$dinfo_customer_city = $dinfo->City;
							$dinfo_customer_state = $dinfo->State;
							$dinfo_customer_zip = $dinfo->Zip_Code;

							//$json_info = "Time:$dinfo_delivery_time<br>Address: $dinfo_address<br>$dinfo_city $dinfo_state $dinfo_zip<br>";
							//$json_info .= print_r($customer_json->deliveryInfo,true) . "<br>";
							//echo "Customer: $customer_name ($customer_id)<br>";
							//echo "Info: $json_info<br>";
							//echo "<br>";

							$count++;
							$str .= "&order_id".$count."=".$order_read['order_id'];
							$str .= "&order_time".$count."=".$dinfo_delivery_time;
							$str .= "&order_address".$count."=".urlencode($dinfo_address);
							$str .= "&order_city".$count."=".$dinfo_city;
							$str .= "&order_state".$count."=".$dinfo_state;
							$str .= "&order_zip".$count."=".$dinfo_zip;
							$str .= "&order_customer_name".$count."=".urlencode($customer_name);
							$str .= "&order_customer_id".$count."=".$customer_id;

							$content_count = 0;
							$content_query = lavu_query("select * from `order_contents` where `order_id`='[1]'",$order_read['order_id']);
							while($content_read = mysqli_fetch_assoc($content_query))
							{
								$c_qty = $content_read['quantity'];
								if($c_qty > 0)
								{
									$c_item = $content_read['item'];
									$c_itemid = $content_read['item_id'];
									$c_price = $content_read['price'];
									$content_count++;
									$str .= "&order_cart".$count."_item".$content_count."=".$c_item."|".$c_itemid."|".$c_qty."|".$c_price;
								}
							}
							$str .= "&order_cart".$count."_count=".$content_count;
						}
					}
				}
			}
			$str .= "&order_count=".$count."&";
			echo $str;
			exit();
		}

		require_once(dirname(__FILE__) . "/com_resources.php");


		echo "<table width='100%' height='100%'>";
		echo "<tr><td style='font-size:12px; color:#555577; font-family:Verdana,Arial' height='10%'>";
		echo "	<table cellpadding=8 width='100%' bgcolor='#778899' style='border:solid 1px #334466'><tr>";
		echo "		<td align='left' width='20%'><input type='button' value='Refresh' onclick='refresh_window()' style='font-size:16px' /></td>";
		echo "		<td>&nbsp;</td>";
		echo "		<td align='right' width='20%'><input type='button' value='Claim' onclick='claim_orders()' style='font-size:24px' /></td>";
		echo "	</tr></table>";
		echo "</td></tr>";
		echo "<tr><td height='90%' align='center' valign='top' style='font-size:12px; color:#555577; font-family:Verdana,Arial'>";

		//echo "Assign Drivers Here<br>";
		//echo "<font style='font-size:12px'>";

		$loc_query = lavu_query("select * from `locations` where `id`='[1]'",$loc_id);
		if(mysqli_num_rows($loc_query))
		{
			$loc_read = mysqli_fetch_assoc($loc_query);
			$timezone = $loc_read['timezone'];

			if ($timezone != "") $datetime = localize_datetime(date("Y-m-d H:i:s"), $timezone);
			else $datetime = date("Y-m-d H:i:s");
			$se_hour = substr(str_pad($loc_read['day_start_end_time'], 4, "0", STR_PAD_LEFT), 0, 2);
			$se_min = substr(str_pad($loc_read['day_start_end_time'], 4, "0", STR_PAD_LEFT), 2, 2);

			if(date("H:i") < $se_hour . ":" . $se_min)
			{
				$min_ts = mktime($se_hour,$se_min,0,date("m"),date("d")-1,date("Y"));
				$max_ts = mktime($se_hour,$se_min,0,date("m"),date("d"),date("Y"));
			}
			else
			{
				$min_ts = mktime($se_hour,$se_min,0,date("m"),date("d"),date("Y"));
				$max_ts = mktime($se_hour,$se_min,0,date("m"),date("d")+1,date("Y"));
			}
			$min_datetime = date("Y-m-d H:i:s",$min_ts);
			$max_datetime = date("Y-m-d H:i:s",$max_ts);

			ini_set("display_errors","1");
			//echo "TimeZone: $timezone<br>";
			//echo "DateTime: $datetime<br>";
		}
		$sec_hash = sha1($dataname.'pumpkin');
		//require_once(dirname(__FILE__).'/driver_js_functions.php');

		//foreach($_POST as $pkey => $pval) echo "<br>post $pkey = $pval";
		//foreach($_GET as $pkey => $pval) echo "<br>get $pkey = $pval";
		//echo "</font>";

		echo "<div id='output'>&nbsp;</div>";
		echo "<div id='debug' style='font-size:10px'></div>";
		echo "</td></tr></table>";

		echo "<script language='javascript'>";
		echo "  orders_selected = ''; ";
		echo "	function lzero(n) { ";
		echo "		if(n * 1 < 10) return '0' + (n * 1); ";
		echo "		else return (n * 1); ";
		echo "	} ";
		echo "	function display_elapsed_from_seconds(sc) { ";
		echo "		var seconds = (sc * 1) % 60; ";
		echo "		var minutes = ((sc * 1) - (seconds * 1)) / 60; ";
		echo "		if(minutes > 0) return lzero(minutes) + ':' + lzero(seconds); ";
		echo "		else return '00:' + lzero(seconds); ";
		echo "	} ";
		echo "	counter = 0; ";
		echo "	function tick_tock() { ";
		echo "		document.getElementById('debug').innerHTML = 'Elapsed: ' + display_elapsed_from_seconds(counter); ";
		echo "		if(counter % 5 == 0)";
		echo "			window.location = '_DO:cmd=send_js&c_name=$comp_name&js=prevent_pin_timeout()'; ";
		echo "		counter++; ";
		echo "	} ";
		echo "function prevent_pin_timeout(tmode) { ";
		echo "  document.getElementById('debug').innerHTML = '&nbsp;&nbsp;' + document.getElementById('debug').innerHTML + '*'; ";
		echo "	 make_ajax_call('/components/driver_orders/assign_drivers.php','ajax_request=orders&timezone=$timezone&min_datetime=$min_datetime&max_datetime=$max_datetime','order_callback','vars'); ";
		echo "} ";
		echo "function apply_obj_id_to_orders_selected(objid) { ";
		echo "		var os_parts = orders_selected.split('|'); ";
		echo "		var new_orders_selected = ''; ";
		echo "		var objid_found = false; ";
		echo "		for(var n=0; n<os_parts.length; n++) { ";
		echo "			if(os_parts[n]!='') { ";
		echo "				if(os_parts[n]==objid) ";
		echo "					{ objid_found = true; } ";
		echo "				else ";
		echo "					{ new_orders_selected += '|' + os_parts[n] + '|'; } ";
		echo "			} ";
		echo "		} ";
		echo "		if(!objid_found) { new_orders_selected += '|' + objid + '|'; } ";
		echo "		orders_selected = new_orders_selected; ";
		echo "		return !objid_found; ";
		echo "} ";
		echo "function obj_id_order_selected(objid) { ";
		echo "		var objfound = orders_selected.indexOf('|' + objid + '|'); ";
		echo "		if(objfound==-1) return false; else return true; ";
		echo "} ";
		echo "function select_order(tobj) { ";
		echo "		var obj_id = tobj.id.replace(/order_table_/ig,''); ";
		echo "		var obj_status = apply_obj_id_to_orders_selected(obj_id); ";
		echo "		if(obj_status) { setborder = 'solid 2px #0000aa'; setbgcolor = '#ddeeff'; } else { setborder = 'solid 1px #000055'; setbgcolor = '#ffffff'; } ";
		echo "		tobj.style.border = setborder; ";
		echo "		tobj.style.backgroundColor = setbgcolor; ";
		echo "} ";
		echo "function claim_orders() { ";
		echo "		var claim_cmd = 'cmd=send_js&c_name=$comp_name&js=claim_pin(ENTERED_NUMBER)'; ";
		echo "		window.location = '_DO:cmd=number_pad&title=PIN&type=PIN&c_name=$comp_name&send_cmd=' + escape(claim_cmd) + '&max_digits=8'; ";
		//echo "		alert('Claim Orders: ' + os_arr.length); ";
		echo "} ";
		echo "function claim_pin(cpn) { ";
		echo "		var claim_list = ''; ";
//echo "alert('orders_selected:'+orders_selected);";
		echo "		var os_parts = orders_selected.split('|'); ";
		echo "		var os_arr = new Array(); ";
		echo "		for(var n=0; n<os_parts.length; n++) { ";
		echo "			if(os_parts[n]!='') { ";
		echo "				os_arr[os_arr.length] = os_parts[n]; ";
		echo "				if(claim_list!='') claim_list += ','; ";
		echo "				claim_list += os_parts[n]; ";
		echo "			} ";
		echo "		} ";
		//Inserted by Brian D.
		echo "      var order_id_list = [];";
		echo "      for(var k = 0; k < os_parts.length; k++) {";
		echo "          if(os_parts[k]=='') continue; ";
		//echo "          alert('Selected'+os_parts[k]);";
		echo "          var encoded_order_id=encodeURIComponent(os_parts[k]);";
		echo "          order_id_list.push(encoded_order_id);";
		echo "      }";
		//End Insert.
		echo "   order_ids_list_str=order_id_list.join();";
		echo "	 make_ajax_call('/components/driver_orders/assign_drivers.php','ajax_request=claim&timezone=$timezone&min_datetime=$min_datetime&max_datetime=$max_datetime&claim_pin=' + cpn + '&claim_list=' + claim_list,'claim_callback','vars'); ";
//echo "alert('Order IDs sent to the printer:'+order_ids_list_str);";//Strange if you remove this then duplicate printing often occurs, with it, not so much. Weird.
		echo "   ajaxGetAndPrintReceiptForOrderIDs(order_ids_list_str);";
		echo "	 orders_selected = ''; ";
		echo "} ";
		//Inserted by Brian D.
		echo "function ajaxGetAndPrintReceiptForOrderIDs(order_ids){";
		echo "   var postURLQueryString = 'ajax_request=get_receipt_str&sec=".$sec_hash."&loc_id=".$loc_id."&dataname=".$dataname."';";
		echo "   var callType = 'text';";
		echo "   var sendType = 'post';";
		echo "   make_ajax_call('/components/driver_orders/assign_drivers.php?order_ids='+order_ids, postURLQueryString, 'receiptsPrintCallback', callType, sendType);";
		echo "}";

		echo "function receiptsPrintCallback(returnStringsJSONStr){";
		echo "    var returnedJSONObj = JSON.parse(returnStringsJSONStr);";
		echo "    var receiptStringsArr  = returnedJSONObj.receiptStrings;";
		echo "    var allReceiptsSingleStringDoCommand = '_DO:cmd=print&order_id=encoded_order_id&print_string=active:';";
		echo "    for(var i = 0;i<receiptStringsArr.length;i++){";
		echo "         var currReceiptStr = receiptStringsArr[i].replace(/:/g,'[c').replace(/\\n/g,':');";
//echo " alert('adding string:'+currReceiptStr);";
		echo "         allReceiptsSingleStringDoCommand += encodeURIComponent(currReceiptStr)+':::';";
		echo "         if(i < receiptStringsArr.length - 1) allReceiptsSingleStringDoCommand += ':[new_print_job]:receipt:';";
		echo "    }";
//echo "    alert('Full allReceiptsDoCommand:'+allReceiptsSingleStringDoCommand);";
		echo "    window.location = allReceiptsSingleStringDoCommand;";
		echo "";
		echo "}";
		//End Insert

		echo "function claim_callback(cvars) { ";
		echo "	 prevent_pin_timeout('refresh'); ";
		echo "} ";
		echo "function order_callback(cvars) { ";
		echo "	 var str = ''; ";
		echo "  var draw_count = 0; ";
		echo "	 str += '<table cellpadding=6><tr>'; ";
		echo "	 for(var i=1; i<=cvars['order_count']; i++) { ";
		echo "	 	var order_id = cvars['order_id' + i]; ";
		echo "		str += '<td valign=\"top\">'; ";
		echo "		if(obj_id_order_selected(order_id)) { setborder = 'solid 2px #0000aa'; setbgcolor = '#ddeeff'; } else { setborder = 'solid 1px #000055'; setbgcolor = '#ffffff'; } ";
		echo "		str += '<table style=\"background-color:' + setbgcolor + '; border:' + setborder + '\" onclick=\"select_order(this)\" id=\"order_table_' + order_id + '\">'; ";
		echo "		str += '<tr><td align=\"center\"><b>' + order_id + '</b></td></tr>'; ";
		echo "		str += '<tr><td align=\"left\" valign=\"top\" style=\"font-size:12px; width:140px; height:140px; overflow:none\">'; ";
		echo "		str += '<font style=\"color:#000077; font-weight:bold\">' + cvars['order_customer_name' + i] + '</font>'; ";
		echo "		str += '<br><font style=\"color:#007700;\">' + cvars['order_address' + i] + '</font>'; ";
		echo "		str += '<br><font style=\"color:#007700;\">' + cvars['order_city' + i] + ' ' + cvars['order_state' + i] + ' ' + cvars['order_zip' + i] + '</font>'; ";
		echo "		var order_count = cvars['order_cart' + i + '_count']; ";
		echo "		for(var n=1; n<=order_count; n++) { ";
		echo "			var cart_item = cvars['order_cart' + i + '_item' + n].split('|'); ";
		echo "			str += '<br>' + cart_item[0] + ' x ' + cart_item[2]; ";
		echo "		} ";
		echo "		str += '</td></tr></table>'; ";
		echo "		str += '</td>'; ";
		echo "		draw_count++; ";
		echo "		if(draw_count % 5 == 0) { str += '</tr><tr>'; } ";
		echo "	 } ";
		echo "	 str += '</tr></table>'; ";
		echo "		if(cvars['order_count'] < 1) str = 'No Current Delivery Orders'; ";
		echo "	 	document.getElementById('output').innerHTML = '<br><br>' + str;";
		//echo "	 var current_timestamp = cvars['time']; ";
		//echo "	 document.getElementById('debug').innerHTML = '<font style=\"color:#000088\">' + document.getElementById('debug').innerHTML + ' - ' + current_timestamp + '</font>'; ";
		echo "} ";
		echo "function refresh() { ";
		echo "	return '1'; ";
		echo "} ";
		echo "function refresh_window() { ";
		echo "	window.location = '_DO:cmd=load_url&c_name=$comp_name&f_name=driver_orders/assign_drivers.php'; ";
		echo "} ";
		echo "setInterval('tick_tock()',1000); ";
		echo "</script>";
	}

	/*
		//Inserted by Brian D.
		if(!empty($_POST['print_order_ids'])){
			$orderIDsToPrint = explode(",",$_POST['print_order_ids']);
			foreach($orderIDsToPrint as $currID){
				$currIDEnc = rawurlencode($currID);
				echo "window.location = '_DO:cmd=print&order_id=$currIDEnc";
			}
		}
		//End Insert
	 */
?>
