<?php
if( !$in_lavu ){
	return;
}
global $location_info, $locationid;
session_start();
//require_once dirname(dirname(dirname(__FILE__))) . '/resources/dimensional_form.php';


// NEW THINGS ADDED
$asPrintString='';
define("TABLE_1_COLUMN_1_PAD_LEFT",   8);
define("TABLE_2_COLUMN_1_PAD_RIGHT",  4);
define("TABLE_2_COLUMN_2_PAD_RIGHT", 23);
define("TABLE_3_COLUMN_1_PAD_LEFT",  27);//First column should be as long as the table2's 1st and 2nd column.
//

if (! function_exists("format_currency")){
	function format_currency( $monitary_amount, $dont_use_monetary_symbol = false ) {
		global $location_info;

		$left_or_right = $location_info['left_or_right'];
		$monitary_symbol =  htmlspecialchars( $location_info['monitary_symbol'] );
		$left_side = "";
		$right_side = "";
		if( !$dont_use_monetary_symbol ){
			if( $left_or_right == 'left' ){
				$left_side = $monitary_symbol;
			} else {
				$right_side = $monitary_symbol;
			}
		}

		if( $location_info['round_up_or_down'] == 'Down' ){
			$rounding_type = PHP_ROUND_HALF_DOWN;
		}

		$precision = $location_info['disable_decimal'];
		$decimal_char = '.';
		$thousands_char = ',';
		if( $location_info['decimal_char'] != '.' ){
			$decimal_char = $location_info['decimal_char'];
			$thousands_char = '.';
		}

		$number = $monitary_amount;
		{  //if next not significant digit is 5
			$number_parts = explode('.', $monitary_amount.'');
			if( !isset($number_parts[1]) ){
				$number_parts[1] = str_pad('', $precision, '0');
				$left_or_right = isset($location_info['left_or_right'])?$location_info['left_or_right']:'left';
				$monitary_symbol =  htmlspecialchars( isset($location_info['monitary_symbol'])?$location_info['monitary_symbol']:'$' );
				$left_side = "";
				$right_side = "";
				if( !$dont_use_monetary_symbol ){
					if( $left_or_right == 'left' ){
						$left_side = $monitary_symbol;
					} else {
						$right_side = $monitary_symbol;
					}
				}

				$round_up_or_down = isset($location_info['round_up_or_down'])?$location_info['round_up_or_down']:'Down';
				if( $round_up_or_down  == 'Down' ){
					$rounding_type = PHP_ROUND_HALF_DOWN;
				}
				$post_decimal = substr( $number_parts[1], 0, $precision );
				$pre_decimal = $number_parts[0];

				$number = ($pre_decimal . '.' . $post_decimal)*1;
				$precision = isset($location_info['disable_decimal'])?$location_info['disable_decimal']:2;
				$decimal_char = isset($location_info['decimal_char'])?$location_info['decimal_char']:'.';
				$thousands_char = ',';
				if( $decimal_char != '.' && $decimal_char != ' ' ){
					$thousands_char = '.';
				}

		//$number = floor( $number * pow( 10, $precision ) ) / pow( 10, $precision );
			}
		}
		return $left_side . number_format($number, $precision, $decimal_char, $thousands_char) . $right_side;
	}
}




function filterRepeatedTimes( $time_iso, $time ){
	static $last_time = '';
	if( $last_time != $time_iso ){
		$last_time = $time_iso;
		return "<time datetime=\"{$time_iso}\" title=\"{$time}\"></time>";
	}
	return '';
}

function timeOrderableItemSort( $a, $b ){
	$timea = strtotime(timestampToISO( $a->time() ));
	$timeb = strtotime(timestampToISO( $b->time() ));

	if( $timea < $timeb ){
		return -1;
	}
	if( $timea > $timeb ){
		return 1;
	}

	if( $a instanceof OrderItem && $b instanceof OrderItem ){
		if( $a->showQuantity() && !$b->showQuantity() ){
			return -1;
		}

		if( !$a->showQuantity() && $b->showQuantity() ){
			return 1;
		}

		if( $a->showAmount() && !$b->showAmount() ){
			return -1;
		}

		if( !$a->showAmount() && $b->showAmount() ){
			return 1;
		}
	}

	if( $a instanceof ActionLog && !($b instanceof ActionLog ) ){
		return -1;
	}

	if( !($a instanceof ActionLog) && $b instanceof ActionLog ){
		return 1;
	}

	if( $a instanceof OrderSendLog && !($b instanceof OrderSendLog ) ){
		return -1;
	}

	if( !($a instanceof OrderSendLog) && $b instanceof OrderSendLog ){
		return 1;
	}

	if( $a instanceof EditAfterSendLog && !($b instanceof EditAfterSendLog ) ){
		return -1;
	}

	if( !($a instanceof EditAfterSendLog) && $b instanceof EditAfterSendLog ){
		return 1;
	}

	return 0;
}

function timestampToISO( $time ){
	global $location_info;
	$tz = date_default_timezone_get();
	if( !empty( $location_info['timezone'] ) ){
		date_default_timezone_set( $location_info['timezone'] );
	}

	$ts = strtotime( $time );
	$result =  date('c', $ts);
	date_default_timezone_set( $tz );
	return $result;
}


$isFirstTable3Line = true;
abstract class OrderBase {
	public function __get( $name ){
		if( isset( $this->$name ) ){
			return $this->$name;
		}
		return null;
	}

	public abstract function drawOrderDetailInteractive();
	public abstract function drawOrderTimelineLogs();
	public abstract function drawOrderDetailPrintable();
	public abstract function time();

	protected function outputRow( $title, $value, $class=''){
		global $asPrintString;
		if( !empty($class) ){
			$class = " class=\"{$class}\"";
		}
		/*
			define("TABLE_3_COLUMN_1_PAD_LEFT",  31);//first column should be as long as the table2's 1st and 2nd column.
		*/

		global $isFirstTable3Line;
		if($isFirstTable3Line){
			$asPrintString .= "\n";
			$isFirstTable3Line = false;
		}
		if(strpos($title, 'Paid with') === 0){

			//Parsing the time paid variable from the custom html element.
			$regexMatches = array();
			preg_match('/title="(.*)">/',$title,$regexMatches);
			$timePaid = $regexMatches[1];

			$title = preg_replace('/<.*>/', "", $title);//Remove all elements.

			$paidWithPart = substr($title, 0, strpos($title, ':'));
			$atTimePart   = substr($title, strpos($title, ':') + 1, strlen($title));
			$asPrintString .= str_pad($paidWithPart.":",TABLE_3_COLUMN_1_PAD_LEFT," ", STR_PAD_LEFT).$value."\n";
			$asPrintString .= str_pad("At: ".$timePaid,TABLE_3_COLUMN_1_PAD_LEFT," ", STR_PAD_LEFT)."\n";
		}else{
			$asPrintString .= str_pad($title.":",TABLE_3_COLUMN_1_PAD_LEFT," ", STR_PAD_LEFT).$value."\n";
		}


		

//echo "Title:".$title;
		echo
			"<tr{$class}>
				<td></td>
				<td class=\"alignRight\">{$title}:</td>
				<td class=\"alignRight\">{$value}</td>
			</tr>";
	}
}

class OrderPayment extends OrderBase {
	protected
		$payTypeID,
		$datetime,
		$paymentType,
		$description,
		$voided,
		$action,
		$amount,
		$tip,
		$total;

	private function __construct(){
		$this->payTypeID = 0;
		$this->datetime = 0;
		$this->paymentType = '';
		$this->description = '';
		$this->voided = false;
		$this->action = '';
		$this->amount = 0;
		$this->tip = 0;
		$this->total = 0;
	}

	public static function createOrderPayment( $payTypeID, $datetime, $paymentType, $description, $voided, $action, $amount, $tip, $total ){
		$result = new OrderPayment();
		$result->payTypeID = $payTypeID;
		$result->datetime = $datetime;
		$result->paymentType = $paymentType;
		$result->description = $description;
		$result->voided = $voided == '1';
		$result->action = $action;
		if( $action == 'Refund' ){
			$result->amount = -$amount;
			$result->tip = -$tip;
			$result->total = -$total;
		} else if( $action == 'Sale' ) {
			$result->amount = $amount;
			$result->tip = $tip;
			$result->total = $total;
		}

		return $result;
	}

	private function outputDetail( $title, $amount ){
		$this->outputRow( $title, format_currency($amount), $this->voided ? 'voided' : '');
	}

	public function drawOrderDetailInteractive(){
		if( $this->action == 'Void' ){
			return;
		}
		$title_str = '';

		if( $this->payTypeID <= '2' ){
			if( $this->action == 'Refund' ){
				$title_str .= 'Refund with ';
			} else if ( $this->action == 'Sale' ){
				$title_str .= 'Paid with ';
			}
		}

		$title_str .= $this->paymentType;
		if( $this->description ){
			$title_str .= " ($this->description)";
		}
		$datetime_iso = timestampToISO( $this->datetime );
		$title_str .=  ":<br />At <time datetime=\"{$datetime_iso}\" title=\"{$this->datetime}\"></time>";
		$this->outputRow( $title_str, format_currency($this->amount), $this->voided ? 'voided' : '');
		if( !empty($this->tip) && $this->tip != 0 ){
			echo "
		<tr>
			<td></td>
			<td class=\"alignRight\">
				<div style=\"position: relative;\">
					<table class=\"payment_detail\">
						<tbody>";
			$this->outputDetail( "Tip", $this->tip );
			$this->outputDetail( "Total", $this->total );
			echo "</tbody>
					</table>
				</div>
			</td>
			<td>
			</td>
		</tr>";
		}
	}

	public function drawOrderDetailPrintable(){

	}

	public function drawOrderTimelineLogs(){

	}

	public function time(){
		return $this->datetime;
	}
}

class OrderDiscount extends OrderBase {
	protected
		$title,
		$value,
		$type,
		$amount;

	private function __construct(){
		$this->title = '';
		$this->value = 0;
		$this->type = 'd';
		$this->amount = 0;
	}

	public static function createDiscount( $title, $value, $type, $amount ){
		$result = new OrderDiscount();
		$result->title = $title;
		$result->value = $value;
		$result->type = $type;
		$result->amount = $amount;
		return $result;
	}

	public function drawOrderDetailInteractive(){

	}

	public function drawOrderDetailPrintable(){

	}

	public function drawOrderTimelineLogs(){}
	public function time(){}
}

class OrderTaxProfile extends OrderBase {
	protected
		$name,
		$percentage,
		$consideredSubtotal,
		$amount;

	private function __construct(){
		$this->name = '';
		$this->percentage = 0;
		$this->consideredSubtotal = 0;
		$this->amount = 0;
	}

	public static function createOrderTaxProfile( $name, $percentage, $consideredSubtotal, $amount ){
		$result = new OrderTaxProfile();
		$result->name = $name;
		$result->percentage = $percentage;
		$result->consideredSubtotal = $consideredSubtotal;
		$result->amount = $amount;

		return $result;
	}

	public function addToConsideredSubtotal( $subtotal ){
		$this->consideredSubtotal += $subtotal;
	}

	public function addToTotal( $amount ){
		$this->amount += $amount;
	}

	public function drawOrderDetailInteractive(){
		$percentage_str = $this->percentage * 100;
		$considered_subtotal_str = format_currency( $this->consideredSubtotal );
		$this->outputRow( $this->name . " ({$percentage_str}% of {$considered_subtotal_str})", format_currency($this->amount) );
	}

	public function drawOrderDetailPrintable(){

	}

	public function drawOrderTimelineLogs(){}
	public function time(){}
}

class OrderItem extends OrderBase {
	protected
		$deviceTime,
		$quantity,
		$name,
		$subtotal,
		$showQuantity,
		$showAmount,
		$isVoided;

	private function __construct(){
		$this->deviceTime = '';
		$this->quantity = 0;
		$this->name = '';
		$this->subtotal = 0;
		$this->showQuantity = false;
		$this->showAmount = false;
		$this->isVoided = false;
	}

	public static function createOrderItem( $deviceTime, $quantity, $name, $subtotal, $showQuantity=false, $showAmount=false, $isVoided=false ){
		$result = new OrderItem();
		$result->deviceTime = $deviceTime;
		$result->quantity = $quantity;
		$result->name = $name;
		$result->subtotal = $subtotal;
		$result->showQuantity = $showQuantity;
		$result->showAmount = $showAmount;
		$result->isVoided = $isVoided;

		return $result;
	}

	public function showQuantity(){
		return $this->showQuantity;
	}

	public function showAmount(){
		return $this->showAmount;
	}

	public function isVoided(){
		return $this->isVoided;
	}

	public function drawOrderDetailInteractive(){
		$class = '';
		if( $this->isVoided ){
			if( $this->subtotal < 0 ){
				$class = ' class="discount voided"';
			} else {
				$class = ' class="voided"';
			}
		} else {
			if( $this->subtotal < 0 ){
				$class = ' class="discount"';
			}
		}
		$quantity_str = ($this->showQuantity) ? htmlspecialchars($this->quantity) : '';
		$amount_str = ($this->showAmount && $this->subtotal*1 != 0) ? htmlspecialchars(format_currency($this->subtotal)) : '';
		$name = $this->name;
		echo <<<HTML
			<tr{$class}>
				<td>{$quantity_str}</td>
				<td>{$name}</td>
				<td class="alignRight">{$amount_str}</td>
			</tr>
HTML;
		static $why = 0;
		global $asPrintString;
		$stringToBuild = str_pad($quantity_str, TABLE_2_COLUMN_1_PAD_RIGHT, " ", STR_PAD_RIGHT) . str_pad($name, TABLE_2_COLUMN_2_PAD_RIGHT, " ", STR_PAD_RIGHT) . $amount_str."\n";
		$asPrintString .= $stringToBuild;
	}

	public function drawOrderDetailPrintable(){
		
	}

	public function drawOrderTimelineLogs(){
		$time_iso = timestampToISO( $this->time() );

		$class = '';
		if( $this->isVoided ){
			if( $this->subtotal < 0 ){
				$class = ' class="discount voided"';
			} else {
				$class = ' class="voided"';
			}
		} else {
			if( $this->subtotal < 0 ){
				$class = ' class="discount"';
			}
		}

		$quantity_str = ($this->showQuantity) ? htmlspecialchars($this->quantity) : '';
		$amount_str = ($this->showAmount && $this->subtotal*1 != 0) ? htmlspecialchars(format_currency($this->subtotal)) : '';
		$time_str = ($this->showQuantity && $this->subtotal*1 != 0) ? filterRepeatedTimes($time_iso, $this->time()) : '';
		$name = $this->name;
		echo <<<HTML
		<tr>
			<td>{$time_str}
			</td>
			<td></td>
			<td>
				<table style="width: 100%">
					<tbody>
						<tr{$class}>
							<td>{$quantity_str}</td>
							<td>{$name}</td>
							<td class="alignRight">{$amount_str}</td>
						</tr>
					</tbody>
				</table>
			</td>
		</tr>
HTML;
		//Table 2
		/*
			define("TABLE_2_COLUMN_1_PAD_RIGHT",  4);
			define("TABLE_2_COLUMN_2_PAD_RIGHT", 27);
		*/
	}

	public function time(){
		return $this->deviceTime;
	}
}

class OrderCheck extends OrderBase {
	protected
		$check,
		$order_id,
		$items,
		$subtotal,
		$discount,
		$autoGratuityPercentage,
		$autoGratuityAmount,
		$taxExempt,
		$taxExemptReason,
		$taxes,
		$total,
		$payments;

	private function __construct( ){
		$this->check = null;
		$this->order_id = null;
		$this->items = array();
		$this->subtotal = null;
		$this->discount = null;
		$this->autoGratuityPercentage = 0;
		$this->autoGratuityAmount = 0;
		$this->taxExempt = false;
		$this->taxExemptReason = '';
		$this->taxes = array();
		$this->taxesIncluded = array();
		$this->total = null;
		$this->payments = array();
	}

	private static function createOrderCheck( $order_id, $check, $items, $subtotal, $discount, $autoGratuityPercentage, $autoGratuityAmount, $taxExempt, $taxExemptReason, $taxes, $taxesIncluded, $total, $payments ){
		$result = new OrderCheck();
		$result->order_id = $order_id;
		$result->check = $check;
		$result->items = $items;
		$result->subtotal = $subtotal;
		$result->discount = $discount;
		$result->autoGratuityPercentage = $autoGratuityPercentage;
		$result->autoGratuityAmount = $autoGratuityAmount;
		$result->taxExempt = $taxExempt;
		$result->taxExemptReason = $taxExemptReason;
		$result->taxes = $taxes;
		$result->taxesIncluded = $taxesIncluded;
		$result->total = $total;
		$result->payments = $payments;

		return $result;
	}

	public static function createOrderChecks( $order_stub, $split_check_details, $order_contents, $cc_transactions ){
		global $location_info;
		$result = array();

		foreach( $split_check_details as $split_check_details_row => $split_check_detail ){

			$items = array();
			$taxes = array();
			$taxesIncluded = array();
			foreach( $order_contents as $order_contents_row => $order_content ){
				if( $order_content['check'] != $split_check_detail['check'] && $order_content['check'] != '0' ){
					continue;
				}

				if( $order_content['quantity']*1 > 0 ){
					$quantity = $order_content['quantity'];
					$subtotal = $order_content['subtotal'];
					$subtotal_with_mods = $order_content['subtotal_with_mods'];
					if( $order_content['check'] == '0' ){
						if( $quantity / $order_stub['no_of_checks'] < 1 ){
							$quantity  .= '/'.$order_stub['no_of_checks'];
						} else {
							$quantity /= $order_stub['no_of_checks'];
						}
						$subtotal /= $order_stub['no_of_checks'];
						$subtotal_with_mods /= $order_stub['no_of_checks'];
					}
					$includedModifierPrice =  ($location_info['display_forced_modifier_prices'] == '1');
					$items[] = OrderItem::createOrderItem($order_content['device_time'], $quantity, $order_content['item'], $includedModifierPrice ? $subtotal : $subtotal_with_mods,true, true);

					if( !empty( $order_content['options'] ) ){
						$items[] = OrderItem::createOrderItem($order_content['device_time'], 1, $order_content['options'], $order_content['forced_modifiers_price'], false, $includedModifierPrice);
					}

					if( !empty( $order_content['special'] ) ){
						$items[] = OrderItem::createOrderItem($order_content['device_time'], 1, $order_content['special'], $order_content['modify_price'], false, $includedModifierPrice);
					}

					if( !empty( $order_content['idiscount_id'] ) ){
						$items[] = OrderItem::createOrderItem($order_content['device_time'], 1, $order_content['idiscount_sh'], -$order_content['idiscount_amount'], false, true);
					}

					{
						$all_tax_profiles_pieces = explode(';;', $order_content['apply_taxrate']);
						$i = 1;
						foreach( $all_tax_profiles_pieces as $all_tax_profiles_pieces_row => $all_tax_profiles_piece ){
							$tax_profile_pieces = explode('::', $all_tax_profiles_piece);
							// 'id'
							// 'name'
							// 'rate'
							// 'amount'
							// 'subtotal'
							// 'name'
							// 'calc'
							// 'type'
							// 'stack'
							// 'tierType1'
							// 'tierValue1'
							// 'calc2'

							$tax_profile_unique_idenfier = $tax_profile_pieces[0] . ':' . $tax_profile_pieces[1] . ':' . $tax_profile_pieces[2];
							if( $order_content['tax_inclusion'] != '1' ){
								if( !isset( $taxes[$tax_profile_unique_idenfier] ) ){
									$taxes[$tax_profile_unique_idenfier] = OrderTaxProfile::createOrderTaxProfile( $tax_profile_pieces[1], $tax_profile_pieces[2], 0, 0);
								}

								if( $order_content['check'] == '0' ){
									$taxes[$tax_profile_unique_idenfier]->addToConsideredSubtotal( $order_content['tax_subtotal'.$i]/$order_stub['no_of_checks'] );
									$taxes[$tax_profile_unique_idenfier]->addToTotal( $order_content['tax'.$i]/$order_stub['no_of_checks'] );
								} else {
									$taxes[$tax_profile_unique_idenfier]->addToConsideredSubtotal( $order_content['tax_subtotal'.$i] );
									$taxes[$tax_profile_unique_idenfier]->addToTotal( $order_content['tax'.$i] );
								}
							} else {
								if( !isset( $taxesIncluded[$tax_profile_unique_idenfier] ) ){
									$taxesIncluded[$tax_profile_unique_idenfier] = OrderTaxProfile::createOrderTaxProfile( $tax_profile_pieces[1], $tax_profile_pieces[2], 0, 0);
								}

								if( $order_content['check'] == '0' ){
									$taxesIncluded[$tax_profile_unique_idenfier]->addToConsideredSubtotal( $order_content['tax_subtotal'.$i]/$order_stub['no_of_checks']);
									$taxesIncluded[$tax_profile_unique_idenfier]->addToTotal( $order_content['tax'.$i]/$order_stub['no_of_checks'] );
								} else {
									$taxesIncluded[$tax_profile_unique_idenfier]->addToConsideredSubtotal( $order_content['tax_subtotal'.$i] );
									$taxesIncluded[$tax_profile_unique_idenfier]->addToTotal( $order_content['tax'.$i] );
								}
							}

							$i++;
						}
					}
				} 
				if( $order_content['void']*1 > 0  ){
					$includedModifierPrice =  ($location_info['display_forced_modifier_prices'] == '1');
					$items[] = OrderItem::createOrderItem($order_content['device_time'], $order_content['void'], $order_content['item'], 0, true, true, true);

					if( !empty( $order_content['options'] ) ){
						$items[] = OrderItem::createOrderItem($order_content['device_time'], 1, $order_content['options'], 0, false, $includedModifierPrice, true);
					}

					if( !empty( $order_content['special'] ) ){
						$items[] = OrderItem::createOrderItem($order_content['device_time'], 1, $order_content['special'], 0, false, $includedModifierPrice, true);
					}

					if( !empty( $order_content['idiscount_id'] ) ){
						$items[] = OrderItem::createOrderItem($order_content['device_time'], 1, $order_content['idiscount_sh'], 0, false, true, true);
					}
				}
			}

			$discount = null;
			if( !empty( $split_check_detail['discount_id'] ) ){
				$discount = OrderDiscount::createDiscount( $split_check_detail['discount_sh'], $split_check_detail['discount_value'], $split_check_detail['discount_type'], $split_check_detail['discount']);
			}

			$payments = array();
			foreach( $cc_transactions as $cc_transactions_row => $cc_transaction ){
				if( $cc_transaction['check'] != $split_check_detail['check'] ){
					continue;
				}

				$payments[] = OrderPayment::createOrderPayment( $cc_transaction['pay_type_id'], $cc_transaction['datetime'], $cc_transaction['pay_type'], $cc_transaction['card_desc'], $cc_transaction['voided'], $cc_transaction['action'], $cc_transaction['total_collected'], $cc_transaction['tip_amount'], $cc_transaction['total_collected']+$cc_transaction['tip_amount']);
			}


			$check = OrderCheck::createOrderCheck( $split_check_detail['order_id'], $split_check_detail['check'], $items, $split_check_detail['subtotal'], $discount, $split_check_detail['gratuity_percent'], $split_check_detail['gratuity'], $order_stub['tax_exempt'] == '1', $order_stub['exemption_name'], $taxes, $taxesIncluded, $split_check_detail['check_total'], $payments );

			$result[] = $check;
		}

		return $result;
	}

	private function outputTaxes(){
		if( $this->taxExempt ){
			$this->outputRow( "Tax Exempt ($this->taxExemptReason)", format_currency( 0 ) );
		} else {
			foreach( $this->taxes as $tax_row => $tax ){
				$tax->drawOrderDetailInteractive();
			}
		}
	}

	private function outputTaxesIncluded(){
		if( !count( $this->taxesIncluded ) ){
			return;
		}
		echo '<tr><td colspan="100" style="text-align: center;">Included Taxes</td></tr>';
		foreach( $this->taxesIncluded as $tax_row => $tax ){
			$tax->drawOrderDetailInteractive();
		}

		$this->outputSepatator();
	}

	private function outputPayments(){
		$total_payments = 0;

		foreach( $this->payments as $payment_row => $payment ){
			$payment->drawOrderDetailInteractive();
			if( !$payment->voided )
				$total_payments += $payment->amount;
		}
		$this->outputRow('Total Paid', format_currency( $total_payments ) );

		$this->outputSepatator();
		if( $total_payments > $this->total ){
			$this->outputRow( 'Overpaid', format_currency($total_payments - $this->total), 'overpaid' );
		} else if ( $total_payments < $this->total ){
			$this->outputRow( 'Amount Due', format_currency($this->total - $total_payments), 'amount_due' );
		}
	}

	private function outputSepatator(){
		echo '<tr><td><br/></td></tr>';
	}

	public function drawOrderDetailInteractive(){
		global $location_info;
		foreach( $this->items as $item_row => $item ){
			$item->drawOrderDetailInteractive();
		}
		$this->outputSepatator();

		$this->outputRow( 'Subtotal', format_currency( $this->subtotal ) );
		if( $this->discount ){
			$this->outputRow( $this->discount->title, format_currency( -$this->discount->amount ), 'discount' );
			$this->outputRow( "After Discount", format_currency( $this->subtotal - $this->discount->amount ) );
		}

		if( !empty($this->autoGratuityAmount) ){
			$gratuity_percent_str = $this->autoGratuityPercentage * 100;
			$this->outputRow( $location_info['gratuity_label'] . " ({$gratuity_percent_str}%)", format_currency( $this->autoGratuityAmount ) );
		}
		$this->outputTaxes();
		$this->outputRow( 'Total', format_currency( $this->total ) );
		$this->outputSepatator();
		$this->outputTaxesIncluded();
		$this->outputPayments();
	}

	public function items(){
		return $this->items;
	}

	public function drawOrderDetailPrintable(){
		
	}

	public function drawOrderTimelineLogs(){

	}

	public function time(){
		return null;
	}
}

class OrderSendLog extends OrderBase {
	protected
		$order_id,
		$location,
		$location_id,
		$tablename,
		$total,
		$server,
		$server_id,
		$send_server,
		$sender_server_id,
		$time_sent,
		$server_time,
		$device_udid,
		$items_sent,
		$islid,
		$course,
		$resend,
		$redirection;

	private function __construct(){
		$this->order_id = null;
		$this->location = null;
		$this->location_id = null;
		$this->tablename = null;
		$this->total = null;
		$this->server = null;
		$this->server_id = null;
		$this->send_server = null;
		$this->sender_server_id = null;
		$this->time_sent = null;
		$this->server_time = null;
		$this->device_udid = null;
		$this->items_sent = array();
		$this->islid = null;
		$this->course = null;
		$this->resend = null;
		$this->redirection = null;
	}

	public static function createOrderSendLogs( $order_send_logs ) {
		$result = array();
		for( $i = 0; $i < count( $order_send_logs ); $i++ ){
			$result[] = OrderSendLog::createOrderSendLog( $order_send_logs[$i] );
		}
		return $result;
	}

	public static function createOrderSendLog( $order_send_log_row ){
		$result = new OrderSendLog();
		$result->order_id = $order_send_log_row['order_id'];
		$result->location = $order_send_log_row['location'];
		$result->location_id = $order_send_log_row['location_id'];
		$result->tablename = $order_send_log_row['tablename'];
		$result->total = $order_send_log_row['total'];
		$result->server = $order_send_log_row['server'];
		$result->server_id = $order_send_log_row['server_id'];
		$result->send_server = $order_send_log_row['full_name'];
		$result->sender_server_id = $order_send_log_row['sender_server_id'];
		$result->time_sent = $order_send_log_row['time_sent'];
		$result->server_time = $order_send_log_row['server_time'];
		$result->device_udid = $order_send_log_row['device_udid'];
		$result->items_sent = explode('|*|', $order_send_log_row['items_sent']);
		$result->islid = $order_send_log_row['islid'];
		$result->course = $order_send_log_row['course'];
		$result->resend = $order_send_log_row['resend'];
		$result->redirection = $order_send_log_row['redirection'];
		return $result;
	}

	public function drawOrderDetailInteractive(){
		return '';
	}
	public function drawOrderTimelineLogs() {
		$time_iso = timestampToISO( $this->time() );
		$time = filterRepeatedTimes( $time_iso, $this->time() );
		echo <<<HTML
		<tr>
			<td>{$time}</td>
			<td>{$this->send_server}</td>
			<td>Send Log:<br />
				&nbsp;&nbsp;&nbsp;{$items_sent}
			</td>
		</tr>
HTML;
	}
	public function drawOrderDetailPrintable(){}
	public function time() {
		return $this->time_sent;
	}
}

class EditAfterSendLog extends OrderBase {
	protected
		$note,
		$order_id,
		$location,
		$location_id,
		$item,
		$options,
		$special,
		$modify_price,
		$quantity,
		$time,
		$server,
		$server_id,
		$auth_by,
		$server_time;

	private function __construct(){
		$this->note = null;
		$this->order_id = null;
		$this->location = null;
		$this->location_id = null;
		$this->item = null;
		$this->options = null;
		$this->special = null;
		$this->modify_price = null;
		$this->quantity = null;
		$this->time = null;
		$this->server = null;
		$this->server_id = null;
		$this->auth_by = null;
		$this->server_time = null;
	}

	public static function createEditAfterSendLog( $edit_after_send_log ) {
		$result = new EditAfterSendLog();
		$result->note = $edit_after_send_log['note'];
		$result->order_id = $edit_after_send_log['order_id'];
		$result->location = $edit_after_send_log['location'];
		$result->location_id = $edit_after_send_log['location_id'];
		$result->item = $edit_after_send_log['item'];
		$result->options = $edit_after_send_log['options'];
		$result->special = $edit_after_send_log['special'];
		$result->modify_price = $edit_after_send_log['modify_price'];
		$result->quantity = $edit_after_send_log['quantity'];
		$result->time = $edit_after_send_log['time'];
		$result->server = $edit_after_send_log['full_name'];
		$result->server_id = $edit_after_send_log['server_id'];
		$result->auth_by = $edit_after_send_log['auth_by'];
		$result->server_time = $edit_after_send_log['server_time'];
		return $result;
	}

	public static function createEditAfterSendLogs( $edit_after_send_logs  ) {
		$result = array();
		for( $i = 0; $i < count( $edit_after_send_logs ); $i++ ){
			$result[] = EditAfterSendLog::createEditAfterSendLog( $edit_after_send_logs[$i] );
		}
		return $result;
	}

	public function drawOrderDetailInteractive(){
		return '';
	}
	public function drawOrderTimelineLogs() {
		$time_iso = timestampToISO( $this->time() );
		$time = filterRepeatedTimes( $time_iso, $this->time() );
		echo <<<HTML
			<tr>
				<td>{$time}</td>
				<td>{$this->server}</td>
				<td>{$this->note}<br />
					Authorized by {$this->auth_by}
				</td>
			</tr>
HTML;
	}
	public function drawOrderDetailPrintable(){}
	public function time() {
		return $this->time;
	}
}

class ActionLog extends OrderBase {
	protected
		$action,
		$loc_id,
		$order_id,
		$time,
		$user,
		$user_id,
		$server_time,
		$item_id,
		$details,
		$check,
		$device_udid,
		$details_short;

	private function __construct(){
		$this->action = null;
		$this->loc_id = null;
		$this->order_id = null;
		$this->time = null;
		$this->user = null;
		$this->user_id = null;
		$this->server_time = null;
		$this->item_id = null;
		$this->details = null;
		$this->check = null;
		$this->device_udid = null;
		$this->details_short = null;
	}

	public static function createActionLog( $action_log ){
		$result = new ActionLog();
		$result->action = $action_log['action'];
		$result->loc_id = $action_log['loc_id'];
		$result->order_id = $action_log['order_id'];
		$result->time = $action_log['time'];
		$result->user = $action_log['full_name'];
		$result->user_id = $action_log['user_id'];
		$result->server_time = $action_log['server_time'];
		$result->item_id = $action_log['item_id'];
		$result->details = $action_log['details'];
		$result->check = $action_log['check'];
		$result->device_udid = $action_log['device_udid'];
		$result->details_short = $action_log['details_short'];
		return $result;
	}

	public static function createActionLogs( $action_logs ){
		$result = array();
		for( $i = 0; $i < count( $action_logs ); $i++ ){
			$result[] = ActionLog::createActionLog( $action_logs[$i] );
		}

		return $result;
	}

	public function drawOrderDetailInteractive(){
		return '';
	}
	public function drawOrderTimelineLogs() {
		$time_iso = timestampToISO( $this->time() );
		$time = filterRepeatedTimes( $time_iso, $this->time() );
		echo <<<HTML
			<tr>
				<td>{$time}</td>
				<td>{$this->user}</td>
				<td>
					{$this->action}<br />
					{$this->details}
				</td>
			</tr>
HTML;
	}
	public function drawOrderDetailPrintable(){}
	public function time() {
		return $this->time;
	}
}

class OrderStub extends OrderBase {
	protected
		$tag,
		$order_id,
		$tableName,
		$opened,
		$closed,
		$void,
		$guests,
		$server,
		$cashier,
		$checks,
		$send_logs,
		$action_logs,
		$edit_after_send_logs;

	private function __construct(){
		$this->tag = '';
		$this->order_id = '';
		$this->tableName = 'Quick Serve';
		$this->opened = time();
		$this->closed = time();
		$this->void = false;
		$this->guests = 0;
		$this->server = '';
		$this->cashier = '';
		$this->checks = array();
		$this->send_logs = array();
		$this->action_logs = array();
		$this->edit_after_send_logs = array();
	}

	public static function createOrderStub( $tag, $order_id, $tableName, $opened, $closed, $void, $guests, $server, $cashier, $checks, $send_logs, $action_logs, $edit_after_send_logs ){
		$result = new OrderStub();
		$result->tag = $tag;
		$result->order_id = $order_id;
		$result->tableName = $tableName;
		$result->opened = $opened;
		$result->closed = $closed;
		$result->void = $void;
		$result->guests = $guests;
		$result->server = $server;
		$result->cashier = $cashier;
		$result->checks = $checks;
		$result->send_logs = $send_logs;
		$result->action_logs = $action_logs;
		$result->edit_after_send_logs = $edit_after_send_logs;

		return $result;
	}

	public static function createOrderSubFromSources( $order_stub, $order_tags, $split_check_details, $order_contents, $cc_transactions, $send_logs, $action_logs, $edit_after_send_logs ){
		$checks = OrderCheck::createOrderChecks( $order_stub, $split_check_details, $order_contents, $cc_transactions );

		global $location_info;
		$order_tag = $location_info['dine_in_order_type_name'];		
		if( $order_stub['togo_status'] != '0' ){
			foreach( $order_tags as $key => $order_tag_element ){
				if( $order_tag_element['id'] == $order_stub['togo_status'] ){
					$order_tag = $order_tag_element['name'];
					break;
				}
			}
		}

		$send_logs = OrderSendLog::createOrderSendLogs( $send_logs );
		$action_logs = ActionLog::createActionLogs( $action_logs );
		$edit_after_send_logs = EditAfterSendLog::createEditAfterSendLogs( $edit_after_send_logs );

		return OrderStub::createOrderStub($order_tag, $order_stub['order_id'], $order_stub['tablename'], $order_stub['opened'], $order_stub['closed'], $order_stub['void'], $order_stub['guests'], $order_stub['server'], $order_stub['cashier'], $checks, $send_logs, $action_logs, $edit_after_send_logs );
	}

	public function drawOrderDetailInteractive(){
		$opened_iso = timestampToISO( $this->opened );
		$closed_iso = timestampToISO( $this->closed );

		$voided = ($this->void) ? '<div class="void_alert">Void</div>' : '';

		$opened_display = ($this->opened == '0000-00-00 00:00:00' || empty($this->opened)) ?  "<span title=\"{$this->opened}\">No Opened Time (ISSUE)</span>" : "<time datetime=\"{$opened_iso}\" title=\"{$this->opened}\"></time>";
		$closed_display = ($this->closed == '0000-00-00 00:00:00' || empty($this->closed)) ?  "<span title=\"{$this->closed}\">Still Open</span>" : "<time datetime=\"{$closed_iso}\" title=\"{$this->closed}\"></time>";
		echo <<<HTML
	<style type="text/css">
		.alignLeft {
			text-align: left;
		}

		.alignRight {
			text-align: right;
		}

		.alignCenter {
			text-align: center;
		}

		table > td {
			vertical-align: text-top;
			padding: 3px 5px;
		}

		tr.discount td {
			color: green;
		}

		tr.voided td {
			text-decoration: line-through;
		}

		tr.overpaid td, tr.amount_due td {
			color: red;
		}

		table.payment_detail {
			float: right;
			font-size: 0.8em;
		}
	</style>
	<div style="display: table">
		<table style='width: 100%;'>
			<tbody>
				<tr>
					<td colspan="100" class="alignCenter">{$this->tag}</td>
				</tr>
				<tr>
					<td class="alignRight">Order #:</td>
					<td class="alignLeft">{$this->order_id}</td>
				</tr>
				<tr>
					<td class="alignRight">Table:</td>
					<td class="alignLeft">{$this->tableName}</td>
				</tr>

				<tr>
					<td class="alignRight">Opened:</td>
					<td class="alignLeft">{$opened_display}</td>
				</tr>
				<tr>
					<td class="alignRight">Closed:</td>
					<td class="alignLeft">{$closed_display}</td>
				<tr>
					<td class="alignRight">Guests:</td>
					<td class="alignLeft">{$this->guests}</td>
				</tr>
				<tr>
					<td class="alignRight">Server:</td>
					<td class="alignLeft">{$this->server}</td>
				</tr>
				<tr>
					<td class="alignRight">Cashier:</td>
					<td class="alignLeft">{$this->cashier}</td>
				</tr>
			</tbody>
		</table>
		<br />
		{$voided}
		<br />
		<table style="width: 100%;">
			<tbody>
HTML;

		//define("TABLE_1_COLUMN_1_PAD_LEFT",  10);
		//error_log("Opened Display:".$opened_display);
		//Here we need to pull out the time from the html element using regex.
		preg_match('/.*title="(.*)">.*/', $opened_display, $openedDisplayTimeMatchesArr);
		$opened_display_pulled = $openedDisplayTimeMatchesArr[1];
		preg_match('/.*title="(.*)">.*/', $closed_display, $closedDisplayTimeMatchesArr);
		$closed_display_pulled = $closedDisplayTimeMatchesArr[1];


		//error_log("Closed Display:".$closed_display);
		global $asPrintString;

		//Company Info
		$asPrintString .= companyInfoString();

		$asPrintString .= "\nCUSTOMER INFO:\n";
		$asPrintString .= orderID2CustomerInfoReceiptString($this->order_id);

		$asPrintString .= "\nORDER INFO:\n";
		$asPrintString .= str_pad("ORDER #:", TABLE_1_COLUMN_1_PAD_LEFT, " ", STR_PAD_LEFT);
		$asPrintString .= " ".$this->order_id."\n";
		$asPrintString .= str_pad("Table:", TABLE_1_COLUMN_1_PAD_LEFT, " ", STR_PAD_LEFT);
		$asPrintString .= " ".$this->tableName."\n";
		$asPrintString .= str_pad("Opened:", TABLE_1_COLUMN_1_PAD_LEFT, " ", STR_PAD_LEFT);
		$asPrintString .= " ".$opened_display_pulled."\n";
		$asPrintString .= str_pad("Closed:", TABLE_1_COLUMN_1_PAD_LEFT, " ", STR_PAD_LEFT);
		$asPrintString .= " ".$closed_display_pulled."\n";
		$asPrintString .= str_pad("Guests:", TABLE_1_COLUMN_1_PAD_LEFT, " ", STR_PAD_LEFT);
		$asPrintString .= " ".$this->guests."\n";
		$asPrintString .= str_pad("Server:", TABLE_1_COLUMN_1_PAD_LEFT, " ", STR_PAD_LEFT);
		$asPrintString .= " ".$this->server."\n";
		$asPrintString .= str_pad("Cashier:", TABLE_1_COLUMN_1_PAD_LEFT, " ", STR_PAD_LEFT);
		$asPrintString .= " ".$this->cashier."\n";

		//End of table
		$asPrintString .= "\n";
		foreach( $this->checks as $check_row => $check ){
			if( count( $this->checks ) > 1 ){
				echo <<<HTML
					<tr>
						<td colspan="100">
						<br />
							<div style="height: 1px; background-color: black; text-align: center">
								<span style="background-color: white; position: relative; top: -0.5em; padding: 5px">
									Check {$check->check}
								</span>
							</div>
							<br />
						</td>
					</tr>
HTML;
			}

			$check->drawOrderDetailInteractive();
		}

			echo <<<HTML
			</tbody>
		</table>
	</div>
HTML;
	}

	public function drawOrderDetailPrintable(){
		
	}

	private function outputSection( $section ){
		if( count( $section ) === 0 ){
			return;
		}

		$it = 0;
		foreach( $section as $k => $v ){
			if( $it++ === 0 ){
				$time_iso = timestampToISO( $v->time() );
				$time = filterRepeatedTimes( $time_iso, $v->time() );
				echo <<<HTML
			<tr>
				<td>{$time}</td>
				<td></td>
				<td>
					<table style="width: 100%">
						<tbody>
HTML;
			}
			$v->drawOrderDetailInteractive();
		}
		echo <<<HTML
						</tbody>
					</table>
				</td>
			</tr>
HTML;
	}

	public function drawOrderTimelineLogs(){
		$allTimeStampedItems = array();

		$checks = $this->checks;
		for( $i = 0; $i < count( $checks ); $i++ ){
			$check = $checks[$i];
			$items = $check->items();
			for( $j = 0; $j < count( $items ); $j++ ){
				$allTimeStampedItems[] = $items[$j];
			}
		}

		for( $i = 0; $i < count($this->send_logs); $i++ ){
			$allTimeStampedItems[] = $this->send_logs[$i];
		}

		for( $i = 0; $i < count($this->action_logs); $i++ ){
			$allTimeStampedItems[] = $this->action_logs[$i];	
		}

		for( $i = 0; $i < count($this->edit_after_send_logs); $i++ ){
			$allTimeStampedItems[] = $this->edit_after_send_logs[$i];
		}

		uasort($allTimeStampedItems, 'timeOrderableItemSort' );
		uasort($allTimeStampedItems, 'timeOrderableItemSort' );

		echo <<<HTML
		<table style="width: 100%">
			<thead>
				<th>Timestamp</th>
				<th>User</th>
				<th>Action</th>
			</thead>
			<tbody>
HTML;
		$time = null;
		$section = array();
		foreach( $allTimeStampedItems as $k => $v ){
			if( $v instanceof OrderItem ){
				if( (count($section) > 0 && $time == $v->time()) || count($section) === 0 ) {
				} else {
					$this->outputSection( $section );
					$section = array();
				}
				$section[] = $v;
				$time = $v->time();
				continue;
			}
			$this->outputSection( $section );
			$v->drawOrderTimelineLogs();
		}
		echo <<<HTML
			</tbody>
		</table>
HTML;
	}

	public function time(){
		return $this->opened;
	}
}


//$order_list defined in get_order_details.php
//$order_list = array($_GET['order_id'],$_GET['order_id']);

//$order_list = array($_GET['order_id'],$_GET['order_id']);
$orderStubs = array();
for($o=0; $o<count($order_list); $o++)
{
	$_GET['order_id'] = $order_list[$o];


	$order_stub = null;
	{
		$arguments = array(
			'order_id' => $_GET['order_id'],
			'loc_id' => $locationid
		);
		$order_stub_query = lavu_query("SELECT * FROM `orders` WHERE `order_id` = '[order_id]' AND `location_id` = '[loc_id]'", $arguments );
		if( $order_stub_query === false || !mysqli_num_rows($order_stub_query)){
			echo "Result:".$order_stub_query;
			echo "location id:".$locationid;
			echo 'num rows:'.mysqli_num_rows($order_stub_query);
			echo 'mysql error:'.lavu_dberror();
			echo 'No Order Found';
			return;
		}
		if( $order_stub = mysqli_fetch_assoc( $order_stub_query ) ){
		} else {
			echo 'error?';
			return;
		}
	}

	$order_tags = array();
	{
		$order_tags_query = lavu_query("SELECT * FROM `order_tags`" );
		if( $order_tags_query === false || !mysqli_num_rows($order_tags_query)){
		} else {
			while( $row = mysqli_fetch_assoc( $order_tags_query ) ){
				$order_tags[] = $row;
			}
		}
	}

	$order_checks = array();
	{
		$arguments = array(
			'order_id' => $_GET['order_id'],
			'loc_id' => $locationid
		);
		$order_checks_query = lavu_query("SELECT * FROM `split_check_details` WHERE `order_id` = '[order_id]' AND `loc_id` = '[loc_id]'", $arguments );
		if( $order_checks_query === false || !mysqli_num_rows($order_checks_query)){
			echo 'No Order Checks Found';
			return;
		}

		while( $row = mysqli_fetch_assoc( $order_checks_query ) ){
			$order_checks[] = $row;
		}
	}

	$order_contents = array();
	{
		$arguments = array(
			'order_id' => $_GET['order_id'],
			'loc_id' => $locationid
		);
		$order_contents_query = lavu_query("SELECT * FROM `order_contents` WHERE `order_id` = '[order_id]' AND `loc_id` = '[loc_id]' AND `item` != 'SENDPOINT'", $arguments );
		if( $order_contents_query === false || !mysqli_num_rows($order_contents_query)){
			echo 'No Order Contents Found';
			return;
		}

		while( $row = mysqli_fetch_assoc( $order_contents_query ) ){
			$order_contents[] = $row;
		}
	}

	$order_payments = array();
	{
		$arguments = array(
			'order_id' => $_GET['order_id'],
			'loc_id' => $locationid
		);
		$order_payments_query = lavu_query("SELECT * FROM `cc_transactions` WHERE `order_id` = '[order_id]' AND `loc_id` = '[loc_id]'", $arguments );
		if( $order_payments_query === false || !mysqli_num_rows($order_payments_query)){

		} else {
			while( $row = mysqli_fetch_assoc( $order_payments_query ) ){
				$order_payments[] = $row;
			}
		}
	}

	$order_send_logs = array();
	{
		$arguments = array(
			'order_id' => $_GET['order_id'],
			'loc_id' => $locationid
		);
		$order_send_logs_query = lavu_query("SELECT `send_log`.*, CONCAT(`users`.`f_name`, ' ', `users`.`l_name`) as `full_name` FROM `send_log` JOIN `users` ON `send_log`.`send_server_id` = `users`.`id` WHERE `order_id`='[order_id]' AND `location_id`='[loc_id]'", $arguments);
		if( $order_send_logs_query !== false && mysqli_num_rows( $order_send_logs_query ) ){
			while( $row = mysqli_fetch_assoc( $order_send_logs_query ) ){
				$order_send_logs[] = $row;
			}
		}
	}

	$action_logs = array();
	{
		$arguments = array(
			'order_id' => $_GET['order_id'],
			'loc_id' => $locationid
		);
		$action_log_query = lavu_query("SELECT `action_log`.*, CONCAT(`users`.`f_name`, ' ', `users`.`l_name`) as `full_name` FROM `action_log` JOIN `users` ON `action_log`.`user_id` = `users`.`id` WHERE `order_id`='[order_id]' AND `action_log`.`loc_id`='[loc_id]'", $arguments);
		if( $action_log_query !== false && mysqli_num_rows( $action_log_query ) ){
			while( $row = mysqli_fetch_assoc( $action_log_query ) ){
				$action_logs[] = $row;
			}
		}
	}

	$edit_after_send_logs = array();
	{
		$arguments = array(
			'order_id' => $_GET['order_id'],
			'loc_id' => $locationid
		);
		$edit_after_send_log_query = lavu_query("SELECT `edit_after_send_log`.*, CONCAT(`users`.`f_name`, ' ', `users`.`l_name`) as `full_name` FROM `edit_after_send_log` JOIN `users` ON `edit_after_send_log`.`server_id` = `users`.`id` WHERE `order_id`='[order_id]' AND `location_id`='[loc_id]'", $arguments);
		if( $edit_after_send_log_query !== false && mysqli_num_rows( $edit_after_send_log_query ) ){
			while( $row = mysqli_fetch_assoc( $edit_after_send_log_query ) ){
				$edit_after_send_logs[] = $row;
			}
		}
	}

	$orderStubs[$o] = OrderStub::createOrderSubFromSources( $order_stub, $order_tags, $order_checks, $order_contents, $order_payments, $order_send_logs, $action_logs, $edit_after_send_logs );

}



function companyInfoString(){
	$result = lavu_query("SELECT * FROM `locations` LIMIT 1");
	$location_row = mysqli_fetch_assoc($result);
	$str = $location_row['title'] . "\n" .
	       $location_row['address'] . "\n" .
	       $location_row['city'].", ".$location_row['state']." ".$location_row['zip']."\n".
	       $location_row['country']."\n";
	if(!empty($location_row['phone'])) { $str .= $location_row['phone'] . "\n"; }
	if(!empty($location_row['website'])) { $str .= $location_row['website'] . "\n"; }
	return $str;
}
function orderID2CustomerInfoReceiptString($orderID){
	$result = lavu_query("SELECT * FROM `orders` WHERE `order_id` = '[1]' LIMIT 1", $orderID);
	if(!$result){ error_log('MySQL error in '.__FILE__.' -8oes- error:'.lavu_dberror() ); return '';}
	if(!mysqli_num_rows($result)){ return ''; }
	$orderRow = mysqli_fetch_assoc($result);
	$original_id_ContainsCustInfo = $orderRow['original_id'];

	$parts = explode('|o|', $original_id_ContainsCustInfo);
	$replaceStr = $parts[0].'|o|'.$parts[1].'|1|'.$parts[2].'|2|'.$parts[3].'|o|';
	$customerInfoJSON = substr($original_id_ContainsCustInfo, strlen($replaceStr));
	$customerJSONInfo = json_decode($customerInfoJSON,1);
	$customerPrintInfo = $customerJSONInfo['print_info'];

	$returnPrintString = '';
	foreach($customerPrintInfo as $key => $val){
		$returnPrintString .= $val['field'].':'.$val['info']."\n";
	}

	return $returnPrintString;
}
function getCreditCardSignatureLineIfNecessary(){
	$orderID = $_GET['order_id'];
	$result = lavu_query("SELECT * FROM `cc_transactions` WHERE `order_id`='[1]'", $orderID);
	$cardsUsed = array(); 
	if(!$result){ error_log("MySQL Error in ".__FILE__." -scgnt4efnx- error:".lavu_dberror()); }
	while($curr_cc_trans_row = mysqli_fetch_assoc($result)){
		if(!empty($curr_cc_trans_row['card_desc']) && strtolower($curr_cc_trans_row['pay_type']) == 'card'){
			$cardsUsed[] = $curr_cc_trans_row;
		}
	}

	$returnString = '';
	if(!empty($cardsUsed)){
		foreach($cardsUsed as $cc_row){
			$returnString .= "\n\nCard: xxxx-xxxx-xxxx-".$cc_row['card_desc']."\n\n\n";
			$returnString .= "Sign Here:\n\n\n\nX_____________________________\n\n\n\n\n\n";
		}
	}
	return $returnString;
}





echo <<<HTML
<div>
	<style type="text/css" scoped>
		tab-area {
			display: block;
		}
		[hide] {
			display: none;
		}
		nav.order_detail_navigation {
			display: table;
			margin: 0 auto;
		}
		nav.order_detail_navigation > div {
			display: inline-block;
			padding: 2px 10px;
			border: 1px solid rgba(175,175,175,1.0);
			color: rgba(175,175,175,1.0);
			border-left: 0;
			cursor: pointer;
		}

		nav.order_detail_navigation > div:nth-child(1) {
			border-left: 1px solid rgba(175,175,175,1.0) !important;
		}

		nav.order_detail_navigation > div:hover, nav.order_detail_navigation > div[selected] {
			color: white;
			background: rgba(175,175,175,1.0);
		}
		table tr td {
			vertical-align: text-top;
		}
	</style>
	<nav class="order_detail_navigation">
		<div onclick="show_report_tab('stub');" selected stub>Stub</div>
		<div onclick="show_report_tab('details');" details>Details</div>
	</nav>
	<tab-area>
		<div stub>
HTML;
//This is what needs to be called.
$o = 0;
foreach($orderStubs as $key => $order)
{


	$order->drawOrderDetailInteractive();
	$asPrintString .= getCreditCardSignatureLineIfNecessary();

	if($o++ < (count($orderStubs) - 1)){
		$asPrintString .= "|-|-|-|-|-|-|-|-|-|-|";//Used for the split
	}
}
echo <<<HTML
		</div>
		<div details hide>
HTML;
//$order->drawOrderTimelineLogs();//Renders elsewhere duplicating str-builder data.
echo <<<HTML
		</div>
	</tab-area>
</div>
HTML;

echo <<<HTML
	<script type="text/javascript">
		var timeElements = document.querySelectorAll('time');
		for( var i = 0; i < timeElements.length; i++ ){
			var d = new Date(timeElements[i].getAttribute('datetime'));
			timeElements[i].appendChild( document.createTextNode( d.toLocaleString() ));
		}
	</script>
HTML;
return;
