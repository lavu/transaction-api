<script type='text/javascript'>
	function parse_ajax_response(str)
	{
		var rsp = new Array();
		rsp['success'] = "0";
		var str_parts = str.split("&");
		for(var i=0; i<str_parts.length; i++)
		{
			var var_parts = str_parts[i].split("=");
			if(var_parts.length > 1)
				rsp[var_parts[0]] = var_parts[1];
		}
		return rsp;
	}

	function make_ajax_call(strURL,querystr,callback,calltype,sendtype) 
	{

		var xmlHttpReq = false;
		var self = this;

		if(!calltype) calltype = "text";
		if(!sendtype) sendtype = "post";
		callback_str = callback + "(self.xmlHttpReq.responseText)";
		set_content_type = "application/x-www-form-urlencoded";

		if (window.XMLHttpRequest) 
		{
			self.xmlHttpReq = new XMLHttpRequest();
		}
		else if (window.ActiveXObject) 
		{
			self.xmlHttpReq = new ActiveXObject("Microsoft.XMLHTTP");
		}
		self.xmlHttpReq.open('POST', strURL, true);

		if(calltype=="vars")
		{
			callback_str = callback + "(parse_ajax_response(self.xmlHttpReq.responseText))";
		}
		if(calltype=="json")
		{
			callback_str = callback + "(JSON.parse(self.xmlHttpReq.responseText)))";
		}
		if(sendtype=="upload")
		{
			self.xmlHttpReq.setRequestHeader('Mime-Type', "multipart/form-data");
			set_content_type = "false";
		}

		self.xmlHttpReq.setRequestHeader('Content-Type', set_content_type);

		self.xmlHttpReq.onreadystatechange = function()
		{
			if (self.xmlHttpReq.readyState == 4)
			{
				eval(callback_str);
			}
		}
		self.xmlHttpReq.send(querystr);

	}
</script>