<?php

//error_log('Inside get_order_details.php');
	header('Content-type: text/html; charset=utf-8'); 
	//Setup DB.
	require_once('/home/poslavu/public_html/admin/cp/resources/core_functions.php');
	require_once('/home/poslavu/public_html/admin/cp/resources/lavuquery.php');


	$sec_sha_expected = sha1($_POST['dataname'].'pumpkin');
	$sec_sha_sent = $_POST['sec'];
	if($sec_sha_expected != $sec_sha_sent){
		echo 'sec failure';
		exit;
	}

	lavu_connect_dn( $_POST['dataname'],'poslavu_'.$_POST['dataname'].'_db');

	$in_lavu = true;
	$locationid = 1;
	$location_info=array();
	$locationResult = lavu_query("SELECT * FROM `locations` LIMIT 1");
	if($locationResult){
		$location_info = mysqli_fetch_assoc($locationResult);
	}
	ini_set('display_errors',1);
	$order_list = explode(",", $_REQUEST['order_ids']);
	ob_start();
	require_once(dirname(__FILE__)."/order_details_v2_clone.php");
	ob_clean();
	//echo "Print string:<pre>".$asPrintString."</pre>";
	$allReceiptsArr=explode("|-|-|-|-|-|-|-|-|-|-|", $asPrintString);
	$fullJSONReturnObjectArr = array('receiptStrings' => $allReceiptsArr);
	$fullJSONReturnStr = json_encode($fullJSONReturnObjectArr);
	//echo "<pre>".$fullJSONReturnStr."</pre>";

//error_log('End of get_order_details.php built receipt response:'.$fullJSONReturnStr);

	echo $fullJSONReturnStr;
?>