<?php

/*
 * File to host a few common methods to avoid duplicating simple
 * functionality throughout the various Controllers for the inventory system
 */

define('_RESOURCES_', dirname(dirname(dirname(dirname(__FILE__)))).'/cp/resources');
require_once (_RESOURCES_ .'/pdo_lavuquery.php');

class queryHelper
{
    //returns the encoded JSON of the associated array returned by Fetch
    public function executeQuery($sql){
        global $dataname;
        PDO_CLavuConnectDN($dataname);
        $results = PDO_CLavuQuery($sql);
        if($results === FALSE){
            return $results;
        }
        return $results->fetchAll('ASSOC');
    }

    //Returns the prepared statement that can then be used to
    //execute prepared queries
    public function prepareQuery($sql){
        global $dataname;
        PDO_CLavuConnectDN($dataname);
        $preppedQuery = PDO_CLavuPrepare($sql);
        return $preppedQuery;
    }
}