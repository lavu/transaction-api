<?php

/***
 * Class for representing the HTTP Request submitted to the Inventory API
 ***/

class APIRequest{
    public $verb; // GET POST PUT
    public $actionType; //of the format <inventorypath>/System/Method
    public $JSON; //JSON object to be passed on to the controller method
    public $body;

    public function __construct(){
        $this->verb = $_SERVER['REQUEST_METHOD'];
        // Extract the Controller/Object/Method from PATH_INFO
        if(!empty($_SERVER['PATH_INFO'])){
            $this->actionType = explode('/', ltrim($_SERVER['PATH_INFO'], '/'));
        }
        if($this->verb && ($this->verb === 'POST' || $this->verb === 'PUT')){
            $body = file_get_contents("php://input");
            $content_type = false;
            if(isset($_SERVER['CONTENT_TYPE'])) {
                $content_type = $_SERVER['CONTENT_TYPE'];
            }
            if($content_type === "application/json"){
                $this->JSON = $body;
            }           
        }
        else{ //pull the GET query string
            if(isset($_SERVER['QUERY_STRING'])){
                parse_str($_SERVER['QUERY_STRING'], $JSON);
            }
        }
    }
}