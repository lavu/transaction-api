/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


function APICallHelper(object, method, controller, JSONinput, callback){
    this.requestURL = "http://demo.lavu.com/components/API/Orders/Orders.php/";
    this.request = new XMLHttpRequest();
    this.object = object;
    this.method = method;
    this.controller = controller;
    this.callback = callback;
    this.JSONInput = JSONinput;
    this.sendRequest = function(){
        var completeURL = this.requestURL + this.controller + "/" + this.object + "/" + this.method;

        this.request.open("POST", completeURL, false);
        this.request.setRequestHeader('Content-Type', 'application/json');
        
        this.request.onreadystatechange = function(){
        if(this.readyState === 4 && this.status === 200){
            var response = JSON.parse(this.responseText);
            callback(this.response);
        }
        else callback(false);
        };
    
        
        
        if(this.JSONinput !== ''){
            this.request.send(this.JSONinput);
        }
        else this.request.send();
    };
}
