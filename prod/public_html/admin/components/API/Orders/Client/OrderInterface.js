/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var orders = [];
var order_contents = [];

function order(id, order_id){
    this.id = id;
    this.order_id = order_id;
}

function order_contents(id, order_id, item_id){
    this.id = id;
    this.order_id = order_id;
    this.item_id = item_id;
}

function OpenOrder(JSONItem){
    var object = 'order';
    var method = 'open';
    var callback = function(responseText){
        if(responseText !== false){
            var responseArray = responseText;
            for (var i = 0; i < responseArray.length; i++){
                curOrder = responseArray[i];
                var newOrder = new order(curOrder['id'], curOrder['order_id']);
                orders[newOrder.id] = newOrder;
            }
        }
    };
     var APICall = new APICallHelper(object, method, "OrderController", JSONItem, callback);
    APICall.sendRequest();
}

function CloseOrder(JSONItem){
    var object = 'order';
    var method = 'close';
    var callback = function(responseText){
        if(responseText !== false){
            var responseArray = responseText;
            for (var i = 0; i < responseArray.length; i++){
                curOrder = responseArray[i];
                var newOrder = new order(curOrder['id'], curOrder['order_id']);
                if(orders[newOrder.id] !== null){
                    orders.splice(newOrder.id, 1);
                }
            }
        }
    };
    var APICall = new APICallHelper(object, method, "OrderController", JSONItem, callback);
    APICall.sendRequest();
}

function SaveOrder(JSONItem){
    var object = 'order';
    var method = 'save';
    var callback = function(responseText){
        if(responseText !== false){
            var responseArray = responseText;
            for (var i = 0; i < responseArray.length; i++){
                curOrder = responseArray[i];
                var newOrder = new order(curOrder['id'], curOrder['order_id']);
                if(orders[newOrder.id] === null){
                    orders[newOrder.id] = newOrder;
                }
            }
        }
    };
    
    var APICall = new APICallHelper(object, method, "OrderController", JSONItem, callback);
    APICall.sendRequest();
}

function AddContentToOrder(JSONItem){
    var object = 'order';
    var method = 'updateContents';
    var callback = function(responseText){
        if(responseText !== false){
            var responseArray = responseText;
            for (var i = 0; i < responseArray.length; i++){
                curOrder = responseArray[i];
                var newOrder = new order_contents(curOrder['id'], curOrder['order_id'], curOrder["item_id"]);
                if(order_contents[newOrder.id] === null){
                    order_contents[newOrder.id] = newOrder;
                }
            }
        }
    };
    
    var APICall = new APICallHelper(object, method, "OrderController", JSONItem, callback);
    APICall.sendRequest();
}

function cancelOrder(JSONItem){
    var object = 'order';
    var method = 'cancel';
    var callback = function(responseText){
        if(responseText !== false){
            var responseArray = responseText;
            for (var i = 0; i < responseArray.length; i++){
                curOrder = responseArray[i];
                var newOrder = new order(curOrder['id'], curOrder['order_id']);
                if(orders[newOrder.id] !== null){
                    orders.splice(newOrder.id, 1);
                }
            }
        }
    };
    var APICall = new APICallHelper(object, method, "OrderController", JSONItem, callback);
    APICall.sendRequest();
}

function getOpenOrders(){
    var object = 'order';
    var method = 'getOpen';
    var callback = function(responseText){
        if(responseText !== false){
            var responseArray = responseText;
            for (var i = 0; i < responseArray.length; i++){
                curOrder = responseArray[i];
                var newOrder = new order(curOrder['id'], curOrder['order_id']);
                if(orders[newOrder.id] !== null){
                    orders[newOrder.id] = newOrder;
                }
            }
        }
    };
    var APICall = new APICallHelper(object, method, "OrderController", '', callback);
    APICall.sendRequest();
}