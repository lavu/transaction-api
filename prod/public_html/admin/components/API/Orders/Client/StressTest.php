<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <script type="text/javascript" src="OrderAPIHelper.js"></script>
        <script type="text/javascript" src="OrderInterface.js"></script>
    </head>
    <body>
        <?php
        $ResultArray = array();
        $numOrders = $_POST["numOrders"];
        $items = $_POST["items"];
        $ioid = $_POST["ioid"];
        $activeDevice = $_POST["activeDevice"];
        $order_number = 1;
        $results = array();
        date_default_timezone_set("America/Denver");
        $date = date('Y-m-d H:i:s', time());
        for ($i = 1; $i <= $numOrders; $i++){
            $fields = array(
              'order_id' => '1_'.$order_number,
               'opened' => $date,
               'ioid' => '66',
               'serial_no' => '0',
               'active_device' => $activeDevice              
            );
            $fieldsArray = array($fields);
            $fieldString = json_encode($fieldsArray);
            
            $result = apiCall("order", "open", $fieldString);
           
            $assocResponse = json_decode($result, true);
            $ResultArray[$assocResponse[0]['id']] = $assocResponse[0];
            $order_number = $order_number + 1;
        }
        
        foreach ($ResultArray as $order){
           // echo print_r($order);
            //echo "<br>";
            $numItems = rand(1, $items);
            for($i = 0; $i < $numItems; $i++){                
                $date = date('Y-m-d H:i:s', time());
                $fields = array(
                    'order_id' => $order['order_id'],
                    'item_id' => '476',
                    'quantity' => '1',
                    'icid' => '456',
                    'device_time' => $date,
                    'ioid' => '66'
                );
                
                $fieldsArray = array($fields);
                $fieldString = json_encode($fieldsArray);
            
                $contentResult = apiCall("order", "updateContents", $fieldString);
                $contentArray = json_decode($contentResult, true);
                if($contentArray["Status"] === "Success"){
                   $saveFields = array('id' => $order['id'],
                                        'date' => $date);
                   $saveString = json_encode($saveFields);
                   $saveResults = apiCall("order", "save", $saveString);
                 
                }
            }
        }
        
        foreach ($ResultArray as $order){  
           // echo (print_r($order));
           // echo '<br>';
            $date = date('Y-m-d H:i:s', time());
            $closeFields = array(
                'id' => $order['id'],
                'closed' => $date
            );
            $closeArray = array($closeFields);
            $closeString = json_encode($closeArray);
            $closeResult = apiCall("order", "close", $closeString);
            
        }
        echo "Test Executed successfully.";
        
        
        function apiCall($object, $method, $JSON){
            $ch = curl_init();
            $curlURL = "http://demo.lavu.com/components/API/Orders/Orders.php/Order/".$object ."/".$method;
            curl_setopt($ch, CURLOPT_URL, $curlURL);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST"); 
            if($JSON !== ''){
                curl_setopt($ch, CURLOPT_POSTFIELDS, $JSON);
            }
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));        
            
            $response = curl_exec($ch);
            return $response;
        }
        ?>
    </body>
</html>
