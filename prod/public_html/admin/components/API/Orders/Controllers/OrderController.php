<?php
/*
 * Controller for interacting with Orders and related tables
 */
if(!defined('_HELPERS_'))
{
    define('_HELPERS_', dirname(dirname(dirname(__FILE__))));
}
require_once (_HELPERS_.'/Helpers/queryHelper.php');

class OrderController{
	
	public function __construct(){

        }

	public function openOrder($JSON){
            if($JSON === ''){
                return array ('ERROR' => 'No Input for Order Creation');
            }
            
            $querystring = "INSERT INTO orders (order_id, opened, ioid, serial_no, active_device, last_modified) VALUES (?,?,?,?,?,?)";
            $decode = json_decode($JSON, true);
            $preppedStatement = prepareQuery($querystring);
            $addedIDs = array();
            if($preppedStatement === FALSE){
                return array('ERROR' => 'Add vendors failed. 1');
            }
            date_default_timezone_set("America/Denver");
            $date = date('Y-m-d H:i:s', time());
            foreach ($decode as $order) {
                $test = $preppedStatement->execute(array($order["order_id"], $order["opened"], $order["ioid"], $order["serial_no"], $order["active_device"],  $date));
                if($test == FALSE){continue;}
                $newID = PDO_CLavuInsertID();
                $addedIDs[] = $newID;
            }
            $idList = join(",", $addedIDs);
            $sql = "SELECT id, order_id FROM orders WHERE id IN ({$idList})";
            $selectResult = executeQuery($sql);
            if($selectResult === FALSE){
                return array('ERROR' => 'Open Order failed to return new order ID');
            }
            return $selectResult;
	}
	
	public function closeOrder($JSON){
            if($JSON === ''){
                 return array ('ERROR' => 'No Input for Order Closing');
            }
           
            $querystring = "UPDATE orders SET closed = ? where id = ?";
            
            $decode = json_decode($JSON, true);
            $preppedStatement = prepareQuery($querystring);
            $addedIDs = array();
            if($preppedStatement === FALSE){
                return array('ERROR' => 'Add vendors failed. 1');
            }
            
            foreach ($decode as $order) {
                $test = $preppedStatement->execute(array($order['closed'], $order['id']));
                if($test == FALSE){continue;}
                $addedIDs[] = $order["id"];
            }
            $idList = join(",", $addedIDs);
            $sql = "SELECT id, order_id FROM orders WHERE id IN ({$idList})";
            $selectResult = executeQuery($sql);
            if($selectResult === FALSE){
                return array('ERROR' => 'Close Order failed to return altered orders');
            }
            return $selectResult;               
	}
	
	public function saveOrder($JSON){
            if($JSON === ''){
                return array ('ERROR' => 'No Input for Order Saving');
            }
            
            $querystring = "UPDATE orders SET closed = ?, last_modified = ? WHERE id = ?";
                
            $decode = json_decode($JSON, true);
            $preppedStatement = prepareQuery($querystring);
            $addedIDs = array();
            if($preppedStatement === FALSE){
                return array('ERROR' => 'Add vendors failed. 1');
            }
            
            foreach ($decode as $order) {
                $test = $preppedStatement->execute(array($order["date"], $order["date"], $order["id"]));
                if($test == FALSE){continue;}
                $addedIDs[] = $order["id"];
            }
            
            $idList = join(",", $addedIDs);
            $sql = "SELECT id, order_id FROM orders WHERE id IN ({$idList})";
            $selectResult = executeQuery($sql);
            if($selectResult === FALSE){
                return array('ERROR' => 'Save Order failed to return altered orders');
            }
            return $selectResult;
	}
	
        public function updateContents($JSON){
            if($JSON === ''){
                return array ('ERROR' => 'No Input for Order Saving');
            }
            
            $querystring = "INSERT INTO order_contents (order_id, item_id, quantity, icid, device_time, ioid) VALUES (?,?,?,?,?,?)" ;
                
            $decode = json_decode($JSON, true);
            $preppedStatement = prepareQuery($querystring);
            $addedIDs = array();
            if($preppedStatement === FALSE){
                return array('Status' => 'Update Contents failed. 1');
            }
            foreach ($decode as $order) {
                $valueArray = array($order["order_id"], $order["item_id"], $order["quantity"], $order["icid"], $order["device_time"], $order["ioid"]);
              //  return print_r($valueArray);
                $test = $preppedStatement->execute($valueArray);
                if($test == FALSE){continue;}
                $newID = PDO_CLavuInsertID();
                $addedIDs[] = $newID;
            }
            /*
            if($addedIDs !== null){
                $idList = join(",", $addedIDs);
                $sql = "SELECT id, order_id, item_id FROM order_contents WHERE id IN ({$idList})";
                $selectResult = executeQuery($sql);
                if($selectResult === FALSE){
                    return array('ERROR' => 'Save Order failed to return altered orders');
                }
                return $selectResult;
            }
            */
            return array('Status' => 'Success');
        }
        
        public function cancelOrder($JSON){
            if($JSON === ''){
                return array ('ERROR' => 'No Input for Order Cancel');
            }
            
            $querystring = "UPDATE orders SET void = 1, void_reason = ? WHERE id = ?";
                
            $decode = json_decode($JSON, true);
            $preppedStatement = prepareQuery($querystring);
            $addedIDs = array();
            if($preppedStatement === FALSE){
                return array('ERROR' => 'Cancel order failed. 1');
            }
            
            foreach ($decode as $order) {
                $test = $preppedStatement->execute(array($order["void_reason"],$order["id"]));
                if($test == FALSE){continue;}
                $addedIDs[] = $order["id"];
            }
            
            $idList = join(",", $addedIDs);
            $sql = "SELECT id, order_id FROM orders WHERE id IN ({$idList})";
            $selectResult = executeQuery($sql);
            if($selectResult === FALSE){
                return array('ERROR' => 'Cancel Order failed to return altered orders');
            }
            return $selectResult;
	}
        
        public function getOpenOrders(){
            $querystring = "select id, order_id FROM orders WHERE closed = ''";
            $selectResult = executeQuery($querystring);
            if($selectResult === FALSE){
                return array ('ERROR' => 'Get Open Orders failed');
            }
            
            return $selectResult;
        }
        
        public function getOrderContents($JSON){
                        if($JSON === ''){
                return array ('ERROR' => 'No Input for Order Cancel');
            }
            
            $querystring = "select id, item_id, order_id from order_contents where id = ?";
                
            $decode = json_decode($JSON, true);
            $preppedStatement = prepareQuery($querystring);
            $addedIDs = array();
            if($preppedStatement === FALSE){
                return array('ERROR' => 'Cancel order failed. 1');
            }
            
            foreach ($decode as $order) {
                $test = $preppedStatement->execute(array($order["id"]));
                if($test == FALSE){continue;}
                return $test;
            }
        }
        
	public function routeCommand($object = '', $method = '', $JSON = ''){
        if($object === '' || $method === ''){
            echo "No valid Command";
            return FALSE;
        }
		if($object == "order"){
			switch($method){
				case 'open':
					return $this->openOrder($JSON);
				case 'save':
					return $this->saveOrder($JSON);
				case 'close':
					return $this->closeOrder($JSON);
                                case 'updateContents':
                                        return $this->updateContents($JSON);
                                case 'cancel':
                                        return $this->cancelOrder($JSON);
                                case 'getOpen':
                                        return $this->getOpenOrders();
                                case 'getContents':
                                        return $this->getOrderContents($JSON);
				default:
					break;	
			}
		}
	 }
}
