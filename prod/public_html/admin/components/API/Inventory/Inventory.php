<?php

/*
 * Handles incoming HTTP Requests to the inventory system and routes them
 */

require_once(dirname(dirname(__FILE__)).'/Helpers/APIRequest.php');
require_once(__DIR__.'/Controllers/InventoryManagementController.php');
require_once(__DIR__.'/Controllers/86CountController.php');
require_once(__DIR__.'/Controllers/VendorController.php');
require_once(__DIR__.'/Controllers/UnitSystemController.php');
require_once(__DIR__.'/Controllers/PurchaseOrderController.php');
require_once(__DIR__.'/Controllers/MenuController.php');

$request = new APIRequest();
$controller;
$controllerInstance;
$object;
$method;
$protocol = (isset($_SERVER['SERVER_PROTOCOL']) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.0');
$responseArray = array();

if(!$request->actionType){
    //if there is nothing submitted to the controller, just return OK
    header("application/JSON");
    $responseArray['response'] = "No Command Received";
    $response = json_encode($responseArray);
    echo $response;
    exit();
}
else if(count($request->actionType) != 3){
    //if the request exists, but is formatted incorrectly, for now return 400
    //bad request
     header("application/JSON");
    $responseArray['response'] = "400 - Bad Request";
    $response = json_encode($responseArray);
    echo $response;
    exit();
}
else{
    $controller = $request->actionType[0];
    $object = $request->actionType[1];
    $method = $request->actionType[2];
}

switch($controller){
    case 'InventoryManagement':
        $controllerInstance = new InventoryManagementController();
        break;
    case 'UnitSystem':
        $controllerInstance = new UnitSystemController();
        break;
    case 'Vendor':
        $controllerInstance = new VendorController();
        break;
    case 'PurchaseOrder':
        $controllerInstance = new PurchaseOrderController();
        break;
    case '86CountController':
        $controllerInstance = new _86CountController();
        break;
    case 'MenuController':
        $controllerInstance = new MenuController();
        break;
    default:
        header("application/JSON");
        $responseArray['response'] = "Problem Loading ". $controller;
        $response = json_encode($responseArray);
        echo $response;
        exit();
}

if($controllerInstance){
    $resultArray = $controllerInstance->routeCommand($object, $method, $request->JSON);
    header("application/JSON");
    if($resultArray === false){
        echo JSON_encode(array('ERROR'=>'Unknown Error'), JSON_PRETTY_PRINT);
    }else if(is_string($resultArray)){
        echo $resultArray;
    }else{
        echo JSON_encode($resultArray, JSON_PRETTY_PRINT);
    }
}

