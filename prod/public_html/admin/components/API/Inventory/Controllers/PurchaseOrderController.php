<?php

/*
 * This class hosts all of the database related methods required for:
 * *Creating a purchase order
 * *Adding an item to a purchase order
 * *Getting items related to a purchase order
 * *Updating items on a purchase order
 * *Recieving a purchase order
 */

if (!defined('_HELPERS_'))
{
    define('_HELPERS_', dirname(dirname(dirname(__FILE__))));
}
require_once(_HELPERS_ . '/Helpers/queryHelper.php');

class PurchaseOrderController
{

    private $_qHelper;

    public function __construct($queryHelper = null)
    {
        if(!$queryHelper) $queryHelper = new queryHelper();

        $this->_qHelper = $queryHelper;
    }

    public function routeCommand($object = '', $method = '', $JSON = '')
    {
        if ($object === '' || $method === '')
        {
            return array('ERROR' => 'No valid command');
        }

        if ($object === 'purchaseorder')
        {
            switch ($method)
            {
                case 'add':
                    return $this->addPurchaseOrder($JSON);
                case 'get':
                    return $this->getPurchaseOrders();
                case 'receive':
                    return $this->receivePurchaseOrders($JSON);
            }
        }
        else
        {
            if ($object === 'item')
            {
                switch ($method)
                {
                    case 'add':
                        return $this->addPurchaseOrderItems($JSON);
                    case 'get':
                        return $this->getPurchaseOrderItems($JSON);
                    case 'update':
                        return $this->updatePurchaseOrderItems($JSON);
                }
            }
        }

        return array('ERROR' => 'No valid command 2');
    }

    private function addPurchaseOrder($JSON)
    {
        global $loc_id;
        $decode = json_decode($JSON, true);
        $querystring = "INSERT INTO inventory_purchaseorder SET vendorID = ?, order_date = CURRENT_TIMESTAMP, loc_id = '{$loc_id}'";
        $preppedStatement = $this->_qHelper->prepareQuery($querystring);
        $addedIDs = array();
        foreach ($decode as $purchaseOrder)
        {
            $preppedStatement->execute(array($purchaseOrder['vendorID']));
            $newID = PDO_CLavuInsertID();
            $addedIDs[] = $newID;
        }
        $idList = join(",", $addedIDs);
        $sql = "SELECT id, vendorID, order_date FROM inventory_purchaseorder WHERE id IN ({$idList})";
        $selectResult = $this->_qHelper->executeQuery($sql);
        if ($selectResult == false)
        {
            return array('ERROR' => 'Add purchase orders failed. 2');
        }
        return $selectResult;
    }

    private function getPurchaseOrders()
    {
        global $loc_id;
        $querystring = "select id, vendorID, order_date, receive_date from inventory_purchaseorder where _deleted = '0' and loc_id = '" . $loc_id . "'";
        $results = $this->_qHelper->executeQuery($querystring);
        return $results;
    }

    private function receivePurchaseOrders($JSON)
    {
        $decode = json_decode($JSON, true);
        $preparedQueryString = "UPDATE inventory_purchaseorder SET receive_date = ?, received = ? WHERE id = ?";
        $preppedStatement = $this->_qHelper->prepareQuery($preparedQueryString);
        $toReturn = array();
        foreach ($decode as $purchaseOrder)
        {
            $temp = $preppedStatement->execute(array(date("Y-m-d H:i:s"), 1, $purchaseOrder['id']));
            if ($temp)
            {
                $toReturn[] = $purchaseOrder;
            }
        }
        if (empty($toReturn))
        {
            return array('ERROR' => 'Receive purchase orders failed. 2');
        }
        return $toReturn;
    }

    private function addPurchaseOrderItems($JSON)
    {
        global $loc_id;
        $decode = json_decode($JSON, true);
        $querystring = "INSERT INTO purchaseorder_items SET purchaseOrderID = ?, vendorItemID = ?, unitID = ?, quantityOrdered = ?, SKU = ?";
        $preppedStatement = $this->_qHelper->prepareQuery($querystring);
        $addedIDs = array();
        try
        {
            foreach ($decode as $purchaseOrderItem)
            {
                $purchaseOrderID = $purchaseOrderItem['purchaseOrderID'];
                $vendorItemID = $purchaseOrderItem['vendorItemID'];
                $unitID = $purchaseOrderItem['unitID'];
                $quantityOrdered = $purchaseOrderItem['quantityOrdered'];
                $SKU = $purchaseOrderItem['SKU'];
                $test = $preppedStatement->execute(array($purchaseOrderID, $vendorItemID, $unitID, $quantityOrdered, $SKU));
                if (!$test)
                {
                    continue;
                }
                $newID = PDO_CLavuInsertID();
                $addedIDs[] = $newID;
            }
        }
        catch(\Exception $e)
        {
            return array("ERROR" => "A required property was not set");
        }

        $idList = join(",", $addedIDs);
        $sql = "SELECT id, purchaseOrderID, vendorItemID, unitID, quantityOrdered, quantityReceived, SKU FROM purchaseorder_items WHERE id IN ({$idList})";
        $selectResult = $this->_qHelper->executeQuery($sql);
        if ($selectResult == false)
        {
            return array('ERROR' => 'Add purchase order items failed. 2');
        }
        return $selectResult;
    }

    private function getPurchaseOrderItems($JSON)
    {
        global $loc_id;
        $decode = json_decode($JSON, true);
        $id = $decode['id'];
        $querystring = "SELECT id, purchaseOrderID, vendorItemID, unitID, quantityOrdered, quantityReceived, SKU FROM purchaseorder_items WHERE _deleted = '0'";
        if ($id !== null)
        {
            $querystring .= " AND purchaseOrderID = ?";
            $preppedStatement = $this->_qHelper->prepareQuery($querystring);
            $preppedStatement->execute(array($id));
            $results = $preppedStatement->fetchAll();
        }
        else
        {
            $results = $this->_qHelper->executeQuery($querystring);
            if ($results == FALSE)
            {
                return array('ERROR' => 'Get purchase orders failed. 3');
            }
        }
        return $results;
    }

    private function updatePurchaseOrderItems($JSON)
    {
        $decode = json_decode($JSON, true);
        $preparedQueryString = "UPDATE purchaseorder_items SET purchaseOrderID=?, vendorItemID=?, unitID=?, quantityOrdered=?, quantityReceived=?, SKU=?, cost=? WHERE id=?";
        $preppedStatement = $this->_qHelper->prepareQuery($preparedQueryString);
        $addedIDs = array();
        try
        {
            foreach ($decode as $purchaseOrderItem)
            {
                $purchaseOrderID = $purchaseOrderItem['purchaseOrderID'];
                $vendorItemID = $purchaseOrderItem['vendorItemID'];
                $unitID = $purchaseOrderItem['unitID'];
                $quantityOrdered = $purchaseOrderItem['quantityOrdered'];
                $quantityReceived = $purchaseOrderItem['quantityReceived'];
                $SKU = $purchaseOrderItem['SKU'];
                $id = $purchaseOrderItem['id'];
                $cost = $purchaseOrderItem['cost'];
                $result = $preppedStatement->execute(array($purchaseOrderID, $vendorItemID, $unitID, $quantityOrdered, $quantityReceived, $SKU, $cost, $id));
                if($result == false)
                {
                    return array("ERROR" => "Add purchase order items failed 2");
                }

                $addedIDs[] = $id;
            }
        }
        catch(\Exception $e)
        {
            return array("ERROR" => "A required property was not set");
        }
        $idList = join(",", $addedIDs);
        $sql = "SELECT id, purchaseOrderID, vendorItemID, unitID, quantityOrdered, quantityReceived, SKU, cost FROM purchaseorder_items WHERE id IN ({$idList})";
        $selectResult = $this->_qHelper->executeQuery($sql);
        return $selectResult;
    }

}