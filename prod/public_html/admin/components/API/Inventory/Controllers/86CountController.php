<?php

/*
 * Controller for the 86 count functionality
 * Can add/get/update 86 counts for modifiers and menu items
 */

if (!defined('_HELPERS_'))
{
    define('_HELPERS_', dirname(dirname(dirname(__FILE__))));
}
require_once(_HELPERS_ . '/Helpers/queryHelper.php');

class _86CountController
{

    public $dataname = "ll-lavudemochr";
    private $_qHelper;

    public function __construct($queryHelper = null)
    {
        if(!$queryHelper) $queryHelper = new queryHelper();

        $this->_qHelper = $queryHelper;
    }

    public function routeCommand($object = '', $method = '', $JSON = '')
    {
        if ($object === '' || $method === '')
        {
            return array("ERROR" => "No valid Command");
        }

        if ($object == 'menuitem')
        {
            switch ($method)
            {
                case 'get':
                    return $this->get86menuItem();
                case 'outofstock':
                    return $this->outOfStockItems();
                case 'lowstock':
                    return $this->lowStockMenuItems();
                case 'add':
                    return $this->add86menuItem($JSON);
                case 'update':
                    return $this->update86menuItem($JSON);
                case 'reserve':
                    return $this->reserveMenuItem($JSON);
                case 'close':
                    return $this->closeMenuItem($JSON);
                case 'cancel':
                    return $this->cancelMenuItem($JSON);
            }
        }

        if ($object == 'modifier')
        {
            switch ($method)
            {
                case 'get':
                    return $this->get86modifier();
                case 'outofstock':
                    return $this->outOfStockModifiers();
                case 'lowstock':
                    return $this->lowStockModifiers();
                case 'add':
                    return $this->add86modifier($JSON);
                case 'update':
                    return $this->update86modifier($JSON);
                case 'reserve':
                    return $this->reserveModifier($JSON);
                case 'close':
                    return $this->closeModifier($JSON);
                case 'cancel':
                    return $this->cancelModifier($JSON);
            }
        }

        if ($object == 'forcedmodifier')
        {
            switch ($method)
            {
                case 'get':
                    return $this->get86ForcedModifier();
                case 'outofstock':
                    return $this->outOfStockForcedModifiers();
                case 'lowstock':
                    return $this->lowStockForcedModifiers();
                case 'add':
                    return $this->add86ForcedModifier($JSON);
                case 'update':
                    return $this->update86ForcedModifier($JSON);
                case 'reserve':
                    return $this->reserveForcedModifier($JSON);
                case 'close':
                    return $this->closeForcedModifier($JSON);
                case 'cancel':
                    return $this->cancelForcedModifier($JSON);
            }
        }

        return array("ERROR" => "No valid Command");
    }

    private function get86menuItem()
    {
        $querystring = "SELECT id, menuItemID, inventoryItemID, quantityUsed, criticalItem, reservation "
            . "FROM menuitem_86 "
            . "WHERE _deleted = 0";
        $results = $this->_qHelper->executeQuery($querystring);
        return $results;
    }

    private function outOfStockItems()
    {
        $querystring = "SELECT id, criticalItem FROM menuitem_86 "
            . "WHERE _deleted = 0 AND inventoryItemID "
            . "IN (SELECT id FROM inventory_items "
            . "WHERE inventory_items.id = menuitem_86.inventoryItemID AND inventory_items.quantity < menuitem_86.quantityUsed)";
        $results = $this->_qHelper->executeQuery($querystring);

        if ($results === FALSE)
        {
            return array('ERROR' => 'Error getting Out of Stock Menu Items');
        }
        return $results;
    }

    private function add86menuItem($JSON)
    {
        $decode = json_decode($JSON);

        $querystring = "INSERT INTO menuitem_86 SET menuItemID = ?, "
            . "inventoryItemID = ?, "
            . "criticalItem = ?,"
            . "quantityUsed = ?,"
            . "unitID = ?";
        $prepStatement = $this->_qHelper->prepareQuery($querystring);
        $addedID = array();
        if ($prepStatement)
        {
            try
            {
                for ($i = 0; $i < count($decode); $i++)
                {
                    $menuItemID = $decode[$i]->menuItemID;
                    $inventoryItemID = $decode[$i]->inventoryItemID;
                    $criticalItem = $decode[$i]->criticalItem;
                    $quantityUsed = $decode[$i]->quantityUsed;
                    $unitId = $decode[$i]->unitID;

                    $prepStatement->execute(array($menuItemID, $inventoryItemID, $criticalItem, $quantityUsed, $unitId));
                    $newID = PDO_CLavuInsertID();
                    array_push($addedID, $newID);
                }
            }
            catch(\Exception $e)
            {
                return array('ERROR' => "Property not set");
            }
        }

        $ids = join(",", $addedID);
        $selectQuery = "SELECT id, menuItemID, inventoryItemID, criticalItem, quantityUsed "
            . "FROM menuitem_86 "
            . "WHERE id IN (" . $ids . ")";
        $result = $this->_qHelper->executeQuery($selectQuery);
        return $result;
    }

    private function update86menuItem($JSON)
    {
        $decode = json_decode($JSON);

        $querystring = "UPDATE menuitem_86 SET "
            . "quantityUsed = ?"
            . "WHERE id = ?";
        $prepStatement = $this->_qHelper->prepareQuery($querystring);
        if ($prepStatement)
        {
            try
            {
                foreach ($decode as $item)
                {
                    $newCount = $item->quantityUsed;
                    $menuItem86ID = $item->id;
                    $prepStatement->execute(array($newCount, $menuItem86ID));
                }
            }
            catch(\Exception $e)
            {
                return array('ERROR' => "Property not set");
            }
        }
        return array('SUCCESS' => "Items updated");
    }

    private function reserveMenuItem($JSON)
    {
        $decode = json_decode($JSON);

        $querystring = "UPDATE menuitem_86 SET reservation = reservation + 1 "
            . "WHERE menuItemID = ?";
        $prepStatement = $this->_qHelper->prepareQuery($querystring);
        if ($prepStatement)
        {
            try
            {
                foreach ($decode as $item)
                {
                    $menuID = $item->menuItemID;
                    $prepStatement->execute(array($menuID));
                }
            }
            catch (\Exception $e)
            {
                return array('ERROR' => "Property not set");
            }
        }
        return array('SUCCESS' => "Items updated");
    }

    private function closeMenuItem($JSON)
    {
        $decode = json_decode($JSON);

        $querystring = "UPDATE inventory_items, menuitem_86 SET menuitem_86.reservation = menuitem_86.reservation - 1, inventory_items.quantity = inventory_items.quantity - menuitem_86.quantityUsed WHERE menuitem_86.menuItemID = ? AND menuitem_86.inventoryItemID = inventory_items.id";
        $prepStatement = $this->_qHelper->prepareQuery($querystring);
        if ($prepStatement)
        {
            try
            {
                foreach ($decode as $item)
                {
                    $menuID = $item->menuItemID;
                    $result = $prepStatement->execute(array($menuID));
                    if(!$result)
                    {
                        return array("ERROR" => "SQL ERROR");
                    }
                }
            }
            catch(\Exception $e)
            {
                return array('ERROR' => "Property not set");
            }
        }

        return array('SUCCESS' => "Items updated");
    }

    private function cancelMenuItem($JSON)
    {
        $decode = json_decode($JSON);

        $querystring = "UPDATE menuitem_86 SET reservation = reservation - 1 "
            . "WHERE menuItemID = ?";
        $prepStatement = $this->_qHelper->prepareQuery($querystring);
        if ($prepStatement)
        {
            try
            {
                foreach ($decode as $item)
                {
                    $menuID = $item->menuItemID;
                    $result = $prepStatement->execute(array($menuID));
                    if(!$result)
                    {
                        return array("ERROR" => "SQL ERROR");
                    }
                }
            }
            catch(\Exception $e)
            {
                return array('ERROR' => "Property not set");
            }
        }

        return array('SUCCESS' => "Items updated");
    }

    private function get86modifier()
    {
        $querystring = "SELECT id, modifierID, inventoryItemID, criticalItem, quantityUsed, reservation "
            . "FROM modifier_86 "
            . "WHERE _deleted = 0";
        $results = $this->_qHelper->executeQuery($querystring);
        return $results;
    }

    private function outOfStockModifiers()
    {
        $querystring = "SELECT id, criticalItem FROM modifier_86 "
            . "WHERE _deleted = 0 AND inventoryItemID "
            . "IN (SELECT id FROM inventory_items "
            . "WHERE inventory_items.id = modifier_86.inventoryItemID AND inventory_items.quantity < modifier_86.quantityUsed)";
        $results = $this->_qHelper->executeQuery($querystring);

        if ($results === FALSE)
        {
            return array('ERROR' => 'Error getting Out of Stock Modifiers');
        }
        return $results;

    }

    private function lowStockModifiers()
    {
        $querystring = "SELECT id, criticalItem FROM modifier_86 "
            . "WHERE _deleted = 0 AND inventoryItemID "
            . "IN (SELECT id FROM inventory_items "
            . "WHERE inventory_items.id = modifier_86.inventoryItemID AND "
            . "inventory_items.quantity < inventory_items.lowQuantity AND "
            . "inventory_items.quantity > 0)";
        $results = $this->_qHelper->executeQuery($querystring);

        if ($results === FALSE)
        {
            return array('ERROR' => 'Error getting low Stock Modifiers');
        }
        return $results;
    }

    //increment reserve count

    private function add86modifier($JSON)
    {
        $decode = json_decode($JSON);

        $querystring = "INSERT INTO modifier_86 SET modifierID = ?, "
            . "inventoryItemID = ?, "
            . "criticalItem = ?, "
            . "quantityUsed = ?, "
            . "unitID = ?";
        $prepStatement = $this->_qHelper->prepareQuery($querystring);
        $addedID = array();
        if ($prepStatement)
        {
            try
            {
                for ($i = 0; $i < count($decode); $i++)
                {
                    $modifierID = $decode[$i]->modifierID;
                    $inventoryItemID = $decode[$i]->inventoryItemID;
                    $criticalItem = $decode[$i]->criticalItem;
                    $quantityUsed = $decode[$i]->quantityUsed;
                    $unitID = $decode[$i]->unitID;
                    if(!$prepStatement->execute(array($modifierID, $inventoryItemID, $criticalItem, $quantityUsed, $unitID)))
                    {
                        return array('ERROR' => "SQL ERROR");
                    }
                    $newID = PDO_CLavuInsertID();
                    array_push($addedID, $newID);
                }
            }
            catch(\Exception $e)
            {
                return array('ERROR' => "Property not set");
            }
        }

        $ids = join(",", $addedID);
        $selectQuery = "SELECT id, modifierID, inventoryItemID, criticalItem, quantityUsed "
            . "FROM modifier_86 "
            . "WHERE id IN (" . $ids . ")";
        $result = $this->_qHelper->executeQuery($selectQuery);
        return $result;
    }

    //increment reserve count

    private function update86modifier($JSON)
    {
        $decode = json_decode($JSON);

        $querystring = "UPDATE modifier_86 SET "
            . "quantityUsed = ?"
            . "WHERE id = ?";
        $prepStatement = $this->_qHelper->prepareQuery($querystring);
        if ($prepStatement)
        {
            try
            {
                foreach ($decode as $item)
                {
                    $newCount = $item->quantityUsed;
                    $mod86ID = $item->id;
                    $result = $prepStatement->execute(array($newCount, $mod86ID));
                }
            }
            catch(\Exception $e)
            {
                return array('ERROR' => "Property not set");
            }

        }
        return array('SUCCESS' => "Items updated");
    }

    //decrement reserve count and inventory item count

    private function reserveModifier($JSON)
    {
        $decode = json_decode($JSON);

        $querystring = "UPDATE modifier_86 SET reservation = reservation + 1 "
            . "WHERE modifierID = ?";
        $prepStatement = $this->_qHelper->prepareQuery($querystring);
        if ($prepStatement)
        {
            try
            {
                foreach ($decode as $item)
                {
                    $mod86ID = $item->modifierID;
                    $prepStatement->execute(array($mod86ID));

                }
            }
            catch(\Exception $e)
            {
                return array('ERROR' => "Property not set");
            }
        }

        return array('SUCCESS' => "Items updated");
    }

    //decrement reserve count and inventory item count

    private function closeModifier($JSON)
    {
        $decode = json_decode($JSON);

        $querystring = "UPDATE inventory_items, modifier_86 "
            . "SET modifier_86.reservation = modifier_86.reservation - 1, "
            . "inventory_items.quantity = inventory_items.quantity - modifier_86.quantityUsed "
            . "WHERE modifier_86.modifierID = ? AND  modifier_86.inventoryItemID = inventory_items.id";
        $prepStatement = $this->_qHelper->prepareQuery($querystring);
        if ($prepStatement)
        {
            try
            {
                foreach ($decode as $item)
                {
                    $mod86ID = $item->modifierID;
                    $prepStatement->execute(array($mod86ID));
                }
            }
            catch(\Exception $e)
            {
                return array('ERROR' => "Property not set");
            }
        }
        return array('SUCCESS' => "Items updated");
    }

    //decrement reserve count without decrementing items

    private function cancelModifier($JSON)
    {
        $decode = json_decode($JSON);

        $querystring = "UPDATE modifier_86 SET reservation = reservation - 1 "
            . "WHERE modifierID = ?";
        $prepStatement = $this->_qHelper->prepareQuery($querystring);
        if ($prepStatement)
        {
            try
            {
                foreach ($decode as $item)
                {
                    $mod86ID = $item->modifierID;
                    $prepStatement->execute(array($mod86ID));
                }
            }
            catch(\Exception $e)
            {
                return array('ERROR' => "Property not set");
            }
        }
        return array('SUCCESS' => "Items updated");
    }

    //decrement reserve count without item

    private function get86ForcedModifier()
    {
        global $loc_id;
        $querystring = "SELECT id, forced_modifierID, inventoryItemID, criticalItem, quantityUsed "
            . "FROM forced_modifier_86 "
            . "WHERE _deleted = '0'";
        $results = $this->_qHelper->executeQuery($querystring);
        return $results;
    }

    private function outOfStockForcedModifiers()
    {
        $querystring = "SELECT id, criticalItem FROM forced_modifier_86 "
            . "WHERE _deleted = 0 AND inventoryItemID "
            . "IN (SELECT id FROM inventory_items "
            . "WHERE inventory_items.id = forced_modifier_86.inventoryItemID AND inventory_items.quantity < forced_modifier_86.quantityUsed)";
        $results = $this->_qHelper->executeQuery($querystring);

        if ($results === FALSE)
        {
            return array('ERROR' => 'Error getting Out of Stock Forced Modifiers');
        }
        return $results;
    }


    //Forced Modifiers:

    private function lowStockForcedModifiers()
    {
        $querystring = "SELECT id, criticalItem FROM forced_modifier_86 "
            . "WHERE _deleted = 0 AND inventoryItemID "
            . "IN (SELECT id FROM inventory_items "
            . "WHERE inventory_items.id = forced_modifier_86.inventoryItemID AND "
            . "inventory_items.quantity < inventory_items.lowQuantity AND "
            . "inventory_items.quantity > 0)";
        $results = $this->_qHelper->executeQuery($querystring);

        if ($results === FALSE)
        {
            return array('ERROR' => 'Error getting low stock Forced Modifiers');
        }
        return $results;
    }

    private function add86ForcedModifier($JSON)
    {
        $decode = json_decode($JSON);

        $querystring = "INSERT INTO forced_modifier_86 SET forced_modifierID = ?, "
            . "inventoryItemID = ?, "
            . "criticalItem = ?,"
            . "quantityUsed = ?,"
            . "unitID = ?";
        $prepStatement = $this->_qHelper->prepareQuery($querystring);
        $addedID = array();
        if ($prepStatement)
        {
            for ($i = 0; $i < count($decode); $i++)
            {
                $modifierID = $decode[$i]->forced_modifierID;
                $inventoryItemID = $decode[$i]->inventoryItemID;
                $criticalItem = $decode[$i]->criticalItem;
                $quantityUsed = $decode[$i]->quantityUsed;
                $unitID = $decode[$i]->unitID;
                $prepStatement->execute(array($modifierID, $inventoryItemID, $criticalItem, $quantityUsed, $unitID));
                $newID = PDO_CLavuInsertID();
                array_push($addedID, $newID);
            }
        }

        $ids = join(",", $addedID);
        $selectQuery = "SELECT id, forced_modifierID, inventoryItemID, criticalItem, quantityUsed "
            . "FROM forced_modifier_86 "
            . "WHERE id IN (" . $ids . ")";
        $result = $this->_qHelper->executeQuery($selectQuery);
        return $result;
    }

    //decrement reserve count without item

    private function update86ForcedModifier($JSON)
    {
        $decode = json_decode($JSON);

        $querystring = "UPDATE forced_modifier_86 SET "
            . "quantityUsed = ?"
            . "WHERE id = ?";
        $prepStatement = $this->_qHelper->prepareQuery($querystring);
        if ($prepStatement)
        {
            try
            {
                foreach ($decode as $item)
                {
                    $newCount = $item->quantityUsed;
                    $mod86ID = $item->id;
                    if(!$prepStatement->execute(array($newCount, $mod86ID)))
                    {
                        return array("ERROR" => "SQL ERROR");
                    }
                }
            }
            catch(\Exception $e)
            {
                return array('ERROR' => "Property not set");
            }
        }
        return array('SUCCESS' => "Items updated");
    }

    private function reserveForcedModifier($JSON)
    {
        $decode = json_decode($JSON);

        $querystring = "UPDATE forced_modifier_86 SET reservation = reservation + 1 "
            . "WHERE forced_modifierID = ?";
        $prepStatement = $this->_qHelper->prepareQuery($querystring);
        if ($prepStatement)
        {
            try
            {
                foreach ($decode as $item)
                {
                    $mod86ID = $item->forced_modifierID;
                    if(!$prepStatement->execute(array($mod86ID)))
                    {
                        return array('ERROR' => "SQL ERROR");
                    }
                }
            }
            catch(\Exception $e)
            {
                return array('ERROR' => "Property not set");
            }
        }
        return array('SUCCESS' => "Items updated");
    }

    //decrement reserve count and inventory item count

    private function closeForcedModifier($JSON)
    {
        $decode = json_decode($JSON);

        $querystring = "UPDATE inventory_items, forced_modifier_86"
            . " SET forced_modifier_86.reservation = forced_modifier_86.reservation - 1,"
            . " inventory_items.quantity = inventory_items.quantity - forced_modifier_86.quantityUsed "
            . " WHERE forced_modifier_86.forced_modifierID = ? AND forced_modifier_86.inventoryItemID = inventory_items.id";
        $prepStatement = $this->_qHelper->prepareQuery($querystring);
        if ($prepStatement)
        {
            try
            {
                foreach ($decode as $item)
                {
                    $mod86ID = $item->forced_modifierID;
                    if(!$prepStatement->execute(array($mod86ID)))
                    {
                        return array('ERROR' => "SQL ERROR");
                    }
                }
            }
            catch(\Exception $e)
            {
                return array('ERROR' => "Property not set");
            }
        }
        return array('SUCCESS' => "Items updated");
    }

    //increment reserve count

    private function cancelForcedModifier($JSON)
    {
        $decode = json_decode($JSON);

        $querystring = "UPDATE forced_modifier_86 SET reservation = reservation - 1 "
            . "WHERE forced_modifierID = ?";
        $prepStatement = $this->_qHelper->prepareQuery($querystring);
        if ($prepStatement)
        {
            try
            {
                foreach ($decode as $item)
                {
                    $mod86ID = $item->forced_modifierID;
                    if(!$prepStatement->execute(array($mod86ID)))
                    {
                        return array('ERROR' => "SQL ERROR");
                    }
                }
            }
            catch(\Exception $e)
            {
                return array('ERROR' => "Property not set");
            }
        }
        return array('SUCCESS' => "Items updated");
    }

    private function lowStockMenuItems()
    {
        $querystring = "SELECT id, criticalItem FROM menuitem_86 "
            . "WHERE _deleted = 0 AND inventoryItemID "
            . "IN (SELECT id FROM inventory_items "
            . "WHERE inventory_items.id = menuitem_86.inventoryItemID AND "
            . "inventory_items.quantity < inventory_items.lowQuantity AND "
            . "inventory_items.quantity > 0)";
        $results = $this->_qHelper->executeQuery($querystring);

        if ($results === FALSE)
        {
            return array('ERROR' => 'Error getting low Stock menu items');
        }
        return $results;
    }

}
