<?php
/*
 * Menu Item Controller
 * Handles all of the functionality relating to the
 * menu items and modifiers for the inventory system
 */
if (!defined('_HELPERS_'))
{
    define('_HELPERS_', dirname(dirname(dirname(__FILE__))));
}
require_once('/' . _HELPERS_ . '/Helpers/queryHelper.php');

class MenuController
{
    private $_qHelper;

    public function __construct($queryHelper = null)
    {
        if(!$queryHelper) $queryHelper = new queryHelper();

        $this->_qHelper = $queryHelper;
    }

    public function routeCommand($object = '', $method = '', $JSON = '')
    {
        if ($object === '' || $method === '')
        {
            return array('ERROR' => 'No valid Command');
        }

        switch ($object)
        {
            case 'menuitem':
                switch ($method)
                {
                    case 'get':
                        return $this->getItems();
                }
                break;
            case 'modifier':
                switch ($method)
                {
                    case 'get':
                        return $this->getModifiers();
                }
                break;
            case 'menucategory':
                switch ($method)
                {
                    case 'get':
                        return $this->getItemCategories();
                }
                break;
            case 'modifiercategory':
                switch ($method)
                {
                    case 'get':
                        return $this->getModifierCategories();
                }
                break;
            case 'forcedmodifier':
                switch ($method)
                {
                    case 'get':
                        return $this->getForcedModifiers();
                }
                break;
            case 'forcedmodifierlist':
                switch ($method)
                {
                    case 'get':
                        return $this->getForcedModifierList();
                }
                break;
            case 'forcedmodifiergroup':
                switch ($method)
                {
                    case 'get':
                        return $this->getForcedModifierGroups();
                }
                break;
        }

        return array('ERROR' => 'No valid Command 2');
    }

    private function getItems()
    {
        $querystring = "SELECT id, `name`, category_id "
            . "FROM menu_items "
            . "WHERE _deleted='0'"
            . "AND `category_id` IN ( SELECT `id` FROM `menu_categories` WHERE `_deleted` ='0')";
        $results = $this->_qHelper->executeQuery($querystring);
        if ($results == FALSE)
        {
            return array('ERROR' => 'Get items failed.');
        }
        return $results;
    }

    private function getModifiers()
    {
        $querystring = "SELECT id, title, category "
            . "FROM modifiers "
            . "WHERE _deleted='0'"
            . "AND `category` IN ( SELECT `id` FROM `modifier_categories` WHERE `_deleted` ='0')";
        $results = $this->_qHelper->executeQuery($querystring);
        if ($results == FALSE)
        {
            return array('ERROR' => 'Get modifiers failed.');
        }
        return $results;
    }

    private function getItemCategories()
    {
        $querystring = "SELECT id, name, description "
            . "FROM menu_categories "
            . "WHERE _deleted='0'";
        $results = $this->_qHelper->executeQuery($querystring);
        if ($results == FALSE)
        {
            return array('ERROR' => 'Get item categories failed.');
        }
        return $results;
    }

    private function getModifierCategories()
    {
        $querystring = "SELECT id, title, description "
            . "FROM modifier_categories "
            . "WHERE _deleted='0'";
        $results = $this->_qHelper->executeQuery($querystring);
        if ($results == FALSE)
        {
            return array('ERROR' => 'Get modifier categories failed.');
        }
        return $results;
    }

    private function getForcedModifiers()
    {
        $querystring = "SELECT id, title, list_id "
            . "FROM forced_modifiers "
            . "WHERE _deleted='0'"
            . "AND `list_id` IN ( SELECT `id` FROM `forced_modifier_lists` WHERE `_deleted` ='0')";
        $results = $this->_qHelper->executeQuery($querystring);
        if ($results == FALSE)
        {
            return array('ERROR' => 'Get forced modifiers failed.');
        }
        return $results;
    }

    private function getForcedModifierList()
    {
        $querystring = "SELECT id, title, description "
            . "FROM forced_modifier_lists "
            . "WHERE _deleted='0'";
        $results = $this->_qHelper->executeQuery($querystring);
        if ($results == FALSE)
        {
            return array('ERROR' => 'Get forced modifier lists failed.');
        }
        return $results;
    }

    private function getForcedModifierGroups()
    {
        $querystring = "SELECT id, title, include_lists "
            . "FROM forced_modifier_groups "
            . "WHERE _deleted='0'";
        $results = $this->_qHelper->executeQuery($querystring);
        if ($results == FALSE)
        {
            return array('ERROR' => 'Get forced modifier groups failed.');
        }
        return $results;
    }
}