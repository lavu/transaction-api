<?php
/*
 * Inventory Management Controller
 * Handles all of the functionality relating to the
 * core inventory items.
 */
if (!defined('_HELPERS_'))
{
    define('_HELPERS_', dirname(dirname(dirname(__FILE__))));
}
require_once(_HELPERS_ . '/Helpers/queryHelper.php');

class InventoryManagementController
{

    public $loc_id;
    private $_routes = array();

    private $_qHelper;

    public function __construct($queryHelper = null)
    {
        global $loc_id;
        $this->loc_id = $loc_id;

        if(!$queryHelper) $queryHelper = new queryHelper();

        $this->_qHelper = $queryHelper;
    }

    //take an array of item updates in JSON format and commit them to the database.

    public function routeCommand($object = '', $method = '', $JSON = '')
    {
        if ($object === '' || $method === '')
        {
            return array('ERROR' => 'No valid Command');
        }
        if ($object === 'item')
        {
            switch ($method)
            {
                case 'add':
                    return $this->addItems($JSON);
                case 'update':
                    return $this->updateItems($JSON);
                case 'get':
                    return $this->getItems();
                //TODO Need to add a way to get a specific items.
            }
        }
        else
        {
            if ($object === 'category')
            {
                switch ($method)
                {
                    case 'add':
                        return $this->addCategory($JSON);
                    case 'get':
                        return $this->getCategories();
                }
            }
            else
            {
                if ($object === 'audit')
                {
                    switch ($method)
                    {
                        case 'add':
                            return $this->addAudit($JSON);
                        case 'get':
                            return $this->getAudit($JSON);
                    }
                }
            }
        }
        return array('ERROR' => 'No valid Command');
    }

    //take an array of new items to add in JSON format and commit them to the database

    private function addItems($JSON)
    {
        $decode = json_decode($JSON, true);
        //var_dump($decode);
        $querystring = "INSERT INTO inventory_items (name, categoryID, unitID,"
            . "quantity, lowQuantity, parQuantity, loc_id) VALUES (?, ?, ?, ?, ?, ?, ?)";
        $preppedStatement = $this->_qHelper->prepareQuery($querystring);
        $addedID = array();
        try
        {
            foreach ($decode as $item)
            {
                $name = $item['name'];
                $categoryID = $item['categoryID'];
                $unitID = $item['unitID'];
                $quantity = $item['quantity'];
                $lowQuantity = $item['lowQuantity'];
                $parQuantity = $item['parQuantity'];
                $test = $preppedStatement->execute(array($name, $categoryID, $unitID, $quantity, $lowQuantity, $parQuantity, $this->loc_id));
                if (!$test)
                {
                    continue;
                }
                $newID = PDO_CLavuInsertID();
                array_push($addedID, $newID);
            }
        }
        catch (\Exception $e)
        {
            return array('ERROR' => 'Json property not set');
        }

        $ids = join(",", $addedID);
        $sql = "SELECT id, name, categoryID, unitID, quantity, lowQuantity, "
            . "parQuantity FROM inventory_items "
            . "WHERE id IN (" . $ids . ")";
        $result = $this->_qHelper->executeQuery($sql);
        if ($result == false)
        {
            return array('ERROR' => 'Add Item failed. 2');
        }
        return $result;
    }

    private function updateItems($JSON)
    {
        $decode = json_decode($JSON, true);
        $preparedQueryString = "UPDATE inventory_items SET "
            . "name=?,"
            . "categoryID=?,"
            . "unitID=?,"
            . "quantity=?,"
            . "lowQuantity=?,"
            . "parQuantity=? "
            . "WHERE id = ?";
        $preppedStatement = $this->_qHelper->prepareQuery($preparedQueryString);

        if (empty($decode))
        {
            return array('ERROR' => 'Json parser error');
        }

        try
        {
            foreach ($decode as $item)
            {
                if ($preppedStatement)
                {
                    $name = $item['name'];
                    $categoryID = $item['categoryID'];
                    $unitID = $item['unitID'];
                    $quantity = $item['quantity'];
                    $lowQuantity = $item['lowQuantity'];
                    $parQuantity = $item['parQuantity'];
                    $id = $item['id'];
                    $success = $preppedStatement->execute(array($name, $categoryID, $unitID, $quantity, $lowQuantity, $parQuantity, $id));
                    if ($success == false)
                    {
                        return array('ERROR' => 'Update Item failed');
                    }
                }
            }
        }
        catch (\Exception $e)
        {
            return array('ERROR' => 'Json property not set');
        }

        return array('SUCCESS' => "true");
    }

    private function getItems()
    {
        $querystring = "SELECT id, name, categoryID, unitID, quantity, lowquantity, parquantity "
            . "FROM inventory_items "
            . "WHERE _deleted='0' AND loc_id = '" . $this->loc_id . "'";

        $results = $this->_qHelper->executeQuery($querystring);
        if ($results == FALSE)
        {
            return array('ERROR' => 'Get items failed.');
        }
        return $results;
    }

    private function addCategory($JSON)
    {
        $decode = json_decode($JSON, true);
        $querystring = "INSERT INTO inventory_categories SET name = ?, description = ?, loc_id = ?";
        $preppedStatement = $this->_qHelper->prepareQuery($querystring);
        if ($preppedStatement == false)
        {
            return array('ERROR' => 'Add category failed. 1');
        }
        try
        {
            $result = $preppedStatement->execute(array($decode['name'], $decode['description'], $this->loc_id));
        }
        catch (\Exception $e)
        {
            return array('ERROR' => 'Json property not set');
        }

        if ($result == FALSE)
        {
            return array('ERROR' => 'Add category failed. 2');
        }
        $newID = PDO_CLavuInsertID();
        $sql = "SELECT id, name, description FROM inventory_categories"
            . " WHERE id = " . $newID;
        $selectResult = $this->_qHelper->executeQuery($sql);
        if ($selectResult == FALSE)
        {
            return array('ERROR' => 'Add category failed. 3');
        }
        return $selectResult;
    }

    private function getCategories()
    {
        $querystring = "SELECT id, name, description "
            . "FROM inventory_categories "
            . "WHERE _deleted='0' AND loc_id = '" . $this->loc_id . "'";

        $results = $this->_qHelper->executeQuery($querystring);
        if ($results == FALSE)
        {
            return array('ERROR' => 'Get categories failed.');
        }
        return $results;
    }

    private function addAudit($JSON)
    {
        $decode = json_decode($JSON, true);
        $queryString = "INSERT INTO inventory_audit SET inventoryItemID = ?, oldCount = (SELECT quantity FROM inventory_items WHERE id = ?), newCount = ?, userID = ?, userNotes = ?, loc_id = ?";
        $preppedStatement = $this->_qHelper->prepareQuery($queryString);

        try
        {
            $result = $preppedStatement->execute(array($decode['inventoryItemID'], $decode['inventoryItemID'], $decode['newCount'], $decode['userID'], $decode['userNotes'], $this->loc_id));
            if ($result == FALSE)
            {
                return array('ERROR' => 'Add audit failed');
            }
        }
        catch (\Exception $e)
        {
            return array('ERROR' => 'Json property not set');
        }


        return array('SUCCESS' => "true");
    }

    private function getAudit($JSON)
    {
        $queryString = "SELECT id, inventoryItemID, oldCount, newCount, userID, userNotes, datetime "
            . "FROM inventory_audit WHERE _deleted = '0'";
        if (!empty($JSON))
        {
            $decode = json_decode($JSON, true);
            $forPrep = array();
            $queryString .= "AND loc_id = ?";
            if (array_key_exists('loc_id', $decode))
            {
                $forPrep[] = $decode['loc_id'];
            }
            else
            {
                $forPrep[] = $this->loc_id;
            }
            if (array_key_exists('inventoryItemID', $decode))
            {
                $queryString .= " AND inventoryItemID = ?";
                $forPrep[] = $decode['inventoryItemID'];
            }
            if (array_key_exists('userID', $decode))
            {
                $queryString .= " AND userID = ?";
                $forPrep[] = $decode['userID'];
            }
            $preppedStatement = $this->_qHelper->prepareQuery($queryString);
            $test = $preppedStatement->execute($forPrep);
            if ($test == FALSE)
            {
                return array('ERROR' => 'Get audit failed. 2');
            }
            return $preppedStatement->fetchAll('ASSOC');
        }
        else
        {
            $results = $this->_qHelper->executeQuery($queryString);
            if ($results == FALSE)
            {
                return array('ERROR' => 'Get audit failed. 3');
            }
            return $results;
        }
    }
}