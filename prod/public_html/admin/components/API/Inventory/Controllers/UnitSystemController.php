<?php

if (!defined('_HELPERS_'))
{
    define('_HELPERS_', dirname(dirname(dirname(__FILE__))));
}
require_once(_HELPERS_ . '/Helpers/queryHelper.php');

class UnitSystemController
{

    private $_qHelper;

    public function __construct($queryHelper = null)
    {
        if(!$queryHelper) $queryHelper = new queryHelper();

        $this->_qHelper = $queryHelper;
    }

    public function routeCommand($object = '', $method = '', $JSON = '')
    {
        if ($object === '' || $method === '')
        {
            return array('ERROR' => "Unit System: No valid Command");
        }
        if ($object == 'unit')
        {
            switch ($method)
            {
                case 'get':
                    return $this->getUnits($JSON);
                case 'add':
                    return $this->addUnit($JSON);
                default:
                    return array('ERROR' => "Unit System - unit: Invalid command");
            }
        }
        else
        {
            if ($object == 'category')
            {
                switch ($method)
                {
                    case 'get':
                        return $this->getCategories();
                    case 'add':
                        return $this->addCategory($JSON);
                    default:
                        return array('ERROR' => "Unit System - category: Invalid command");
                }
            }
        }
        return array('ERROR' => "Unit System: No valid object");
    }

    private function GetUnits($JSON)
    {
        $querystring = "SELECT id, name, symbol, categoryID, conversion "
            . "FROM inventory_unit "
            . "WHERE _deleted='0'";
        $results = false;
        $decode = json_decode($JSON, true);
        if (!empty($decode))
        {
            $results = array();
            $querystring .= " AND id = ?";
            $preppedStatement = $this->_qHelper->prepareQuery($querystring);
            foreach ($decode as $unit)
            {
                $temp = $preppedStatement->execute(array($unit['id']));
                if ($temp)
                {
                    $results[] = $preppedStatement->fetchAll('ASSOC');
                }
            }
        }
        else
        {
            $results = $this->_qHelper->executeQuery($querystring);
        }
        if ($results === FALSE)
        {
            return array('ERROR' => 'Unable to get units');
        }
        return $results;
    }

    private function addUnit($JSON)
    {
        if ($JSON === '')
        {
            return array('ERROR' => 'No Units Added, JSON Error');
        }
        $decode = json_decode($JSON, true);
        $querystring = "INSERT INTO inventory_unit SET name = ?, symbol = ?, categoryID = ?, conversion = ?";
        $preppedStatement = $this->_qHelper->prepareQuery($querystring);
        try
        {
            $result = $preppedStatement->execute(
                array(
                    $decode[0]['name'],
                    $decode[0]['symbol'],
                    $decode[0]['categoryID'],
                    $decode[0]['conversion']
                )
            );
        }
        catch (\Exception $e)
        {
            return array('ERROR' => 'One of the required fields for Unit add is not set');
        }

        $newRowID = PDO_CLavuInsertID();
        $selectQueryString = "SELECT id, name, symbol, categoryID, conversion "
            . "FROM inventory_unit "
            . "WHERE id = " . $newRowID;
        $selectResult = $this->_qHelper->executeQuery($selectQueryString);
        if ($selectResult === FALSE)
        {
            return array('ERROR' => 'Unable to find unit after creation.');
        }
        return $selectResult;
    }

    private function getCategories()
    {
        $querystring = "SELECT id, name "
            . "FROM unit_category "
            . "WHERE _deleted = '0'";
        $results = $this->_qHelper->executeQuery($querystring);
        if ($results === FALSE)
        {
            return array('ERROR' => 'Unable to get unit categories.');
        }
        return $results;
    }

    private function addCategory($JSON)
    {
        $decode = json_decode($JSON, true);
        $querystring = "INSERT INTO unit_category SET name = ?";
        $preppedStatement = $this->_qHelper->prepareQuery($querystring);
        try
        {
            $preppedStatement->execute(array($decode[0]['name']));
        }
        catch (\Exception $e)
        {
            return array('ERROR' => 'Unit category add must have a name');
        }

        $newRowID = PDO_CLavuInsertID();
        $selectQueryString = "SELECT id, name "
            . "FROM unit_category "
            . "WHERE id = " . $newRowID;
        $selectResult = $this->_qHelper->executeQuery($selectQueryString);
        if ($selectResult === FALSE)
        {
            return array('ERROR' => 'Unable to find unit category after creation.');
        }
        return $selectResult;
    }

}
