<?php

/*
 * Controller for Vendors and Vendor Items
 * Handles the database interactiosn related to those items
 */

if (!defined('_HELPERS_'))
{
    define('_HELPERS_', dirname(dirname(dirname(__FILE__))));
}
require_once(_HELPERS_ . '/Helpers/queryHelper.php');

class VendorController
{

    private $_qHelper;

    public function __construct($queryHelper = null)
    {
        global $loc_id;
        $this->loc_id = $loc_id;

        if(!$queryHelper) $queryHelper = new queryHelper();

        $this->_qHelper = $queryHelper;
    }

    public function routeCommand($object = '', $method = '', $JSON = '')
    {
        if ($object === '')
        {
            return array('ERROR' => 'No valid object');
        }
        if ($object == 'vendor')
        {
            switch ($method)
            {
                case 'get':
                    return $this->getVendors();
                case 'add':
                    return $this->addVendors($JSON);
                case 'update':
                    return $this->updateVendors($JSON);
                default:
                    break;
            }
        }
        else
        {
            if ($object == 'item')
            {
                switch ($method)
                {
                    case 'get':
                        return $this->getVendorItems($JSON);
                    case 'add':
                        return $this->addVendorItems($JSON);
                    case 'update':
                        return $this->updateVendorItems($JSON);
                    default:
                        break;
                }
            }
        }
        return array('ERROR' => 'No valid method');
    }

    private function getVendors()
    {
        $querystring = "SELECT id, name, contactName, contactPhone, contactEmail FROM vendors WHERE _deleted = '0'";
        $results = $this->_qHelper->executeQuery($querystring);
        if ($results === FALSE)
        {
            return array('ERROR' => 'Get vendors failed.');
        }
        return $results;
    }

    private function addVendors($JSON)
    {
        $decode = json_decode($JSON, true);
        $querystring = "INSERT INTO vendors SET name = ?, contactName = ?, contactPhone = ?, contactEmail = ?, loc_id = " . $this->loc_id;
        $preppedStatement = $this->_qHelper->prepareQuery($querystring);
        $addedIDs = array();
        if ($preppedStatement === FALSE)
        {
            return array('ERROR' => 'Add vendors failed. 1');
        }
        try
        {
            foreach ($decode as $vendor)
            {
                $test = $preppedStatement->execute(array($vendor['name'], $vendor['contactName'], $vendor['contactPhone'], $vendor['contactEmail']));
                if ($test == FALSE)
                {
                    continue;
                }
                $newID = PDO_CLavuInsertID();
                $addedIDs[] = $newID;
            }
        }
        catch (\Exception $e)
        {
            return array('ERROR' => 'Vendor Add property not set');
        }
        $idList = join(",", $addedIDs);
        $sql = "SELECT ID, name, contactName, contactPhone, contactEmail FROM vendors WHERE id IN ({$idList})";
        $selectResult = $this->_qHelper->executeQuery($sql);
        if ($selectResult === FALSE)
        {
            return array('ERROR' => 'Add vendors failed. 2');
        }
        return $selectResult;
    }

    private function updateVendors($JSON)
    {
        $decode = json_decode($JSON, true);
        $preparedQueryString = "UPDATE vendors SET "
            . "name=?,"
            . "contactName=? ,"
            . "contactPhone=? ,"
            . "contactEmail=? "
            . "WHERE id = ?";
        $preppedStatement = $this->_qHelper->prepareQuery($preparedQueryString);
        $toReturn = array();
        try
        {
            foreach ($decode as $vendor)
            {
                $temp = array($vendor['name'], $vendor['contactName'], $vendor['contactPhone'], $vendor['contactEmail'], $vendor['id']);
                $test = $preppedStatement->execute($temp);
                if ($test)
                {
                    $toReturn[] = $vendor;
                }
            }
        }
        catch (\Exception $e)
        {
            return array('ERROR' => 'Vendor update property not set');
        }
        return $toReturn;
    }

    private function getVendorItems($JSON)
    {
        $decode = json_decode($JSON, true);
        $queryString = "SELECT id, inventoryItemID, vendorID, unitID, minOrderQuantity, SKU FROM vendor_items WHERE _deleted = '0'";
        $queryOptions = array();

        if (isset($decode['id']))
        {
            $queryOptions[] = $decode['id'];
            $queryString .= " AND id = ?";
        }

        if (isset($decode['inventoryItemID']))
        {
            $queryOptions[] = $decode['inventoryItemID'];
            $queryString .= " AND inventoryItemID = ?";
        }

        if (isset($decode['vendorID']))
        {
            $queryOptions[] = $decode['vendorID'];
            $queryString .= " AND vendorID = ?";
        }

        if (isset($decode['unitID']))
        {
            $queryOptions[] = $decode['unitID'];
            $queryString .= " AND unitID = ?";
        }

        if (isset($decode['SKU']))
        {
            $queryOptions[] = $decode['SKU'];
            $queryString .= " AND SKU = ?";
        }

        $preppedStatement = $this->_qHelper->prepareQuery($queryString);
        $results = $preppedStatement->execute($queryOptions);
        if ($results === false)
        {
            return array('ERROR' => 'Get vendor items failed');
        }

        return $preppedStatement->fetchAll();
    }

    private function addVendorItems($JSON)
    {
        $decode = json_decode($JSON, true);
        $querystring = "INSERT INTO vendor_items SET inventoryItemID = ?, vendorID = ?, unitID = ?, minOrderQuantity = ?, SKU = ?";
        $preppedStatement = $this->_qHelper->prepareQuery($querystring);
        $addedIDs = array();
        if ($preppedStatement == FALSE)
        {
            return array('ERROR' => 'Add vendor items failed. 1');
        }
        foreach ($decode as $vendorItem)
        {
            $forPrep = array(
                $vendorItem['inventoryItemID'],
                $vendorItem['vendorID'],
                $vendorItem['unitID'],
                $vendorItem['minOrderQuantity'],
                $vendorItem['SKU']
            );
            $test = $preppedStatement->execute($forPrep);
            if ($test)
            {
                $newID = PDO_CLavuInsertID();
                $addedIDs[] = $newID;
            }
        }
        $idList = join(",", $addedIDs);
        $sql = "SELECT id, inventoryItemID, vendorID, unitID, minOrderQuantity, SKU FROM vendor_items WHERE id IN ({$idList})";
        $selectResult = $this->_qHelper->executeQuery($sql);
        if ($selectResult == FALSE)
        {
            return array('ERROR' => 'Add vendor items failed. 2');
        }
        return $selectResult;
    }

    private function updateVendorItems($JSON)
    {
        $decode = json_decode($JSON, true);
        $preparedQueryString = "UPDATE vendor_items SET "
            . "inventoryItemID=?,"
            . "vendorID=?,"
            . "unitID=?,"
            . "minOrderQuantity=?,"
            . "SKU=? "
            . "WHERE id = ?";
        $preppedStatement = $this->_qHelper->prepareQuery($preparedQueryString);
        if ($preppedStatement == FALSE)
        {
            return array('ERROR' => 'Update vendor items failed. 1');
        }
        $toReturn = array();

        try
        {
            foreach ($decode as $vendorItem)
            {
                $itemID = $vendorItem['inventoryItemID'];
                $vendorID = $vendorItem['vendorID'];
                $unitID = $vendorItem['unitID'];
                $minOrder = $vendorItem['minOrderQuantity'];
                $SKU = $vendorItem['SKU'];
                $id = $vendorItem['id'];
                $test = $preppedStatement->execute(array($itemID, $vendorID, $unitID, $minOrder, $SKU, $id));
                if ($test)
                {
                    $toReturn[] = $vendorItem;
                }
            }
        }
        catch (\Exception $e)
        {
            return array("ERROR" => 'Error vendor items property not set');
        }

        return $toReturn;
    }

}