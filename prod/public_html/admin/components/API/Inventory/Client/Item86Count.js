/*
 * JS for loading/interacting with the 86 count system.
 * Loads and can add to the item count
 */
var MenuItems86 = [];
var Modifiers86 = [];

function menuItem86(id, menuID, inventoryID, criticalItem, quantityUsed){
    this.id = id;
    this.menuID = menuID;
    this.inventoryID = inventoryID;
    this.criticalItem = criticalItem;
    this.quantityUsed = quantityUsed;
    this.update = function(){

    };
}

function modifier86(id, modifierID,inventoryID, criticalItem, quantityUsed){
    this.id = id;
    this.modifierID = modifierID;
    this.inventoryID = inventoryID;
    this.criticalItem = criticalItem;
    this.quantityUsed = quantityUsed;
    this.update = function(){

    };
}

function load86CountSystem(){
    loadMenuItem86();
    loadModifier86();
}

function loadMenuItem86(){
    var object = 'menuitem';
    var method = 'get';
    var callback = function(responseText){
        if(responseText !== false){
            var responseArray = responseText;
            for (var i = 0; i < responseArray.length; i++){
                curItem = responseArray[i];
                var newItem = new menuItem86(curItem['id'], curItem['menuItemID'], curItem['inventoryItemID'], curItem['criticalItem'], curItem['quantityUsed']);
                MenuItems86[newItem.id] = newItem;
            }
        }
    };
     var APICall = APICallHelper(object, method, "86CountController", '', callback);
    APICall.sendRequest();
}

function loadModifier86(){
    var object = 'modifier';
    var method = 'get';
    var callback = function(responseText){
        if(responseText !== false){
            var responseArray = responseText;
            for (var i = 0; i < responseArray.length; i++){
                curItem = responseArray[i];
                var newItem = new modifier86(curItem['id'], curItem['modifierID'], curItem['inventoryItemID'], curItem['criticalItem'], curItem['quantityUsed']);
                Modifiers86[newItem.id] = newItem;
            }
        }
    };
     var APICall = APICallHelper(object, method, "86CountController", '', callback);
    APICall.sendRequest();
}

function addMenuItem86(JSONItem){
    var object = 'menuitem';
    var method = 'add';
    var callback = function(responseText){
        if(responseText !== false){
            var responseArray = responseText;
            for (var i = 0; i < responseArray.length; i++){
                curItem = responseArray[i];
                var newItem = new menuItem86(curItem['id'], curItem['menuItemID'], curItem['inventoryItemID'], curItem['criticalItem'], curItem['quantityUsed']);
                MenuItems86[newItem.id] = newItem;
            }
        }
    };
     var APICall = APICallHelper(object, method, "86CountController", JSONItem, callback);
    APICall.sendRequest();
}

function addModifier86(JSONItem){
    var object = 'modifier';
    var method = 'add';
    var callback = function(responseText){
        if(responseText !== false){
            var responseArray = responseText;
            for (var i = 0; i < responseArray.length; i++){
                curItem = responseArray[i];
                var newItem = new modifier86(curItem['id'], curItem['modifierID'], curItem['inventoryItemID'], curItem['criticalItem'], curItem['quantityUsed']);
                Modifiers86[newItem.id] = newItem;
            }
        }
    };
     var APICall = APICallHelper(object, method, "86CountController", JSONItem, callback);
    APICall.sendRequest();
}

/*
 *JSON format:
 * {
 * object: modifier or menuitem
 * action: reserve, close, cancel
 * id: [list of ids for current object/action]
 * }
 */
function processOrder($JSONItem){
    var order = JSON.parse($JSONItem);
    for(var i = 0; i < order.length; i++){
        var curCommand = order[i];
        var object = curCommand['object'];
        var action = curCommand['action'];
        var idList = JSON.stringify(curCommand['id']);

        var callback = function(responseText){
          if(responseText !== false){
              //everything went fine
          }
          
                    
        };

         var APICall = APICallHelper(object, method, "86CountController", JSONItem, callback);
        APICall.sendRequest();
    }

}

function updateMenuItem86($JSONItem){
    //currently not implemented, all action taken through processOrder($JSONItem)
}

function updateModifier86($JSONItem){
    //currently not implemented, all action taken through processOrder($JSONItem)
}
