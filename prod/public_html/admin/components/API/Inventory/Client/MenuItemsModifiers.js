/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var modifiers = [];
var modifier_categories = [];
var menu_items = [];
var menu_categories = [];
var forced_modifiers = [];
var forced_modifier_lists = [];
var forced_modifier_groups = [];

function modifiers(id, title, category){
    this.id = id;
    this.title = title;
    this.category = category;
}

function menu_item(id, name, category_id){
    this.id = id;
    this.name = name;
    this.category_id = category_id;
}

function menuCategory(id, name, description){
    this.id = id;
    this.name = name;
    this.description = description;
    
}

function modifierCategory(id, title, description){
    this.id = id;
    this.title = title;
    this.description = description;
    
}

function forced_modifier(id, title, list_id){
    this.id = id;
    this.title = title;
    this.list_id = list_id;
}

function forced_modifier_list(id, title, description){
    this.id = id;
    this.title = title;
    this.description = description;
}

function forced_modifier_group(id, title, include_lists){
    this.id = id;
    this.title = title;
    this.include_lists = include_lists;
}

function loadMenuItems(){
    var object = 'menuitem';
    var method = 'get';
    var callback = function(responseText){
        if(responseText !== false){
            var responseArray = responseText;
            for (var i = 0; i < responseArray.length; i++){
                curItem = responseArray[i];
                var newItem = new menuItem86(curItem['id'], curItem['name'], curItem['category_id']);  
                menu_items[newItem.id] = newItem;
            }
        }
    };
    var APICall = APICallHelper(object, method, "MenuController", '', callback);
    APICall.sendRequest();
}

function loadModifiers(){
    var object = 'modifier';
    var method = 'get';
    var callback = function(responseText){
        if(responseText !== false){
            var responseArray = responseText;
            for (var i = 0; i < responseArray.length; i++){
                curItem = responseArray[i];
                var newItem = new menuItem86(curItem['id'], curItem['title'], curItem['category']);  
                modifiers[newItem.id] = newItem;
            }
        }
    };
    var APICall = APICallHelper(object, method, "MenuController", '', callback);
    APICall.sendRequest();
}

function loadMenuCategories(){
    var object = 'menucategory';
    var method = 'get';
    var callback = function(responseText){
        if(responseText !== false){
            var responseArray = responseText;
            for (var i = 0; i < responseArray.length; i++){
                curItem = responseArray[i];
                var newItem = new menuItem86(curItem['id'], curItem['name'], curItem['description']);  
                menu_categories[newItem.id] = newItem;
            }
        }
    };
    var APICall = APICallHelper(object, method, "MenuController", '', callback);
    APICall.sendRequest();
}

function loadModifierCategories(){
    var object = 'modifiercategory';
    var method = 'get';
    var callback = function(responseText){
        if(responseText !== false){
            var responseArray = responseText;
            for (var i = 0; i < responseArray.length; i++){
                curItem = responseArray[i];
                var newItem = new menuItem86(curItem['id'], curItem['name'], curItem['description']);  
                modifier_categories[newItem.id] = newItem;
            }
        }
    };
    var APICall = APICallHelper(object, method, "MenuController", '', callback);
    APICall.sendRequest();
}

function loadForcedModifiers(){
     var object = 'forcedmodifier';
    var method = 'get';
    var callback = function(responseText){
        if(responseText !== false){
            var responseArray = responseText;
            for (var i = 0; i < responseArray.length; i++){
                curItem = responseArray[i];
                var newItem = new forced_modifier(curItem['id'], curItem['title'], curItem['list_id']);  
                forced_modifiers[newItem.id] = newItem;
            }
        }
    };
    var APICall = APICallHelper(object, method, "MenuController", '', callback);
    APICall.sendRequest();
}

function loadForcedModifierLists(){
    var object = 'forcedmodifierlist';
    var method = 'get';
    var callback = function(responseText){
        if(responseText !== false){
            var responseArray = responseText;
            for (var i = 0; i < responseArray.length; i++){
                curItem = responseArray[i];
                var newItem = new forced_modifier_list(curItem['id'], curItem['title'], curItem['description']);  
                forced_modifier_lists[newItem.id] = newItem;
            }
        }
    };
    var APICall = APICallHelper(object, method, "MenuController", '', callback);
    APICall.sendRequest();
}

function loadForcedModifierGroups(){
    var object = 'forcedmodifiergroup';
    var method = 'get';
    var callback = function(responseText){
        if(responseText !== false){
            var responseArray = responseText;
            for (var i = 0; i < responseArray.length; i++){
                curItem = responseArray[i];
                var newItem = new forced_modifier_group(curItem['id'], curItem['title'], curItem['include_lists']);  
                forced_modifier_groups[newItem.id] = newItem;
            }
        }
    };
    var APICall = APICallHelper(object, method, "MenuController", '', callback);
    APICall.sendRequest();
}

