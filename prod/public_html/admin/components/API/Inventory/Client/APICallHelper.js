function APICall(object, method, controller, JSONinput, callback){
    this.requestURL = "http://demo.lavu.com/components/API/Inventory/Inventory.php/";
    this.request = new XMLHttpRequest();
    this.object = object;
    this.method = method;
    this.controller = controller;
    this.callback = callback;
    this.JSONInput = JSONinput;
    this.sendRequest = function(){
        var completeURL = this.requestURL + this.controller + "/" + this.object + "/" + this.method;

        this.request.open("POST", completeURL, false);
        this.request.setRequestHeader('Content-Type', 'application/json');
        
        this.request.onreadystatechange = function(){
        if(this.request.readyState === 4 && this.request.status === 200){
            var response = JSON.parse(this.request.responseText);
            this.callback(response);
        }
        else this.callback(false);
        };
    
        
        
        if(this.JSONinput != ''){
            this.request.send(this.JSONinput);
        }
        else this.request.send();
    };
}
