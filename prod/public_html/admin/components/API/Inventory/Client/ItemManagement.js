/*
 * JS to handle the loading and operation of the Inventory Management System
 */

var items = [];
var categories = [];

function item (id, name, categoryID, unitID, quantity, lowQuantity, parQuantity){
    this.id = id;
    this.name = name;
    this.unitID = unitID;
    this.categoryID = categoryID;
    this.quantity = quantity;
    this.lowQuantity = lowQuantity;
    this.parQuantity = parQuantity;
}

function category (id, name, description){
    this.id = id;
    this.name = name;
    this.items = new Array();
    this.description = description;
}

//submit requests to /UnitSystem/unit/get and /UnitSystem/conversion/get
//populate an array of unit objects, each of which
//has an array of units that it can be converted to.
function loadInventoryManagement(){
   loadCategories();
   if(categories.count === 0 || items.count ===0){
       alert("Could not load unit system");
   }
   //calls function to print the units/categories as tables
   //DEBUG ONLY
    //var tables = printUnits();
    //document.write(tables);
}

//Calls the UnitSystem controller to request JSON represenation of units
function loadItems(){
        var object = "item";
        var method = "get";
        var callback = function(responseText){
            itemCallback(responseText);
        };

        var APICall = APICallHelper(object, method, "ItemManagement", '', callback);
        APICall.sendRequest();
}

//calls InventoryManagement controller for JSON represenation of categories
function loadCategories(){
    var object = "category";
    var method = "get";
    var callback = function(responseText){
        if(responseText !== false){
            categoryCallback(responseText);
            loadItems();
        }
    };
    var APICall = APICallHelper(object, method, "ItemManagement", '', callback);
    APICall.sendRequest();
}



function addCategory(JSONinput){
    var object = "category";
    var method = "add";
    var callback = function(responseText){
        categoryCallback(responseText);
    };

    var APICall = APICallHelper(object, method, "ItemManagement", JSONinput, callback);
    APICall.sendRequest();
}

//pushes updated items to database and ensures updates are tracked locally
function updateItems(JSONinput){
    var object = "item";
    var method = "update";
    var callback = function(responseText){
        if(responseText !== false){
            var responseArray = responseText;
            for (var i=0; i < responseArray.length(); i++){
                curItem = responseArray[i];
                if(items[curItem['id']] !== null){
                    items[curItem['id']].name = curItem['name'];
                    items[curItem['id']].categoryID = curItem['categoryID'];
                    items[curItem['id']].unitID = curItem['unitID'];
                    items[curItem['id']].quantity = curItem['quantity'];
                    items[curItem['id']].loqQuantity = curItem['lowQuantity'];
                    items[curItem['id']].parQuantity = curItem['parQuantity'];
                }
            }
        }
    };

    var APICall = APICallHelper(object, method, "ItemManagement", JSONinput, callback);
    APICall.sendRequest();
}

//pushes updated categories to database and ensures changes are tracked locally
function updateCategories(JSONinput){
    var object = "category";
    var method = "update";
    var callback = function(responseText){
        if(responseText !== false){
            var responseArray = responseText;
            for (var i=0; i < responseArray.length(); i++){
                curItem = responseArray[i];
                if(categories[curItem['id']] !== null){
                    categories[curItem['id']].name = curItem['name'];
                    categories[curItem['id']].description = curItem['description'];
                }
            }
        }
    };

    var APICall = APICallHelper(object, method, "ItemManagement", JSONinput, callback);
    APICall.sendRequest();
}

//callback method for item adds/gets
function itemCallback(responseText){
    if(responseText !== false){
        var responseArray = responseText;
        for (var i = 0; i < responseArray.length; i++){
            curItem = responseArray[i];
            var newItem = new item(curItem['id'], curItem['name'], curItem['categoryID'], curItem['unitID'], curItem['quantity'],
            curItem['lowQuantity'], curItem['parQuantity']);
            items[newItem.id] = newItem;
            if(newItem.categoryID in categories){
                categories[newItem.categoryID].items[newItem.id] = newItem;
            }
        }
    }
}

function addItem(JSONinput){
    var object = "item";
    var method = "add";
    var callback = function(responseText){
        itemCallback(responseText);
    };
    var APICall = APICallHelper(object, method, "ItemManagement", JSONinput, callback);
    APICall.sendRequest();
}

//callback method for category adds/gets
function categoryCallback(responseText){
    if(responseText !== false){
            var categoryArray = responseText;
            for (var i = 0; i < categoryArray.length; i++){
                curItem = categoryArray[i];
                var newCategory = new category(curItem['id'], curItem['name'], curItem['description']);
                categories[newCategory.id] = newCategory;
            }
        }
}


//debug method to print out table of units
function printUnits(){
    //print units
    var result = "<table border=1>";
        result += "<tr>";
        result += "<td>id</td><td>name</td><td>quantity</td><td>categoryID</td><td>unitID</td>";
        result += "</tr>";
        var it;
    for(it in items) {
        result += "<tr>";
        result += "<td>"+ items[it].id + "</td><td>"+ items[it].name + "</td><td>"
                + items[it].quantity + "</td><td>"+ items[it].categoryID + "</td><td>"+ items[it].unitID + "</td>";
        result += "</tr>";
    }
    result += "</table>";

    //print Categories
        result += "<table border=1>";
        result += "<tr>";
        result += "<td>id</td><td>name</td><td>description</td>";
        result += "</tr>";
        var cat;
    for(cat in categories) {
        result += "<tr>";
        result += "<td>"+ categories[cat].id + "</td><td>"+ categories[cat].name + "</td><td>"+ categories[cat].description + "</td>";
        result += "</tr>";
    }
    result += "</table>";

    return result;
}