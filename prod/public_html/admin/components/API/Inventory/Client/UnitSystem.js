/*
 * JS to handle the loading and operation of the Unit System
 */
var units = [];
var categories = [];

function unit (id, name, symbol, categoryID, conversion){
    this.id = id;
    this.name = name;
    this.symbol = symbol;
    this.categoryID = categoryID;
    this.conversion = conversion;
}

function category (id, name){
    this.id = id;
    this.name = name;
    this.units = [];
    this.convert = function(toUnitID, value ){
        if(this.units.indexOf(units[toUnitID]) > -1){
            var intermediateVal = value / 1; //convert to base;
            return intermediateVal * units[toUnitID].Conversion;
        }
    };
}

//submit requests to /UnitSystem/unit/get and /UnitSystem/conversion/get
//populate an array of unit objects, each of which
//has an array of units that it can be converted to.
function loadUnitSystem(){
   loadCategories();
   if(categories.count === 0 || units.count ===0){
       alert("Could not load unit system");
   }
   //calls function to print the units/categories as tables
   // DEBUG ONLY
   // var tables = printUnits();
   // document.write(tables);
}

//Calls the UnitSystem controller to request JSON represenation of units
function loadUnits(){
        var object = "unit";
        var method = "get";
        var callback = function(responseText){
            if(responseText !== false){
                unitCallback(responseText);
            }
        };

        var APICall = APICallHelper(object, method, "UnitSystem", '', callback);
        APICall.sendRequest();
}

//calls UnitSystem controller for JSON represenation of categories
function loadCategories(){
    var object = "category";
    var method = "get";
    var callback = function(responseText){
        if(responseText !== false){
            if(categoryCallback(responseText)){
                loadUnits();
            }
        }
    };
    var APICall = APICallHelper(object, method, "UnitSystem", '', callback);
    APICall.sendRequest();
}

function addUnit(JSONinput){
    var object = "unit";
    var method = "add";
    var callback = function(responseText){
       unitCallback(responseText);
    };

    var APICall = APICallHelper(object, method, "UnitSystem", JSONinput, callback);
    APICall.sendRequest();
}

function addCategory(JSONinput){
    var object = "category";
    var method = "add";
    var callback = function(responseText){
        categoryCallback(responseText);
    };

    var APICall = APICallHelper(object, method, "UnitSystem", JSONinput, callback);
    APICall.sendRequest();
}

//callback for unit functions
function unitCallback(responseText){
     var newUnitArray = responseText;
        for (var i = 0; i < newUnitArray.length; i++){
            curItem = newUnitArray[i];
            var curUnit = new unit(curItem['id'], curItem['name'], curItem['symbol'], curItem['categoryID'], curItem['conversion']);
            units[curUnit.id] = curUnit;
            if(curUnit.categoryID in categories){
                categories[curUnit.categoryID].units.push(curUnit);
            }
        }
}

//callback for category functions
function categoryCallback(responseText){
    var newCategoryArray = responseText;
        for (var i = 0; i < newCategoryArray.length; i++){
            curItem = newCategoryArray[i];
            var newCategory = new category(curItem['id'], curItem['name']);
            categories[newCategory.id] = newCategory;
        }
        return true;
}


//debug method to print out table of units
function printUnits(){
    //print units
    var result = "<table border=1>";
        result += "<tr>";
        result += "<td>id</td><td>name</td><td>symbol</td><td>categoryID</td><td>conversion</td>";
        result += "</tr>";
        var it;
    for(it in units) {
        result += "<tr>";
        result += "<td>"+ units[it].id + "</td><td>"+ units[it].name + "</td><td>"
                + units[it].symbol + "</td><td>"+ units[it].categoryID + "</td><td>"+ units[it].conversion + "</td>";
        result += "</tr>";
    }
    result += "</table>";

    //print Categories
        result += "<table border=1>";
        result += "<tr>";
        result += "<td>id</td><td>name</td>";
        result += "</tr>";
        var cat;
    for(cat in categories) {
        result += "<tr>";
        result += "<td>"+ categories[cat].id + "</td><td>"+ categories[cat].name + "</td>";
        result += "</tr>";
    }
    result += "</table>";

    return result;
}