/*
 * Purchase Order Interface
 */


var PurchaseOrders = [];

function PurchaseOrder(id, vendorID, date){
    this.id = id;
    this.vendorID = vendorID;
    this.date = date;
}

function PurchaseOrderItem(id, purchaseOrderID, vendorItemID, unitID, quantityOrdered, quantityReceived, SKU){
    this.id = id;
    this.purchaseOrderID = purchaseOrderID;
    this.vendorID = vendorItemID;
    this.unitID = unitID;
    this.quantityOrdered = quantityOrdered;
    this.quantityReceived = quantityReceived;
    this.SKU = SKU;
}

function loadPurchaseOrders(){
    var object = 'purchaseorder';
    var method = 'get';
    var callback = function(responseText){
        for(var i = 0; i < responseText.length; i++){
            var curPO = responseText[i];
            var newPO = new PurchaseOrder(curPO['id'], curPO['vendorID'], curPO['date']);
            PurchaseOrders[newPO.id] = newPO;
        }
    };

    var APICall = APICallHelper(object, method, "PurchaseOrder", '', callback);
    APICall.sendRequest();
}


function loadPurchaseOrderItems(){
    var object = 'item';
    var method = 'get';
    var callback = function(responseText){
        for(var i = 0; i < responseText.length; i++){
            var curItem = responseText[i];
            var newItem = new PurchaseOrderItem(curItem['id'], curItem['purchaseOrderID'], curItem['vendorID'], curItem['unitID'],
                                          curItem['quantityOrdered'], curItem['quantityReceived'], curItem['SKU']);
            PurchaseOrders[newItem.id] = newItem;
        }
    };

    var APICall = APICallHelper(object, method, "PurchaseOrder", '', callback);
    APICall.sendRequest();
}

/*
 * JSON:
 * vendorID:<value>
 */
function newPurchaseOrder(JSONinput){
    var object = 'purchaseorder';
    var method = 'add';
    var callback = function(responseText){
        for(var i = 0; i < responseText.length; i++){
            var curPO = responseText[i];
            var newPO = new PurchaseOrder(curPO['id'], curPO['vendorID'], curPO['date']);
            PurchaseOrders[newPO.id] = newPO;
        }
    };

    var APICall = APICallHelper(object, method, "PurchaseOrder", JSONinput, callback);
    APICall.sendRequest();
}

/*
 * JSON:
 * purchaseOrderID:<value>
 * vendorItemID:<value>
 * unitID:<value>
 * quantityOrdered: <value>
 * SKU:<value>
 */
function addItemToPurchaseOrder(JSONinput){
    var object = 'item';
    var method = 'add';
    var callback = function(responseText){
        for(var i = 0; i < responseText.length; i++){
            var curItem = responseText[i];
            var newItem = new PurchaseOrderItem(curItem['id'], curItem['purchaseOrderID'], curItem['vendorID'], curItem['unitID'],
                                          curItem['quantityOrdered'], curItem['quantityReceived'], curItem['SKU']);
            PurchaseOrders[newItem.id] = newItem;
        }
    };

    var APICall = APICallHelper(object, method, "PurchaseOrder", JSONinput, callback);
    APICall.sendRequest();
}

/*
 * JSON:
 * id:<value>
 * purchaseOrderID:<value>
 * vendorItemID:<value>
 * unitID:<value>
 * quantityOrdered: <value>
 * quantityReceived:<value>
 * SKU:<value>
 */
function updateItemInPurchaseOrder(JSONinput){
    var object = 'item';
    var method = 'add';
    var callback = function(responseText){
        if(responseText !== true){
            echo(responseText);
        }
    };

    var APICall = APICallHelper(object, method, "PurchaseOrder", JSONinput, callback);
    APICall.sendRequest();
}

/*
 * JSON:
 * id:<value>
 */
function receivePurchaseOrder(JSONItem){
    var object = 'purchaseorder';
    var method = 'receive';
    var callback = function(responseText){
      if(responseText !== true){
          echo(responseText);
      }
    };

    var APICall = APICallHelper(object, method, "PurchaseOrder", JSONItem, callback);
    APICall.sendRequest();
}
