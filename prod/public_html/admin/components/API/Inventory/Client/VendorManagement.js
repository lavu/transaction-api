/*
 * Vender Management javascript interface
 */

var Vendors = [];
var VendorItems = [];

function Vendor(id, name, contactName, contactPhone, contactEmail){
    this.id = id;
    this.name = name;
    this.contactName = contactName;
    this.contactPhone = contactPhone;
    this.contactEmail = contactEmail;
}

function VendorItem(id, inventoryItemID, vendorID, unitID, minOrderQuantity, SKU){
    this.id = id;
    this.inventoryItemID = inventoryItemID;
    this.vendorID = vendorID;
    this.unitID = unitID;
    this.minOrderQuantity = minOrderQuantity;
    this.SKU = SKU;
}

function loadVendors(){
    var object = 'vendor';
    var method = 'get';
    var callback = function(responseText){
      for (var i = 0; i < responseText.length; i++){
          curVendor = responseText[i];
          var newVendor = new Vendor(curVendor['id'], curVendor['name'], curVendor['contactName'], curVendor['contactPhone'], curVendor['contactEmail']);
          Vendors[newVendor.id] = newVendor;
      }
    };

    var apiCall = new APICall(object, method, "Vendor", '', callback);
    apiCall.sendRequest();
}

function loadVendorItems(){
    var object = 'item';
    var method = 'get';
    var callback = function(responseText){
        for (var i = 0; i < responseText.length; i++){
          curVendorItem = responseText[i];
          var newVendorItem = new Vendor(curVendorItem['id'], curVendorItem['inventoryItemID'], curVendorItem['vendorID'],
                                        curVendorItem['unitID'], curVendorItem['minOrderQuantity'], curVendorItem['SKU']);
          VendorItems[newVendorItem.id] = newVendorItem;
      }
    };

    var apiCall = new APICall(object, method, "Vendor", JSONItem, callback);
    apiCall.sendRequest();
}
/*
 * JSON structure:
 * name:<value>
 * contactInfo:<value>
 */
function newVendor(JSONItem){
    var object = 'vendor';
    var method = 'add';
    var callback = function(responseText){
        for (var i = 0; i < responseText.length; i++){
          curVendor = responseText[i];
          var newVendor = new Vendor(curVendor['id'], curVendor['name'], curVendor['contactName'], curVendor['contactPhone'], curVendor['contactEmail']);
          Vendors[newVendor.id] = newVendor;
        }
    };

    var apiCall = new APICall(object, method, "Vendor", JSONItem, callback);
    apiCall.sendRequest();
}

/*
 * JSON structure:
 * inventoryItemID:<value>
 * vendorID:<value>
 * unitID:<value>
 * minOrderQuantity:<value>
 * SKU:<value>
 */
function newVendorItem(JSONItem){
    var object = 'item';
    var method = 'add';
    var callback = function(responseText){
        for (var i = 0; i < responseText.length; i++){
          curVendorItem = responseText[i];
          var newVendorItem = new Vendor(curVendorItem['id'], curVendorItem['inventoryItemID'], curVendorItem['vendorID'],
                                        curVendorItem['unitID'], curVendorItem['minOrderQuantity'], curVendorItem['SKU']);
          VendorItems[newVendorItem.id] = newVendorItem;
      }
    };

   var apiCall = new APICall(object, method, "Vendor", JSONItem, callback);
   apiCall.sendRequest();
}

/*
 * JSON structure:
 * id:<value>
 * inventoryItemID:<value>
 * vendorID:<value>
 * unitID:<value>
 * minOrderQuantity:<value>
 * SKU:<value>
 */
function updateVendorItem(JSONItem){
    var object = 'item';
    var method = 'update';
    var callback = function(responseText){
        if(responseText !== true){
            alert(responseText);
        }
    };

    var apiCall = new APICall(object, method, "Vendor", '', callback);
    apiCall.sendRequest();
}

/*
 * JSON structure:
 * id:<value>
 * name:<value>
 * contactInfo:<value>
 */
function updateVendor(JSONItem){
    var object = 'item';
    var method = 'add';
    var callback = function(responseText){
       if(responseText !== true){
           alert(responseText);
      }
    };

    var apiCall = new APICall(object, method, "Vendor", JSONItem, callback);
    apiCall.sendRequest();
}