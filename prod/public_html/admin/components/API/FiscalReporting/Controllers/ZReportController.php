<?php
/**
 * Created by PhpStorm.
 * User: Chris
 * Date: 1/31/17
 * Time: 11:00 AM
 */
if (!defined('_HELPERS_'))
{
    define('_HELPERS_', dirname(dirname(dirname(__FILE__))));
}

class _ZReportController{
    public function _construct(){

    }
    public function routeCommand($object = '', $method = '', $JSON = '')
    {
        if ($object === '' || $method === '')
        {
            return array("ERROR" => "No valid Command");
        }

        if ($object == 'ZReport')
        {
            switch ($method)
            {
                case 'insert':
                    return $this->insertZReport($JSON);
            }
        }

        return array("ERROR" => "No valid Command");
    }

    private function insertZReport($JSON){
        $ZReport = json_decode($JSON, true);
        $dataname = $ZReport['dataname'];
        $ZReportArray = $ZReport['CerrarJornadaFiscal'];
        error_log(print_r($ZReportArray, true));
        foreach($ZReportArray as $value) {
            $ZReportDetails = $value;
            $register = isset($ZReportDetails['register']) ? $ZReportDetails['register'] : "Unavailable";
            $firstInvoice = isset($ZReportDetails['first']) ? $ZReportDetails['first'] : "Unavailable";
            $lastInvoice = isset($ZReportDetails['last']) ? $ZReportDetails['last'] : "Unavailable";
            lavu_connect_dn($dataname);
            $queryString = "Insert into `fiscal_reports` set " .
                "`register` = '" . $register .
                "',`first_invoice` = '" . $firstInvoice .
                "',`last_invoice` = '" . $lastInvoice .
                "',`fiscal_day` = '" . $ZReportDetails['Fecha'] .
                "',`net_total` = '" . $ZReportDetails['DF_TotalGravado'] .
                "',`untax_total` = '" . $ZReportDetails['DF_TotalNoGravado'] .
                "',`total` = '" . $ZReportDetails['DF_Total'] .
                "',`tax` = '" . $ZReportDetails['DF_TotalIVA'] .
                "',`refund_total` = '" . $ZReportDetails['NC_Total'] .
                "',`refund_net` = '" . $ZReportDetails['NC_TotalGravado'] .
                "',`refund_tax` = '" . $ZReportDetails['NC_TotalIVA'] .
                "',`untax_refund` = '" . $ZReportDetails['NC_TotalNoGravado'] .
                "',`report_id` = '" . $ZReportDetails['Numero'] . "'";

            error_log("query string: " . $queryString);
            lavu_query($queryString);

            error_log(lavu_dberror());
        }
    }
}

