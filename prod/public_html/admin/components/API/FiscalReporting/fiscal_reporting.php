<?php
/**
 * Created by PhpStorm.
 * User: Chris
 * Date: 1/31/17
 * Time: 10:56 AM
 */
require_once(dirname(dirname(__FILE__)).'/Helpers/APIRequest.php');
require_once(dirname(dirname(dirname(dirname(__FILE__)))).'/cp/resources/core_functions.php');
require_once("Controllers/ZReportController.php");
$request = new APIRequest();
$controller;
$controllerInstance;
$object;
$method;

$protocol = (isset($_SERVER['SERVER_PROTOCOL']) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.0');
$responseArray = array();

if(!$request->actionType){
    //if there is nothing submitted to the controller, just return OK
    header("application/JSON");
    $responseArray['response'] = "No Command Received";
    $response = json_encode($responseArray);
    echo $response;
    exit();
}
else if(count($request->actionType) != 3){
    //if the request exists, but is formatted incorrectly, for now return 400
    //bad request
    header("application/JSON");
    $responseArray['response'] = "400 - Bad Request";
    $response = json_encode($responseArray);
    echo $response;
    exit();
}
else{

    $controller = "ZReport";
    $object = "ZReport";
    $method = "insert";
}

switch($controller){
    case 'ZReport':
        $controllerInstance = new _ZReportController();
        break;
    default:
        header("application/JSON");
        $responseArray['response'] = "Problem Loading ". $controller;
        $response = json_encode($responseArray);
        echo $response;
        exit();
}

if($controllerInstance){
    $resultArray = $controllerInstance->routeCommand($object, $method, $request->JSON);
    header("application/JSON");
    if($resultArray === false){
        echo JSON_encode(array('ERROR'=>'Unknown Error'), JSON_PRETTY_PRINT);
    }else if(is_string($resultArray)){
        echo $resultArray;
    }else{
        echo JSON_encode($resultArray, JSON_PRETTY_PRINT);
    }
}