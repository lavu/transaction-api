<?php
/**
 * Created by Code.
 * User: Hayat
 * Date: 25/01/19
 * Time: 12:50 PM
 */

require_once(__DIR__.'/../../../../inc/papi/jwtPapi.php');

use \Firebase\JWT\JWT;

$session_id = reqvar("PHPSESSID", "");
$mode = strtolower(reqvar("mode", ""));
session_start($session_id);

$dataname = reqvar("dataname", "");
$dataname = ($dataname) ? $dataname : admin_info('dataname');
$locationId = reqvar("loc_id", "");
$locationId = ($locationId) ? $locationId : sessvar('locationid');

if ($dataname) {
    $args['dataname'] = $dataname;
    $args['locationid'] = $locationId;
    $jwtPapiObj = new jwtPapi($args);
    if ($mode == 'auth_token') {
        if ($locationid) {
            $authToken = $jwtPapiObj->generatePapiAuthToken();
            $response = '1|'.$authToken;
        } else {
            $response =  '0|Location id is not found.';
        }
    } else if ($mode == 'enc_dataname') {
        if ($dataname) {
            $encDataname =  $jwtPapiObj->genEncryptedDataname();
            $response = '1|'.$encDataname;
        }
    } else if ($mode == 'dec_dataname') {
        if ($dataname) {
            $text = reqvar("text", "");
            $decryptDataname =  $jwtPapiObj->genDecryptDataname($text);
            $response = '1|'.$decryptDataname;
        }
    } else if ($mode == 'offline_transaction') {
        global $data_name;
        $data_name = $dataname;
        require_once(__DIR__."/../../../cp/resources/core_functions.php");
        $transactionData = reqvar("transaction_data", "");
        $transactionArr = json_decode($transactionData, 1);
        $offlineId = $transactionArr['offline_transaction_id'];
        $lavuId = $transactionArr['lavu_id'];
        $db = 'poslavu_'.$data_name.'_db';
        if ($offlineId && $lavuId) {
            $query = "UPDATE `" . $db . "`.`cc_transactions` set `transaction_id` = '[1]', `preauth_id` = '[1]', `ref_data` = '[2]' WHERE  `transaction_id` = '[3]' AND `loc_id` = " . $locationId;
            $result = lavu_query($query, $lavuId, $transactionData, $offlineId);
            // We need order ioid in both condition, so moved query outside of the if else condition
            $transactionId  = ($result) ? $lavuId : $offlineId;
            $query          = "SELECT `order_id`, `check`, `ioid` FROM  `" . $db . "`.`cc_transactions` WHERE `transaction_id` = '[1]' AND `loc_id` = " . $locationId;
            $orderQuery     = lavu_query($query, $transactionId);
            $orderInfo      = mysqli_fetch_assoc($orderQuery);
            if ($result) {
                $response = '1|Transaction id is updated|'.$transactionData.'|'.$orderInfo['ioid'];
            } else {
                $response = '0|Transaction is not updated|'.$transactionData.'|' . $orderInfo['order_id'] . '|' . $orderInfo['check'] . '|' . $orderInfo['ioid'];
            }
        } else {
            $response = '0|Offline transaction id OR Lavu id is not found|'.$transactionData;
        }
    }
} else {
    $response = '0|Dataname is not found.';
}
echo $response;