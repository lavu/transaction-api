<?php

class norwayCashRegister extends norwayMain
{

    protected static $instance = null;

    public $filePathDir = null;
    
    public $urlPathDir = null;

    public $xmlObj = null;

    public $currentFileName = 'SAF-T Cash Register';
    
   

    public $rootTagAttributes = array(
        'xmlns:n1' => 'urn:StandardAuditFile-Taxation-CashRegister:NO',
        'xmlns:xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
        'xsi:schemaLocation' => 'urn:StandardAuditFile-Taxation-CashRegister:NO Norwegian_SAF-T_Cash_Register_Schema_v_1.00.xsd'
    );

    private function __construct()
    {
        $this->filePathDir = self::getFilePathDir();
        if (!file_exists($this->filePathDir)) {
            mkdir($this->filePathDir, 0777, true);
        }
        $this->urlPathDir = self::getUrlPathDir();
    }

    public static function getFilePathDir() {
        $relativePath = DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR  . 'images' .DIRECTORY_SEPARATOR .'norway_fiscal_exports';
        return __DIR__ . $relativePath;
    }

    public static function getUrlPathDir() {
        return (isset($_SERVER['HTTPS']) ? "https" : "http") . "://".$_SERVER['HTTP_HOST']. DIRECTORY_SEPARATOR . 'images' .DIRECTORY_SEPARATOR .'norway_fiscal_exports';
    }

    public static function getInstance()
    {
        if (! isset(self::$instance)) {
            self::$instance = new self();
        }
        
        return self::$instance;
    }

    public function auditFileSender()
    {
        $array = array(
            'companyIdent' => $this->organizationNumber,
            'companyName' => admin_info('company_name'),
            'taxRegistrationCountry' => sessvar('location_info')['country'],
            'taxRegIdent' => $this->organizationNumber . $this->organizationVAT,
            'streetAddress' => $this->streetAddress(),
            'postalAddress' => $this->postalAddress()
        );
        
        return $array;
    }

    public function streetAddress()
    {
        return array(
            'StreetName' => sessvar('location_info')['address'],
            'Number' => (int) sessvar('location_info')['address'],
            'City' => sessvar('location_info')['city'],
            'PostalCode' => sessvar('location_info')['zip'],
            'Region' => sessvar('location_info')['state'],
            'Country' => sessvar('location_info')['country']
        );
    }

    public function postalAddress()
    {
        return array(
            'StreetName' => sessvar('location_info')['address'],
            'Number' => (int) sessvar('location_info')['address'],
            'City' => sessvar('location_info')['city'],
            'PostalCode' => sessvar('location_info')['zip'],
            'Region' => sessvar('location_info')['state'],
            'Country' => sessvar('location_info')['country']
        );
    }

    public function generateXMLFile($fiscalYear, $timePeriodInMonths, $startDate = false, $endDate =false)
    {
        try {
            
            if (isset($timePeriodInMonths) && $timePeriodInMonths != '') {
                $this->initializeInputData($this->currentFileName, $fiscalYear, $timePeriodInMonths);
            } else {
                $this->initializeInputData($this->currentFileName, $fiscalYear, '', $startDate, $endDate);
            }
           
            $this->createXmlFile();
            // $this->auditfile();
            
            /*
             * $this->xmlObj = new SimpleXMLElement('<auditfile/>');
             * if (! empty($this->rootTagAttributes)) {
             * foreach ($this->rootTagAttributes as $key => $val) {
             * $this->xmlObj->addAttribute($key, $val);
             * }
             * }
             * norwayXml::array2XML($this->xmlObj, $this->xmlArray);
             * echo $this->xmlObj->asXML();
             */
            // $xml->saveXML($this->filePathDir . DIRECTORY_SEPARATOR . $this->currentFileName);
            
            // new code to save xml file
            // $xmlRequest = norwayXml::parse($this->xmlArray);
            // $params = new SoapVar($xmlRequest, XSD_ANYXML);
            // header("Content-Type:text/xml");
            // echo $xmlRequest;
            
            // end
        } catch (Exception $e) {
            print_r($e->getMessage());
        }
    }

    public function auditFile()
    {
        $this->xmlArray['auditfile'] = array(
            $this->header(),
            $this->company()
        );
    }

    public function header()
    {
        return $array['header'] = array(
            'fiscalYear' => $this->fiscalYear,
            'startDate' => $this->startDate,
            'endDate' => $this->endDate,
            'curCode' => sessvar('location_info')['primary_currency_code'],
            'dateCreated' => $this->currentDate,
            'timeCreated' => $this->currentTime,
            'softwareDesc' => 'POSLavu Admin Control Panel', // pending
            'softwareVersion' => admin_info('version'),
            'softwareCompanyName' => 'Lavu Inc', // pending
            'auditFileVersion' => '1.0', // pending
            'headerComment' => '', // pending
            'userID' => admin_info('loggedin'),
            'auditfileSender' => $this->auditFileSender()
        );
    }

    public function company()
    {
        return $array['company'] = array(
            'companyIdent' => $this->organizationNumber,
            'companyName' => admin_info('company_name'),
            'taxRegistrationCountry' => sessvar('location_info')['country'],
            'taxRegIdent' => $this->organizationNumber . $this->organizationVAT,
            'streetAddress' => $this->streetAddress(),
            'postalAddress' => $this->postalAddress(),
            'vatCodeDetails' => $this->vatCodeDetails(),
            'periods' => $this->periods(),
            'employees' => $this->employees(),
            'articles' => $this->articles(),
            'basics' => $this->basics(),
            'location' => $this->location()
        );
    }

    public function vatCodeDetails()
    {
        $standardVatCodes = array(
            '0' => '0',
            '< 0.10' => '33',
            '< 0.15' => '31',
            '< 0.25' => '3'
        );
        
        // $query = lavu_query('SELECT * FROM tax_rates');
    }

    public function periods()
    {
        return array(
            'period' => array(
                'periodNumber' => $this->timePeriodInMonths,
                'periodDesc' => 'Last ' . $this->timePeriodInMonths . ' months data',
                'startDatePeriod' => $this->startDate,
                'startTimePeriod' => $this->startDayTime,
                'endDatePeriod' => $this->endDate,
                'endTimePeriod' => $this->endDayTime
            )
        );
    }

    public function employees()
    {}

    public function articles()
    {}

    public function basics()
    {}

    public function location()
    {
        
        // pick the cash registers between the selected period
        $cashRegistersQuery = lavu_query(
            "SELECT o.last_mod_register_name, o.order_id,o.total,o.subtotal,IF(cct.action = 'Sale', 'C', 'D') as amounttype,o.closed FROM orders o LEFT JOIN cc_transactions cct ON cct.order_id = o.order_id 
                    WHERE o.order_status IN ('closed','reclosed') AND o.closed BETWEEN '" . $this->startDate . " " . $this->startDayTime . "' AND '" . $this->endDate .
                 " " . $this->endDayTime . "'  AND o.location_id=" . sessvar('location_info')['id'] . " ORDER BY o.last_mod_register_name,o.closed ASC ");
        
        $data = array();
        
        if (mysqli_num_rows($cashRegistersQuery) > 0) {
            while ($info = mysqli_fetch_assoc($cashRegistersQuery)) {
                // $data[] = $info;
                // array_push($array, array(
                // 'cashregister' => array(
                // 'registerID' => $info['register_name']
                // )
                // ));
                // $data['cashregister']['cashtransaction'] = $info;
                
                array_push($data, array(
                    'cashtransaction' => $info
                ));
                $cashtransactions[] = $info;
                $registers[] = $info['last_mod_register_name'];
            }
        }
        
        $registers = array_unique($registers);
        $mainregisters = array();
        foreach ($registers as $reg) {
            
            array_push($mainregisters, array(
                'cashregister' => array(
                    'registerID' => $reg
                )
            ));
            
            // array_push($mainregisters['cashregister']['registerID'], array('cashtransaction' => array('wertest','hashjdg')));
            
            // $mainregisters['cashregister'] = $registerTransactions;
        }
        
        // print_r($mainregisters);die;
        
        return $mainregisters;
        // print_r($registers);die;
        // $newData1 = array();
        // foreach($registers as $reg) {
        // $newData1['cashregister'] = array();
        // foreach($cashtransactions as $one) {
        // //print_r($one); echo $reg; die;
        
        // if ($reg == $one['last_mod_register_name']) {
        // array_push($newData1[$reg], array('cashtransaction' => $one));
        // //$newData1[$reg]['cashtransaction'] = $one['cashtransaction'];
        // }
        // }
        // }
        
        // print_r($registers);
        // $newData = array();
        // foreach($data as $one) {
        // //$newData['cashregister'] = array('registerID' => $one['cashtransaction']['last_mod_register_name']);
        
        // array_push($newData, array('cashregister' => array('registerID' => $one['cashtransaction']['last_mod_register_name'], 'cashtransaction' => $one['cashtransaction'])));
        // }
        
        echo '<pre>';
        print_r($newData1);
        die('here');
        
        return $newData1;
    }

    public function buildCashRegister($cashRegisterName)
    {}
}