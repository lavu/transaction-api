<?php

class norwayMain
{

    public $fiscalYear;

    public $timePeriodInMonths;

    public $startDate;

    public $startDayTime;

    public $endDate;

    public $endDayTime;

    public $currentDate;

    public $currentTime;

    public $xmlString = '';

    public $xmlArray = array();

    public $organizationNumber = null;

    public $organizationVAT = null;

    public $currentFileName = null;

    public function initializeInputData($fileName, $fiscalYear, $timePeriodInMonths, $startDate=false, $endDate=false)
    {
        $this->currentFileName = $fileName;
        $this->fiscalYear = $fiscalYear;
        $this->timePeriodInMonths = $timePeriodInMonths;
        
        // get first day of the back dated timePeriodInMonths
        if(!$startDate){
            $this->startDate = date('Y-m-' . '01', strtotime('-' . $timePeriodInMonths . ' month'));
        } else {
            $this->startDate = $startDate;
        }
        //$this->startDate = '2017-03-15'; // for testing
        $this->startDayTime = '00:00:00';
        
        // lastday of the previous month
        if(!$endDate){
            // lastday of the previous month
            $date = new DateTime();
            $date->modify("last day of previous month");
            $this->endDate = $date->format("Y-m-d");
            
        } else {
            $this->endDate = $endDate;
        }
        
        $this->endDayTime = '23:59:59';
        
        $this->currentDate = date('Y-m-d');
        $this->currentTime = date('H:i:s');
        
        // get organization number
        $norwayConfigQuery = lavu_query("SELECT `value` FROM `config` WHERE `setting` = 'norway_vat_id'");
        $norwayConfigValues = mysqli_fetch_assoc($norwayConfigQuery);

        if($norwayConfigValues['value'] != "")
        {
            $this->organizationNumber = (int) str_replace(" ", "", $norwayConfigValues['value']);
            $this->organizationVAT = strtoupper(str_replace($this->organizationNumber, "", str_replace(" ", "", $norwayConfigValues['value'])));
        } else {
            $orgDataArr = explode('Org. nr.', admin_info('company_name'));
            $this->organizationNumber = (int) str_replace(" ", "", $orgDataArr[1]);
            $this->organizationVAT = strtoupper(str_replace($this->organizationNumber, "", str_replace(" ", "", $orgDataArr[1])));
        }
    }

    public function createXmlFile()
    {
        $this->currentFileName .= '_' . $this->organizationNumber . '_' . date('YmdHis') . '.xml';
    }

    public function streetAddress()
    {
        $address = array(
            'StreetName' => sessvar('location_info')['address'],
            'Number' => (int) sessvar('location_info')['address'],
            'City' => sessvar('location_info')['city'],
            'PostalCode' => sessvar('location_info')['zip'],
            'Region' => sessvar('location_info')['state'],
            'Country' => sessvar('location_info')['country'],
            'AddressType' => 'StreetAddress'
        );
        return $address;
    }

    public function postalAddress()
    {
        $address = array(
            'StreetName' => sessvar('location_info')['address'],
            'Number' => (int) sessvar('location_info')['address'],
            'City' => sessvar('location_info')['city'],
            'PostalCode' => sessvar('location_info')['zip'],
            'Region' => sessvar('location_info')['state'],
            'Country' => sessvar('location_info')['country'],
            'AddressType' => 'PostalAddress'
        );
        return $address;
    }
}