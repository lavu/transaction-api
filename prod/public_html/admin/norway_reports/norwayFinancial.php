<?php

class norwayFinancial extends norwayMain
{

    protected static $instance = null;

    public $filePathDir = null;

    public $currentFileName = 'SAF-T Financial';

    public $rootTagAttributes = array(
        'xmlns:n1' => 'urn:StandardAuditFile-Taxation-Financial:NO',
        'xmlns:xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
        'xsi:schemaLocation' => 'urn:StandardAuditFile-Taxation-Financial:NO Norwegian_SAF-T_Financial_Schema_v_1.00.xsd'
    );

    private function __construct()
    {
        $this->filePathDir = __DIR__ . DIRECTORY_SEPARATOR . 'exports';
    }

    public static function getInstance()
    {
        if (! isset(self::$instance)) {
            self::$instance = new self();
        }
        
        return self::$instance;
    }

    public function header()
    {
        $this->xmlArray['header'] = array(
            'AuditFileVersion' => '1.0', // pending
            'AuditFileCountry' => sessvar('location_info')['country'],
            'AuditFileDateCreated' => date('Y-m-d'),
            'SoftwareCompanyName' => 'Lavu Inc', // pending
            'SoftwareID' => 'POSLavu', // pending
            'SoftwareVersion' => admin_info('version'),
            'Company' => array(
                'RegistrationNumber' => $this->organizationNumber,
                'Name' => sessvar('location_info')['title'],
                'Address' => $this->streetAddress(),
                'Address' => $this->postalAddress(),
                'Contact' => array(
                    'ContactPerson' => array(
                        'FirstName' => '',
                        'Initials' => '',
                        'LastName' => '',
                        'Salutation' => '',
                        'OtherTitles' => ''
                    ),
                    'Telephone' => '',
                    'Fax' => '',
                    'Email' => '',
                    'Website' => '',
                    'MobilePhone' => ''
                ),
                'TaxRegistration' => array(
                    'TaxRegistrationNumber' => $this->organizationNumber . $this->organizationVAT,
                    'TaxAuthority' => 'Skatteetaten', // hardcoded as per documentation
                    'TaxVerificationDate' => '' // pending
                ),
                'BankAccount' => array(
                    'BankAccountNumber' => '', // pending
                    'BIC' => '', // pending
                    'CurrencyCode' => '', // pending
                    'GeneralLedgerAccountID' => '' // pending
                )
            ),
            'DefaultCurrencyCode' => sessvar('location_info')['primary_currency_code'],
            'SelectionCriteria' => array(
                'PeriodStart' => date("m", strtotime($this->startDate)),
                'PeriodStartYear' => date("Y", strtotime($this->startDate)),
                'PeriodEnd' => date("m", strtotime($this->endDate)),
                'PeriodEndYear' => date("Y", strtotime($this->endDate))
            ),
            'HeaderComment' => '', // pending
            'TaxAccountingBasis' => 'A', // pending
            'UserID' => admin_info('loggedin')
        );
    }

    public function generateXML($fiscalYear, $timePeriodInMonths)
    {
        try {
            $this->initializeInputData($this->currentFileName, $fiscalYear, $timePeriodInMonths);
            $this->createXmlFile();
            $this->header();
            $this->company();
            $xml = new SimpleXMLElement('<auditfile/>');
            if (! empty($this->rootTagAttributes)) {
                foreach ($this->rootTagAttributes as $key => $val) {
                    $xml->addAttribute($key, $val);
                }
            }
            norwayXml::array2XML($xml, $this->xmlArray);
            $xml->asXML();
            $xml->saveXML($this->filePathDir . DIRECTORY_SEPARATOR . $this->currentFileName);
        } catch (Exception $e) {
            print_r($e->getMessage());
        }
    }
}