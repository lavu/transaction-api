<?php
	// session_start();
	// if(isset($_GET['admin_area'])) $_SESSION['admin_area'] = "old";
	// if(isset($_SESSION['admin_area'])) $admin_area = $_SESSION['admin_area'];
	// else $admin_area = "new";

	// putenv("TZ=America/Chicago");

	// if($admin_area!="old"){
	// 	header("Location: http://".$_SERVER['HTTP_HOST']."/cp/");
	// 	exit();
	// }

	header("Location: https://".$_SERVER['HTTP_HOST']."/cp/");
	exit();

	if(strpos($_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'],"poslavu.com/admin")!==false) {
		header("Location: https://admin.poslavu.com/");
	}
	if ((isset($_SESSION['company_code'])) && ($_SESSION['login_status'] != 1) && ($_SESSION['login_status'] != 2)) {
		header("Location: menu.php");
	}
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>POSLavu</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link rel="stylesheet" href="style.css" type="text/css" />
		<script language='javascript' src='lib/js_functions.js'></script>
		<script language='javascript' src='lib/login.js'></script>
	</head>

	<body class='main'>
		<center>
			<div id='root'>
				<table cellspacing='0' cellpadding='0' width='964px'>
					<tr><td align='center' style='padding:2px 0px 5px 0px'><img src='images/poslavu_logo_sm.png'></td></tr>
					<tr><td class='main_panel_top'>&nbsp;</td></tr>
					<tr>
						<td class='main_panel_mid' align='center'>
							<table cellspacing='0' cellpadding='0' width='964px'>
								<!--<tr><td align='center' style='padding:30px 0px 40px 0px'><img src='images/poslavu_logo.png'></td></tr>-->
								<tr>
									<td align='center' style='padding:0px 0px 60px 0px'>
										&nbsp;<br>
										<form id='login_form' name='login_form' method='post' action='menu.php'>
											<input type='hidden' name='cc' value='NEEDTOGET'>
											<table cellspacing='7' cellpadding='0'>
												<tr>
													<td align='right' style='padding:0px 15px 0px 0px'><img src='images/username.png'></td>
													<td class='login_input' align='left'><input type='text' id='username_field' name='username' size='22' style='border:0px solid #FFFFFF; font-size:15px'></td>
												</tr>
												<tr>
													<td align='right' style='padding:0px 17px 0px 0px'><img src='images/password.png'></td>
													<td class='login_input' align='left'><input type='password' name='password' size='22' style='border:0px solid #FFFFFF; font-size:15px'></td>
												</tr>
												<tr>
													<td colspan='2' style='padding:12px 0px 0px 0px'>
														<table cellspacing='0' cellpadding='0' width='100%'>
															<tr>
																<td align='left' style='padding:0px 0px 0px 10px'><!--<a href=''><img src='images/register_btn.png' border='0'></a>-->&nbsp;</td>
																<td align='right' style='padding:0px 11px 0px 0px'><a onclick='if (validateLogin(login_form)) { document.login_form.submit(); }' style='cursor:pointer'><img src='images/submit_btn.png' border='0'></a></td>
															</tr>
														</table>
													</td>
												</tr>
											</table>
										</form>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr><td class='main_panel_bottom'>&nbsp;</td></tr>
				</table>
			</div>
		</center>
		<?php
			if (isset($_SESSION['login_status'])) {
				if ($_SESSION['login_status'] == 1) {
					echo "<script language='javascript'>alert(\"Invalid username or password. Please try again.\");</script>";
					$_SESSION['login_status'] = 0;
				} else if ($_SESSION['login_status'] == 2) {
					echo "<script language='javascript'>alert(\"User has insufficient access level for back end functions.\");</script>";
					$_SESSION['login_status'] = 0;
				}
				session_destroy();
			}
		?>
		<script language='javascript'>document.getElementById('login_form').username.focus();</script>
	</body>
</html>