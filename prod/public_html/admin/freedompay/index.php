<?php
/**
 * Script to generate the iFrame from freedompay so that the POS system can
 * display the page and allow for manual entry. Requires the client to send
 * the appropriate params via a POST request.
 */

/**
 * Retrieve Iframe String from freedompay
 */
function retrieveIFrame($esKey, $storeId, $terminalId, $hostname)
{
    $curl = curl_init();
    curl_setopt_array($curl, array(
                              CURLOPT_URL            => "https://$hostname/api/v1.3/controls/init?=",
                              CURLOPT_RETURNTRANSFER => true,
                              CURLOPT_ENCODING       => "",
                              CURLOPT_MAXREDIRS      => 10,
                              CURLOPT_TIMEOUT        => 30,
                              CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
                              CURLOPT_CUSTOMREQUEST  => "POST",
                              CURLOPT_POSTFIELDS     => "EsKey=$esKey&StoreId=$storeId&TerminalId=$terminalId",
                              CURLOPT_HTTPHEADER     => array(
                                                         "Content-Type: application/x-www-form-urlencoded",
                                                         "cache-control: no-cache",
                                                        ),
                             ));

    $response = curl_exec($curl);
    $err      = curl_error($curl);
    curl_close($curl);
    return array(
            'response' => $response,
            'error'    => $err,
           );
}

/**
 * Script Start
 */
$errors     = array();
$storeId    = $_REQUEST['storeId'];
$terminalId = $_REQUEST['terminalId'];
$hostname   = getenv('FREEDOMPAY_IFRAME_HOSTNAME');
$esKey      = getenv('FREEDOMPAY_ESKEY');

if (empty($esKey)) {
    $errors[] = 'esKey has not been set as an envar';
}
if (!isset($storeId)) {
    $errors[] = 'storeId was not provided';
}
if (!isset($terminalId)) {
    $errors[] = 'terminalId was not provided';
}

if (empty($hostname)) {
    $hostname = 'hpc.uat.freedompay.com';
}

$htmlResponse   = 'Iframe not retrieved<br />';
$sessionKey     = '';
$matches        = array();
$iFrameResponse = retrieveIFrame($esKey, $storeId, $terminalId, $hostname);
if ($iFrameResponse['response'] && empty($errors)) {
    $htmlResponse = $iFrameResponse['response'];
    preg_match('/sessionKey=(.*)("|&)/', $htmlResponse, $matches);
    $sessionKey = isset($matches[1]) ? $matches[1] : 'No session key found';
} else {
    foreach ($errors as $e) {
        $htmlResponse .= $e . '<br />';
    }
}
?>
<!DOCTYPE html>
<html lang='en'>
<head>
    <style>
        html, body, iframe {
            height: 800px;
        }
    </style>
</head>
<body>
<?php echo $htmlResponse ?>
<script language="JavaScript">
  window.addEventListener('message', function (event) {
    if (event.data.data && event.data.data.paymentKeys) {
      var data = event.data.data;
      var token = data.paymentKeys[0];
      var attributes = data.attributes;
      var maskedCardNumber = '';
      var cardIssuer = '';
      attributes.forEach(function(obj) {
        if (obj['Key'] === 'CardIssuer') {
          cardIssuer = obj['Value'];
        }
        if (obj['Key'] === 'MaskedCardNumber') {
          maskedCardNumber = obj['Value'];
        }
      });
      window.location = 'lavu://?token=' + token + '&maskedCardNumber=' + maskedCardNumber + '&cardIssuer=' + cardIssuer + "&sessionKey=<?php echo $sessionKey?>";
    }
  });
</script>
</body>
</html>
