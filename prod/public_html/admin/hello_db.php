<?php // IMPORTANT: Do not delete!

require_once(__DIR__."/cp/resources/lavuquery.php");

function array_to_xml($data, &$xml_data) {

	foreach ($data as $key => $value) {

		if (is_array($value)) {

			if (is_numeric($key)) {
				$key = 'item'.$key;
			}

			$subnode = $xml_data->addChild($key);
			array_to_xml($value, $subnode);

		} else {

			$xml_data->addChild("$key",htmlspecialchars("$value"));
		}
	}
}

function performDataBaseCheck() {

	$conn = ConnectionHub::getConn('poslavu');

	if (!$conn) {
		logError("mysql resource not available");
		return "NO_DB";
	}

	$check_for_table = "restaurants";
	$check_for_field = "data_name";
	// table and field may vary depending on the platform being hit
	
	$table_result = $conn->query('read', "SHOW TABLES LIKE '".$check_for_table."'", false);
	if (!$table_result) {
		logError($conn->error());
		return "INVALID_TABLE_RESULT";
	}
	
	if (mysqli_num_rows($table_result) <= 0) {
		logError("`".$check_for_table."` table not found");
		return "TABLE_NOT_FOUND";
	}
	
	$field_result = $conn->query('read',"SELECT `".$check_for_field."` FROM `".$check_for_table."` LIMIT 1");
	if (!$field_result) {
		logError($conn->error());
		return "INVALID_FIELD_RESULT";
	}		
	$conn->closeDB();
	return "OK";
}

function logError($error) {
	
	error_log("hello_db failed - ".$error);		
}

// Do not instrument this high throughput transaction that is skewing the application average response time
if (extension_loaded('newrelic') && function_exists('newrelic_ignore_transaction')) { // Ensure PHP agent is available
    newrelic_ignore_transaction();
}

$start_ts = microtime(true);
$status = performDataBaseCheck();
$end_ts = microtime(true);

$response = array(
	'status'		=> $status,
	'response_time'	=> round((($end_ts - $start_ts) * 1000), 3),
);

$xml_data = new SimpleXMLElement("<pingdom_http_custom_check/>");

array_to_xml($response, $xml_data);

echo $xml_data->asXML();
?>
