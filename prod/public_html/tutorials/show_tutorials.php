<?php
	$pt = "/subdomains/webv1/tutorials/";
	if(isset($in_lavu) && $in_lavu)
	{
	}
	else
	{
		require_once("/home/poslavu/public_html/admin/cp/resources/lavuquery.php");
		$maindb = "poslavu_MAIN_db";
	}
		
	function item_type($videoid)
	{
		global $pt;
		if(strpos($videoid,"."))
		{
			if(strpos(strtolower($videoid),".pdf"))
				return "PDF";
			else
				return "Document";
		}
		else
			return "YouTube";
	}
	
	function button_img($videoid)
	{
		global $pt;
		$type = item_type($videoid);
		if($type=="PDF")
			$path = $pt."images/t_pdf.png";
		else if($type=="Document")
			$path = $pt."images/t_doc.png";
		else
			$path = "https://i4.ytimg.com/vi/" . $videoid . "/default.jpg";
		return "<img src=\"$path\" width=\"70\" height=\"50\" />";
	}

	function side_button($title,$detail,$videoid,$categoryid,$itemid)
	{
		global $pt;
		$detail = item_type($videoid);
		return "<table id=\"button_".$categoryid."_".$itemid."\" width=\"210\" height=\"60\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" background=\"".$pt."images/tab_bg.png\" style=\"cursor:pointer\" onclick=\"open_video(\'$videoid\',$categoryid,$itemid)\"><tr><td width=\"10\" height=\"5\"></td><td height=\"5\"></td><td width=\"5\" height=\"5\"></td></tr><tr><td width=\"10\"></td><td align=\"left\" valign=\"top\"><table width=\"195\" height=\"50\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr><td height=\"30\" align=\"left\" valign=\"top\" class=\"vidtitle\">" . $title . "</td><td width=\"70\" height=\"50\" rowspan=\"3\" align=\"left\" valign=\"bottom\" bgcolor=\"#333333\">" . button_img($videoid) . "</td></tr><tr><td height=\"20\" align=\"left\" valign=\"top\" class=\"vidtime\">" . $detail . "</td></tr></table></td><td width=\"5\"></td></tr><tr><td width=\"10\" height=\"5\"></td><td height=\"5\"></td><td width=\"5\" height=\"5\"></td></tr></table>";
	}
	
	function show_tutorials() { 
		global $pt;
?>
<style type="text/css">
<!--
.tutelink {	font-family:  Arial, Helvetica, sans-serif;
	font-size: 11px;
	
	color: #8e8e8e;
}
.tutetitle {	font-family:  "Trebuchet MS", Arial, Helvetica, sans-serif;
	font-size: 15px;
	color: #000000;
	font-weight: 500;
}
.vidtime {color: #cdcdcd;
	font-family:  Arial, Helvetica, sans-serif;
	font-size: 11px;
	font-weight: bold;
}
.vidtitle {color: #fff;
	font-family:  Arial, Helvetica, sans-serif;
	font-size: 11px;
	font-weight: bold;
}
.maintitle {color: #8c8c8c;
	font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
}
a {
	color:#666;
}

a:hover {
	color:#8BA036;
	}
-->
</style>
<?php
	
	$categories = array();
	$video_list = array();
	$cat_query = mlavu_query("select * from `poslavu_MAIN_db`.`tutorial_categories` order by _order asc");
	while($cat_read = mysqli_fetch_assoc($cat_query))
	{
		$categories[] = array($cat_read['id'],$cat_read['title']);
		$item_query = mlavu_query("select * from `poslavu_MAIN_db`.`tutorials` where `category`='[1]' order by _order asc",$cat_read['id']);
		while($item_read = mysqli_fetch_assoc($item_query))
		{
			$set_path = $item_read['path'];
			if(substr(strtolower($set_path),0,10)=="tutorials/")
				$set_path = $pt . substr($set_path,10);
			$video_list[] = array($cat_read['id'],$item_read['title'],$set_path,$item_read['id']);
		}
	}
	/*$categories = array();
	$categories[] = array("frontend","Front End");
	$categories[] = array("backend","Back End");
	$categories[] = array("setup","Set Up");
	$categories[] = array("overview","General Overview");
	
	$video_list = array();
	$video_list[] = array("frontend","Quick Serve","laCuFmP7k4U");
	$video_list[] = array("frontend","Table Layout","t5W6UCKWnAQ");
	$video_list[] = array("frontend","Tab Layout","_NT6uAthLLc");
	$video_list[] = array("backend","Additional Backend Settings","sKGwC4xwraw");
	$video_list[] = array("backend","Setting up Modifiers","hVImPtEyTVE");
	$video_list[] = array("setup","Airport Configration for Printng","1lPf9uuc9ts");
	$video_list[] = array("setup","Test Document","docs/testdoc.htm");
	$video_list[] = array("overview","Modifiers","dPyUA5noo-I");
	$video_list[] = array("overview","Introduction","zzHUsYS9Xro");*/
	
	$use_default = 0;
	$def_query = mlavu_query("select * from `poslavu_MAIN_db`.`tutorials` where `category`='0' and `title`='default'");
	if(mysqli_num_rows($def_query))
	{
		$def_read = mysqli_fetch_assoc($def_query);
		$use_default = $def_read['path'];
	}
	
	$def_cmd = "";
	$ctitles = "";
	$clinks = "";
	$cscript = "cbuttons = new Array(); ";
	$cbuttons = array();
	for($i=0; $i<count($categories); $i++)
	{
		$ctitles .= "<td align=\"left\" valign=\"top\" class=\"tutetitle\">".$categories[$i][1]."</td>";
		$clinks .= "<td width=\"215\" align=\"left\" valign=\"top\">";
		$clinks .= "<table>";
		$button_count = 0;
		$buttons = "";
		for($n=0; $n<count($video_list); $n++)
		{
			if($video_list[$n][0]==$categories[$i][0])
			{
				$vid_cmd = "open_video(\"".$video_list[$n][2]."\",\"$i\",\"$n\")";
				$clinks .= "<tr><td style=\"padding-left:0px; padding-right:6px;\" class=\"tutelink\" id=\"link_".$i."_".$n."\"><a style='cursor:pointer' onclick='$vid_cmd'>" . $video_list[$n][1] . "</a></td></tr>";
				$buttons .= "<tr><td>";
				if($use_default==$video_list[$n][3]) $def_cmd = $vid_cmd;
				$buttons .= side_button($video_list[$n][1],"Video",$video_list[$n][2],$i,$n);
				$buttons .= "</td></tr>";
				$button_count++;
			}
		}
		$cbuttons[$i] = $buttons;
		$cscript .= "cbuttons[$i] = '$buttons'; ";
		$clinks .= "</table>";
		$clinks .= "</td>";
	}
	
	$cscript .= "\n";
	$cscript .= "function start_default_video() { ";
	$cscript .= $def_cmd;
	$cscript .= "} ";
?>
<script language="javascript">
	<?php echo $cscript; ?>
	
	function open_video(videoid,categoryid,itemid)
	{
		if(videoid.indexOf(".") > 0)
		{
			sethtml = "<iframe width=\"640\" height=\"385\" src=\"" + videoid + "\"></iframe>";
			sidevis = "visible";
		}
		else
		{
			sethtml = "<object width=\"640\" height=\"385\"><param name=\"movie\" value=\"https://www.youtube.com/v/" + videoid + "\" /></param><param name=\"allowFullScreen\" value=\"true\" /></param><param name=\"allowscriptaccess\" value=\"always\" /></param><embed src=\"https://www.youtube.com/v/" + videoid + "\" type=\"application/x-shockwave-flash\" allowscriptaccess=\"always\" allowfullscreen=\"true\" width=\"640\" height=\"385\"></embed></object>";
			sidevis = "visible";
		}
		document.getElementById("video_player").innerHTML = sethtml;
		document.getElementById("side_area").style.visibility = sidevis;
		document.getElementById("side_area").innerHTML = "<table>" + cbuttons[categoryid] + "</table>";
		highlight_selection(categoryid,itemid);
	}
	function add_side_buttons()
	{
		str = "";
		document.getElementById("side_area").innerHTML = str;
	}
	last_selected = "";
	function highlight_selection(cid,iid)
	{
		sid = cid + "_" + iid;
		if(last_selected!="")
		{
			document.getElementById("link_" + last_selected).style.backgroundColor = "#ffffff";
			document.getElementById("link_" + last_selected).style.color = "#8e8e8e";
		}
		document.getElementById("button_" + sid).style.background = "URL(<?php echo $pt?>images/tab_current.png)";
		document.getElementById("link_" + sid).style.backgroundColor = "#cccccc";
		document.getElementById("link_" + sid).style.color = "#ffffff";
		last_selected = sid;
	}
</script>

<body onLoad="start_default_video()">
<table width="884" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td height="400" align="center" valign="top">
    <table width="884" height="410" border="0" cellspacing="0" cellpadding="0" background="<?php echo $pt?>images/tutebox.png" >
      <tr>
        <td width="10" height="12"></td>
        <td height="12"></td>
        <td width="10" height="12"></td>
      </tr>
      <tr>
        <td width="10"></td>
        <td align="left" valign="top"><table width="860" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="642" height="387" align="center" valign="middle" id="video_player" bgcolor="#000000">&nbsp;</td>
            <td width="10"></td>
            <td width="212" align="center" valign="top" style="visibility:hidden" id="side_area">
            
            <!--start side area-->
            <table width="210" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="center" valign="top" class="maintitle">Overview</td>
              </tr>
              <tr>
                <td><table width="210" border="0" cellspacing="1" cellpadding="0">
                  <tr>
                    <td><table width="210" height="60" border="0" cellspacing="0" cellpadding="0" background="<?php echo $pt?>images/tab_bg.png">
                      <tr>
                        <td width="10" height="5"></td>
                        <td height="5"></td>
                        <td width="5" height="5"></td>
                      </tr>
                      <tr>
                        <td width="10"></td>
                        <td align="left" valign="top"><table width="195" height="50" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td height="20" align="left" valign="bottom" class="vidtitle">Title</td>
                            <td width="70" height="50" rowspan="3" align="left" valign="bottom" bgcolor="#333333"></td>
                          </tr>
                          <tr>
                            <td height="10"></td>
                          </tr>
                          <tr>
                            <td height="20" align="left" valign="top" class="vidtime">1:34</td>
                          </tr>
                        </table></td>
                        <td width="5"></td>
                      </tr>
                      <tr>
                        <td width="10" height="5"></td>
                        <td height="5"></td>
                        <td width="5" height="5"></td>
                      </tr>
                    </table></td>
                  </tr>
                  
                  <tr>
                    <td><table width="210" height="60" border="0" cellspacing="0" cellpadding="0" background="<?php echo $pt?>images/tab_bg.png">
                      <tr>
                        <td width="10" height="5"></td>
                        <td height="5"></td>
                        <td width="5" height="5"></td>
                      </tr>
                      <tr>
                        <td width="10"></td>
                        <td align="left" valign="top"><table width="195" height="50" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td height="20" align="left" valign="bottom" class="vidtitle">Title</td>
                            <td width="70" height="50" rowspan="3" align="left" valign="bottom" bgcolor="#333333"></td>
                          </tr>
                          <tr>
                            <td height="10"></td>
                          </tr>
                          <tr>
                            <td height="20" align="left" valign="top" class="vidtime">1:34</td>
                          </tr>
                        </table></td>
                        <td width="5"></td>
                      </tr>
                      <tr>
                        <td width="10" height="5"></td>
                        <td height="5"></td>
                        <td width="5" height="5"></td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td>
                    
                    <table width="210" height="60" border="0" cellspacing="0" cellpadding="0" background="<?php echo $pt?>images/tab_bg.png">
                      <tr>
                        <td width="10" height="5"></td>
                        <td height="5"></td>
                        <td width="5" height="5"></td>
                      </tr>
                      <tr>
                        <td width="10"></td>
                        <td align="left" valign="top"><table width="195" height="50" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td height="20" align="left" valign="bottom" class="vidtitle">Title</td>
                            <td width="70" height="50" rowspan="3" align="left" valign="bottom" bgcolor="#333333"></td>
                          </tr>
                          <tr>
                            <td height="10"></td>
                          </tr>
                          <tr>
                            <td height="20" align="left" valign="top" class="vidtime">1:34</td>
                          </tr>
                        </table></td>
                        <td width="5"></td>
                      </tr>
                      <tr>
                        <td width="10" height="5"></td>
                        <td height="5"></td>
                        <td width="5" height="5"></td>
                      </tr>
                    </table>
                    
                    </td>
                  </tr>
                  <tr>
                    <td><table width="210" height="60" border="0" cellspacing="0" cellpadding="0" background="<?php echo $pt?>images/tab_bg.png">
                      <tr>
                        <td width="10" height="5"></td>
                        <td height="5"></td>
                        <td width="5" height="5"></td>
                      </tr>
                      <tr>
                        <td width="10"></td>
                        <td align="left" valign="top"><table width="195" height="50" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td height="20" align="left" valign="bottom" class="vidtitle">Title</td>
                            <td width="70" height="50" rowspan="3" align="left" valign="bottom" bgcolor="#333333"></td>
                          </tr>
                          <tr>
                            <td height="10"></td>
                          </tr>
                          <tr>
                            <td height="20" align="left" valign="top" class="vidtime">1:34</td>
                          </tr>
                        </table></td>
                        <td width="5"></td>
                      </tr>
                      <tr>
                        <td width="10" height="5"></td>
                        <td height="5"></td>
                        <td width="5" height="5"></td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td><table width="210" height="60" border="0" cellspacing="0" cellpadding="0" background="<?php echo $pt?>images/tab_bg.png">
                      <tr>
                        <td width="10" height="5"></td>
                        <td height="5"></td>
                        <td width="5" height="5"></td>
                      </tr>
                      <tr>
                        <td width="10"></td>
                        <td align="left" valign="top"><table width="195" height="50" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td height="20" align="left" valign="bottom" class="vidtitle">Title</td>
                            <td width="70" height="50" rowspan="3" align="left" valign="bottom" bgcolor="#333333"></td>
                          </tr>
                          <tr>
                            <td height="10"></td>
                          </tr>
                          <tr>
                            <td height="20" align="left" valign="top" class="vidtime">1:34</td>
                          </tr>
                        </table></td>
                        <td width="5"></td>
                      </tr>
                      <tr>
                        <td width="10" height="5"></td>
                        <td height="5"></td>
                        <td width="5" height="5"></td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td><table width="210" height="60" border="0" cellspacing="0" cellpadding="0" background="<?php echo $pt?>images/tab_bg.png">
                      <tr>
                        <td width="10" height="5"></td>
                        <td height="5"></td>
                        <td width="5" height="5"></td>
                      </tr>
                      <tr>
                        <td width="10"></td>
                        <td align="left" valign="top"><table width="195" height="50" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td height="20" align="left" valign="bottom" class="vidtitle">Title</td>
                            <td width="70" height="50" rowspan="3" align="left" valign="bottom" bgcolor="#333333"></td>
                          </tr>
                          <tr>
                            <td height="10"></td>
                          </tr>
                          <tr>
                            <td height="20" align="left" valign="top" class="vidtime">1:34</td>
                          </tr>
                        </table></td>
                        <td width="5"></td>
                      </tr>
                      <tr>
                        <td width="10" height="5"></td>
                        <td height="5"></td>
                        <td width="5" height="5"></td>
                      </tr>
                    </table></td>
                  </tr>
                </table></td>
              </tr>
            </table>
            <!--end side area-->
            
            </td>
          </tr>
        </table></td>
        <td width="10"></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td align="center" bgcolor="#E5E5E5"><table width="860" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td align="center"><span class="maintitle">BROWSE VIDEO TUTORIALS</span></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td align="center"><table width="860" border="0" cellspacing="0" cellpadding="5">
        <tr>
        <?php			
			echo $ctitles;
        ?>
        </tr>
        <tr>
        <?php
			echo $clinks;
        ?>
        </tr>
    </table></td>
  </tr>
</table>
<?php
	}
?>
