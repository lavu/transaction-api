<?php
	require_once(dirname(__FILE__).'/mailer.php');
	function init_default_shift_info(){
		return array(	"loaded"=>false,
						"id"=>0,
						"user_id"=>"",
						"start_datetime"=>"",
						"end_datetime"=>"",
						"coverage_requested"=>"no",
						"coverage_message"=>"",
						"coverage_accepted_by"=>"",
						"coverage_accepted_datetime"=>"");
	}

	function output_schedule_area($sarea,$svars){
		$str = "";
		if($sarea=="shift_info"){
			$shift_start = $svars['shift_start'];
			$shift_end = $svars['shift_end'];
			$shift_date = $svars['shift_date'];
			$shift_id = $svars['shift_id'];
			$shift_action = $svars['shift_action'];
			$back_click = "open_floating_area(\"shift_info\",\"shift_start=$shift_start&shift_end=$shift_end&shift_date=$shift_date&shift_id=$shift_id\")";
			$shift_info = load_shift_info($shift_id);
			$user_info = load_user_info($shift_info['user_id']);
			$user_fullname = $user_info['display_name'];
			$str .= "<style>";
			$str .= "	.main_grey_text { ";
			$str .= "		font-size:18px; color:#777777; ";
			$str .= "	} ";
			$str .= "	.secondary_grey_text { ";
			$str .= "		font-size:18px; color:#aaaaaa; ";
			$str .= "	} ";
			$str .= "	.error_text { ";
			$str .= "		font-size:11px; color:#aa0000; ";
			$str .= "	} ";
			$str .= "</style>";
			$str .= "<font class='main_grey_text'>";
			$str .= "<table><tr><td style='border-bottom:solid 1px #777777; font-size:18px; color:#777777' width='240' align='center'>" . $user_fullname . "</td></tr></table><br>";
			$str .= "<font style='color:#444488'>" . output_simple_date($shift_date) . "</font><br>";
			$str .= colorize_time($shift_start) . " - " . colorize_time($shift_end) . "<br><br>";
			if($shift_action=="request_coverage" || $shift_action=="submit_coverage_request"){
                $coverage_notes = $svars['coverage_notes'];
				if($shift_action=="submit_coverage_request"){
					$coverage_notes_length = strlen(trim($coverage_notes));
					if(trim($coverage_notes)==""){
						$str .= "<font class='error_text'>Please enter a reason to continue...</font><br>";
						$show_coverage_form = true;
					}else if($coverage_notes_length > 140){
						$str .= "<font class='error_text'>Please limit your coverage reason to 140 characters<br>(you submitted $coverage_notes_length characters)...</font><br>";
						$show_coverage_form = true;
					}else{
						// #MAIL HERE REQUEST
						MailRequestMessage($sarea,$svars,$user_info);
						$success = update_shift_info($shift_id,array("coverage_requested"=>"yes","coverage_message"=>$coverage_notes));
						if($success){
							$str .= "<br>Coverage Request Submitted!<br><br>";
							$str .= "<input type='button' value='Ok >>' style='font-size:10px; color:#888888' onclick='close_floating_window()' /> ";
							$str .= "<script language='javascript'>getNeighboringMonth(0);</script>";
						}else{
							$str .= "<br>Error: Failed to Submit your coverage request.  You may want to try again, <br>or contact support if the issue persists<br><br>";
						}
						$show_coverage_form = false;
					}
				}else{
					$show_coverage_form = true;
					$coverage_notes = "";
				}
				if($show_coverage_form){
					$submit_click = "open_floating_area(\"shift_info\",\"shift_start=$shift_start&shift_end=$shift_end&shift_date=$shift_date&shift_id=$shift_id&shift_action=submit_coverage_request&coverage_notes=\" + escape(document.getElementById(\"request_text\").value))";
					$str .= "<table><tr><td valign='bottom'>";
					$str .= "<input type='button' value='<< Back' style='font-size:10px; color:#888888' onclick='$back_click' /> ";
					$str .= "</td><td valign='bottom' class='secondary_grey_text'>";
					$str .= "Reason for Coverage Request:";
					$str .= "</td></tr></table>";
					$str .= "<textarea rows='6' cols='44' id='request_text' name='request_text'>".str_replace("<","&lt;",$coverage_notes)."</textarea>";
					$str .= "<input type='button' value='Submit Coverage Request' style='color:#444466' onclick='$submit_click' />";
				}
			}else if($shift_info['coverage_requested']=="yes" || $shift_info['coverage_requested']=="accepted"){
				if($shift_info['coverage_accepted_by']!=""){
					$ac_user_info = load_user_info($shift_info['coverage_accepted_by']);
					$str .= "Coverage Accepted By " . $ac_user_info['display_name'];
				}else if($shift_action=="accept_coverage_request"){
					if(user_has_conflicting_shift(admin_info("loggedin"),$shift_id)){
						$str .= "<br>Unable to Transfer shift:<br>You already have a shift during this time.<br><br>";
					}else{
						$success = duplicate_shift($shift_id,array("coverage_requested"=>"no","coverage_accepted_by"=>"","coverage_message"=>"","coverage_accepted_datetime"=>"","user_id"=>admin_info("loggedin")));
						if($success){
							// #MAIL HERE Accept
							MailAcceptMessage($sarea,$svars,$user_info,admin_info("loggedin"));
							$success = update_shift_info($shift_id,array("coverage_requested"=>"accepted","coverage_accepted_by"=>admin_info("loggedin"),"coverage_accepted_datetime"=>date("Y-m-d H:i:s")));
							$str .= "<br>Coverage Request Accepted!<br><br>";
							$str .= "<input type='button' value='Ok >>' style='font-size:10px; color:#888888' onclick='close_floating_window()' /> ";
							$str .= "<script language='javascript'>getNeighboringMonth(0);</script>";
						}else{
							$str .= "<br>Error: Failed to Submit your coverage acceptance.  You may want to try again, <br>or contact support if the issue persists<br><br>";
						}
					}
				}else if($shift_action=="revoke_coverage_request"){
					// #MAIL HERE Revoke
					MailRevokeMessage($sarea,$svars,$user_info);
					$success = update_shift_info($shift_id,array("coverage_requested"=>"revoked","coverage_message"=>$coverage_notes));
					if($success){
						$str .= "<br>Coverage Request Revoked!<br><br>";
						$str .= "<input type='button' value='Ok >>' style='font-size:10px; color:#888888' onclick='close_floating_window()' /> ";
						$str .= "<script language='javascript'>getNeighboringMonth(0);</script>";
					}else{
						$str .= "<br>Error: Failed to Submit your coverage request.  You may want to try again, <br>or contact support if the issue persists<br><br>";
					}
				}else{
					$str .= "Coverage Requested";
					$str .= "<table><tr><td align='right' valign='top' style='font-size:12px; font-weight:bold; color:#777777'>";
					$str .= "Notes: ";
					$str .= "</td><td align='left' valign='top' style='font-size:12px; color:#777777'>";
					$str .= str_replace("\n","<br>",str_replace("<","&lt;",$shift_info['coverage_message']));
					$str .= "</td></tr></table>";
					if($shift_info['user_id']!=admin_info("loggedin")){
						$accept_click = "";
						$accept_click .= "if(confirm(\"Are you sure you want to Accept this Shift?  Once you accept this shift you will be responsible for it.\")) ";
						$accept_click .= "open_floating_area(\"shift_info\",\"shift_start=$shift_start&shift_end=$shift_end&shift_date=$shift_date&shift_id=$shift_id&shift_action=accept_coverage_request\") ";
						$str .= "<br><input type='button' value='Accept Coverage Request' style='color:#444466' onclick='$accept_click' />";
					}else{
						$revoke_click = "open_floating_area(\"shift_info\",\"shift_start=$shift_start&shift_end=$shift_end&shift_date=$shift_date&shift_id=$shift_id&shift_action=revoke_coverage_request\")";
						$str .= "<br><input type='button' value='Revoke Coverage Request' style='color:#444466' onclick='$revoke_click' />";
					}
				}
			}else{
				$request_shift_click = "open_floating_area(\"shift_info\",\"shift_start=$shift_start&shift_end=$shift_end&shift_date=$shift_date&shift_id=$shift_id&shift_action=request_coverage\")";
				$str .= "<input type='button' value='Request Coverage' onclick='$request_shift_click' style='color:#444466'>";
			}
			$str .= "</font>";
		}else{
			$str .= "Unknown Area: $sarea<br>";
		}
		return $str;
	}

	function colorize_time($t){
		$t = strtolower(trim($t));
		$t = str_replace("am","<font style='color:#777799'>am</font>",$t);
		$t = str_replace("pm","<font style='color:#777799'>pm</font>",$t);
		$t = "<font style='color:#689812'>$t</font>";
		return $t;
	}

	function output_simple_date($d,$f="D M d"){
		$date_parts = explode("-",$d);
		if(count($date_parts) > 2){
			$year = $date_parts[0];
			$month = $date_parts[1];
			$day = $date_parts[2];
			$dts = mktime(0,0,0,$month,$day,$year);
			return date($f,$dts);
		}else{
			return $d;
		}
	}

	function urlencode_array($arr,$encode_dimensional=false){
		$str = "";
		foreach($arr as $akey => $aval){
			if($str!="") $str .= "&";
			$str .= $akey . "=" . urlencode($aval);
		}
		if($encode_dimensional){
			$str = str_replace("=","(pass_eq)",str_replace("&","(pass_and)",$str));
		}
		return $str;
	}

	function urldecode_string($str,$decode_dimensional=false){
		if($decode_dimensional){
			$str = str_replace("=","(pass_inner_eq)",str_replace("&","(pass_inner_and)",$str));
			$str = str_replace("(pass_eq)","=",str_replace("(pass_and)","&",$str));
		}
		$arr = array();
		$str_parts = explode("&",$str);
		for($i=0; $i<count($str_parts); $i++){
			$inner_parts = explode("=",$str_parts[$i]);
			if(count($inner_parts) > 1){
				$setval = urldecode($inner_parts[1]);
				if($decode_dimensional){
					$setval = str_replace("(pass_inner_eq)","=",str_replace("(pass_inner_and)","&",$setval));
				}
				$arr[$inner_parts[0]] = $setval;
			}
		}
		return $arr;
	}

	function load_user_info($user_id){
		$user_query = lavu_query("select * from `users` where `id`='[1]'",$user_id);
		if(mysqli_num_rows($user_query)){
			$user_read = mysqli_fetch_assoc($user_query);
			$user_fullname = trim($user_read['f_name'] . " " . $user_read['l_name']);
			if($user_fullname=="") $user_fullname = "user: " . $user_read['username'];
			$user_read['display_name'] = $user_fullname;
			return $user_read;
		}
		return false;
	}

	function load_shift_info($shift_id){
		$shift_info = array();
		$shift_info['loaded'] = false;
		$shift_info['id'] = $shift_id;
		$shift_query = lavu_query("select * from `user_schedules` where `id`='[1]'",$shift_id);
		if(mysqli_num_rows($shift_query)){
			$shift_read = mysqli_fetch_assoc($shift_query);
			$shift_info['start_datetime'] = $shift_read['start_datetime'];
			$shift_info['end_datetime'] = $shift_read['end_datetime'];
			$shift_info['user_id'] = $shift_read['user_id'];
			$shift_info['loaded'] = true;
			$default_info = init_default_shift_info();
			$extra_info = urldecode_string($shift_read['data_long']);
			foreach($extra_info as $ekey => $eval){
				$shift_info[$ekey] = $eval;
			}
			foreach($default_info as $dkey => $dval){
				if(!isset($shift_info[$dkey])) $shift_info[$dkey] =  $dval;
			}
		}
		return $shift_info;
	}

	function user_has_conflicting_shift($current_user_id,$shift_id){
		$shift_info = load_shift_info($shift_id);
		if($shift_info['loaded']){
			//return "Shift Id: $shift_id<br>User Id: $current_user_id<br>";
			$exist_query = lavu_query("select `id` from `user_schedules` where `user_id`='[user_id]' and `start_datetime` < '[end_datetime]' and `end_datetime` > '[start_datetime]' and `data_long` NOT LIKE '%coverage_requested=accepted%'",array("user_id"=>$current_user_id,"start_datetime"=>$shift_info['start_datetime'],"end_datetime"=>$shift_info['end_datetime']));
			return mysqli_num_rows($exist_query);
		}else{
			return false;
		}
	}

	function duplicate_shift($shift_id,$shift_vars){
		return update_shift_info($shift_id,$shift_vars,"insert");
	}

	function update_shift_info($shift_id,$shift_vars,$update_action="update"){
		$conn = ConnectionHub::getConn('rest');
		$shift_info = load_shift_info($shift_id);
		if($shift_info['loaded']){
			$shift_query = lavu_query("select * from `user_schedules` where `id`='[1]'",$shift_id);
			if(mysqli_num_rows($shift_query)){
				$update_vars = array();
				$shift_read = mysqli_fetch_assoc($shift_query);
				$extra_info = urldecode_string($shift_read['data_long']);
				foreach($shift_vars as $skey => $sval){
					if(isset($shift_read[$skey])){
						$update_vars[$skey] = $sval;
					}else{
						$extra_info[$skey] = $sval;
					}
				}
				$update_vars['data_long'] = urlencode_array($extra_info);
				if($update_action=="insert"){
					$col_code = "";
					$val_code = "";
					$val_arr = array();
					foreach($shift_read as $skey => $sval){
						if($skey!="id"){
							if($col_code!="") $col_code .= ",";
							if($val_code!="") $val_code .= ",";
							$setval = $sval;
							if(isset($update_vars[$skey])) $setval = $update_vars[$skey];
							$val_arr[$skey] = $setval;
							$skey_escaped = $conn->escapeString($skey);
							$col_code .= "`$skey_escaped`";
							$val_code .= "'[$skey_escaped]'";
						}
					}
					return lavu_query("insert into `user_schedules` ($col_code) values ($val_code)",$val_arr);
				}else{
					$update_code = "";
					foreach($update_vars as $ukey => $uval){
						$ukey_escaped = $conn->escapeString($ukey);
						if($update_code!="") $update_code .= ",";
						$update_code .= "`$ukey_escaped`='[$ukey_escaped]'";
					}
					$update_vars['id'] = $shift_id;
					return lavu_query("update `user_schedules` set $update_code where `id`='[id]'",$update_vars);
				}
			}
		}
		return false;
	}

	function MailRequestMessage($sarea,$svars,$user_info){
		$data_name = sessvar("admin_dataname");
		if($data_name == '' ){return;}
		$shift_description = $svars['shift_date']." : ".$svars['shift_start']." - ".$svars['shift_end'];
		$test = ShiftMailerEnabled();
		if( empty( $test ) === true){return;}
		$constraint = ShiftTradeConstraint();
		$mailer = new EmployeeMailer($data_name);
		if($constraint !== '0-0'){
			$constraints = explode('-', $constraint);
			if(count($constraints) == 2){
				$mailer->setMinLevel($constraints[0]);
				$mailer->setMaxLevel($constraints[1]);
				$mailer->mail($user_info['username']." has made a coverage request for the following shift.\n$shift_description\n");
			}
		}
		$tempUser = $user_info['email'];
		if(empty($tempUser) ){
			$tempUser = $user_info['username'];
		}
		if(strpos($tempUser, '@') !== false){
            $tempUserInfo['email'] = $tempUser;
            $tempUserInfo['access_level'] = $user_info['access_level'];
            $mailer->userName($tempUserInfo);
			$mailer->mail("You have made a shift coverage request for the following shift.\n$shift_description\n");
		}
	}

	function MailAcceptMessage($sarea,$svars,$user_info){
		$data_name = sessvar("admin_dataname");
		if($data_name == '' ){return;}
		$test = ShiftMailerEnabled();
		if( empty( $test ) === true){return;}
		$constraint = ShiftAcceptConstraint();
		$mailer = new EmployeeMailer($data_name);
		if($constraint !== '0-0'){
			$constraints = explode('-', $constraint);
			if(count($constraints) == 2){
				$mailer->setMinLevel($constraints[0]);
				$mailer->setMaxLevel($constraints[1]);
				$mailer->mail($user_info['username'].' has accepted the shift coverage request.');
			}
		}
		$tempUser = $user_info['email'];
		if(empty($tempUser) ){
			$tempUser = $user_info['username'];
		}
		if(strpos($tempUser, '@') !== false){
            $tempUserInfo['email'] = $tempUser;
            $tempUserInfo['access_level'] = $user_info['access_level'];
            $mailer->userName($tempUserInfo);
			$mailer->mail('You have successfully accepted a shift coverage request.');
		}
	}

	function MailRevokeMessage($sarea,$svars,$user_info){
		$data_name = sessvar("admin_dataname");
		if($data_name == '' ){return;}
		$test = ShiftMailerEnabled();
		if( empty( $test ) === true){return;}
		$constraint = ShiftRevokeConstraint();
		$mailer = new EmployeeMailer($data_name);
		if($constraint !== '0-0'){
			$constraints = explode('-', $constraint);
			if(count($constraints) == 2){
				$mailer->setMinLevel($constraints[0]);
				$mailer->setMaxLevel($constraints[1]);
				$mailer->mail($user_info['username'].' has revoked their shift coverage request.');
			}
		}
		$tempUser = $user_info['email'];
		if(empty($tempUser) ){
			$tempUser = $user_info['username'];
		}
		if(strpos($tempUser, '@') !== false){
			$tempUserInfo['email'] = $tempUser;
            $tempUserInfo['access_level'] = $user_info['access_level'];
			$mailer->userName($tempUserInfo);
			$mailer->mail('You have successfully revoked your shift coverage request.');
		}
	}

	function ShiftMailerEnabled(){
		//shift_change_mail_notifications
		$query = lavu_query("select `value` from `config` where `setting`='shift_change_mail_notifications'");
		if(mysqli_num_rows($query)){
			$result = mysqli_fetch_assoc($query);
			return $result['value'];
		}
		return '0';
	}

	function ShiftTradeConstraint(){
		//shift_trading_email_list
		$query = lavu_query("select `value` from `config` where `setting`='shift_trading_email_list'");
		if(mysqli_num_rows($query)){
			$result = mysqli_fetch_assoc($query);
			return $result['value'];
		}
		return '0-0';
	}

	function ShiftAcceptConstraint(){
		//shift_accept_email_list
		$query = lavu_query("select `value` from `config` where `setting`='shift_accept_email_list'");
		if(mysqli_num_rows($query)){
			$result = mysqli_fetch_assoc($query);
			return $result['value'];
		}
		return '0-0';
	}


	function ShiftRevokeConstraint(){
		//shift_revoke_email_list
		$query = lavu_query("select `value` from `config` where `setting`='shift_revoke_email_list'");
		if(mysqli_num_rows($query)){
			$result = mysqli_fetch_assoc($query);
			return $result['value'];
		}
		return '0-0';
	}

	function ShiftApproveConstraint(){
		//shift_approve_email_list
		$query = lavu_query("select `value` from `config` where `setting`='shift_approve_email_list'");
		if(mysqli_num_rows($query)){
			$result = mysqli_fetch_assoc($query);
			return $result['value'];
		}
		return '0-0';
	}

?>
