<?php
	session_name("MYPOSLAVUSESSID");
	session_start();
	putenv("TZ=America/Chicago");
	require_once("/home/poslavu/public_html/admin/cp/resources/core_functions.php");
	$rdb = admin_info("database");
	$loggedin_id = admin_info("loggedin");
	$data_name = sessvar("admin_dataname");
	$locationid = sessvar("locationid");
	$locinfo = sessvar('location_info');
	$comPackage = $locinfo['component_package_code'];
	$maindb = "poslavu_MAIN_db";
	$in_lavu = 1;
	$lavu_minimum_login_level = 1;

	//------------------------------- Functions also defined in admin.poslavu.com index.php file (yes it is repeated code =O )-------------------//
	function check_lockout_status($lockout_level){
		$lockfname = "/home/poslavu/private_html/connects_lockout.txt";
		$fp = fopen($lockfname,"r");
		$lockout_status = fread($fp,filesize($lockfname));
		fclose($fp);
		if($lockout_status!=""){
			$lockout_parts = explode("|",$lockout_status);
			if(count($lockout_parts) > 1 && $lockout_parts[0] * 1 >= $lockout_level && $lockout_parts[1] * 1 > time() - 300){
				echo "We apologize for the inconvenience but the POSLavu BackEnd is temporarily Unavailable. Please check back later.";
				exit();
			}
		}
	}

	function is_dev(){
		if(strpos($_SERVER['REQUEST_URI'],"v2/")!==false){
			return true;
		}else{
			return false;
		}
	}

	function is_lavu_admin(){
		if (admin_info("lavu_admin") == 1){
			return true;
		}else{
			return false;
		}
	}
	//------------------------------------- End needlessly redefined functions yeh! --------------------------------------------------------------//

	function getLocationInfo($locId){
		$locationsResult = lavu_query("SELECT * FROM `locations` WHERE `id`='[1]'", $locId);
		if($locationsResult && mysqli_num_rows($locationsResult) ){
			return mysqli_fetch_assoc($locationsResult);
		}else{
			return false;
		}
	}

	function setDefaultLocation() {
		if(admin_info("loc_id")==0 || !is_numeric(admin_info("loc_id"))){
			$location_query = lavu_query("select * from `locations` WHERE `_disabled` != '1' order by `title` asc limit 1");
			if(mysqli_num_rows($location_query)){
				$location_read = mysqli_fetch_assoc($location_query);
				set_sessvar("locationid",$location_read['id']);
				set_sessvar("admin_loc_id",$location_read['id']);
			}
		}
		return admin_info("loc_id");	
	}

	/***DB CONNECTS ****/
	check_lockout_status(2);
	mlavu_select_db("poslavu_MAIN_db");
	lavu_auto_login();
	lavu_connect_dn(admin_info("dataname"), $rdb);
	setDefaultLocation();
	/***********END DB CONNECTS *****/

	if(isset($_GET['logout'])){admin_logout();}
	if(!admin_loggedin()){
		$content_status .= "&nbsp;";
		ob_start();
		define( 'LAVU_MINIMUM_LOGIN_LEVEL', 0 );
		require_once("/home/poslavu/public_html/admin/cp/areas/login.php");
		$outputHTML  = ob_get_clean();
		//$outputHTML .= "<script type='text/javascript'>document.getElementById('header').innerHTML='Employee Login'; document.title='Employee Login';</script>";
		$outputHTML = str_replace( '<span class="section_title">Log in</span>', '<span class="section_title">Employee Login</span>', $outputHTML );
		echo $outputHTML;
		exit();
	}else{
		// G L O B A L   V A R I A B L E S...
		$loc_id = admin_info("loc_id");
		$location_info = getLocationInfo($loc_id);
		// Top HTML
		if(!$_POST['is_ajax_request']){
			$tabbedMenuHTML = getTabbedMenuHTML();
			echo "<style>";
			echo "body, table { font-family:Verdana,Arial; font-size:12px; } ";
			echo "</style>";
			echo "<body bgcolor='#d4d6d4'>";
			echo "<center>";
			echo "<table cellpadding=12 cellspacing=0 bgcolor='#ffffff' align='center' border='0px'>";
			echo "<tr><td align='left' bgcolor='#eeeeee'>";
			echo "<div>$tabbedMenuHTML</div>";
			echo "</td><td align='right' bgcolor='#eeeeee'>";
			echo "<div><font style='color:#777777'>Logged In:</font> <font style='color:#444488'>" . admin_info("fullname") . "</font>&nbsp;&nbsp;&nbsp;&nbsp;<input type='button' style='color:#555555; font-size:10px' value='Logout' onclick='window.location = \"/?logout=1\"'></div>";
			echo "</td></tr>";
			echo "<tr><td align='middle' valign='top' height='600' colspan='80'>";
		}
		$mode = (isset($_GET['mode']))?$_GET['mode']:"employee_my_account";
		$fname = dirname(__FILE__) . "/".$mode.".php";
		if(is_file($fname)){
			if(!$_POST['is_ajax_request']){
				echo "<div style='padding:8px'>";
			}
			require_once($fname);
			if(!$_POST['is_ajax_request']){
				echo "</div>";
			}
		}else if($mode=="reset_password"){
			echo "<table><tr><td width='800' align='center'><br><br><br><br>Password has been reset.<br><br><input type='button' value='Continue >>' onclick='window.location = \"/\"' /></td></tr></table>";
		}
		if(!$_POST['is_ajax_request']){
			// Bottom HTML
			echo "</td></tr></table>";
			echo "<div style='position:fixed; left:0px; top:50px; width:100%; height:100%; z-index:100; visibility:hidden' id='floating_window'>";
			echo "<table width='100%'><tr><td align='center'>";
			echo "<table style='border:solid 2px #777777' cellpadding=6 cellspacing=0 bgcolor='#ffffff'>";
			echo "<tr><td align='right'><b><font style='color:#aaaaaa; font-size:20px; cursor:pointer' onclick='close_floating_window()'>X</font></b></td></tr>";
			echo "<tr>";
			echo "<td width='480' height='320' id='floating_window_content' align='center' valign='top'>";
			echo "&nbsp;";
			echo "</td>";
			echo "</tr>";
			echo "</table>";
			echo "</td></tr></table>";
			echo "</div>";
			echo "<script language='javascript'>";
			echo "function open_floating_area(load_area,load_vars) { ";
			echo "	document.getElementById('floating_window_content').innerHTML = '<table width=\"100%\" height=\"100%\"><tr><td align=\"center\" valign=\"center\"><img src=\"http://hotel.lavuhotel.com/cp/images/animated-progress.gif\" /></td></tr></table>'; ";
			echo "	document.getElementById('floating_window').style.visibility = 'visible'; ";
			echo "	runGenericAjaxRequestDennedyStyle(load_area,load_vars,'floating_window_content'); ";
			echo "} ";
			echo "function open_floating_window(set_content) { ";
			echo "	document.getElementById('floating_window_content').innerHTML = set_content; ";
			echo "	document.getElementById('floating_window').style.visibility = 'visible'; ";
			echo "} ";
			echo "function close_floating_window() { ";
			echo "	document.getElementById('floating_window_content').innerHTML = ''; ";
			echo "	document.getElementById('floating_window').style.visibility = 'hidden'; ";
			echo "} ";
			echo "function find_and_eval_script_tags(estr,script_start,script_end) ";
			echo "{ ";
			echo "  var xhtml = estr.split(script_start); ";
			echo "	for(var n=1; n<xhtml.length; n++) ";
			echo "	{ ";
			echo "		var xhtml2 = xhtml[n].split(script_end); ";
			echo "		if(xhtml2.length > 1) ";
			echo "		{ ";
			echo "			run_xhtml = xhtml2[0]; ";
			echo "			eval(run_xhtml); ";
			echo "		} ";
			echo "	} ";
			echo "} ";
			echo "</script>";
			echo "</center>";
			echo "</body>";
		}
	}

	function getTabbedMenuHTML(){
		ob_start();
		$t1color = "#aaaaaa";//"#98b624";
		$t2color = "#aaaaaa";
		$mode = (isset($_GET['mode']))?$_GET['mode']:"";
		if($mode=="employee_schedule"){
			$t2color = "#98b624";
		}else{
			$t1color = "#98b624";
		}
		?>
		<table cellpadding="0" cellspacing="1">
			<tr>
				<td style='width:100px;height:25px;background-color:<?php echo $t1color?>;color:#fff;text-align:center;vertical-align:middle;cursor:pointer'
							onclick="window.location='/?mode=employee_my_account'">
					My Account
				</td>
				<td style='width:100px;height:25px;background-color:<?php echo $t2color?>;color:#fff;text-align:center;vertical-align:middle;cursor:pointer'
							onclick="window.location='/?mode=employee_schedule'">
				  My Schedule
				</td>
			</tr>
		</table>
		<?php
		$outputHTMLReturn = ob_get_clean();
		return $outputHTMLReturn;
	}
?>
