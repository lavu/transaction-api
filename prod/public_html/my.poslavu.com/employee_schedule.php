<?php
	require_once(dirname(__FILE__) . "/schedule_areas.php"); // contains some functions that are needed

	$current_ajax_mode = (isset($_REQUEST['ajax_mode']))?$_REQUEST['ajax_mode']:"";
	if($current_ajax_mode!=""){
		$str = "";
		require_once(dirname(__FILE__) . "/schedule_areas.php");
		$str .= output_schedule_area($current_ajax_mode,$_POST);
		$ajaxJson = array();
		$ajaxJson['set_html'] = $str;
		echo json_encode($ajaxJson);
		exit();
	}

	//We determine if this is an ajax request or original page load.
	if($_POST['is_ajax_request']){
		$dataName = $_POST['data_name'];
		$monthRequested = $_POST['month_requested'];
		$yearRequested  = $_POST['year_requested'];
		$userID = admin_info("loggedin");//$_POST['user_id'];
		ENTRY_ajaxMonthRequest($monthRequested, $yearRequested, $userID);
	}else{
		$loc_timezone = $location_info['timezone'];
		$localizedDate = localize_datetime(date("Y-m-d"), $loc_timezone);
		$session_user_id = admin_info('loggedin');
		if($session_user_id){
			ENTRY_mainEntryNotAjax();
		}
	}

	function ENTRY_ajaxMonthRequest($requestedMonth, $requestedYear, $userID){
		//We also need the prior month as well as the following month for when the calendar overflows.
		//$priorMonthShiftRowsForUserArr = 
		//This Month.
		$formattedMonth = getPreviousCurrentAndNextFormattedMonths($requestedMonth, $requestedYear, $userID);
		$weekdayOfFirstForMonth = getWeekdayOffsetOfFirstForMonth($requestedMonth, $requestedYear);
		$daysInThisMonth = getNumberOfDaysInMonth($requestedMonth, $requestedYear);
		$lastMonth = $requestedMonth == 1 ? 12 : $requestedMonth - 1;
		$lastMonthsYear = $requestedMonth == 1 ? $requestedYear -1 : $requestedYear;
		$numDaysInLastMonth = getNumberOfDaysInMonth($lastMonth, $lastMonthsYear);
		$ajaxJSONResponseArr = array();
		$ajaxJSONResponseArr['calendar_body_html'] = generateCalendarBodyTable($requestedYear . "-" . $requestedMonth, $weekdayOfFirstForMonth, $daysInThisMonth, $numDaysInLastMonth, $formattedMonth);
		$ajaxJSONResponseArr['calendar_top_nav_bar_html'] = getTopNavigationBarHTML($requestedMonth, $requestedYear);
		echo json_encode($ajaxJSONResponseArr);
		exit();
	}

	function ENTRY_mainEntryNotAjax(){
		global $session_user_id, $localizedDate;
		//We need to find which month to show the user.
		$localizedDateParts = explode("-", $localizedDate);
		$year = $localizedDateParts[0];
		$month = $localizedDateParts[1];
		$output = getCSS();
		$formattedMonth = getPreviousCurrentAndNextFormattedMonths($month, $year, $session_user_id);
		$lastMonth = $month == 1 ? 12 : $month - 1;
		$lastMonthsYear = $month == 1 ? $year -1 : $year;
		$numDaysInLastMonth = getNumberOfDaysInMonth($lastMonth, $lastMonthsYear);
		$weekdayOfFirstForMonth = getWeekdayOffsetOfFirstForMonth($month, $year);
		$daysInThisMonth = getNumberOfDaysInMonth($month, $year);
		$output  = '';
		$output .= getTopNavigationBarHTML($month, $year);
		$output .= "<br>";
		$output .= "<div id='calendar_table_container'>";
		$output .= generateCalendarBodyTable($year . "-" . $month, $weekdayOfFirstForMonth, $daysInThisMonth,$numDaysInLastMonth, $formattedMonth);
		$output .= "</div>";
		$output .= "<script type='text/javascript'>var curr_year = $year; var curr_month = $month;</script>";
		$output .= getAjaxJavascript();//htmlspecialchars()
		echo $output;
	}

	function getPreviousCurrentAndNextFormattedMonths($requestedMonth, $requestedYear, $userID){
		$prevMonth;
		$prevMonthsYear;
		$nextMonth;
		$nextMonthsYear;
		if($requestedMonth == 1){
			$prevMonth = 12;
			$prevMonthsYear = $requestedYear - 1;
			$nextMonth = 2;
			$nextMonthsYear = $requestedYear;
		}else if($requestedMonth == 12){
			$prevMonth = 11;
			$prevMonthsYear = $requestedYear;
			$nextMonth = 1;
			$nextMonthsYear = $requestedYear + 1;
		}else{
			$prevMonth = $requestedMonth - 1;
			$prevMonthsYear = $requestedYear;
			$nextMonth = $requestedMonth + 1;
			$nextMonthsYear = $requestedYear;
		}

		//Previous Month.
		$prevMonthNumDays = getNumberOfDaysInMonth($prevMonth, $prevMonthsYear);
		$prevMonthsShiftRowsForUserArr = getShiftRowsThatAreWithinMonth($prevMonth, $prevMonthsYear, $userID);
		$prevMonthFormatted = formatAndGroupMonthShiftsIntoDays($prevMonthsShiftRowsForUserArr);

		//This Month.
		$thisMonthNumDays = getNumberOfDaysInMonth($requestedMonth, $requestedYear);
		$monthShiftRowsForUserArr = getShiftRowsThatAreWithinMonth($requestedMonth, $requestedYear, $userID);
		$formattedMonth = formatAndGroupMonthShiftsIntoDays($monthShiftRowsForUserArr);

		//Next Month.
		$nextMonthNumDays = getNumberOfDaysInMonth($nextMonth, $nextMonthsYear);
		$nextMonthsShiftRowsForUserArr = getShiftRowsThatAreWithinMonth($nextMonth, $nextMonthsYear, $userID);
		$nextMonthFormatted = formatAndGroupMonthShiftsIntoDays($nextMonthsShiftRowsForUserArr);
		//Now we form these three formatted months into a single larger array where index 0 and < 0 are from the 
		//previous month.  Indices 1 to $(numberOfDaysInMonth are from this month, and > numberOfDaysInThisMonth
		//are the next month.
		//We begin by adding the previous month
		for($i = 0; $i < $prevMonthNumDays; $i++){
			$formattedMonth[0 - $i] = $prevMonthFormatted[$prevMonthNumDays - $i];
		}
		//Now we add the next month
		for($i = 1; $i <= $nextMonthNumDays; $i++){
			$formattedMonth[$thisMonthNumDays + $i] = $nextMonthFormatted[$i];
		}
		return $formattedMonth;
	}

	function formatAndGroupMonthShiftsIntoDays($monthArrOfShifts){
		//Kind of brutal, we now have a month array of day arrays of shifts.
		$monthArray = array();
		//We create a bin for every possible day.
		for($d = 1; $d < 32; $d++){
			$monthArray[$d] = array();
		}
		//Now we loop through every shift, format it, and place it into the array.
		foreach($monthArrOfShifts as $key => $shiftRow){
			//Handling the start time formatting.
			$startTimeMilitary = $shiftRow['start_time'];
			$startTimeMilitaryParts = explode(":", $startTimeMilitary);
			$startHourMilitary = $startTimeMilitaryParts[0];
			$startMins = $startTimeMilitaryParts[1];
			$startPeriod = floor($startHourMilitary / 12) % 2 == 0 ? 'am' : 'pm';
			$startHour = $startHourMilitary % 12;
			$startHour = $startHour == 0 ? 12 : $startHour;
			//Handling the end time formatting.
			$endTimeMilitary = $shiftRow['end_time'];
			$endTimeMilitaryParts = explode(":", $endTimeMilitary);
			$endHourMilitary = $endTimeMilitaryParts[0];
			$endMins = $endTimeMilitaryParts[1];
			$endPeriod = floor($endHourMilitary / 12) % 2 == 0 ? 'am' : 'pm';
			$endHour = $endHourMilitary % 12;
			$endHour = $endHour == 0 ? 12 : $endHour;
			//Put both together into the shift array.
			$shiftAsArr = array($startHour . ":" . $startMins . $startPeriod, $endHour . ":" . $endMins . $endPeriod,$shiftRow['id'],$shiftRow);
			//Now we place the shift in the bin (its day).
			$exactDate = $shiftRow['start_datetime'];
			$exactDate = explode(' ', $exactDate);
			$exactDate = $exactDate[0];
			$exactDateParts = explode("-",$exactDate);
			$day = $exactDateParts[2];
			$monthArray[intval($day)][$startTimeMilitary . "U" . $shiftRow['user_id'] . "I" . $shiftRow['id']] = $shiftAsArr;
		}
		//Finally we sort all shifts for each day, by its key which is the start time in military format.
		for($d = 1; $d < 32; $d++){
			$dayArr = &$monthArray[$d];
			ksort($dayArr);
			$monthArray[$d] = array_values($monthArray[$d]);//Reset the indices.
		}
		ksort($monthArray);
		return $monthArray;
	}

	function getShiftRowsThatAreWithinMonth($pMonth, $year, $userID){
		//We now want the 4-digit representation of a year.
		if(strlen($year) == 2){
			$year = '20' . $year;
		}
		//We want to make sure we are using leading zeros in the month.
		if(strlen($pMonth) == 1){
			$pMonth = '0' . $pMonth;
		}
		
		//We pull all shifts where the Sunday Base was in the given month or the month prior.
		//Then we caculate the actual date and remove any dates not in the given month.
		$currentMonthNumber  = intval($pMonth);
		$currentMonthYear    = intval($year);
		$nextMonthNumber     = $currentMonthNumber == 12 ? 1 : $currentMonthNumber + 1;
		$nextMonthYear       = $currentMonthNumber == 12 ? $currentMonthYear + 1 : $currentMonthYear;
		
		//Make sure that the month values have leading zeros if needed.
		if(strlen($currentMonthNumber) == 1){
			$currentMonthNumber = '0' . $currentMonthNumber;
		}
		if(strlen($nextMonthNumber) == 1){
			$nextMonthNumber = '0' . $nextMonthNumber;
		}
		
		//The bounds for the query
		$lowerBoundDateTime = $year . '-' . $pMonth . '-01 00:00:00';
		$upperBoundDateTime = $nextMonthYear . '-' . $nextMonthNumber . '-01 00:00:00';
		
		//Pull all shifts where the date is from the selected month or the month before it.
		$getAllShiftsInMonthQuery  = "SELECT * FROM `user_schedules` WHERE (`user_id` = '[1]' or `data_long` LIKE '%coverage_requested=yes%') AND ";
		$getAllShiftsInMonthQuery .= "(`start_datetime` >= '[2]' AND `start_datetime` < '[3]') ";
		$getAllShiftsInMonthQuery .= "ORDER BY `start_datetime`";
		$shiftsQueryResult = lavu_query($getAllShiftsInMonthQuery, $userID, $lowerBoundDateTime, $upperBoundDateTime);
		$thisMonthShiftsArray = array();
		while($currShiftRow = mysqli_fetch_assoc($shiftsQueryResult)){
			$thisMonthShiftsArray[] = $currShiftRow;
		}
		return $thisMonthShiftsArray;
	}

	function getTopNavigationBarHTML($month, $year){
		$fullDate = "$year-$month-01";
		
		//Now we need to format the date for user display.
		$monthInDisplayFormat = date('F  Y', strtotime($fullDate));
		$navBarHTML = "
			<div id='calendar_top_navigation_bar' style='width:100%';>
				<table width= > 
					<tr>
						<td> 
							<div style='width:50px;'>
								<input type='button' style='width:50px;' onclick='getNeighboringMonth(-1)' value='<<'>
							</div>
						</td>
						<td>
							<div style='width:150px; text-align:center;'>
								$monthInDisplayFormat
							</div>
						</td>
						<td>
							<div style='width:50px;'>
								<input type='button' style='width:50px;' onclick='getNeighboringMonth(1)' value='>>'>
							</div>
						</td>
					</tr>
				</table>
			</div>
		";
		return $navBarHTML;
	}

	function getAjaxJavascript(){
		ob_start();
		?>
			<script type='text/javascript'>
				// Added after the fact by CF
				function runGenericAjaxRequestDennedyStyle(run_mode,run_vars,write_to_div){
					var xmlhttp;
					if (window.XMLHttpRequest){
						xmlhttp=new XMLHttpRequest();
					}else{
						xmlhttp=new ActiveXObject('Microsoft.XMLHTTP');
					}
					xmlhttp.onreadystatechange=function(){
						if(xmlhttp.readyState == 4 && xmlhttp.status == 200){
							var jsonHTMLCollectionResponse = xmlhttp.responseText;
							var returnedArr = JSON.parse(jsonHTMLCollectionResponse);
							var set_html = returnedArr['set_html'];
							document.getElementById(write_to_div).innerHTML = set_html;
							find_and_eval_script_tags(set_html,"<" + "script language=\"javascript\"" + ">","<" + "/script>");
							find_and_eval_script_tags(set_html,"<" + "script language='javascript'" + ">","<" + "/script>");
						}
					}
					//var url = 'employee_schedule.php';
					var url = '/index.php?mode=employee_schedule&ajax_mode=' + run_mode;
					xmlhttp.open('POST', url, true);
					xmlhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
					
					var post_params  = 'is_ajax_request=1&' + run_vars;
					//'is_ajax_request=1&month_requested=' + curr_month + '&year_requested=' + curr_year;
					xmlhttp.send(post_params);
				}

				//We should have globally:   curr_year  &  curr_month
				function getRequestedMonth(month, year){
					var xmlhttp;
					if (window.XMLHttpRequest){
						xmlhttp=new XMLHttpRequest();
					}else{
						xmlhttp=new ActiveXObject('Microsoft.XMLHTTP');
					}
					xmlhttp.onreadystatechange=function(){
						if(xmlhttp.readyState == 4 && xmlhttp.status == 200){
							var jsonHTMLCollectionResponse = xmlhttp.responseText;
							var returnedArr = JSON.parse(jsonHTMLCollectionResponse);
							document.getElementById('calendar_table_container').innerHTML = returnedArr['calendar_body_html'];
							document.getElementById('calendar_top_navigation_bar').innerHTML = returnedArr['calendar_top_nav_bar_html'];
						}
					}
					//var url = 'employee_schedule.php';
					var url = '/index.php?mode=employee_schedule';
					xmlhttp.open('POST', url, true);
					xmlhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
					
					var post_params  = 'is_ajax_request=1&month_requested=' + curr_month + '&year_requested=' + curr_year;
					xmlhttp.send(post_params);
				}

				function getNeighboringMonth(plus_1_or_minus_1){
					if(plus_1_or_minus_1 == 1){
						if(curr_month == 12){
							curr_month = 1;
							curr_year++;
						}else{
							curr_month++;
						}
					}
					else if(plus_1_or_minus_1 == -1){
						if(curr_month == 1){
							curr_month = 12;
							curr_year--;
						}else{
							curr_month--;
						}
					}
					getRequestedMonth(curr_month, curr_year);
				}
			</script>
		<?php
		return ob_get_clean();
	}

	function getCSS(){
		$cssString = "
		<style type='text/css'>

		/* <td> tags always try to center things. */
		.td_day_div_style{
			position:relative;
			text-align:right;
			height:100%;
		}
		.dayofweek {
			color:#999999;
		}
		.dayOfWeekHeader td {
			
		}
		</style>
		";
		
		return $cssString;
	}

	//View Methods
	//Returns the html that is used for a day within
	//the month calendar.    
	function generateDayTable($yearmonth, $dayOfMonthLabelText, $shiftArr, $width, $height, $out_of_month_bounds = false){
		$backColor = $out_of_month_bounds ? "#cccecc" : "#f0f2f0";
		 $borderColor = $out_of_month_bounds ? "#aaadaa" : "#d0d3d0";
		 $labelColor = $out_of_month_bounds ? "#999999" : "#82a00d";
		$d_html = "
			<div style='overflow:hidden;align:center' align='center'>
			<table style='align:center;' align='center' cellspacing=0 cellpadding=4>
				<tr>
					<td style='background-color:$backColor; border:solid 1px $borderColor'>
						<div class='td_day_div_style' style='width:$width;height:$height;text-align:right; color:$labelColor'>
							$dayOfMonthLabelText <br>
							<table style='width:100%;overflow:scroll' cellpadding='4' cellspacing=0>";
							
							//The shift rows will adjust in size to the size of the font, so the greater the
							//amount of shifts in a day, the lesser the font-size so that all shift information
							//fits within the day.
							$fontSize = 10;
							if(count($shiftArr) > 4){
								$fontSize = 9;
							} 
							if(count($shiftArr) > 5){
								$fontSize = 8;
							}
							if(count($shiftArr) > 6){
								$fontSize = 6.5;
							}
							//Now we build the table of what shifts they have.
							for($i=0;$i < count($shiftArr)  && $i < 7; $i++){
								$shift_start = $shiftArr[$i][0];
								$shift_end = $shiftArr[$i][1];
								$shift_date = $yearmonth;
								$shift_id = $shiftArr[$i][2];
								$shift_read = $shiftArr[$i][3];
								$preshift_text = "";
								$aftshift_text = "";
								$fontColor = "#98b624";
								$overColor = "#628620";
								$extra_info = urldecode_string($shift_read['data_long']);
								if(isset($extra_info['coverage_requested']) && ($extra_info['coverage_requested']=="yes" || $extra_info['coverage_requested']=="accepted")){
									$preshift_text = "*";
									if($shift_read['user_id']==admin_info("loggedin")){
										$fontColor = "#aa0000";
										$overColor = "#ddaaaa";
										if($extra_info['coverage_requested']=="accepted"){
											$fontColor = "#996677";
											$preshift_text .= "<strike>";
											$aftshift_text = "</strike>";
										}
									}else{
										$fontColor = "#0000aa";
										$overColor = "#aaaadd";
									}
								}
								$shiftClick = "open_floating_area(\"shift_info\",\"shift_start=$shift_start&shift_end=$shift_end&shift_date=$shift_date&shift_id=$shift_id\")";
								$d_html .= "<tr><td style='font-size:$fontSize; color:$fontColor;cursor:pointer' onclick='$shiftClick' onmouseover='this.bgColor = \"$overColor\"' onmouseout='this.bgColor = \"$backColor\"'><nobr><b>$preshift_text$shift_start - $shift_end$aftshift_text</b></nobr></td></tr>";
							}
		$d_html .= "
							</table>
						</div>
					</td>
				</tr>
			</table>
			</div>
		";
		return $d_html;
	}

	function generateCalendarBodyTable($yearmonth, $firstDayOffsetInt, $daysInMonth, $daysInPreviousMonth, $monthFormattedShiftsArr){
		$dawstyle = "color:#777777;";
		$neededRowsInCalendarMonth = ceil(($firstDayOffsetInt + $daysInMonth)/7);
		$dayCellHeight = 460/$neededRowsInCalendarMonth . 'px';
		$dayCellWidth =  800/7 .'px';
		$CALENDAR_BODY_DAYS_HTML  = "<table style=''>";
		//The top day of week headers.
		$CALENDAR_BODY_DAYS_HTML .= "<tr style='text-align:center;'><td style='$dawstyle'>Sunday</td><td style='$dawstyle'>Monday</td><td style='$dawstyle'>Tuesday</td><td style='$dawstyle'>Wednesday</td><td style='$dawstyle'>Thursday</td><td style='$dawstyle'>Friday</td><td style='$dawstyle'>Saturday</td></tr>";
		for($y = 0; $y < $neededRowsInCalendarMonth; $y++){
			$CALENDAR_BODY_DAYS_HTML .= "<tr>";
			//We make each row a table so that if one cell grows, they all do in that row.
			for($x = 0; $x < 7; $x++){
				$relativeDayOfMonthNumber = $y * 7 + $x - $firstDayOffsetInt + 1;
				$relativeDayOfMonthNumber = $relativeDayOfMonthNumber > $daysInMonth ? $relativeDayOfMonthNumber : $relativeDayOfMonthNumber;
				$displayDayOfMonthNumber;
				if($relativeDayOfMonthNumber <= 0){
					$displayDayOfMonthNumber = $daysInPreviousMonth + $relativeDayOfMonthNumber;   
				}else if($relativeDayOfMonthNumber > $daysInMonth){
					$displayDayOfMonthNumber = $relativeDayOfMonthNumber % $daysInMonth;
				}else{
					$displayDayOfMonthNumber = $relativeDayOfMonthNumber;
				}
				$outBounds = !($relativeDayOfMonthNumber >= 1 && $relativeDayOfMonthNumber <= $daysInMonth);
				$CALENDAR_BODY_DAYS_HTML .= "<td>";
				$CALENDAR_BODY_DAYS_HTML .= generateDayTable($yearmonth . "-" . $displayDayOfMonthNumber, $displayDayOfMonthNumber, $monthFormattedShiftsArr[$relativeDayOfMonthNumber],$dayCellWidth,$dayCellHeight, $outBounds);
				$CALENDAR_BODY_DAYS_HTML .= "</td>";
			}
			$CALENDAR_BODY_DAYS_HTML .= "</tr>";
		}
		$CALENDAR_BODY_DAYS_HTML .= "</table>";
		return $CALENDAR_BODY_DAYS_HTML;
	}

	function getWeekdayOffsetOfFirstForMonth($month, $year){
		$time = strtotime("$year-$month-1");
		return date("w",$time);
	}

	function getNumberOfDaysInMonth($month_1_based, $year){
		return cal_days_in_month(CAL_GREGORIAN, $month_1_based, $year);
	}

?>
