<?php
	echo "<table><tr><td width='800' align='center' valign='top'>";

	if(!function_exists("assign_file_table_setting_names")){
		function assign_file_table_setting_names($v1,$v2,$v3,$v4,$v5){
			return true;
		}
		function get_help_mark_styling_using_html(){
			return true;
		}
	}

	function verify_user_main($action,$cc,$username,$email){
		$set_cc = str_replace("'","''",$cc);
		$set_username = str_replace("'","''",$username);
		$rquery = mlavu_query("select * from `poslavu_MAIN_db`.`customer_accounts` where `dataname`='[1]'",$set_cc);
		if(mysqli_num_rows($rquery)){
			$rread = mysqli_fetch_assoc($rquery);
			$cid = $rread['id'];
			$verify_query = mlavu_query("select * from `poslavu_MAIN_db`.`customer_accounts` where `username`='[1]'",$set_username);
			if(mysqli_num_rows($verify_query)){
				if($action=="remove"){
					mlavu_query("delete from `poslavu_MAIN_db`.`customer_accounts` where `dataname`='[1]' and `username`='[2]'",$set_cc,$set_username);
				}
				if ($action=="update") {
					mlavu_query("UPDATE `poslavu_MAIN_db`.`customer_accounts` SET `email` = '[1]' WHERE `username` = '[2]'", $email, $set_username);
				}
			}else{
				if($action=="insert" || $action=="update"){
					$cinfo = array();
					$cinfo['dataname'] = $set_cc;
					$cinfo['restaurant'] = $cid;
					$cinfo['username'] = $set_username;
					$cinfo['email'] = $email;
					mlavu_query("insert into `poslavu_MAIN_db`.`customer_accounts` (`dataname`,`restaurant`,`username`,`email`) values ('[dataname]','[restaurant]','[username]','[email]')",$cinfo);
				}
			}
		}
	}

	function convert_to_urlvar($str){
		return strtolower(str_replace(" ","_",$str));
	}

	function count_times_passsed_since($try_type,$try_count,$try_span){
		$try_query = lavu_query("select * from `config` where `setting`='count times' and `value`='[1]'",$try_type);
		if(mysqli_num_rows($try_query)){
			$try_read = mysqli_fetch_assoc($try_query);
			
			$last_try_time = $try_read['value2'];
			$last_try_count = $try_read['value3'];
			$try_id = $try_read['id'];
			
			if($last_try_time > time() - (60 * $try_span)){
				$last_try_count = ($last_try_count * 1);
			}else{
				$last_try_count = 0;
			}
		}else{
			$last_try_count = 0;
			$try_id = 0;
		}
		
		$set_try_count = $last_try_count + 1;
		$set_try_time = time();
		$set_try_datetime = date("Y-m-d H:i:s");
		
		if($try_id > 0){
			lavu_query("update `config` set `value2`='[2]', `value3`='[3]', `value4`='[4]' where `id`='[5]'",$try_type,$set_try_time,$set_try_count,$set_try_datetime,$try_id);
		}else{
			lavu_query("insert into `config` (`setting`,`value`,`value2`,`value3`,`value4`) values ('count times','[1]','[2]','[3]','[4]')",$try_type,$set_try_time,$set_try_count,$set_try_datetime);
		}
		
		if($set_try_count > $try_count){
			return true;
		}else{
			return false;
		}
	}

	$submode = urlvar("submode","general_info");
	$tablename = "users";
	$forward_to_base = "index.php?mode=employee_my_account";
	$forward_to = $forward_to_base . "&submode=$submode";
	$allow_order_by = true;
	$account_areas = array("General Info","Password","PIN");
	echo "<table><tr><td align='left'>";
	echo "<table cellspacing=2 cellpadding=4>";
	echo "<tr>";
	for($i=0; $i<count($account_areas); $i++){
		$area_name = $account_areas[$i];
		$area_name_url = convert_to_urlvar($area_name);
		$bgcolor = "#eeeeee";
		$color = "#000000";
		$bordercolor = "#cccccc";
		if($submode==$area_name_url){
			$bgcolor = "#98b624";
			$color = "#ffffff";
			$bordercolor = "#98b624";
		}
		echo "<td style='background-color:$bgcolor; color:$color; border:solid 1px $bordercolor; cursor:pointer' onclick='window.location = \"$forward_to_base&submode=$area_name_url\"'>";
		echo $area_name;
		echo "</td>";
	}
	echo "</tr>";
	echo "</table>";

	$fields = array();
	$sidespaces = "";
	$sidespaces .= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
	$sidespaces .= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
	$sidespaces .= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
	$sidespaces .= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";

	if($submode=="general_info"){
		$fields[] = array($sidespaces . speak("Account Settings") . $sidespaces,$sidespaces . "Account Settings" . $sidespaces,"seperator");
		$fields[] = array(speak("First Name"),"f_name","text","size:30","list:yes");
		$fields[] = array(speak("Last Name"),"l_name","text","size:30","list:yes");
		$fields[] = array(speak("Username"),"username","hidden","","list:yes");
		$fields[] = array(speak("Contact Info"),"Contact Info","seperator");
		$fields[] = array(speak("Address"),"address","text","size:40");
		$fields[] = array(speak("Home Phone"),"phone","text","","list:yes");
		$fields[] = array(speak("Mobile Phone"),"mobile","text","","list:yes");
		$fields[] = array(speak("Email Address"),"email","text","size:30");
		$fields[] = array(speak("PIN"),"PIN","hidden","","list:yes");
		$fields[] = array(speak("Submit"),"submit","submit");
	}else if($submode=="password"){
		$fields[] = array($sidespaces . speak("Account Settings" . $sidespaces),$sidespaces . "Account Settings" . $sidespaces,"seperator");
		$fields[] = array(speak("Username"),"username","hidden","","list:yes");
		$fields[] = array(speak("Password"),"password","create_password","size:8");
		$fields[] = array(speak("Submit"),"submit","submit");
	}else if($submode=="pin"){
		$fields[] = array($sidespaces . speak("Account Settings") . $sidespaces,$sidespaces . "Account Settings" . $sidespaces,"seperator");
		$fields[] = array(speak("Username"),"username","hidden","","list:yes");
		$fields[] = array(speak("PIN"),"PIN","text","size:8");	
		$fields[] = array(speak("Submit"),"submit","submit");
	}

	$rowid = admin_info("loggedin");
	if($rowid && count($fields) > 0){
		$max_try_counts = 4;
		$max_try_span = 15;
		if(isset($_POST['username']) && admin_info("username")!=$_POST['username']){
			echo "<br><br><b>Unable to Update</b><br>Error: Username Mismatch.  Unable to process this form.<br><br>Please log out and log back in to continue (your session may be expired)<br>";
			exit();
		}
		if(isset($_POST['password'])){
			$min_password_characters = 6;
			if(count_times_passsed_since("Password Change - " . admin_info("username"),$max_try_counts,$max_try_span)){
				echo "<br><br><b>Unable to Update</b><br>Sorry, but for security reasons you are unable to<br>change your password more than $max_try_counts times per $max_try_span minutes<br>Please try again later.";
				exit();
			}else if(strlen($_POST['password']) < $min_password_characters){
				echo "<br><br><b>Unable to Update</b><br>Sorry, but for security reasons your password must be at least<br>$min_password_characters characters long";
				exit();
			}
		}
		if(isset($_POST['PIN'])){
			$min_pin_characters = 4;
			if(count_times_passsed_since("PIN Change - " . admin_info("username"),$max_try_counts,$max_try_span)){
				echo "<br><br><b>Unable to Update</b><br>Sorry, but for security reasons you are unable to<br>change your PIN number more than $max_try_counts times per $max_try_span minutes<br>Please try again later.";
				exit();
			}else if(strlen($_POST['PIN']) < $min_pin_characters || !is_numeric($_POST['PIN'])){
				echo "<br><br><b>Unable to Update</b><br>Sorry, but for security reasons your pin number must be at least<br>$min_pin_characters characters long and must be made of numeric characters";
				exit();
			}
		}
		require_once(resource_path() . "/form.php");
	}
	echo "</td></tr></table>";
	echo "</td></tr></table>";
?>
