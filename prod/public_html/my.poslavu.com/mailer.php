<?php
require_once("/home/poslavu/public_html/admin/cp/resources/lavuquery.php");
/**
* Mass Mails Employees of a given dataname
*/
class EmployeeMailer{
	private $dataname = false;
	private $userList = false;
	private $lowestUserLevel = false;
	private $highestUserLevel = false;
	private $subject = 'Lavu Automailer';
	private $message = false;
	private $singleUserMode = false;
	private $from = "From: support@lavu.com <support@lavu.com>";

	function __construct($dataname){
		$this->dataname = $dataname;
	}

	private function getUserList(){
        lavu_connect_dn($this->dataname,true);
		$query = 'SELECT `username`,`email`,`access_level` FROM `users` WHERE `active` = 1 AND `_deleted` = 0 AND ( `username` LIKE "%@%" OR  `email` != "")';
		$result = lavu_query($query);
		if(empty($result)){return array();}
		$return = array();
		while($rows = mysqli_fetch_array($result, MYSQL_NUM)){
			$temp = array();
			if(empty($rows[1]) ){
				$temp['email'] = $rows[0];
			}else{
				$temp['email'] = $rows[1];
			}
			$temp['access_level'] = $rows[2];
			if($this->checkUser($temp)){
				$return[] = $temp;
			}
		}
		$this->userList = $return;
		return $return;
	}

	private function checkUser($user){
		if(false !== $this->lowestUserLevel){
			if($user['access_level'] < $this->lowestUserLevel ){
				return false;
			}
		}
		if(false !== $this->highestUserLevel){
			if($user['access_level'] > $this->highestUserLevel ){
				return false;
			}
		}
		return true;
	}

	private function mailUser($user){
		mail($user['email'],$this->subject,$this->message, $this->from);
	}

	public function mail($message){
		if(empty($message) ){return false;}
		$this->message = $message;
		if(!empty($this->singleUserMode) ){
			foreach ($this->userList as $user) {
				if($this->singleUserMode['email'] == $user['email']){
					//if a single user email is sent after a bulk mail
					//then don't send it provided the user was already included in the bulk mail
					return;
				}
			}
            if ($this->checkUser($this->singleUserMode)) {
                $this->mailUser($this->singleUserMode);
                return;
            }
		} else {
            $this->userList = array();
            $this->getUserList();
            foreach ($this->userList as $user) {
                $this->mailUser($user);
            }
        }
	}

	public function setSubject($input){
		$this->subject = $input;
		return true;
	}

	public function setMinLevel($input){
		$this->lowestUserLevel = $input;
		return true;
	}

	public function setMaxLevel($input){
		$this->highestUserLevel = $input;
		return true;
	}

	public function userName($input){
		$this->singleUserMode = array();
		$this->singleUserMode = $input;
		return true;
	}

}
?>
