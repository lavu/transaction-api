<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="distribution" content="global" />
	<meta name="robots" content="index, follow" />
	<meta name="ROBOTS" content="NOODP" />
	<meta name="GOOGLEBOT" content="NOODP" />
	<meta name="Slurp" content="NOODP" />
	<meta name="msnbot" content="NOODP" />
	<meta name="language" content="EN" />

	<meta name="subject" content="Point of Sale Takeout To Go" />

	<meta name="Classification" content="Point of Sale, iPad POS" />
	<meta http-equiv="Expires" content="never" />
	<meta http-equiv="PRAGMA" content="NO-CACHE" />

	<title>iPad POS | iPad Point of Sale Restaurant Takeout To Go</title>
	<meta name="Keywords" content="iPad POS, Point of Sale , POS iPad, Point, Sale, iPad, iPod Touch, iPhone, Restaurant, iPad Point of Sale, POS ipad, POS Lavu, iPad Point of Sale, Restaurant POS, iPads POS, iPads, Point of Sale System, Coffee Shop POS, Bar Point of Sale, Bar POS, Restaurant Point of Sale, " />
	<meta name="Description" content="POSlavu iPad POS System is the leader in iPad Point of sale revolution. Lavu ToGo is an online takeout program" />
	<meta name="subject" content="iPad POS" />
	<meta name="apple-itunes-app" content="app-id=391800399" />


	<link href="assets/styles/togo_welcome.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<table width="980" border="0" align="center" cellpadding="0" cellspacing="0" id="homepage">
		<tr>
			<td height="20" align="center" valign="top"><img src="assets/images/colorbar.png" width="980" height="8" /></td>
		</tr>
		<tr>
			<td align="center" valign="top">
				<table width="900" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="400" align="center"><img src="assets/images/logo_togo.png" width="260" height="130" alt="Lavu ToGo" /></td>
						<td width="30">&nbsp;</td>
						<td align="right" class="bannertext">Online Ordering Made Easy</td>
					</tr>
				</table>
				<table width="900" height="10" border="0" cellspacing="0" cellpadding="0" background="assets/images/line_h.png">
					<tr>
						<td width="400" align="center"></td>
					</tr>
				</table>
				<div id="banner_home"></div>
				<table width="900" height="10" border="0" cellspacing="0" cellpadding="0" background="assets/images/line_h.png">
					<tr>
						<td width="400" align="center"></td>
					</tr>
				</table>
				<table width="900" border="0" cellpadding="0" cellspacing="0" id="banner_table">
					<tr>
						<td height="23"><img src="assets/images/num1.png" width="23" height="23" /></td>
						<td width="270"><h1>ONLINE ORDER PLACED</h1></td>
						<td></td>
						<td><img src="assets/images/num2.png" width="23" height="23" /></td>
						<td width="270"><h1>ORDER RECEIVED</h1></td>
						<td></td>
						<td><img src="assets/images/num3.png" width="23" height="23" /></td>
						<td width="240"><h1>READY for PICK UP</h1></td>
					</tr>
					<tr>
						<td style="text-align:right">- <br />
							- <br /> - <br />
						</td>
						<td valign="top">customers browse your online menu <br />
							they select Items and modify to their taste<br />
							select a time for pickup
						</td>
						<td width="15">&nbsp;</td>
						<td style="text-align:right">- <br />
							- <br /> - <br />
						</td>
						<td valign="top">you are notified that an order has been placed<br />
							order waits until prep time occurs<br />
							order sent to kitchen
						</td>
						<td width="15">&nbsp;</td>
						<td style="text-align:right">- <br />
							- <br />
							<br />
						</td>
						<td valign="top">order complete and ready ToGo <br />
							customer email on file for promos
						</td>
					</tr>
				</table>
				<div style="height:40px" ></div>
				<h2>Easy to Get Set Up</h2>
				<table width="900" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td height="20" align="center" valign="top">
							We have made it as easy as possible to get started with Lavu ToGo.<br />
							All you need to do to embed the menu is paste 1 line of code into your current webpage, or you can link to your custom menu URL (web address).<br />
							The intuitive menu system is ready for browsing and taking orders.
						</td>
					</tr>
				</table>
				<div style="height:40px" ></div>
				<div class="lineshade"></div>
				<div style="height:40px" ></div>
				<h2>Do you already use POS Lavu?</h2>
				<table width="900" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td height="20" align="center" valign="top">It doesn’t get much easier to set up Lavu ToGo if you already use POSLavu point of sale:</td>
					</tr>
				</table>
				<br />
				<table width="900" border="0" cellpadding="0" cellspacing="0" id="subtable_wrap"	>
					<tr>
						<td height="208" align="center" valign="top">
							<table width="780" border="0" cellpadding="0" cellspacing="0" id="subtable">
								<tr>
									<td width="178" align="center" valign="top">&nbsp;</td>
									<td width="23" align="center" valign="middle">&nbsp;</td>
									<td width="178" align="center" valign="top"><img src="assets/images/i_signup.png" width="61" height="100" alt="LavuToGo Signup" /></td>
									<td width="22" align="center" valign="middle"><img src="assets/images/plus.png" width="18" height="18" /></td>
									<td width="178" align="center" valign="top"><img src="assets/images/i_embed.png" width="67" height="100" alt="LavuToGo embed code" /></td>
									<td width="23" align="center" valign="middle">&nbsp;</td>
									<td width="178" align="center" valign="top">&nbsp;</td>
								</tr>
								<tr>
									<td align="center"><h2>&nbsp;</h2></td>
									<td>&nbsp;</td>
									<td align="center"><h2>Enable</h2></td>
									<td>&nbsp;</td>
									<td align="center"><h2>Embed</h2></td>
									<td>&nbsp;</td>
									<td align="center"><h2>&nbsp;</h2></td>
								</tr>
								<tr>
									<td align="center" valign="top">&nbsp;</td>
									<td></td>
									<td align="center" valign="top">Click one box in the Lavu ControlPanel</td>
									<td></td>
									<td align="center" valign="top">Copy and Paste one (1) line of code (which we provide) into your website</td>
									<td></td>
									<td align="center" valign="top">&nbsp;</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				<div class="lineshade"></div>
				<div style="height:60px" ></div>
				<!--
				<h2>Not yet a POS Lavu User?</h2>
				 <table width="900" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>

								</td>
							</tr>
							<tr>
								<td height="20" align="center" valign="top">To use Lavu ToGo if you are new to Lavu:</td>
							</tr>
						</table>
				<br />
				<table width="900" border="0" cellpadding="0" cellspacing="0" id="subtable_wrap" >
					<tr>
						<td height="208" align="center" valign="top"><table width="780" border="0" cellpadding="0" cellspacing="0" id="subtable">
							<tr>
								<td width="178" align="center" valign="top"><img src="assets/images/i_signup.png" width="61" height="100" alt="LavuToGo Signup" /></td>
								<td width="23" align="center" valign="middle"><img src="assets/images/plus.png" width="18" height="18" /></td>
								<td width="178" align="center" valign="top"><img src="assets/images/i_contract.png" width="67" height="100" alt="LavuToGo contract" /></td>
								<td width="22" align="center" valign="middle"><img src="assets/images/plus.png" width="18" height="18" /></td>
								<td width="178" align="center" valign="top"><img src="assets/images/i_embed.png" width="67" height="100" alt="LavuToGo embed code" /></td>
								<td width="23" align="center" valign="middle"><img src="assets/images/plus.png" width="18" height="18" /></td>
								<td width="178" align="center" valign="top"><img src="assets/images/i_setup.png" width="67" height="100" alt="Lavu to Go setup menu" /></td>
							</tr>
							<tr>
								<td align="center"><h2>Sign Up</h2></td>
								<td>&nbsp;</td>
								<td align="center"><h2>Contract</h2></td>
								<td>&nbsp;</td>
								<td align="center"><h2>Embed</h2></td>
								<td>&nbsp;</td>
								<td align="center"><h2>Setup</h2></td>
							</tr>
							<tr>
								<td align="center" valign="top">Register now for your Lavu ToGo account</td>
								<td></td>
								<td align="center" valign="top">Set up an account with a Merchant Service Provider so you can accept credit card payments</td>
								<td></td>
								<td align="center" valign="top">Copy and Paste one (1) line of code (which we provide) into your website</td>
								<td></td>
								<td align="center" valign="top">Enter your menu Items, prices, and upload images</td>
							</tr>
						</table></td>
					</tr>
				</table>

				<div class="lineshade"></div>

				<div style="height:60px" ></div>
				-->
				<h2>Not yet a POS Lavu user?</h2>
				<table width="900" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td height="20" align="center" valign="top">
							<p>There are many benefits to choosing Lavu for your point of sale.</p>
							<p>take a look at <a href="http://www.poslavu.com" target="_blank">POS Lavu</a></p>
						</td>
					</tr>
				</table>

				<h3>Lavu ToGo is currently available for Gold and Platinum level POS Lavu users only.</h3>
				<h3>Lavu ToGo is currently NOT compatible with Lavu Local Server.</h3>
				<div style="height:30px" ></div>
				<div class="lineshade"></div>
				<!--
				<table width="900" border="0" align="center" cellpadding="0" cellspacing="0">
					<tr>
						<td width="740" align="right" valign="middle">VIEW OUR SAMPLE WEBPAGE to see how the same Lavu ToGo interface works with a variety of different designs and styles...</td>
						<td width="20" align="center">&nbsp;</td>
						<td width="140" align="center"><a href="/sample3/menu.php"><img src="assets/images/lavutogo_t_screen.png" width="120" height="85" border="0" /></a></td>
					</tr>
				</table>
				-->
				<div style="height:60px" ></div>
			</td>
		</tr>
		<tr>
			<td align="center" valign="middle" bgcolor="#8f8f8f">
				<div id="footer">
					<a href="http://www.poslavu.com" target="_blank">POINT OF SALE SOLUTION</a> |
					<a href="http://www.poslavu.com/en/ipad-pos-signup">SIGN UP</a> |
					<a href="http://www.poslavu.com/en/ipad-pos-contact">CONTACT US</a>
				</div>
			</td>
		</tr>
	</table>

</body>