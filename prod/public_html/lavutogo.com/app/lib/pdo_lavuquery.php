<?php
date_default_timezone_set("America/Chicago");
$lavuPDOConnection = false;
$lavuAdminPDOConnection = false;
error_reporting(E_ALL);

function PDO_MLavuConnect($dbname = false){
	global $lavuAdminPDOConnection;
	if($lavuAdminPDOConnection == false){
		$lavuAdminPDOConnection = new PDO("mysql:host=localhost", 'root', 'tnk4zig');
		if ($lavuAdminPDOConnection->errorCode()) {
			trigger_error($lavuAdminPDOConnection->errorCode().print_r($lavuAdminPDOConnection->errorCode(),1) );
			return false;
		}
	}
	if($dbname !== false){
		return PDO_LavuConnectDB($dbname);
	}
	return true;
}

function PDO_MLavuConnectDB($dbname){
	global $lavuAdminPDOConnection;
	if($lavuAdminPDOConnection == false){
		PDO_MLavuConnect();
	}
	if(empty($dbname)){
		trigger_error('Database Name Empty');
		return false;
	}
	$stmt = $lavuAdminPDOConnection->query("USE {$dbname};");
	if($stmt == false){
		error_log($lavuAdminPDOConnection->errorCode().print_r($lavuAdminPDOConnection->errorCode(),1) );
		trigger_error($stmt->errorCode().print_r($stmt->errorCode(),1) );
		return false;
	}
	if($stmt->errorCode() != 0){
		trigger_error($stmt->errorCode().'|'.print_r($stmt->errorCode(),1) );
		return false;
	}
	if($lavuAdminPDOConnection->errorCode() != 0){
		trigger_error($lavuAdminPDOConnection->errorCode().'|'.print_r($lavuAdminPDOConnection->errorCode(),1) );
		return false;
	}
	return true;
}

function PDO_MLavuEncode($escapestr){
	global $lavuAdminPDOConnection;
	if($lavuAdminPDOConnection == false){
		PDO_MLavuConnect();
	}
	return $lavuAdminPDOConnection->quote($escapestr);
}

function PDO_MLavuQuery($queryString){
	global $lavuAdminPDOConnection;
	if($lavuAdminPDOConnection == false){
		PDO_MLavuConnect();
	}
	$stmt = $lavuAdminPDOConnection->query($queryString);
	if($stmt == false){
		error_log($lavuAdminPDOConnection->errorCode().print_r($lavuAdminPDOConnection->errorCode(),1) );
		trigger_error($stmt->errorCode().print_r($stmt->errorCode(),1) );
		return false;
	}
	if($stmt->errorCode() != 0){
		trigger_error($stmt->errorCode().'|'.print_r($stmt->errorCode(),1) );
		return false;
	}
	if($lavuAdminPDOConnection->errorCode() != 0){
		trigger_error($lavuAdminPDOConnection->errorCode().'|'.print_r($lavuAdminPDOConnection->errorCode(),1) );
		return false;
	}
	$toReturn = new PDO_LavuQueryStatement($stmt);
	PDO_QueryLogger($queryString,null,'MLAVU QUERY');
	return $toReturn;
}

function PDO_MLavuPrepare($queryString,$vars = false){
	global $lavuAdminPDOConnection;
	if($lavuAdminPDOConnection == false){
		PDO_MLavuConnect();
	}
	if(empty($vars)){
		return $lavuAdminPDOConnection->query($queryString);
	}
	$stmt = $lavuAdminPDOConnection->prepare($queryString);
	if($stmt == false){
		error_log('1'.$lavuAdminPDOConnection->errorCode().print_r($lavuAdminPDOConnection->errorCode(),1) );
		trigger_error('2'.$stmt->errorCode().print_r($stmt->errorCode(),1) );
		return false;
	}
	$stmt->execute($vars);
	if($stmt->errorCode() != 0){
		trigger_error('3'.$stmt->errorCode().'|'.print_r($stmt->errorCode(),1) );
		return false;
	}
	if($lavuAdminPDOConnection->errorCode() != 0){
		trigger_error('4'.$lavuAdminPDOConnection->errorCode().'|'.print_r($lavuAdminPDOConnection->errorCode(),1) );
		return false;
	}
	$toReturn = new PDO_LavuQueryStatement($stmt);
	PDO_QueryLogger($queryString,$vars,'MLAVU QUERY');
	return $toReturn;
}

function PDO_MLavuInsertID(){
	global $lavuAdminPDOConnection;
	if($lavuAdminPDOConnection == false){
		PDO_MLavuConnect();
	}
	return $lavuAdminPDOConnection->lastInsertId();
}

function PDO_MLavuClose(){
	#you probably shouldn't close mlavu's connection...
	return; 
	global $lavuAdminPDOConnection;
	$lavuAdminPDOConnection = false;
}

function PDO_MLavuGetError(){
	global $lavuAdminPDOConnection;
	if($lavuAdminPDOConnection == false){
		PDO_MLavuConnect();
	}
	$toReturn = array();
	$toReturn['ErrorCode'] = $lavuAdminPDOConnection->errorCode();
	$toReturn['ErrorInfo'] = $lavuAdminPDOConnection->errorInfo();
	return $toReturn;
}

function PDO_QueryLogger($queryString,$vars = null,$queryType = 'LAVU QUERY'){
	global $data_name;
	global $got_loc_id;
	if ($data_name) {
		$myPath = "/home/poslavu/logs/company/".$data_name;
		if (!is_dir($myPath)){mkdir($myPath,0775,true);}
		$dt = new DateTime();
		$dt->setTimezone(new DateTimeZone('America/Chicago') );
		$myDate = $dt->format('Y-m-d');
		$myTime = $dt->format('Y-m-d H:i:s');
		$log_file = "{$myPath}/mysql_{$myDate}.log";
		$loc_str = ($got_loc_id) ? "LOCATION: {$got_loc_id} - " :"";
		$log_string = "{$myTime} - MLAVU QUERY - {$loc_str} - {$queryString} - ";
		if(!empty($vars)){
			$log_string .= print_r($vars,1);
		}
		$log_string2 = str_replace(array("\r\n", "\n", "\r"), '\n', $log_string);
		file_put_contents ( $log_file, $log_string2, FILE_APPEND);
	}
}

class PDO_LavuQueryStatement{
	private $myStatement = false;

	public function __construct($statement){
		if(empty($statement)){
			trigger_error('Statement Empty');
		}
		if('PDOStatement' == get_class($statement)){
			$this->myStatement = $statement;
		}else{
			trigger_error('Statement class incorrect: '.get_class($statement));
		}
	}

	public function __toString(){
		if(false == $this->myStatement){
			return false;
		}
		return $this->myStatement->__toString();
	}

	public function closeCursor(){
		if(false == $this->myStatement){
			return false;
		}
		return $this->myStatement->closeCursor();
	}

	public function columnCount(){
		if(false == $this->myStatement){
			return false;
		}
		return $this->myStatement->columnCount();
	}

	public function errorCode(){
		if(false == $this->myStatement){
			return false;
		}
		return $this->myStatement->errorCode();
	}

	public function errorInfo(){
		if(false == $this->myStatement){
			return false;
		}
		return $this->myStatement->errorInfo();
	}

	public function execute($args){
		if(false == $this->myStatement){
			return false;
		}
		return $this->myStatement->execute($args);
	}

	public function fetch($type = 'BOTH'){
		if(false == $this->myStatement){
			return false;
		}
		switch ($type) {
			case 'BOTH':
				return $this->myStatement->fetch(PDO::FETCH_BOTH);
				break;
			case 'ASSOC':
				return $this->myStatement->fetch(PDO::FETCH_ASSOC);
				break;
			case 'NUMERIC':
				return $this->myStatement->fetch(PDO::FETCH_NUM);
				break;
			default:
				trigger_error('Invalid Fetch Type');
				return false;
				break;
		}
	}

	public function fetchAll($type = 'BOTH'){
		if(false == $this->myStatement){
			return false;
		}
		switch ($type) {
			case 'BOTH':
				return $this->myStatement->fetchAll(PDO::FETCH_BOTH);
				break;
			case 'ASSOC':
				return $this->myStatement->fetchAll(PDO::FETCH_ASSOC);
				break;
			case 'NUMERIC':
				return $this->myStatement->fetchAll(PDO::FETCH_NUM);
				break;
			default:
				trigger_error('Invalid Fetch Type');
				return false;
				break;
		}
	}

	public function rowCount(){
		if(false == $this->myStatement){
			return false;
		}
		return $this->myStatement->rowCount();
	}

	#function bindColumn(){}
	#function bindParam(){}
	#function bindValue(){}
	#function debugDumpParams(){}
	#function fetchColumn(){}
	#function fetchObject(){}
	#function getAttribute(){}
	#function getColumnMeta(){}
	#function nextRowset(){}
	#function setAttribute(){}
}

?>
