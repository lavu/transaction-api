<?php
	putenv("TZ=America/Chicago");
	date_default_timezone_set("America/Chicago");
	require_once(dirname(__FILE__) . "/lsk.php"); // lavu security key functions
	
	$gateway_log_path = "/home/poslavu/logs/company/";

	function resource_path()
	{
		return dirname(__FILE__);
	}
	
	function display_header($str)
	{
		echo "<div style='font-size:18px; color:#ffffff; background-color:#acacac; font-weight:bold; width:360px; text-align:center'>$str</div>";
	}
	
	//------------ time
	function display_date($d)
	{
		$dt = explode("-",$d);
		if(count($dt) > 2)
		{
			return $dt[1] . "/" . $dt[2] . "/" . $dt[0];
		}
		else return $d;
	}
	
	function display_time($t)
	{
		$m = $t % 100;
		$h = (($t - $m) / 100);
		if($m < 10) $m = "0" . ($m * 1);
		$a = "am";
		if($h==0 || $h==24)
		{
			$h = 12;
		}
		else if($h==12)
		{
			$a = "pm";
		}
		else if($h>24)
		{
			$h -= 24;
			$a = "am";
		}
		else if($h>12)
		{
			$h -= 12;
			$a = "pm";
		}
		return ($h . ":" . $m . " " . $a);
	}
	
	//--------------------------------- session / login functions -------------------------//
	function sessname($str)
	{
		return "lavutogo_234347273_" . $str;
	}
	
	function sessvar($str,$def=false)
	{
		if(isset($_SESSION[sessname($str)]))
			return $_SESSION[sessname($str)];
		else
			return $def;
	}
	
	function sessvar_isset($str)
	{
		return isset($_SESSION[sessname($str)]);
	}
	
	function set_sessvar($str,$val)
	{
		$_SESSION[sessname($str)] = $val;
	}
	
	function unset_sessvar($str){
		unset($_SESSION[sessname($str)]);
	}

	function get_requested_company_code() {
		if (isset($_REQUEST['cc'])) {
			$company_code = $_REQUEST['cc'];
		} else if (isset($_SESSION['company_code'])) {
			$company_code = $_SESSION['company_code'];
		} else {
			$company_code = "";
		}
		return $company_code;
	}
		
	function get_company_code_info($company_code)
	{
		$ccrt = lsecurity_pass($company_code);
		$company_code = $ccrt['cc'];
		$company_code_key = $ccrt['key'];
		if(isset($_POST['cc']))
		{
			$ccrt = lsecurity_pass($_POST['cc']);
			$_POST['cc'] = $ccrt['cc'];
		}
		if(isset($_GET['cc']))
		{
			$ccrt = lsecurity_pass($_GET['cc']);
			$_GET['cc'] = $ccrt['cc'];
		}
		if(isset($_REQUEST['cc']))
		{
			$ccrt = lsecurity_pass($_REQUEST['cc']);
			$_REQUEST['cc'] = $ccrt['cc'];
		}
		return $ccrt;
	}
		
	//---------------------------------- query / database functions ----------------------------//
	/*function integration_keystr() // placed in lsk.php to be shared by core_functions and info_inc
	{
		return "AIH3476ag7gaGAn84nbaZZZ48hah";
	}*/
	
	require_once(dirname(__FILE__) . "/lavuquery.php");
	
	//------------------------------ misc functions -------------------------------------------//
	function spaces($n)
	{
		$str = "";
		for($i=0; $i<$n; $i++)
			$str .= "&nbsp;";
		return $str;
	}
	
	function urlvar($str,$def=false)
	{
		if(isset($_GET[$str]))
			return $_GET[$str];
		else
			return $def;
	}
	
	function postvar($str,$def=false)
	{
		if(isset($_POST[$str]))
			return $_POST[$str];
		else
			return $def;
	}
	
	function reqvar($str,$def=false)
	{
		if(isset($_POST[$str]))
			return $_POST[$str];
		else if(isset($_GET[$str]))
			return $_GET[$str];
		else
			return $def;
	}
	
	function removeEmpty($old_array) 
	{
		$new_array = array();
		foreach ($old_array as $old) 
		{
			if ($old != "") $new_array[] = $old;
		}		
		return $new_array;
	}
	
	//------------------------------ user functions -------------------------------------------//
	
	function current_user() {
	
		$user = array();
		if ( sessvar('user') ) {
		
			$consumer_results = mlavu_query("SELECT * FROM `consumers` WHERE `profile_identifier` = '[1]'", sessvar('user'));
					
			if( mysqli_num_rows($consumer_results) )
				$user = mysqli_fetch_assoc($consumer_results);		
		}	
		
		return $user;
	}
	
	function user_signed_in() {
	
		if ( sessvar('user') ) 
			return true;
		
		return false;
	
	}
	
	function logout() {
	
		session_destroy();
	
	}

	
	//-------------------------- floating info window -----------------------------------//
	function transparency($opacity)
	{
		return "style='filter:alpha(opacity=".$opacity.");-moz-opacity:.".$opacity.";opacity:.".$opacity."'";
	}
	
	$dialog_iframe_size = array(555,448);
	function create_info_layer($dwidth=false, $dheight=false)
	{
		global $dialog_iframe_size;
		if($dwidth) $dialog_iframe_size[0] = $dwidth;
		if($dheight) $dialog_iframe_size[1] = $dheight;
		
		$str = "";
		$str .= "<style>";
		$str .= ".close_btn, .close_btn a:link, .close_btn a:visited, .close_btn a:hover {font-size:18px; color:#aa7777;} ";
		$str .= "</style>";
		
		$str .= "<div style='position:absolute; left:40px; top:0px; z-index:1240; visibility:hidden' id='info_window_bg' name='info_window_bg'>";
			$str .= "<table width='$dwidth' height='$dheight' id='info_area_bgtable' style='border:solid 2px #888888' bgcolor='#eeeebb' ".transparency(90).">";
			$str .= "<td align='center' valign='top'>";
			$str .= "&nbsp;";
			$str .= "</td>";
			$str .= "</table>";
		$str .= "</div>";
		
		$str .= "<div style='position:absolute; left:40px; top:0px; z-index:1280; visibility:hidden' id='info_window' name='info_window'>";
			$str .= "<table width='$dwidth' height='$dheight' id='info_area_table'>";
			$str .= "<td align='center' valign='top'>";
			$str .= "<table width='100%'><td width='100%' align='right' class='close_btn'><a onclick='info_visibility()' style='cursor:pointer; color:#444444'>close <b>X</b></a></td></table>";
			$str .= "<div id='info_area' name='info_area'>";
			$str .= "&nbsp;";
			$str .= "</div>";
			$str .= "</td>";
			$str .= "</table>";
		$str .= "</div>";
		$str .= "<script language='javascript' src='resources/info_window.js'></script>";
		$str .= "<script language='javascript'>";		
		$str .= "inventory_window_exists = true;";
		$str .= "window.onscroll = set_info_location;";
		$str .= "</script>";
		
		return $str;
	}
	
	//require_once(dirname(__FILE__) . "/../../lib/JSON.php");
		
	if (!function_exists('json_decode')) {
		// Custom JSON decode
		function json_decode($str, $output_steps=false)
		{
			if($str[0]=="[")
				$str = trim($str,"[]");
			if($str[0]=="{" && substr($str, -1)!="}")
				$str = trim($str,"{}");
			return json_decode_step($str, $output_steps);
		}
	}
	
	function json_decode_step($str, $output_steps)
	{	
		if ($output_steps) echo "<br><br>".$str;
	
		$in_quotes = false;
		$esc_next = false;
		$qt_name = "";
		$qt_content = "";
		$colon_found = false;
		$brackets = array();
		$bracket_contents = "";
		$output = "";
		$vars = array();
		
		for($i=0; $i<strlen($str); $i++)
		{
			$ch = $str[$i];
						
			if($in_quotes)
			{
				if($esc_next)
				{
					$esc_next = false;
					$qt_content .= $ch;
				}
				else if($ch=="\\")
				{
					$esc_next = true;
				}
				else if($ch=="\"")
				{
					$in_quotes = false;
					if(count($brackets) < 1)
					{
						if($colon_found)
						{
							$vars[$qt_name] = $qt_content;
							$qt_content = "";
						}
						else
						{
							$qt_name = $qt_content;
							//if ($qt_name == "config_settings" || $qt_name == "qp_titles" || $output_steps==1) echo "<br><br>".$qt_name;
							$qt_content = "";
						}
					}
					$output .= $ch;
				}
				else
				{
					$qt_content .= $ch;
					if ($output_steps) echo "<br>".$qt_content;
				}
			}
			else
			{
				if($ch=="["||$ch=="{")
				{
					if(count($brackets) < 1)
					{
						$output .= $ch;
						$bracket_contents = "";
					}
					array_push($brackets,$ch);
				}
				else if($ch=="]"||$ch=="}")
				{
					//if($qt_name=="modifiers") echo count($brackets) . ":" . $bracket_contents . "<br><br>";
					if(count($brackets) > 0)
						array_pop($brackets);
					if(count($brackets) < 1)
					{
						$os = false;
						if($colon_found)
						{
							//$vars[$qt_name] = "testing...";
							//if ($qt_name == "config_settings" || $qt_name == "qp_titles") { $os = true; echo "<br><br>".$bracket_contents; }
							$vars[$qt_name] = json_decode($bracket_contents, $os);
						}
						else
						{
							$bracket_contents = trim($bracket_contents,"{}");
							//if ($qt_name == "config_settings" || $qt_name == "qp_titles") { $os = true; echo "<br><br>".$bracket_contents; }
							$vars[] = json_decode($bracket_contents, $os);
						}
						$output .= $ch;
					}
				}
				else if($ch=="\"")
				{
					$in_quotes = true;
				}
				else if(count($brackets) > 0)
				{
				}
				else if($ch==":")
				{
					$colon_found = true;
				}
				else if($ch==",")
				{
					if ($qt_name != "" && !$colon_found) $vars[] = $qt_name;
					$qt_name = "";
					$qt_content = "";
					$colon_found = false;
				}
				else
				{
					$output .= $ch;
				}
			}
			if(count($brackets) > 0)
			{
				$bracket_contents .= $ch;
			}
		}
		if ($qt_name != "" && !$colon_found) $vars[] = $qt_name;
		//if($colon_found)
		//{
			//echo $qt_name . ": " . $bracket_contents . "<br><br>";
		//}
		return $vars;
	}
	
	function display_decoded_json($tinfo)
	{
		echo "<table>";
		foreach($tinfo as $var => $val)
		{
			echo "<tr><td valign='top'>" . $var . "</td><td valign='top'>";
			if(is_array($val))
			{
				display_decoded_json($val);
			}
			else
				echo $val;
			echo "</td></tr>";;
		}
		echo "</table>";
	}
	
	// lavu togo functions
	
/*
	function sendInfoToLavuToGo($ltg_name, $loc_id, $company_name, $logo_img, $enabled) {

		$dn = admin_info('dataname');
		$lavu_code = lc_encode("lavu2go:$dn:".date("Ymd"));
		$j_key = lc_encode(lsecurity_name($dn, admin_info('companyid')));
	
		$vars = "lc=$lavu_code&data_name=$dn&j_key=$j_key&ltg_name=$ltg_name&location_id=$loc_id&company_name=$company_name&logo_img=$logo_img&enabled=$enabled";
	
		//mail("richard@greenkeyconcepts.com","TEST",$vars);
	
		$url = 'https://lavutogo.com/app/company_info_api.php';
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $vars);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$response = curl_exec($ch);
		curl_close ($ch);
	}
*/

	function lc_encode($data)
	{
		$key_access = array(0, 2, 3, 7, 9);
		$text_string = $data;
		$key = array(rand(1,8),rand(1,8),rand(1,8),rand(1,8),rand(1,8),rand(1,8),rand(1,8),rand(1,8),rand(1,8),rand(1,8));
		$return_number = "";
		for ($c = 0; $c < strlen($data); $c++)
		{
			$code = ord($data[$c]);
			$mult = $key[$key_access[($c % 5)]];
			$return_number .= str_pad(($mult * $code), 4, "0", STR_PAD_LEFT);
		}
	
		return implode("", $key).$return_number;
	}
	
	function lc_decode($data)
	{
		if (empty($data))
		{
			return "";
		}
	
		$key_access = array(0, 2, 3, 7, 9);
		$ecodes = array();
		$dchars = array();		
		$number_string = $data;
		$key = substr($number_string, 0, 10);
		$edata = substr($number_string, 10, (strlen($number_string) - 8));
		for ($g = 0; $g < floor(strlen($edata) / 4); $g++)
		{
			$dchars[] = chr(((int)substr($edata, ($g * 4), 4) / $key[$key_access[($g % 5)]]));
		}

		return join("", $dchars);
	}
	
	// form functions
	function alter_properties_to_assoc($field_props)
	{
		if($field_props!="") // alter properties from comma delimited list into associative array
		{
			$props = explode(",",$field_props);
			$field_props = array();
			for($n=0; $n<count($props); $n++)
			{
				$prop_parts = explode(":",str_replace("::","[colon]",$props[$n]));
				$prop_key = trim($prop_parts[0]);
				if(count($prop_parts) > 1) $prop_val = trim($prop_parts[1]);
				else $prop_val = $prop_key;
				
				$field_props[$prop_key] = str_replace("[colon]",":",$prop_val);
			}
		}
		else $field_props = array();
		return $field_props;
	}
	
	class FormField {
		public $title;
		public $name;
		public $type;
		public $props;
		public $props_core;
		public $help_link;
		public $value;
		public $db_include;
		public $encode_field;
		
		public function __construct($field_info) {
			$this->title = $field_info[0];
			$this->name = $field_info[1];
			$this->type = $field_info[2];
			if(!isset($field_info[3])) $field_info[3] = array();
			if(is_array($field_info[3]))
			{
				$this->props = $field_info[3];
			}
			else
			{
				$this->props = alter_properties_to_assoc($field_info[3]);
			}
			if(!isset($field_info[4])) $field_info[4] = false;
			if(is_array($field_info[4]))
			{
				$this->props_core = $field_info[4];
			}
			else
			{
				$this->props_core = alter_properties_to_assoc($field_info[4]);
			}
			foreach($this->props_core as $key => $val)
			{
				$this->$key = $val;
			}
			if (!isset($field_info[5])) $field_info[5] = "";
			$this->help_link = $field_info[5];
		}
	}
	
	if (!function_exists('json_encode')) {

		function json_encode($a=false) {
			if (is_null($a)) return 'null';
			if ($a === false) return 'false';
			if ($a === true) return 'true';
			if (is_scalar($a)) {
				if (is_float($a)) {
					// Always use "." for floats.
					return floatval(str_replace(",", ".", strval($a)));
				}
	
				if (is_string($a)) {
					static $jsonReplaces = array(array("\\", "/", "\n", "\t", "\r", "\b", "\f", '"'), array('\\\\', '\\/', '\\n', '\\t', '\\r', '\\b', '\\f', '\"'));
					return '"' . str_replace($jsonReplaces[0], $jsonReplaces[1], $a) . '"';
				} else {
					return $a;
				}
			}
			$isList = true;
	
			for ($i = 0, reset($a); $i < count($a); $i++, next($a)) {
				if (key($a) !== $i) {
					$isList = false;
					break;
				}
			}
			$result = array();
	
			if ($isList) {
				foreach ($a as $v) $result[] = json_encode($v);
				return '[' . join(',', $result) . ']';
			} else {
				foreach ($a as $k => $v) $result[] = json_encode($k).':'.json_encode($v);
				return '{' . join(',', $result) . '}';
			}
		}
	}
	
	function create_datepicker($name,$setdate,$onselect)
	{
		echo "<script type=\"text/javascript\" src=\"resources/datepickercontrol.js\"></script>";
		echo "<link type=\"text/css\" rel=\"stylesheet\" href=\"resources/datepickercontrol.css\">";
		echo "<script language='javascript'>";
		echo "DatePickerControl.onSelect = function(inputid) { ";
		echo "  " . str_replace("[value]","document.getElementById(inputid).value",$onselect);
		echo "} ";
		echo "</script>";
						
		$set_date_start = $setdate;
		$date_start_ts = mktime(0,0,0,substr($set_date_start,5,2),substr($set_date_start,8,2),substr($set_date_start,0,4));
		
		echo "<input type=\"hidden\" id=\"DPC_TODAY_TEXT\" value=\"today\">";
		echo "<input type=\"hidden\" id=\"DPC_BUTTON_TITLE\" value=\"Open calendar...\">";
		echo "<input type=\"hidden\" id=\"DPC_MONTH_NAMES\" value=\"['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']\">";
		echo "<input type=\"hidden\" id=\"DPC_DAY_NAMES\" value=\"['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat']\">";
		echo "<input type=\"text\" name=\"$name\" id=\"$name\" datepicker=\"true\" datepicker_format=\"YYYY-MM-DD\" value=\"".date("m/d/Y",$date_start_ts)."\">";
	}
	
	function current_ts($min_offset=0)
	{
		$hour = date("H") - 2;
		$min = date("i") + $min_offset;
		$sec = date("s");
		$mon = date("m");
		$day = date("d");
		$year = date("Y");
		return mktime($hour,$min,$sec,$mon,$day,$year);
	}
	
	function update_api_access_log($company_dataname,$set_location)
	{
		$qinfo = array();
		$qinfo['ipaddress'] = $_SERVER['REMOTE_ADDR'];
		$qinfo['dataname'] = $company_dataname;
		$qinfo['time'] = date("Y-m-d H:i:s");
		$qinfo['location'] = $set_location;
		mlavu_query("insert into `poslavu_MAIN_db`.`api_access_log` (`ipaddress`,`dataname`,`time`,`location`) values ('[ipaddress]','[dataname]','[time]', '[location]')",$qinfo);
	}
	
	function deBug($output) {
		mail("richard@greenkeyconcepts.com", "debug", $output);
	}
	
	function getFieldInfo($known_info, $known_field, $tablename, $get_field) {
		if (($tablename == "menus") && ($known_field == "id") && ($known_info == 0)) {
			return "No menu selected...";
		}
		
		$field_info = "";
	
		$get_field_info = lavu_query("SELECT $get_field FROM $tablename WHERE $known_field = '$known_info' LIMIT 1");
		$extract = mysqli_fetch_assoc($get_field_info);
		$field_info = $extract[$get_field];
	
		return $field_info;
	}

	function assignNext($field, $tablename, $where_field, $where_value) {
		$get_last = lavu_query("SELECT $field FROM $tablename WHERE $where_field = '$where_value' ORDER BY $field * 1 DESC LIMIT 1");
		if (@mysqli_num_rows($get_last) > 0) {
			$extract = mysqli_fetch_assoc($get_last);
			$next_value = $extract[$field] + 1;
		} else {
			$next_value = 1;
		}
		
		return $next_value;
	}
	
	function assignNextWithPrefix($field, $tablename, $where_field, $where_value, $prefix) {
		$n = strlen($prefix);
		$get_last = lavu_query("SELECT `$field` FROM `$tablename` WHERE `$where_field` = '$where_value' AND LEFT ($field, $n) = '$prefix' ORDER BY SUBSTR($field, ($n + 1)) * 1 DESC LIMIT 1");
		if (@mysqli_num_rows($get_last) > 0) {
			$extract = mysqli_fetch_assoc($get_last);
			$next_value = $prefix.((substr($extract[$field], $n) * 1) + 1);
		} else {
			if (strlen($prefix) >= 3) $next_value = $prefix."1";
			else $next_value = $prefix.assignNext($field, $tablename, $where_field, $where_value);
		}
		
		return $next_value;
	}

	function get_integration_from_location($locid, $use_db="") {
		if($use_db&&$use_db!="") $use_db = "`$use_db`.";
		$get_integration_settings = mlavu_query("SELECT AES_DECRYPT(`integration1`,'".integration_keystr()."') as integration1, AES_DECRYPT(`integration2`,'".integration_keystr()."') as integration2, AES_DECRYPT(`integration3`,'".integration_keystr()."') as integration3, AES_DECRYPT(`integration4`,'".integration_keystr()."') as integration4, `gateway`, `cc_transtype` from ".$use_db."`locations` where id='[1]'", $locid);
		if(mysqli_num_rows($get_integration_settings))
		{
			$integration_extract = mysqli_fetch_assoc($get_integration_settings);
		}
		else $integration_extract = false;
		return $integration_extract;
	}

	function get_location_info($data_name, $loc_id) {
		$get_location_info = lavu_query("SELECT * FROM `poslavu_".$data_name."_db`.`locations` WHERE `id` = '[1]'", $loc_id);
		if (mysqli_num_rows($get_location_info) > 0) {
			$location_info = mysqli_fetch_assoc($get_location_info);
			return $location_info;
		} else {
			return false;
		}
	}
	
	function checkUsernameAvailability($username, $id) {
	
		global $company_info;
	
		$username_established = FALSE;
		$new_username = $username;
		$append_number = 0;
		$filter = "";
	
		if ($id != "") {
			$filter = " AND id != '".$id."' AND company_code != '".$_SESSION['company_code']."'";
		}
	
		while ($username_established == FALSE) {
			$found = FALSE;
			mysql_select_db("poslavu_MAIN_db");
			$get_data_names = mysql_query("SELECT * FROM restaurants");
			if (@mysqli_num_rows($get_data_names) > 0) {
				while ($c_info = mysqli_fetch_array($get_data_names)) {
					mysql_select_db("poslavu_".$c_info['data_name']."_db");
					$check_usernames = mysql_query("SELECT * FROM users WHERE username = '".$new_username."'".$filter);
					if (@mysqli_num_rows($check_usernames) > 0) {
						$found = TRUE;
						$append_number++;
						$new_username = $username.$append_number;
						break;
					}
				}
				if ($found == FALSE) {
					$username_established = TRUE;
				}
			}
		}
	
		mysql_select_db("poslavu_".$company_info['data_name']."_db");
	
		return $new_username;
	}
	
	function JS_alert($message) {
		echo "<script language='javascript'>alert(\"".str_replace('"','&quot;',$message)."\");</script>";
	}

	function write_log_entry($type, $info1, $info2) {
	
		global $gateway_log_path;

		if ($type == "gateway") {
		
			$log_path = $gateway_log_path;
		
			$log_file = fopen($gateway_log_path.$info1."/gateway_".date("Y-m-d").".log", "a");
			fputs($log_file, $info2);
			fclose($log_file);
	
		}
	}

	function UTCDateTime($add_z, $ts="")
	{
		if ($ts=="")
		{
			$ts = time();
		}

		$utc_tz = new DateTimeZone('UTC');
		$datetime = new DateTime(NULL, $utc_tz);
		$datetime->setTimestamp($ts);

		$datetime_str = $datetime->format("Y-m-d H:i:s");
		if ($add_z)
		{
			$datetime_str .= "Z";
		}

		return $datetime_str;
	}
	
	function localize_datetime($timestamp, $to_tz) {
		$old_tz = date_default_timezone_get();
		$unix_time = strtotime($timestamp);
		if (!empty($to_tz)){
			date_default_timezone_set($to_tz);
		}
		$result = strftime("%Y-%m-%d %H:%M:%S", $unix_time);
		date_default_timezone_set($old_tz);
		return $result;
	}

	function localize_date($timestamp, $to_tz) {
		$old_tz = date_default_timezone_get();
		$unix_time = strtotime($timestamp);
		if (!empty($to_tz)){
			date_default_timezone_set($to_tz);
		}
		$result = strftime("%Y-%m-%d", $unix_time);
		date_default_timezone_set($old_tz);
		return $result;
	}
	
	function timize($time_string) {
	
		$h = substr($time_string, 0, -2);
		$m = substr($time_string, -2);
		$ap = "am";
		if ((int)$h > 12) { $h -= 12; $ap = "pm"; }
		if ((int)$h == 0) { $h = 12; }
		
		return $h.":".$m.$ap;
	}

	function json_escape($string) {
		static $jsonReplaces = array(array("\\", "/", "\n", "\t", "\r", "\b", "\f", '"'), array('\\\\', '\\/', '\\n', '\\t', '\\r', '\\b', '\\f', '\"'));
		return str_replace($jsonReplaces[0], $jsonReplaces[1], $string);
	}

	function lavu_echo($text, $close_db=true, $exit=true, $flush=false)
	{
		if ($close_db)
		{
			mlavu_close_db();
			lavu_close_db();
		}

		echo $text;

		if ($flush)
		{
			flush();
		}

		if ($exit)
		{
			lavu_exit();
		}
	}

	function lavu_exit()
	{
		global $mConn;
		global $Conn;

		$rtype = is_resource($mConn)?get_resource_type($mConn):"";
		if (strpos($rtype, "mysql")) mysqli_close($mConn);

		$rtype = is_resource($Conn)?get_resource_type($Conn):"";
		if (strpos($rtype, "mysql")) mysqli_close($Conn);

		exit();
	}
	
	function determineDeviceIdentifier($req) {
	
		$identifier = "";
		if (isset($req['UDID'])) $identifier = $req['UDID'];
		else if (isset($req['MAC'])) $identifier = $req['MAC'];
		else if (isset($req['UUID'])) $identifier = $req['UUID'];
	
		return $identifier;
	}

	function isLLS($location_info)
	{
		if ($location_info == NULL)
		{
			return FALSE;
		}

		if (!is_array($location_info))
		{
			return FALSE;
		}

		$use_net_path = (int)$location_info['use_net_path'];
		if ($use_net_path <= 0)
		{
			return FALSE;
		}

		$net_path = $location_info['net_path'];
		if (strlen($net_path) == 0)
		{
			return FALSE;
		}

		if (isset($location_info['net_path_is_lls']))
		{
			return ((int)$location_info['net_path_is_lls'] == 1);
		}

		return (!strstr($net_path, "lavu") || strstr($net_path, "lsvpn"));
	}

	function ltgDebug($filepath, $message, $force=false)
	{
		global $ltg_debug;
		global $data_name;
		global $lavu_req_id;

		if (!$ltg_debug && !$force)
		{
			return;
		}

		if (empty($data_name))
		{
			$data_name = "<no data_name>";
		}

		if (empty($lavu_req_id))
		{
			$lavu_req_id = "S".lavuRequestID();
			error_log("LTG - ".$data_name." - assigned req_id: ".$lavu_req_id);
		}

		$fp_parts = explode("/", $filepath);
		$fp_count = count($fp_parts);
		$filename = $fp_parts[($fp_count - 1)];

		error_log("LTG - ".$data_name." - req_id: ".$lavu_req_id." - ".$filename." - ".$message);
	}

	function lavuRequestID()
	{
		return str_pad((string)rand(1, 9999), 4, "0", STR_PAD_LEFT);
	}

	function reliableRemoteIP()
	{
		return (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))?$_SERVER['HTTP_X_FORWARDED_FOR']:$_SERVER['REMOTE_ADDR'];
	}
?>