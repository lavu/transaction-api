<?php
	date_default_timezone_set("America/Chicago");
	$Conn = false;
	$mConn = false;
	//date_default_timezone_set('America/Chicago');
	function lavu_connect_shard_db($data_name=false,$setdb=false)
	{
		global $Conn;
		$letter = substr($data_name,0,1);
		if(ord($letter) >= ord("a") && ord($letter) <= ord("z"))
			$letter = $letter;
		else
			$letter = "OTHER";
		$tablename = "rest_" . $letter;

		$rest_query = mlavu_query("select `companyid`,`data_name`, AES_DECRYPT(`data_access`,'lavupass') as `data_access` from `poslavu_MAINREST_db`.`$tablename` where `data_name`='[1]'",$data_name);
		if(mysqli_num_rows($rest_query))
		{
			$rest_read = mysqli_fetch_assoc($rest_query);
			$companyid = $rest_read['companyid'];
			
			//10.130.216.67
			if($rest_read['data_name']=="17edison")
			{
				//echo "17edison<br>";
				$Conn = mysqli_connect("10.130.216.67", "poslavu", "A8n8d8y8", TRUE);
			}
			else
				$Conn = mysqli_connect("localhost", "usr_pl_".$companyid, $rest_read['data_access'], TRUE);
			if($setdb)
				mysqli_select_db($setdb,$Conn);
		}
	}
	
	function lavu_connect_db($companyid=false,$setdb=false)
	{
		global $Conn;
		$rest_query = mlavu_query("select `id`,`data_name`, AES_DECRYPT(`data_access`,'lavupass') as `data_access` from `poslavu_MAIN_db`.`restaurants` where `id`='[1]'",$companyid);
		if(mysqli_num_rows($rest_query))
		{
			//$rest_read = mysqli_fetch_assoc($rest_query);
			//$str .= "<td>" . $rest_read['data_name'] . "</td>";
			//$str .= "<td>" . $rest_read['data_access'] . "</td>";
			
			$rest_read = mysqli_fetch_assoc($rest_query);
			if($rest_read['data_name']=="17edison")
			{
				//echo "17edison<br>";
				$Conn = mysqli_connect("10.130.216.67", "poslavu", "A8n8d8y8", TRUE);
			}
			else
				$Conn = mysqli_connect("localhost", "usr_pl_".$companyid, $rest_read['data_access'], TRUE);
			if($setdb)
				mysqli_select_db($setdb,$Conn);
		}
	}

	function lavu_close_db()
	{
		global $Conn;

		if ($Conn)
		{
			mysqli_close($Conn);
		}

		$Conn = false;
	}

	function lavu_select_db($setdb)
	{
		global $Conn;
		mysqli_select_db($setdb,$Conn);
	}
	
	function lavu_query_encode($str)
	{
		return mysqli_real_escape_string($str);
		//return str_replace("'","''",$str);
	}
	
	function lavu_insert_id()
	{
		global $Conn;
		return mysqli_insert_id($Conn);
	}
	
	function lavu_query($query,$var1="[_empty_]",$var2="[_empty_]",$var3="[_empty_]",$var4="[_empty_]",$var5="[_empty_]",$var6="[_empty_]")
	{
		
		global $Conn;
		global $data_name;
		global $got_loc_id;
		global $cc_companyid;
		global $rdb;
		
		$debug_reconnect = "";
		$send_debug = false;
		if ($data_name) $rdb = "poslavu_".$data_name."_db";
		
		if (!$Conn || !mysqli_ping($Conn)) {
			$debug_reconnect = "cc_companyid: $cc_companyid - rdb: $rdb";
			usleep(500);
			//lavu_connect_db($cc_companyid, $rdb);
			lavu_connect_shard_db($data_name, "poslavu_".$data_name."_db");
			if (!$Conn || !mysqli_ping($Conn)) $debug_reconnect .= "\n\n".mysqli_error();
			if (strstr($debug_reconnect, "Too many connections")) $send_debug = true;
		}

		if($var1!="[_empty_]" && is_array($var1))
		{
			foreach($var1 as $key => $val)
			{
				$query = str_replace("[$key]",lavu_query_encode($val),$query);
			}
		}
		else
		{
			if($var1!="[_empty_]") $query = str_replace("[1]","[-__1__-]",$query);
			if($var2!="[_empty_]") $query = str_replace("[2]","[-__2__-]",$query);
			if($var3!="[_empty_]") $query = str_replace("[3]","[-__3__-]",$query);
			if($var4!="[_empty_]") $query = str_replace("[4]","[-__4__-]",$query);
			if($var5!="[_empty_]") $query = str_replace("[5]","[-__5__-]",$query);
			if($var6!="[_empty_]") $query = str_replace("[6]","[-__6__-]",$query);

			if($var1!="[_empty_]") $query = str_replace("[-__1__-]",lavu_query_encode($var1),$query);
			if($var2!="[_empty_]") $query = str_replace("[-__2__-]",lavu_query_encode($var2),$query);
			if($var3!="[_empty_]") $query = str_replace("[-__3__-]",lavu_query_encode($var3),$query);
			if($var4!="[_empty_]") $query = str_replace("[-__4__-]",lavu_query_encode($var4),$query);
			if($var5!="[_empty_]") $query = str_replace("[-__5__-]",lavu_query_encode($var5),$query);
			if($var6!="[_empty_]") $query = str_replace("[-__6__-]",lavu_query_encode($var6),$query);
		}
		
		$loc_str = "";
		if ($got_loc_id) {
			$loc_str .= "LOCATION: ".$got_loc_id." - ";
		}
		
		//echo $query . "<br>";
		$result_string = "{SUCCEEDED}";
		$result = mysqli_query($query, $Conn);
		if (!$result) $result_string = "{FAILED}\n".mysqli_error();

		if ($send_debug != "") mail("richard@greenkeyconcepts.com,sal@lavu.com", "lavu_query reconnect (Lavu ToGo)", $debug_reconnect."\n\n".$query."\n\n".$result_string);

		if ($data_name) {
			$log_file = fopen("/home/poslavu/logs/company/".$data_name."/mysqli_".date("Y-m-d").".log", "a");
			$log_string = date("Y-m-d H:i:s", time())." - ".$loc_str.$query." $result_string\n\n";
			fputs($log_file, $log_string);
			fclose($log_file);
		}			
			
		return $result;
	}	
	
	//--------------------------------------------------------------------------------//
	function mlavu_connect_db()
	{
		global $mConn;
		$mConn = @mysqli_connect("localhost", "root", "tnk4zig", TRUE);
		//mysqli_select_db("poslavu_MAIN_db",$Conn);
	}
	
	function mlavu_close_db()
	{
		global $mConn;

		if ($mConn)
		{
			mysqli_close($mConn);
		}

		$mConn = false;
	}
	
	function mlavu_select_db($setdb)
	{
		global $mConn;
		mysqli_select_db($setdb,$mConn);
	}
	
	function mlavu_query_encode($str)
	{
		return mysqli_real_escape_string($str);
		//return str_replace("'","''",$str);
	}
	
	function mlavu_insert_id()
	{
		global $mConn;
		return mysqli_insert_id($mConn);
	}
	
	function mlavu_query($query,$var1="[_empty_]",$var2="[_empty_]",$var3="[_empty_]",$var4="[_empty_]",$var5="[_empty_]",$var6="[_empty_]")
	{
		global $mConn;
		global $data_name;
		global $got_loc_id;
		
		$debug_reconnect = "";
		$send_debug = false;
		
		if (!$mConn || !mysqli_ping($mConn)) {
			$debug_reconnect = "data_name: $data_name";
			usleep(500);
			mlavu_connect_db();
			mlavu_select_db("lavutogo");
			if (!$mConn || !mysqli_ping($mConn)) $debug_reconnect .= "\n\n".mysqli_error();
			if (strstr($debug_reconnect, "Too many connections")) $send_debug = true;
		}
		
		if($var1!="[_empty_]" && is_array($var1))
		{
			foreach($var1 as $key => $val)
			{
				$query = str_replace("[$key]",mlavu_query_encode($val),$query);
			}
		}
		else
		{
			if($var1!="[_empty_]") $query = str_replace("[1]",mlavu_query_encode($var1),$query);
			if($var2!="[_empty_]") $query = str_replace("[2]",mlavu_query_encode($var2),$query);
			if($var3!="[_empty_]") $query = str_replace("[3]",mlavu_query_encode($var3),$query);
			if($var4!="[_empty_]") $query = str_replace("[4]",mlavu_query_encode($var4),$query);
			if($var5!="[_empty_]") $query = str_replace("[5]",mlavu_query_encode($var5),$query);
			if($var6!="[_empty_]") $query = str_replace("[6]",mlavu_query_encode($var6),$query);
		}
		
		$loc_str = "";
		if ($got_loc_id) $loc_str .= "LOCATION: ".$got_loc_id." - ";

		//echo $query . "<br>";
		$result_string = "{SUCCEEDED}";
		$result = mysqli_query($query, $mConn);
		if (!$result) $result_string = "{FAILED}\n".mysqli_error();

		//if ($send_debug) mail("sal@lavu.com", "mlavu_query reconnect", $debug_reconnect."\n\n".$query."\n\n".$result_string);

		if ($data_name) {
			if (!is_dir("/home/poslavu/logs/company/".$data_name)) mkdir("/home/poslavu/logs/company/".$data_name,0755);
			$log_file = fopen("/home/poslavu/logs/company/".$data_name."/mysqli_".date("Y-m-d").".log", "a");
			$log_string = date("Y-m-d H:i:s", time())." - MLAVU QUERY - ".$loc_str.stripslashes($query)." $result_string\n\n";
			fputs($log_file, $log_string);
			fclose($log_file);
		}

		return $result;
	}

	function debug_mlavu_query($query,$var1=false,$var2=false,$var3=false,$var4=false)
	{
		global $mConn;
		if(is_array($var1))
		{
			foreach($var1 as $key => $val)
			{
				$query = str_replace("[$key]",mlavu_query_encode($val),$query);
			}
		}
		else
		{
			if($var1) $query = str_replace("[1]",mlavu_query_encode($var1),$query);
			if($var2) $query = str_replace("[2]",mlavu_query_encode($var2),$query);
			if($var3) $query = str_replace("[3]",mlavu_query_encode($var3),$query);
			if($var4) $query = str_replace("[4]",mlavu_query_encode($var4),$query);
		}
		
		$result = mysqli_query($query,$mConn);
		return "query: " . $query;
	}

	function buildInsertFieldsAndValues($vars, &$fields, &$values)
	{
		$fields = "";
		$values = "";

		$keys = array_keys($vars);
		foreach ($keys as $key)
		{
			if ($fields != "")
			{
				$fields .= ", ";
				$values .= ", ";
			}
			$fields .= "`".$key."`";
			$values .= "'[".$key."]'";
		}
	}

	function buildUpdate($vars, &$update)
	{
		$update = "";

		$keys = array_keys($vars);
		foreach ($keys as $key)
		{
			if ($update != "")
			{
				$update .= ", ";
			}
			$update .= "`".$key."` = '[".$key."]'";
		}
	}
?>