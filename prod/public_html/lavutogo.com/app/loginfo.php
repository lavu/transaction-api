<?php

	class LavuCardInformation{
		public
		$card_number,
		$card_year,
		$card_month,
		$card_code,
		$card_amount;
	}
	
	class LavuOrderPayments{
		public
		$approved,
		$pnref,
		$cn_info,
		$message,
		$auth_code,
		$card_type,
		$order_id;
		
		public
		$card_information;
		
		public function __construct(){
			$this->card_information = new LavuCardInformation();
		}
	}
	
	class LavuCustomerInfo{
		public
		$first_name,
		$last_name,
		$phone_number,
		$email,
		$user_agent;
	}
	
	class LavuOrderContents{
		public
		$item,
		$price,
		$quantity,
		$options,
		$special,
		$modify_price,
		$print,
		$check_number = 1,
		$seat_number = 1,
		$item_id,
		$printer,
		$apply_taxrate,
		$custom_taxrate,
		$modifier_list_id,
		$forced_modifier_group_id,
		$forced_modifiers_price,
		$course_number = 1,
		$open_item = 0,
		$allow_deposit = 1,
		$void = 0,
		$device_time,
		$tax_inclusion,
		$sent = 0,
		$tax_exempt = 0,
		$checked_out = 0,
		$price_override = 0;
	}
	//"1|$new_pnref|".substr($cn, (strlen($cn) - 4), 4)."|$respmsg|$authcode|$card_type|||".$process_info['order_id']
	
	class LavuOrderInformation{
		public 
		$order_id,
		$cc,
		$loc_id,
		$subtotal,
		$total,
		$taxrate,
		$tax,
		$today_date,
		$togo_date,
		$togo_time,
		$ltg_name;
		
		public
		$customer_information,
		$order_contents_information;
		
		public function __construct(){
			$this->customer_information = new LavuCustomerInfo();
			$this->order_contents_information = array();
		}
	}
	
	class LavuDebugInfo{
		
	}
	
	function log_details($log_information, $order_id, $total, $consumer_id, $merchant_id, $log_row=""){
		$details = array( 
			//'transaction_information'  => json_encode($order_log_information),
			'order_id'                   => $order_id,
			'created_at'                 => 'now()',
			'amount'                     => $total,
			'consumer_id'                => $consumer_id,
			'merchant_id'                => $merchant_id,
			'description'                => ''
		);
		
		/// DETERMINE IF A LOG ENTRY FOR THIS ORDER ALREADY EXISTS
		if(empty($log_row)){
			$entry_exists = mlavu_query("SELECT * FROM `lavutogo`.`orders` WHERE `order_id` = '[order_id]' AND `merchant_id` = '[merchant_id]'", $details);
			if($entry_exists && mysqli_num_rows($entry_exists)){
				$log = mysqli_fetch_assoc($entry_exists);
				$log_row = $log['id'];
				mysqli_free_result($entry_exists);
			}
		}
		
		$target = "";
		
		if($log_information instanceof LavuOrderPayments){
			$details['transaction_information'] = json_encode($log_information);
			$target = "`transaction_information`='[transaction_information]', ";
		}
		else if($log_information instanceof LavuOrderInformation){
			$details['order_information'] = json_encode($log_information);
			$target = "`order_information`='[order_information]', ";
		}
		else if($log_information instanceof LavuDebugInfo){
			
			/**
			 * Recover Previous Log Information
			 **/
			$debug_information = mlavu_query("SELECT `debug_information` FROM `lavutogo`.`orders` WHERE `id`='[1]'", $log_row);
			if($debug_information && mysqli_num_rows($debug_information)){
				$debug_information_row = mysqli_fetch_assoc($debug_information);
				$debug_information_obj = json_decode($debug_information_row['debug_information']);
				//error_log(print_r($debug_information_obj,1));
				if(!empty($debug_information_obj)){
					foreach($debug_information_obj as $key => $value){
						$log_information->$key = $value;
					}
				}
			}
			
			$details['debug_information'] = json_encode($log_information);
			$target = "`debug_information`='[debug_information]', ";
		}
		
		if(!empty($log_row)){
			$details['id'] = $log_row;
			mlavu_query("UPDATE `lavutogo`.`orders` SET `order_id` = '[order_id]', `created_at`=[created_at], `consumer_id`='[consumer_id]', `merchant_id`='[merchant_id]', `amount` = '[amount]', $target `description`='[description]' WHERE `id`='[id]'", $details);
		}
		else{
			mlavu_query("INSERT INTO `lavutogo`.`orders` SET `order_id` = '[order_id]', `created_at`=[created_at], `consumer_id`='[consumer_id]', `merchant_id`='[merchant_id]', `amount` = '[amount]', $target `description`='[description]'", $details);
			$log_row = mlavu_insert_id();
		}
		
		return $log_row;
		
	}
	
	
$merchant_id = '';
{
	$merchant_id_result = mlavu_query("SELECT `id` FROM `lavutogo`.`merchants` WHERE `ltg_name`='[1]' AND `data_name` = '[2]'", postvar("ltg_name"), $ltg_merch['data_name']);
	if($merchant_id_result){
		$merchant_id = mysqli_fetch_assoc($merchant_id_result);
		$merchant_id = $merchant_id['id'];
	}
}

$consumer_id = '';
{
	$consumer_id_result = mlavu_query("SELECT `id` FROM `lavutogo`.`consumers` WHERE `email`='[1]'", $cust_email);
	if($consumer_id_result){
		$consumer_id = mysqli_fetch_assoc($consumer_id_result);
		$consumer_id = $consumer_id['id'];
	}
}