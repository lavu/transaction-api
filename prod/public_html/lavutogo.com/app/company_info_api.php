<?php
	// require core_functions lib
	require_once(__DIR__."/lib/core_functions.php");

	$company_name	= reqvar("company_name", "");
	$data_name		= reqvar("data_name", "");
	$enabled		= reqvar("enabled", "");
	$is_dev			= reqvar("is_dev", "0");
	$j_key			= lc_decode(reqvar("j_key", ""));
	$lavu_req_id	= urlvar("YIG", "");
	$lc				= explode(":", lc_decode(reqvar("lc", "")));
	$location_id	= reqvar("location_id", "");
	$logo_img		= reqvar("logo_img", "");
	$ltg_debug		= (reqvar("ltg_debug", "0") == "1");
	$ltg_name		= reqvar("ltg_name");
	$useoptmod		= reqvar("useoptmod", "0");
	$use_lavu_url	= reqvar("use_lavu_url", "");

	ltgDebug(__FILE__, "request in: ".json_encode($_REQUEST));

	if ($lc[0] != "lavu2go")
	{
		respondAndExit("error", "base validation mismatch");
	}

	if (empty($data_name))
	{
		respondAndExit("error", "missing data_name");
	}

	if ($lc[1] != $data_name)
	{
		respondAndExit("error", "data_name mismatch");
	}

	if ($lc[2] != date("Ymd"))
	{
		respondAndExit("error", "date check failed");
	}

	if (empty($location_id))
	{
		respondAndExit("error", "missing location_id");
	}

	$vars = array(
		'cc_key'				=> $j_key,
		'company'			=> $company_name,
		'enabled'			=> $enabled,
		'is_dev'				=> $is_dev,
		'last_modified'		=> UTCDateTime(true),
		'lavu_central_url'	=> $use_lavu_url,
		'logo_img'			=> $logo_img,
		'ltg_debug'			=> $ltg_debug,
		'ltg_name'			=> $ltg_name,
		'use_opt_mods'		=> $useoptmod
	);

	$update = "";

	buildUpdate($vars, $update);

	$vars['data_name']		= $data_name;
	$vars['location_id']		= $location_id;

	$fields = "";
	$values = "";

	buildInsertFieldsAndValues($vars, $fields, $values);

	// mlavu_query defaults to the `lavutogo` database if a connection has not been previously started
	$check_for_record = mlavu_query("SELECT `id` FROM `lavutogo`.`merchants` WHERE `data_name` = '[1]' AND `location_id` = '[2]'", $data_name, $location_id);
	if (!$check_for_record)
	{
		respondAndExit("db_error", "failed to check for database record: ".mysql_error());
	}

	if (mysqli_num_rows($check_for_record) > 0)
	{
		$info = mysqli_fetch_assoc($check_for_record);
		$vars['id'] = $info['id'];

		// update local db with new company info
		$update_record = mlavu_query("UPDATE `lavutogo`.`merchants` SET ".$update." WHERE `id` = '[id]'", $vars);
		if (!$update_record)
		{
			respondAndExit("db_error", "failed to update database record: ".mysql_error());
		}

		respondAndExit("success", "updated existing record");
	}
	else
	{
		$create_record = mlavu_query("INSERT INTO `lavutogo`.`merchants` (".$fields.") VALUES (".$values.")", $vars);
		if (!$create_record)
		{
			respondAndExit("db_error", "failed to create database record: ".mysql_error());
		}

		respondAndExit("success", "created new record");
	}

	function respondAndExit($status, $message)
	{
		$response = json_encode(
			array(
				'status'		=> $status,
				'message'	=> $message
			)
		);

		ltgDebug(__FILE__, "response: ".$response);

		lavu_echo($response);
	}
?>