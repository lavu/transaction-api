<?php
//error_log(__FILE__.__LINE__." Micro Timing:".microtime());
	error_reporting(E_ALL);
	ini_set('display_errors','0');

	force_ssl();
	force_www();

	session_start();

	require_once(__DIR__."/lib/core_functions.php");

	mlavu_connect_db();
	mlavu_select_db("lavutogo");

	$ltg_name = (empty($_GET['uri']))?"mattlavu":$_GET['uri'];

	$data_name = "";
	$is_dev = "";
	$json_cc = "";
	$lavu_central_url = "";
	$ltg_debug = "";
	$locationid = "";
	$use_optional_mods = "";

	// bootstrap !MUST! be run before togo_connect
	bootstrap($ltg_name);

	require_once(__DIR__."/togo_connect.php");

	LTG_Updater($ltg_name);

	$ltg_merch = get_local_data($ltg_name);
	if (empty($ltg_merch))
	{
		error_log(__FILE__.__FUNCTION__." SUPER DERP $ltg_name");
	}

	$tinfo = json_decode($ltg_merch['menu_data'], true);
	$lread = json_decode($ltg_merch['location_data'], true);
	$lread = array_merge($tinfo['config_settings'], $lread);

	if (strlen($lread['disable_decimal']) == 0)
	{
		$lread['disable_decimal'] = "0";
	}

	if (strlen($lread['taxrate']) == 0)
	{
		$lread['taxrate'] = "0";
	}

	$menu_groups = $tinfo['menu_groups'];

	$Rest_Loc = array(
		'delivery_limiters'	=> DeliveryLimiters($lread),
		'delivery_option'	=> (isset($lread['ltg_delivery_option']))?$lread['ltg_delivery_option']:0,
		'logo'				=> $ltg_merch['logo_img'],
		'ltg_times'			=> LTGTimes($lread),
		'phone'				=> $lread['phone']
	);

	$canReachServer = true;

	// INSERTED BY Brian D.  We make sure we can access the net path, whether through lsvpn or whatever.
	$is_LLS = isLLS($lread);

	if ($is_LLS)
	{
		$json_cc = $ltg_merch['cc_key'];

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://lsvpn.poslavu.com/hello.php");
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "cc=".$json_cc);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 4);
		$response = curl_exec ($ch);
		$curl_info = curl_getinfo($ch);
		curl_close ($ch);
		/*
		if($curl_info['http_code'] <  200 || $curl_info['httpd_code'] > 300  || $response != "Hello") {
			echo "We apologize, but the Restaurant's Server is unable to be Reached at this time. Please try again in a little bit, or call the Restaurant.";
			return;
		}
		*/
		if ($curl_info['http_code'] != 200 || $response != "Hello")
		{
			echo "<body style='background-color:gray;text-align:center;'>";
			echo "We apologize, but the Restaurant's Server is unable to be Reached at this time. Please try again in a little bit, or call the Restaurant.";
			echo "</body>";
			return;
		}
	}
	// End Insert

	// Inserted by Brian D. PIZZA STUFF TO FACTOR OUT INTO REQUIRE_ONCE...  Remember the part in menu_item_click that checks in JS if item is pizza creator item.
	$merchantDeserializedRow = $ltg_merch;
	$merchantDeserializedRow['menu_data'] = $tinfo;
	$merchantDeserializedRow['location_data'] = $lread;

	require_once(__DIR__."/components/pizza/ltg_specific_functions.php");

	setGlobalDeserializedMerchantRow($merchantDeserializedRow);

	$menuItemsThatUsePizzaMaker = getListOfMenuItemIDsThatUsePizzaMaker();

	//error_log('Menu items that use the pizza maker: ' . print_r($menuItemsThatUsePizzaMaker,1) );

	$menuItemsByIDThatUsePizzaMaker = array();
	foreach ($menuItemsThatUsePizzaMaker as $currPizzaMakerUsenMenuItem)
	{
		$menuItemsByIDThatUsePizzaMaker[$currPizzaMakerUsenMenuItem['id']] = $currPizzaMakerUsenMenuItem['id'];
	}
	$menuItemsByIDThatUsePizzaMakerJSONStr = json_encode($menuItemsByIDThatUsePizzaMaker);
	$component_security_hash = sha1($merchantDeserializedRow['data_name']."garbonzo");

	//$allPizzaFModRowsTiedToTheirConfigPizzaModsRow = getAllPizzaFModRowsTiedToConfPizzaModsRow();
	if (sessvar('additional_session_info_obj'))
	{
		echo "<script type='text/javascript'> window.additional_session_info_obj = ".sessvar('additional_session_info_obj').";</script>";
	}

	// END Insert PIZZA STUFFs...

	readfile("./ltg-template.html");

	// Javascript Exports
	echo "\n";
	echo "<script type='text/javascript' src='/assets/javascripts/cart_scripts2.js'></script>\n";
	echo "<script>\n";
	echo "var Menu_Groups = ".menu_groups_to_javascript($menu_groups).";\n";
	echo "var Restaurant_Name = \"".htmlspecialchars($lread['title'])."\";\n";
	echo "var LTG_Name = \"".$ltg_name."\";\n";
	echo "var Data_Name = \"".$data_name."\";\n";
	echo "var Loc_ID = \"".$lread['id']."\";\n";
	echo "var IsDevAccount = \"".$is_dev."\";\n";
	echo "var LTG_Debug = \"".$ltg_debug."\";\n";
	echo "var Payment_URL = \"".useLavuCentralURL("gateway")."\";\n";
	echo "var Base_URL = \"".$base_url."\";\n";
	echo "var Decimal_Places = ".$lread['disable_decimal'].";\n";
	echo "var Loc_Taxrate = ".$lread['taxrate'].";\n";
	echo "var Rest_Loc = ".json_encode($Rest_Loc).";\n";
	echo "var RemoveMe = ".$ltg_merch['menu_data'].";\n";
	echo "var ltg_payment_option = ".(isset($tinfo['config_settings']['ltg_payment_option'])?$tinfo['config_settings']['ltg_payment_option']:1).";\n";

	// Inserted by Brian D.
	echo "var ltg_menu_items_that_use_pizza_creator = ".$menuItemsByIDThatUsePizzaMakerJSONStr.";";
	echo "var ltg_dataname = \"".$ltg_merch['data_name']."\";";
	echo "var ltg_sec = \"".$component_security_hash."\";";
	// End Insert

	echo RestoreFromSession($ltg_name);
	echo GenerateTheoLTG($lread, $tinfo);

	echo "</script>\n";
	// END Javascript Exports

	// Include Javascript
	echo "<script src='./ltg.js'></script>"."\n";
	// error_log(__FILE__.__LINE__." Micro Timing:".microtime());
	exit;

	function DeliveryLimiters($lread)
	{
		$delivery_limiters = array();

		$ltg_allowed_zipcodes = isset($lread['ltg_allowed_zipcodes'])?$lread['ltg_allowed_zipcodes']:"";
		$ltg_allowed_zipcodes_json = '[]';
		if (!empty($ltg_allowed_zipcodes))
		{
			$ltg_allowed_zipcodesArr = explode(",", $ltg_allowed_zipcodes);
		}
		if (!empty($ltg_allowed_zipcodesArr) && count($ltg_allowed_zipcodesArr) > 0)
		{
			$delivery_limiters['zipcodes'] = $ltg_allowed_zipcodesArr;
		}
		else
		{
			$delivery_limiters['zipcodes'] = false;
		}

		$ltg_allowed_cities = isset($lread['ltg_allowed_cities'])?$lread['ltg_allowed_cities']:"";
		$ltg_allowed_cities_json = '[]';
		if (!empty($ltg_allowed_cities))
		{
			$ltg_allowed_citiesArr = explode(",", $ltg_allowed_cities);
		}
		if (!empty($ltg_allowed_citiesArr) && count($ltg_allowed_citiesArr) > 0)
		{
			$delivery_limiters['cities'] = $ltg_allowed_citiesArr;
		}
		else
		{
			$delivery_limiters['cities'] = false;
		}

		return $delivery_limiters;
	}

	function LTGTimes($lread)
	{
		if (isset($lread['timezone']) && !empty($lread['timezone']))
		{
			date_default_timezone_set($lread['timezone']);
		}

		$currentTS			= time();
		$now_datetime		= date("Y-m-d H:i:s", $currentTS);
		$now_parts			= explode(" ", $now_datetime);
		$now_date			= explode("-", $now_parts[0]);
		$now_time			= explode(":", $now_parts[1]);
		$max_advance_days	= $lread['max_advance_days'];

		$pickup_dates = array();
		for ($d = 0; $d <= $max_advance_days; $d++)
		{
			$dts = mktime($now_time[0], $now_time[1], $now_time[2], $now_date[1], ($now_date[2] + $d), $now_date[0]);
			$pickup_dates[] = array(date("Y-m-d", $dts), date("n/j/y", $dts));
		}

		// ltg start/end timestamps
		$ltg_start_hour = substr(str_pad($lread['lavu_togo_start_time'], 4, "0", STR_PAD_LEFT), 0, 2);
		$ltg_start_min  = substr(str_pad($lread['lavu_togo_start_time'], 4, "0", STR_PAD_LEFT), 2, 2);
		$ltg_end_hour   = substr(str_pad($lread['lavu_togo_end_time']  , 4, "0", STR_PAD_LEFT), 0, 2);
		$ltg_end_min    = substr(str_pad($lread['lavu_togo_end_time']  , 4, "0", STR_PAD_LEFT), 2, 2);

		$ltg_start_ts	= mktime($ltg_start_hour, $ltg_start_min, 0, $now_date[1], $now_date[2], $now_date[0]);
		if ($ltg_end_hour < $ltg_start_hour)
		{
			// do not use now_date for end day -- could be next day for end (ie. 2am closing, etc)
			$ltg_end_ts = mktime($ltg_end_hour, $ltg_end_min, 0, $now_date[1], $now_date[2]+1, $now_date[0]);
		}
		else
		{
			// otherwise use today's day
			$ltg_end_ts = mktime($ltg_end_hour, $ltg_end_min, 0, $now_date[1], $now_date[2], $now_date[0]);
		}

		$pickup_times = array();
		for ($ts = $ltg_start_ts; $ts <= $ltg_end_ts; $ts += 300)
		{
			$valid_today = 0;
			if ($ts > ($currentTS + ($lread['minimum_notice_time'] * 60)))
			{
				$valid_today = 1;
			}
			$pickup_times[] = array(date("H:i:s", $ts), date("g:i a", $ts), $valid_today);
		}

		$returnArray = array(
			'pickup_times'		=> $pickup_times,
			'pickup_dates'		=> $pickup_dates,
			'ltg_start_hour'	=> $ltg_start_hour,
			'ltg_end_hour'		=> $ltg_end_hour,
			'ltg_start_min'		=> $ltg_start_min,
			'ltg_end_min'		=> $ltg_end_min,
			'ltg_start_ts'		=> $ltg_start_ts,
			'ltg_end_ts'			=> $ltg_end_ts,
			'ltg_start_string'	=> date("g:i a", $ltg_start_ts),
			'ltg_end_string'	=> date("g:i a", $ltg_end_ts),
			'now_date'			=> date("Y-m-d", $currentTS)
		);

		return $returnArray;
	}

	function RestoreFromSession($ltg_name)
	{
		$str = "";
		if (sessvar("saved_ltg_name") == $ltg_name)
		{
			if (sessvar_isset("cart_contents"))
			{
				$cart_contents = sessvar("cart_contents");
				$str .= "var Cart_Contents = ".$cart_contents.";\n";
				ltgDebug(__FILE__, __FUNCTION__." got cart_contents for ".$ltg_name);
			}

			if (sessvar_isset("order_id"))
			{
				$order_id = sessvar("order_id");
				$str .= "var Order_ID = ".$order_id.";\n";
				ltgDebug(__FILE__, __FUNCTION__." got order_id ".$order_id." for ".$ltg_name);
			}
		}

		return $str;
	}

	function GenerateTheoLTG($lread, $tinfo)
	{
		$lfields = array(
			'address',
			'city',
			'country',
			'decimal_char',
			'disable_decimal',
			'modules',
			'monitary_symbol',
			'primary_currency_code',
			'state',
			'tax_inclusion',
			'taxrate',
			'thousands_char',
			'timezone',
			'title',
			'website',
			'zip',
			'ltg_email',
			'ltg_iframe_src',
			'ltg_phone_carrier',
			'ltg_phone_number',
			'ltg_print_failed_alarm',
			'ltg_use_opt_mods',
			'left_or_right'
		);

		$lcopy = array();
		foreach ($lread as $key => $value )
		{
			if (in_array($key, $lfields))
			{
				$lcopy[$key] = $value;
			}
		}

		$setup_js = "var Location_Info = LavuToGo.LocationInfo.createLocationInfo(".json_encode($lcopy).");\n";
		$setup_js .= "var Tax_Profile;\n";

		foreach ($tinfo['tax_profiles'] as $tax_profile_id => $tax_profile_rates)
		{
			$setup_js .= "window.Tax_Profile = LavuToGo.TaxProfile.createTaxProfile('".$tax_profile_id."');\n";
			foreach ($tax_profile_rates as $tax_profile_rate)
			{
				$tax_profile_rate['calc'] = str_replace(",", ".", $tax_profile_rate['calc']);
				$setup_js .= "window.Tax_Profile.addTaxProfileRate(LavuToGo.TaxProfileRate.createTaxProfileRate(".json_encode( $tax_profile_rate )."));\n";
			}
		}

		foreach ($tinfo['menu_items'] as $value)
		{
			if ($value['_deleted']=="0" && $value['ltg_display']=="1")
			{
				$setup_js .= "LavuToGo.MenuItem.createMenuItem(".json_encode($value).");\n";
			}
		}

		foreach ($tinfo['menu_categories'] as $value)
		{
			if ($value['_deleted']=="0" && $value['ltg_display']=="1")
			{
				$setup_js .= "LavuToGo.MenuCategory.createMenuCategory(".json_encode($value).");\n";
			}
		}

		$setup_js .= "var Optional_Mod_List = new Array();";

		foreach ($tinfo['modifier_lists'] as $value)
		{
			$optional_mod_list_id	= str_replace("\"", "&quot;", $value['id']);
			$optional_mod_segments	= explode("|*|", $value['modifiers']);

			$setup_js .= "omodlist = LavuToGo.OptionalModifierList.createOptionalModifierList(".$optional_mod_list_id.");\n";

			for ($j=0; $j<count($optional_mod_segments); $j++)
			{
				$optional_mod_data	= explode("|o|", $optional_mod_segments[$j]);
				$optional_mod_id    = htmlspecialchars($optional_mod_data[0]);
				$optional_mod_title = htmlspecialchars($optional_mod_data[1]);
				$optional_mod_price = htmlspecialchars($optional_mod_data[2]);

				if (empty($optional_mod_price))
				{
					$optional_mod_price = 0;
				}

				$setup_js .= "omodlist.addOption(LavuToGo.OptionalModifier.createOptionalModifier(".$optional_mod_id.", \"".$optional_mod_title."\", ".$optional_mod_price.", omodlist.id()));\n";
			}
			$setup_js .= "window.Optional_Mod_List.push(omodlist);\n";
		}

		$setup_js .= "window.Forced_Mod_List = new Array();\n";

		foreach ($tinfo['forced_modifier_lists'] as $value)
		{
			$fm_title	= trim($value['title']);
			$fm_type	= $value['type'];
			$fm_mods	= $value['modifiers'];
			$fm_id		= $value['id'];

			$fm_title_in_quotes	= str_replace("\"", "&quot;", $fm_title);
			$fm_type_in_quotes	= str_replace("\"", "&quot;", $fm_type);

			$setup_js .= "fmodifier = LavuToGo.ForcedModifier.createForcedModifier( ".$fm_id.", \"".$fm_title_in_quotes."\", \"".$fm_type_in_quotes."\");\n";

			foreach ($fm_mods as $fm_mod_value)
			{
				$setup_js .= "fmodifier.addOption(LavuToGo.ForcedModifierOption.createForcedModifierOption(".json_encode($fm_mod_value).", fmodifier.id()));\n";
			}

			$setup_js .= "window.Forced_Mod_List.push(fmodifier);\n";
		}

		$setup_js .= "window.Forced_Mod_Groups_List = ".json_encode($tinfo['forced_modifier_groups']).";\n";
		return $setup_js;
	}

	function menu_groups_to_javascript($menu_groups)
	{
		$return = array();

		foreach ($menu_groups as $mg)
		{
			if ($mg['_deleted'] == "0")
			{
				$return[] = array(
					'id'			=> $mg['id'],
					'group_name'	=> $mg['group_name']
				);
			}
		}

		return json_encode($return);
	}

	function menu_categories_to_javascript($menu_categories)
	{
		$return = array();

		foreach ($menu_categories as $mc)
		{
			if ($mc['_deleted']=="0" && $mc['ltg_display']=="1")
			{
				$return[] = $mc;
			}
		}

		return json_encode($return);
	}

	function bootstrap($ltg_name)
	{
		global $base_url;

		$base_url = trim($_SERVER['SCRIPT_NAME'], "index.php");
		$base_url = trim($base_url, "ltg-experiments.php");

		// check if valid merchant (and enabled)
		$merchant_results = mlavu_query("SELECT * FROM `merchants` WHERE `ltg_name` LIKE '[1]' AND `enabled` = 1", $ltg_name);
		$merchant = array();
		if (mysqli_num_rows($merchant_results))
		{
			$merchant = mysqli_fetch_assoc($merchant_results);
		}

		if (count($merchant) < 1 )
		{
			require_once(__DIR__."/../404.php");
			exit();
		}

		// set valid api values
		global $data_name;
		global $is_dev;
		global $json_cc;
		global $lavu_central_url;
		global $ltg_debug;
		global $locationid;
		global $use_optional_mods;

		$data_name			= $merchant['data_name'];
		$is_dev				= $merchant['is_dev'];
		$json_cc			= $merchant['cc_key'];
		$lavu_central_url	= $merchant['lavu_central_url'];
		$locationid			= $merchant['location_id'];
		$ltg_debug			= $merchant['ltg_debug'];
		$use_optional_mods	= $merchant['use_opt_mods'];

		ltgDebug(__FILE__, __FUNCTION__." succeeded");

		set_sessvar("ltg_name", $ltg_name); // added by CF 1-3-13
	}

	function force_ssl()
	{
		if ($_SERVER['SERVER_PORT'] != "443")
		{
			header("Location: https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
			exit();
		}
	}

	function force_www()
	{
		if (strpos($_SERVER['HTTP_HOST'], "www") !== 0)
		{
			header("Location: https://www.".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
			exit();
		}
	}
?>