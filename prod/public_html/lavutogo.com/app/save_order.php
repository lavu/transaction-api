<?php
	putenv("TZ=America/Chicago");
	session_start();
	// require libs (core_functions, togo_connect, etc)
	require_once(dirname(__FILE__).'/lib/core_functions.php');
	require_once(dirname(__FILE__).'/togo_connect.php');
	include_once(dirname(__FILE__).'/lib/sendgrid-php/SendGrid_loader.php');
	ini_set('display_errors','0');
	// query local db for menu & location data
	$ltg_merch = get_local_data(postvar("ltg_name"));
	$ltg_name = postvar("ltg_name");
	$json_cc = $ltg_merch['cc_key'];
	$tinfo = json_decode($ltg_merch['menu_data'], true);
	$lread = json_decode($ltg_merch['location_data'], true);
	$configs = $tinfo['config_settings'];
	$nowtime = localize_datetime(date("Y-m-d H:i:s"), $lread['timezone']);
	$subtotal = postvar("subtotal");
	$total = postvar("total");
	$taxrate = postvar("taxrate");
	$tax = postvar("tax");
	$cart_contents = postvar("cart_contents");
	$firstname = postvar("firstname");
	$lastname = postvar("lastname");
	$cust_phone = postvar("phone");
	$cust_email = postvar("email");
	$today_date = postvar("today_date");
	$togo_date = postvar("togo_date");
	$order_id = postvar("order_id");
	//Inserted by Brian D. New fields.
	$is_delivery = postvar("is_delivery");
	$street_address = postvar("street_address");
	$postal_code = postvar("postal_code");
	$city = postvar("city");
	$state = postvar("state");
	$country = postvar("country");
	//End insert

	if(empty($ltg_name)){
		error_log(print_r($_REQUEST,1));
		echo "0|No LTG name found, Possibly Reached in Error.";
		exit();
	}

	require_once(dirname(__FILE__) .'/loginfo.php');
	$order_log_information = new LavuOrderInformation();
	$order_log_debug_information = new LavuDebugInfo();


	if(empty($order_id)){
		$order_id = "";
	}

	if ($today_date == $togo_date) {
		$togo_time = postvar("togo_time_today");
		$order_log_information->togo_time = $togo_time;
	}else {
		$togo_time = postvar("togo_time_future");
		$order_log_information->togo_time = $togo_time;
	}

	//LOG INFORMATION
	$order_log_information->customer_information->first_name      = postvar("firstname");
	$order_log_information->customer_information->last_name       = postvar("lastname");
	$order_log_information->customer_information->phone_number    = postvar("phone");
	$order_log_information->customer_information->email           = postvar("email");
	$order_log_information->subtotal                              = $subtotal;
	$order_log_information->total                                 = $total;
	$order_log_information->taxrate                               = $taxrate;
	$order_log_information->tax                                   = $tax;
	$order_log_information->loc_id                                = $lread['id'];
	$order_log_information->cc                                    = $json_cc;

	$vars      = array();
	$vars[]    = $order_id;                            //  0 - order id
	$vars[]    = $nowtime;                             //  1 - opened
	$vars[]    = "0000-00-00 00:00:00";                //  2 - closed
	$vars[]    = $subtotal;                            //  3 - subtotal
	$vars[]    = $taxrate;                             //  4 - taxrate
	$vars[]    = $tax;                                 //  5 - tax
	$vars[]    = $total;                               //  6 - total
	$vars[]    = "Lavu ToGo";                          //  7 - server
	if( postvar("set_void", "3") == '0' ){
		$vars[]    = "Lavu ToGo UNPAID";               //  8 - tablename
	} else {
		$vars[]    = "Lavu ToGo";                      //  8 - tablename
	}
	$vars[]    = 1;                                    //  9 - send_status
	$vars[]    = 1;                                    // 10 - guests
	$vars[]    = 0;                                    // 11 - server id
	$vars[]    = 1;                                    // 12 - number of checks
	$vars[]    = "";                                   // 13 - multiple tax rates
	$vars[]    = 0;                                    // 14 - to tab or not to tab
	$vars[]    = 0;                                    // 15 - deposit status
	$vars[]    = "Lavu ToGo";                          // 16 - register setting (new location setting to allow customer to set Lavu ToGo register setting?)
	$vars[]    = "Lavu ToGo";                          // 17 - register name
	$vars[]    = 0;                                    // 18 - tax exempt
	$vars[]    = "";                                   // 19 - exemption id
	$vars[]    = "";                                   // 20 - exemption name
	$vars[]    = "";                                   // 21 - alternate tablename (use for customer name?)
	$vars[]    = "";                                   // 22 - past names (meant to hold history of table names)
	$vars[]    = 1;                                    // 23 - togo status
	$vars[]    = $firstname." ".$lastname;             // 24 - togo name
	$vars[]    = $cust_phone;                          // 25 - customer phone number
	$vars[]    = $cust_email;                          // 26 - customer email address
	$vars[]    = $togo_date." ".$togo_time;            // 27 - pickup date/time
	$vars[]    = "LTG - ".$firstname." ".$lastname;    // 28 - tabname
	$uniq = rand(1000,9999);
	$iodate = date('YmdHis');
	$ioid =  "{$ltg_merch['location_id']}{$lread['id']}LTG-{$uniq}-{$iodate}";
	$vars[]    = $ioid;    // 29 - IOID
	$orderstr  = join("|*|", $vars);
	$cart_parts = explode("|*|", $cart_contents);
	$item_vars = array();
	$receipt_contents = "";
	$price_check = 0;

	for($n=0; $n<count($cart_parts); $n++){
		$item_props = explode("|o|", $cart_parts[$n]);
		$order_contents_log                                  = new LavuOrderContents();
		$order_contents_log->item                            = $item_props[0];    //  0 - item
		$order_contents_log->price                           = $item_props[1];    //  1 - price
		$order_contents_log->quantity                        = $item_props[2];    //  2 - quantity
		$order_contents_log->options                         = $item_props[3];    //  3 - options
		$order_contents_log->special                         = $item_props[12];   //  12 - special
		$order_contents_log->modify_price                    = $item_props[3];   //  3 - options
		$order_contents_log->print                           = $item_props[5];    //  5 - print
		$order_contents_log->item_id                         = $item_props[9];    //  9 - item id
		$order_contents_log->printer                         = $item_props[10];    // 10 - printer
		$order_contents_log->apply_taxrate                   = $item_props[11];    // 11 - apply taxrate
		$order_contents_log->custom_taxrate                  = $item_props[12];    // 12 - custom taxrate
		$order_contents_log->modifier_list_id                = $item_props[13];   // 13 - modifier list id
		$order_contents_log->forced_modifier_group_id        = $item_props[14];   // 14 - forced modifier group id
		$order_contents_log->forced_modifiers_price          = $item_props[15];    // 15 - forced modifiers price
		$order_contents_log->device_time                     = $nowtime;          // 21 - device time (local time)
		$order_contents_log->tax_inclusion                   = $item_props[27];   // 27 - tax inclusion
		$order_log_information->order_contents_information[] = $order_contents_log;

		if(!CheckItemPrice($item_props[9],$item_props[1])){
			error_log('CheckItemPrice FFFFFFUUUUUUUCCCCCCKKKKKKK');
			exit('0|SHPXLBH!1');
		}
		$price_check += ($order_contents_log->price * $order_contents_log->quantity);
	}
	$subtotal2 = preg_replace ( '/([.,])(\d\d\d)/' , '$2' , $subtotal);
	$total2 = preg_replace ( '/([.,])(\d\d\d)/' , '$2' , $total);
	// Fuck floating point numbers...
	if( ($price_check > ($subtotal2 + 0.001) ) || ($subtotal2 > ($total2 + 0.001)) ){
		error_log(__FILE__." price_check : {$price_check} subtotal : {$subtotal2} total : {$total2}");
		exit('0|SHPXLBH!2');
	}

	$contentsstr = $cart_contents;
	$vars = array();
	$vars["m"] = 28;
	$vars["cc"] = $json_cc;
	$vars["loc_id"] = $ltg_merch['location_id'];
	$vars["order_info"] = $orderstr;
	$vars["order_contents"] = $contentsstr;
	$vars["use_server_prefix"] = "2";
	$vars["set_opened"] = $nowtime;
	$vars["set_closed"] = "0000-00-00 00:00:00";
	$vars["set_void"] = postvar("set_void", "3");

	if($is_delivery){

		//Shows up on
		$deliveryInfoArr = array();
		$deliveryInfoArr['First_Name'] = $firstname;
		$deliveryInfoArr['Last_Name'] = $lastname;
		$deliveryInfoArr['Phone_Number'] = $cust_phone;
		$deliveryInfoArr['is_delivering'] = '1';
		//$deliveryInfoArr['delivery_time'] = date("Y-m-d H:i:s");
		$deliveryInfoArr['delivery_time'] = $togo_date." ".$togo_time;
		$deliveryInfoArr['customer_id'] = '-1';
		$deliveryInfoArr['address'] = array();
		$deliveryInfoArr['address']['street'] = $street_address;
		$deliveryInfoArr['address']['city'] = $city;
		$deliveryInfoArr['address']['state'] = $state;
		$deliveryInfoArr['address']['zip'] = $postal_code;
		$deliveryInfoArr['address']['country'] = $country;
		$deliveryInfoArr['Street_Address'] = $street_address;
		$deliveryInfoArr['City'] = $city;
		$deliveryInfoArr['State'] = $state;
		$deliveryInfoArr['Zip_Code'] = $postal_code;

		$printInfoArr = array();
		$printInfoArr[] = array("field"=>"First_Name","print"=>"r","info"=>$firstname);
		$printInfoArr[] = array("field"=>"Last_Name","print"=>"r","info"=>$lastname);
		$printInfoArr[] = array("field"=>"Phone_Number","print"=>"r","info"=>$cust_phone);
		$printInfoArr[] = array("field"=>"Street_Address","print"=>"r","info"=>$street_address);
		$printInfoArr[] = array("field"=>"State","print"=>"r","info"=>$state);
		$printInfoArr[] = array("field"=>"Zip Code","print"=>"r","info"=>$postal_code);


		$originalIDArr = array('deliveryInfo' => $deliveryInfoArr, 'print_info' => $printInfoArr);
		$deliveryInfoJSONStr = json_encode($originalIDArr);
		$vars["original_id"] = "19|o|Customer|o|$firstname $lastname|o|'-1'|o|$deliveryInfoJSONStr";
	}

	$vars['UDID'] = $_SERVER['REMOTE_ADDR'];
	$save_result = api_response($vars);
	$save_pieces = explode(":", $save_result);
	$succeeded = substr($save_result, 0, 1) == "1";

	if(!$succeeded){
		echo "0|Unable to save order.  Please try again.  If the issue persists, please place the order at the store.";
		exit();
	}
	$order_id = $save_pieces[1];
	$order_log_information->order_id = $order_id;
	$log_id = log_details($order_log_information, $order_id, $total, $consumer_id, $merchant_id);
	log_details($order_log_debug_information, $order_id, $total, $consumer_id, $merchant_id, $log_id);

	$mailvars = array();
	$mailvars['merchant_image'] = "https://admin.poslavu.com/images/".$ltg_merch['data_name']."/".$ltg_merch['logo_img'];
	$mailvars['merchant_name'] = $lread['title'];
	$mailvars['customer_fn'] = $firstname;
	$mailvars['customer_ln'] = $lastname;
	$mailvars['order_id'] = $order_id;
	$mailvars['togo_time'] = $togo_time;
	$mailvars['togo_date'] = $togo_date;
	$mailvars['customer_email'] = $cust_email;
	if( postvar("set_void", "3") == '0' ){
		send_order_confirmation($mailvars);
	}

	echo implode("|", $save_pieces);

	if ( (array_key_exists('ltg_phone_number', $configs) && $configs['ltg_phone_number'] != '') ) {

		$to_str = "";
		$sendgrid = new SendGrid('ticket@poslavu.com', 'balilavu');
		$mail = new SendGrid\Mail();

		switch ( $configs['ltg_phone_carrier'] ) {
			case 'att':
				$to_str = $configs['ltg_phone_number']."@txt.att.net";
			break;
			case 'tmobile':
				$to_str = $configs['ltg_phone_number']."@tmomail.net";
			break;
			case 'sprint':
				$to_str = $configs['ltg_phone_number']."@messaging.sprintpcs.com";
			break;
			case 'verizon':
				$to_str = $configs['ltg_phone_number']."@vtext.com";
			break;
		}

		$mail->addTo($to_str)->
		setFrom('order@lavutogo.com')->
		setText("Order ID: $order_id placed from Lavu ToGO. Pickup time is at $togo_time");
		$order_log_debug_information->sentTxtNotification='1';
		$sendgrid->smtp->send($mail);

	}

			/**
		 * SEND AN EMAIL RECIEPT TO THE RESTAURANT
		 **/
		if ((array_key_exists('ltg_email', $configs) && $configs['ltg_email'] != '') ) {
				$sendgrid = new SendGrid('ticket@poslavu.com', 'balilavu');
				$mail2 = new SendGrid\Mail();
				// send standard email to merchant for order notification!

				$mail2->addTo($configs['ltg_email'])->
								setFrom('order@lavutogo.com')->
								setSubject("Lavu ToGO Order $order_id")->
								setText("Please check your open orders for ORDER ID: $order_id. Pickup time is at $togo_time");

				$sendgrid->smtp->send($mail2);
				$order_log_debug_information->sentMailNotification='1';
		}



	function CheckItemPrice($m_item,$price){
		global $tinfo;
		$tinfo['menu_items'];
		foreach ($tinfo['menu_items'] as $item) {
			if($item['id'] == $m_item){
				if($item['price'] > $price){
					return false;
				}else{
					return true;
				}
			}
		}
		error_log('Never Matched Item ID :'.print_r($m_item, 1 ) );
		return true;
	}

	function send_order_confirmation($mailvars){
		/*
		$mailvars = array();
		$mailvars['merchant_image'];
		$mailvars['merchant_name'];
		$mailvars['customer_fn'];
		$mailvars['customer_ln'];
		$mailvars['order_id'];
		$mailvars['togo_time'];
		$mailvars['togo_date']
		$mailvars['customer_email'];
		*/
		$emailText  = "<html><body><img src='{$mailvars['merchant_image']}' /><br />\n";
		$emailText .= "<h3>Hi, {$mailvars['customer_fn']} {$mailvars['customer_ln']}</h3>\n";
		$emailText .= "<h4>Order confirmation for your online order {$mailvars['order_id']} scheduled for {$mailvars['togo_time']}.</h4>\n";
		$emailText .= "<p>\nThe merchant does not guarantee the order will be ready at the time you requested... \n";
		$emailText .= "	Please be patient if they're busy. \n";
		$emailText .= "	Thank you for your service. \n";
		$emailText .= "	Please order online with us again!\n";
		$emailText .= "</p>\n<br><br>\n";
		$emailText .= "<img src='https://www.lavutogo.com/assets/images/powered_by_lavu.png' /></body></html>\n";
		$sendgrid = new SendGrid('ticket@poslavu.com', 'balilavu');
		$mail_confirmation = new SendGrid\Mail();
		$mail_confirmation->addTo(
				$mailvars['customer_email'])->
				setFrom('noreply@lavutogo.com')->
				setSubject("Your Online Order from ".$mailvars['merchant_name'])->
				setHtml("$emailText"
			);
		
		$sendgrid->smtp->send($mail_confirmation);
	}
