//
//??? (In Progress)
//
//
// Info Pane / Map (DONE)
//
{
	function openMapsWindow(name, phone, address, city, state, zip){
		str = '<iframe width="480" height="375" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="//maps.google.com/maps?&q='+escape(address)+',+'+escape(city)+',+'+escape(state)+',+'+escape(zip)+'&hnear='+escape(city)+',+'+escape(state)+'&output=embed"></iframe>';
		str += '<br/>';
		str += ' <button id="closeMapWindowButton" class="redButton" onclick="CloseMod()">close window</button>';
		OpenModbox(480,400,str);
	}

	function formatAMPM(date) {
		var hours = date.getHours();
		var minutes = date.getMinutes();
		var ampm = hours >= 12 ? 'pm' : 'am';
		hours = hours % 12;
		hours = hours ? hours : 12; // the hour '0' should be '12'
		minutes = minutes < 10 ? '0'+minutes : minutes;
		var strTime = hours + ':' + minutes + ' ' + ampm;
		return strTime;
	}

	function InfoPane(){
		if(window.Rest_Loc.logo !== ''){
			var imageURL = "//admin.poslavu.com/private_api/images.php?image="+escape(window.Rest_Loc.logo).replace(/%/g,"%25")+"&logo=yes&dataname="+window.Data_Name;
			document.getElementById('RestaurantLogo').src = imageURL;
		}
		document.getElementById('Map').innerHTML = "<img id='mapImage' src='https://maps.googleapis.com/maps/api/staticmap?markers="+escape(window.Location_Info._title+' '+window.Location_Info._address+' '+window.Location_Info._city+' '+window.Location_Info._state+' '+window.Location_Info._zip)+'&hnear='+escape(window.Location_Info._city)+',+'+escape(window.Location_Info._state)+"&size=155x160' />";
		document.getElementById('mapImage').onclick = function(){
			openMapsWindow(window.Location_Info._title,window.Location_Info._phone,window.Location_Info._address,window.Location_Info._city,window.Location_Info._state,window.Location_Info._zip);
		};
		var restInfo = document.getElementById('RestInfo');
		while (restInfo.firstChild) {
			restInfo.removeChild(restInfo.firstChild);
		}
		var infoP= document.createElement("h3");
		var textNode = document.createTextNode('ToGO Hours');
		infoP.appendChild(textNode);
		restInfo.appendChild(infoP);

		infoP= document.createElement("p");
		textNode = document.createTextNode((window.Rest_Loc.ltg_times.ltg_start_string)+' – '+(window.Rest_Loc.ltg_times.ltg_end_string));
		infoP.appendChild(textNode);
		restInfo.appendChild(infoP);

		infoP= document.createElement("h3");
		textNode = document.createTextNode('Directions');
		infoP.appendChild(textNode);
		restInfo.appendChild(infoP);

		infoP= document.createElement("p");
		textNode = document.createTextNode(window.Location_Info._title);
		infoP.appendChild(textNode);
		restInfo.appendChild(infoP);

		infoP= document.createElement("p");
		textNode = document.createTextNode(window.Location_Info._address);
		infoP.appendChild(textNode);
		restInfo.appendChild(infoP);

		infoP= document.createElement("p");
		textNode = document.createTextNode(window.Location_Info._city+", "+window.Location_Info._state+" "+window.Location_Info._zip);
		infoP.appendChild(textNode);
		restInfo.appendChild(infoP);

		infoP= document.createElement("p");
		textNode = document.createTextNode(window.Rest_Loc.phone);
		infoP.appendChild(textNode);
		restInfo.appendChild(infoP);
	}
}

//
// Brian D. Functions (DONE?)
//
{
	function AskPickupOrDelivery(){
		var delivery_cities_and_zips_html = "";
		var windowHeight = -1;
		//There's 2 options really, First: Both City and Zip-Codes are not white-listed, OR: either cities/zips are white-listed
		var showingCitiesAndZips = true;//// = ltg_filter_by_zipcodes_js || ltg_filter_by_cities_js
		if(showingCitiesAndZips){
			buttomDivHeight = 320;
			windowHeight = 400;
		}else{
			windowHeight = 180;
			buttomDivHeight = 0.8 * windowHeight;
		}
		/* ltg_allowed_postalcodes_json.indexOf(postal_code_input.value) > -1 */
		var htmlStr = "";
		htmlStr += "<br><h3>Will this be for pickup or delivery?</h3><br><hr>";
		htmlStr += "<div style ='width:100%;'>";
		htmlStr +=   "<span style ='width:49%;display: inline-block;vertical-align: top;'><br>";
		htmlStr +=     "<button class='greenButton' onclick='response_pickup_or_delivery(\"pickup\");' >Pick Up</button><br>";
		htmlStr +=   "</span>";
		htmlStr +=   "<span style ='width:49%;display: inline-block;vertical-align: top; border-left:1px solid black;'><br>";
		htmlStr +=     "<button class='greenButton' onclick='response_pickup_or_delivery(\"delivery\");' >Delivery</button>";
		if(showingCitiesAndZips){
			htmlStr +=  "<div overflow-y:scroll;'>";
			if(window.Rest_Loc.delivery_limiters.cities){
				htmlStr += "Deliveries are available to the following cities:<br>"+ListtoULLI(window.Rest_Loc.delivery_limiters.cities,false);
			}
			if(window.Rest_Loc.delivery_limiters.zipcodes){
				htmlStr += "Deliveries are limited to the following postal codes:<br>"+ListtoULLI(window.Rest_Loc.delivery_limiters.zipcodes,true);
			}
			if(!(window.Rest_Loc.delivery_limiters.zipcodes) && !(window.Rest_Loc.delivery_limiters.cities) ){
				htmlStr +=  "<br>";
			}
			htmlStr +=  "</div>";
		}
		htmlStr +=   "</span>";
		htmlStr += "</div>";
		htmlStr += "<hr><br><button class='redButton' onclick='response_pickup_or_delivery(\"Cancel\");' >Back to order</button><br><br>";
		OpenModbox(480,windowHeight,htmlStr);
		document.getElementById('mod_box').style.height = 'auto';
		return;
	}

	function if_delivery_validate_as_within_delivery_zone_return_true_if_error(){
		if( delivery_or_pickup == 'delivery' ){
			var postal_code_input = document.getElementById('postal_code');
			if( ltg_allowed_postalcodes_json.indexOf(postal_code_input.value) > -1 ){
				return false;//No errors.
			}else{
				alert("Sorry, your location is not included in this location's designated delivery area.");
				return true;
			}
		}else{
			return false;
		}
	}

	function response_pickup_or_delivery(pickupDeliveryOrCancel){
		if(pickupDeliveryOrCancel == 'delivery'){
			SetPickupOrDelivery(2);
		}else if(pickupDeliveryOrCancel == 'pickup'){
			SetPickupOrDelivery(0);
		}else{
			CloseMod();
			return;
		}
		window.hasAskedIfDeliveryOrCheckout = true;
		CloseMod();
		StartCheckout();
	}
}

//
// Checkout
//
{
	function StartCheckout() {
		if(window.Shopping_Cart.entries().length < 1){
			alert("You have not ordered any items yet.");
			return;
		}
		if(window.hasAskedIfDeliveryOrCheckout !== true){
			window.hasAskedIfDeliveryOrCheckout = false;
		}
		if(window.Rest_Loc.delivery_option == 1 && !window.hasAskedIfDeliveryOrCheckout){
			AskPickupOrDelivery();
			return;
		}
		window.hasAskedIfDeliveryOrCheckout = false;
		//StartSocialCheckout();
		CheckoutProcess();
	}

	function StartSocialCheckout() {
		var xmlhttp = BuildAJAXObject();
		xmlhttp.onreadystatechange = function(){
			if (xmlhttp.readyState == 4) {
				var str = '';
				if (xmlhttp.status == 200) {//success
					str = xmlhttp.responseText;
				}
				str += "<div><p class='modbox_text'>or continue placing your order as a guest...</p>";
				str += "<input type='button' class='redButton' value='Cancel' onclick='CloseMod()'>";
				str += "&nbsp;&nbsp;&nbsp;";
				str += "<input type='button' class='greenButton' value='Continue >>' onclick='CheckoutProcess();' /></div>";
				OpenModbox(480,400,str);
			}
		};
		xmlhttp.open('GET', window.Base_URL+'/social_hub.php?ltg_name='+window.LTG_Name, true);
		xmlhttp.send();
	}

	function CheckoutProcess(email,firstname,lastname,phone) {
		var checkout_amount = LavuToGo.NumberFormatter.createNumberFormatter().toMonitaryString( window.Shopping_Cart.total() );
		// eww something references subtotal by the global in shopping cart
		var subtotal = window.Shopping_Cart.subtotal();
		var cart_string = window.Shopping_Cart.toLavuString();
		str = "<div id='checkout_process' ><br><h4>Order Total: <strong>"+escapeHtml(checkout_amount)+"</strong><h4><p>&nbsp;</p></div>";
		OpenModbox(480,400,str);
		document.getElementById('mod_box').style.height = 'auto';
		var checkoutElement = document.getElementById('checkout_process');
		var formRows ='<table class="formTable">';
		formRows += '<tr><td class="coltitle">First Name   </td><td class="colvalue"><input id="form_firstname" size="25" type="text"></td></tr>';
		formRows += '<tr><td class="coltitle">Last Name    </td><td class="colvalue"><input id="form_lastname"  size="25" type="text"></td></tr>';
		formRows += '<tr><td class="coltitle">Phone Number </td><td class="colvalue"><input id="form_phone"     size="25" type="text"></td></tr>';
		formRows += '<tr><td class="coltitle">Email Address</td><td class="colvalue"><input id="form_email"     size="25" type="text"></td></tr>';
		if(window.delivery_or_pickup == 'delivery' ){
			formRows += '<tr><td class="coltitle">Street Address</td><td class="colvalue"><input id="form_sAddress" size="25" type="text"></td></tr>';
			if(window.Rest_Loc.delivery_limiters.zipcodes){
				formRows += '<tr id=zipLimiter><td class="coltitle">ZIP / Postal Code</td><td class="colvalue" style="text-align:center;">';
				formRows += "<select id='form_zip' style='width:171px;'>";
				formRows += "<option value='' selected disabled></option>";
				for(var i = 0; i < window.Rest_Loc.delivery_limiters.zipcodes.length; i++){
					formRows += "<option value='"+window.Rest_Loc.delivery_limiters.zipcodes[i]+"'>"+window.Rest_Loc.delivery_limiters.zipcodes[i]+"</option>";
				}
				formRows += "</select></td></tr>";
			}else{
				formRows += '<tr><td class="coltitle">ZIP / Postal Code</td><td class="colvalue"><input id="form_zip" size="25" type="text"></td></tr>';
			}
			if(window.Rest_Loc.delivery_limiters.cities){
				formRows += '<tr id=cityLimiter><td class="coltitle">City</td><td class="colvalue" style="text-align:center;">';
				formRows += "<select id='form_city' style='width:171px;'>";
				formRows += "<option value='' selected disabled></option>";
				for(var i = 0; i < window.Rest_Loc.delivery_limiters.cities.length; i++){
					formRows += "<option value='"+window.Rest_Loc.delivery_limiters.cities[i]+"'>"+window.Rest_Loc.delivery_limiters.cities[i]+"</option>";
				}
				formRows += "</select></td></tr>";
			}else{
				formRows += '<tr><td class="coltitle">City</td><td class="colvalue"><input id="form_city" size="25" type="text"></td></tr>';
			}
			formRows += '<tr><td class="coltitle">State / Province</td><td class="colvalue"><input id="form_state" size="25" type="text"></td></tr>';
			formRows += '<tr id=ltgDate><td class="coltitle">Delivery Dates</td><td class="colvalue" style="text-align:center;">';
			formRows += "<select id='form_date' style='width:171px;' onchange='GenerateDeliveryTimes();'>";
			for(var i = 0; i < window.Rest_Loc.ltg_times.pickup_dates.length; i++){
				formRows += "<option value='"+i+"'>"+window.Rest_Loc.ltg_times.pickup_dates[i][0]+"</option>";
			}
			formRows += "</select></td></tr>";
			formRows += '<tr id=ltgTimes><td class="coltitle">Delivery Time</td><td class="colvalue" style="text-align:center;">';
			formRows += "<select id='form_time' style='width:171px;'>";
			formRows += "<option value='' selected disabled></option>";
			formRows += "</select></td></tr>";
		}else{
			formRows += '<tr id=ltgDate><td class="coltitle">Pickup Dates</td><td class="colvalue" style="text-align:center;">';
			formRows += "<select id='form_date' style='width:171px;' onchange='GenerateDeliveryTimes();'>";
			for(var i = 0; i < window.Rest_Loc.ltg_times.pickup_dates.length; i++){
				formRows += "<option value='"+i+"'>"+window.Rest_Loc.ltg_times.pickup_dates[i][0]+"</option>";
			}
			formRows += "</select></td></tr>";
			formRows += '<tr id=ltgTimes><td class="coltitle">Pickup Time</td><td class="colvalue" style="text-align:center;">';
			formRows += "<select id='form_time' style='width:171px;'>";
			formRows += "<option value='' selected disabled></option>";
			formRows += "</select></td></tr>";
		}
		//formRows += '<tr><td class="coltitle">Notes</td><td class="colvalue"><input id="form_notes" size="25" type="text"></td></tr>';
		formRows += '</table><br>';
		checkoutElement.innerHTML += formRows;
		GenerateDeliveryTimes();
		var tempButton;
		if( ltg_payment_option & LAVUTOGO_PAYMENT_ON_PICKUP ) {
			tempButton = document.createElement("button");
			tempButton.className = 'greenButton';
			tempButton.appendChild(document.createTextNode('Pay at ' + window.wording_pickup));
			tempButton.onclick = function( event ){CheckoutFormSubmit(0);};
			checkoutElement.appendChild(tempButton);
			checkoutElement.appendChild(document.createTextNode('\u00a0\u00a0\u00a0\u00a0') );
		}
		if( ltg_payment_option & LAVUTOGO_PRE_PAY ) {
			tempButton = document.createElement("button");
			tempButton.className = 'greenButton';
			if(ltg_payment_option & LAVUTOGO_PAYMENT_ON_PICKUP){
				tempButton.appendChild(document.createTextNode('Pay Now'));
			}else{
				tempButton.appendChild(document.createTextNode('Place Order'));
			}
			tempButton.onclick = function( event ){CheckoutFormSubmit(3);};
			checkoutElement.appendChild(tempButton);
		}
		checkoutElement.appendChild(document.createElement('br'));
		checkoutElement.appendChild(document.createElement('br'));
		tempButton = document.createElement("button");
		tempButton.className = 'redButton';
		tempButton.appendChild(document.createTextNode("Cancel"));
		tempButton.onclick = function( event ){CloseMod();};
		checkoutElement.appendChild(tempButton);
		checkoutElement.appendChild(document.createElement('br'));
		checkoutElement.appendChild(document.createElement('br'));
	}

	function GenerateDeliveryTimes(){
		var tempDate = document.getElementById('form_date').value;
		var formRows = '';
		//formRows = "<option value='' selected disabled></option>";
		for(var i = 0; i < window.Rest_Loc.ltg_times.pickup_times.length; i++){
			if(tempDate === null || tempDate < 1){
				if(window.Rest_Loc.ltg_times.pickup_times[i][2] == 1){
					formRows += "<option value='"+i+"'>"+window.Rest_Loc.ltg_times.pickup_times[i][1]+"</option>";
				}
			}else{
				formRows += "<option value='"+i+"'>"+window.Rest_Loc.ltg_times.pickup_times[i][1]+"</option>";
			}
		}
		document.getElementById('form_time').innerHTML = formRows;
	}

	function CheckoutFormSubmit(payOption){
		delete window.Customer_Info;
		window.Customer_Info = [];
		var error = false;
		var error_out = '';
		var tempInput = '';
		tempInput = document.getElementById('form_firstname').value;
		if(tempInput.length < 1){
			error_out += "Please enter a first name\n";
			error = true;
		}else{
			window.Customer_Info.firstName = tempInput;
		}
		tempInput = document.getElementById('form_lastname').value;
		if(tempInput.length < 1){
			error_out += "Please enter a last name\n";
			error = true;
		}else{
			window.Customer_Info.lastName = tempInput;
		}
		tempInput = document.getElementById('form_phone').value.replace(/\D/g,'');
		if(tempInput.length < 7){
			error_out += "Please enter a valid phone number\n";
			error = true;
		}else{
			window.Customer_Info.phoneNumber = tempInput;
		}
		tempInput = document.getElementById('form_email').value;
		if( (tempInput.length < 1) || (tempInput.indexOf("@") < 1) || (tempInput.indexOf("@") == (tempInput.length -1 ) ) ){
			error_out += "Please enter a valid email\n";
			error = true;
		}else{
			window.Customer_Info.email = tempInput;
		}
		if(window.delivery_or_pickup == 'delivery' ){
			tempInput = document.getElementById('form_sAddress').value;
			if (tempInput.length < 1){
				error_out += "Please enter a valid street address\n";
				error = true;
			}else{
				window.Customer_Info.sAddress = tempInput;
			}
			tempInput = document.getElementById('form_city').value;
			if (tempInput.length < 1){
				error_out += "Please enter a city\n";
				error = true;
			}else{
				window.Customer_Info.city = tempInput;
			}
			tempInput = document.getElementById('form_state').value;
			if (tempInput.length < 1){
				error_out += "Please enter a state\n";
				error = true;
			}else{
				window.Customer_Info.state = tempInput;
			}
			tempInput = document.getElementById('form_zip').value;
			if (tempInput.length < 1){
				error_out += "Please enter a zip or postal code\n";
				error = true;
			}else{
				window.Customer_Info.zip = tempInput;
			}
		}
		tempInput = document.getElementById('form_date').value;
		if (tempInput === undefined){
			error_out += "Please select a date\n";
			error = true;
		}else{
			window.Customer_Info.date = window.Rest_Loc.ltg_times.pickup_dates[tempInput][0];
		}
		tempInput = document.getElementById('form_time').value;
		if (tempInput === undefined){
			error_out += "Please select a time\n";
			error = true;
		}else{
			window.Customer_Info.formTime = window.Rest_Loc.ltg_times.pickup_times[tempInput][0];
		}
		/*
		tempInput = document.getElementById('form_notes').value;
		if (tempInput.length > 140){
			error_out += "Notes are limited to 140 Characters.\n";
			error = true;
		}else{
			window.Customer_Info.notes = tempInput;
		}*/
		if(error){
			//Don't continue to payment screen.
			alert(error_out);
			return;
		}

		if(payOption === 0){
			ProcessOrder(0);
		}
		if(payOption === 3){
			ProcessOrder(3);
		}
	}

	function ProcessOrder(payOption){
		if(window.blocker === 1){
			return;
		}else{
			window.blocker = 1;
		}
		window.orderSavePost = [];
		if(window.delivery_or_pickup == 'delivery' ){
			orderSavePost.push(new Array("street_address",window.Customer_Info.sAddress) );
			orderSavePost.push(new Array("postal_code"   ,window.Customer_Info.zip) );
			orderSavePost.push(new Array("city"          ,window.Customer_Info.city) );
			orderSavePost.push(new Array("state"         ,window.Customer_Info.state) );
			orderSavePost.push(new Array("is_delivery",1) );
		}else{
			orderSavePost.push(new Array("is_delivery",0) );
		}
		orderSavePost.push(new Array("firstname"       ,window.Customer_Info.firstName) );
		orderSavePost.push(new Array("lastname"        ,window.Customer_Info.lastName) );
		orderSavePost.push(new Array("phone"           ,window.Customer_Info.phoneNumber) );
		orderSavePost.push(new Array("email"           ,window.Customer_Info.email) );
		orderSavePost.push(new Array("togo_date"       ,window.Customer_Info.date ) );
		orderSavePost.push(new Array("togo_time_today" ,window.Customer_Info.formTime) );
		orderSavePost.push(new Array("togo_time_future",window.Customer_Info.formTime) );
		orderSavePost.push(new Array("ltg_name",window.LTG_Name   ) );
		orderSavePost.push(new Array("data_name",window.Data_Name ) );
		orderSavePost.push(new Array("loc_id",window.Loc_ID ) );
		orderSavePost.push(new Array("taxrate",window.Loc_Taxrate) );
		orderSavePost.push(new Array("tax",(LavuToGo.NumberFormatter.createNumberFormatter().toFormattedString(window.matsBadTax ) ) ) );
		orderSavePost.push(new Array("today_date",window.Rest_Loc.ltg_times.now_date ) );
		orderSavePost.push(new Array("checkout",1) ); // Actually checking out and not just saving to server
		orderSavePost.push(new Array("subtotal"     ,LavuToGo.NumberFormatter.createNumberFormatter().toFormattedString(window.Shopping_Cart.subtotal() ) ) );
		orderSavePost.push(new Array("total"        ,LavuToGo.NumberFormatter.createNumberFormatter().toFormattedString(window.Shopping_Cart.subtotal() + window.matsBadTax ) ) );
		orderSavePost.push(new Array("cart_contents",window.Shopping_Cart.toLavuString() ) );
		SaveOrder( payOption);
	}

	function SaveOrder( set_void){
		window.paymentOption = set_void;
		var myModBox = document.getElementById('mod_box');
		myModBox.innerHTML = '<br><h1>Processing...</h1><br>';
		var xmlhttp =  BuildAJAXObject();
		xmlhttp.open('POST', window.Base_URL+'save_order.php', true);
		xmlhttp.onreadystatechange = function() {
			if (xmlhttp.readyState == 4) {
				if (xmlhttp.status == 200) {
					OrderSuccessful(xmlhttp.responseText);
				} else {
					myModBox.innerHTML = '<br><h1>Something went wrong.</h1><br>';
					var tempButton = document.createElement("button");
					tempButton.className = 'redButton';
					tempButton.appendChild(document.createTextNode('Close') );
					tempButton.onclick = function( event ){CloseMod();window.blocker = 0;};
					myModBox.appendChild(tempButton);
					myModBox.appendChild(document.createElement('br'));
					myModBox.appendChild(document.createElement('br'));
				}
			}
		};
		xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		window.saveOrderVars = "";
		for(i=0; i < window.orderSavePost.length; i++){ //Refactored by Brian D. Now checks if element exists.
			if(saveOrderVars.length > 1){
				saveOrderVars += "&";
			}
			saveOrderVars += encodeURIComponent(window.orderSavePost[i][0]) + "=" + encodeURIComponent(window.orderSavePost[i][1]);
		}
		//if(order_id){
		//	vars += "&order_id=" + order_id;
		//}
		if( set_void !== undefined && set_void !== null ){
			saveOrderVars += "&set_void=" + set_void;
		}
		xmlhttp.send(saveOrderVars);
	}

	function OrderSuccessful(ltgResponse){
		var splitResponse = ltgResponse.split('|');
		if(splitResponse[0] != 1){
			OrderFail(ltgResponse);
			return;
		}
		var myModBox = document.getElementById('mod_box');
		myModBox.innerHTML = '<br><h1 id="saveStatus" >Order Saved.<br>Order ID: '+splitResponse[1]+'</h1><br>';
		var tempButton = document.createElement("button");
		tempButton.className = 'greenButton';
		tempButton.appendChild(document.createTextNode('Ok') );
		tempButton.onclick = function( event ){CloseMod();window.blocker = 0;};
		myModBox.appendChild(tempButton);
		myModBox.appendChild(document.createElement('br'));
		myModBox.appendChild(document.createElement('br'));
		NewClearCart();
		ShowCartContents();
		console.log(window.paymentOption);
		if(window.paymentOption > 0){
			myModBox.innerHTML = '<br><h3 id="saveStatus" >Redirecting to secure payment platform.<br>Order ID: '+splitResponse[1]+'</h3><br>';
			OrderMakePayment(ltgResponse,splitResponse[1]);
		}else{
			document.getElementById('saveStatus').innerHTML = 'Order Completed<br>Order ID: '+splitResponse[1];
		}
	}

	function OrderMakePayment(ltgResponse, order_id)
	{
		window.orderSavePost.push(new Array( "order_id", order_id ));
		window.orderSavePost.push(new Array( "is_dev", window.IsDevAccount ));
		window.orderSavePost.push(new Array( "is_ltg", "1" ));
		window.orderSavePost.push(new Array( "ltg_debug", window.LTG_Debug ));
		
		var url = window.Payment_URL + "/lib/gateway_lib/hosted_checkout/entry_point.php";

		var formString = '<form id="payment_redirection" action="' + url + '" method="post" style="display:none;">';
		formString += '<input type="text" name="return_url" value="' + window.Base_URL + window.LTG_Name + '" />';
		for (i = 0; i < window.orderSavePost.length; i++)
		{ //Refactored by Brian D. Now checks if element exists.
			formString += '<input type="text" name="' + encodeURIComponent(window.orderSavePost[i][0]) + '" value="' + encodeURIComponent(window.orderSavePost[i][1]) + '" />';
		}
		formString += '</form>';

		document.getElementById('mod_box').innerHTML += formString;
		document.getElementById('payment_redirection').submit();
	}

	function OrderFail(ltgResponse){
		var myModBox = document.getElementById('mod_box');
		myModBox.innerHTML = '<br><h1>Something went wrong.</h1><br><p>'+ltgResponse+'</p>';
		var tempButton = document.createElement("button");
		tempButton.className = 'redButton';
		tempButton.appendChild(document.createTextNode('Close') );
		tempButton.onclick = function( event ){CloseMod();window.blocker = 0;};
		myModBox.appendChild(tempButton);
		myModBox.appendChild(document.createElement('br'));
		myModBox.appendChild(document.createElement('br'));
		setTimeout( window.location.reload(), 3000 );
	}
}

//
// Modifiers Choosing (Done)
//


function ItemImagePath(itemID){
	var myItem = LavuToGo.MenuItems.menuItems().getMenuItemByID(itemID);
	var imageName = myItem._image;
	if(imageName.length < 1){
		return '/assets/images/placeholder.png';
	}
	var imagePath = "//admin.poslavu.com/images/"+window.Data_Name+"/items/main/"+escape(imageName);
	return imagePath;
}

{
	function AskOptModQuestion(itemID){
		var form_str ='';
		var currentMod = window.Item_Opt_Mods[window.Item_Opt_Mods.length -1];
		type = currentMod._type;
		form_str ='<div id="AskChoice">';
		form_str +='<img src="'+ItemImagePath(itemID)+'">'+"\n";
		form_str += "<p id='ModQuestionHeader'>"+escapeHtml(LavuToGo.MenuItems.menuItems().getMenuItemByID(itemID)._name)+"</p>\n";
		form_str +='<form id="checklistForm">';
		for (var i = 0; i < currentMod._options.length; i++) {
			if(currentMod._options[i]._price > 0){
				tempCost = LavuToGo.NumberFormatter.createNumberFormatter().toMonitaryString( currentMod._options[i]._price );
				form_str += "<p><input type='checkbox' name='"+i+"'>"+escapeHtml(currentMod._options[i]._title)+" ("+escapeHtml(tempCost)+")</p>\n";
			}else{
				form_str += "<p><input type='checkbox' name='"+i+"'>"+escapeHtml(currentMod._options[i]._title)+"</p>\n";
			}
		}
		form_str += "<p>&nbsp;</p></form></div><button class='greenButton' onclick='AnswerOptModQuestion("+itemID+");'>OK</button><br><br>\n";
		form_str += "<button class='redButton' onclick='CloseMod();'>Cancel</button>\n<br>\n<br>\n<br>\n";
		OpenModbox(650,600,form_str);
		document.getElementById('mod_box').style.height = 'auto';
	}

	function AnswerOptModQuestion(itemID){
		var x = document.getElementById("checklistForm");
		var modIndexes = new Array();
		for (var i = 0; i < x.length; i++) {
			if( (x.elements[i].type=='checkbox') && (x.elements[i].checked !== false) ){
				modIndexes.push(x.elements[i].name);
			}
		}
		CloseMod();
		var current_mod = window.Item_Opt_Mods[window.Item_Opt_Mods.length -1];
		for (var j = 0; j < modIndexes.length; j++) {
			window.Item_Opt_Mods_Choices.push(current_mod._options[modIndexes[j]]);
		}
		window.Item_Opt_Mods.pop();
		ItemProcessor(itemID);
	}

	function AskModQuestion(itemID){
		var form_str ='';
		current_mod = window.Item_Force_Mods[window.Item_Force_Mods.length -1];
		type = current_mod._type;
		if (type=="seat" || type=="course" ||type=="gift_card" ||type=="loyalty_card" || type=="custom" || type=="weight"){
			window.Item_Force_Mods.pop();
			ItemProcessor(itemID);
			return;
		}
		//form_str += "<p>"+type+"</p>\n";
		if(type == 'checklist'){
			form_str += AskChecklist(itemID,current_mod);
		}
		if(type == 'choice'   ){
			form_str += AskChoice(   itemID,current_mod);
		}
		if(type == 'quantity'   ){
			form_str += AskQuantity( itemID,current_mod);
		}
		form_str += "<button class='redButton' onclick='CloseMod();'>Cancel</button>\n<br>\n<br>\n<br>\n";
		OpenModbox(650,600,form_str);
		document.getElementById('mod_box').style.height = 'auto';
		return;
	}

	function AskChecklist(itemID,currentMod){
		str ='<div id="AskChoice">';
		str +='<img src="'+ItemImagePath(itemID)+'">'+"\n";
		str += "<p id='ModQuestionHeader'>"+escapeHtml(LavuToGo.MenuItems.menuItems().getMenuItemByID(itemID)._name)+"</p>\n";
		str +='<form id="checklistForm">';
		for (var i = 0; i < currentMod._options.length; i++) {
			if(currentMod._options[i]._detour.length < 1){
				if(currentMod._options[i]._cost > 0){
					tempCost = LavuToGo.NumberFormatter.createNumberFormatter().toMonitaryString( currentMod._options[i]._cost );
					str += "<p><input type='checkbox' name='"+i+"'>"+escapeHtml(currentMod._options[i]._title)+" ("+escapeHtml(tempCost)+")</p>\n";
				}else{
					str += "<p><input type='checkbox' name='"+i+"'>"+escapeHtml(currentMod._options[i]._title)+"</p>\n";
				}
			}
		}
		str += "<p>&nbsp;</p></form>\n<button onclick='AnswerModChecklist("+itemID+");'>OK</button></div><br><br>\n";
		return str;
	}

	function AskChoice(itemID,currentMod){
		str ='<div id="AskChoice">';
		str +='<img src="'+ItemImagePath(itemID)+'">'+"\n";
		str += "<p id='ModQuestionHeader'>"+escapeHtml(LavuToGo.MenuItems.menuItems().getMenuItemByID(itemID)._name)+"</p>\n";
		for (var i = 0; i < currentMod._options.length; i++) {
			if(currentMod._options[i]._detour.length < 1){
				if(currentMod._options[i]._cost > 0){
					tempCost = LavuToGo.NumberFormatter.createNumberFormatter().toMonitaryString( currentMod._options[i]._cost );
					str += "<button onclick='AnswerModChoice("+itemID+","+i+");'>"+escapeHtml(currentMod._options[i]._title)+" ("+escapeHtml(tempCost)+")</button><br>\n";
				}else{
					str += "<button onclick='AnswerModChoice("+itemID+","+i+");'>"+escapeHtml(currentMod._options[i]._title)+"</button><br>\n";
				}
			}else{
				str += "<button onclick='ModDetour("+itemID+","+i+");'>"+escapeHtml(currentMod._options[i]._title)+"</button><br>\n";
			}
		}
		str +='</div><br><br>';
		return str;
	}

	function AskQuantity(itemID,currentMod){
		str  ='<div id="AskChoice"><img src="/assets/images/placeholder.png">';
		str += "<p id='ModQuestionHeader'>"+escapeHtml(LavuToGo.MenuItems.menuItems().getMenuItemByID(itemID)._name)+"</p>\n";
		str += '<p id="QuantityChooser">Quantity:'+"\n";
		str += '<input id="quantityCounter" type="number" min="1" max="10" value="1"></input>'+"\n";
		str += "</p>\n";
		str += '<button onclick="AnswerModQuantity('+itemID+',document.getElementById('+"'quantityCounter'"+').value);">'+"OK</button></div><br>\n";
		return str;
	}

	function ModDetour(itemID,modIndex){
		CloseMod();
		var current_mod = window.Item_Force_Mods[window.Item_Force_Mods.length -1];
		current_detour = current_mod._options[modIndex]._detour;
		// TODO add mod to cart
		window.Item_Force_Mods_Choices.push(current_mod._options[modIndex]);
		window.Item_Force_Mods.pop();
		if(current_detour.charAt(0)=="l") { // detour to forced mod list
			GetForcedModList( current_detour.slice(1));
		} else if(current_detour.charAt(0)=="g") { // detour to forced mod group
			GetForcedModGroup(current_detour.slice(1));
		}
		ItemProcessor(itemID);
	}

	function AnswerModChoice(itemID,modIndex){
		CloseMod();
		var current_mod = window.Item_Force_Mods[window.Item_Force_Mods.length -1];
		// TODO add mod to cart
		window.Item_Force_Mods_Choices.push(current_mod._options[modIndex]);
		window.Item_Force_Mods.pop();
		ItemProcessor(itemID);
	}

	function AnswerModChecklist(itemID){
		var x = document.getElementById("checklistForm");
		var modIndexes = new Array();
		for (var i = 0; i < x.length; i++) {
			if( (x.elements[i].type=='checkbox') && (x.elements[i].checked === true) ){
				modIndexes.push(x.elements[i].name);
			}
		}
		CloseMod();
		var current_mod = window.Item_Force_Mods[window.Item_Force_Mods.length -1];
		for (var j = 0; j < modIndexes.length; j++) {
			// TO add mod to cart modIndexes[i]
			window.Item_Force_Mods_Choices.push(current_mod._options[modIndexes[j]]);
		}
		window.Item_Force_Mods.pop();
		ItemProcessor(itemID);
	}

	function AnswerModQuantity(itemID,myQuantity){
		CloseMod();
		var current_mod = window.Item_Force_Mods[window.Item_Force_Mods.length -1];
		if(myQuantity > 0){
			window.Item_Quantity = myQuantity;
		}
		window.Item_Force_Mods.pop();
		ItemProcessor(itemID);
	}
}

//
// Get Modifiers (Done)
//
{
	function GetOptionalMods(itemID){
		omod_id = LavuToGo.MenuItems.menuItems().getMenuItemByID(itemID)._modifier_list_id;
		//initialize Item_Opt_Mods
		window.Item_Opt_Mods = new Array();
		for (var i = 0; i < window.Optional_Mod_List.length; i++) {
			if(window.Optional_Mod_List[i]._id == omod_id){
				window.Item_Opt_Mods.push( window.Optional_Mod_List[i] );
			}
		}
		if(window.Item_Opt_Mods.length > 0){
			return true;
		}else{
			return false;
		}
	}

	function GetForcedMods(itemID){
		fmod_id = LavuToGo.MenuItems.menuItems().getMenuItemByID(itemID)._forced_modifier_group_id;
		//initialize Item_Force_Mods
		window.Item_Force_Mods = new Array();
		//If no item force mod id then get category
		if((fmod_id.length < 1) || (fmod_id == 'C') || (fmod_id == '0')){
			category_id = LavuToGo.MenuItems.menuItems().getMenuItemByID(itemID)._category_id;
			fmod_id = LavuToGo.MenuCategory.getMenuCategoryByID(category_id)._forced_modifier_group_id;
			//If no category force mod id then we are done
			if((fmod_id.length < 1) || (fmod_id == 'C') || (fmod_id == '0')){
				return false;
			}
		}
		if('f' == fmod_id.charAt(0)){
			//F means it is already a list
			GetForcedModList(fmod_id.slice(1));
		}else{
			GetForcedModGroup(fmod_id);
		}
		if(window.Item_Force_Mods.length > 0){
			return true;
		}else{
			return false;
		}
	}

	function GetForcedModGroup(fmod_group_id){
		fmod_lists = '';
		for (var i = 0; i < window.Forced_Mod_Groups_List.length; i++) {
			temp_id = window.Forced_Mod_Groups_List[i].id;
			if(fmod_group_id == temp_id){
				fmod_lists = window.Forced_Mod_Groups_List[i].include_lists;
				i = window.Forced_Mod_Groups_List.length;
			}
		}
		fmod_list_array = fmod_lists.split('|');
		for (var j = fmod_list_array.length; j >= 0 ; j--) {
			GetForcedModList(fmod_list_array[j]);
		}
		return true;
	}

	function GetForcedModList(fmod_list_id){
		for (var i = 0; i < window.Forced_Mod_List.length; i++) {
			temp_id = window.Forced_Mod_List[i]._id;
			if(fmod_list_id == temp_id){
				window.Item_Force_Mods.push( window.Forced_Mod_List[i]);
				return true;
			}
		}
		return false;
	}
}

//
// Cart Management (Done)
//
{
	function showPizzaCreatorModComponent(menu_item_id){
		window.pizza_menu_item_id = menu_item_id;
		var htmlStr = '';
		//htmlStr += "<form target='pizzaComponentIFrame'></form>";
		htmlStr += "<iframe style='width:100%;height:100%;name='pizzaComponentIFrame' src='./components/index.php?dataname="+ltg_dataname+"&cmp=pizza&item_id="+menu_item_id+"&sec="+ltg_sec+"' frameborder='0' allowtransparency='true'></iframe>";
		OpenModbox(800,575,htmlStr);
		document.getElementById('mod_box').style.marginLeft = -5;
	}

	function InitializePizzaAdHocForcedModifiersFromSession(){
		if( typeof(window.additional_session_info_obj) != 'undefined'){
			if (typeof(window.additional_session_info_obj['additional_f_mod_info']) != 'undefined'){
				var specialModifiers = window.additional_session_info_obj['additional_f_mod_info'];
				for(var i = 0; i < specialModifiers.length; i++){
					var currForcedModObject = specialModifiers[i];
					var retVal = LavuToGo.ForcedModifierOption.createForcedModifierOption(currForcedModObject, currForcedModObject['id']);
				}
			}
		}
	}


	window.exit_pizza_component = function(){
		window.pizza_menu_item_id = null;
		//close_mod();
		CloseMod();
	}

	window.save_pizza_to_order = function(pizza_builder_info){

		//pizza_builder_info has ['mod_title'], ['mod_price'], ['mod_id'], and ['mod_new_line']

		//alert('pizza builder info:' + JSON.stringify(pizza_builder_info));
		//add_to_cart(itemid,mod_answers,omod_answers);
		var modIDsArr = pizza_builder_info['mod_id'];//array of mod ids.

		menu_item_id = window.pizza_menu_item_id;

		//We need to create a special forced modifier for *whole, *left, *right.
		//createForcedModifierOption( json, forced_modifier_id );
		var wholeFModPlaceholder = LavuToGo.ForcedModifierOption.createForcedModifierOption( {'id':-1,'title':'-- Whole --','cost':0,'detour':0}, -1);
		var rightFModPlaceholder = LavuToGo.ForcedModifierOption.createForcedModifierOption( {'id':-2,'title':'-- Right --','cost':0,'detour':0}, -2);
		var leftFModPlaceholder  = LavuToGo.ForcedModifierOption.createForcedModifierOption( {'id':-3,'title':'-- Left --' ,'cost':0,'detour':0}, -3);

		//window.Item_Force_Mods_Choices.push(wholeFModPlaceholder);
		//window.Item_Force_Mods_Choices.push(rightFModPlaceholder);
		//window.Item_Force_Mods_Choices.push(leftFModPlaceholder);

		//To Store on the session so we can recreate the special forced mods, which are Whole, Right, Left and all pizza_mods portions (which have special prices).
		var additional_f_mod_info = [];

		//To be stored on session (after ajax call) so that when page reloads the new forced mods exist.
		additional_f_mod_info.push({'id':-1,'title':'-- Whole --','cost':0,'detour':0});
		additional_f_mod_info.push({'id':-2,'title':'-- Right --','cost':0,'detour':0});
		additional_f_mod_info.push({'id':-3,'title':'-- Left --' ,'cost':0,'detour':0});

		var temp_map_4_portion = {'*whole':-1,'*right':-2,'*left':-3};

		var arrayForPassingToAddToCart = [];
//TODO APPEND ALL TO THE GLOBAL window.Item_Force_Mods_Choices
//TODO APPEND ALL TO THE GLOBAL window.Item_Opt_Mods_Choices
		//----
		var portionMode = 'whole';
		for(var i = 0; i < modIDsArr.length; i++){
			var forced_mod_id = modIDsArr[i];
			if(forced_mod_id == 0){
				var forced_mod_title = pizza_builder_info['mod_title'][i];
				forced_mod_id = temp_map_4_portion[forced_mod_title];
				if(forced_mod_id == -1){
					window.Item_Force_Mods_Choices.push(wholeFModPlaceholder);
				}else if(forced_mod_id == -2){
					window.Item_Force_Mods_Choices.push(rightFModPlaceholder);
				}else if(forced_mod_id == -3){
					window.Item_Force_Mods_Choices.push(leftFModPlaceholder);
				}
			}else{
				var id_adjust = pizza_builder_info['mod_title'][i] == '*whole' ? 0 : 0.5;
				var modifier_id = pizza_builder_info['mod_id'][i] + id_adjust;
				var currCustomForcedModifier = LavuToGo.ForcedModifierOption.createForcedModifierOption( {'id':modifier_id,'title':pizza_builder_info['mod_title'][i],'cost':pizza_builder_info['mod_price'][i],'detour':0}, modifier_id);
				window.Item_Force_Mods_Choices.push(currCustomForcedModifier);
				//In case refresh, this is to be stored on session to recreate the JS forced mod option.
				additional_f_mod_info.push({'id':modifier_id,'title':pizza_builder_info['mod_title'][i],'cost':pizza_builder_info['mod_price'][i],'detour':0});
				forced_mod_id = modifier_id;
			}
			//arrayForPassingToAddToCart.push([forced_mod_id,forced_mod_id,forced_mod_id]);
		}



		///var additional_session_info = {};
		///additional_session_info.additional_f_mod_info = additional_f_mod_info;
		window.additional_session_info_obj = {};
		window.additional_session_info_obj.additional_f_mod_info = additional_f_mod_info;

		//AddToCart(menu_item_id, arrayForPassingToAddToCart, [], 1, additional_session_info);


		AddToCart(menu_item_id); //HERE UNCOMMENT THIS <----
		//close_mod();
		CloseMod();
	}
	// ************************************************* end pizza creator functions
	//End Insert.
	function MenuItemClick(itemID){
		window.Item_Force_Mods_Choices = new Array();
		window.Item_Opt_Mods_Choices = new Array();
		window.Item_Quantity = 1;
		//Inserted by Brian D.
		if(ltg_menu_items_that_use_pizza_creator[itemID] != 'undefined' && ltg_menu_items_that_use_pizza_creator[itemID] > 0){
			showPizzaCreatorModComponent(itemID);
			return;
		}
		//End Insert..........

		GetForcedMods(itemID);
		GetOptionalMods(itemID);
		ItemProcessor(itemID);
	}

	function ItemProcessor(itemID){
		if(window.Item_Force_Mods.length > 0){
			AskModQuestion(itemID);
		}else if((window.Item_Opt_Mods.length > 0) && (window.Location_Info.ltg_use_opt_mods() != "0" ) ){
			AskOptModQuestion(itemID);
		}else{
			AddToCart(itemID);
		}
	}

	function AddToCart(itemID){
		item_entry = LavuToGo.CartEntry.createCartEntry( itemID, window.Item_Force_Mods_Choices, window.Item_Opt_Mods_Choices );
		if(window.Item_Quantity > 0){
			item_entry.setQuantity( Number(item_entry.quantity()) + Number(window.Item_Quantity) );
		}else{
			item_entry.setQuantity( Number(item_entry.quantity()) + Number(1) );
		}
		window.Item_Quantity = 0;
		window.Shopping_Cart.addEntry( item_entry );
		ShowCartContents();
		StoreCartContents();
	}

	function EditCartItem(rowID) {
		var entry = window.Shopping_Cart.getEntryForItemRow( rowID );
		if( !entry ){
			return;
		}
		var item_name = entry.item().name();
		var item_qty = entry._quantity;
		var maxqty = (item_qty * 1) + 15;
		var item_cost = LavuToGo.NumberFormatter.createNumberFormatter().toMonitaryString( entry.subtotal() );
		var item_mods = entry.forcedModifiers();
		var mods_list = [];
		var mods_price = '';
		var mod_cost = 0;
		if(item_mods.length > 0){
			for(m=0; m < item_mods.length; m++){
				mods_list.push(item_mods[m].title());
				if(item_mods[m]._cost > 0){
					mod_cost += Number(item_mods[m].cost());
				}
			}
			if(mod_cost > 0){
				mods_price = LavuToGo.NumberFormatter.createNumberFormatter().toMonitaryString( mod_cost );
			}
		}

		box_str = '<div id="editItemBox">';
		box_str += '<p><h1 id="itemBox_ItemName">'+escapeHtml(item_name)+'</h1></p>';
		box_str += '<p>QTY';
		box_str += '<input type="number" onchange="SetItemQuantity('+rowID+',this.value,this);" min="1" max="'+maxqty+'" value="'+item_qty+'"><br>'+"\n";
		box_str += '</p>'+"\n";
		box_str += '<p id="itemBox_ItemCost">Cost:'+escapeHtml(item_cost)+'</p>'+"\n";
		box_str += '<div>'+"\n";
		if(mods_list.length > 0){
			box_str += '<p>Options/Mods:</p>'+"\n";
			box_str += ListtoULLI(mods_list,false);
		}
		if(mod_cost > 0){
			box_str += '<br>Options:'+escapeHtml(mods_price);
		}
		box_str += '</div>'+"\n";
		box_str += '<p><button class="redButton" onclick="RemoveItem('+rowID+'); CloseMod();">Remove Item</button></p>'+"<br>\n";
		box_str += '<p><button class="redButton" onclick="CloseMod();">Close</button></p></div>'+"\n";

		OpenModbox(300,400,box_str);
		document.getElementById('mod_box').style.height = "auto";
		document.getElementById('mod_box').style.width = "auto";

	}

	function SetItemQuantity(itemRow,newQuantity,inputBox){
		var cart_entry = window.Shopping_Cart.getEntryForItemRow( itemRow );
		if( cart_entry ){
			if( newQuantity < 1 ){
				alert('Quantity must be positive.');
				inputBox.value = 1;
				SetItemQuantity(itemRow,1,inputBox);
				return;
			} else {
				cart_entry.setQuantity( Math.floor(newQuantity) );
			}
		}
		StoreCartContents();
		ShowCartContents();
	}

	function RemoveItem(itemRow){
		var cart_entry = window.Shopping_Cart.getEntryForItemRow( itemRow );
		window.Shopping_Cart.removeEntry( cart_entry );
		StoreCartContents();
		ShowCartContents();
		CloseMod();
		return;
	}

	function ShowCartContents(){
		var Stuff = window.Shopping_Cart._entries;
		tempString = '';
		for (var i = 0; i < Stuff.length; i++) {
			var temp = Stuff[i];
			tempString += "<div>\n";
			tempString += "<span>\n";
			tempString += "<span >\n";
			tempString += "<span class='edit'><button onclick=EditCartItem("+i+")>Edit</button></span>";
			tempString += "<span class='quantity'>"+temp._quantity+"&nbsp;x&nbsp;</span>";
			tempString += "</span>\n";
			tempString += "<span class='itemName'>"+escapeHtml(temp._item._name)+"</span>\n";
			tempString += "</span>\n";
			tempString += "<span class='itemtotal'>"+escapeHtml(LavuToGo.NumberFormatter.createNumberFormatter().toMonitaryString( temp.subtotal() ))+"</span><br>\n";
			mod_cost   = 0;
			omod_cost  = 0;
			force_mods = temp.forcedModifiers();
			opt_mods   = temp.optionalModifiers();
			for(m=0; m<force_mods.length; m++){
				tempString += "<span class='modifiers'>•&nbsp;"+escapeHtml(force_mods[m].title())+"</span>\n";
				if(force_mods[m]._cost > 0){
					tempCost = LavuToGo.NumberFormatter.createNumberFormatter().toMonitaryString( force_mods[m]._cost );
					tempString += "<span class='modifierPrices'>("+escapeHtml(tempCost)+")</span>";
				}
				tempString += "<br>\n";
			}
			for(m=0; m<  opt_mods.length; m++){
				tempString += "<span class='modifiers'>•&nbsp;"+escapeHtml(opt_mods[m].title())+"</span>";
				if(opt_mods[m]._cost > 0){
					tempCost = LavuToGo.NumberFormatter.createNumberFormatter().toMonitaryString( opt_mods[m]._cost );
					tempString += "<span class='modifierPrices'>("+escapeHtml(tempCost)+")</span>";
				}
				tempString += "<br>\n";
			}
			tempString += '</div>';
		}
		window.subtotal = window.Shopping_Cart.subtotal(); //Added to fix undefined error because of global being referenced.
		tempString += "<hr><p class='subtotal' >Subtotal: "+escapeHtml(LavuToGo.NumberFormatter.createNumberFormatter().toMonitaryString( window.Shopping_Cart.subtotal() ) )+"</p>\n";
		tempString += CartTax();
		tempString += "<p class='subtotal' >Total: "+escapeHtml(LavuToGo.NumberFormatter.createNumberFormatter().toMonitaryString( window.Shopping_Cart.subtotal() + window.matsBadTax ) )+"</p>\n";
		document.getElementById('Notepad').innerHTML = tempString;
	}

	function CartTax(){
		var setcart = '';
		LavuToGo.TaxBreakdown.resetBreakdowns();
		LavuToGo.TaxBreakdown.resetBreakdownsIncluded();
		var subtotal = Shopping_Cart.subtotal();
		var totalled = Shopping_Cart.total();
		var total_cost = LavuToGo.NumberFormatter.createNumberFormatter().undoPrecision( totalled );
		var taxed = LavuToGo.NumberFormatter.createNumberFormatter().undoPrecision( totalled - subtotal );
		var taxed_amount = LavuToGo.NumberFormatter.createNumberFormatter().toMonitaryString( totalled );
		var tax_breakdowns = LavuToGo.TaxBreakdown.getBreakdowns();
		var tax_breakdowns_included = LavuToGo.TaxBreakdown.getBreakdownsIncluded();
		window.matsBadTax = 0;
		if( !tax_breakdowns.length && !tax_breakdowns_included.length ){
			setcart += "<p class='subtotal'>No Tax: " + escapeHtml(LavuToGo.NumberFormatter.createNumberFormatter().toMonitaryString(Number(0)))+ "</p>\n";
		} else {
			var i = 0;
			for(i = 0; i < tax_breakdowns.length; i++ ){
				var tax_breakdown = tax_breakdowns[i];
				var tax_name = escapeHtml(tax_breakdown.taxName());
				var percentage = escapeHtml((tax_breakdown.taxRate()*100).toFixed( 2 ).toString().replace(/\D/,Location_Info.decimal_char()));
				var considiered_amount = escapeHtml(LavuToGo.NumberFormatter.createNumberFormatter().toMonitaryString( tax_breakdown.amount() ) );
				var tax_rate_amount = escapeHtml(LavuToGo.NumberFormatter.createNumberFormatter().toMonitaryString( Math.round(tax_breakdown.amount() * tax_breakdown.taxRate()) ) );
				setcart += "<p class='subtotal'>" + tax_name + " (" + percentage + "% of " + considiered_amount + "): " + tax_rate_amount + "</p>\n";
				matsBadTax += tax_breakdown.taxTotal();
			}
		}
		if(tax_breakdowns_included.length){
			setcart += "<p>INCLUDED TAXES</p>\n";
		}
		for(i = 0; i < tax_breakdowns_included.length; i++ ){
			var tax_breakdown = tax_breakdowns_included[i];
			var tax_name = escapeHtml(tax_breakdown.taxName());
			var percentage = escapeHtml((tax_breakdown.taxRate()*100).toFixed( 2 ).toString().replace(/\D/,Location_Info.decimal_char()));
			var considiered_amount = escapeHtml(LavuToGo.NumberFormatter.createNumberFormatter().toMonitaryString( tax_breakdown.amount() ) );
			var tax_rate_amount = escapeHtml(LavuToGo.NumberFormatter.createNumberFormatter().toMonitaryString( Math.round(tax_breakdown.amount() * tax_breakdown.taxRate()) ) );
			setcart += "<p class='subtotal'>" + tax_name + " (" + percentage + "% of " + considiered_amount + "): " + tax_rate_amount + "</p>\n";
		}
		return setcart;
	}
}

//
//Pop Up Box Utility Funcs (Done)
//
{
	function OpenModbox(set_width,set_height,set_content) {
		document.getElementById('mod_box').style.width = set_width + "px";
		document.getElementById('mod_box').style.height = set_height + "px";
		document.getElementById('mod_box').style.display = "block";
		document.getElementById('mod_box').innerHTML = set_content;
		if(set_width < 800){document.getElementById('mod_box').style.marginLeft = (800 - set_width)/2 ;}
		document.getElementById('grey_overlay').style.display = "block";
	}

	function CloseMod(){
		document.getElementById('mod_box').style.display = "none";
		document.getElementById('mod_box').innerHTML = '';
		document.getElementById('grey_overlay').style.display = "none";
	}
}

//
// Menu UI Generation (Done)
//
{
	function GenerateGroups(Stuff){
		var menusElement = document.getElementById("Menus");
		for (var i = 0; i < Stuff.length; i++) {
			if(Stuff[i].group_name != 'Universal'){
				var menusButton = document.createElement("button");
				menusButton.className = 'MenusButton';
				menusButton.setAttribute('data', Stuff[i].id );
				menusButton.onclick = function( event ){GenerateCategories(this.getAttribute('data'));};
				var menusText = document.createTextNode(Stuff[i].group_name);
				menusButton.appendChild(menusText);
				menusElement.appendChild(menusButton);
			}else{
				window.universalGroup = Stuff[i].id;
			}
		}
	}

	function GenerateCategories(id){
		var Stuff = LavuToGo.MenuCategory.menuCategories();
		var catsElement = document.getElementById("Categories");
		while (catsElement.firstChild) {
			catsElement.removeChild(catsElement.firstChild);
		}
		for (var i = 0; i < Stuff.length; i++) {
			if( (Stuff[i]._group_id == id) || (Stuff[i]._group_id ==  window.universalGroup) ){
				var catButton = document.createElement("button");
				catButton.className = 'CatButton';
				catButton.setAttribute('data', Stuff[i]._id );
				catButton.onclick = function( event ){GenerateItems(this.getAttribute('data'));};
				var catImg = document.createElement("img");
				catButton.appendChild(catImg);
				catButton.appendChild(document.createElement("br"));
				catButton.appendChild(document.createTextNode(Stuff[i]._name));
				catsElement.appendChild(catButton);
				CatImageLoader(catImg,i);
			}
		}
	}

	function GenerateItems(id){
		var Stuff = LavuToGo.MenuItems.menuItems()._items;
		var itemsElement = document.getElementById("ItemArea");
		while (itemsElement.firstChild) {
			itemsElement.removeChild(itemsElement.firstChild);
		}
		for (var i = 0; i < Stuff.length; i++) {
			if(Stuff[i]._category_id == id){
				var itemButton = document.createElement("button");
				itemButton.className = 'ItemButton';
				itemButton.setAttribute('data', Stuff[i]._id );
				itemButton.onclick = function( event ){MenuItemClick(this.getAttribute('data'));};
				var itemImg = document.createElement("img");
				itemButton.appendChild(itemImg);
				itemButton.appendChild(document.createElement("br"));
				itemButton.appendChild(document.createTextNode(Stuff[i]._name));
				itemsElement.appendChild(itemButton);
				ItemImageLoader(itemImg,Stuff[i]._id);
			}
		}
	}
}

//
// General Utils
//
{
	function ListtoULLI(commaList,twoColumn){
		var tempArray = commaList;
		var tempString = '';
		if(twoColumn){
			tempString = '<ul class="twoColumn">';
		}else{
			tempString = '<ul>';
		}
		for (index = 0; index < tempArray.length; ++index) {
			tempString += '<li style="text-align:left;">'+escapeHtml(tempArray[index])+'</li>';
		}
		tempString += '</ul>';
		return tempString;
	}

	var entityMap = {
		"&": "&amp;",
		"<": "&lt;",
		">": "&gt;",
		'"': '&quot;',
		"'": '&#39;',
		" ": '&nbsp;',
		"/": '&#x2F;'
	};

	function escapeHtml(string) {
		return String(string).replace(/[&<>"'\/]/g, function (s) {
			return entityMap[s];
		});
	}

	function ItemImageLoader(imgObject,itemID){
		imgObject.onload = '';
		var myItem = LavuToGo.MenuItems.menuItems().getMenuItemByID(itemID);
		var imageName = myItem._image;
		imgObject.style.backgroundColor="#8fad17";
		if(imageName.length < 1){
			imgObject.src = '/assets/images/1x1.png';
			return;
		}
		var imageURL = "//admin.poslavu.com/images/"+window.Data_Name+"/items/"+escape(imageName);
		imgObject.src = imageURL;
	}

	function CatImageLoader(imgObject,catIndex){
		imgObject.onload = '';
		var Stuff = LavuToGo.MenuCategory.menuCategories();
		var imageName = Stuff[catIndex]._image;
		imgObject.style.backgroundColor="#8fad17";
		if(imageName.length < 1){
			imgObject.src = '/assets/images/1x1.png';
			return;
		}
		var imageURL = "//admin.poslavu.com/images/"+window.Data_Name+"/categories/"+escape(imageName);
		imgObject.src = imageURL;
	}

	function BuildAJAXObject() {
		if(window.XMLHttpRequest){
			return new XMLHttpRequest();
		}
		return new ActiveXObject("Microsoft.XMLHTTP");
	}

	function StoreCartContents() {
		var xmlhttp =  BuildAJAXObject();
		xmlhttp.open('POST', window.Base_URL+'store_session.php', true);
		xmlhttp.onreadystatechange = function() {
			console.log('Saved Cart Contents');
		};
		xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		var vars = "cart_contents=" + encodeURIComponent( Shopping_Cart.serialize() )+ "&ltg_name="+window.LTG_Name;
		if(window.order_id){
			vars += "&order_id=" + window.order_id;
		}
		//Inserted by Brian D.
		if(window.additional_session_info_obj){
			vars += "&additional_session_info_obj=" + encodeURIComponent(JSON.stringify(additional_session_info_obj));
		}
		//End Insert
		xmlhttp.send(vars);
	}

	function SetPickupOrDelivery(ltg_option){
		if(ltg_option=="1"){ //Delivery And Pickup (user chooses)
			window.delivery_or_pickup = "--ASK--";//Will be asked
			window.wording_pickup = "--ASK--";
			window.wording_pickup_receive = "--ASK--";
		}
		else if(ltg_option=="2"){ //Delivery Only
			window.delivery_or_pickup  = "delivery";
			window.wording_pickup = "Delivery";
			window.wording_pickup_receive = "be ready for your delivery";
		}else{
			window.delivery_or_pickup = "pickup";//Will be asked
			window.wording_pickup = "Pickup";
			window.wording_pickup_receive = "pick up your order on time";
		}
	}


	function AddElement(hide,id,myValue,text,parent,type,myClass){
		var inputTemp = document.createElement(type);
		if(hide === true){
			inputTemp.type = 'hidden';
		}
		if(id.length > 0){
			inputTemp.id = id;
		}
		if(myValue.length > 0){
			inputTemp.value = myValue;
		}
		if(text.length > 0){
			var textNode = document.createTextNode(text);
			inputTemp.appendChild(textNode);
		}
		if(myClass.length > 0){
			inputTemp.className = myClass;
		}
		if(parent){
			parent.appendChild(inputTemp);
		}
		return inputTemp;
	}

	function GetUrlVars() {
		var vars = {};
		var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {vars[key] = value;} );
		return vars;
	}

	function NewClearCart(){
		for(var i = window.Shopping_Cart._entries.length; i > 0; i--){
			window.Shopping_Cart._entries[i-1].setQuantity(0);
			window.Shopping_Cart.removeEntry(window.Shopping_Cart._entries[i]);
		}
		window.Shopping_Cart.clearCart();
		StoreCartContents();
		ShowCartContents();
	}

}

document.getElementById("RestaurantName").textContent += Restaurant_Name;
GenerateGroups(window.Menu_Groups);
GenerateCategories(window.Menu_Groups[0].id);
InfoPane();
var Shopping_Cart = LavuToGo.ShoppingCart.shoppingCart();
if(window.Cart_Contents){
	Shopping_Cart.unserialize( Cart_Contents);
	ShowCartContents();
}
SetPickupOrDelivery(window.Rest_Loc.delivery_option);
window.Customer_Info = {};

if( GetUrlVars()['return'] == 'success'){
	NewClearCart();
	alert('Order Successfully Placed');
	var new_url = window.location.href.substring(0, window.location.href.indexOf('?'));
	window.location.href =  new_url;
}

if( GetUrlVars()['return'] == 'failure'){
	alert('Order payment failed. Please Try again.');
	var new_url = window.location.href.substring(0, window.location.href.indexOf('?'));
	window.location.href =  new_url;
}


var preloadImage = new Image();
preloadImage.src = "./app/components/pizza/images/pizza_blur.png";
if (typeof variable !== 'undefined') {
	InitializePizzaAdHocForcedModifiersFromSession();//For when refreshing the page
}