<?php

	putenv("TZ=America/Chicago");
	error_reporting(E_ALL|E_STRICT);

	require_once(__DIR__."/lib/core_functions.php");
	require_once(__DIR__."/lib/pdo_lavuquery.php");
	require_once(__DIR__."/togo_connect2.php");

	// TODO
	// make this script work with web based session
	// and ajax calls (checkout.php)

	if (isset($locationid) && isset($data_name))
	{
		// load data into db if modified
		$in_lavu = load_data($data_name, $ltg_name);
	}

	//
	// API/Data functions
	//
	function vars_to_str($vars)
	{
		$str = "";
		foreach ($vars as $key => $val)
		{
			if ($str != "")
			{
				$str .= "&";
			}
			$str .= urlencode($key)."=".urlencode($val);
		}

		return $str;
	}

	function api_response($json_vars)
	{
		$json_url = useLavuCentralURL("json_connect")."/lib/json_connect.php";

		if (is_array($json_vars))
		{
			$json_vars = vars_to_str($json_vars);
		}

		$json_vars = "usejcinc=7&".$json_vars;

		ltgDebug(__FILE__, __FUNCTION__." sending request to ".$json_url);
		ltgDebug(__FILE__, __FUNCTION__." json_vars: ".$json_vars);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $json_url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $json_vars);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 4);

		$curl_start = microtime(true);

		$response = curl_exec($ch);

		$curl_end = microtime(true);
		$curl_duration = round((($curl_end - $curl_start) * 1000), 2);
		$curl_info = curl_getinfo($ch);
		$curl_error = curl_error($ch);

		$info_and_error = (empty($response))?" - info: ".json_encode($curl_info)." - error: ".$curl_error:"";

		ltgDebug(__FILE__, __FUNCTION__." - response: ".(empty($response)?"empty":substr($response, 0, 64)."...")." - duration: ".$curl_duration." ms".$info_and_error);

		curl_close($ch);

		return $response;
	}

	function gateway_response($vars)
	{
		global $json_cc;

		$gateway_url = useLavuCentralURL("gateway")."/lib/gateway.php";

		$vars = "cc=".$json_cc."&".$vars;

		ltgDebug(__FILE__, __FUNCTION__." sending request to ".$gateway_url);
		ltgDebug(__FILE__, __FUNCTION__." json_vars: ".$json_vars);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $gateway_url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $vars);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		$curl_start = microtime(true);

		$response = curl_exec($ch);

		$curl_end = microtime(true);
		$curl_duration = round((($curl_end - $curl_start) * 1000), 2);
		$curl_info = curl_getinfo($ch);
		$curl_error = curl_error($ch);

		$info_and_error = (empty($response))?" - info: ".json_encode($curl_info)." - error: ".$curl_error:"";

		ltgDebug(__FILE__, __FUNCTION__." - response: ".(empty($response)?"empty":substr($response, 0, 64)."...")." - duration: ".$curl_duration." ms".$info_and_error);

		curl_close($ch);

		return $response;
	}

	function lzero($n)
	{
		if ($n < 10)
		{
			return "0".($n * 1);
		}
		else
		{
			return ($n * 1);
		}
	}

	function load_data($data_name="", $ltg_name="")
	{
		set_sessvar("data_name", $data_name);
		set_sessvar("ltg_name", $ltg_name);

		return LTG_Updater($ltg_name);
	}

	function get_local_data($ltg_name="")
	{
		if (empty($ltg_name))
		{
			$ltg_name = sessvar("ltg_name");
		}
		else
		{
			if (!sessvar_isset("ltg_name"))
			{
				set_sessvar("ltg_name", $ltg_name);
			}
		}

		if (empty($ltg_name))
		{
			return NULL;
		}

		$merchant_results = mlavu_query("SELECT * FROM `merchants` WHERE `ltg_name` = '[1]'", $ltg_name);
		$merchant = array();
		if (mysqli_num_rows($merchant_results))
		{
			$merchant = mysqli_fetch_assoc($merchant_results);
		}

		return $merchant;
	}

	function get_local_data_as_array()
	{
		$ltg_merch = get_local_data();

		$tinfo = json_decode($ltg_merch['menu_data'], true);
		$lread = json_decode($ltg_merch['location_data'], true);
		$lread = @array_merge($tinfo['config_settings'], $lread);

		return array(
			'location_info'	=> $lread,
			'menu_data'		=> $tinfo
		);
	}

	function last_modified_local($ltg_name="")
	{
		// last modified datetime
		$last_mod_results = mlavu_query("SELECT `last_modified` FROM `merchants` WHERE `ltg_name` = '[1]'", $ltg_name);
		$merchant = array();
		if (mysqli_num_rows($last_mod_results))
		{
			$merchant = mysqli_fetch_assoc($last_mod_results);
		}

		return $merchant;
	}

	function save_order()
	{
		// todo
		// save order tied to the consumer
	}

	function useLavuCentralURL($type)
	{
		global $lavu_central_url;

		if (substr($lavu_central_url, 0, 4) == "http")
		{
			return $lavu_central_url;
		}

		switch ($type)
		{
			case "gateway":

				return "https://cg1.lavusys.com";

			case "json_connect":

				$local_data = get_local_data_as_array();
				if (!empty($local_data))
				{
					if (isLLS($local_data['location_info']))
					{
						return "https://lsvpn.poslavu.com";
					}
				}

				// no break

			case "private":
			default:

				return "https://ac1.lavusys.com";
		}
	}
?>