<?php
	//THESE FUNCTIONS WERE ORIGINALLY IMPLEMENTED IN THE 'IN-APP' PIZZA CREATOR, HERE THEY ARE COPIED OVER
	//AND IMPLEMENTED IF NECESSARY.  THEY MUST BE *implemented* HERE THOUGH AS THEY HAVE REFERENCES IN THE
	//ORIGINAL CODE-BASE.  THUS THIS IS THE IMPLEMENTED INTERFACE FOR FUNCTIONS THAT WOULD DIFFER BETWEEN
	//LTG AND IN-APP.
	//Original data functions brought down.
	function get_decimal_data() {
		global $merchantsDBRowWDeserializedFields;
		global $choosenMenuItemID;
		$decimalDigitCount = $merchantsDBRowWDeserializedFields['location_data']['disable_decimal'];
		$decimalChar = $merchantsDBRowWDeserializedFields['location_data']['decimal_char'];
		return array('disable_decimal' => $decimalDigitCount, 'decimal_char' => $decimalChar);
	}

	//Added by Brian D.  Same functionality of similar function in pizza_settings, created here to decouple.
	function creator_name_2_id($name){
		global $merchantsDBRowWDeserializedFields;
		global $choosenMenuItemID;
		$allPizzaCreatorConfigRows = getAllPizzaCreatorConfigRows();
		foreach($allPizzaCreatorConfigRows as $currRow){
			if( trim($currRow['value2']) == trim($name)){
				return $currRow['value'];
			}
		}
		return -1;
	}

	function getPizzaModsForCreatorID($locationid, $creator_id){
		$creator_id = empty($creator_id) ? 0 : $creator_id;
		global $merchantsDBRowWDeserializedFields;
		global $choosenMenuItemID;
		$modConfigRows = getAllPizzaModConfigRows();
		$modConfigRowsForThisPizzaCreator = array();
		foreach($modConfigRows as $currModRow){
			$value7 = $currModRow['value7'];
			$value7 = empty($value7) ? 0 : $value7;
			if($value7 == $creator_id){
				$modConfigRowsForThisPizzaCreator[] = $currModRow;
			}
		}
		return $modConfigRowsForThisPizzaCreator;
	}

	function getForceModListForFModLID($forcedModListID){
		global $merchantsDBRowWDeserializedFields;
		global $choosenMenuItemID;
		$forcedModifierListsWithEmbeddedForcedMods = $merchantsDBRowWDeserializedFields['menu_data']['forced_modifier_lists'];
		foreach($forcedModifierListsWithEmbeddedForcedMods as $currForcedModListWEmbeddedForcedMods){
			if($currForcedModListWEmbeddedForcedMods['id'] == $forcedModListID){
				return $currForcedModListWEmbeddedForcedMods;
			}
		}
		return false;
	}

	function setPizzaModsAsDeletedIfTiedToDeletedForcedModList(){
		global $merchantsDBRowWDeserializedFields;
		global $choosenMenuItemID;
	}

	function getAllForcedModifiersInForcedModifierList($forcedModifierListID){
		global $merchantsDBRowWDeserializedFields;
		global $choosenMenuItemID;
		$forcedModifierListArr = getForceModListForFModLID($forcedModifierListID);
		$forcedModifiersArr = $forcedModifierListArr['modifiers'];
		$i = 0;
		foreach($forcedModifiersArr as $key => $val){
			$forcedModifiersArr[$key]['list_id'] = $forcedModifierListID;
			$forcedModifiersArr[$key]['_order'] = $i++;
		}
		return $forcedModifiersArr;
	}

	//Possible config settings.
	//Return true or false.
	function getConfBackgroundEmptyImageSetting(){
		global $merchantsDBRowWDeserializedFields;
		global $choosenMenuItemID;
	}

	//Returns bool true or false.
	function getConfDisablePortionsSetting(){
		global $merchantsDBRowWDeserializedFields;
		global $choosenMenuItemID;
	}

	function getConfDisableDefaultHeaviness(){
		global $merchantsDBRowWDeserializedFields;
		global $choosenMenuItemID;
	}

	function get_item_cost() {
		global $merchantsDBRowWDeserializedFields;
		global $choosenMenuItemID;
		$menuItem = getMenuItemRowForMenuID($choosenMenuItemID);
		$itemsPrice = (float)$menuItem['price'];
		return $itemsPrice;
	}
?>