<?php

	function setGlobalDeserializedMerchantRow($p_merchantsDBRowWDeserializedFields){
		global $merchantsDBRowWDeserializedFields; $merchantsDBRowWDeserializedFields = $p_merchantsDBRowWDeserializedFields;
	}

	function getListOfMenuItemIDsThatUsePizzaMaker(){
		global $merchantsDBRowWDeserializedFields;
		$pizzaCreatorConfigRows = getAllPizzaCreatorConfigRows();
		$allForcedModifierListIDsRepresentingAPizzaCreator = array();
		if(!empty($pizzaCreatorConfigRows)){
			foreach($pizzaCreatorConfigRows as $currPizzaCreatorConfig){
				$allForcedModifierListIDsRepresentingAPizzaCreator[$currPizzaCreatorConfig['value6']] = $currPizzaCreatorConfig['value6'];
			}
		}
		//Now we pull all menu items and check if it is in the list.
		$menuItemsTiedToPizzaCreatorArr = array();
		$menuItems = &$merchantsDBRowWDeserializedFields['menu_data']['menu_items'];
		foreach($menuItems as $currMenuItem){
			$forcedModifierGroupID = str_replace('f', '', $currMenuItem['forced_modifier_group_id']);
			if( strtolower(trim($forcedModifierGroupID)) == 'c'){
				$menuItemsCategory = getMenuCategoryByID($currMenuItem['category_id']);
				$forcedModifierGroupID = $menuItemsCategory['forced_modifier_group_id'];
				$forcedModifierGroupID = str_replace('f', '', $forcedModifierGroupID);
			}
			if(!empty($allForcedModifierListIDsRepresentingAPizzaCreator[$forcedModifierGroupID])){
				$menuItemsTiedToPizzaCreatorArr[$currMenuItem['id']] = $currMenuItem;
			}
		}
		return $menuItemsTiedToPizzaCreatorArr;
	}

	function getMenuCategoryByID($menuCategoryID){
		global $merchantsDBRowWDeserializedFields;
		static $menuCategories = array();
		static $initialized = false;
		if(!$initialized){
			$initialized = true;
			$allMenuCategories = &$merchantsDBRowWDeserializedFields['menu_data']['menu_categories'];
			foreach($allMenuCategories as &$currCategory){
				$menuCategories[$currCategory['id']] = $currCategory;
			}
		}
		return isset($menuCategories[$menuCategoryID]) ? $menuCategories[$menuCategoryID] : false;
	}

	//This needs to be called prior to loading pizza-wrapper code.
	//$merchantsDBRowDeserializedFieldsArr: the merchants row where the json fields are replaced with their deserialized array.
	function initialize_4_ltg($p_merchantsDBRowWDeserializedFields, $p_choosenMenuItemID){
		global $dataname;
		global $merchantsDBRowWDeserializedFields; $merchantsDBRowWDeserializedFields = $p_merchantsDBRowWDeserializedFields;
		global $choosenMenuItemID; $choosenMenuItemID = $p_choosenMenuItemID;
		loadSuperglobalArrs();
	}

	function loadSuperglobalArrs(){
		global $merchantsDBRowWDeserializedFields;
		global $choosenMenuItemID;
		$choosenMenuItemRow = getMenuItemRowForMenuID($choosenMenuItemID);
		$pizzaBuilderID = getPizzaBuilderIDForMenuItemNegOneIfNone($choosenMenuItemRow);
		$_REQUEST['cmd'] = getPizzaCreatorNameForID($pizzaBuilderID);//Needs to be loaded with the name of the pizza creator that will be in use for the item.
	}

	function getMenuItemRowForMenuID($p_choosenMenuItemID){
		global $merchantsDBRowWDeserializedFields;
		$menuItems = &$merchantsDBRowWDeserializedFields['menu_data']['menu_items'];
		foreach($menuItems as &$currMenuItemRow){
			if($currMenuItemRow['id'] == $p_choosenMenuItemID){
				return $currMenuItemRow;
			}
		}
		return false;
	}

	function getPizzaBuilderIDForMenuItemNegOneIfNone($menuItemDBRow){
		global $merchantsDBRowWDeserializedFields;
		$forcedModifierGroupID = $menuItemDBRow['forced_modifier_group_id'];
		$forcedModifierListID = str_replace('f', '', $forcedModifierGroupID);
		if(strtolower(trim($forcedModifierListID)) == 'c'){
			$itemsCategory = getMenuCategoryByID($menuItemDBRow['category_id']);
			$forcedModifierGroupID = $itemsCategory['forced_modifier_group_id'];
			$forcedModifierListID = intval(str_replace('f', '', $forcedModifierGroupID));
		}
		$pizzaCreatorConfigRows = getAllPizzaCreatorConfigRows($merchantsDBRowWDeserializedFields);
		foreach($pizzaCreatorConfigRows as $currRow){
			$currRowsFModListID = empty($currRow['value6']) ? 0 : $currRow['value6'];
			if($currRowsFModListID == $forcedModifierListID){
				return empty($currRow['value']) ? 0 : $currRow['value'];
			}
		}
		return -1;
	}

	function getAllPizzaCreatorConfigRows(){
		global $merchantsDBRowWDeserializedFields;
		$pizzaSettings = getPizzaSettings($merchantsDBRowWDeserializedFields);
		return $pizzaSettings['pizza_creators'];
	}

	function getAllPizzaModConfigRows(){
		global $merchantsDBRowWDeserializedFields;
		$pizzaSettings = getPizzaSettings($merchantsDBRowWDeserializedFields);
		return $pizzaSettings['pizza_mods'];
	}

	function getPizzaSettings(){
		global $merchantsDBRowWDeserializedFields;
		static $pizzaSettings = null;
		if($pizzaSettings != null){
			return $pizzaSettings;
		}
		$pizzaSettings = @json_decode( $merchantsDBRowWDeserializedFields['location_data']['ltg_pizza_info'], 1);
		return $pizzaSettings;
	}

	function getPizzaCreatorNameForID($id){
		$pizzaCreatorConfigRows = getAllPizzaCreatorConfigRows();
		foreach($pizzaCreatorConfigRows as $currPizzaCreatorConfigRow){
			if($id == trim($currPizzaCreatorConfigRow['value'])){
				return $currPizzaCreatorConfigRow['value2'];
			}
		}
		return false;
	}

	//CRAP WE HAVE TO PUT UP WITH.  COMES FROM CHANGING PIZZA TO BECOME 'build your own'.
	function getAllReferencedDetoursFromCategoriesArr($all_categories, $useCaching = true){
		return array();
	}
	function _getAllReferencedDetoursFromArrayOfForcedModifierRows($modifierRowsArr){
		return array();
	}
	function _getAllRows_forcedModifierLists_joinedToTheirForcedMods($allDetourIDs){
		return array();
	}

?>