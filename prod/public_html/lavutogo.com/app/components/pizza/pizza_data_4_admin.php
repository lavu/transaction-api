<?php

	//Original data functions brought down.
	function get_decimal_data() {
		// in case the location data can't be found
		$a_badval = array('disable_decimal'=>'2', 'decimal_char'=>'.');
		// load the location data and return it, if found
		// otherwise, return the badval
		$s_location_id = (isset($_POST['loc_id'])) ? $_POST['loc_id'] : '1';
		$decimal_query = lavu_query("SELECT `disable_decimal`,`decimal_char` FROM `locations` WHERE `id`='[id]'", array('id'=>$s_location_id));
		if ($decimal_query === FALSE || mysqli_num_rows($decimal_query) == 0)
			return $a_badval;
		return mysqli_fetch_assoc($decimal_query);
	}

	//Added by Brian D.  Same functionality of similar function in pizza_settings, created here to decouple.
	function creator_name_2_id($name){
		$result = lavu_query("SELECT * FROM `config` WHERE `setting`='pizza creator' AND `value2`='[1]' AND `_deleted`='0'", $name);
		if(!$result){ error_log("Mysql_error in ".__FILE__." error: ".mysql_error()); return -1; }
		if(mysqli_num_rows($result) == 0){ return -1; }
		$row = mysqli_fetch_assoc($result);
		return empty($row['value']) ? 0 : $row['value'];
	}

	function getPizzaModsForCreatorID($locationid, $creator_id){
		$result = null;
		if($creator_id == '0'){
			$result = lavu_query("select * from `config` WHERE `location`='[1]' AND `setting`='pizza_mods' AND (`value7`='0' OR `value7`='') AND `_deleted`='0'", $locationid);
		}else{
			$result = lavu_query("select * from `config` WHERE `location`='[1]' AND `setting`='pizza_mods' AND `value7`='[2]' AND `_deleted`='0'", $locationid, $creator_id);
		}
		if(!$result){ error_log("Mysql error in ".__FILE__." -sgh- error:".error_log()); return false; }
		$pizzaModConfigRows = array();
		while($currRow = mysqli_fetch_assoc($result)){
			$pizzaModConfigRows[] = $currRow;
		}
		return $pizzaModConfigRows;
	}

	function getForceModListForFModLID($forcedModListID){
		$result = lavu_query("SELECT * FROM `forced_modifier_lists` WHERE `id`='[1]'", $forcedModListID);
		if(!$result){ error_log("Mysql error in ".__FILE__." -cnost8- error:".mysql_error()); return false;}
		if(mysqli_num_rows($result)){ return mysqli_fetch_assoc($result); }
		return false;
	}

	function setPizzaModsAsDeletedIfTiedToDeletedForcedModList(){
		$query = "UPDATE `config` conf JOIN `forced_modifier_lists` fml ".
				 "ON `conf`.`value2` = `fml`.`id` SET `conf`.`_deleted`='1' ".
				 "where `conf`.`setting`='pizza_mods' AND `conf`.`_deleted`='0' AND `fml`.`_deleted`<>'0'";
		$result = lavu_query($query);
		if(!$result){ error_log("Mysql error in ".__FILE__." -cde- error:".mysql_error()); return false;}
		return true;
	}

	function getAllForcedModifiersInForcedModifierList($forcedModifierListID){
		$result = lavu_query("SELECT * FROM `forced_modifiers` WHERE `list_id`='[1]' ORDER BY `_order` ASC", $forcedModifierListID);
		$forcedModifiersArr = array();
		while($currRow = mysqli_fetch_assoc($result)){
			$forcedModifiersArr[] = $currRow;
		}
		return $forcedModifiersArr;
	}

	//Possible config settings.
	//Return true or false.
	function getConfBackgroundEmptyImageSetting(){
		static $value = null;
		if($value !== null)
			return $value;
		$result = lavu_query("SELECT * FROM `config` WHERE `setting`='build_your_own_background_image'");
		if(mysqli_num_rows($result) == 0){
			$value = false;
		}
		else{
			$row = mysqli_fetch_assoc($result);
			$value = $row['value'];
		}
		return $value;
	}

	//Returns bool true or false.
	function getConfDisablePortionsSetting(){
		static $value = null;
		if($value !== null)
			return $value;
		$result = lavu_query("SELECT * FROM `config` WHERE `setting`='build_your_own_disable_portions'");
		if(mysqli_num_rows($result) == 0){
			$value = false;
		}
		else{
			$row = mysqli_fetch_assoc($result);
			$value = strtolower($row['value']) == 'true' ? true : false;
		}
		return $value;
	}
	function getConfDisableDefaultHeaviness(){
		static $value = null;
		if($value !== null)
			return $value;
		$result = lavu_query("SELECT * FROM `config` WHERE `setting`='build_your_own_disable_default_heavyness'");
		if(mysqli_num_rows($result) == 0){
			$value = false;
		}else{
			$row = mysqli_fetch_assoc($result);
			$value = strtolower($row['value']) == 'true' ? true : false;
		}
		return $value;
	}
	
	/**
	 * Because the entire pizza is one "item", the item id and possibly the item cost should be passed through $_POST vars.
	 *     Get the item cost.
	 * @return float The cost of the item.
	 */
	function get_item_cost() {
		// return the price, if it's set
		return (float)$_POST['item_price'];
		// get the id of the menu item
		$item_id = $_POST['item_id'];
		$item_query = lavu_query("SELECT `price` FROM `menu_items` WHERE `id`='[id]'",
			array('id'=>$item_id));
		// get the cost of the item
		$item_cost = 0;
		if ($item_query !== FALSE && mysqli_num_rows($item_query) > 0) {
			$item_read = mysqli_fetch_assoc($item_query);
			$item_cost = (float)$item_read['price'];
		}
		return $item_cost;
	}
?>