<?php

	if(  strpos($_SERVER['DOCUMENT_ROOT'], "/home/poslavu/public_html/admin") !== false ){ //Being ran on admin/clouds.
		$isInApp = true;
		if (!isset($v2cp)){
			$v2cp = (strstr(__FILE__, '/dev/')) ? 'v2' : 'cp';
		}
		$s_root_dir = dirname(__FILE__).'/../../'.($v2cp == 'v2' ? '../' : '');
		$dont_draw_pizza_settings = TRUE;
		require_once($s_root_dir.$v2cp.'/resources/lavuquery.php');
		require_once(dirname(__FILE__).'/admin_specific_functions.php');
		require_once(dirname(__FILE__).'/pizza_data_4_admin.php');//If running on admin gets data differently then when running on ltg. Does live queries.
	}else{//Being ran on ltg.
		$isInApp = false;
		require_once(dirname(__FILE__).'/../../lib/core_functions.php');
		require_once(dirname(__FILE__).'/pizza_data_4_ltg.php');//Loads the $_REQUEST/$_GET/$_POST arrays with what's expected and pulls from JSON data-store.
		require_once(dirname(__FILE__).'/ltg_specific_functions.php');
	}


	function build_forced_modifier_categories() {

		setPizzaModsAsDeletedIfTiedToDeletedForcedModList();

		// global settings
		global $locationid;
		global $build_forced_modifier_categories_returned_value;
		if (!isset($locationid))
			$locationid = 1;
		if (isset($build_forced_modifier_categories_returned_value) && isset($build_forced_modifier_categories_returned_value[$locationid])){
			return $build_forced_modifier_categories_returned_value[$locationid];
		}
		$i_creator_id = creator_name_2_id($_REQUEST['cmd']);
		$a_configs = getPizzaModsForCreatorID($locationid, $i_creator_id);
		$a_retval = array();
		// build the categories
		foreach($a_configs as $intKey => $a_config) {
			// get the title and index of the list
			$s_title = $a_config['value'];
			// get the forced modifier list
			$i_forced_mod_list_id = (int)$a_config['value2'];
			if($i_forced_mod_list_id == 0 || empty($s_title)){ //HERE We would put in any other checks too.
				continue;
			}
			$a_forced_mod_list = getForceModListForFModLID($i_forced_mod_list_id);
			$a_forced_modifiers = getAllForcedModifiersInForcedModifierList($i_forced_mod_list_id);//admin / ltg specific.
			// create the new category in the list
			$currForcedModCategory  = array();
			$currForcedModCategory['title'] = $s_title;
			$currForcedModCategory['choice'] = ($a_forced_mod_list['type'] == 'choice');
			$currForcedModCategory['page'] = max( (int)$a_config['value8'], 1);
			$currForcedModCategory['order'] = (int)$a_config['value6'];
			$currForcedModCategory['multi-column'] = (int)$a_config['value9'];
			$currForcedModCategory['partial_serving_pricing'] = ($a_config['value10'] == "") ? 1.00 : (float)$a_config['value10'];
			$currForcedModCategory['mods'] = $a_forced_modifiers;
			$a_retval[] = $currForcedModCategory;
		}
		//Global instance.
		$build_forced_modifier_categories_returned_value[$locationid] = $a_retval;
		return $a_retval;
	}

	/**
	 * Get the forced modifier categories that apply to the pizza creator.
	 * @return array An array of forced mods in the form
	 *     array(
	 *         array( 'title'=>string, 'mods'=>db row from forced modifiers, 'choice'=>boolean either choice or list, 'page'=>int ),
	 *         ...
	 *     )
	 */
	function get_categories() {

		global $a_get_categories_return_value;
		if (isset($a_get_categories_return_value))
			return $a_get_categories_return_value;

		// get the forced modifiers
		$a_forced_modifiers_categories = build_forced_modifier_categories();

		// assign ids
		$i_max_page = 0;
		$a_page_data = array();
		foreach($a_forced_modifiers_categories as $s_category_id=>$a_category) {
			$a_forced_modifiers_categories[$s_category_id]['id'] = $s_category_id;

			// get page data
			$i_page = $a_category['page'];
			$i_max_page = max($i_page, $i_max_page);
			if (!isset($a_page_data[$i_page]))
				$a_page_data[$i_page] = array('categories'=>array());
			$a_page_data[$i_page]['categories'][] = $s_category_id;
		}

		// condense the pages
		for ($i = 1; $i <= $i_max_page; $i++) {

			// check if there are any categories in the table
			// if so, continue
			if (isset($a_page_data[$i]) && count($a_page_data[$i]['categories']) > 0)
				continue;

			// find the next page with categories
			// if there is no next page, we're done condensing
			$i_next_page = $i;
			foreach($a_page_data as $i_page=>$a_data) {
				if (count($a_data['categories']) > 0)
					$i_next_page = max($i_page, $i_next_page);
				if ($i_next_page > $i)
					break;
			}
			if ($i_next_page <= $i)
				break;

			// condense the categories from the next page onto this page
			foreach($a_page_data[$i_next_page]['categories'] as $k=>$s_category_id) {
				$a_page_data[$i]['categories'][] = $s_category_id;
				$a_forced_modifiers_categories[$s_category_id]['page'] = $i;
				unset($a_page_data[$i_next_page]['categories'][$k]);
			}
		}

		$a_get_categories_return_value = $a_forced_modifiers_categories;
		return $a_forced_modifiers_categories;
	}

	/**
	 * Retrieves the maximum page number of the categories.
	 * @return int The maximum page number of the categories.
	 */
	function get_categories_max_page() {

		// check for a cached value
		global $i_get_categories_max_page_retval;
		if (isset($i_get_categories_max_page_retval))
			return $i_get_categories_max_page_retval;

		// get the data
		$a_forced_modifiers_categories = get_categories();
		$i_max_page = 1;
		foreach($a_forced_modifiers_categories as $a_category)
			$i_max_page = max($a_category['page'], $i_max_page);

		$i_get_categories_max_page_retval = $i_max_page;
		return $i_max_page;
	}

	// touch javascript events
	function je($s_type) {
		global $b_dev_components;

		if ($b_dev_components) {
			return 'onclick';
		} else {
			return "on{$s_type}";
		}
	}

	function draw_drawPrice_function() {
		$a_decimal_data = get_decimal_data();
		$precision = $a_decimal_data['disable_decimal'];
		$decimal_char = $a_decimal_data['decimal_char'];

		return "
		function drawPrice(fval) {
			var precision = $precision;
			var decimal_char = '$decimal_char';
			return toFixed(fval, precision, decimal_char);
		}

		// from http://stackoverflow.com/questions/2221167/javascript-formatting-a-rounded-number-to-n-decimals/2909252#2909252
		function toFixed(value, precision, decimal_char) {
			var precision = precision || 0,
			neg = value < 0,
			power = Math.pow(10, precision),
			value = Math.round(value * power),
			integral = String((neg ? Math.ceil : Math.floor)(value / power)),
			fraction = String((neg ? -value : value) % power),
			padding = new Array(Math.max(precision - fraction.length, 0) + 1).join('0');

			return precision ? integral + decimal_char +  padding + fraction : integral;
		}";
	}

	$s_shared_javascript = "";
	//ob_start();
	?>
	
	<script type='text/javascript'>
		
		if(typeof is_in_app === 'undefined'){
			var is_in_app = <?php echo $isInApp ? 'true' : 'false'; ?>;
		}
	
		//This function handles do commands for both 'in-app' and 'web-page' embedded applications.
		//Example Do commands:
		//"_DO:cmd=send_js&c_name=pizza_mod&js=PageTurner.turnPage("+nextPageNum+")";

		function do_com_cmd(cmdstr)
		{
			if(is_in_app){
				window.location = cmdstr;
			}
			else
			{ 
				cmdstr = cmdstr.replace('_DO:', '');
				var urlParsedURLObject = {};
				var pairsArr = cmdstr.split("&");
				for(var i = 0; i < pairsArr.length; i++){
					var parts = pairsArr[i].split('=');
					urlParsedURLObject[parts[0]] = parts[1];
				}
				if(urlParsedURLObject['cmd']=="send_js")
				{
					var jsStr = urlParsedURLObject['js'];
					//alert(jsStr);
					eval(jsStr);
				}
				else if(urlParsedURLObject['cmd']=="close_overlay"){
					window.parent.exit_pizza_component();
				}
				else if(urlParsedURLObject['cmd']=="add_mod"){
					var pizza_info = decodeURIComponent(urlParsedURLObject);
					
					//Object keeps parallel arrays (in string delimited form) of modifier: title, price, modifier_id, whether to display new line.
					//We deserialize into parallel arrays.
					var fullPizzaInfoObject = {};
					fullPizzaInfoObject.mod_title = decodeURIComponent(urlParsedURLObject['mod_title']).split('|*|');
					fullPizzaInfoObject.mod_price = decodeURIComponent(urlParsedURLObject['mod_price']).split('|*|');
					fullPizzaInfoObject.mod_id = decodeURIComponent(urlParsedURLObject['mod_id']).split('|*|');
					fullPizzaInfoObject.mod_new_line = decodeURIComponent(urlParsedURLObject['mod_new_line']).split('|*|');
					
					window.parent.save_pizza_to_order(fullPizzaInfoObject);
				}
			}
		}
	
	
		/**
		 * A precursor to touchEnd.
		 *     NOTE: MUST BE CALLED if touchEnd is to be trusted.
		 * @param  a javscript event event The event that was fired.
		 * @param  int               i     The index of the touch event status.
		 * @return N/A                     No return value.
		 */
		function touchStart(event, i){
			if (typeof(i) == 'undefined')
				i = 0;

			_start_x_loc_old[i] = _start_x_loc[i];
			_start_y_loc_old[i] = _start_y_loc[i];
			_start_x_loc[i] = event.touches[0].clientX;
			_start_y_loc[i] = event.touches[0].clientY;
			var between = (new Date().getTime())-_time_start[i];
			_time_start[i] = new Date().getTime();
			_is_double_tap[i] =false;
			if(between < 250)
				if( (Math.abs(_start_x_loc[i]-_start_x_loc_old[i]) < 20 && Math.abs(_start_y_loc[i]-_start_y_loc_old[i]) < 20 ))
					_is_double_tap[i]=true;
		}

		/**
		 * Sets the last scroll time of the window to try and reduce false positives on clicks.
		 */
		function setLastScroll() {
			_last_scroll_datetime = (new Date()).getTime();
		}
		_last_scroll_datetime = 0;

		/**
		 * Checks if a touchend event is the result of a touch or a scroll.
		 *     NOTE: touchStart MUST BE CALLED if touchEnd is to be trusted.
		 * @param  a javscript event event The event that was fired.
		 * @param  int               i     The index of the touch event status.
		 * @return string One of 'tap', 'double tap', 'scroll', or 'tap and hold'
		 */
		function touchEnd(event, i){
			if (typeof(i) == 'undefined')
				i = 0;

			// initialize some values
			event.preventDefault();
			var istouch = false;
			var isdouble = false;
			var ishold = false;
			var isscroll = false;

			// do some calculations
			if( Math.abs(_start_x_loc[i]-event.changedTouches[0].clientX) < 15 &&
				Math.abs(_start_y_loc[i]-event.changedTouches[0].clientY) < 15 &&
				(new Date()).getTime() - _last_scroll_datetime > 200 ){
				istouch = true;
				if(_is_double_tap[i])
					isdouble = true;
			}else{
				isscroll = true;
			}
			if ((new Date().getTime()) - _time_start[i] > 1000)
				ishold = true;

			// return a value
			var retval = '';
			if (isscroll)
				retval = 'scroll';
			else if (ishold)
				retval = 'tap and hold';
			else if (isdouble)
				retval = 'double tap';
			else
				retval = 'tap';
			return retval;
		}
		_time_start = [];
		_start_x_loc = [];
		_start_y_loc = [];
		_start_x_loc_old = [];
		_start_y_loc_old = [];
		_is_double_tap = [];

		/**
		 * Always returns true if $b_dev_components (in sa_cp/index.php?mode=manage_customers&submode=general_tools&tool=edit_coms)
		 * @param  javascript event event The event created by a click or touchend
		 * @return boolean                TRUE if $b_dev_components or it's a tap, FALSE otherwise
		 */
		function touchSuccess(event) {
			<?php
				global $b_dev_components;
				if ($b_dev_components)
					echo "return true;";
			?>

			var touch_result = touchEnd(event);
			if (touch_result == 'tap')
				return true;
			return false;
		}

	</script>
