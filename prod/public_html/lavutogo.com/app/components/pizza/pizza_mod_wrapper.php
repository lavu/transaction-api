<?php
	if(  strpos($_SERVER['DOCUMENT_ROOT'], "/home/poslavu/public_html/admin") !== false ){
		$isInApp = true;
		if (!isset($v2cp))
			$v2cp = (strstr(__FILE__, '/dev/')) ? 'v2' : 'cp';
		$s_root_dir = dirname(__FILE__).'/../../'.($v2cp == 'v2' ? '../' : '');
		require_once($s_root_dir.$v2cp.'/resources/lavuquery.php');
		require_once(dirname(__FILE__) . "/../comconnect.php");
		$pizza_blur_image_path = "images/pizza_blur.png";
		
		require_once(dirname(__FILE__).'/pizza_mod_shared_funcs.php');
		

		//PATH VARS
		$jqueryURL = "/manage/js/jquery/js/jquery-1.9.0.js";
		
		//If using
		$usingBlankBackground = getConfBackgroundEmptyImageSetting();
		$pizza_blur_image_path = $usingBlankBackground ? '/dev/components/pizza/images/blank_blur.png' : $pizza_blur_image_path;
		
		$pizza_view_icon_path = "/dev/components/pizza/images/view_icon_small.gif";
	}else{
		require_once(dirname(__FILE__).'/pizza_mod_shared_funcs.php');

		$isInApp = false;
		//PATH VARS
		$jqueryURL = "./pizza/jquery-1.9.0.js";
		//$pizza_blur_image_path = "./app/components/pizza/images/pizza_sizer_sauce_cheese.png";
		$pizza_blur_image_path = "./pizza/images/pizza_blur.png";
		
		$pizza_view_icon_path = "./pizza/images/view_icon_small.gif";
	}
	
	
	$page_turn_top = 174;

	//Draws the forced modifier on the pizza, e.g. the pepperoni appearing ontop of the pizza. etc.
	function wrapper_draw_fmod_categories($a_forced_mod_categories, $dataname) {

		$i_choice = 0;
		$i_top_min = 9999;
		$i_choice_count = 1;
		global $stuff;
		global $dataname;
		global $isInApp;
		// how many choices need to be drawn
		foreach($a_forced_mod_categories as $a_fmod_category) {
			if ($a_fmod_category['choice']) {
				$i_choice_count++;
			}
		}

		// go through each item of each category and find its picture
		foreach($a_forced_mod_categories as $a_fmod_category) {
			if ($a_fmod_category['choice']) {

				// if the category is a choice, draw it as text next to the pizza
				if (count($a_fmod_category['mods']) > 0) {
					$s_cat_name = $a_fmod_category['title'];
					$i_top = 240-24*$i_choice_count+24*$i_choice;
					$i_top_min = min($i_top_min, $i_top);
					$i_list_id = (int)$a_fmod_category['mods'][0]['list_id'];

					$s_title = '<div style="position:absolute; top:'.$i_top.'px; left:26px; display:none;" class="choices">'.$s_cat_name.': ';
					$s_description = '<font id="choice_'.$i_list_id.'" class="choices_light">&nbsp;</font></div>';
					echo $s_title . $s_description;

					$i_choice++;
				}
			} else {

				// if the category is a checklist, go through each fmod,
				// check that the picture field is set and the picture exists,
				// and then add it
				foreach($a_fmod_category['mods'] as $a_fmod) {
					$i_list_id = (int)$a_fmod['list_id'];
					$i_mod_id = (int)$a_fmod['id'];
					$s_picture_name = "full_" . trim($a_fmod['extra5']);
					$s_url = '//admin.poslavu.com/images/test_pizza_sho/fmods/full/'.$s_picture_name;
					
					//Inserted By Brian D.  THIS IS WHERE IT DRAWS THE BACKGROUND IMAGE OF THE F. MODIFIERS. E.G. The pepperoni/onions etc.
					$usingBlankBackground = getConfBackgroundEmptyImageSetting();
					echo '<div id="pic_'.$i_list_id.'_'.$i_mod_id.'_left"  style="/*background-color:rgba(100,0,0,0.2);*/ width:50%; height:100%; position:fixed; left:0px; top:0px; overflow:hidden; z-index:-1; display:none;">'.
							 '<img src="'.$s_url.'" style="position:absolute; right:-500px; top:0px;">'.
						 '</div>';
					echo '<div id="pic_'.$i_list_id.'_'.$i_mod_id.'_right" style="/*background-color:rgba(0,100,0,0.2);*/ width:50%; height:100%; position:fixed; right:0px; top:0px; overflow:hidden; z-index:-1; display:none;">'.
							 '<img src="'.$s_url.'" style="position:absolute; left:-523px; top:0px;">'.
						 '</div>';
				}
			}
		}

		// draw the total cost text
		$i_top = $i_top_min-24;
		echo '<div style="position:fixed; top:'.$i_top.'px; left:26px; display:none;" class="choices">Cost: <font id="total_cost" class="choices_light">&nbsp;</font></div>';
	}
?>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Pizza Toppings Wrapper</title>

		<style type="text/css">
<!--
body {
	background-color: transparent;
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	-webkit-tap-highlight-color:rgba(0,0,0,0);
	-webkit-user-select: none;
}

div {
	-webkit-user-select: none;
}
span {
	-webkit-user-select: none;
}
select {
	-webkit-user-select: none;
}
font {
	-webkit-user-select: none;
}
img {
	-webkit-user-select: none;
}
table {
	-webkit-user-select: none;
}
td {
	-webkit-user-select: none;
}
tr {
	-webkit-user-select: none;
}

.style4 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #8896AD;
}
.style6 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 24px;
	font-weight: bold;
	color: #8896AD;
}
.style8 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 15px;
	font-weight: bold;
	color: #07142a;
}
.choices {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 15px;
	font-weight: bold;
	color: #555;
}
.choices_light {
	color: #678;
}
-->
.button_color {
	background-color:#fff;
	border:1px solid rgb(158,158,158);
	border-radius: 10px;
	box-shadow: 0px 0px 6px 6px #eee inset;
}
		</style>

		<?php

			if(reqvar("cm"))
			{
				$cancel_cmd = "cancel";
			}
			else
			{
				$cancel_cmd = "close_overlay";
			}

			$bg = "images/win_overlay_2btn_short.png";
			$a_forced_mod_categories = build_forced_modifier_categories();
		?>

		<script type="text/javascript" src="<?php echo $jqueryURL;?>"></script>
		<?php echo $s_shared_javascript; ?>
		<script type="text/javascript">
			modifiers = {};
			if(typeof is_in_app === 'undefined'){
				var is_in_app = <?php echo $isInApp ? 'true' : 'false'; ?>;
			}
			
			function decode_unicode_str( str ){
				var result = "";
				var it = 0;
				while( it < str.length ){
					var num_bytes = 0;
					var first_byte = str.charCodeAt(str[it]);
					var s = 0;
					while( (0x80 >> s)&first_byte ){ s++; }
					num_bytes = Math.max(0,--s);
					if( num_bytes == 0 ){
						result += str[it];
					} else {
						var unicode = 0;
						for( var i = 1; i <= num_bytes; i++ ){
							if( (0xC0 & str.charCodeAt(it + i)) != 0x80 ){
								return ""; //Detected a non-continuation... abandon hope
							}
						}
						//Everything's validated, moving on
						for( var i = num_bytes; i > 0; i-- ){
							unicode = unicode | ((0x3F & str.charCodeAt(it+i))<<(6*(num_bytes-i)));
						}
						unicode = unicode | ((0xFF>>(num_bytes+1))&first_byte)<<(6*num_bytes);

						result += String.fromCharCode( unicode );
					}
					it+= num_bytes+1;
				}
				return result;
			}
			function addModifier(modname, portion, is_choice, list_id, mod_id, heavy_text, cost, category_page, category_order, modifier_order) {
				var listObj = { modname: decode_unicode_str(modname), portion: portion, is_choice: is_choice, mod_id: mod_id, heavy_text: heavy_text, cost: cost, category_page: category_page, category_order: category_order, modifier_order: modifier_order };

				if (modifiers[list_id]) {

					// if it's a choice, it should be the only thing for that list_id
					// otherwise, add it if it doesn't already exist
					if (is_choice) {
						modifiers[list_id][0] = listObj;
					} else {
						// find the existing listObj, if it already exists
						var existingkey = null;
						var existing = $.grep(modifiers[list_id], function(v, k) {
							if (v['mod_id'] == mod_id) {
								existingkey = k;
								return true;
							}
							return false;
						});

						// add or update the listObj
						if (existing.length > 0)
							modifiers[list_id][existingkey] = listObj;
						else
							modifiers[list_id][modifiers[list_id].length] = listObj;
					}
				} else {

					// add it to the list_id
					modifiers[list_id] = [];
					modifiers[list_id][0] = (listObj)
				}

				if (is_choice) {

					// set the choice text
					var jchoice = $('#choice_'+list_id);
					jchoice.html('');
					jchoice.append(modname);
				} else {

					// show or hide the picture
					hidePic(list_id, mod_id);
					showPic(list_id, mod_id, portion);
				}
			}

			function removeModifier(modname, portion, is_choice, list_id, mod_id) {
				if (modifiers[list_id]) {
					// find the existing listObj, if it already exists
					var existingkey = null;
					var existing = $.grep(modifiers[list_id], function(v, k) {
						if (v['mod_id'] == mod_id) {
							existingkey = k;
							return true;
						}
						return false;
					});

					// remove it, if it exists
					if (existing.length > 0) {
						modifiers[list_id].splice(existingkey, 1);
						hidePic(list_id, mod_id);
					}
				}
			}

			// draws the picture if it can find it
			// portion should be one of 'left', 'right', or 'whole'
			function showPic(list_id, mod_id, portion) {
				var div_id = 'pic_'+list_id+'_'+mod_id;
				var div_ids = [];

				if (portion == 'left' || portion == 'whole')
					div_ids.splice(0,0,div_id+'_left');
				if (portion == 'right' || portion == 'whole')
					div_ids.splice(0,0,div_id+'_right');

				for (var i = 0; i < div_ids.length; i++) {
					var div = $('#'+div_ids[i]);
					if (div.length > 0) {
					   div.show();//JQuery thing.  When $(#...) returns the element, but adds additional methods.
					}
				}
			}

			// hides the picture, if it can find it
			function hidePic(list_id, mod_id) {
				var div_ids = [];
				div_ids[0] = 'pic_'+list_id+'_'+mod_id+'_left';
				div_ids[1] = 'pic_'+list_id+'_'+mod_id+'_right';

				for (var i = 0; i < div_ids.length; i++) {
					var div = $('#'+div_ids[i]);
					if (div.length > 0) {
						div.hide();
					}
				}
			}

			function setTotalCost(total_cost) {
				$("#total_cost").html(drawPrice(total_cost));
			}
			

			window.isViewingPizza = false;
			function viewPizza(element) {
				if (window.isViewingPizza) {
					//window.location='_DO:cmd=send_js&c_name=pizza_mod&js=hidePizza()';
					do_com_cmd('_DO:cmd=send_js&c_name=pizza_mod&js=hidePizza()');
					$(element).css({ opacity:0.5 });
				} else {
					//window.location='_DO:cmd=send_js&c_name=pizza_mod&js=viewPizza()';
					do_com_cmd('_DO:cmd=send_js&c_name=pizza_mod&js=viewPizza()');
					$(element).css({ opacity:1 });
				}
				window.isViewingPizza = !window.isViewingPizza;
			}

			<?php echo draw_drawPrice_function(); ?>

			function apply_mods(dontChangeLocation) {
				var nochoice_titles = [];
				var nochoice_prices = [];
				var nochoice_mod_ids = [];
				var nochoice_newlines = [];
				var whole_titles = [];
				var whole_prices = [];
				var whole_mod_ids = [];
				var whole_newlines = [];
				var left_titles = [];
				var left_prices = [];
				var left_mod_ids = [];
				var left_newlines = [];
				var right_titles = [];
				var right_prices = [];
				var right_mod_ids = [];
				var right_newlines = [];

				var titles = [];
				var prices = [];
				var mod_ids = [];
				var newlines = [];
				var titles_text = '';
				var prices_text = '';
				var mod_ids_text = '';
				var newlines_text = '';

				var sortedModifiers = [];
				$.each(modifiers, function(i, list_mods) {
					for( var i = 0; i < list_mods.length; i++ ){
						sortedModifiers.push( list_mods[i] );
					}
				});

				sortedModifiers = sortedModifiers.sort(
					function( moda, modb ){
						if( moda.category_page < modb.category_page ){
							return -1;
						} else if( moda.category_page > modb.category_page ){
							return 1;
						} else if( moda.category_order < modb.category_order ){
							return -1;
						} else if( moda.category_order > modb.category_order ){
							return 1;
						} else if( moda.modname.modifier_order < modb.modname.modifier_order ){
							return -1;
						} else if( moda.modname.modifier_order > modb.modname.modifier_order ){
							return 1;
						} else if( moda.modname.toLowerCase() < modb.modname.toLowerCase() ){
							return -1;
						} else if( moda.modname.toLowerCase() > modb.modname.toLowerCase() ){
							return 1;
						}
						
						return 0;
					}
				);

				// get data from each modifier to build the above array
				var line = 0;
				$.each(sortedModifiers, function(i, list_mod) {

					// sort the modifiers by portion, where 'whole' comes first
					// list_mods = list_mods.sort(function(a,b) {
					// 	if (a.portion == b.portion)
					// 		return 0;
					// 	if (a.portion == 'whole')
					// 		return -1;
					// 	if (b.portion == 'whole')
					// 		return 1;
					// 	return (a.portion < b.portion) ? -1 : 1;
					// });
					var mod = list_mod;

					var target_titles;
					var target_prices;
					var target_mod_ids;
					var target_newlines;

					switch( mod.portion ){
						case 'whole':
							target_titles = whole_titles;
							target_prices = whole_prices;
							target_mod_ids = whole_mod_ids;
							target_newlines = whole_newlines;
							break;
						case 'left':
							target_titles = left_titles;
							target_prices = left_prices;
							target_mod_ids = left_mod_ids;
							target_newlines = left_newlines;
							break;
						case 'right':
							target_titles = right_titles;
							target_prices = right_prices;
							target_mod_ids = right_mod_ids;
							target_newlines = right_newlines;
							break;
						default:
							return; //unknown option
					}

					var title = mod.modname;
					if (!mod.is_choice && mod.heavy_text && mod.heavy_text.toLowerCase() != 'normal')
						title += ', '+mod.heavy_text;

					var newline = 0;
					if( mod.is_choice ){
						nochoice_titles.push(title);
						nochoice_prices.push(mod.cost);
						nochoice_mod_ids.push(mod.mod_id);
						nochoice_newlines.push(1);
					} else {
						// add the modifiers to the above arrays
						// add a newline to the list of return values
						// whenever the portion changes
						if (target_titles.length === 0) {
							target_titles.push('*'+mod.portion);
							target_prices.push(0);
							target_mod_ids.push(0);
							target_newlines.push(1);
							newline = 1;
						}

						// add the options to the list of return values
						target_titles.push(title);
						target_prices.push(mod.cost);
						target_mod_ids.push(mod.mod_id);
						target_newlines.push(newline);

						line++;
					}
				});
				
				titles = nochoice_titles.concat( whole_titles ).concat( left_titles ).concat( right_titles );
				prices = nochoice_prices.concat( whole_prices ).concat( left_prices ).concat( right_prices );
				mod_ids = nochoice_mod_ids.concat( whole_mod_ids ).concat( left_mod_ids ).concat( right_mod_ids );
				newlines = nochoice_newlines.concat( whole_newlines ).concat( left_newlines ).concat( right_newlines );

				titles_text = titles.join('|*|');
				prices_text = prices.join('|*|');
				mod_ids_text = mod_ids.join('|*|');
				newlines_text = newlines.join('|*|');
				//alert(titles_text);


				var cmd = "_DO:cmd=add_mod&mod_title="+encodeURIComponent(titles_text)+"&mod_price="+encodeURIComponent(prices_text)+"&mod_id="+encodeURIComponent(mod_ids_text)+"&mod_new_line="+encodeURIComponent(newlines_text);	
				if (!dontChangeLocation){
					do_com_cmd(cmd);
					//window.location = cmd;
				}
				

				return cmd;
			}

			PageTurner2 = {
				currentPageNum: 1,
				minPage: 1,
				maxPage: <?php echo get_categories_max_page(); ?>,
				turnPage: function(which) {

					// get some initial variables
					var currentPageNum = this.currentPageNum;
					var nextPageNum = this.setPage((which == 'next') ? currentPageNum+1 : currentPageNum-1);
					var jpageTurnBack = $(".page_turn.turn_page_back");
					var jpageTurnNext = $(".page_turn.turn_page_forward");

					// draw the page turners
					if (nextPageNum < this.maxPage) {
						jpageTurnNext.children().text("Page "+(nextPageNum+1));
						jpageTurnNext.show();
					} else {
						jpageTurnNext.hide();
					}
					if (nextPageNum > this.minPage) {
						jpageTurnBack.children().text("Page "+(nextPageNum-1));
						jpageTurnBack.show();
					} else {
						jpageTurnBack.hide();
					}

					// change the page
					if (currentPageNum != nextPageNum){
						//window.location = "_DO:cmd=send_js&c_name=pizza_mod&js=PageTurner.turnPage("+nextPageNum+")";  
						do_com_cmd('_DO:cmd=send_js&c_name=pizza_mod&js=PageTurner.turnPage("+nextPageNum+")');
					}

				},
				setPage: function(pageNum) {
					this.currentPageNum = Math.max(Math.min(pageNum,this.maxPage), this.minPage);
					return this.currentPageNum;
				},
				setPageMarker: function(pageNum) {
					var pageIcons = $(".page_icon");
					for (var i = 0; i < pageIcons.length; i++) {
						var jpageIcon = $(pageIcons[i]);
						if (jpageIcon.hasClass('icon_'+pageNum)) {
							jpageIcon.css({ 'background-color':'#ccc', 'box-shadow':'gray 0 0 5px 1px inset' });
						} else {
							jpageIcon.css({ 'background-color':'#fff', 'box-shadow':'#ccc 0 3px 3px 0' });
						}
					}
					this.setPage(pageNum);
				},
				updatePage: function(pageNum) {
					do_com_cmd("_DO:cmd=send_js&c_name=pizza_mod&js=setPage("+pageNum+")");
				}
			}

			setTimeout(function() {
				$(document).ready(function() {
					PageTurner2.setPageMarker(1);
				});
			}, 500);
		</script>

	</head>
	<body>

		<?php wrapper_draw_fmod_categories($a_forced_mod_categories, $dataname); ?>
		<div id="main_background_div_id" style="position:fixed; left:0px; top:0px; width:100%; height:100%; background: #eee url('<?php echo $pizza_blur_image_path; ?>') no-repeat fixed top; border:0px; border-radius:30px; z-index:-2;">
			
		</div>
		
		
		<script type='text/javascript'>
		
			//"touchStart(event);" <?php echo je('touchend'); ?>="if (touchSuccess(event)) { window.location='_DO:cmd=<?php echo $cancel_cmd;?>' }" 
			function close_icon_invoked(event){
				if (touchSuccess(event)){ 
					do_com_cmd('_DO:cmd=<?php echo $cancel_cmd;?>');
				}
			}

			function x_pressed(){
				do_com_cmd("_DO:cmd=<?php echo $cancel_cmd;?>");
			} 
			
			function set_paging_button_colors(pIndex){
				var page_icons_arr = document.getElementsByClassName('page_icon');
				for(var i = 0; i < page_icons_arr.length; i++){
					var currPageIconDiv = page_icons_arr[i];
					currPageIconDiv.style.backgroundColor = (i == pIndex) ? '#ccc' : '#fff';
				}
			}

		</script>
		<!-- END EDITING -->
		<div style="width:100%; height:100%; border-radius:30px; z-index:1; position:fixed; left:0; top:0;">
			<table><tr><td id="close_window_div_id" class="close_icon" ontouchstart="touchStart(event);" ontouchend="close_icon_invoked(event)" style="cursor:pointer; position:fixed; top:10px; left:10px; width:50px; height:50px; background-color:white; border:0px; border-radius:10px; box-shadow:gray 0 0 5px 1px inset; color:gray; font-size:38px;" align='center' valign='middle'>
				&#x2716;
			</td></tr></table>
			<div class="total_cost_container choices" style="position:fixed; top:10px; right:10px;">

			</div>
			<?php
			if (get_categories_max_page() > 1) {
				for ($i = 1; $i <= get_categories_max_page(); $i++) {
			?>
			<div class="page_icon icon_<?php echo $i; ?>" ontouchstart="touchStart(event);" <?php echo je('touchend'); ?>="if (touchSuccess(event)) { set_paging_button_colors(<?php echo ($i-1); ?>); PageTurner2.updatePage(<?php echo $i; ?>); }" style="cursor:pointer; position:fixed; top:<?php echo (-40+$i*60); ?>px; right:10px; width:50px; height:50px; background-color:<?php echo ($i == 1) ? "#ccc" : "#fff"; ?>; border:0px; border-radius:10px; box-shadow:gray 0 0 5px 1px inset; display:table;">
				<div style="margin:3px 0 0 16px; color:gray; font-size:38px; font-weight:bold;"><?php echo $i; ?></div>
			</div>
			<?php
				}
			}
			?>
			<div class="check_icon" ontouchstart="touchStart(event);" <?php echo je('touchend'); ?>="if (touchSuccess(event)) { apply_mods(); }" style="cursor:pointer; position:fixed; bottom:70px; right:10px; width:50px; height:50px; background-color:white; border:0px; border-radius:10px; box-shadow:gray 0 0 5px 1px inset;">
				<div style="cursor:pointer; margin:-2px 0 0 10px; color:gray; font-size:38px; font-weight:bold;">&#x2714;</div>
			</div>
		</div>
	</body>
</html>
