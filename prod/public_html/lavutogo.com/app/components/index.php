<?php
	// THIS FILE HANDLES IFRAME COMPONENT REQUESTS FOR LTG.  THE FIRST OF THESE IS THE PIZZA CREATOR, SMAME
	//  CODE THAT EXISTS WITHIN THE APP, THIS FILE IS CALLED AS AN IFRAME FOR ITS EMBEDDED USE.
	ini_set('display_errors', 0);
	require_once(dirname(__FILE__).'/../lib/lavuquery.php');
	$componentChoosen = $_REQUEST['cmp'];
	$dataname = $_REQUEST['dataname'];
	$expected_sec_key = sha1($dataname.'garbonzo');
	$component_width = isset($_REQUEST['component_width']) ? intval($_REQUEST['component_width']) : 800;
	$component_height = isset($_REQUEST['component_height']) ? intval($_REQUEST['component_height']) : 700;
	if($expected_sec_key != $_REQUEST['sec']){
		echo 'denied'; exit;
	}
	$merchantRow = getDatanameRowFromDB($dataname);
	$merchantRow['menu_data'] = json_decode($merchantRow['menu_data'], true);
	$merchantRow['location_data'] = json_decode($merchantRow['location_data'], true);
	$merchantRow['location_data'] = array_merge($merchantRow['menu_data']['config_settings'], $merchantRow['location_data']);
	if($componentChoosen == 'pizza'){
		$itemID = intval($_REQUEST['item_id']);
		ob_start();
		require_once(dirname(__FILE__).'/pizza/ltg_specific_functions.php');
		initialize_4_ltg($merchantRow, $itemID);
		//Hardcoded to 'Build a Brick Oven Pizza', for dn: punk_pizzeria. (takes menu id).
		echo "<center>";
		echo "<center>";
		echo "<div id='wrapper_div_id' style='position:relative'>";
		echo "<div style='position:fixed; left:0px; top:0px;width:".$component_width."px;height:".$component_height."px;z-index:8;overflow-y:scroll'>";
		require_once(dirname(__FILE__).'/pizza/pizza_mod_wrapper.php');
		echo "</div>";
		echo "<div id='forground_div_id' style='position:fixed;top:0px;left:60px;width:".($component_width-140)."px;height:".$component_height."px;z-index:40; overflow:auto;'>";//SET TO CENTER.
		require_once(dirname(__FILE__).'/pizza/pizza_mod.php');
		echo "</div></div></center>";
		$pizzaCreatorStr = ob_get_contents();
		ob_end_clean();
		$pizzaCreatorStr = str_replace("ontouchstart","onmousedown",$pizzaCreatorStr);
		$pizzaCreatorStr = str_replace("ontouchend","onmouseup",$pizzaCreatorStr);
		$pizzaCreatorStr = str_replace("event.touches[0]", "document", $pizzaCreatorStr);
		$pizzaCreatorStr = str_replace("touchSuccess(event)","1",$pizzaCreatorStr);
		$pizzaCreatorStr = str_replace("function 1","function bigcowfatdog()",$pizzaCreatorStr);
		echo $pizzaCreatorStr;
		exit;
	}

	function getDatanameRowFromDB($dataname){
		$result = mlavu_query("SELECT * FROM `lavutogo`.`merchants` WHERE `data_name`='[1]'",$dataname);
		if(!$result || mysqli_num_rows($result) == 0){ return false; }
		return mysqli_fetch_assoc($result);
	}
?>