<?php

	require_once(__DIR__."/togo_connect.php");
	require_once(__DIR__."/lib/lavuquery.php");
	require_once(__DIR__."/lib/pdo_lavuquery.php");

	function LTG_Updater($ltg_name)
	{
		GLOBAL $LTG_INFO;

		if (empty($LTG_INFO))
		{
			$LTGcheck = LTG_BasicInfo($ltg_name);
			$LTG_INFO = $LTGcheck;
		}
		else
		{
			$LTGcheck = $LTG_INFO;
		}

		if ($LTGcheck === false)
		{
			ltgDebug(__FILE__, __FUNCTION__." - LTGcheck is false", true);
			return false;
		}

		$lastCheckTS	= $LTGcheck['check_ts'];
		$lastUpdateTS	= $LTGcheck['lastmod_ts'];
		$json_cc		= $LTGcheck['cc_key'];
		$data_name		= $LTGcheck['data_name'];
		$loc_id			= $LTGcheck['location_id'];

		$force		= (empty($lastCheckTS) || empty($lastUpdateTS));
		$delay		= 10; // seconds between updates
		$currentTS	= time();

		if (!$force && (($lastCheckTS + $delay) > $currentTS))
		{
			ltgDebug(__FILE__, __FUNCTION__." - skipping update - lastCheckTS + delay: ".($lastCheckTS + $delay)." - currentTS: ".$currentTS);
			return false;
		}

		$queryArray = array( $currentTS, $ltg_name );

		PDO_MLavuConnectDB("lavutogo");
		PDO_MLavuPrepare("UPDATE `lavutogo`.`merchants` SET `check_ts` = ? WHERE `ltg_name` = ?", $queryArray);

		$lastUpdateTS_API = API_getLastUpdateTime($json_cc, $loc_id);
		if ($lastUpdateTS_API!==false && !$force && $lastUpdateTS==$lastUpdateTS_API)
		{
			ltgDebug(__FILE__, __FUNCTION__." - menu up-to-date - lastUpdateTS: ".$lastUpdateTS." - lastUpdateTS_API: ".$lastUpdateTS_API);
			return false;
		}

		ltgDebug(__FILE__, __FUNCTION__." - ready to get location and menu update - force: ".($force?"yes":"no"));

		$location_data = API_getLocInfo($json_cc, $loc_id);
		$lread = json_decode($location_data, 1);
		$tinfo = api_response("m=3&ltg=1&loc_id=".$loc_id."&menu_id=".$lread['menu_id']."&cc=".$json_cc);
		if ($tinfo)
		{
			$queryArray2 = array(
				'menu_data'		=> $tinfo,
				'lastmod_ts'		=> $lastUpdateTS_API,
				'location_data'	=> $location_data,
				'ltg_name'		=> $ltg_name
			);

			$result = PDO_MLavuPrepare(
				"UPDATE `lavutogo`.`merchants` SET `menu_data` = :menu_data , `lastmod_ts` = :lastmod_ts ,`location_data` = :location_data WHERE `ltg_name` = :ltg_name ;",
				$queryArray2
			);

			return true;
		}
		else
		{
			ltgDebug(__FILE__, __FUNCTION__." - failed to get update - empty response");
		}

		return false;
	}

	function LTG_BasicInfo($ltg_name)
	{
		$last_mod_results = mlavu_query("SELECT `lastmod_ts`,`check_ts`,`location_id`,`cc_key`,`data_name` FROM `merchants` WHERE `ltg_name` = '[1]'", $ltg_name);
		if (mysqli_num_rows($last_mod_results))
		{
			return mysqli_fetch_assoc($last_mod_results);
		}
		else
		{
			return false;
		}
	}

	function LTG_setCheckTS($ltg_name, $newTS)
	{
		mlavu_query("UPDATE `merchants` SET `check_ts` = '[2]' WHERE `ltg_name` = '[1]'", $ltg_name, $newTS);

		return true;
	}

	function API_getLocInfo($json_cc, $loc_id)
	{
		$return = API_call("cc=".$json_cc."&loc_id=".$loc_id."&mode=loc_info");

		return $return;
	}

	function API_getLastUpdateTime($json_cc, $loc_id)
	{
		$temp = API_call("cc=".$json_cc."&loc_id=".$loc_id."&mode=menu_time");
		$LTGcheck = json_decode($temp, 1);
		$return = $LTGcheck['menu_last_updated'];

		return $return;
	}

	function API_call($toSend)
	{
		$json_url = useLavuCentralURL("private")."/private_api/ltg2_api.php";

		ltgDebug(__FILE__, __FUNCTION__." sending request to ".$json_url);
		ltgDebug(__FILE__, __FUNCTION__." toSend: ".$toSend);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $json_url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $toSend);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 4);

		$curl_start = microtime(true);

		$response = curl_exec($ch);

		$curl_end = microtime(true);
		$curl_duration = round((($curl_end - $curl_start) * 1000), 2);
		$curl_info = curl_getinfo($ch);
		$curl_error = curl_error($ch);

		$info_and_error = (empty($response))?" - info: ".json_encode($curl_info)." - error: ".$curl_error:"";

		ltgDebug(__FILE__, __FUNCTION__." - response: ".(empty($response)?"empty":substr($response, 0, 64)."...")." - duration: ".$curl_duration." ms".$info_and_error);

		curl_close($ch);

		return $response;
	}
?>