<?php
putenv("TZ=America/Chicago");
session_start();
require_once(dirname(__FILE__).'/lib/core_functions.php');

$order_id = postvar("order_id");
if(!empty($order_id)){
	set_sessvar("order_id", $order_id);
}

$cart_contents = postvar("cart_contents");
if(!empty($cart_contents)){
	set_sessvar("cart_contents", $cart_contents);
}

$saved_ltgname = postvar("ltg_name");
if(!empty($saved_ltgname)){
	set_sessvar("saved_ltg_name", $saved_ltgname);
}

$additional_session_info_post = postvar("additional_session_info_obj");
if(!empty($additional_session_info_post)){
	$previousAdditionalSessionInfoJSONArr = sessvar('additional_session_info_obj') ? sessvar('additional_session_info_obj') : "[]";
	$additionalSessionInfoArr = json_decode($previousAdditionalSessionInfoJSONArr,1);
	$additional_session_info_obj = json_decode($additional_session_info_post,1);
	//Sub-arrays must be merged individually, then replaced back in.
	$additional_f_mod_info_POST = $additional_session_info_obj['additional_f_mod_info'];
	$additional_f_mod_info_SESS = !empty($additionalSessionInfoArr['additional_f_mod_info']) ? $additionalSessionInfoArr['additional_f_mod_info'] : array();
	$merged_additional_f_mod_info = array_merge($additional_f_mod_info_POST,$additional_f_mod_info_SESS);
	$additionalSessionInfoArr['additional_f_mod_info'] = $merged_additional_f_mod_info;
	set_sessvar("additional_session_info_obj", json_encode($additionalSessionInfoArr));
}