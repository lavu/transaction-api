<?php
error_reporting(E_ALL);
ini_set('display_errors','1');
echo "<pre>";
require_once(__DIR__.'/app/lib/lavuquery.php');

mlavu_connect_db();
mlavu_select_db("lavutogo");
$result = mlavu_query('SELECT DISTINCT(`data_name`) as "dn" FROM `merchants` ORDER BY `data_name`;');
$datanames = array();
while($temp = mysqli_fetch_assoc($result ) ){
	$datanames[] = $temp['dn'];
}

$datanamesInfo = curlChecker($datanames);

foreach ($datanamesInfo['bad'] as $bad_dataname) {
	echo "$bad_dataname\n";
	deleteLTGDN($bad_dataname);
}

function curlChecker($datanames){
	$toSend = array();
	$toSend['command'] = 'DatanamesCheck';
	$toSend['dataname'] = json_encode($datanames);
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, 'https://admin.poslavu.com/private_api/ltg3-api.php');
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($toSend));
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	$contents = curl_exec ($ch);
	curl_close ($ch);
	#if($contents == 'bad'){return false;}
	#if($contents == 'good'){return true;}
	#trigger_error('Bad Curl?');
	$toReturn = json_decode($contents,true);
	return $toReturn;
}

function deleteLTGDN($dataname){
	mlavu_query('DELETE FROM `merchants` WHERE `data_name` = "'.$dataname.'";');
}