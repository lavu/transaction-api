<?php
	session_start();

	//if($_SERVER['REMOTE_ADDR']!='74.95.18.90' && $_SERVER['REMOTE_ADDR']!="75.161.18.90" && $_SERVER['REMOTE_ADDR']!='67.42.135.99' && $_SERVER['REMOTE_ADDR']!='67.0.123.196' && $_SERVER['REMOTE_ADDR']!='174.56.118.101' && $_SERVER['REMOTE_ADDR']!='67.0.157.118' && $_SERVER['REMOTE_ADDR']!="174.28.86.221" && $_SERVER['REMOTE_ADDR']!='50.130.130.9' && $_SERVER['REMOTE_ADDR']!='174.28.78.240' && $_SERVER['REMOTE_ADDR']!='50.56.125.112' && $_SERVER['REMOTE_ADDR']!="97.123.27.213" && $_SERVER['REMOTE_ADDR']!="209.181.115.24")
	//if (!in_array($_SERVER['REMOTE_ADDR'], array("74.95.18.90", "67.42.135.99", "67.0.123.196", "209.181.115.24", "50.56.125.112")))
	 // Lavu Extreme, Corey's House, Richard's House, Lavu Treehouse, Lavu ToGo
	//{
	//	echo "...";
	//	exit();
	//}
	
	date_default_timezone_set("America/Denver");
	ini_set("max_execution_time", "150");
	require_once(dirname(__FILE__) . "/functions.php");
	$Conn = lavu_connect_db();
	lavu_select_db("lsvpn");
	
	$mode = getvar("mode");
	
	$name = "incoming request";
	$url = $_SERVER['REQUEST_URI'];
	$fpath = $_SERVER['PHP_SELF'];
	$fpath_end = strrpos($fpath,"/");
	
	if($fpath_end)
	{
		$fpath = substr($fpath,0,$fpath_end);
		$url = substr($url,$fpath_end);
	}

	$post_str = "";
	foreach($_POST as $key => $val)
	{
		if($post_str!="") $post_str .= "&";
		$post_str .= $key . "=" . urlencode($val);
	}

	$post = $post_str;
	$reply = "";
	$datetime = date("Y-m-d H:i:s");
	$processed = "0";
	$responded = "0";
	if(isset($_GET['cc'])) $cc = $_GET['cc'];
	else if(isset($_POST['cc'])) $cc = $_POST['cc'];
	else $cc = "";
	
	$cc_parts = explode("_key_",$cc);
	$cc = $cc_parts[0];
	$key = $cc_parts[1];
	
	if($cc=="")
	{
		if(isset($_SESSION['session_cc']))
		{
			$cc = $_SESSION['session_cc'];
		}
		
		if(isset($_SESSION['session_cc_key'])){
			$key = $_SESSION['session_cc_key'];
		}
	}
	
	if($cc=="" || $key == "")
	{
		echo "...";
		exit();
	}
	
	$_SESSION['session_cc'] = $cc;
	$_SESSION['session_cc_key'] = $key;
	
	{//SECURITY CHECK
		$security_result = lavu_query("SELECT * FROM `servers` WHERE `dataname` = '[1]' AND `cc_key` = PASSWORD('[2]')", $cc, $key);
		if(!$security_result){
			echo "Something Went Wrong.";
			exit();
		}
		
		if(!mysqli_num_rows($security_result)){
			echo "Invalid Credentials.";
			exit();	
		}
		
		mysqli_free_result($security_result);
	}
	
	if(strpos($url,"."))
	{
		$ext = strtolower(substr($url,strrpos($url,".") + 1));
		$ext_parts = explode("?",$ext);
		if(count($ext_parts) > 1)
			$ext = $ext_parts[0];
		
		if($ext=="jpg" || $ext=="jpe" || $ext=="gif" || $ext=="png")
		{
			lavu_close_db();
			
			//header('Content-type: image/png;');
			//$full_url = "http://admin.poslavu.com".$url;
			//$img_str = file_get_contents('$full_url');
			//echo "img: " . $img_str;
			//echo "ext: " . $ext . "<br>";
			
			header("Location: http://admin.poslavu.com" . $url);
			exit();
		}
	}
	
	//echo $url;
	//exit();
	
	lavu_query("insert into `requests` (`name`,`url`,`post`,`reply`,`datetime`,`processed`,`responded`,`cc`) values ('[1]','[2]','[3]','[4]','[5]','[6]','[7]','[8]')",$name,$url,$post,$reply,$datetime,$processed,$responded,$cc);
	$request_id = lavu_insert_id();
	
	$listen_continue = true;
	$listen_count = 0;
	$listen_max_count = 300;//120; // 60 seconds (with half second interval)
	$listen_interval = 2;//5; // half second
	$str = "";
	
	while($listen_continue)
	{
		$req_query = lavu_query("select * from `requests` where `processed`='1' and `id`='[1]' order by id desc limit 1",$request_id);
		if(mysqli_num_rows($req_query))
		{
			$req_read = mysqli_fetch_assoc($req_query);
			//lavu_query("update `requests` set `responded`='1' where `id`='[1]'",$request_id);
			lavu_query("delete from `requests` where `id`='[1]'",$request_id);
			$str = $req_read['reply'];
			$listen_continue = false;
			$timeoutdate = date("Y-m-d H:i:s", mktime(0,0,0,date("m"),date("d")-2,date("Y")));
			lavu_query("delete from `requests` where `datetime` < '[1]'", $timeoutdate);
		}
		else
		{
			time_nanosleep(0, $listen_interval * 100000000);
			$listen_count++;
			if($listen_count >= $listen_max_count)
			{
				$str = "";
				$listen_continue = false;
			}
		}
	}
	
	echo $str;		
	
	lavu_close_db();
