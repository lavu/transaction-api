<?php
	$Conn = false;
	
	function postvar($var,$def="")
	{
		return (isset($_POST[$var]))?$_POST[$var]:$def;
	}
	
	function getvar($var,$def="")
	{
		return (isset($_GET[$var]))?$_GET[$var]:$def;
	}
	
	function lavu_connect_db()
	{
		global $Conn;
		$Conn = mysqli_connect("localhost","lsvpn","tnk4zig");
		return $Conn;
	}
	
	function lavu_insert_id()
	{
		global $Conn;
		return mysql_insert_id($Conn);
	}
	
	function lavu_close_db()
	{
		global $Conn;
		mysqli_close($Conn);
		$Conn = false;
	}
	
	function lavu_select_db($setdb)
	{
		global $Conn;
		mysql_select_db($setdb,$Conn);
	}
	
	function lavu_query_encode($str)
	{
		global $Conn;
		return mysqli_real_escape_string($str,$Conn);
	}
	
	function lavu_query($query,$var1="[_empty_]",$var2="[_empty_]",$var3="[_empty_]",$var4="[_empty_]",$var5="[_empty_]",$var6="[_empty_]",$var7="[_empty_]",$var8="[_empty_]")
	{	
		global $Conn;

		if($var1!="[_empty_]" && is_array($var1))
		{
			foreach($var1 as $key => $val)
			{
				$query = str_replace("[$key]",lavu_query_encode($val),$query);
			}
		}
		else
		{
			if($var1!="[_empty_]") $query = str_replace("[1]","[-__1__-]",$query);
			if($var2!="[_empty_]") $query = str_replace("[2]","[-__2__-]",$query);
			if($var3!="[_empty_]") $query = str_replace("[3]","[-__3__-]",$query);
			if($var4!="[_empty_]") $query = str_replace("[4]","[-__4__-]",$query);
			if($var5!="[_empty_]") $query = str_replace("[5]","[-__5__-]",$query);
			if($var6!="[_empty_]") $query = str_replace("[6]","[-__6__-]",$query);
			if($var7!="[_empty_]") $query = str_replace("[7]","[-__7__-]",$query);
			if($var8!="[_empty_]") $query = str_replace("[8]","[-__8__-]",$query);

			if($var1!="[_empty_]") $query = str_replace("[-__1__-]",lavu_query_encode($var1),$query);
			if($var2!="[_empty_]") $query = str_replace("[-__2__-]",lavu_query_encode($var2),$query);
			if($var3!="[_empty_]") $query = str_replace("[-__3__-]",lavu_query_encode($var3),$query);
			if($var4!="[_empty_]") $query = str_replace("[-__4__-]",lavu_query_encode($var4),$query);
			if($var5!="[_empty_]") $query = str_replace("[-__5__-]",lavu_query_encode($var5),$query);
			if($var6!="[_empty_]") $query = str_replace("[-__6__-]",lavu_query_encode($var6),$query);
			if($var7!="[_empty_]") $query = str_replace("[-__7__-]",lavu_query_encode($var7),$query);
			if($var8!="[_empty_]") $query = str_replace("[-__8__-]",lavu_query_encode($var8),$query);
		}
		
		$result = mysql_query($query, $Conn);
					
		return $result;
	}	
?>
