<?php
	ini_set("max_execution_time", "150");
	require_once(dirname(__FILE__) . "/functions.php");
	$Conn = lavu_connect_db();
	lavu_select_db("lsvpn");
	
	$mode = getvar("mode");
	
	date_default_timezone_set("America/Denver");
	if($mode=="listen_requests")
	{		
		$min_datetime = date("Y-m-d H:i:s",(time() - 120));
		
		$cc = getvar("cc");
		$cc_key = postvar("cc_key");
		$lsvpn_version = getvar("lsvpn_version","");
		$listen_continue = true;
		$listen_count = 0;
		$listen_max_count = 600;//240; // 120 seconds (with half second interval)
		$listen_interval = 2;//5; // half second
		$str = "";
		$to_update = "";
		
		$cc_query = lavu_query("select * from `servers` where `dataname`='[1]' and `ipaddress`='[2]' order by id desc limit 1",$cc,$_SERVER['REMOTE_ADDR']);
		if(mysqli_num_rows($cc_query))
		{
			$cc_read = mysqli_fetch_assoc($cc_query);
			$status_id = $cc_read['id'];
			$cc_key = $cc_read['cc_key'];
			if(empty($cc_key)){
				$to_update = ", `cc_key`=PASSWORD('[4]')";
			}
			
			
			if($cc_read['connection_started']=="" || (time() - $cc_read['last_connection'] * 1) > 4)
				$set_connection_started = time();
			else
				$set_connection_started = $cc_read['connection_started'];		
		}
		else
		{
			lavu_query("insert into `servers` (`dataname`,`ipaddress`,`last_connection`,`connection_started`,`lsvpn_version`) values ('[1]','[2]','','','[3]')",$cc,$_SERVER['REMOTE_ADDR'],$lsvpn_version);
			$status_id = lavu_insert_id();
			$set_connection_started = time();
		}
		
		lavu_query("update `servers` set `connection_started`='[1]', `lsvpn_version`='[2]' $to_update where `id`='[3]'",$set_connection_started,$lsvpn_version,$status_id, $cc_key);
		
		while($listen_continue)
		{
			lavu_query("update `servers` set `last_connection`='[1]' where `id`='[2]'",time(),$status_id);
			$req_query = lavu_query("select * from `requests` where `processed`!='1' and `datetime`>='[1]' and `cc`='[2]' order by datetime asc limit 1",$min_datetime,$cc);
			if(mysqli_num_rows($req_query))
			{
				$req_read = mysqli_fetch_assoc($req_query);
				lavu_query("update `requests` set `processed`='2' where `id`='[1]'",$req_read['id']);
				
				$str .= "found=1";
				foreach($req_read as $key => $val)
				{
					$str .= "&" . $key . "=" . urlencode($val);
				}
				$listen_continue = false;
			}
			else
			{
				time_nanosleep(0, $listen_interval * 100000000);
				$listen_count++;
				if($listen_count >= $listen_max_count)
				{
					$str .= "found=0&datetime=" . urlencode(date("Y-m-d H:i:s"));
					$listen_continue = false;
				}
			}
		}
		
		echo $str;
	}
	else if($mode=="send_reply")
	{
		$reply = postvar("reply");
		$id = postvar("id");
		$reply_cmd = postvar("reply_cmd","");
		
		if($reply_cmd=="remove")
		{
			lavu_query("delete from `requests` where `id`='[1]' limit 1",$id);
		}
		else
		{
			lavu_query("update `requests` set `processed`='1', `reply`='[1]' where `id`='[2]'",$reply,$id);
		}
	}
	
	lavu_close_db();