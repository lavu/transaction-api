<?php
	require_once(dirname(__FILE__) . "/functions.php");
	$Conn = lavu_connect_db();
	lavu_select_db("lsvpn");
	
	$cc = getvar("cc");
	$ccname = getvar("ccname");
	$lcount = getvar("lcount",0);
	$cmd = getvar("cmd","");
	$connection_lost = false;
	
	if($cc!="")
	{
		echo "<style>";
		echo " body, table { font-family:Verdana,Arial; font-size:12px; } ";
		echo "</style>";
		echo "Tunnel Status for: $ccname";
		echo "<br><br>";
		
		if($cmd=="disconnect")
		{
			$name = "command";
			$url = "cmd/disconnect";
			$post = "";
			$reply = "";
			$datetime = date("Y-m-d H:i:s");
			$processed = "0";
			$responded = "0";
			
			lavu_query("insert into `requests` (`name`,`url`,`post`,`reply`,`datetime`,`processed`,`responded`,`cc`) values ('[1]','[2]','[3]','[4]','[5]','[6]','[7]','[8]')",$name,$url,$post,$reply,$datetime,$processed,$responded,$cc);
		}
		
		$server_count = 0;
		$status_query = lavu_query("select * from `servers` where `dataname`='[1]' and `last_connection` * 1 > '[2]' * 1",$cc,time() - 60);
		while($status_read = mysqli_fetch_assoc($status_query))
		{
			$connected_for = $status_read['last_connection'] * 1 - $status_read['connection_started'] * 1;
			$lsvpn_version = $status_read['lsvpn_version'];
			
			$seconds = $connected_for % 60;
			$minutes = ($connected_for - $seconds) / 60;

			if($seconds < 10)
				$seconds = "0" . ($seconds * 1);
			
			$show_duration = "";
			if($minutes > 0)
			{
				$total_minutes = $minutes;
				$minutes = $total_minutes % 60;
				$hours = ($total_minutes - $minutes) / 60;
				
				if($hours > 0)
				{
					$show_duration .= "<font color='#000088'><b>$hours:$minutes:$seconds</b></font> Hours";
				}
				else
				{
					$show_duration .= "<font color='#000088'><b>$minutes:$seconds</b></font> Minute";
					if($minutes != 1) $show_duration .= "s";
				}
			}
			else
			{
				$show_duration .= " <font color='#000088'><b>$seconds</b></font> Second";
				if($seconds != 1) $show_duration .= "s";
			}
			
			echo "Server: <font color='#008800'>" . $status_read['ipaddress'] . "</font> - ";
			
			if($status_read['last_connection'] * 1 > time() - 4)
			{
				echo "Connected $show_duration";
				if($lsvpn_version!="")
					echo " <font color='#008800'>(v".$lsvpn_version.")</font>";
			}
			else
			{
				echo "Connection Lost";
				$connection_lost = true;
			}
			echo "<br>";
			
			$server_count++;
		}
		
		if($server_count < 1)
		{
			if($lcount <= 60)
			{
				echo "Waiting for Connection ";
				for($i=0; $i<$lcount; $i++)
					echo ".";
			}
			else
			{
				echo "No Servers Connected";
			}
		}
		else
		{
			if($lsvpn_version > 1)
			{
				if($server_count > 0)
				{
					if($connection_lost)
					{
						//echo "<br><br><input type='button' value='Reconnect' onclick='window.parent.refresh()' />";
					}
					else
					{
						echo "<br><br><input type='button' value='Disconnect' onclick='window.location = \"show_status.php?cc=$cc&ccname=$ccname&lcount=$lcount&cmd=disconnect\"' />";
					}
				}
			}
		}

		$lcount++;
		echo "<script language='javascript'>";
		echo "function reload_status() { ";
		echo "  window.location = 'show_status.php?cc=$cc&ccname=$ccname&r=".rand(10000,99999)."&lcount=$lcount&t=".time()."'; ";
		echo "} ";
		echo "setInterval('reload_status()', 1000); ";
		echo "</script>";
	}
?>
