

var LavuObject = function(obj){
	Object.call(this);
	
}
LavuObject.prototype.constructor = LavuObject;
LavuObject.prototype = new Object();

var Deletable = function(obj){
	LavuObject.call(this);
	// PRIVATE
	var private = {
		__deleted : 0
	};
	
	for(name in private){
		if(obj && obj[name.substr(1)]){
			private[name] = obj[name.substr(1)];
		}
	}
	
	this.getDeleted = function() { return private.__deleted; }
	this.setDeleted = function( deleted ) { private.__deleted = deleted; }
}
Deletable.prototype.constructor = Deletable;
Deletable.prototype = new LavuObject();

var Orderable = function(obj){
	Deletable.call(this);
	// PRIVATE
	var private = {
		__order : 0
	}
	
	for(name in private){
		if(obj && obj[name.substr(1)]){
			private[name] = obj[name.substr(1)];
		}
	}
	
	this.getOrder = function() { return private.__order; }
	this.setOrder = function( order ) { private.__order = order; }
}
Orderable.prototype.constructor = Orderable;
Orderable.prototype = new Deletable();