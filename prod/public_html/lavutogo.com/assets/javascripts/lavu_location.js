/***
 * Location Class
 **/
var Location = function(obj){
	
	var private = {
		_id                   : 0,
		_title                : "",
		_address              : "",
		_city                 : "",
		_state                : "",
		_zip                  : "",
		_phone                : "",
		_website              : "",
		_manager              : "",
		_taxrate              : 0,
		_menu_id              : 0,
		_menu				  : null,
		_net_path_print       : "",
		_country              : "",
		_monitary_symbol      : "",
		_left_or_right        : "",
		_gateway              : "",
		_cc_transtype         : "",
		_disable_decimal      : 0,
		_day_start_end_time   : "",
		_timezone             : "",
		_decimal_char         : "",
		_thousands_char       : "",
		_config				  : []
	};
	
	for(name in private){
		if(obj && obj[name.substr(1)]){
			private[name] = obj[name.substr(1)];
		}
	}
	
	this.getID                 = function() { return private._id; };
	this.getTitle              = function() { return private._title; };
	this.getAddress            = function() { return private._address; };
	this.getCity               = function() { return private._city; };
	this.getState              = function() { return private._state; };
	this.getZip                = function() { return private._zip; };
	this.getPhone              = function() { return private._phone; };
	this.getWebsite            = function() { return private._website; };
	this.getManager            = function() { return private._manager; };
	this.getTaxRate            = function() { return private._taxrate; };
	this.getMenuID             = function() { return private._menu_id; };
	this.getNetPathPrint       = function() { return private._net_path_print; };
	this.getCountry            = function() { return private._country; };
	this.getMonitarySymbol     = function() { return private._monitary_symbol; };
	this.getLeftOrRight        = function() { return private._left_or_right; };
	this.getGateway            = function() { return private._gateway; };
	this.getCCTransType        = function() { return private._cc_transtype; };
	this.getDisableDecimal     = function() { return private._disable_decimal; };
	this.getDayStartEndTime    = function() { return private._day_start_end_time; };
	this.getTimezone           = function() { return private._timezone; };
	this.getDecimalChar        = function() { return private._decimal_char; };
	this.getThousandsChar      = function() { return private._thousands_char; };
}