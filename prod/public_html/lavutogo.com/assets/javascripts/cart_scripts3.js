//
// Modified so 'shopping_cart' is 'Shopping_Cart'
//
(function(){
	if (! window.LavuToGo ){
		window.LavuToGo = {};
	}

	// 1 = CREDIT CARD PRE PAY ONLY
	// 2 = ON RECEIPT/Pick UP
	// 3 = BOLTH

	LAVUTOGO_PRE_PAY = 0x01 << 0;
	LAVUTOGO_PAYMENT_ON_PICKUP = 0x01 << 1;
	LAVUTOGO_PAYMENT_ON_PICKUP_OR_PRE_PAY = LAVUTOGO_PRE_PAY | LAVUTOGO_PAYMENT_ON_PICKUP;

	(function(){
		function LocationInfo(){
			this._address = 0;
			this._city = 0;
			this._country = 0;
			this._decimal_char = 0;
			this._disable_decimal = 0;
			this._modules = 0;
			this._monitary_symbol = 0;
			this._primary_currency_code = 0;
			this._state = 0;

			this._tax_inclusion = 0;
			this._taxrate = 0;
			this._thousands_char = 0;
			this._timezone = 0;
			this._title = 0;
			this._website = 0;
			this._zip = 0;

			this._ltg_email = 0;
			this._ltg_iframe_src = 0;
			this._ltg_phone_carrier = 0;
			this._ltg_phone_number = 0;
			this._ltg_print_failed_alarm = 0;
			this._ltg_use_opt_mods = 0;
		}

		function address(){ return this._address; }
		function city(){ return this._city; }
		function country(){ return this._country; }
		function decimal_char(){ return this._decimal_char; }
		function disable_decimal(){ return this._disable_decimal; }
		function ltg_email(){ return this._ltg_email; }
		function ltg_iframe_src(){ return this._ltg_iframe_src; }
		function ltg_phone_carrier(){ return this._ltg_phone_carrier; }
		function ltg_phone_number(){ return this._ltg_phone_number; }
		function ltg_print_failed_alarm(){ return this._ltg_print_failed_alarm; }
		function ltg_use_opt_mods(){ return this._ltg_use_opt_mods; }
		function modules(){ return this._modules; }
		function monitary_symbol(){ return this._monitary_symbol; }
		function primary_currency_code(){ return this._primary_currency_code; }
		function state(){ return this._state; }
		function tax_inclusion(){ return this._tax_inclusion == '1'; }
		function taxrate(){ return this._taxrate; }
		function thousands_char(){ return this._thousands_char; }
		function timezone(){ return this._timezone; }
		function title(){ return this._title; }
		function website(){ return this._website; }
		function zip(){ return this._zip; }

		var singleton_location = null;
		function createLocationInfo( json ){
			if(!singleton_location){
				singleton_location = new LocationInfo();
				for( var field in json ){
					singleton_location['_' + field] = json[field];
				}
			}

			LavuToGo.NumberFormatter.createNumberFormatter(singleton_location._disable_decimal, singleton_location._monitary_symbol, singleton_location._left_or_right, singleton_location._decimal_char);
			return singleton_location;
		}

		LavuToGo.LocationInfo = LocationInfo;
		LavuToGo.LocationInfo.prototype.constructor = LocationInfo;
		LavuToGo.LocationInfo.prototype.address = address;
		LavuToGo.LocationInfo.prototype.city = city;
		LavuToGo.LocationInfo.prototype.country = country;
		LavuToGo.LocationInfo.prototype.decimal_char = decimal_char;
		LavuToGo.LocationInfo.prototype.disable_decimal = disable_decimal;
		LavuToGo.LocationInfo.prototype.modules = modules;
		LavuToGo.LocationInfo.prototype.monitary_symbol = monitary_symbol;
		LavuToGo.LocationInfo.prototype.primary_currency_code = primary_currency_code;
		LavuToGo.LocationInfo.prototype.state = state;
		LavuToGo.LocationInfo.prototype.tax_inclusion = tax_inclusion;
		LavuToGo.LocationInfo.prototype.taxrate = taxrate;
		LavuToGo.LocationInfo.prototype.thousands_char = thousands_char;
		LavuToGo.LocationInfo.prototype.timezone = timezone;
		LavuToGo.LocationInfo.prototype.title = title;
		LavuToGo.LocationInfo.prototype.website = website;
		LavuToGo.LocationInfo.prototype.zip = zip;
		LavuToGo.LocationInfo.prototype.ltg_email = ltg_email;
		LavuToGo.LocationInfo.prototype.ltg_iframe_src = ltg_iframe_src;
		LavuToGo.LocationInfo.prototype.ltg_phone_carrier = ltg_phone_carrier;
		LavuToGo.LocationInfo.prototype.ltg_phone_number = ltg_phone_number;
		LavuToGo.LocationInfo.prototype.ltg_print_failed_alarm = ltg_print_failed_alarm;
		LavuToGo.LocationInfo.prototype.ltg_use_opt_mods = ltg_use_opt_mods;
		LavuToGo.LocationInfo.createLocationInfo = createLocationInfo;
	})();

	(function(){
		function MenuCategory(){
			this._active = 0;
			this._apply_taxrate = 0;
			this._chain_reporting_group = 0;
			this._custom_taxrate = 0;
			this._description = 0;
			this._forced_modifier_group_id = 0;
			this._group_id = 0;
			this._happyhour = 0;
			this._id = 0;
			this._image = 0;
			this._last_modified_date = 0;
			this._ltg_display = 0;
			this._menu_id = 0;
			this._modifier_list_id = 0;
			this._name = 0;
			this._price_tier_profile_id = 0;
			this._print = 0;
			this._print_order = 0;
			this._printer = 0;
			this._super_group_id = 0;
			this._tax_inclusion = 0;
			this._tax_profile_id = 0;
		}

		function active() { return this._active; }
		function apply_taxrate() { return this._apply_taxrate; }
		function chain_reporting_group() { return this._chain_reporting_group; }
		function custom_taxrate() { return Number(this._custom_taxrate); }
		function description() { return this._description; }
		function forced_modifier_group_id() { return this._forced_modifier_group_id; }
		function group_id() { return this._group_id; }
		function happyhour() { return this._happyhour; }
		function id() { return this._id; }
		function image() { return this._image; }
		function last_modified_date() { return this._last_modified_date; }
		function ltg_display() { return this._ltg_display; }
		function menu_id() { return this._menu_id; }
		function modifier_list_id() { return this._modifier_list_id; }
		function name() { return this._name; }
		function price_tier_profile_id() { return this._price_tier_profile_id; }
		function print() { return this._print; }
		function print_order() { return this._print_order; }
		function printer() { return this._printer; }
		function super_group_id() { return this._super_group_id; }
		function tax_profile_id() { return this._tax_profile_id; }
		function tax_inclusion() {
			if( this._tax_inclusion == 'Default' || this._tax_inclusion === ''){
				var locationInfo = LavuToGo.LocationInfo.createLocationInfo();
				return locationInfo.tax_inclusion();
			}
			return this._tax_inclusion == '1';
		}

		var _menu_categories = [];
		function createMenuCategory( json ){
			var result = new MenuCategory();
			for( var i = 0; i < _menu_categories.length; i++ ){
				if( _menu_categories[i].id() == json.id ){
					return _menu_categories[i];
				}
			}

			for( var property in json ){
				if( result['_' + property] !== undefined ){
					result['_' + property] = json[property];
				}
			}

			_menu_categories.push( result );
			return result;
		}

		function getMenuCategoryByID( id ){
			for( var i = 0; i < _menu_categories.length; i++ ){
				if( _menu_categories[i].id() == id ){
					return _menu_categories[i];
				}
			}
			return null;
		}

		function menuCategories(){
			return _menu_categories;
		}

		LavuToGo.MenuCategory = MenuCategory;
		LavuToGo.MenuCategory.prototype.active = active;
		LavuToGo.MenuCategory.prototype.apply_taxrate = apply_taxrate;
		LavuToGo.MenuCategory.prototype.chain_reporting_group = chain_reporting_group;
		LavuToGo.MenuCategory.prototype.constructor = MenuCategory;
		LavuToGo.MenuCategory.prototype.custom_taxrate = custom_taxrate;
		LavuToGo.MenuCategory.prototype.description = description;
		LavuToGo.MenuCategory.prototype.forced_modifier_group_id = forced_modifier_group_id;
		LavuToGo.MenuCategory.prototype.group_id = group_id;
		LavuToGo.MenuCategory.prototype.happyhour = happyhour;
		LavuToGo.MenuCategory.prototype.id = id;
		LavuToGo.MenuCategory.prototype.image = image;
		LavuToGo.MenuCategory.prototype.last_modified_date = last_modified_date;
		LavuToGo.MenuCategory.prototype.ltg_display = ltg_display;
		LavuToGo.MenuCategory.prototype.menu_id = menu_id;
		LavuToGo.MenuCategory.prototype.modifier_list_id = modifier_list_id;
		LavuToGo.MenuCategory.prototype.name = name;
		LavuToGo.MenuCategory.prototype.price_tier_profile_id = price_tier_profile_id;
		LavuToGo.MenuCategory.prototype.print = print;
		LavuToGo.MenuCategory.prototype.print_order = print_order;
		LavuToGo.MenuCategory.prototype.printer = printer;
		LavuToGo.MenuCategory.prototype.super_group_id = super_group_id;
		LavuToGo.MenuCategory.prototype.tax_inclusion = tax_inclusion;
		LavuToGo.MenuCategory.prototype.tax_profile_id = tax_profile_id;
		LavuToGo.MenuCategory.createMenuCategory = createMenuCategory;
		LavuToGo.MenuCategory.getMenuCategoryByID = getMenuCategoryByID;
		LavuToGo.MenuCategory.menuCategories = menuCategories;
	})();

	(function(){
		function MenuItems(){
			this._items = [];
		}

		function items(){
			return this._items;
		}

		function addItem( menuItem ){
			if(!menuItem ){
				return;
			}

			if(!(menuItem instanceof LavuToGo.MenuItem)){
				return;
			}

			this._items.push( menuItem );
		}

		var menu_items = null;
		function menuItems(){
			if(!menu_items){
				menu_items = new MenuItems();
			}
			return menu_items;
		}

		function getMenuItemByID( id ){
			for(var i = 0; i < this._items.length; i++ ){
				if(this._items[i].id() == id ){
					return this._items[i];
				}
			}
			return null;
		}

		LavuToGo.MenuItems = MenuItems;
		LavuToGo.MenuItems.prototype.constructor = MenuItems;
		LavuToGo.MenuItems.prototype.items = items;
		LavuToGo.MenuItems.prototype.addItem = addItem;
		LavuToGo.MenuItems.prototype.getMenuItemByID = getMenuItemByID;
		LavuToGo.MenuItems.menuItems = menuItems;
	})();

	(function(){
		function MenuItem(){
			this._active = false;
			this._allow_deposit = '';
			this._apply_taxrate = 0;
			this._category_id = 0;
			this._chain_reporting_group = 0;
			this._custom_taxrate = 0;
			this._description = '';
			this._forced_modifier_group_id = 0;
			this._happyhour = 0;
			this._hidden_value = '';
			this._hidden_value2 = '';
			this._hidden_value3 = '';
			this._id = 0;
			this._image = '';
			this._image2 = '';
			this._image3 = '';
			this._ingredients = '';
			this._inv_count = 0;
			this._last_modified_date = '';
			this._ltg_display = false;
			this._menu_id = 0;
			this._misc_content = '';
			this._modifier_list_id = 0;
			this._name = '';
			this._open_item = '';
			this._options1 = '';
			this._options2 = '';
			this._options3 = '';
			this._price = 0;
			this._price_tier_profile_id = 0;
			this._print = false;
			this._printer = '';
			this._quick_item = '';
			this._show_in_app = false;
			this._super_group_id = 0;
			this._tax_inclusion = false;
			this._tax_profile_id = 0;
			this._track_86_count = false;
			this._UPC = '';
		}

		function allow_deposit() { return this._allow_deposit; }
		function apply_taxrate() { return this._apply_taxrate; }
		function category_id() { return this._category_id; }
		function chain_reporting_group() { return this._chain_reporting_group; }
		function custom_taxrate() { return Number(this._custom_taxrate); }
		function description() { return this._description; }
		function happyhour() { return this._happyhour; }
		function hidden_value() { return this._hidden_value; }
		function hidden_value2() { return this._hidden_value2; }
		function hidden_value3() { return this._hidden_value3; }
		function id() { return this._id; }
		function image() { return this._image; }
		function image2() { return this._image2; }
		function image3() { return this._image3; }
		function ingredients() { return this._ingredients; }
		function inv_count() { return this._inv_count; }
		function last_modified_date() { return this._last_modified_date; }
		function menu_id() { return this._menu_id; }
		function misc_content() { return this._misc_content; }
		function name() { return this._name; }
		function open_item() { return this._open_item; }
		function options1() { return this._options1; }
		function options2() { return this._options2; }
		function options3() { return this._options3; }
		function price() { return this._price; }
		function print() { return this._print; }
		function price_tier_profile_id() { return this._price_tier_profile_id; }
		function quick_item() { return this._quick_item; }
		function show_in_app() { return this._show_in_app; }
		function super_group_id() { return this._super_group_id; }
		function track_86_count() { return this._track_86_count; }
		function UPC() { return this._UPC; }
		//These defer to Category default if they aren't set.
		function active() {
			if( this._active == '1' && this._category_id ){
				var menuCategory = LavuToGo.MenuCategory.getMenuCategoryByID( this._category_id );
				if( ! menuCategory ){
					return '1';
				} else {
					return menuCategory.active();
				}
			}
			return this._active;
		}
		function forced_modifier_group_id() {
			if( 'C' == this._forced_modifier_group_id ){
				var menuCategory = LavuToGo.MenuCategory.getMenuCategoryByID( this._category_id );
				if( ! menuCategory ){
					return '';
				} else {
					return menuCategory.forced_modifier_group_id();
				}
			}
			return this._forced_modifier_group_id;
		}
		function ltg_display() {
			if( ( this._ltg_display == '1' || this._ltg_display === '') && this._category_id ){
				var menuCategory = LavuToGo.MenuCategory.getMenuCategoryByID( this._category_id );
				if( ! menuCategory ){
					return '1';
				} else {
					return menuCategory.ltg_display();
				}
			}
			return this._ltg_display;
		}
		function modifier_list_id() {
			if( 'C' == this._modifier_list_id ){
				var menuCategory = LavuToGo.MenuCategory.getMenuCategoryByID( this._category_id );
				if( ! menuCategory ){
					return '';
				} else {
					return menuCategory.modifier_list_id();
				}
			}
			return this._modifier_list_id;
		}
		function printer() {
			if( ( this._printer == '0' || this._printer === '' ) && this._category_id ){
				var menuCategory = LavuToGo.MenuCategory.getMenuCategoryByID( this._category_id );
				if( ! menuCategory ){
					return '1';
				} else {
					return menuCategory.printer();
				}
			}
			return this._printer;
		}

		function tax_inclusion() {
			if( ( this._tax_inclusion == 'Default' || this._tax_inclusion === '' )&& this._category_id ){
				var menuCategory = LavuToGo.MenuCategory.getMenuCategoryByID( this._category_id );
				if( ! menuCategory ){
					return false;
				} else {
					return menuCategory.tax_inclusion();
				}
			}
			return this._tax_inclusion =='1';
		}

		function tax_profile_id() {
			if( this._tax_profile_id === '' && this._category_id ){
				var menuCategory = LavuToGo.MenuCategory.getMenuCategoryByID( this._category_id );
				if( ! menuCategory ){
					return '0';
				} else {
					return menuCategory.tax_profile_id();
				}
			}
			return this._tax_profile_id;
		}

		function createMenuItem( json ){
			var result = new MenuItem();
			for( var index in json ){
				if( result['_' + index] !== undefined ){
					result['_' + index] = json[index];
				}
			}

			if( result._price ){
				result._price = LavuToGo.NumberFormatter.createNumberFormatter().resolvePrecision( result._price );
			}

			var element =  LavuToGo.MenuItems.menuItems().getMenuItemByID( result.id() );
			if(!element){
				LavuToGo.MenuItems.menuItems().addItem( result );
				return result;
			}
			return element;
		}

		LavuToGo.MenuItem = MenuItem;
		LavuToGo.MenuItem.prototype.active = active;
		LavuToGo.MenuItem.prototype.allow_deposit = allow_deposit;
		LavuToGo.MenuItem.prototype.apply_taxrate = apply_taxrate;
		LavuToGo.MenuItem.prototype.category_id = category_id;
		LavuToGo.MenuItem.prototype.chain_reporting_group = chain_reporting_group;
		LavuToGo.MenuItem.prototype.constructor = MenuItem;
		LavuToGo.MenuItem.prototype.custom_taxrate = custom_taxrate;
		LavuToGo.MenuItem.prototype.description = description;
		LavuToGo.MenuItem.prototype.forced_modifier_group_id = forced_modifier_group_id;
		LavuToGo.MenuItem.prototype.happyhour = happyhour;
		LavuToGo.MenuItem.prototype.hidden_value = hidden_value;
		LavuToGo.MenuItem.prototype.hidden_value2 = hidden_value2;
		LavuToGo.MenuItem.prototype.hidden_value3 = hidden_value3;
		LavuToGo.MenuItem.prototype.id = id;
		LavuToGo.MenuItem.prototype.image = image;
		LavuToGo.MenuItem.prototype.image2 = image2;
		LavuToGo.MenuItem.prototype.image3 = image3;
		LavuToGo.MenuItem.prototype.ingredients = ingredients;
		LavuToGo.MenuItem.prototype.inv_count = inv_count;
		LavuToGo.MenuItem.prototype.last_modified_date = last_modified_date;
		LavuToGo.MenuItem.prototype.ltg_display = ltg_display;
		LavuToGo.MenuItem.prototype.menu_id = menu_id;
		LavuToGo.MenuItem.prototype.misc_content = misc_content;
		LavuToGo.MenuItem.prototype.modifier_list_id = modifier_list_id;
		LavuToGo.MenuItem.prototype.name = name;
		LavuToGo.MenuItem.prototype.open_item = open_item;
		LavuToGo.MenuItem.prototype.options1 = options1;
		LavuToGo.MenuItem.prototype.options2 = options2;
		LavuToGo.MenuItem.prototype.options3 = options3;
		LavuToGo.MenuItem.prototype.price = price;
		LavuToGo.MenuItem.prototype.price_tier_profile_id = price_tier_profile_id;
		LavuToGo.MenuItem.prototype.print = print;
		LavuToGo.MenuItem.prototype.printer = printer;
		LavuToGo.MenuItem.prototype.quick_item = quick_item;
		LavuToGo.MenuItem.prototype.show_in_app = show_in_app;
		LavuToGo.MenuItem.prototype.super_group_id = super_group_id;
		LavuToGo.MenuItem.prototype.tax_inclusion = tax_inclusion;
		LavuToGo.MenuItem.prototype.tax_profile_id = tax_profile_id;
		LavuToGo.MenuItem.prototype.track_86_count = track_86_count;
		LavuToGo.MenuItem.prototype.UPC = UPC;
		LavuToGo.MenuItem.createMenuItem = createMenuItem;
	})();

	(function(){
		function OptionalModifierList(){
			this._id = 0;
			this._options = [];
		}

		function id(){
			return this._id;
		}

		function options(){
			return this._options;
		}

		function addOption( optionalModifier ){
			if( !optionalModifier ){
				return;
			}

			if( !(optionalModifier instanceof LavuToGo.OptionalModifier)){
				return;
			}

			this._options.push( optionalModifier );
		}

		var _optional_modifier_list = [];
		function createOptionalModifierList( id ){
			var result = new OptionalModifierList();
			for(var i = 0; i < _optional_modifier_list.length; i++ ){
				if( _optional_modifier_list[i].id() == Number( id ) ){
					return _optional_modifier_list[i];
				}
			}
			result._id = id;
			_optional_modifier_list.push( result );
			return result;
		}

		LavuToGo.OptionalModifierList = OptionalModifierList;
		LavuToGo.OptionalModifierList.prototype.constructor = OptionalModifierList;
		LavuToGo.OptionalModifierList.prototype.id = id;
		LavuToGo.OptionalModifierList.prototype.options = options;
		LavuToGo.OptionalModifierList.prototype.addOption = addOption;
		LavuToGo.OptionalModifierList.createOptionalModifierList = createOptionalModifierList; //static
	})();

	(function(){
		function OptionalModifier(){
			this._id = 0;
			this._title = '';
			this._price = 0;
			this._optional_modifier_list_id = 0;
		}

		function id(){
			return this._id;
		}

		function title(){
			return this._title;
		}

		function price(){
			return this._price;
		}

		function optionalModifierList(){
			return this._optional_modifier_list_id;
		}

		var _optional_modifiers = [];
		function createOptionalModifier( id, title, price, optional_modifier_list_id ){
			var result = new OptionalModifier();
			for( var i = 0; i < _optional_modifiers.length; i++ ){
				if(_optional_modifiers[i].id() == Number( id ) ){
					return _optional_modifiers[i];
				}
			}
			result._id = Number( id );
			result._title = title;
			result._price = LavuToGo.NumberFormatter.createNumberFormatter().resolvePrecision( price );
			result._optional_modifier_list_id = optional_modifier_list_id;
			_optional_modifiers.push( result );
			return result;
		}

		LavuToGo.OptionalModifier = OptionalModifier;
		LavuToGo.OptionalModifier.prototype.constructor = OptionalModifier;
		LavuToGo.OptionalModifier.prototype.id = id;
		LavuToGo.OptionalModifier.prototype.title = title;
		LavuToGo.OptionalModifier.prototype.price = price;
		LavuToGo.OptionalModifier.prototype.optionalModifierList = optionalModifierList;
		LavuToGo.OptionalModifier.createOptionalModifier = createOptionalModifier; // static
	})();

	(function(){
		function ForcedModifier(){
			this._id = 0;
			this._name = '';
			this._type = '';
			this._options = [];
		}

		function options(){
			return this._options;
		}

		function addOption( option ){
			if(!option){
				return;
			}

			if( !(option instanceof LavuToGo.ForcedModifierOption ) ){
				return;
			}

			this._options.push( option );
		}

		function id(){
			return this._id;
		}

		function name(){
			return this._name;
		}

		function type(){
			return this._type;
		}

		var _forced_modifiers = [];
		function createForcedModifier( id, name, type ){
			var result = new ForcedModifier();
			result._id = id;
			result._name = name;
			result._type = type;
			for(var i = 0; i < _forced_modifiers.length; i++ ){
				if(_forced_modifiers[i].id() == id ){
					return _forced_modifiers[i];
				}
			}
			_forced_modifiers.push( result );
			return result;
		}

		LavuToGo.ForcedModifier = ForcedModifier;
		LavuToGo.ForcedModifier.prototype.constructor = ForcedModifier;
		LavuToGo.ForcedModifier.prototype.options = options;
		LavuToGo.ForcedModifier.prototype.addOption = addOption;
		LavuToGo.ForcedModifier.prototype.id = id;
		LavuToGo.ForcedModifier.prototype.name = name;
		LavuToGo.ForcedModifier.prototype.type = type;
		LavuToGo.ForcedModifier.createForcedModifier = createForcedModifier; //static
	})();

	(function(){
		function ForcedModifierOption(){
			this._id = 0;
			this._title = '';
			this._cost = 0;
			this._detour = 0;
			this._forced_modifier_id = 0;
		}

		function id(){
			return this._id;
		}

		function title(){
			return this._title;
		}

		function cost(){
			return this._cost;
		}

		function detour(){
			return this._detour;
		}

		function forcedModifierID(){
			return this._forced_modifier_id;
		}

		var _forced_modifier_options = [];
		function createForcedModifierOption( json, forced_modifier_id ){
			var result = new ForcedModifierOption();
			result._id = Number( json.id );
			result._title = json.title;
			result._cost = LavuToGo.NumberFormatter.createNumberFormatter().resolvePrecision( json.cost );
			result._detour = json.detour;
			result._forced_modifier_id = Number( forced_modifier_id );

			for(var i = 0; i < _forced_modifier_options.length; i++ ){
				if( _forced_modifier_options[i].id() == result.id() ){
					return _forced_modifier_options[i];
				}
			}
			_forced_modifier_options.push( result );
			return result;
		}

		LavuToGo.ForcedModifierOption = ForcedModifierOption;
		LavuToGo.ForcedModifierOption.prototype.constructor = ForcedModifierOption;
		LavuToGo.ForcedModifierOption.prototype.id = id;
		LavuToGo.ForcedModifierOption.prototype.title = title;
		LavuToGo.ForcedModifierOption.prototype.cost = cost;
		LavuToGo.ForcedModifierOption.prototype.detour = detour;
		LavuToGo.ForcedModifierOption.prototype.forcedModifierID = forcedModifierID;
		LavuToGo.ForcedModifierOption.createForcedModifierOption = createForcedModifierOption; //static
	})();

	(function(){
		function ShoppingCart(){
			this._entries = [];
		}

		function addEntry( cartEntry ){
			if( !cartEntry ){
				return;
			}

			if( !(cartEntry instanceof LavuToGo.CartEntry) ){
				return;
			}

			if( this._entries.indexOf( cartEntry ) === -1 ){
				this._entries.push( cartEntry );
			}
		}

		function removeEntry( cartEntry ){
			if( !cartEntry ){
				return;
			}

			if( !(cartEntry instanceof LavuToGo.CartEntry) ){
				return;
			}

			var index = this._entries.indexOf( cartEntry );
			if( index < 0 ){
				return;
			}
			cartEntry.setQuantity( 0 );
			this._entries.splice( index, 1 );
		}

		function clearCart(){
			this._entries = [];
		}

		function entries(){
			return this._entries;
		}

		var global_cart = null;
		function shoppingCart(){
			if(!global_cart){
				global_cart = new LavuToGo.ShoppingCart();
			}
			return global_cart;
		}

		function getEntryForItemRow( row ){
			if (this._entries[row] ){
				return this._entries[row];
			}
			return null;
		}

		function subtotal(){
			var result = 0;
			for ( var i = 0; i < this._entries.length; i++ ){
				result += this._entries[i].subtotal();
			}
			return result;
		}

		function total(){
			var subtotal = this.subtotal();
			var result = 0;
			for ( var i = 0; i < this._entries.length; i++ ){
				result += this._entries[i].total( subtotal );
			}
			return result;
		}

		function serialize(){
			var result = [];
			for(var i = 0; i < this._entries.length; i++ ){
				var entry = this._entries[i];
				var obj = {};
				obj.itemid = entry.item().id();
				obj.qty = entry.quantity();
				obj.fmods = [];
				var j = 0;
				var forcedModChoices = entry.forcedModifiers();
				for(j = 0; j < forcedModChoices.length; j++ ){
					obj.fmods.push( forcedModChoices[j].id() );
				}
				obj.omods = [];
				var optionalModChoices = entry.optionalModifiers();
				for(j = 0; j < optionalModChoices.length; j++ ){
					obj.omods.push( optionalModChoices[j].id() );
				}
				result.push( obj );
			}
			return JSON.stringify( result );
		}

		function unserialize( json ){
			if(!json){
				return;
			}
			this.clearCart();
			for( var i = 0; i < json.length; i++ ){
				var obj = json[i];
				var fmods_arr = [];
				var omods_arr = [];
				var entry = null;
				var j = 0;
				if( obj instanceof Array ){
					//old serialization
					//obj[0] = item_id
					//obj[1] = qty
					//obj[2][j][2] = option_id|array of option_ids (forced)
					////obj[2][j].id = option_id (optional)
					fmods_arr = [];
					for(j = 0; j < obj[2].length; j++ ){
						if( obj[2][j][2] instanceof Array ){
							for(var k = 0; k < obj[2][j][2].length; k++ ){
								fmods_arr.push( LavuToGo.ForcedModifierOption.createForcedModifierOption( { 'id' : obj[2][j][2][k] } ) );
							}
						} else {
							fmods_arr.push( LavuToGo.ForcedModifierOption.createForcedModifierOption( { 'id' : obj[2][j][2] } ) );
						}
					}
					omods_arr = [];
					for(j = 0; j < obj[3].length; j++ ){
						omods_arr.push( LavuToGo.OptionalModifier.createOptionalModifier( obj[3][j].id ) );
					}
					entry = LavuToGo.CartEntry.createCartEntry( obj[0], fmods_arr, omods_arr );
					entry.setQuantity( Number( obj[1] ) );
					Shopping_Cart.addEntry( entry );
				} else if( obj instanceof Object ){
					//new serialization
					//obj.itemid = item_id
					//obj.qty = quantity
					//obj.fmods[j] = option_id (forced)
					//obj.omods[j] = option_id (optional)
					fmods_arr = [];
					for(j = 0; j < obj.fmods.length; j++ ){
						fmods_arr.push( LavuToGo.ForcedModifierOption.createForcedModifierOption( { 'id' : obj.fmods[j] } ) );
					}
					omods_arr = [];
					for(j = 0; j < obj.omods.length; j++ ){
						omods_arr.push( LavuToGo.OptionalModifier.createOptionalModifier( obj.omods[j] ) );
					}
					entry = LavuToGo.CartEntry.createCartEntry( obj.itemid, fmods_arr, omods_arr );
					entry.setQuantity( Number( obj.qty ) );
					Shopping_Cart.addEntry( entry );
				} else {
					continue;
				}
			}
		}

		function toLavuString( join_str ){
			if(!join_str){
				join_str = '|*|';
			}
			var result = [];
			for( var i = 0; i < this._entries.length; i++ ){
				result.push( this._entries[i].toLavuString( '|o|' ) );
			}
			return result.join( join_str );
		}

		LavuToGo.ShoppingCart = ShoppingCart;
		LavuToGo.ShoppingCart.prototype.constructor = ShoppingCart;
		LavuToGo.ShoppingCart.prototype.addEntry = addEntry;
		LavuToGo.ShoppingCart.prototype.removeEntry = removeEntry;
		LavuToGo.ShoppingCart.prototype.clearCart = clearCart;
		LavuToGo.ShoppingCart.prototype.entries = entries;
		LavuToGo.ShoppingCart.prototype.getEntryForItemRow = getEntryForItemRow;
		LavuToGo.ShoppingCart.prototype.subtotal = subtotal;
		LavuToGo.ShoppingCart.prototype.total = total;
		LavuToGo.ShoppingCart.prototype.serialize = serialize;
		LavuToGo.ShoppingCart.prototype.unserialize = unserialize;
		LavuToGo.ShoppingCart.prototype.toLavuString = toLavuString;
		LavuToGo.ShoppingCart.shoppingCart = shoppingCart; //static
	})();

	(function(){
		function CartEntry(){
			this._item_id = 0;
			this._item = null;
			this._quantity = 0;
			this._forced_mods = [];
			this._optional_mods = [];
		}

		var _cart_entries = [];
		function createCartEntry( item_id, modifiers, so_modifiers ){
			if( isNaN(Number(item_id))){
				return null;
			}

			if( !(modifiers instanceof Array) ){
				return null;
			}

			if( !(so_modifiers instanceof Array) ){
				return null;
			}
			var i = 0;
			for( i = 0; i < _cart_entries.length; i++ ){
				var match = false;
				if( _cart_entries[i]._item_id == item_id ){
					//possible match
					match = true;
					var entry = _cart_entries[i];
					var entry_forced_modifiers = entry.forcedModifiers();
					match = match && entry_forced_modifiers.length === modifiers.length;
					var j = 0;
					for( j = 0; j < modifiers.length; j++ ){
						match = match && entry_forced_modifiers.indexOf( modifiers[j] ) !== -1;
					}
					var entry_optional_modifiers = entry.optionalModifiers();
					match = match && entry_optional_modifiers.length === so_modifiers.length;
					for( j = 0; j < so_modifiers.length; j++ ){
						match = match && entry_optional_modifiers.indexOf( so_modifiers[j] ) !== -1;
					}
				}
				//Inserted By Brian D.
				if(ltg_menu_items_that_use_pizza_creator[item_id] != 'undefined' && ltg_menu_items_that_use_pizza_creator[item_id] > 0){
					match = false; //If it is a pizza creator item they may set the quantity manually, but do to ad-hoc forced mods we list all of them
				}
				//End
				if( match ){
					return _cart_entries[i];
				}
			}

			var result = new CartEntry();
			result._item = LavuToGo.MenuItems.menuItems().getMenuItemByID( item_id );
			result._item_id = Number(item_id);
			for( i = 0; i < modifiers.length; i++ ){
				result.addForcedModifier( modifiers[i] );
			}
			for( i = 0; i < so_modifiers.length; i++ ){
				result.addOptionalModifier( so_modifiers[i] );
			}
			//result._quantity = Number(quantity);
			_cart_entries.push( result );

			return result;
		}

		function setQuantity( quantity ){
			if( ! isNaN( Number( quantity ) ) ) {
				this._quantity = Number( quantity );
			}
		}

		function item(){
			return this._item;
		}

		function quantity(){
			return this._quantity;
		}

		function addForcedModifier( modifier ){
			if( !modifier ){
				return;
			}

			if( !(modifier instanceof LavuToGo.ForcedModifierOption ) ){
				return;
			}

			this._forced_mods.push( modifier );
		}

		function forcedModifiers(){
			return this._forced_mods;
		}

		function addOptionalModifier( modifier ){
			if( !modifier ){
				return;
			}

			if( !(modifier instanceof LavuToGo.OptionalModifier) ){
				return;
			}

			this._optional_mods.push( modifier );
		}

		function optionalModifiers(){
			return this._optional_mods;
		}

		function preQuantityTotal(){
			result = 0;
			var item = this.item();
			var basePrice = Number( item.price() );

			var forcedModifierPrice = 0;
			var j = 0;
			for( j = 0; j < this.forcedModifiers().length; j++ ){
				forcedModifierPrice += Number( this.forcedModifiers()[j].cost() );
			}
			var optionalModifierPrice = 0;
			for( j = 0; j < this.optionalModifiers().length; j++ ){
				optionalModifierPrice += Number( this.optionalModifiers()[j].price() );
			}
			result += basePrice + forcedModifierPrice + optionalModifierPrice;
			return result;
		}

		function subtotal(){
			return this.preQuantityTotal() * this._quantity;
		}

		function total( subtotal ){
			var tax_profile_id = this.item().tax_profile_id();
			var taxProfile = LavuToGo.TaxProfile.createTaxProfile( tax_profile_id ); //get tax profile;
			var pre_tax_amount = this.preQuantityTotal() * this._quantity;
			if( this.item().tax_inclusion() ){
				taxProfile.calculateIncludedTax( this.preQuantityTotal() * this._quantity, this.item().custom_taxrate(), subtotal );
				return pre_tax_amount;
			}
			var tax_amount = Math.round( taxProfile.calculateTax( this.preQuantityTotal() * this._quantity, this.item().custom_taxrate(), subtotal ) );
			return pre_tax_amount + tax_amount;
		}

		function toLavuString( join_str ) {
			if( !join_str ){
				join_str = '|o|';
			}
			var i = 0;
			var result = [];

			var numFormatter = LavuToGo.NumberFormatter.createNumberFormatter();
			var special = '';
			var omods = this.optionalModifiers();
			var omod_price = 0;
			for( i = 0; i < omods.length; i++ ){
				omod_price += omods[i].price();
				if( special ){
					special += ', ';
				}
				special += omods[i].title();
			}
			var options = '';
			var fmods = this.forcedModifiers();
			var fmod_price = 0;
			for( i = 0; i < fmods.length; i++ ){
				fmod_price += fmods[i].cost();
				if( options ){
					options += ' - ';
				}
				options += fmods[i].title();
			}

			var tax_profile = LavuToGo.TaxProfile.createTaxProfile( this.item().tax_profile_id() );
			if(!tax_profile){
				return '';
			}
			var tax_details =  null;
			if( this.item().tax_inclusion() ){
				tax_details = tax_profile.getIncludedTaxRates( this.subtotal(), this.item().custom_taxrate() );
			} else {
				tax_details = tax_profile.getTaxRates( this.subtotal(), this.item().custom_taxrate() );
			}
			var tax_amount = 0;
			for( i = 0; i < tax_details.length; i++ ){
				tax_amount += tax_details[i].amount;
			}

			result.push( this.item().name() );							//  0 - item
			result.push( numFormatter.undoPrecision( this.item().price() ) );//  1 - price
			result.push( this.quantity() );								//  2 - quantity
			result.push( options );										//  3 - options
			result.push( special );										//  4 - special
			result.push( this.item().print() );							//  5 - print
			result.push( numFormatter.undoPrecision( omod_price ) );	//  6 - modify price
			result.push( 1 );											//  7 - check number
			result.push( 1 );											//  8 - seat number
			result.push( this.item().id() );							//  9 - item id
			result.push( this.item().printer() );						// 10 - printer
			//result.push( this.item().apply_taxrate() );
			var apply_taxrates = [];
			for(i = 0; i < tax_details.length; i++ ){
				//id::name::rate
				var rate_arr = [];
				rate_arr.push( tax_details[i].id );
				rate_arr.push( tax_details[i].name );
				rate_arr.push( tax_details[i].calc );
				rate_arr.push( tax_details[i].stack );
				rate_arr.push( tax_details[i].tierType1 );
				rate_arr.push( tax_details[i].tierValue1 );
				rate_arr.push( tax_details[i].calc2 );
				apply_taxrates.push( rate_arr.join('::') );
			}
			result.push( apply_taxrates.join(';;') );					// 11 - apply taxrate
			result.push( Number(this.item().custom_taxrate())===0?'':this.item().custom_taxrate());// 12 - custom taxrate
			result.push( this.item().modifier_list_id() );				// 13 - modifier list id
			result.push( this.item().forced_modifier_group_id() );		// 14 - forced modifier group id
			result.push( numFormatter.undoPrecision( fmod_price ) );	// 15 - forced modifiers price
			result.push( 1 );											// 16 - course number
			result.push( 0 );											// 17 - open item
			result.push( 0 );											// 18 - allow deposit
			result.push( '' );											// 19 - deposit info
			result.push( 0 );											// 20 - void flag
			var d = new Date();
			result.push( d.toISOString().replace(/T/,' ').substr(0,19) );// 21 - device time
			result.push( '' );											// 22 - notes
			result.push( '' );											// 23 - hidden data 1
			result.push( '' );											// 24 - hidden data 2
			result.push( '' );											// 25 - hidden data 3
			result.push( '' );											// 26 - hidden data 4
			result.push( this.item().tax_inclusion()?'1':'0' );			// 27 - tax inclusion
			result.push( 0 );											// 28 - sent
			result.push( 0 );											// 29 - tax exempt
			result.push( '' );											// 30 - exemption id
			result.push( '' );											// 31 - exemption name
			result.push( 0 );											// 32 - checked out
			result.push( '' );											// 33 - hidden data 5
			result.push( 0 );											// 34 - price override
			result.push( '' );											// 35 - original price
			result.push( '' );											// 36 - override id
			result.push( '' );											// 37 - idiscount id
			result.push( '' );											// 38 - idiscount sh
			result.push( '' );											// 39 - idiscount value
			result.push( '' );											// 40 - idiscount type
			result.push( '' );											// 41 - idiscount amount
			result.push( '' );											// 42 - hidden data 6
			result.push( '' );											// 43 - idiscount info
			result.push( this.item().category_id() );					// 44 - category id
			result.push( '' );											// 45 - internal order id (ioid)
			result.push( '' );											// 46 - internal contents id (icid)
			// ---------- 2.2.1+ ----------
			result.push( '' );											// 47 - discount_amount
			result.push( '' );											// 48 - discount_value
			result.push( '' );											// 49 - discount_type
			result.push( numFormatter.undoPrecision( this.subtotal() ) );// 50 - after_discount
			result.push( numFormatter.undoPrecision( tax_amount ) );	// 51 - tax_amount
			result.push( numFormatter.undoPrecision( this.subtotal() + tax_amount ) );				// 52 - total_with_tax
			result.push( 0 );											// 53 - itax_rate
			result.push( 0 );											// 54 - itax
			result.push( (tax_details[0])?tax_details[0].rate:0 );		// 55 - tax_rate1
			result.push( (tax_details[0])?numFormatter.undoPrecision( tax_details[0].amount ):0 );	// 56 - tax1
			result.push( (tax_details[1])?tax_details[1].rate:0 );		// 57 - tax_rate2
			result.push( (tax_details[1])?numFormatter.undoPrecision( tax_details[1].amount ):0 );	// 58 - tax2
			result.push( (tax_details[2])?tax_details[2].rate:0 );		// 59 - tax_rate3
			result.push( (tax_details[2])?numFormatter.undoPrecision( tax_details[2].amount ):0 );	// 60 - tax3
			result.push( (tax_details[0])?numFormatter.undoPrecision( tax_details[0].subtotal ):0 );	// 61 - tax_subtotal1
			result.push( (tax_details[1])?numFormatter.undoPrecision( tax_details[1].subtotal ):0 );	// 62 - tax_subtotal2
			result.push( (tax_details[2])?numFormatter.undoPrecision( tax_details[2].subtotal ):0 );	// 63 - tax_subtotal3
			result.push( '' );											// 64 - discount_id
			result.push( 1 );											// 65 - split_factor
			result.push( (tax_details[0])?tax_details[0].name:0 );		// 66 - tax_name1
			result.push( (tax_details[1])?tax_details[1].name:0 );		// 67 - tax_name2
			result.push( (tax_details[2])?tax_details[2].name:0 );		// 68 - tax_name3
			result.push( '' );											// 69 - itax_name
			result.push( numFormatter.undoPrecision( this.subtotal() ) );// 70 - after_gratuity
			// ---------- 2.3.5+ ----------
			result.push( (tax_details[3])?tax_details[3].rate:0 );		// 71 - tax_rate4
			result.push( (tax_details[3])?numFormatter.undoPrecision( tax_details[3].amount ):0 );	// 72 - tax4
			result.push( (tax_details[3])?numFormatter.undoPrecision( tax_details[3].subtotal ):0 );	// 73 - tax_subtotal4
			result.push( (tax_details[3])?tax_details[3].name:0 );		// 74 - tax_name4
			result.push( (tax_details[4])?tax_details[4].rate:0 );		// 75 - tax_rate5
			result.push( (tax_details[4])?numFormatter.undoPrecision( tax_details[4].amount ):0 );	// 76 - tax5
			result.push( (tax_details[4])?numFormatter.undoPrecision( tax_details[4].subtotal ):0 );	// 77 - tax_subtotal5
			result.push( (tax_details[4])?tax_details[4].name:0 );		// 78 - tax_name5

			return result.join( join_str );
		}

		LavuToGo.CartEntry = CartEntry;
		LavuToGo.CartEntry.prototype.constructor = CartEntry;
		LavuToGo.CartEntry.prototype = {};
		LavuToGo.CartEntry.prototype.setQuantity = setQuantity;
		LavuToGo.CartEntry.prototype.item = item;
		LavuToGo.CartEntry.prototype.quantity = quantity;
		LavuToGo.CartEntry.prototype.addForcedModifier = addForcedModifier;
		LavuToGo.CartEntry.prototype.forcedModifiers = forcedModifiers;
		LavuToGo.CartEntry.prototype.addOptionalModifier = addOptionalModifier;
		LavuToGo.CartEntry.prototype.optionalModifiers = optionalModifiers;
		LavuToGo.CartEntry.prototype.preQuantityTotal = preQuantityTotal;
		LavuToGo.CartEntry.prototype.subtotal = subtotal;
		LavuToGo.CartEntry.prototype.total = total;
		LavuToGo.CartEntry.prototype.toLavuString = toLavuString;
		LavuToGo.CartEntry.createCartEntry = createCartEntry; //static
	})();

	(function(){
		function TaxBreakdown(){
			this._tax_name = '';
			this._tax_rate = 0;
			this._considered_amount = 0;
			this._tax_total = 0;
		}

		function taxName(){
			return this._tax_name;
		}

		function taxRate(){
			return this._tax_rate;
		}

		function taxTotal(){
			return this._tax_total;
		}

		function amount(){
			return this._considered_amount;
		}

		function addAmount( amount ){
			if( isNaN( Number( amount ) ) ){
				return;
			}
			this._considered_amount += Number( amount );
			this._tax_total = Math.round(this._tax_rate * this._considered_amount);
		}

		var _tax_breakdowns = [];
		var _tax_breakdowns_included = [];

		function createTaxBreakdown( taxRate, taxName, amount ){
			var breakdown = new TaxBreakdown();
			breakdown._tax_rate = taxRate;
			breakdown._tax_name = taxName;
			for(var i = 0; i < _tax_breakdowns.length; i++ ){
				if( _tax_breakdowns[i].taxRate() === taxRate && _tax_breakdowns[i].taxName() == taxName ){
					breakdown = _tax_breakdowns[i];
					break;
				}
			}
			if( _tax_breakdowns.indexOf( breakdown ) === -1 ){
				_tax_breakdowns.push( breakdown );
			}
			breakdown.addAmount( amount );
		}

		function resetBreakdowns(){
			_tax_breakdowns = [];
		}

		function getBreakdowns(){
			return _tax_breakdowns;
		}

		function createTaxBreakdownIncluded( taxRate, taxName, amount ){
			var breakdown = new TaxBreakdown();
			breakdown._tax_rate = taxRate;
			breakdown._tax_name = taxName;
			for(var i = 0; i < _tax_breakdowns_included.length; i++ ){
				if( _tax_breakdowns_included[i].taxRate() === taxRate && _tax_breakdowns_included[i].taxName() == taxName ){
					breakdown = _tax_breakdowns_included[i];
					break;
				}
			}
			if( _tax_breakdowns_included.indexOf( breakdown ) === -1 ){
				_tax_breakdowns_included.push( breakdown );
			}
			breakdown.addAmount( amount );
		}

		function resetBreakdownsIncluded(){
			_tax_breakdowns_included = [];
		}

		function getBreakdownsIncluded(){
			return _tax_breakdowns_included;
		}

		LavuToGo.TaxBreakdown = TaxBreakdown;
		LavuToGo.TaxBreakdown.prototype.constructor = TaxBreakdown;
		LavuToGo.TaxBreakdown.prototype.taxName = taxName;
		LavuToGo.TaxBreakdown.prototype.taxRate = taxRate;
		LavuToGo.TaxBreakdown.prototype.taxTotal = taxTotal;
		LavuToGo.TaxBreakdown.prototype.amount = amount;
		LavuToGo.TaxBreakdown.prototype.addAmount = addAmount;
		LavuToGo.TaxBreakdown.createTaxBreakdownIncluded = createTaxBreakdownIncluded;
		LavuToGo.TaxBreakdown.createTaxBreakdown = createTaxBreakdown;
		LavuToGo.TaxBreakdown.getBreakdowns = getBreakdowns;
		LavuToGo.TaxBreakdown.getBreakdownsIncluded = getBreakdownsIncluded;
		LavuToGo.TaxBreakdown.resetBreakdowns = resetBreakdowns;
		LavuToGo.TaxBreakdown.resetBreakdownsIncluded = resetBreakdownsIncluded;
	})();

	(function(){
		function TaxProfile(){
			this._id = 0;
			this._tax_profile_rates = [];
		}

		function id(){
			return this._id;
		}

		function taxProfileRates(){
			return this._tax_profile_rates;
		}

		function addTaxProfileRate( taxProfileRate ){
			if( !taxProfileRate ){
				return;
			}

			if( !(taxProfileRate instanceof LavuToGo.TaxProfileRate ) ){
				return;
			}

			this._tax_profile_rates.push( taxProfileRate );
		}

		function getIncludedTaxRates( amount, overwrite ){
			return this._calculateTax( amount, true, true, overwrite, amount, LavuToGo.TaxBreakdown.createTaxBreakdownIncluded );
		}

		function calculateIncludedTax( amount, overwrite, subtotal ){
			return this._calculateTax( amount, true, false, overwrite, subtotal, LavuToGo.TaxBreakdown.createTaxBreakdownIncluded );
		}

		function getTaxRates( amount, overwrite ){
			return this._calculateTax( amount, false, true, overwrite, amount );
		}

		function calculateTax( amount, overwrite, subtotal ){
			return this._calculateTax( amount, false, false, overwrite, subtotal );
		}

		function _calculateTax( amount, taxIncluded, returnRates, overwrite1, subtotal, func ){
			if(!func){
				func = LavuToGo.TaxBreakdown.createTaxBreakdown;
			}
			var ending_rate = 0;
			var rates_arr = [];
			for( var i = 0; i < this._tax_profile_rates.length; i++ ){
				var tax_profile_rate = this._tax_profile_rates[i];
				var considered_rate = tax_profile_rate.calc();

				var tier_value1 = LavuToGo.NumberFormatter.createNumberFormatter().resolvePrecision( tax_profile_rate.tier_value1() );
				if( tax_profile_rate.tier_type1() && subtotal >= tier_value1 ){ //change rate based on subtotal
					considered_rate = tax_profile_rate.calc2();
				}

				if( tax_profile_rate.stack() && i !== 0){
					if( returnRates ){
						var tmp = {};
						rates_arr.push( {
							'id' : this.id(),
							'rate' : considered_rate,
							'amount' : Math.round(amount * (((1+ending_rate)*(1+considered_rate))-1)),
							'subtotal' : amount * (1+ending_rate),
							'name' : tax_profile_rate.title(),
							'calc' : tax_profile_rate.calc(),
							'type' : tax_profile_rate.type(),
							'stack' : tax_profile_rate.stack()?'1':'0',
							'tierType1' : tax_profile_rate.tier_type1()?'1':'0',
							'tierValue1' : tax_profile_rate.tier_value1(),
							'calc2' : tax_profile_rate.calc2()
						} );
					} else if( !taxIncluded ) {
						func( considered_rate, tax_profile_rate.title(), Math.round((1 + ending_rate) * amount) );
					}
					ending_rate = ((1 + ending_rate) * (1 + considered_rate)) - 1;
				} else {
					if( i === 0 && overwrite1 !== undefined && overwrite1 !== 0 ){
						considered_rate = overwrite1;
					}
					if( returnRates ){
						rates_arr.push( {
							'id' : this.id(),
							'rate' : considered_rate,
							'amount' : Math.round(amount * considered_rate),
							'subtotal' : amount,
							'name' : tax_profile_rate.title(),
							'calc' : tax_profile_rate.calc(),
							'type' : tax_profile_rate.type(),
							'stack' : tax_profile_rate.stack()?'1':'0',
							'tierType1' : tax_profile_rate.tier_type1()?'1':'0',
							'tierValue1' : tax_profile_rate.tier_value1(),
							'calc2' : tax_profile_rate.calc2()
						} );
					} else if( !taxIncluded ) {
						func( considered_rate, tax_profile_rate.title(), amount );
					}
					ending_rate += considered_rate;
				}
			}

			if( taxIncluded ){
				var pre_tax_amount = Math.round( amount / ( 1 + ending_rate ) );
				return this._calculateTax( pre_tax_amount, false, returnRates, overwrite1,pre_tax_amount, func );
			}

			if( returnRates ){
				return rates_arr;
			}
			return amount * ending_rate;
		}

		var _tax_profiles = [];
		function createTaxProfile( id ){
			var result = new TaxProfile();
			for( var i = 0; i < _tax_profiles.length; i++ ){
				if( _tax_profiles[i].id() == id ){
					return _tax_profiles[i];
				}
			}
			result._id = id;
			_tax_profiles.push( result );
			return result;
		}

		LavuToGo.TaxProfile = TaxProfile;
		LavuToGo.TaxProfile.prototype.constructor = TaxProfile;
		LavuToGo.TaxProfile.prototype.id = id;
		LavuToGo.TaxProfile.prototype.taxProfileRates = taxProfileRates;
		LavuToGo.TaxProfile.prototype.addTaxProfileRate = addTaxProfileRate;
		LavuToGo.TaxProfile.prototype.getIncludedTaxRates = getIncludedTaxRates;
		LavuToGo.TaxProfile.prototype.calculateIncludedTax = calculateIncludedTax;
		LavuToGo.TaxProfile.prototype.getTaxRates = getTaxRates;
		LavuToGo.TaxProfile.prototype.calculateTax = calculateTax;
		LavuToGo.TaxProfile.prototype._calculateTax = _calculateTax;
		LavuToGo.TaxProfile.createTaxProfile = createTaxProfile; // static
	})();

	(function(){
		function TaxProfileRate(){
			this._id = 0;
			this._loc_id = 0;
			this._title = '';
			this._calc = 0;
			this._type = '';
			this._stack = 0;
			this._tier_type1 = '';
			this._tier_value1 = 0;
			this._force_tier1 = false;
			this._calc2 = 0;
			this._force_apply = false;
			this._profile_id = 0;
		}

		function id(){ return this._id; }
		function loc_id(){ return this._loc_id; }
		function title(){ return this._title; }
		function calc(){ return this._calc; }
		function type(){ return this._type; }
		function stack(){ return this._stack; }
		function tier_type1(){ return this._tier_type1; }
		function tier_value1(){ return this._tier_value1; }
		function force_tier1(){ return this._force_tier1; }
		function calc2(){ return this._calc2; }
		function force_apply(){ return this._force_apply; }
		function profile_id(){ return this._profile_id; }

		var _tax_profile_rates = [];
		function createTaxProfileRate( json ){
			var result = new TaxProfileRate();
			for( var i = 0; i < _tax_profile_rates.length; i++ ){
				if( _tax_profile_rates[i].id() == json.id ){
					return _tax_profile_rates[i];
				}
			}
			result._id = json.id;
			result._loc_id = Number( json.loc_id );
			result._title = json.title;
			result._calc = Number( json.calc );
			result._type = json.type;
			result._stack = Number(json.stack) === 1;
			result._tier_type1 = json.tier_type1 =='subtotal';
			result._tier_value1 = Number( json.tier_value1 );
			result._force_tier1 = json.force_tier1;
			result._calc2 = Number( json.calc2 );
			result._force_apply = Number(json.force_apply) === 1;
			result._profile_id = Number( json.profile_id );

			_tax_profile_rates.push( result );
			return result;
		}

		LavuToGo.TaxProfileRate = TaxProfileRate;
		LavuToGo.TaxProfileRate.prototype.constructor = TaxProfileRate;
		LavuToGo.TaxProfileRate.prototype.id = id;
		LavuToGo.TaxProfileRate.prototype.loc_id = loc_id;
		LavuToGo.TaxProfileRate.prototype.title = title;
		LavuToGo.TaxProfileRate.prototype.calc = calc;
		LavuToGo.TaxProfileRate.prototype.type = type;
		LavuToGo.TaxProfileRate.prototype.stack = stack;
		LavuToGo.TaxProfileRate.prototype.tier_type1 = tier_type1;
		LavuToGo.TaxProfileRate.prototype.tier_value1 = tier_value1;
		LavuToGo.TaxProfileRate.prototype.force_tier1 = force_tier1;
		LavuToGo.TaxProfileRate.prototype.calc2 = calc2;
		LavuToGo.TaxProfileRate.prototype.force_apply = force_apply;
		LavuToGo.TaxProfileRate.prototype.profile_id = profile_id;
		LavuToGo.TaxProfileRate.createTaxProfileRate = createTaxProfileRate; //static
	})();

	(function(){
		function NumberFormatter(){
			this._precision = 0;
			this._monetary_symbol = '$';
			this._left_or_right = 'left';
			this._thousands_char = ',';
			this._decimal_char = '.';

			this._pre_computed_power = 0;
		}

		function resolvePrecision( floating_point ){
			var result = Math.round(floating_point * this._pre_computed_power);
			return result;
		}

		function undoPrecision( integer ){
			var result = integer / this._pre_computed_power;
			return result;
		}

		function toFormattedString( integer ){
			var result = '';

			var number = integer/this._pre_computed_power;
			var number_str = number+'';
			matches = (number_str).match( /([\d]+)\D([\d]+)?/ );

			var pre_decimal;
			var post_decimal;
			if( matches ){
				pre_decimal = matches[1]; //before decimal
				post_decimal = matches[2]; //after decimal, may not be present
			} else {
				pre_decimal = number_str;
				post_decimal = new Array( this._precision + 1 ).join( "0" );
			}

			post_decimal += new Array( (this._precision - post_decimal.length) + 1 ).join("0");


			var progress = pre_decimal.length%3;
			if( progress !== 0 ){
				result += number_str.substr(0, progress );
				if(progress != pre_decimal.length){
					result += this._thousands_char;
				}
			}
			while( progress < pre_decimal.length ){
				result += number_str.substr( progress, 3 );
				if( progress + 3 < pre_decimal.length ){
					result += this._thousands_char;
				}
				progress += 3;
			}
			result += this._decimal_char + post_decimal;

			return result;
		}

		function toMonitaryString( integer ){
			var result = this.toFormattedString( integer );

			if( this._left_or_right == 'left' ){
				result = this._monetary_symbol + result;
			} else {
				result += this._monetary_symbol;
			}
			return result;
		}
		var _global_number_formatter = null;
		function createNumberFormatter( precision, monetary_symbol, left_or_right, decimal_char ){
			if(!_global_number_formatter){
				_global_number_formatter = new NumberFormatter();
				_global_number_formatter._precision = Number( precision );
				_global_number_formatter._pre_computed_power = Math.pow( 10, precision );
				_global_number_formatter._monetary_symbol = monetary_symbol;
				_global_number_formatter._left_or_right = left_or_right;
				if( decimal_char != '.' ){
					_global_number_formatter._thousands_char = '.';
				}
				_global_number_formatter._decimal_char = decimal_char;

			}
			return _global_number_formatter;
		}

		LavuToGo.NumberFormatter = NumberFormatter;
		LavuToGo.NumberFormatter.prototype.constructor = NumberFormatter;
		LavuToGo.NumberFormatter.prototype.resolvePrecision = resolvePrecision;
		LavuToGo.NumberFormatter.prototype.undoPrecision = undoPrecision;
		LavuToGo.NumberFormatter.prototype.toFormattedString = toFormattedString;
		LavuToGo.NumberFormatter.prototype.toMonitaryString = toMonitaryString;
		LavuToGo.NumberFormatter.createNumberFormatter = createNumberFormatter; //static
	})();

	(function(){
		function Consumer(){
			this._email = null;
			this._phone = null;
			this._first_name = null;
			this._last_name = null;
		}

		function email(){
			return this._email;
		}

		function phone(){
			return this._phone;
		}

		function firstName(){
			return this._first_name;
		}

		function lastName(){
			return this._last_name;
		}

		var _consumer = null;
		function createConsumer( email, phone, firstName, lastName ){
			var result = new Consumer();
			_consumer = result;
			result._email = email;
			result._phone = phone;
			result._first_name = firstName;
			result._last_name = lastName;
			return result;
		}

		function guestForm(){
			var str = '';
			str += 'First Name: <input type="text" name="first_name" id="first_name" placeholder="First Name" /><br />';
			str += 'Last Name: <input type="text" name="last_name" id ="last_name" placeholder="Last Name" /><br />';
			str += 'Email: <input type="email" name="email" id="email" placeholder="Email" /><br />';
			str += 'Phone: <input type="phone" name="phone" id="phone" placeholder="Phone" /><br />';
			return str;
		}

		function consumer(){
			return _consumer;
		}

		LavuToGo.Consumer = Consumer;
		LavuToGo.Consumer.prototype.constructor = Consumer;
		LavuToGo.Consumer.prototype.email = email;
		LavuToGo.Consumer.prototype.phone = phone;
		LavuToGo.Consumer.prototype.firstName = firstName;
		LavuToGo.Consumer.prototype.lastName = lastName;
		LavuToGo.Consumer.createConsumer = createConsumer;
		LavuToGo.Consumer.consumer = consumer;
		LavuToGo.Consumer.guestForm = guestForm;
	})();

	(function(){

		function DeliveryOption(){ }
		DeliveryOption.prototype.constructor = DeliveryOption;

		function Delivery(){ }
		Delivery.prototype.constructor = Delivery;
		Delivery.prototype = new DeliveryOption();
		LavuToGo.Delivery = new Delivery();

		function PickUp(){ }
		PickUp.prototype.constructor = PickUp;
		PickUp.prototype = new DeliveryOption();
		LavuToGo.PickUp = new PickUp();

		function CheckoutWhereOptions(){

		}
	})();

	(function(){
		function CheckoutOptions(){
			this._who = null;
			this._what = null;
			this._where = null;
			this._when = null;
			this._how = null;
		}

	})();
})();