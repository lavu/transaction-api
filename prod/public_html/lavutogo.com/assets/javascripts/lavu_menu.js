/***
 * Menu Class
 * 
 **/
var Menu = function(obj){
	LavuObject.call(this, obj);
	var private = {
		_id                : 0,
		_title             : "",
		_created_by        : "",
		_modifications     : "",
		_menu_categories   : [],
		_menu_groups       : [],
		_menu_items        : []
	};
	
	for(name in private){
		if(obj && obj[name.substr(1)]){
			private[name] = obj[name.substr(1)];
		}
	}
	
	this.getID             = function() { return private._id; }
	this.getTitle          = function() { return private._title; }
	this.getCreatedBy      = function() { return private._created_by; }
	this.getModifiecations = function() { return private._modifications; }
	
	this.addMenuCategory = function(menu_category){
		if(!(menu_category instanceof MenuCategory)){
			return false;
		}
		
		private.menu_categories[ menu_category.getID() ] = menu_category;
		return true;
	}
	
	this.getMenuCategory = function( category_id ){
		return private.menu_categories[ category_id ];
	}
	
	this.addMenuGroup = function(menu_group){
		if(!(menu_group instanceof MenuGroup)){
			return false;
		}
		
		private.menu_groups[ menu_group.getID() ] = menu_group;
		return true;
	}
	
	this.getMenuGroup = function( group_id ){
		return private.menu_groups[ group_id ];
	}
	
	this.addMenuItem = function( menu_item ){
		if(!(menu_item instanceof MenuItem)){
			return false;
		}
		
		private.menu_items[ menu_item.getID() ] = menu_item;
		return true;
	}
	
	this.getMenuItem = function( item_id ){
		return private.menu_items[ item_id ];
	}
}
Menu.prototype.constructor = Menu;
Menu.prototype = new LavuObject();


/***
 * MenuGroup Class
 *
 * 
 **/
var MenuGroup = function(obj){
	Orderable.call(this); 
	var private = {
		_id        : 0,
		_menu_id   : 0,
		_group_name      : "",
		_orderby   : "",
		_menu 	  : null
	};
	
	for(name in private){
		if(obj && obj[name.substr(1)]){
			private[name] = obj[name.substr(1)];
		}
	}
	 
	this.getID         = function() { return private._id; }
	this.getMenuID     = function() { return private._menu_id; }
	this.getName       = function() { return private._group_name; }
	this.getOrderBy    = function() { return private._orderby; }
	
	this.setMenu = function( newmenu ){
		if(!(newmenu instanceof Menu)){
			return false;
		}
		
		private.menu = newmenu;
		return true;
	}
	
	this.getMenu = function(){
		return private.menu;
	}
}
MenuGroup.prototype.constructor = MenuGroup;
MenuGroup.prototype = new Orderable();


/***
 *	MenuCategory Class
 **/
var MenuCategory = function(obj){
	Orderable.call(this, obj);
	var private = {
		_id                        : 0,
		_menu_id                   : 0,
		_menu 					   : null,
		_group_id                  : 0,
		_menu_group				   : null,
		_name                      : "",
		_image                     : "",
		_description               : "",
		_active                    : "",
		_print                     : "",
		_last_modified_date        : "",
		_printer                   : "",
		_modifier_list_id          : 0,
		_modifier_list			   : null,
		_apply_taxrate             : "",
		_custom_taxrate            : 0,
		_forced_modifier_group_id  : 0,
		_forced_modifier_group	   : null,
		_print_order               : "",
		_super_group_id            : 0,
		_super_group			   : null,
		_tax_inclusion             : "",
		_ltg_display               : "",
		_happyhour                 : "",
		_tax_profile_id            : 0,
		_tax_prifle				   : null
	}
	
	for(name in private){
		if(obj && obj[name.substr(1)]){
			private[name] = obj[name.substr(1)];
		}
	}
	
	this.getID                     = function() { return private._id; }
	this.getMenuID                 = function() { return private._menu_id; }
	this.getGroupID                = function() { return private._group_id; }
	this.getName                   = function() { return private._name; }
	this.getImage                  = function() { return private._image; }
	this.getDescription            = function() { return private._description; }
	this.getActive                 = function() { return private._active; }
	this.getPrint                  = function() { return private._print; }
	this.getLastModifiedDate       = function() { return private._last_modified_date; }
	this.getPrinter                = function() { return private._printer; }
	this.getModifierListId         = function() { return private._modifier_list_id; }
	this.getApplyTaxRate           = function() { return private._apply_taxrate; }
	this.getCustomTaxRate          = function() { return private._custom_taxrate; }
	this.getForcedModifierGroupID  = function() { return private._forced_modifier_group_id; }
	this.getPrintOrder             = function() { return private._print_order; }
	this.getSuperGroupID           = function() { return private._super_group_id; }
	this.getTaxInclusion           = function() { return private._tax_inclusion; }
	this.getLTGDisplay             = function() { return private._ltg_display; }
	this.getHappyHour              = function() { return private._happyhour; }
	this.getTaxProfileID           = function() { return private._tax_profile_id; }
}
MenuCategory.prototype.constructor = MenuCategory;
MenuCategory.prototype = new Orderable();

/***
 * MenuItem Class
 * 
 **/
var MenuItem = function(obj){
	Orderable.call(this);
	var private = {
		_id                        : 0,
		_category_id               : 0,
		_menu_id                   : 0,
		_menu					   : null,
		_name                      : "",
		_price                     : "",
		_description               : "",
		_image                     : "",
		_image2                    : "",
		_image3                    : "",
		_misc_content              : "",
		_options1                  : "",
		_options2                  : "",
		_options3                  : "",
		_active                    : "",
		_print                     : "",
		_quick_item                : "",
		_last_modified_date        : "",
		_printer                   : "",
		_apply_taxrate             : "",
		_custom_taxrate            : "",
		_modifier_list_id          : 0,
		_modifier_list			   : null,
		_forced_modifier_group_id  : 0,
		_forced_modifier_group	   : null,
		_ingredients               : "",
		_open_item                 : "",
		_hidden_value              : "",
		_hidden_value2             : "",
		_hidden_value3             : "",
		_allow_deposit             : "",
		_UPC                       : "",
		_inv_count                 : 0,
		_show_in_app               : "",
		_super_group_id            : 0,
		_super_group			   : null,
		_tax_inclusion             : "",
		_ltg_display               : "",
		_happyhour                 : "",
		_tax_profile_id            : 0,
		_tax_profile			   : null
	}
	
	for(name in private){
		if(obj && obj[name.substr(1)]){
			private[name] = obj[name.substr(1)];
		}
	}
	
	this.getID                    = function() { return private._id; }
	this.getCategoryID            = function() { return private._category_id; }
	this.getMenuID                = function() { return private._menu_id; }
	this.getName                  = function() { return private._name; }
	this.getPrice                 = function() { return private._price; }
	this.getDescription           = function() { return private._description; }
	this.getImage                 = function() { return private._image; }
	this.getImage2                = function() { return private._image2; }
	this.getImage3                = function() { return private._image3; }
	this.getMiscContent           = function() { return private._misc_content; }
	this.getOptions1              = function() { return private._options1; }
	this.getOptions2              = function() { return private._options2; }
	this.getOptions3              = function() { return private._options3; }
	this.getActive                = function() { return private._active; }
	this.getPrint                 = function() { return private._print; }
	this.getQuickItem             = function() { return private._quick_item; }
	this.getLastModifiedDate      = function() { return private._last_modified_date; }
	this.getPrinter               = function() { return private._printer; }
	this.getApplyTaxRate          = function() { return private._apply_taxrate; }
	this.getCustomTaxRate         = function() { return private._custom_taxrate; }
	this.getModifierListID        = function() { return private._modifier_list_id; }
	this.getForcedModifierGroupID = function() { return private._forced_modifier_group_id; }
	this.getIngredients           = function() { return private._ingredients; }
	this.getOpenItem              = function() { return private._open_item; }
	this.getHiddenValue           = function() { return private._hidden_value; }
	this.getHiddenValue2          = function() { return private._hidden_value2; }
	this.getHiddenValue3          = function() { return private._hidden_value3; }
	this.getAllowDeposit          = function() { return private._allow_deposit; }
	this.getUPC                   = function() { return private._UPC; }
	this.getInvCount              = function() { return private._inv_count; }
	this.getShowInApp             = function() { return private._show_in_app; }
	this.getSuperGroupID          = function() { return private._super_group_id; }
	this.getTaxInclusion          = function() { return private._tax_inclusion; }
	this.getLTGDisplay            = function() { return private._ltg_display; }
	this.getHAppyHour             = function() { return private._happyhour; }
	this.getTaxProfileID          = function() { return private._tax_profile_id; }
	
}
MenuItem.prototype.constructor = MenuItem;
MenuItem.prototype = new Orderable();