
<!DOCTYPE HTML>
<html>
<head>
	<meta charset="UTF-8">
    <title>Lavu ToGo - 404 Page Not Found</title>
	<style type="text/css">
		html, body, div, span, applet, object, iframe,
		h1, h2, h3, h4, h5, h6, p, blockquote, pre,
		a, abbr, acronym, address, big, cite, code,
		del, dfn, em, img, ins, kbd, q, s, samp,
		small, strike, strong, sub, sup, tt, var,
		b, u, i, center,
		dl, dt, dd, ol, ul, li,
		fieldset, form, label, legend,
		table, caption, tbody, tfoot, thead, tr, th, td,
		article, aside, canvas, details, embed, 
		figure, figcaption, footer, header, hgroup, 
		menu, nav, output, ruby, section, summary,
		time, mark, audio, video {
			margin: 0;
			padding: 0;
			border: 0;
			font-size: 100%;
			font: inherit;
			vertical-align: baseline;
		}
		/* HTML5 display-role reset for older browsers */
		article, aside, details, figcaption, figure, 
		footer, header, hgroup, menu, nav, section {
			display: block;
		}
		body {
			line-height: 1;
		}
		ol, ul {
			list-style: none;
		}
		blockquote, q {
			quotes: none;
		}
		blockquote:before, blockquote:after,
		q:before, q:after {
			content: '';
			content: none;
		}
		table {
			border-collapse: collapse;
			border-spacing: 0;
		}
		body{
			font: 16px/1.4em 'Helvetica Neue', Helvetica, Arial, sans-serif;
			color: #333;
			background: #f7f7f7;
			overflow: hidden;
			}
		.wrapper{
			width: 80%;
			margin: 0 auto;
			padding: 40px 0 200px;
			z-index: 20;
			}
		h1{
			font-size: 2em;
			font-weight: bold;
			margin-bottom: 10px;
			}
		p{	
			padding-top: 10px;
			}
		a{
			font-weight: bold;
			text-decoration: none;
			color: #5C7996;
			}
		#sync{
			width: 600px;
			height: 515px;
			background: url("http://lavutogo.com/assets/images/Lavu_Icon_600w.png") no-repeat;
			position: absolute;
			bottom: -110px;
			right: -150px;
			z-index: 10;
			}
	</style>
</head>
<body>
	<div class="wrapper">
		<div id="sync"></div>
		<h1>Page Not Found</h1>
		<p>
			Sorry, the Lavu ToGo merchant you&rsquo;re looking for doesn&rsquo;t exist or is disabled.<br/>
			<span>Go back to the <a href="http://lavutogo.com" title="Lavu ToGo Home Page">home page</a> or <a href="http://www.poslavu.com/en/ipad-pos-contact">contact us</a> if there&rsquo;s a problem.</span>
		</p>
	</div>
</body>
</html>
