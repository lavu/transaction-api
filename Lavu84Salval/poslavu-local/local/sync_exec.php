<?php

	if (!isset($in_setup_area)) {
		$in_setup_area = false;
	}
	//$s_remote_request_url = "https://api-lls.poslavu.com/cp/reqserv/index.php";
	$s_remote_request_url = "http://api-lls.lavu.com/cp/reqserv/index.php";
	$i_sync_exec_version_number = 4; // Incremented to 4, now posts LLS_Version info.
	$i_sync_version_number = (int)get_get_var("v");
	
	$lls_install_info_arr = parse_lls_version_and_install_date();
	$i_lls_install_version = $lls_install_info_arr['version'];
	$i_lls_install_date = $lls_install_info_arr['date_installed'];
	
	$debug_call_stack = array();

	function wrapItemWithStr( $str, $item ){
		return "{$str}{$item}{$str}";
	}

	function wrapWithBackticks( $str ){
		return wrapItemWithStr("`", $str);
	}

	function wrapWithSingleQuotes( $str ){
		return wrapItemWithStr("'", "[{$str}]");
	}

	function get_get_var($s_getname) {
		if (isset($_GET[$s_getname])) {
			return $_GET[$s_getname];
		}
		return "";
	}
	
	function parse_lls_version_and_install_date(){
		//IF version > a2.1_001 else the version.txt file doesn't exist
		$fp = fopen("/poslavu-local/local/version.txt", "r");
		$contents = fread($fp, filesize("/poslavu-local/local/version.txt"));
		//error_log($contents);
		fclose($fp);
		$parts = explode("=",$contents);
		$version = isset($parts[1]) ? $parts[1] : '';
		$date_installed = isset($parts[2]) ? $parts[2] : '';
		return array("version" => $version, "date_installed" => $date_installed);
	}
	
	function get_version_numbers_as_post() {
		global $i_sync_exec_version_number;
		global $i_sync_version_number;
		global $i_lls_install_version;
		global $i_lls_install_date;
		
		$i_sync_version_number = (int)$i_sync_version_number;
		$i_sync_exec_version_number = (int)$i_sync_exec_version_number;
		
		return "&sync_version=".$i_sync_version_number."&sync_exec_version=".$i_sync_exec_version_number.
					"&lls_install_version=".$i_lls_install_version."&lls_install_date=".$i_lls_install_date;
	}
	
	function sync_interval_setting($n)
	{
		if($n==1)
			return 5;
		else
			return 60;
	}
	
	function last_sync_filename()
	{
		return dirname(__FILE__) . "/../logs/last_sync.txt";
	}
	
	function get_last_sync_data()
	{
		$lsfile = last_sync_filename();
		if(is_file($lsfile))
		{
			$ft = fopen($lsfile,"r");
			$last_sync_data = fread($ft,filesize($lsfile));
			fclose($ft);
		}
		else $last_sync_data = "0|";
		$last_sync_data = explode("|",$last_sync_data);
		
		$last_sync_time = $last_sync_data[0];
		$last_management_time = (isset($last_sync_data[1]))?$last_sync_data[1]:0;
		$sync_counter = (isset($last_sync_data[2]))?$last_sync_data[2]:0;
		
		$rdata = array();
		$rdata['last_sync_time'] = $last_sync_time;
		$rdata['last_management_time'] = $last_management_time;
		$rdata['time_since'] = time() - $last_sync_time;
		$rdata['time_since_management'] = time() - $last_management_time;
		$rdata['sync_counter'] = $sync_counter;
		return $rdata;
	}
	
	function set_last_sync_time($rdata,$set_mgmt=false)
	{

		if($set_mgmt!==false)
			$str = $rdata['last_sync_time'] . "|" . $set_mgmt;
		else
			$str = time() . "|" . $rdata['last_management_time'] . "|" . ($rdata['sync_counter']+1);
		
		$lsfile = last_sync_filename();
		$ft = fopen($lsfile,"w");
		fwrite($ft,$str);
		fclose($ft);
	}

	function set_remote_control_mode($set_mode)
	{
		$tls_info = get_last_sync_data();
		if($set_mode==1)
			set_last_sync_time($tls_info,time());
		else if($set_mode==0)
			set_last_sync_time($tls_info,0);
	}
		
	$file_write_str = "";
	function prepare_file_for_writing($file_name,$file_contents)
	{
		global $file_write_str;
		$fns = "!_"."!"."fns"."!"."_!";
		$fne = "!_"."!"."fne"."!"."_!";
		$fcs = "!_"."!"."fcs"."!"."_!";
		$fce = "!_"."!"."fce"."!"."_!";
				
		$file_write_str .= $fns . $file_name . $fne . $fcs . $file_contents . $fce;
	}
	
	function sync_recent_records($sync_table_list)
	{
		for($i=0; $i<count($sync_table_list); $i++)
		{
			$tname = $sync_table_list[$i];
			sync_recent_records_to_remote($tname);
		}
	}
	
	function setup_tables_for_syncing($sync_table_list, $auto_sync_records=false)
	{
		for($i=0; $i<count($sync_table_list); $i++)
		{
			$tname = $sync_table_list[$i];
			echo "<br>--------checking " . $tname . "------<br>";

			$ind_query = lavu_query("select * from `[1]` limit 1",$tname);
			if($ind_query === FALSE || !mysqli_num_rows($ind_query))
			{
				$success = lavu_query("insert into `[1]` (`id`) values ('1')",$tname);
				if($success)
					echo "seeded " . $tname . "<br>";
				else echo "failed to seed  " . $tname . "<br>";
				$ind_query = lavu_query("select * from `[1]` limit 1",$tname);
			}	
		
			if($ind_query !== FALSE && mysqli_num_rows($ind_query))
			{
				echo $tname . "<br>";
				$lastmod_found = false;
				$lastsync_found = false;
				$lastmod_index_found = false;
				$lastsync_index_found = false;

				$ind_read = mysqli_fetch_assoc($ind_query);
				foreach($ind_read as $key => $val)
				{
					if($key=="lastmod") $lastmod_found = true;
					if($key=="lastsync") $lastsync_found = true;
				}

				$ix_query = lavu_query("show indexes from `[1]`",$tname);
				while($ix_read = mysqli_fetch_assoc($ix_query))
				{
					if($ix_read['Column_name']=="lastmod") $lastmod_index_found = true;
					else if($ix_read['Column_name']=="lastsync") $lastsync_index_found = true;
					//else echo "  " . $ix_read['Column_name'] . "<br>";
				}

				if(!$lastmod_found)
				{					
					$success = lavu_query("alter table `[1]` add `lastmod` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP",$tname);
					if($success) echo "<br>added lastmod for $tname<br>";
				}
				if(!$lastsync_found)
				{
					$success = lavu_query("alter table `[1]` add `lastsync` varchar(255) not null default ''",$tname);
					if($success) echo "<br>added lastsync for $tname<br>";
				}
				if(!$lastmod_index_found)
				{
					echo "<br>adding lastmod index:<br>";
					$success = lavu_query("alter table `[1]` ADD INDEX([2])",$tname,"lastmod");
					if($success) echo "<br>added index for lastmod on table $tname"; else echo "<br>could not add index for lastmod on table $tname<br>";
				}
				if(!$lastsync_index_found)
				{
					echo "<br>adding lastsync index:<br>";
					$success = lavu_query("alter table `[1]` ADD INDEX([2])",$tname,"lastsync");
					if($success) echo "<br>added index for lastsync on table $tname"; else echo "<br>could not add index for lastsync on table $tname<br>";
				}

				if($auto_sync_records)
				{
					if(is_numeric($auto_sync_records))
						$auto_sync_record_count = $auto_sync_records;
					else
						$auto_sync_record_count = 400;
					$recent_query = lavu_query("select * from `[1]` order by id desc limit 1",$tname);
					if(mysqli_num_rows($recent_query))
					{
						$recent_read = mysqli_fetch_assoc($recent_query);
						$maxid = $recent_read['id'];
						$minid = $maxid - $auto_sync_record_count;
						if(minid < 0) $minid = 0;
	
						$success = lavu_query("update `[1]` set `lastmod`='[2]', `lastsync`='[2]' where id < '[3]'",$tname,"2011-01-01 00:00:00",$minid); 
						if($success) echo "<br>updated sync data for old records in $tname<br>"; else echo "<br>failed to update sync data for $tname<br>";
					}
				}
			}
		}
	}
	
	function report_sync_finish($mode,$parts)
	{
		global $server_remote_dataname;
		global $server_remote_api_key;
		global $server_remote_api_token;
		global $server_remote_locationid;
		global $local_sync_interval;
		global $s_remote_request_url;
		
		$postvars = "dataname=".$server_remote_dataname;
		$postvars .= "&key=".$server_remote_api_key;
		$postvars .= "&token=".$server_remote_api_token;
		
		$postvars .= "&cmd=sync_finished";
		$postvars .= "&table=users";
		$postvars .= "&sync_mode=" . $mode;
		$postvars .= "&local_sync_interval=" . $local_sync_interval;
		//$postvars .= "&colnames=".urlencode($col_names);
		//$postvars .= "&colvals=".urlencode($col_vals);
		$postvars .= "&rowid=0";
		$postvars .= get_version_numbers_as_post();
		
		$ch = curl_init(); 
		curl_setopt($ch, CURLOPT_URL, $s_remote_request_url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postvars);
		$ch_response = curl_exec($ch);
		
		echo "<br>Sync Finished: $ch_response";
	}
	
	function find_table_to_sync()
	{
		
		global $server_remote_dataname;
		global $server_remote_api_key;
		global $server_remote_api_token;
		global $server_remote_locationid;
		global $local_sync_interval;
		global $s_remote_request_url;

		
		$postvars = "dataname=".$server_remote_dataname;
		$postvars .= "&key=".$server_remote_api_key;
		$postvars .= "&token=".$server_remote_api_token;
		
		$postvars .= "&cmd=sync_updated";
		$postvars .= "&table=users";
		$postvars .= "&local_sync_interval=" . $local_sync_interval;
		//$postvars .= "&colnames=".urlencode($col_names);
		//$postvars .= "&colvals=".urlencode($col_vals);
		$postvars .= "&rowid=0";
		$postvars .= get_version_numbers_as_post();
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $s_remote_request_url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postvars);
		$ch_response = curl_exec($ch);
		
		echo "response for $server_remote_dataname: $ch_response<br>";
		$parts = explode("|",$ch_response,3);
		$mode = $parts[0];
		if($mode!="" && $mode!="0" && count($parts) > 1)
		{
			$tablename = trim($parts[1]);
			$rows = (isset($parts[2]))?trim($parts[2]):"";
			if($mode=="void orders" && count($parts) > 2)
			{
				$close_time = trim($parts[1]);
				$void_orders = explode(",",trim($parts[2]));
				
				for($v = 0; $v<count($void_orders); $v++)
				{
					lavu_query("update `orders` set `void`='1', `closed`='[1]' where `order_id`='[2]' and `location_id`='[3]'",$close_time,trim($void_orders[$v]),$server_remote_locationid);
				}
			}
			else if($mode=="remote control" && count($parts) > 1)
			{
				$set_remote_control = trim($parts[1]);
				if($set_remote_control=="1")
					$local_sync_interval = sync_interval_setting(1);
				else if($set_remote_control=="0")
					$local_sync_interval = sync_interval_setting(0);
					
				set_remote_control_mode($set_remote_control);
			}
			else if($mode=="health check")
			{
				require_once(dirname(__FILE__) . "/health_check.php");		
				echo "Performing Health Check:<br><br>" . perform_health_check();
			}
			else if($mode=="run local query" && count($parts) > 2)
			{
				$run_query = $parts[2];
				
				$run_query_parts = explode("[_query_]",$run_query);
				if(count($run_query_parts) > 1)
				{
					for($r=0; $r<count($run_query_parts); $r++)
					{
						$run_query = trim($run_query_parts[$r]);
						lavu_query($run_query);
					}
				}
				else
				{
					$query_result = lavu_query($run_query);
					
					$response = "";
					
					if($query_result)
					{
						if(mysqli_num_rows($query_result))
						{
							$response .= "<style>";
							$response .= ".qtable { font-family:Verdana, Arial; font-size:10px; } ";
							$response .= "</style>";
							$response .= "<table>";
							$rowcount = 0;
							while($row = mysqli_fetch_assoc($query_result))
							{
								$response .= "<tr><td colspan='3' style='border-bottom:solid 2px black' align='center'>Row ".($rowcount + 1)."</td></tr>";
								foreach($row as $key => $val)
								{
									//if($val==null) $val = "null: " . gettype($val);
									$response .= "<tr><td align='right' bgcolor='#dedede' style='padding-right:8px' class='qtable'>".$key."</td><td width='10'>&nbsp;</td><td width='360' class='qtable'>" . $val . "</td></tr>";
								}
								$rowcount++;
							}
							$response .= "</table>";
						}
						else
						{
							$response .= "Success";
						}
					}
					else
					{
						$response .= "Failed: $run_query";
					}
					
					transmit_query_result($run_query,$response);
				}
			}
			else if($mode=="sync database")
			{
				sync_local_database_structure();
				transmit_local_file_structure();
			}
			else if($mode=="open tunnel")
			{
				run_lsvpn();			
			}
			else if($mode=="copy remote file" && count($parts) > 2)
			{
				$remote_filename = $parts[1];
				$local_filename = $parts[2];
				
				copy_remote_file($remote_filename,$local_filename);
				transmit_local_file_structure();
			}
			else if($mode=="remove local file")
			{
				$local_filename = $parts[1];
				
				remove_local_file($local_filename);
				transmit_local_file_structure();
			}
			else if($mode=="upload signature")
			{
				$signature_name = $parts[1];
				
				$postvars = array();
				$postvars['dataname'] = $server_remote_dataname;
				$postvars['key'] = $server_remote_api_key;
				$postvars['token'] = $server_remote_api_token;
				$postvars['cmd'] = "upload_signature";
				$postvars['signature_name'] = $signature_name;
				$postvars['signature_file'] = "@".dirname(__FILE__) . "/../images/$server_remote_dataname/signatures/$signature_name";
				//$postvars .= get_version_numbers_as_post(); //Error appending string to array.
				
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $s_remote_request_url);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $postvars);
				$response = curl_exec($ch);
				
				echo $response;
			}

			else if($mode=="upload waiver")
			{
                               
				$signature_name = $parts[1];
				
				$postvars = array();
				$postvars['signature_type'] = 'waiver';
				$postvars['dataname'] = $server_remote_dataname;
				$postvars['key'] = $server_remote_api_key;
				$postvars['token'] = $server_remote_api_token;
				$postvars['cmd'] = "upload_signature";
				$postvars['signature_name'] = $signature_name;
				$postvars['signature_file'] = "@".dirname(__FILE__) . "/../components/lavukart/companies/$server_remote_dataname/signatures/$signature_name";
				//$postvars .= get_version_numbers_as_post(); //Error appending string to array

				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $s_remote_request_url);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $postvars);
				$response = curl_exec($ch);
				
				echo $response;
			}
			else if($mode=="write to local file")
			{
				$local_filename = $parts[1];
				$local_file_contents = $parts[2];
				
				/*$fp = fopen($local_filename,"w");
				fwrite($fp, $local_file_contents);
				fclose($fp);*/
				prepare_file_for_writing($local_filename,$local_file_contents);
				
				//--------- read file structure and transmit back to server
				/*$fp = fopen($local_filename,"r");
				$str = fread($fp, filesize($local_filename));
				fclose($str);
				
				transmit_local_file($local_filename,$str);*/
				
				transmit_local_file_structure();				
			}
			else if($mode=="retrieve local file")
			{
				$local_filename = $parts[1];
				
				$fp = fopen($local_filename,"r");
				$str = fread($fp, filesize($local_filename));
				fclose($str);
				
				transmit_local_file($local_filename,$str);
			}
			else if($mode=="read file structure")
			{
				transmit_local_file_structure();
			}
			else if($mode=="entire_table" && $tablename!="")
			{
				sync_remote_to_local("entire_table",$tablename);
			}
			else if($mode=="rows" && $tablename!="" && $rows!="")
			{
				sync_remote_to_local("rows",$tablename,$rows);
			}
			else
			{
				require_once(dirname(__FILE__) . "/health_check.php");
				run_other_sync_mode($mode,$parts);
			}
			
			report_sync_finish($mode,$parts);
		}	
	}
		
	function output_array_cols($order_cols)
	{
		$str_cols = "";
		for($i=0; $i<count($order_cols); $i++)
		{
			$key = $order_cols[$i];
			if($str_cols != "")
				$str_cols .= "[_col_]";
			$str_cols .= $key;
		}
		return $str_cols;
	}
	function get_col_list_from_array($order_cols,$order_read,$ignore_cols="")
	{
		$ignore_cols = explode(",",$ignore_cols);
		if(count($order_cols)<1)
		{
			foreach($order_read as $key => $val)
			{
				if($key!="lastmod" && $key!="lastsync")
				{
					$include_col = true;
					for($n=0; $n<count($ignore_cols); $n++)
					{
						if($ignore_cols[$n]==$key)
							$include_col = false;
					}
					if($include_col)
						$order_cols[] = $key;
				}
			}
		}
		return $order_cols;
	}
	function get_val_list_from_arrays($order_cols, $order_read, $ignore_cols="" ,$build_array=false)
	{
		$ignore_cols = explode(",",$ignore_cols);
		$order_properties = $build_array?array():"";
		$ncount = 0;
		for($n=0; $n<count($order_cols); $n++)
		{
			$colname = $order_cols[$n];

			$include_col = true;
			for($m=0; $m<count($ignore_cols); $m++)
			{
				if($ignore_cols[$m]==$colname)
					$include_col = false;
			}
			if($include_col)
			{
				if ($build_array) {
					$order_properties[] = $order_read[$colname];
				} else {
					if ($ncount > 0) {
						$order_properties .= "[_col_]";
					}
					$order_properties .= $order_read[$colname];
				}
				$ncount++;
			}
		}
		return $order_properties;
	}
	
	function sync_recent_records_to_remote($tablename)
	{
		global $server_remote_dataname;
		global $server_remote_api_key;
		global $server_remote_api_token;
		global $s_remote_request_url;
		global $debug_call_stack;
		
		$use_ialid = true;

		$row_query = lavu_query("select * from `$tablename` where (`lastmod` > `lastsync` or `lastsync`='') order by `lastmod` asc limit 20");
		if($row_query && mysqli_num_rows($row_query))
		{
			$debug_call_stack[] = "{ syncing {$tablename} " . mysqli_num_rows($row_query) . " records }";
			$str = "";
			$cols = array();
			$rows = "";
			$str = "";
			while($row_read = mysqli_fetch_assoc($row_query))
			{
				$rowid = $row_read['id'];
	
				if ($tablename == "action_log") {
	
					$ialid = $row_read['ialid']; // internal action log ID	
					if (strlen($ialid) < 16) {
						
						$mt = explode(" ", (string)microtime());
						$sec = $mt[1];
						$dot_msec = explode(".", $mt[0]);
						$msec = $dot_msec[1];
						$randomString = str_pad((string)rand(1, 99999999), 8, "0", STR_PAD_LEFT);
						$new_ialid = $sec."-".$randomString."-".$msec;

						$row_read['ialid'] = $new_ialid;
						
						$save_ialid = lavu_query("UPDATE `action_log` SET `ialid` = '[1]' WHERE `id` = '[2]'", $new_ialid, $rowid);
						if (!$save_ialid) {
							$use_ialid = false;
							error_log("ialid update failed: ".mysql_error());
						}
					}
				}
				
				$cols = get_col_list_from_array($cols,$row_read);
				$vals = get_val_list_from_arrays($cols,$row_read);
			
				if($str != "")
					$str .= "[_row_]";
				if ($use_ialid) {
					$str .= "[_ialid_start_]";
					$str .= $row_read['ialid'];
					$str .= "[_ialid_end_]";
				}
				$str .= "[_row_id_start_]";
				$str .= $rowid;
				$str .= "[_row_id_end_]";
				$str .= "[_row_contents_start_]";
				$str .= $vals;
				$str .= "[_row_contents_end_]";

				//Check to see if we're trying to sync more than 5MB
				if( strlen( $str ) > 5242880 ) {
					break;
				}
			}
			
			$colstr = "";
			$colstr .= "[_cols_start_]";
			$colstr .= output_array_cols($cols);
			$colstr .= "[_cols_end_]";
			
			$str = $colstr . "[_rows_start_]" . $str . "[_rows_end_]";
			
			$postvars = "dataname=".$server_remote_dataname;
			$postvars .= "&key=".$server_remote_api_key;
			$postvars .= "&token=".$server_remote_api_token;
			
			$postvars .= "&cmd=sync_records_up";
			$postvars .= "&use_ialid=".($use_ialid?"1":"0");
			$postvars .= "&tablename=".$tablename;
			$postvars .= "&data=".urlencode($str);
			$postvars .= get_version_numbers_as_post();
	
			if ($server_remote_dataname == "84_salval") {
				$s_remote_request_url = "https://admin.poslavu.com/cp/reqserv/index.php";
			}

			$sync_id = rand(1000, 9999);
			error_log("sync_recent_records_to_remote (".$sync_id.") - dn: ".$server_remote_dataname." - url: ".$s_remote_request_url);

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $s_remote_request_url);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $postvars);
			$ch_response = curl_exec($ch);
	
			error_log("ch_response (".$sync_id."): ".$ch_response);

			//error_log("error_info: ".json_encode($error_info));
			//error_log("curl_errno: ".$curl_errno);
							
			$rsp = explode("|",$ch_response,3);
			if($rsp[0]=="1")
			{
				if(count($rsp) > 1)
				{
					$idlist = explode(",",$rsp[1]);
					for($n=0; $n<count($idlist); $n++)
					{
						$rowid = trim($idlist[$n]);
						if(is_numeric(str_replace("-","",$rowid)))
						{
							lavu_query("update `$tablename` set `lastsync`=now() where `id`='[1]'",$rowid);
							echo "$tablename row $rowid synced<br>";
						}
						else
						{
						}
					}
				}
				if(count($rsp) > 2)
					echo "<br>" . $rsp[2];
			}
		}
	}
	
	function sync_recent_orders_to_remote()
	{
		global $server_remote_dataname;
		global $server_remote_api_key;
		global $server_remote_api_token;
		global $server_remote_locationid;
		global $s_remote_request_url;

		$order_id_list = "";
		
		$order_query = lavu_query("select `order_id` from `orders` where `location_id`='[1]' and (`lastmod` > `lastsync` or `lastsync`='') order by `lastmod` asc limit 20",$server_remote_locationid);

		error_log("sync_recent_orders_to_remote order count: ".mysqli_num_rows($order_query));

		while($order_read = mysqli_fetch_assoc($order_query)) {

			if ($order_id_list!="") {
				$order_id_list .= " or ";
			}
			$order_id_list .= "`order_id`='".mysqli_real_escape_string($order_read['order_id'])."'";
		}

		$order_id_query = "`location_id`='".mysqli_real_escape_string($server_remote_locationid)."' and ($order_id_list)";
		require_once(dirname(__FILE__) . "/../cp/resources/rfix_functions.php");
		process_fix("closed", "", "", $server_remote_locationid, "", $order_id_query);
		
		$order_query = lavu_query("select * from `orders` where `location_id`='[1]' and (`lastmod` > `lastsync` or `lastsync`='') order by `lastmod` asc limit 20", $server_remote_locationid);
		
		if(mysqli_num_rows($order_query))
		{
			$order_cols = array();
			$order_contents_cols = array();
			$modifiers_used_cols = array();
			$order_transactions_cols = array();
			$paid_in_out_internal_ids = array();
			$order_split_checks_cols = array();
			$order_alternate_payments_cols = array();
			//$order_rows = "";
			//$str = "";

			$orders_array = array();

			while($order_read = mysqli_fetch_assoc($order_query))
			{
				$order_id = $order_read['order_id'];
				$ioid = $order_read['ioid'];
				
				$order_cols = get_col_list_from_array($order_cols, $order_read, "id");
				$order_properties = get_val_list_from_arrays($order_cols, $order_read, "id", true);
				 
				// order_contents   			
				$order_contents = array();
				$content_query = lavu_query("select * from `order_contents` where `order_id`='[1]' and `loc_id`='[2]'",$order_id,$server_remote_locationid);

				error_log("order ".$order_id." has ".mysqli_num_rows($content_query)." items");

				while ($content_read = mysqli_fetch_assoc($content_query))
				{
					$order_contents_cols = get_col_list_from_array($order_contents_cols, $content_read, "id");
					$order_contents[] = get_val_list_from_arrays($order_contents_cols, $content_read, "id", true);
				}

				// modifiers_used
				$modifiers_used = array();
				$mu_query = lavu_query("select * from `modifiers_used` where `order_id`='[1]' and `loc_id`='[2]'",$order_id,$server_remote_locationid);
				while ($mu_read = mysqli_fetch_assoc($mu_query))
				{
					$modifiers_used_cols = get_col_list_from_array($modifiers_used_cols, $mu_read, "id");
					$modifiers_used[] = get_val_list_from_arrays($modifiers_used_cols, $mu_read, "id", true);
				}
				
				$paid_in_out_filter = "";
				$is_paid_in_or_out = (strstr($order_id, "Paid"));
				$paid_in_out_internal_ids[$order_id] = array();
				if ($is_paid_in_or_out) {
					$trans_min_datetime = date("Y-m-d H:i:s",mktime(0,0,0,date("m"),date("d")-3,date("Y")));
					$paid_in_out_filter = " AND `process_data` != 'synced' ORDER BY `id` DESC LIMIT 250";
				}
								 
				// order_transactions
				$order_transactions = array();
				$trans_query = lavu_query("select * from `cc_transactions` where `order_id`='[1]' and `loc_id`='[2]'".$paid_in_out_filter,$order_id,$server_remote_locationid);
				
				error_log("order ".$order_id." has ".mysqli_num_rows($trans_query)." transactions");
				
				while ($trans_read =mysqli_fetch_assoc($trans_query))
				{
					$internal_id = $trans_read['internal_id'];
					if (empty($internal_id)) {
						$internal_id = rand(1, 250)."-".newInternalID();
						if (!empty($internal_id)) {
							$update_record = lavu_query("UPDATE `cc_transactions` SET `internal_id` = '[1]' WHERE `id` = '[2]'", $internal_id, $trans_read['id']);	
							if ($update_record) {
								$trans_read['internal_id'] = $internal_id;
							} else {
								$internal_id = "";	
							}
						}
					}
					if ($is_paid_in_or_out && !empty($internal_id)) {
						$paid_in_out_internal_ids[$order_id][] = $internal_id;
					}
				
					$order_transactions_cols = get_col_list_from_array($order_transactions_cols, $trans_read, "id");
					$order_transactions[] = get_val_list_from_arrays($order_transactions_cols, $trans_read, "id", true);
				}
				
				error_log($order_id." paid_in_out_internal_ids count: ".count($paid_in_out_internal_ids));
				
				// split_check_details
				$order_split_checks = array();
				$split_chk_query = lavu_query("select * from `split_check_details` where `order_id`='[1]' and `loc_id`='[2]'",$order_id,$server_remote_locationid);
				while ($splitchk_read =mysqli_fetch_assoc($split_chk_query))
				{
					$order_split_checks_cols = get_col_list_from_array($order_split_checks_cols, $splitchk_read, "id");
					$order_split_checks[] = get_val_list_from_arrays($order_split_checks_cols, $splitchk_read, "id", true);
				}
				
				// alternate payments
				$order_alternate_payments = array();
				$alternate_payments_query = lavu_query("select * from `alternate_payment_totals` where `order_id`='[1]' and `loc_id`='[2]'",$order_id,$server_remote_locationid);
				while ($alternate_payments_read = mysqli_fetch_assoc($alternate_payments_query))
				{
					$order_alternate_payments_cols = get_col_list_from_array($order_alternate_payments_cols, $alternate_payments_read, "id");
					$order_alternate_payments[] = get_val_list_from_arrays($order_alternate_payments_cols, $alternate_payments_read, "id", true);
				}
												
				/*if($str != "")
					$str .= "[_order_row_]"; 
					
				$str .= "[_order_id_start_]";
				$str .= $order_id;
				$str .= "[_order_id_end_]"; 

				$str .= "[_ioid_start_]";
				$str .= $ioid;
				$str .= "[_ioid_end_]"; 
				
				$str .= "[_order_properties_start_]";
				$str .= $order_properties;
				$str .= "[_order_properties_end_]"; 
				
				$str .= "[_order_contents_start_]";
				$str .= $order_contents;
				$str .= "[_order_contents_end_]"; 
	
				$str .= "[_modifiers_used_start_]";
				$str .= $modifiers_used;
				$str .= "[_modifiers_used_end_]"; 
			
				$str .= "[_order_transactions_start_]";
				$str .= $order_transactions;
				$str .= "[_order_transactions_end_]";
				
				// split checks
				$str .= "[_order_split_checks_start_]";
				$str .= $order_split_checks;
				$str .= "[_order_split_checks_end_]";

				// alternate payments
				$str .= "[_order_alternate_payments_start_]";
				$str .= $order_alternate_payments;
				$str .= "[_order_alternate_payments_end_]";*/
				
				$sync_order = array(
					'order_id'		=> $order_id,
					'ioid'			=> $ioid,
					'properties'	=> $order_properties,
					'contents'		=> $order_contents,
					'modifiers'		=> $modifiers_used,
					'transactions'	=> $order_transactions,
					'split_checks'	=> $order_split_checks,
					'alt_payments'	=> $order_alternate_payments
				);
				
				$orders_array[] = $sync_order;
			}
			
			/*$colstr = "";
			$colstr .= "[_order_cols_start_]";
			$colstr .= output_array_cols($order_cols);
			$colstr .= "[_order_cols_end_]";        
			
			// order contents
			$colstr .= "[_order_contents_cols_start_]";
			$colstr .= output_array_cols($order_contents_cols);
			$colstr .= "[_order_contents_cols_end_]";        

			// modifiers used
			$colstr .= "[_modifiers_used_cols_start_]";
			$colstr .= output_array_cols($modifiers_used_cols);
			$colstr .= "[_modifiers_used_cols_end_]";
			
			// order cc transactions
			$colstr .= "[_order_transactions_cols_start_]";
			$colstr .= output_array_cols($order_transactions_cols);
			$colstr .= "[_order_transactions_cols_end_]";
			
			// split checks
			$colstr .= "[_order_split_checks_cols_start_]";
			$colstr .= output_array_cols($order_split_checks_cols);
			$colstr .= "[_order_split_checks_cols_end_]";       
			
			// alternate payments
			$colstr .= "[_order_alternate_payments_cols_start_]";
			$colstr .= output_array_cols($order_alternate_payments_cols);
			$colstr .= "[_order_alternate_payments_cols_end_]";       
			
			$str = $colstr . "[_orders_start_]" . $str . "[_orders_end_]";*/
			
			$sync_array = array(
				'order_cols'			=> $order_cols,
				'contents_cols'		=> $order_contents_cols,
				'modifiers_cols'	=> $modifiers_used_cols,
				'transactions_cols'	=> $order_transactions_cols,
				'split_check_cols'	=> $order_split_checks_cols,
				'alt_payment_cols'	=> $order_alternate_payments_cols,
				'orders'				=> $orders_array
			);
			
			$postvars = "dataname=".$server_remote_dataname;
			$postvars .= "&key=".$server_remote_api_key;
			$postvars .= "&token=".$server_remote_api_token;
			
			$postvars .= "&cmd=sync_orders_up";
			//$postvars .= "&data=".urlencode($str);
			$postvars .= "&json_data=".urlencode(json_encode($sync_array));
			$postvars .= sendKnownRecentApiOrders();
			//$postvars .= "&data=BASE64|_|".base64_encode($str); // might end up being helpful with transmitting unusual characters?
			$postvars .= get_version_numbers_as_post();
			
			//error_log("postvars: ".$postvars);
			
			if ($server_remote_dataname == "84_salval") {
				$s_remote_request_url = "https://admin.poslavu.com/cp/reqserv/index.php";
			}
	
			$sync_id = rand(1000, 9999);
			error_log("sync_recent_orders_to_remote url (".$sync_id."): ".$s_remote_request_url);
			error_log("postvars length (".$sync_id."): ".strlen($postvars));

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $s_remote_request_url);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $postvars);
			$ch_response = curl_exec($ch);
			
			error_log("ch_response (".$sync_id."): ".$ch_response);
			
			$error_info = curl_getinfo($ch);
  			$curl_errno = curl_errno($ch);
			
			//error_log("error_info: ".json_encode($error_info));
			//error_log("curl_errno: ".$curl_errno);
  
			echo ":: sync response: {$ch_response}";
			$rsp = explode("|",$ch_response);		
			if ($rsp[0] == "1") {

				if (count($rsp) > 1) {

					$idlist = explode(",", $rsp[1]);
					for ($n = 0; $n < count($idlist); $n++) {

						$rowid = trim($idlist[$n]);
						if (is_numeric(str_replace("-", "", $rowid))) {
							
							lavu_query("UPDATE `orders` SET `lastsync` = NOW() WHERE `order_id` = '[1]' AND `location_id` = '[2]'", $rowid, $server_remote_locationid);
							echo "order ".$rowid." synced<br>";
						
						} else if (strstr($rowid, "Paid")) {
							
							$pio_internal_ids = $paid_in_out_internal_ids[$rowid];
							if (count($pio_internal_ids) > 0) {

								$internal_ids_str = implode("','", $pio_internal_ids);
								$mark_as_synced = lavu_query("UPDATE `cc_transactions` SET `process_data` = 'synced' WHERE `order_id` = '[1]' AND `internal_id` IN ('".$internal_ids_str."')", $rowid);
								if ($mark_as_synced) {

									error_log("marked ".count($pio_internal_ids)." ".$rowid." records as synced (".$sync_id.")");	

									$check_for_unsynced_pios = lavu_query("SELECT `id` FROM `cc_transactions WHERE `order_id` = '[1]' AND `location_id` = '[2]' AND `process_data` != 'synced'", $rowid, $server_remote_locationid);
									if (mysqli_num_rows($check_for_unsynced_pios) == 0) {
										
										lavu_query("UPDATE `orders` SET `lastsync` = NOW() WHERE `order_id` = '[1]' AND `location_id` = '[2]'", $rowid, $server_remote_locationid);						
									}

								} else {

									error_log("failed to mark ".$rowid." records as synced (".$sync_id."): ".mysql_error());	
								}
							}
						}
					}
				}
				if (count($rsp) > 2) {
					echo "<br>" . $rsp[2];
				}
				if (count($rsp) > 3) {
					if (substr($rsp[3], 0, 24) == "[_new_api_orders_start_]") {
						processNewApiOrders($server_remote_locationid, $ch_response);
					}
				}
				if(count($rsp) > 4) {
					if (substr($rsp[4], 0, 23) == "[_sync_down_txs_start_]") {
						processSyncedDownTransactions($ch_response);
					}
				}
			}
		}
	}
	
	function sendKnownRecentApiOrders() {
	
		global $location_info;
		global $server_remote_locationid;
		
		$order_list = array();
		$return_str = "";
		
		if (isset($location_info['lls_fetch_api_orders'])) {
			if ($location_info['lls_fetch_api_orders'] == "1") {
				$six_hours_ago = date("Y-m-d H:i:s", (time() - 21600));
				$six_hours_from_now = date("Y-m-d H:i:s", (time() + 21600));
				$order_query = lavu_query("SELECT `order_id` FROM `orders` WHERE (`order_id` LIKE '2-%' OR `order_id` LIKE '7-%') AND `opened` >= '".$six_hours_ago."' AND `opened` < '".$six_hours_from_now."'");
				if (mysqli_num_rows($order_query) > 0) {
					while ($info = mysqli_fetch_assoc($order_query)) {
						$order_list[] = $info['order_id'];
					}
				}
				$return_str = "&known_api_orders=".join(",", $order_list);
			}
		}
		
		return $return_str;		
	}
	
	function processNewApiOrders($loc_id, $full_response_string) {
	
		global $Conn;
		
		$new_api_order_data = get_special_tag_contents("new_api_orders", $full_response_string);
		
		$log_file = fopen("../logs/new_api_orders_test.txt", "w");
		fputs($log_file, $new_api_order_data);
		fclose($log_file);
		
		$new_api_order_ids = explode(",", get_special_tag_contents("api_order_ids", $new_api_order_data));
		error_log("new_api_order_ids: ".print_r($new_api_order_ids, true));
			
		$orders_cols = explode("[_col_]", get_special_tag_contents("orders_cols", $new_api_order_data));
		$order_contents_cols = explode("[_col_]",get_special_tag_contents("order_contents_cols", $new_api_order_data));
		$modifiers_used_cols = explode("[_col_]",get_special_tag_contents("modifiers_used_cols", $new_api_order_data));
		$transactions_cols = explode("[_col_]",get_special_tag_contents("transactions_cols", $new_api_order_data));
		$split_checks_cols = explode("[_col_]",get_special_tag_contents("split_checks_cols", $new_api_order_data));
		$alt_payments_cols = explode("[_col_]",get_special_tag_contents("alt_payments_cols", $new_api_order_data));
		
		$orders_str = "'".implode("','", $new_api_order_ids)."'";
		$reset_orders = lavu_query("DELETE FROM `orders` WHERE `location_id` = '[1]' AND `order_id` IN (".$orders_str.")", $loc_id);
		$reset_order_contents = lavu_query("DELETE FROM `order_contents` WHERE `loc_id` = '[1]' AND `order_id` IN (".$orders_str.")", $loc_id);
		$reset_modifiers_used = lavu_query("DELETE FROM `modifiers_used` WHERE `loc_id` = '[1]' AND `order_id` IN (".$orders_str.")", $loc_id);
		$reset_transactions = lavu_query("DELETE FROM `cc_transactions` WHERE `loc_id` = '[1]' AND `order_id` IN (".$orders_str.")", $loc_id);
		$reset_split_checks = lavu_query("DELETE FROM `split_check_details` WHERE `loc_id` = '[1]' AND `order_id` IN (".$orders_str.")", $loc_id);
		$reset_alt_payments = lavu_query("DELETE FROM `alternate_payment_totals` WHERE `loc_id` = '[1]' AND `order_id` IN (".$orders_str.")", $loc_id);
		
		$orders_rows = explode("[_row_]", get_special_tag_contents("orders_vals", $new_api_order_data));
		$order_contents_rows = explode("[_row_]", get_special_tag_contents("order_contents_vals", $new_api_order_data));
		$modifiers_used_rows = explode("[_row_]", get_special_tag_contents("modifiers_used_vals", $new_api_order_data));
		$transactions_rows = explode("[_row_]", get_special_tag_contents("transactions_vals", $new_api_order_data));
		$split_checks_rows = explode("[_row_]", get_special_tag_contents("split_checks_vals", $new_api_order_data));
		$alt_payments_rows = explode("[_row_]", get_special_tag_contents("alt_payments_vals", $new_api_order_data));

		error_log("orders_cols: (".count($orders_cols).") ".print_r($orders_cols, true));
		error_log("orders_rows: (".count($orders_rows).") ".print_r($orders_rows, true));
	
		$success = true;
		foreach ($orders_rows as $orders_vals_str) {
			$orders_vals = explode("[_col_]", $orders_vals_str);
			$query_str = create_insert_query_str("orders", $orders_cols, $orders_vals, $Conn, false);
			error_log($query_str);
			$success = lavu_query($query_str);
			if (!$success) break;
		}
		
		if ($success) {

			error_log("successfully wrote new api order records");
			
			$success = true;
			foreach ($order_contents_rows as $order_contents_vals_str) {
				if ($order_contents_vals_str != "") {
					$order_contents_vals = explode("[_col_]", $order_contents_vals_str);
					$query_str = create_insert_query_str("order_contents", $order_contents_cols, $order_contents_vals, $Conn, false);
					$success = lavu_query($query_str);
					if (!$success) break;
				}
			}
			if (!$success) error_log("failed to write to order_contents for new api orders: ".mysql_error());
			
			$success = true;
			foreach ($modifiers_used_rows as $modifiers_used_vals_str) {
				if ($modifiers_used_vals_str != "") {
					$modifiers_used_vals = explode("[_col_]", $modifiers_used_vals_str);
					$query_str = create_insert_query_str("modifiers_used", $modifiers_used_cols, $modifiers_used_vals, $Conn, false);
					$success = lavu_query($query_str);
					if (!$success) break;
				}
			}
			if (!$success) error_log("failed to write to modifiers_used for new api orders: ".mysql_error());
			
			$success = true;
			foreach ($transactions_rows as $transactions_vals_str) {
				if ($transactions_vals_str != "") {
					$transactions_vals = explode("[_col_]", $transactions_vals_str);
					$query_str = create_insert_query_str("cc_transactions", $transactions_cols, $transactions_vals, $Conn, false);
					$success = lavu_query($query_str);
					if (!$success) break;
				}
			}
			if (!$success) error_log("failed to write to cc_transactions for new api orders: ".mysql_error());
		
			$success = true;
			foreach ($split_checks_rows as $split_checks_vals_str) {
				if ($split_checks_vals_str != "") {
					$split_checks_vals = explode("[_col_]", $split_checks_vals_str);
					$query_str = create_insert_query_str("split_check_details", $split_checks_cols, $split_checks_vals, $Conn, false);
					$success = lavu_query($query_str);
					if (!$success) break;
				}
			}
			if (!$success) error_log("failed to write to split_check_details for new api orders: ".mysql_error());
		
			$success = true;
			foreach ($alt_payments_rows as $alt_payments_vals_str) {
				if ($alt_payments_vals_str != "") {
					$alt_payments_vals = explode("[_col_]", $alt_payments_vals_str);
					$query_str = create_insert_query_str("alternate_payment_totals", $alt_payments_cols, $alt_payments_vals, $Conn, false);
					$success = lavu_query($query_str);
					if (!$success) break;
				}
			}
			if (!$success) error_log("failed to write to alternate_payment_totals for new api orders: ".mysql_error());
		
		} else error_log("failed to write to orders table for new api orders: ".mysql_error());
	}
	
	function processSyncedDownTransactions($full_response_string) {
		
		global $Conn;
		
		$tx_data = get_special_tag_contents("sync_down_txs", $full_response_string);
		$tx_cols = explode("[_col_]", get_special_tag_contents("sync_down_txs_cols", $tx_data));
		$tx_rows = explode("[_row_]", get_special_tag_contents("sync_down_txs_vals", $tx_data));

		$update_ioids = array();
		
		foreach ($tx_rows as $tx_row_str) {

			$tx_row = explode("[_col_]", $tx_row_str);
			$internal_id = $tx_row[$id_index];
			
			$ioid = "";
			$internal_id = "";
			$update_list = "";
			for ($i = 0; $i < count($tx_cols); $i++) {
				$col = $tx_cols[$i];
				if ($col == "ioid") $ioid = $tx_row[$i];
				if ($col == "internal_id") $internal_id = $tx_row[$i];
				else if ($col != "id") {
					if (!empty($update_list)) $update_list .= ", ";
					$update_list .= "`".$col."` = '".mysqli_real_escape_string($tx_row[$i], $Conn)."'";
				}
			}

			if (!empty($internal_id) && !empty($update_list)) {
				$update_record = lavu_query("UPDATE `cc_transactions` SET ".$update_list." WHERE `internal_id` = '[1]'", $internal_id);
				if ($update_record) {
					$affected_rows = mysql_affected_rows();
					if (!empty($ioid)) {
						$did_update = ($affected_rows > 0);
						if (!$did_update) {
							$tx_vals = explode("[_col_]", $tx_row_str);
							$query_str = create_insert_query_str("cc_transactions", $tx_cols, $tx_vals, $Conn, false);
							error_log("insert query: ".$query_str);
							$did_update = lavu_query($query_str);
							if (!$did_update) error_log("failed to insert new synced down transaction record: ".mysql_error());
						}
						if ($did_update && !in_array($ioid, $update_ioids)) $update_ioids[] = $ioid;
					}
				} else error_log("failed to update transaction: ".mysql_error());
			}
		}
		
		if (count($update_ioids) > 0) {
			$update_order_last_mod = lavu_query("UPDATE `orders` SET `last_modified` = '[1]', `last_mod_ts` = '[2]', `pushed_ts` = '[2]', `last_mod_device` = 'LLS' WHERE `ioid` IN ('".implode("','", $update_ioids)."')", date("Y-m-d H:i:s"), time());
			if (!$update_order_last_mod) {
				error_log("failed to update orders.last_modified, .last_mod_ts, and .pushed_ts: ".mysql_error());
			}
		}
	}
	
	function get_special_tag_contents($tag, $data)
	{
		$start_tag = "[_".$tag."_start_]";
		$end_tag = "[_".$tag."_end_]";
		if (strpos($data, $start_tag)!==false && strpos($data, $end_tag)!==false){
			$start_pos = strpos($data, $start_tag) + strlen($start_tag);
			$end_pos = strpos($data, $end_tag);
			$content_length = $end_pos - $start_pos;
			return substr($data, $start_pos, $content_length);
		}
		else return "";
	}
	
	function create_insert_col_list($cols, $include_id=true)
	{
		$clist = "";
		$icount = 0;
		for($i=0; $i<count($cols); $i++){
			if($cols[$i]!="id" || $include_id){
				if($icount > 0){
					$clist .= ",";
				}
				$clist .= "`".$cols[$i]."`";
				$icount++;
			}
		}
		return $clist;
	}
	
	function create_insert_val_list($vals, $db_conn, $cols=false, $include_id=true)
	{
		$vlist = "";
		$icount = 0;
		for($i=0; $i<count($vals); $i++)
		{
			if($cols[$i]!="id" || $include_id)
			{
				if($icount > 0)
					$vlist .= ",";
				$vlist .= "'".mysqli_real_escape_string($vals[$i], $db_conn)."'";
				$icount++;
			}
		}
		return $vlist;
	}

	function create_insert_query_str($tablename, $cols, $vals, $db_conn, $include_id=true)
	{
		$col_list = create_insert_col_list($cols, $include_id);
		$val_list = create_insert_val_list($vals, $db_conn, $cols, $include_id);
		return "insert into `$tablename` ($col_list) values ($val_list)";
	}
	
	function run_local_query($query)
	{
	}
	
	function get_api_response($data="")
	{
		global $server_remote_dataname;
		global $server_remote_api_key;
		global $server_remote_api_token;
		global $s_remote_request_url;
		
		$postvars = "dataname=".$server_remote_dataname;
		$postvars .= "&key=".$server_remote_api_key;
		$postvars .= "&token=".$server_remote_api_token;
		$postvars .= get_version_numbers_as_post();
		
		if($data!="")
			$postvars .= "&" . $data;
			
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $s_remote_request_url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postvars);
		$ch_response = curl_exec($ch);
		
		return $ch_response;
	}
	
	function get_general_table_data()
	{
		global $server_mysql_database;
		$db_info = get_database_info($server_mysql_database);
		
		return "table_count=".$db_info['table_count'] . "&field_count=".$db_info['field_count'] . "&field_sync_count=".$db_info['field_sync_count']."&table_name_list=".urlencode($db_info['table_name_list']);
	}
	
	function transmit_local_file($filename,$file_contents)
	{
		get_api_response("cmd=transmit_local_file&filename=".urlencode($filename)."&data=".urlencode($file_contents));
	}
	
	function transmit_query_result($command,$response)
	{
		get_api_response("cmd=transmit_query_result&query_command=".urlencode($command)."&query_result=".urlencode($response));
	}

	function transmit_post_result($label,$response)
	{
		get_api_response("cmd=transmit_post_result&query_label=".urlencode($label)."&query_result=".urlencode($response));
	}
	
	function transmit_local_file_structure()
	{
		$basedir = str_replace("/local","",dirname(__FILE__));
		
		$read_structure = crawl_dir_structure($basedir);
		$read_tabledata = get_general_table_data();
		
		get_api_response("cmd=transmit_local_file_structure&tabledata=".urlencode($read_tabledata)."&data=".urlencode($read_structure));
		//echo str_replace("\n","<br>",$read_structure);
	}
	
	function crawl_dir_structure($dir,$level=0)
	{
		$str = "";
		if($level > 16) return $str;
		
		$fcount = 0;
		$dp = opendir($dir);
		while($fname = readdir($dp))
		{
			$fcount++;
			if($fcount < 200) // max 200 files per part
			{
				$right_dot = strrpos($fname,".");
				if($right_dot)
					$ext = substr($fname,$right_dot + 1);
				else $ext = "";
				
				if($fname!="." && $fname!=".." && $fname!="")
				{
					$filename = $dir . "/" . $fname;
					//for($i=0; $i<$level; $i++)
					//	$str .= "\t";
					$str .= $filename;
					if(is_file($filename))
					{
						$userinfo = posix_getpwuid(fileowner($filename));
						$str .= " | " . filesize($filename) . " | " . date("Y-m-d H:i:s",filemtime($filename)) . " | " . $userinfo['name'];
					}
					$str .= "\n";
					if(is_dir($filename) && $ext!="app" && $fname!=".git" && $fname!="signatures" && $fname!="dev" && $fname!="logs")
					{
						$str .= crawl_dir_structure($filename,$level + 1);
					}
				}
			}
		}
		return $str;
	}
	
	function remove_local_file($local_filename_list)
	{
		$local_filename_list = explode(",",$local_filename_list);
		
		for($i=0; $i<count($local_filename_list); $i++)
		{
			$local_filename = $local_filename_list[$i];		
			$full_local_filename = dirname(__FILE__) . "/" . $local_filename;
			
			echo "Removing $full_local_filename ....";
			unlink($full_local_filename);
		}
	}
	
	function copy_remote_file($remote_filename_list,$local_filename_list)
	{
		$remote_filename_list = explode(",",$remote_filename_list);
		$local_filename_list = explode(",",$local_filename_list);
		
		for($i=0; $i<count($remote_filename_list); $i++)
		{
			$remote_filename = trim($remote_filename_list[$i]);
			$local_filename = trim($local_filename_list[$i]);
			
			if($remote_filename!="" && $local_filename!="")
			{
				$response = get_api_response("cmd=get_repo_file&filename=" . $remote_filename);
				
				if(substr($response,0,23)=="<!--download success-->")
				{
					$response = substr($response,23);
					
					$full_local_filename = dirname(__FILE__) . "/" . $local_filename;
					echo "Writing to $full_local_filename ....";
					/*$fp = fopen($full_local_filename,"w");
					fwrite($fp, $response);
					fclose($fp);
					
					chmod($full_local_filename, 0755);*/
					prepare_file_for_writing($full_local_filename,$response);
				}
			}
		}
	}
	
	function get_tag_contents($tag,$str)
	{
		$contents = array();
		$str_parts = explode("<".$tag.">",$str); // look for start tag
		for($i=1; $i<count($str_parts); $i++)
		{
			$current_parts = explode("</".$tag.">",$str_parts[$i]); // look for end tag
			if(count($current_parts) > 1) // full tag found, get contents
			{
				$contents[] = $current_parts[0];
			}
		}
		return $contents;
	}
	
	function get_tag_value($tag,$str)
	{
		$contents = get_tag_contents($tag,$str);
		if(count($contents) > 0)
		{
			return $contents[0];
		}
		else return "";
	}
	
	function get_database_info($dbname)
	{
		$table_list = array();
		$field_list = array();
		$table_count = 0;
		$field_count = 0;
		$field_sync_count = 0;
		$table_name_list = "";
		
		// creates array
		// dbinfo['tables'] = table_list
		// dbinfo['fields'] = field_list[tablename][0] = array of field names
		//                                         [1][fieldname] = associative array of field info
	
		$table_query = lavu_query("SHOW tables from $dbname");
		while($table_read = mysqli_fetch_row($table_query))
		{
			$tablename = $table_read[0];
			if($table_name_list!="") $table_name_list .= ","; $table_name_list .= $tablename;
			$table_count++;
			$table_list[] = $tablename;
			$field_list[$tablename] = array(array(),array());
			$table_field_count = 0;
			$table_field_sync_count = 0;
			$field_query = lavu_query("SHOW columns from `[1]`.`[2]`",$dbname,$tablename);
			while($field_read = mysqli_fetch_row($field_query))
			{
				$fieldname = $field_read[0];
				$field_count++;
				$table_field_count++;
				if($fieldname=="lastsync" || $fieldname=="lastmod")
				{
					$field_sync_count++;
					$table_field_sync_count++;
				}
				
				$field_info = array();
				$field_info['name'] = $fieldname;
				$field_info['type'] = $field_read[1];
				$field_info['null'] = $field_read[2];
				$field_info['key'] = $field_read[3];
				$field_info['default'] = $field_read[4];
				$field_info['extra'] = $field_read[5];
				
				$field_list[$tablename][0][] = $fieldname;
				$field_list[$tablename][1][$fieldname] = $field_info;
			}
			$table_name_list .= " - " . $table_field_count . " - " . $table_field_sync_count;
		}
		
		$dbinfo = array();
		$dbinfo['tables'] = $table_list;
		$dbinfo['fields'] = $field_list;
		$dbinfo['table_count'] = $table_count;
		$dbinfo['field_count'] = $field_count;
		$dbinfo['field_sync_count'] = $field_sync_count;
		$dbinfo['table_name_list'] = $table_name_list;
		return $dbinfo;
	}
	
	function run_lsvpn()
	{
		ini_set("max_execution_time","3000"); // 50 minutes max
	
		$postvars = "";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "http://localhost/local/lsvpn.php");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postvars);
		$response = curl_exec($ch);
		
		echo $response;
	}

	function run_schedule_package_level_sync() {

		global $s_remote_request_url;
		global $server_remote_dataname;

		$postvars = array("dataname"=>$server_remote_dataname, "cmd"=>"schedule_package_level_sync", "doquit"=>"1");
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
		curl_setopt($ch, CURLOPT_MAXREDIRS, 10 );
		curl_setopt($ch, CURLOPT_URL, $s_remote_request_url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postvars);
		$response = curl_exec($ch);
		$error = curl_error($ch);

		echo $response;

		if (strpos($response, "success") == 0) {
			$query = lavu_query("SELECT * FROM `config` WHERE `type`='location_config_setting' AND `setting`='package_last_sync_time'");
			if ($query === FALSE || mysqli_num_rows($query) == 0) {
				lavu_query("INSERT INTO `config` (`type`,`setting`,`value`) VALUES ('location_config_setting','package_last_sync_time','[time]')", array('time'=>date("Y-m-d H:i:s")));
			} else {
				lavu_query("UPDATE `config` SET `value`='[time]' WHERE `type`='location_config_setting' AND `setting`='package_last_sync_time'", array('time'=>date("Y-m-d H:i:s")));
			}
		}
	}
	
	function sync_local_database_structure()
	{
		global $server_remote_dataname;
		global $server_remote_api_key;
		global $server_remote_api_token;
		global $server_component_type;
		global $s_remote_request_url;
		
		$postvars = "dataname=".$server_remote_dataname;
		$postvars .= "&key=".$server_remote_api_key;
		$postvars .= "&token=".$server_remote_api_token;

		$postvars .= "&cmd=database_structure";
		$postvars .= get_version_numbers_as_post();
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $s_remote_request_url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postvars);
		$ch_response = curl_exec($ch);

		$remote_table_list = array();
		$remote_field_list = array();
		
		$table_count = 0;
		$field_count = 0;
		//---------------------------------- Go through remote table data and put it in same format as get_database_info() -----------------------------//
		$table_tags = get_tag_contents("db_table",$ch_response);
		for($t=0; $t<count($table_tags); $t++)
		{
			$table_name = get_tag_value("db_tablename",$table_tags[$t]);
			$table_fields = get_tag_contents("db_field",$table_tags[$t]);
			$table_count++;
			
			if(count($table_name) > 0)
			{
				$set_field_names = array();
				$set_field_info = array();
				for($f=0; $f<count($table_fields); $f++)
				{
					$field_name = get_tag_value("db_fieldname",$table_fields[$f]);
					$field_type = get_tag_value("db_fieldtype",$table_fields[$f]);
					$field_null = get_tag_value("db_fieldnull",$table_fields[$f]);
					$field_key = get_tag_value("db_fieldkey",$table_fields[$f]);
					$field_default = get_tag_value("db_field_default",$table_fields[$f]);
					$field_extra = get_tag_value("db_fieldextra",$table_fields[$f]);
					
					if(count($field_name) > 0)
					{
						$set_field_names[] = $field_name;
						$set_field_info[$field_name] = array("name"=>$field_name,"type"=>$field_type,"null"=>$field_null,"key"=>$field_key,"default"=>$field_default,"extra"=>$field_extra);
						$field_count++;
					}
				}
				
				$remote_table_list[] = $table_name;
				$remote_field_list[$table_name] = array($set_field_names,$set_field_info);
			}		
		}
		$source_db_info = array();
		$source_db_info['tables'] = $remote_table_list;
		$source_db_info['fields'] = $remote_field_list;
		$source_db_info['table_count'] = $table_count;
		$source_db_info['field_count'] = $field_count;
		sync_database_structure_to($source_db_info);
		
		$sync_tables = get_tables_to_sync( $server_component_type );
				
		setup_tables_for_syncing(array_merge(array("orders"),$sync_tables), false);
		
	}
	
	function sync_database_structure_to($source_db_info)
	{
		global $server_mysql_database;
		$company_db_name = $server_mysql_database;
		
		$make_changes = true;
		$dev_tables = $source_db_info['tables'];
		$dev_fields = $source_db_info['fields'];
		
		$company_db_info = get_database_info($company_db_name);
		$company_tables = $company_db_info['tables'];
		$company_fields = $company_db_info['fields'];
		
		//echo "<br>dev tables: " . count($dev_tables) . "<br>company tables: " . count($company_tables) . "<br>dev fields: " . $source_db_info['field_count'] . "<br>company fields: " . $company_db_info['field_count'];
		//--------------------------------------- Compare remote database to local database -----------------------------------------//
		for($i=0; $i<count($dev_tables); $i++)
		{
			$tablename = $dev_tables[$i];
			$process_table = true;//false;
			/*for($n=0; $n<count($core_tables); $n++)
			{
				if($core_tables[$n]==$tablename)
					$process_table = true;
			}*/
			
			if($process_table)
			{
				//echo "<br>" . $tablename;
				if(!isset($company_fields[$tablename])) // create new table
				{
					$fieldlist = $dev_fields[$tablename][0];
					$fields = $dev_fields[$tablename][1];

					//CREATE TABLE `poslavu_DEV_db`.`test_table` (
					//`t1` VARCHAR( 255 ) NOT NULL ,
					//`t2` INT NOT NULL ,
					//`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY 
					//) ENGINE = MYISAM //Should be INNODB
					
					$tcode = "";
					for($n=0; $n<count($fieldlist); $n++)
					{
						$fieldname = $fieldlist[$n];
						$fieldtype = $fields[$fieldname]['type'];
						$fieldnull = $fields[$fieldname]['null'];
						$fieldkey = $fields[$fieldname]['key'];
						if(trim($fieldnull)!="") $notnull = true; else $notnull = false;
						if(trim(strtolower($fieldkey))=="pri") $usekey = true; else $usekey = false;

						if($tcode!="") $tcode .= ", ";
						$tcode .= "`$fieldname` $fieldtype";
						if($notnull) $tcode .= " NOT NULL";
						if($usekey) $tcode .= " AUTO_INCREMENT PRIMARY KEY";
					}
					$query = "CREATE TABLE `$company_db_name`.`$tablename` ($tcode) ENGINE = INNODB";
					if($make_changes)
					{
						$str = "<br><font color='blue'>Executing:</font> $query";
						$success = mlavu_query($query);
						if($success) $str .= " <b><font color='green'>(Success)</font></b>";
						else $str .= " <b><font color='red'>(Failed)</font></b>";
						$str .= "<br>";
						echo $str;
						$log .= $str;
					}
					else echo "<br><br>" . $query;
				}
				else
				{
					$fieldlist = $dev_fields[$tablename][0];
					$fields = $dev_fields[$tablename][1];
					
					for($n=0; $n<count($fieldlist); $n++)
					{
						$fieldname = $fieldlist[$n];
						$fieldtype = $fields[$fieldname]['type'];
						if(!isset($company_fields[$tablename][1][$fieldname])) // create new column
						{
							$query = "ALTER TABLE `$company_db_name`.`$tablename` ADD `$fieldname` $fieldtype NOT NULL";
							if($make_changes)
							{
								$str = "<br><font color='blue'>Executing:</font> $query";
								$success = mlavu_query($query);
								if($success) $str .= " <b><font color='green'>(Success)</font></b>";
								else $str .= " <b><font color='red'>(Failed)</font></b>";
								$str .= "<br>";
								echo $str;
								$log .= $str;
							}
							else echo "<br><br>" . $query;
						}
						else
						{
							// check column type
							$company_fieldtype = $company_fields[$tablename][1][$fieldname]['type'];
							if($fieldtype!=$company_fieldtype)
							{
								$query = "ALTER TABLE `$company_db_name`.`$tablename` CHANGE `$fieldname` `$fieldname` $fieldtype NOT NULL";
								
								if($make_changes)
								{
									$str = "<br><font color='blue'>Executing:</font> $query";
									$success = mlavu_query($query);
									if($success) $str .= " <b><font color='green'>(Success)</font></b>";
									else $str .= " <b><font color='red'>(Failed)</font></b>";
									$str .= "<br>";
									echo $str;
									$log .= $str;
								}
								else echo "<br><br>" . $query;
							}
						}
					}
				}
			}
		}
	}
			
	function sync_remote_to_local($mode,$tname,$rows="")
	{
		global $server_remote_dataname;
		global $server_remote_api_key;
		global $server_remote_api_token;
		global $s_remote_request_url;
		
		$row_vals = array();
		if($mode=="condition")
		{
			$condition = $rows;
			$rows = "";
		}
		else if($mode=="partial")
		{
			$row_parts = explode(",",$rows);
			
			for($i=0; $i<count($row_parts); $i++)
			{
				$row_part = explode(":",$row_parts[$i]);
				if(count($row_part) > 1)
				{
					$set_rowid = trim($row_part[0]);
					$set_rowval = trim($row_part[1]);
					$row_vals[$set_rowid] = $set_rowval;
					//echo "set " . $set_rowid . " to " . $row_vals[$set_rowid] . " - " . $set_rowval;
				}
			}
			$lastid = (isset($row_vals['lastid']))?$row_vals['lastid']:0;
			$step = (isset($row_vals['step']))?$row_vals['step']:100;
			$min_order_id = (isset($row_vals['min_order_id']))?$row_vals['min_order_id']:"";
			$rows = "";
			
		} 	
		
		$postvars = "dataname=".$server_remote_dataname;
		$postvars .= "&key=".$server_remote_api_key;
		$postvars .= "&token=".$server_remote_api_token;
		
		$postvars .= "&cmd=sync_down";
		$postvars .= "&table=".$tname;
		$postvars .= "&mode=".$mode;
		if($mode=="condition")
			$postvars .= "&condition=".urlencode($condition);
		else if($mode=="partial")
			$postvars .= "&step=".$step."&lastid=".$lastid."&min_order_id=".$min_order_id;
		else if($mode=="rows" || $rows!="")
			$postvars .= "&rows=".$rows;
			
		//$postvars .= "&colnames=".urlencode($col_names);
		//$postvars .= "&colvals=".urlencode($col_vals);
		//$postvars .= "&rowid=".$rowid;
		$postvars .= get_version_numbers_as_post();
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $s_remote_request_url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postvars);
		$ch_response = curl_exec($ch);

		$return_response = array();
		if(strpos($ch_response,"[_response_info_]")!==false)
		{
			$ch_response = explode("[_response_info_]",$ch_response);
			$ch_info = $ch_response[0];
			$ch_response = $ch_response[1];
			
			$ch_info = explode("&",$ch_info);
			for($n=0; $n<count($ch_info); $n++)
			{
				$ch_part = explode("=",$ch_info[$n]);
				if(count($ch_part) > 1)
					$return_response[$ch_part[0]] = trim($ch_part[1]);
			}
		}
	
		$rows = explode("[_row_]",$ch_response);
		if(count($rows) > 1)
		{
			$colrow = $rows[0];
			$cols = array();
			$idlist = array();
			$using_rowids = false;
			$colnames = explode("[_sep_]",$colrow);
			for($i=0; $i<count($colnames); $i++)
			{
				$cols[] = $colnames[$i];
			}
			for($i=1; $i<count($rows); $i++)
			{
				$insert_names = "";
				$insert_values = "";
				$update_code = "";
				$rowid = 0;
				$vals = explode("[_sep_]",$rows[$i]);
				$query_vars = array();
				for($n=0; $n<count($vals); $n++)
				{
					$value = $vals[$n];
					$colname = $cols[$n];
					if($colname=="id") 
					{
						$rowid = $value;
						$using_rowids = true;
						$idlist["row_" . $rowid] = "found";
					}
					if($insert_names!="") $insert_names .= ",";
					if($insert_values!="") $insert_values .= ",";
					if($update_code!="") $update_code .= ",";
					$insert_names .= "`$colname`";
					$insert_values .= "'[$n]'";
					$update_code .= "`$colname`='[$n]'";
					$query_vars[$n] = $value;
					
				}

				$after_inserts = array();

				if($rowid)
				{
					$query_vars['id'] = $rowid;
					$exist_query = lavu_query("select * from `$tname` where `id`='[1]'",$rowid);
					if(mysqli_num_rows($exist_query))
					{
						echo "updating $tname<br>";
						$exists_record = mysqli_fetch_assoc( $exist_query );

						if( $tname == "config" && $exists_record['type'] == "local_setting"){
							$after_inserts[] = $exists_record;
						}
						lavu_query("update `$tname` set $update_code where `id`='[id]'",$query_vars);
					}
					else
					{
						$progressStatement = "Syncing $rowid for table: $tname <br>";
						echo $progressStatement;
						//echo "inserting into $tname<br>";
						lavu_query("insert into `$tname` ($insert_names) values ($insert_values)",$query_vars);
					}
				}
				{

					foreach($after_inserts as $insert_arr){
						unset($insert_arr['id']);
						$insert_fields = implode(",", array_map(wrapWithBackticks, array_keys($insert_arr)));
						$insert_values = implode(",", array_map(wrapWithSingleQuotes, array_keys($insert_arr)));

						lavu_query("insert into `$tname` ($insert_fields) values ($insert_values)",$insert_arr);
					}
				}
			}
			if($mode=="entire_table" && $using_rowids)
			{
				$list_query = lavu_query("select * from `$tname`");
				while($list_read = mysqli_fetch_assoc($list_query))
				{
					$rowid = $list_read['id'];
					if(isset($idlist["row_" . $rowid]) || ($tname == "config" && $list_read['type'] == "local_setting") )
					{
					}
					else
					{
						echo "Row not found in remote database: " . $rowid . "<br>";
						lavu_query("delete from `$tname` where `id`='[1]'",$rowid);
					}
				}
			}
		}
		return $return_response;
	}
	
	function get_tables_to_sync($server_component_type = '') {
		
		// Additional tables to sync (this array of tables do NOT include orders,order_contents, etc)
		////For customer management adding tables: (don't need:lk_waivers, i.e. syncs down), med_customers, med_action_log, 
		if(isset($server_component_type) && $server_component_type=="kart")
		{
			$sync_tables = array("clock_punches","action_log","edit_after_send_log","send_log","devices","ingredients","ingredient_usage",
									"cash_data","menu_items","lk_events","lk_customers","lk_event_schedules","lk_laps",
									"lk_group_events","lk_parts","lk_karts","lk_service","lk_categories","lk_service_types","lk_service_complete",
									"lk_service_info","lk_parts_received","med_customers","med_action_log");
		}
		else
		{
			$sync_tables = array("clock_punches","action_log","edit_after_send_log","send_log","devices",
										"ingredients","ingredient_usage","cash_data","menu_items","med_customers","med_action_log");
		}
		
		return $sync_tables;
	}

	function get_order_id_month_ago() {
		
		global $server_remote_dataname;
		global $server_remote_api_key;
		global $server_remote_api_token;
		global $s_remote_request_url;
		
		// credentials for api
		$postvars = "dataname=".$server_remote_dataname;
		$postvars .= "&key=".$server_remote_api_key;
		$postvars .= "&token=".$server_remote_api_token;
		// cmd to execute
		$postvars .= "&cmd=get_order_id_month_ago";
		$postvars .= get_version_numbers_as_post();
		
		// init response
		$response = "0";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $s_remote_request_url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postvars);
		$response = curl_exec($ch);
	
	
		return trim($response);		
	}
	
	function show_debug_box($str)
	{
		return "<textarea rows='25' cols='140'>" . str_replace("<","&lt;",str_replace("&","&amp;",$str)) . "</textarea>";
	}
	
	if(!$in_setup_area)
	{
		ob_start();
		require_once(dirname(__FILE__) . "/../lib/info_inc.php");
		global $server_remote_id;
		$delay = (isset( $server_remote_id ) ? $server_remote_id : rand(1, 100)) % 100;
		$delay = floor(($delay/100) * 60);
		//sleep( $delay );
		$start_time = round(microtime(true) * 1000, 2);
		
		if(isset($_GET['remote_control']))
		{
			set_remote_control_mode($_GET['remote_control']);
		}
		
		$tls_info = get_last_sync_data();
		$tls = $tls_info['time_since'];
		$tls_mgmt = $tls_info['time_since_management'];			
		
		if($tls_mgmt <= 598)
		{
			$remote_management = true;
			$local_sync_interval = sync_interval_setting(1);
		}
		else if(isset($_GET['runnow']))
		{
			$remote_management = true;
			$local_sync_interval = sync_interval_setting(0);
		}
		else if($tls_mgmt > 598)
		{
			$remote_management = false;
			$local_sync_interval = sync_interval_setting(0);
		}
		
		if($tls <= 58 && !$remote_management)
		{
			//echo "last sync time: " . $tls;
			//echo "<br>last mgmt time: " . $tls_mgmt;
			exit();
		}
		else
		{

			set_last_sync_time($tls_info);
		}

		// sync the package level down from the server
		if ($b_package_should_be_synced) {
			run_schedule_package_level_sync();
		}
		
		/*-------------- seperating point ------------------*/
		
		$sync_tables = get_tables_to_sync($server_component_type);
		
		if(isset($_GET['spec']) && $_GET['spec']=="test")
		{
			require_once(dirname(__FILE__) . "/health_check.php");
			
			
			$pMode = "records_check";
			$parts = array();
			$parts[] = $pMode;
			$parts[] = "extra_command";
			$parts[] = "download_item_images|698,699,enchilada.jpg,iced mocha.jpg|";
			$debug_call_stack[] = 'run_other_sync_mode'; 
			run_other_sync_mode($pMode,$parts);
			
			
			//echo "<br><br>Records Check<br><br>" . perform_records_check();
			//sync_local_database_structure();
			//copy_remote_file("fwd_script.php","fwd_script.php");
			//transmit_local_file_structure();
			//echo "general table data: " . get_general_table_data();
		}
		else if(isset($_GET['setup_tables']) && $_GET['setup_tables']=="1")
		{
			if(isset($_GET['autosync'])){
				$auto_sync_records = $_GET['autosync'];
			} else
				$auto_sync_records = false;
			echo "setting up tables for syncing . . . . . .<br><br>";
			$debug_call_stack[] = 'setup_tables_for_syncing';
			setup_tables_for_syncing(array_merge(array("orders"),$sync_tables), $auto_sync_records);
			
			if(isset($_GET['setup_kart']))
			{
				$debug_call_stack[] = 'setup_kart';
				$create_local_print_jobs_query = "CREATE TABLE `local_print_jobs` (`datetime` VARCHAR( 255 ) NOT NULL, `type` VARCHAR( 255 ) NOT NULL, `name` VARCHAR( 255 ) NOT NULL, `data` TEXT NOT NULL, `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY) ENGINE = INNODB";
				lavu_query($create_local_print_jobs_query);
			}
		}
		else if (isset($_GET['run_lsvpn']))
		{
			$debug_call_stack[] = 'run_lsvpn';
			run_lsvpn();
		}
		else if (isset($_GET['setup_indexing']))
		{
			$debug_call_stack[] = 'setup_indexing';
			require_once(dirname(__FILE__) . "/health_check.php");		
			echo "Indexing:<br><br>" . setup_indexing();
		}
		else
		{
			$debug_call_stack[] = 'find_table_to_sync';
			find_table_to_sync();
			$debug_call_stack[] = 'sync_recent_orders_to_remote';
			sync_recent_orders_to_remote();
			$debug_call_stack[] = 'sync_recent_records';
			sync_recent_records($sync_tables);
		}
		
		$end_time = round(microtime(true) * 1000,2);
		$run_time = round($end_time - $start_time,2);
		error_log('sync_counter: '.$tls_info['sync_counter'] . ". Performed: " . implode(", ", $debug_call_stack) .  ", took {$run_time}ms",0);
		// every 10th sync perform a record check
		if ( $tls_info['sync_counter'] % 10 == 0 ) { // consider changing interval to 15?
			require_once(dirname(__FILE__) . "/health_check.php");
			perform_records_check();			
		}
		
		$ob_output = ob_get_contents();
		ob_end_clean();
		
		if ($file_write_str != "") {
			echo $file_write_str;
		}
		echo $ob_output;
	}
?>