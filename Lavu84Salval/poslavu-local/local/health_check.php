<?php
	

    function updateOrInsertForRow($table, $conditionArr, $row){
        $whereClause = 'WHERE ';
        foreach($conditionArr as $key => $val){
            $whereClause .= "`".lavu_query_encode($key)."`='".lavu_query_encode($val)."' AND ";
        }
        $whereClause = substr($whereClause, 0, -5);//Remove last ' AND '
        
        $existsResult = lavu_query("SELECT * FROM `[1]` $whereClause", $table);
        if(mysqli_num_rows($existsResult)){//Then we update the rows
            $queryStr = 'UPDATE `[1]` SET ';
            foreach($row as $key => $val){
                $queryStr .= "`".lavu_query_encode($key)."`='".lavu_query_encode($val)."',";
            }
            $queryStr = substr($queryStr, 0, -1);
            $queryStr .= $whereClause;
            $result = lavu_query($queryStr, $table);
            if(!$result){ error_log("MYSQL error -aef3- in ".__FILE__." mysql error:".mysql_error()); }
        }else{//Then we need to insert the row
            $queryStr = 'INSERT INTO [table_name_xxx] (';
            $columns  = '(';
            $values   = '(';
            foreach($row as $key => $val){
                $columns .= "`".lavu_query_encode($key)."`,";
                $values  .= "'".lavu_query_encode($val)."',";
            }
            if(strlen($columns) > 1){ $columns = substr($columns, 0, -1); }
            if(strlen($values)  > 1){ $values  = substr($values,  0, -1); }
            $columns .= ')';
            $values  .= ')';
            $fullQuery = 'INSERT INTO `[1]` '.$columns.' VALUES '.$values;
            $result = lavu_query($fullQuery, $table);
            if(!$result){ error_log("MYSQL error -l7r6- in ".__FILE__." mysql error:".mysql_error()); }
        }
    }
	function get_response_from_local($try_url,$postvars="")
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $try_url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postvars);
		$ch_response = curl_exec($ch);
		
		return $ch_response;
	}
	
	function read_file($file, $lines) {
		//global $fsize;
		$handle = fopen($file, "r");
		$linecounter = $lines;
		$pos = -2;
		$beginning = false;
		$text = array();
		while ($linecounter > 0) {
			$t = " ";
			while ($t != "\n") {
				if(fseek($handle, $pos, SEEK_END) == -1) {
					$beginning = true; 
					break; 
				}
				$t = fgetc($handle);
				$pos --;
			}
			$linecounter --;
			if ($beginning) {
				rewind($handle);
			}
			$text[$lines-$linecounter-1] = fgets($handle);
			if ($beginning) break;
		}
		fclose ($handle);
		return array_reverse($text);
	}
	 
	function read_end_of_httpd_log($linecount=10,$ommit_date_until="")
	{
		$fname = "/var/log/apache2/error_log";
		
		$fsize = round(filesize($fname)/1024/1024,2);
		 
		$str = "";
		$exact_line_encountered = false;
		$lines = read_file($fname, $linecount);
		foreach ($lines as $line) {
			if($ommit_date_until=="")
				$str .= $line;
			else
			{
				$linestr = explode("[error]",$line);
				if(count($linestr) > 1)
				{
					if(convert_httpd_datestamp_to_datestr($linestr[0]) > $ommit_date_until['datetime'])
						$str .= $line;
					else if(convert_httpd_datestamp_to_datestr($linestr[0])==$ommit_date_until['datetime'])
					{
						if($exact_line_encountered==true)
							$str .= $line;
						else if(trim($ommit_date_until['lastline'])==trim($line))
						{
							$exact_line_encountered = true;
						}
					}
				}
			}
		}
		
		return $str;
	}

	function convert_httpd_datestamp_to_datestr($datestamp)
	{
		$date_parts = explode(" ",trim($datestamp));
		if(count($date_parts) > 4)
		{
			$month = $date_parts[1];
			$day = $date_parts[2];
			if($day * 1 < 10) $day = "0" . ($day * 1);
			$time = $date_parts[3];
			$year = trim(str_replace("]","",$date_parts[4]));
			
			$months = array("jan"=>"01","feb"=>"02","mar"=>"03","apr"=>"04","may"=>"05","jun"=>"06","jul"=>"07","aug"=>"08","sep"=>"09","oct"=>"10","nov"=>"11","dec"=>"12");
			return $year . "-" . $months[strtolower($month)] . "-" . $day . " " . $time;
		}
		else return "";
	}
				
	function get_last_datestamp_from_httpd_log()
	{
		$lastdate = "";
		$lastline = "";
		
		$str = read_end_of_httpd_log(20);
		$lines = explode("\n",$str);
		for($i=0; $i<count($lines); $i++)
		{
			$line_data = explode("[error]",$lines[$i]);
			if(count($line_data) > 1)
			{					
				$datestr = convert_httpd_datestamp_to_datestr($line_data[0]);
				if($datestr!="")
				{
					$lastdate = $datestr;
					$lastline = $lines[$i];
				}
			}
		}
		return array("datetime"=>$lastdate,"lastline"=>$lastline);
	}
	
	function test_local_post($test_command,$test_url,$postvars="")
	{
		$ommit_date_until = get_last_datestamp_from_httpd_log();
		
		//$test_command = "Index File";
		//$test_url = "http://localhost/index.php";
		//$postvars = "";
		$post_response = get_response_from_local($test_url,$postvars);
		$error_log = read_end_of_httpd_log(20,$ommit_date_until);

		$keyvals = array("Command"=>$test_command,"URL"=>$test_url,"Post"=>$postvars,"Response"=>$post_response,"Error Log"=>$error_log);
		
		$response = "";
		$response .= "<table>";
		$response .= "<tr><td colspan='3' style='border-bottom:solid 2px black' align='left'><table cellspacing=0 cellpadding=0><tr><td width='120'>&nbsp;</td><td>".$test_command."</td></tr></table></td></tr>";
		foreach($keyvals as $key => $val)
		{
			$response .= "<tr><td align='right' bgcolor='#dedede' style='padding-right:8px' class='qtable'>".$key."</td><td width='10'>&nbsp;</td><td width='800' class='qtable'>" . str_replace("\n","<br>",$val) . "</td></tr>";
		}
		$response .= "</table>";
		
		return $response;
	}
		
	function perform_health_check()
	{		
		$response = "";
		$response .= "<style>";
		$response .= ".qtable { font-family:Verdana, Arial; font-size:10px; } ";
		$response .= "</style>";
		
		global $server_remote_dataname;
		global $server_remote_cckey;
		global $server_remote_locationid;
		
		$test_cc = $server_remote_dataname . "_key_" . $server_remote_cckey;
		$test_locid = $server_remote_locationid;
		$response .= test_local_post("Index","http://localhost/index.php","");
		$response .= test_local_post("Hello","http://localhost/hello.php","");
		$response .= test_local_post("Open Orders","http://localhost/lib/json_connect.php?m=7&usejcinc=7&cc=$test_cc&loc_id=$test_locid&page=1&filter=","");
		$response .= test_local_post("Punch Status","http://localhost/lib/json_connect.php?m=punch_status&usejcinc=7&cc=$test_cc&loc_id=$test_locid&server_id=1");
		
		$error_log_str = read_end_of_httpd_log(250);
		$response .= "<table>";
		$response .= "<tr><td colspan='3' style='border-bottom:solid 2px black' align='left'><table cellspacing=0 cellpadding=0><tr><td width='120'>&nbsp;</td><td>Error Log</td></tr></table></td></tr>";
		$response .= "<tr><td align='right' bgcolor='#dedede' style='padding-right:8px' class='qtable'>&nbsp;</td><td width='10'>&nbsp;</td><td width='800' class='qtable'>" . str_replace("\n","<br>",$error_log_str) . "</td></tr>";
		$response .= "</table>";
		//$response .= test_local_post("ABC","http://localhost/abc.php","");
		
		transmit_query_result("Health Check",$response);
		
		return $response;
	}
	
	function lavu_index_exists($tname,$iname)
	{
		$index_found = false;
		$ix_query = lavu_query("show indexes from `[1]`",$tname);
		while($ix_read = mysqli_fetch_assoc($ix_query))
		{
			if($ix_read['Column_name']==$iname) $index_found = true;
		}
		return $index_found;
	}
	
	function setup_indexing()
	{
		$ilist = array();
		$list[] = array("orders: order_id, opened, closed");
		$ilist[] = array("order_contents: order_id");
		$ilist[] = array("cc_transactions","order_id");
		
		$ilist[] = array("action_log");
		
		$lklist[] = array();
		$lklist[] = array("lk_events");
		$lklist[] = array("lk_event_schedules");
		$lklist[] = array("lk_laps");
		
		$lklist[] = array("lk_customers");
		$lklist[] = array("lk_action_log");
		
		return ".......";
	}
	
	function perform_records_check($days_back="default")
	{
		global $server_remote_dataname;
		global $server_remote_api_key;
		global $server_remote_api_token;
		
		// locationid
		global $server_remote_locationid;
		
		if($days_back=="default")
		{
			$rc_days_query = lavu_query("select * from `config` where `type`='location_config_setting' and `setting`='records_check_days_back' order by id desc limit 1");
			if(mysqli_num_rows($rc_days_query))
			{
				$rc_days_read = mysqli_fetch_assoc($rc_days_query);
				$days_back = $rc_days_read['value'];
				
				if(!is_numeric($days_back))
					$days_back = "60";
			}
			else
				$days_back = "60";
		}
		
		$postvars = "dataname=".$server_remote_dataname;
		$postvars .= "&key=".$server_remote_api_key;
		$postvars .= "&token=".$server_remote_api_token;
		$postvars .= "&cmd=records_check";
		
		$compare_str = "concat(`subtotal`,':',`total`,`cash_paid`,':',`card_paid`,':',`closed`,':',`reclosed_datetime`,':',`reopened_datetime`,':',`card_gratuity`)";
		$postvars .= "&compare=" . urlencode($compare_str);
		
		$get_min_date = date("Y-m-d H:i:s",mktime(date("H"),date("m"),0,date("m"),date("d")-$days_back,date("Y")));
		
		$str = "";
		$rec_query = lavu_query("SELECT $compare_str as `val`, `order_id` as `order_id` FROM `orders` WHERE  `location_id` = '[2]' AND (`opened`>='[1]' OR `closed`>='[1]' OR `reopened_datetime`>='[1]' OR `reclosed_datetime`>='[1]') ORDER BY id DESC",$get_min_date, $server_remote_locationid);
		while($rec_read = mysqli_fetch_assoc($rec_query))
		{
			if($str!="")
				$str .= "|";
			$str .= $rec_read['order_id'] . "^" . $rec_read['val'];
		}
		$postvars .= "&records=" . urlencode($str);
		
		global $s_remote_request_url;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $s_remote_request_url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postvars);
		$ch_response = curl_exec($ch);
		
		$response = explode("|",$ch_response);
		if($response[0]=="1" && count($response) > 1)
		{
			$no_matches = explode(",",$response[1]);
			for($n=0; $n<count($no_matches); $n++)
			{
				//echo "no match: " . $no_matches[$n] . "<br>";
				lavu_query("update `orders` set `lastsync`='1900-00-00 00:00:00' where `order_id`='[1]'",$no_matches[$n]);
			}
			
			$open_auths = $response[2];
			if (!empty($open_auths)) lavu_query("UPDATE `orders` SET `lastsync`='1900-00-00 00:00:00' WHERE `order_id` IN (".$open_auths.")");
		}
		
		//echo "count: " . mysqli_num_rows($rec_query) . "<br>";
		//echo "strlen: " . strlen($str) . "<br>";
		//echo "<br>response: " . $ch_response;
	}
	
	function run_other_sync_mode($mode,$parts)
	{
		global $server_remote_dataname;

		if($mode=="records_check") // Records Check Mode is used to piggy back other modes through
		{
			if(isset($parts[1]) && $parts[1]=="extra_command" && isset($parts[2])) 
			{
				$mode_parts = explode("|",$parts[2]);
				$mode = trim($mode_parts[0]);
				$value1 = (isset($mode_parts[1]))?trim($mode_parts[1]):"";
				$value2 = (isset($mode_parts[2]))?trim($mode_parts[2]):"";
			}
		} // End Records Check piggy back
		
		//-------------------------------------- Define Additional Modes -------------------------------//
		
		if($mode=="create_log_dir")
		{
			$crdc = "]"."{"."|"."}"."[";
			prepare_file_for_writing("/poslavu-local/logs",$crdc);
			transmit_local_file_structure();
		}
		else if($mode=="exec_command")
		{
    		$crdc = "]"."*"."|"."*"."[";
			prepare_file_for_writing($parts[1],$crdc);
			transmit_local_file_structure();	
		}
		else if($mode=="update_or_insert_query")
		{               
    		$jsonInfoStr=$parts[1];
            $jsonInfoArr=json_decode($jsonInfoStr,1);
    		if(!$jsonInfoArr){
    		    error_log("DESERIALIZATION OF JSON FAILED IN ".__FILE__);
        		return;
    		}
    		//updateOrInsertForRow($table, $conditionArr, $row)
    		foreach($jsonInfoArr as $currUpdateOrInsert){
        		$table = $currUpdateOrInsert['table'];
        		$conditionArr = $currUpdateOrInsert['update_if'];
        		$setArr = $currUpdateOrInsert['set'];
        		updateOrInsertForRow($table, $conditionArr, $setArr);
    		}
		}
		else if($mode=="create_directory")
		{
			$crdc = "]"."{"."|"."}"."[";
			prepare_file_for_writing("/poslavu-local/".$parts[1],$crdc);
			transmit_local_file_structure();	
		}
		else if($mode=="write_to_local_files")
		{
			$remote_filename = $parts[1];
			$local_filename = $parts[2];
			
			copy_remote_file($remote_filename,$local_filename);
			transmit_local_file_structure();
		}
		else if($mode=="download_item_images" || $mode=="download_category_images")
		{
		
		
			$incommingValues = explode(',', $value1);
			
			//echo "Incomming Values: " . print_r($incommingValues,true) . "<br><br>";
			
			$ids = array();
			$imageNames = array();
			
			//We iterate through the incommingValues and place them whether they are numeric or names.
			for($i=0;$i<count($incommingValues);$i++){
				if(is_numeric($incommingValues[$i])){
					$ids[] = $incommingValues[$i];
				}else{
					$imageNames[] = $incommingValues[$i];
				}
			}
			
			//echo "Images By Item IDs: " . print_r($ids, true);
			
			//ids and imageNames above will become combined into menuItemNamesArr.
			$menuItemNamesArr = array();
			
			//We append all the names by ID to the menuItemNamesArr
			if( count($ids) ){
				$queryCond = "";
				for($i=0;$i<count($ids);$i++){
					if($queryCond!="") $queryCond .= " OR ";
					$queryCond .= "id = '[" . ($i) . "]'";
				}
				$tablename = "menu_items";
				if (strpos($mode, "category")) $tablename = "menu_categories";
				$queryStatement = "SELECT `image` FROM `".$tablename."` WHERE ($queryCond) AND `image` != ''";
				$qResult = lavu_query($queryStatement, $ids);
				while($arr = mysqli_fetch_assoc($qResult)){
					$menuItemNamesArr[] = $arr['image'];
				}
			}
	 
	 		//echo "Images By Item IDs To Names: " . print_r($menuItemNamesArr, true);
	 
	 		//We append all the names by name to the menuItemNamesArr
	 		for($i=0;$i<count($imageNames);$i++){
	 			$menuItemNamesArr[] = $imageNames[$i];
	 		}
	 
	 		//echo "Final list of images to download: " . print_r($menuItemNamesArr);
	 
	  
			//If the destination directory does not exist we create it. 
			if(!is_dir("/poslavu-local/images/". $server_remote_dataname . '/items')){
				mkdir("/poslavu-local/images/". $server_remote_dataname . '/items', 0777);
			}
			if(!is_dir("/poslavu-local/images/". $server_remote_dataname . '/items/full/')){
				mkdir("/poslavu-local/images/". $server_remote_dataname . '/items/full/', 0777);
			}
			if(!is_dir("/poslavu-local/images/". $server_remote_dataname . '/items/main/')){
				mkdir("/poslavu-local/images/". $server_remote_dataname . '/items/main/', 0777);
			}

			if(!is_dir("/poslavu-local/images/". $server_remote_dataname . '/categories')){
				mkdir("/poslavu-local/images/". $server_remote_dataname . '/categories', 0777);
			}
			// no need for categories/full - category close ups not used on iPad
			if(!is_dir("/poslavu-local/images/". $server_remote_dataname . '/categories/main/')){
				mkdir("/poslavu-local/images/". $server_remote_dataname . '/categories/main/', 0777);
			}
			
			//Finally, we perform the download for each image.
			global $s_remote_request_url;
			$url_no_ht_parts = explode("/cp/",str_replace("https://","",str_replace("http://","",$s_remote_request_url)));
			$url_no_ht = trim($url_no_ht_parts[0]);
			
			$img_dirs = array();
			if (strpos($mode, "category")) {
				$img_dirs[] = "categories/main/";
			} else {
				$img_dirs[] = "items/full/";
				$img_dirs[] = "items/main/";
			}			
			
			for($i=0;$i<count($menuItemNamesArr);$i++){
	
				foreach ($img_dirs as $img_dir) {
	
					$sourceWebBase = $url_no_ht . '/images/' . $server_remote_dataname . '/' . $img_dir;
					$destinationPathBase = '/poslavu-local/images/' . $server_remote_dataname . '/' . $img_dir;
							
					$destination = $destinationPathBase . $menuItemNamesArr[$i]; //str_replace( $menuItemNamesArr[$i];
					$sourceURL = $sourceWebBase . str_replace(" ", '%20', $menuItemNamesArr[$i]);
								
					$ch = curl_init();
					curl_setopt($ch, CURLOPT_URL, $sourceURL);
					curl_setopt($ch, CURLOPT_HEADER, 0);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
					$imageContents = curl_exec($ch);
					$returnCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
					curl_close($ch);
					
					if($imageContents && $returnCode == 200){
						error_log('Code 200, writing to disk:' . $sourceURL);
						$fp = fopen($destination, "w");
						fwrite($fp, $imageContents);
						fclose($fp);
					}else{
						error_log("404 image:" . $sourceURL);
					}
				}
			}		
		}
		else if($mode=="index_tables"){
			$rdb = $value1;
			$index_table_list = explode(";", $value2);
			for($i=0; $i<count($index_table_list); $i++)
			{
				$tinfo = explode(":",$index_table_list[$i]);
				if(count($tinfo) > 1)
				{
					$tname = trim($tinfo[0]);
					$tfields = explode(",",$tinfo[1]);
					
					echo "<br>--------checking " . $rdb . " - " . $tname . "------";
		
					$tindexes = array();
					$ix_query = lavu_query("show indexes from `[1]`.`[2]`",$rdb,$tname);
					while($ix_read = mysqli_fetch_assoc($ix_query))
					{
						$tcolname = $ix_read['Column_name'];
						$tindexes[$tcolname] = true;
						echo "<br>found $tcolname";
					}
					
					for($n=0; $n<count($tfields); $n++)
					{
						$tcolname = trim($tfields[$n]);
						if(!isset($tindexes[$tcolname]))
						{
							echo "<br>adding index for $tcolname";
							$success = mlavu_query("alter table `[1]`.`[2]` ADD INDEX([3])",$rdb,$tname,$tcolname);
							if($success) echo "&nbsp;<font color='green'>success</font>"; else echo "&nbsp;<font color='red'>failed</font>";
						}
					}
				}
			}
		}
	}
?>