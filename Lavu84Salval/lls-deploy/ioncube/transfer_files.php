<?php

	date_default_timezone_set('America/Denver');

	// mkdir if not exists
	function verify_dir($dirname)
	{
		$dirname = dest_file($dirname);
		
		$dir_path = "";
		$dir_parts = explode("/",$dirname);
		for($i=0; $i<count($dir_parts)-1; $i++)
		{
			$dir_path .= $dir_parts[$i];
			if($dir_path!="" && !is_dir($dir_path) && !is_file($dir_path))
			{
				mkdir($dir_path);
				chmod($dir_path,0755);
				echo "Creating Directory: ". $dir_path . "\n";
			}
			$dir_path .= "/";
		}
	}
	
	function transfer_directory($dir,$target_dir,$depth=0)
	{
		if($depth > 20) return;
		if(!is_dir($target_dir))
		{
			echo "Creating Directory: " . $target_dir . "\n";
			mkdir($target_dir);
			chmod($target_dir,0755);
		}
		if(!is_dir($target_dir))
		{
			echo "Directory doesn't exist: " . $target_dir . "\n";
			return;
		}

		$files = scandir($dir);
		for($i=0; $i<count($files); $i++)
		{
			if($files[$i]!="." && $files[$i]!=".." && $files[$i]!="_notes" && $files[$i]!=".DS_Store")
			{
				if(is_dir($dir . $files[$i]))
				{
					transfer_directory($dir . $files[$i] . "/",$target_dir . $files[$i] . "/", $depth + 1);
				}
				else
				{
					//echo $dir . $files[$i] . "\n";
					process_transfer_file($files[$i],$dir,$target_dir);
				}
			}
		}
	}
	
	function source_file($fname)
	{
		return str_replace("(dev/)","dev/",$fname);
	}
	
	function dest_file($fname)
	{
		return str_replace("(dev/)","",$fname);
	}
	
	function process_transfer_file($file,$dir,$target_dir,$encrypted_dir,$localmode=false)
	{

		if(strrpos($file,"/"))
			$basename = substr($file,strrpos($file,"/") + 1);
		else $basename = $file;

		//echo "-----" . $file . "\n";
		if($basename=="_notes" || substr($basename,strlen($basename)-1,1)=="~")
		{
		}
		else if($basename=="*")
		{
			$files = scandir($dir . str_replace("*","",source_file($file)));
			for($i=0; $i<count($files); $i++)
			{
				if($files[$i]!="." && $files[$i]!=".." && $files[$i]!="_notes" && $files[$i]!=".DS_Store")
				{
					if(is_dir($dir . str_replace("*","",$file) . $files[$i]))
					{
						// ignore directory
					}
					else
					{
						//echo $dir . str_replace("*","",$file) . $files[$i] . "\n";
						process_transfer_file($files[$i],$dir . str_replace("*","",$file),$target_dir . str_replace("*","",$file),$encrypted_dir . str_replace("*","",$file),$localmode);
					}
				}
			}
		}
		else if(trim($basename!=""))
		{
			if(strrpos($basename,"."))
				$ext = substr($file,strrpos($file,".") + 1);
			else
				$ext = "";
			//echo "______" . $file . "\n";

			if($ext == "php" && $basename!="sync.php" && $basename!="results_sheet_template2.php" && $basename!="results_sheet_template.php")
			{
				$fname = $file;
				$fp = fopen(source_file($dir . $fname),"r");
				$fcontents = fread($fp,filesize(source_file($dir . $fname)));
				fclose($fp);
				
				$fcontents = str_replace("/home/poslavu/public_html/admin/","/poslavu-local/",$fcontents);
				$fcontents = str_replace("/home/poslavu/logs","/poslavu-local/logs",$fcontents);
				$fcontents = str_replace("<"."?\n","<"."?"."php\n",$fcontents);
				$fcontents = str_replace("lavu_close_all_dbs();","//lavu_close_all_dbs();", $fcontents);
				$fcontents = str_replace('require_once(dirname(__FILE__)."/../../cp/resources/implement_platinum3.php");', "\$b_implement_platinum3 = TRUE; \$b_implement_plat3_distro_both_licenses = TRUE;", $fcontents);
				$fcontents = str_replace('require_once(dirname(__FILE__)."/../cp/resources/implement_platinum3.php");', "\$b_implement_platinum3 = TRUE; \$b_implement_plat3_distro_both_licenses = TRUE;", $fcontents);
				$fcontents = str_replace('require_once(dirname(__FILE__)."/cp/resources/implement_platinum3.php");', "\$b_implement_platinum3 = TRUE; \$b_implement_plat3_distro_both_licenses = TRUE;", $fcontents);
				$fcontents = str_replace("sa_cp/billing/package_levels_object.php", "cp/objects/package_levels_object.php", $fcontents);

				if($basename=="lavuquery.php")
				{
					$fcontents = str_replace("0755);","0777);",$fcontents);
					$fcontents = str_replace("function lavu_connect_db(","function remote_lavu_connect_db(",$fcontents);
					$fcontents = str_replace("function mlavu_connect_db(","function remote_mlavu_connect_db(",$fcontents);
					$fcontents = str_replace("function mlavu_query(","function remote_mlavu_query(",$fcontents);
					$fcontents = str_replace("require_once(\"/home/poslavu/private_html/myinfo.php\");","// require_once(\"/home/poslavu/private_html/myinfo.php\");",$fcontents);
					$fcontents = str_replace("require_once(","//require_once(",$fcontents);
				}
				else if($basename=="core_functions.php")
				{
					$fcontents = str_replace("lavu_json_decode(","lavu_j_s_o_n_decode(",$fcontents);
					$fcontents = str_replace("json_decode(","remote_json_decode(",$fcontents);
					$fcontents = str_replace("if(!function_exists(\"json_decode\"))","if(!function_exists(\"remote_json_decode\"))",$fcontents);
					$fcontents = str_replace("lavu_j_s_o_n_decode(","lavu_json_decode(",$fcontents);
					$fcontents = str_replace("function register_connect(","function remote_register_connect(",$fcontents);
					$fcontents = str_replace("/home/poslavu/logs/company/","/poslavu-local/logs/company/",$fcontents);
					$fcontents = str_replace("function speak(","function remote_speak(",$fcontents);
				} 
				else if($basename=="deviceValidations.php")
				{
					$fcontents = str_replace("$"."reseller_admin = false;","return false; // no access to MAIN database",$fcontents);
				}
				else if($basename=="inapp_management.php")
				{
					$fcontents = str_replace("return json_decode(","return remote_json_decode(",$fcontents);
					$fcontents = str_replace("return lsecurity_name(","global "."$"."company_code; global "."$"."company_code_key; return "."$"."company_code.\"_key_\"."."$"."company_code_key; //lsecurity_name(",$fcontents);
				}				
				else if($basename=="json_connect.php")
				{
					$fcontents = str_replace("if ("."$"."company_code!=\"NEEDTOGET\")","if (1==2 && "."$"."company_code!=\"NEEDTOGET\")",$fcontents);
					//$fcontents = str_replace("if (lsecurity_id("."$"."company_code_key,"."$"."data_name)!="."$"."company_info['id'] && lsecurity_id1("."$"."company_code_key)!="."$"."company_info['id'])","if ("."$"."company_code_key != "."$"."server_remote_cckey)",$fcontents);
					
					//$fcontents = str_replace("if ((lsecurity_id("."$"."company_code_key,"."$"."data_name)!="."$"."cid && lsecurity_id1("."$"."company_code_key)!="."$"."cid) || empty("."$"."cid))","if ("."$"."company_code_key != "."$"."server_remote_cckey)",$fcontents);
					$fcontents = preg_replace("/if.*lsecurity_id.*lsecurity_id1.*{/", "if ("."$"."company_code_key != "."$"."server_remote_cckey){", $fcontents);
					$fcontents = preg_replace("/require_once.*RequestTimingLogger.*\n/",'//php transfer_files.php - commented out require for cp/objects/RequestTimingLogger.php',$fcontents);
				}
				else if(substr($basename,0,6)=="jc_inc")
				{
					$fcontents = str_replace("register_connect(","//register_connect(",$fcontents);
				}
				else if($basename=="gateway.php")
				{
					$fcontents = str_replace("if (lsecurity_id("."$"."company_code_key) == "."$"."company_info['id']) {","if ("."$"."company_code_key == "."$"."server_remote_cckey) { // replaced lsecurity_id()",$fcontents);
					$gbefore = "$"."res_query = mlavu_query(\"SELECT `id`, `company_name` FROM `poslavu_MAIN_db`.`restaurants`";
					$gafter = "$"."res_query = lavu_query(\"SELECT `id`, `title` as `company_name` FROM `locations` WHERE `id`='["."1"."]'\","."$"."server_remote_locationid);\n\t\t/"."/"."$"."res_query = mlavu_query(\"SELECT `id`, `company_name` FROM `poslavu_MAIN_db`.`restaurants`";
					$fcontents = str_replace($gbefore,$gafter,$fcontents);
				}
				else if($basename=="receipt_append_results.php")
				{
					$fcontents = preg_replace("/if.*lsecurity_id.*lsecurity_id1.*{/", "if ("."$"."company_code_key != "."$"."server_remote_cckey){", $fcontents);
				}
				else if($basename=="till_report.php")
				{
					$fcontents = str_replace("require_once(\"/poslavu-local/cp/resources/core_functions.php\");","require_once(dirname(__FILE__) . \"/info_inc.php\");\n//require_once(\"/home/poslavu/public_html/admin/cp/resources/core_functions.php\");",$fcontents);
					$fcontents = str_replace("mlavu_connect_db();","//mlavu_connect_db();",$fcontents);
					$fcontents = str_replace("mlavu_select_db(\"poslavu_","//mlavu_select_db(\"poslavu_",$fcontents);
				}
				else if($basename=="comconnect.php")
				{
					$fparts = explode("// sent by the app:",$fcontents);
					if(count($fparts) > 1)
					{
						$fcontents = "";
						$fcontents .= "<"."?"."php\n";
						$fcontents .= "\tsession_start();\n";
						$fcontents .= "\trequire_once(dirname(__FILE__) . \"/../lib/info_inc.php\");\n";
						$fcontents .= $fparts[1];
					}
				}
				else if($basename=="lkb.php")
				{
					$fcontents = str_replace("require_once(\"/poslavu-local/cp/resources/core_functions.php\");","require_once(\"/poslavu-local/lib/info_inc.php\");",$fcontents);
				}
				else if($basename=="info_inc.php")
				{
					$fcontents = "";
					$fcontents .= "<"."?"."php\n";
					$fcontents .= "require_once(\"/poslavu-local/cp/resources/core_functions.php\");\n";
					$fcontents .= "require_once(\"/poslavu-local/local/local_settings.php\");\n";
					$fcontents .= "$"."cc_required = true;\n";
					$fcontents .= "$"."company_code = (isset("."$"."_REQUEST['cc']))?"."$"."_REQUEST['cc']:\"\";\n";
					$fcontents .= "$"."ccrt = lsecurity_pass("."$"."company_code);\n";
					$fcontents .= "$"."company_code = "."$"."ccrt['cc'];\n";
					$fcontents .= "$"."company_code_key = "."$"."ccrt['key'];\n";
					$fcontents .= "?".">\n";
				}

				if($target_dir!="")
				{
					verify_dir($target_dir . $fname);
					verify_dir($encrypted_dir . $fname);
					
					$ftmp = dest_file($target_dir . $fname);
					echo "***** Writing PHP File: source to " . $ftmp . "\n";
					$fw = fopen($ftmp,"w");
					fwrite($fw,$fcontents);
					fclose($fw);
					chmod($ftmp,0755);

					$fpath = dest_file($encrypted_dir . $fname);
					echo "***** Encoding File: " . $ftmp . " to " . $fpath . "\n";
					//exec("./ioncube_encoder53 $ftmp -o $fpath");
					exec("./ioncube_encoder53 $ftmp -o $fpath");
				}
				else
				{
					verify_dir($encrypted_dir . $fname);
					
					$fpath = dest_file($encrypted_dir . $fname);
					chmod($fpath,0755);
					echo "***** Encoding File: " . $dir.$fname . " to " . $fpath . "\n";
					exec("./ioncube_encoder53 ".$dir.$fname." -o $fpath");
				}
			}
			else
			{
				if($target_dir!="")
				{
					verify_dir($target_dir . $file);
					echo "Copying File: source to " . dest_file($target_dir . $file) . "\n";
					copy(source_file($dir . $file),dest_file($target_dir . $file));
				}
				if($encrypted_dir!="")
				{
					verify_dir($encrypted_dir . $file);
					echo "Copying File: " . source_file($dir . $file) . " to " . dest_file($encrypted_dir . $file) . "\n";
					copy(source_file($dir . $file),dest_file($encrypted_dir . $file));
				}
				
				/*if(strpos($file,".js"))
				{
					echo "<br>" . $file;
					exit();
				}*/
			}
			//echo "ext: " . $ext . "\n\n";
		}
	}

	function transfer_files_from_list($list,$source_dir,$target_dir,$encrypted_dir)
	{
		$localmode = false;
		$list = explode("\n",$list);
		for($i=0; $i<count($list); $i++)
		{
			$file = trim($list[$i]);

			if(strpos($file,"- local -"))
			{
				$localmode = true;
			}
			else if($file!="")
			{
				if($localmode)
				{
					process_transfer_file($file,$target_dir,"",$encrypted_dir,$localmode);
				}
				else
				{
					process_transfer_file($file,$source_dir,$target_dir,$encrypted_dir,$localmode);
				}
			}
		}
	}
	
	//transfer_directory("/Users/corey/Documents/admin.poslavu.com/public_html/admin/","/poslavu-local/");
	
	$filename = dirname(__FILE__) . "/../transfer_list.txt";
	$fp = fopen($filename,"r");
	$transfer_list = fread($fp,filesize($filename));
	fclose($fp);
			
	transfer_files_from_list($transfer_list,"/poslavu-scripts/","/poslavu-local/","/poslavu-encoded/");
	
	$fp = fopen("/poslavu-local/lib/lls-main-timestamp", "w");
	fputs($fp, date("Y-m-d H:i:s"));
	fclose($fp);
	
	copy("/poslavu-local/lib/lls-main-timestamp", "/poslavu-encoded/lib/lls-main-timestamp");
?>
