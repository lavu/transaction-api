//
//  HostessTests.m
//  HostessTests
//
//  Created by Theodore Schnepper on 8/11/12.
//  Copyright (c) 2012 Theodore Schnepper. All rights reserved.
//

#import "HostessTests.h"

@implementation HostessTests

- (void)setUp
{
    [super setUp];
    
    // Set-up code here.
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}

- (void)testExample
{
    STFail(@"Unit tests are not implemented yet in HostessTests");
}

@end
