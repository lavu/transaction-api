//
//  HOSTLocationSelectionViewController.h
//  Hostess
//
//  Created by Theodore Schnepper on 1/14/13.
//  Copyright (c) 2013 Theodore Schnepper. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HOSTLavuPOST.h"
#import "HOSTLoginInformation.h"

@interface HOSTLocationSelectionViewController : UIViewController <UIPickerViewDataSource, UIPickerViewDelegate>
@property (weak, nonatomic) IBOutlet UIPickerView *LocationSelector;
@property (weak, nonatomic) IBOutlet UIButton *LocationSelectionButton;


- (IBAction) selectLocation: (id) sender withEvents: (UIEvent*) event;
-(IBAction) logout;

@end
