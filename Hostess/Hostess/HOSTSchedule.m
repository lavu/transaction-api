//
//  HOSTSchedule.m
//  Hostess
//
//  Created by Theodore Schnepper on 5/30/13.
//  Copyright (c) 2013 Theodore Schnepper. All rights reserved.
//

#import "HOSTSchedule.h"

@implementation HOSTSchedule

@synthesize schedules = _schedules;

- (id) init {
    _schedules = [[NSMutableArray alloc] init];
    return self;
}

- (void) update:(NSDictionary *)dict {
    if(!dict){
        return;
    }
    
    HOSTScheduleTime* sched = [[HOSTScheduleTime alloc] initWithDictionary: dict];
    if([_schedules containsObject: sched]){
        int index = [_schedules indexOfObject: sched];
        sched = [_schedules objectAtIndex: index];
        [sched updateWithDictionary: dict];
    } else {
        [_schedules addObject: sched];
    }
}

@end
