//
//  HOSTTableDetailViewController.m
//  Hostess
//
//  Created by Theodore Schnepper on 1/18/13.
//  Copyright (c) 2013 Theodore Schnepper. All rights reserved.
//

#import "HOSTTableDetailViewController.h"

@implementation HOSTTableDetailViewController

@synthesize table;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

typedef enum {
    TableDetailViewIcon = 1,
    TableDetailViewWaiters = 2,
    TableDetailViewTime = 3,
    TableDetailViewGuests = 4
} TableDetailViewTags;

- (void)viewDidLoad {
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    UIView* tableViewContainer = [[self mainView] viewWithTag: TableDetailViewIcon];
    [tableViewContainer setBackgroundColor: [UIColor clearColor]];
    
    HOSTTableView* tableView = [[HOSTTableView alloc] initWithTable: table];
    CGRect tableFrame = [tableViewContainer frame];
    tableFrame.origin.x = 0.0f;
    tableFrame.origin.y = 0.0f;
    [tableViewContainer addSubview: tableView];
    [tableView setFrame: tableFrame];
    [tableView setNeedsDisplay];
    
    UIScrollView* waitersViewContainer = (UIScrollView*)[[self mainView] viewWithTag: TableDetailViewWaiters];
    CGSize waiterViewContainerSize = [waitersViewContainer frame].size;
    float dim = waiterViewContainerSize.width;
    
    waiterViewContainerSize.height = MAX(waiterViewContainerSize.height, [table waiterCount] * dim);
    [waitersViewContainer setContentSize: waiterViewContainerSize];
    
    CGRect waiterFrame;
    waiterFrame.origin.x = 0.0f;
    waiterFrame.origin.y = 0.0f;
    waiterFrame.size.width = dim;
    waiterFrame.size.height = dim;
    for(float i = 0.0f; i < [table waiterCount]; i++) {
        waiterFrame.origin.y = i * dim;
        HOSTWaiter* waiter = [[table assignedWaiters] objectAtIndex: i];
        HOSTWaiterView* waiterView = [[HOSTWaiterView alloc] initWithWaiter: waiter];
        [waiterView setSelected: true];
        [waitersViewContainer addSubview: waiterView];
        [waiterView setFrame: waiterFrame];
        [waiterView setNeedsDisplay];
    }
    UILabel* timeLabel = (UILabel*)[[self mainView] viewWithTag: TableDetailViewTime];
    if(table.currentOrder) {
        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat: @"HH:mm:ss"];
        double duration = [[table currentOrder] currentOrderDuration];
        int hours = (duration / 60.0f) / 60.0f;
        duration -= ((float) hours) * 3600.f;
        int minutes = (duration / 60.0f);
        duration -= ((float) minutes) * 60.0f;
        int seconds = duration;
        
        NSString* str = [NSString stringWithFormat:@"%02d:%02d:%02d", hours, minutes, seconds];
        
        [timeLabel setText: str];
    } else {
        [timeLabel setText: @"No Order"];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setMainView:nil];
    [super viewDidUnload];
}
@end