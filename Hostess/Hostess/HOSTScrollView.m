//
//  HOSTScrollView.m
//  Hostess
//
//  Created by Theodore Schnepper on 8/23/12.
//  Copyright (c) 2012 Theodore Schnepper. All rights reserved.
//

#import "HOSTScrollView.h"

@implementation HOSTScrollView

@synthesize parent = _parent;
BOOL _locked;

- (BOOL) locked {
    return _locked;
}

- (void) setLocked:(BOOL)locked {
    _locked = locked;
    [self setScrollEnabled:!locked];
}

- (id) init {
    _parent = NULL;
    _locked = FALSE;
    
    return [super init];
}

- (id) initWithFrame:(CGRect)frame {
    _parent = NULL;
    _locked = FALSE;

    return [super initWithFrame:frame];
}

- (id) initWithCoder:(NSCoder *)aDecoder {
    _parent = NULL;
    _locked = FALSE;
    
    return [super initWithCoder:aDecoder];
}

UITouch* trackedTouch = NULL;

- (void) touchesBegan: (NSSet *) touches withEvent:(UIEvent *)event {
    if(_locked) {
        if(!trackedTouch) {
            trackedTouch = [touches anyObject];
        }
        [self sendTouch];
    }
    
    [super touchesBegan: touches withEvent: event];
}

- (void) touchesMoved: (NSSet *)touches withEvent:(UIEvent *)event {
    if(_locked && trackedTouch) {
        [self sendTouch];
    }
    
    [super touchesMoved:touches withEvent:event];
}

- (void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    if(_locked && trackedTouch) {
        if(_target && _selector) {
            [_target performSelector:_selector withObject:NULL withObject:NULL];
        }
        
        if(_parent) {
            [_parent clearPoints];
            [_parent setNeedsDisplay];
            trackedTouch = NULL;
        }
    }
    [super touchesEnded:touches withEvent:event];
}

- (void) touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesCancelled:touches withEvent:event];
}

- (void) sendTouch {
    if(!_parent) {
        return;
    }
    
    [_parent addTouchPoint: trackedTouch];
    [_parent setNeedsDisplay];
}

SEL _selector;
id _target;
- (void) setSelector: (SEL) selector target: (id) target {
    if(_selector){
        //free(_selector);
        _selector = Nil;
    }
    _target = target;
    _selector = selector;
}

@end