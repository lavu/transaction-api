//
//  HOSTWaiter.h
//  Hostess
//
//  Created by Theodore Schnepper on 8/12/12.
//  Copyright (c) 2012 Theodore Schnepper. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HOSTModel.h"
#import "HOSTTableInformation.h"
#import "HOSTTimeCard.h"
#import "HOSTSchedule.h"

@interface HOSTWaiter : HOSTModel
@property (nonatomic, assign) int eID;
@property (nonatomic, assign) int index;
@property (copy, nonatomic) NSString* fullName;
@property (copy, nonatomic) NSString* firstName;
@property (copy, nonatomic) NSString* lastName;
@property (nonatomic, copy, readonly) UIColor* color;
@property (nonatomic, copy) NSMutableArray* assignedTables;
@property (nonatomic, copy) HOSTTimeCard* timeCard;
@property (nonatomic, copy) HOSTSchedule* schedule;
@property (nonatomic, assign) UIView* view;

- (id) init;
- (id) initWithID: (int) eID firstName: (NSString*) fname lastName: (NSString*) lname;

- (BOOL) setColor: (UIColor*) color;
- (UIColor*) getColor;
- (void) toggleAssignment: (id) table;
- (BOOL) isHere;
- (void) updatePunches: (NSDictionary*) dict;
- (void) updateSchedules: (NSDictionary*) dict;
@end
