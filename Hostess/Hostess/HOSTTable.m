//
//  HOSTTable.m
//  Hostess
//
//  Created by Theodore Schnepper on 8/12/12.
//  Copyright (c) 2012 Theodore Schnepper. All rights reserved.
//

#import "HOSTTable.h"

@implementation HOSTTable

@synthesize tableName = _tableName;
@synthesize tableType = _tableType;
@synthesize loc = _loc;
@synthesize radians = _radians;
@synthesize assignedWaiters = _assignedWaiters;
@synthesize waiterCount = _waiterCount;
@synthesize image = _image;
@synthesize useVector = _useVector;
@synthesize state = _state;
@synthesize view = _view;
@synthesize currentOrder = _currentOrder;

+ (id) createTable:(int)type name:(NSString *)name xPos:(float)x yPos:(float)y width:(float)w height:(float)h radians:(float)r {
    HOSTTable* table = [[HOSTTable alloc] init];
    CGRect rect;
    rect.origin.x = x;
    rect.origin.y = y;
    rect.size.height = h;
    rect.size.width = w;
    [table setLoc:rect];
    [table setTableType:type];
    [table setTableName:name];
    [table setRadians:r];
    
    return table;
}

+ (int) tableType:(NSString *)shapeName {
    if([shapeName isEqual: @"circle"]) {
        return 0;
    }
    if([shapeName isEqual: @"square"]) {
        return 1;
    }
    if([shapeName isEqual: @"diamond"]) {
        return 2;
    }
    if([shapeName isEqual: @"slant_right"]) {
        return 3;
    }
    if([shapeName isEqual: @"slant_left"]) {
        return 4;
    }
    return 0;
}

- (id) init {
    _tableType = 0;
    _tableName = @"";
    _assignedWaiters = [[NSMutableArray alloc] init];
    _waiterCount = 0;
    _image = NULL;
    _useVector = true;
    _state = TableStateEmpty;
    
    return self;
}


- (void) toggleAssignment:(id) waiter {
    if(![self assignWaiter: waiter]) {
        [self removeWaiter: waiter];
    }
}
- (BOOL) assignWaiter:(id)waiter {
    for(id waiters in _assignedWaiters) {
        if(waiters == waiter) {
            return FALSE;
        }
    }
    
    [_assignedWaiters addObject: waiter];
    [_view setNeedsDisplay];
    return TRUE;
}

- (BOOL) removeWaiter:(id)waiter {
    for(id waiters in self.assignedWaiters) {
        if (waiters == waiter) {
            [_assignedWaiters removeObject: waiter];
            [_view setNeedsDisplay];
            return TRUE;
        }
    }
    
    return FALSE;
}

- (void) setState:(TableState)state {
    _state = state;
    [_view setNeedsDisplay];
}

- (int) waiterCount {
    return [_assignedWaiters count];
}

- (int) getWaiterCount {
    return [_assignedWaiters count];
}

- (void) setCurrentOrder:(HOSTOrder *)currentOrder {
    _currentOrder = currentOrder;
    if(_currentOrder) {
        [self setState: TableStateOccupied];
        if([_currentOrder checkHasPrinted]){
            [self setState: TableStateCheckPrinted];
        }
    } else {
        [self setState: TableStateEmpty];
    }
    
}

@end