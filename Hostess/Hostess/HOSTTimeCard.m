//
//  HOSTTimeCard.m
//  Hostess
//
//  Created by Theodore Schnepper on 8/21/12.
//  Copyright (c) 2012 Theodore Schnepper. All rights reserved.
//

#import "HOSTTimeCard.h"

@implementation HOSTTimeCard

@synthesize punches = _punches;

- (id) init {
    _punches = [[NSMutableArray alloc] init];
    return self;
}

- (BOOL) isWithinTime:(NSDate *)date {
    for(HOSTTimePunch* punch in _punches) {
        if([punch timeWithin:date])
            return TRUE;
    }
    return FALSE;
}

- (void) update: (NSDictionary*) dict {
    HOSTTimePunch* punch = [[HOSTTimePunch alloc] initWithDictionary:dict];
    
    if([_punches containsObject:punch]) {  //Update Punch
        
    } else {
        [_punches addObject: punch];
    }
}

@end