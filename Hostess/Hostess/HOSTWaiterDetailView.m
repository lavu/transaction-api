//
//  HOSTWaiterDetailView.m
//  Hostess
//
//  Created by Theodore Schnepper on 8/27/12.
//  Copyright (c) 2012 Theodore Schnepper. All rights reserved.
//

#import "HOSTWaiterDetailView.h"

@implementation HOSTWaiterDetailView

@synthesize waiter = _waiter;

- (id) initWithWaiter:(HOSTWaiter *)waiter {
    _waiter = waiter;
    return [self init];
}

+ (id) populateWithWaiter:(HOSTWaiter *)waiter parentView: (UIView*) parent {
    HOSTWaiterDetailView* view = [[HOSTWaiterDetailView alloc] initWithWaiter:waiter];
    
    CGRect rect;
    {
        CGRect frame = parent.frame;
        rect.size.width = 500.0f;
        rect.size.height = 500.0f;
    
        rect.origin.x = (frame.size.height - rect.size.width) / 2.0f;
        rect.origin.y = (frame.size.width - rect.size.height) / 2.0f;
        [view setFrame: rect];
        [view setBackgroundColor: [UIColor whiteColor]];
    
        rect.size.width = 50.0f;
        rect.size.height = 20.0f;
        rect.origin.x = 0.0f;
        rect.origin.y = 0.0f;
    }
    UIButton* button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [button setFrame:rect];
    [button setTitle:@"Exit" forState:UIControlStateNormal];
    
    [button addTarget:view action:@selector(kill) forControlEvents:UIControlEventTouchUpInside];
    [parent performSelectorOnMainThread:@selector(addSubview:) withObject:view waitUntilDone:TRUE];

    [view addSubview:button];
    
    {
        HOSTCollapsibleView* test = [[HOSTCollapsibleView alloc] init];
        CGRect frame = CGRectMake(10.0f, 20.0f, 200.0f, 460.0f);
        [test setFrame: frame];
        [test setBackgroundColor: [UIColor grayColor]];
        
        { // ASSIGNED COLOR
            UIView* assignedColor = [[UIView alloc] init];
            [assignedColor setAccessibilityLabel:@"Assigned Color"];
            [assignedColor setBackgroundColor:[waiter color]];
            [assignedColor setFrame: CGRectMake(0.0f, 0.0f, 20.0f, 20.0f)];
            
            UIButton* changeColorButton = [UIButton buttonWithType:UIButtonTypeInfoLight];
            [changeColorButton setFrame: CGRectMake(0.0f, 0.0f, 20.0f, 20.0f)];
            
            [assignedColor addSubview: changeColorButton];
            
            [test addSubview:assignedColor];
        }
        
        
        { // ASSIGNED TABLES SECTION
            UIView* assignedTables = [[UIView alloc] init];
            [assignedTables setAccessibilityLabel:@"Assigned Tables"];
            CGRect f;
            int tableCount = 0;
            for(HOSTTable* table in [waiter assignedTables]) {
                f = CGRectMake(0.0f, 25.0f * tableCount++, frame.size.width, 20.0f);
                UILabel* label = [[UILabel alloc] initWithFrame:f];
                [label setText: table.tableName];
                [label setAdjustsFontSizeToFitWidth:TRUE];
                
                [assignedTables addSubview:label];
            }
            f = CGRectMake(0.0f, 0.0f, frame.size.width, tableCount * 25.0f);
            [assignedTables setFrame:f];
            [test addSubview:assignedTables];
        }
        
        { //TIMECARD SECTION
            UIView* timepunches = [[UIView alloc] init];
            [timepunches setAccessibilityLabel:@"Time Card Punches"];
            
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"HH:mm:ss"];
            
            CGRect f;
            int punchCount = 0;
            HOSTTimeCard* timecard = [waiter timeCard];
            for(HOSTTimePunch* punch in timecard.punches) {
                f = CGRectMake(0.0f, 25.0f * punchCount++, frame.size.width, 20.f);
                UILabel* label = [[UILabel alloc] initWithFrame:f];
                [label setAdjustsFontSizeToFitWidth:TRUE];
                
                NSString* start = [dateFormatter stringFromDate:[punch startPunch]];
                NSString* end = [dateFormatter stringFromDate:[punch endPunch]];
                double total = [punch getTimeBetweenPunches] / (3600.0f);
                
                if(![punch hasEndingPunch]) {
                    [label setBackgroundColor:[UIColor greenColor]];
                    [label setText: [NSString stringWithFormat:@"%@ - %@ : %g", start, @"", total]];
                }
                else {
                    if([punch timeWithin:[NSDate date]]) {
                        [label setBackgroundColor:[UIColor greenColor]];
                    }
                    [label setText: [NSString stringWithFormat:@"%@ - %@ : %g", start, end, total]];
                }
                
                [timepunches addSubview: label];
            }
            
            f = CGRectMake(0.0f, 0.0f, frame.size.width, punchCount * 25.0f);
            [timepunches setFrame: f];
            
            [test addSubview: timepunches];
        }
        
        [view addSubview: test];
    }
    [view setNeedsDisplay];
    [parent setNeedsDisplay];
    
    return view;
}

BOOL killed = false;
- (BOOL)resignFirstResponder {
    if(!killed)
    [self kill];
    return [super resignFirstResponder];
}

- (void) kill {
    NSLog(@"Killed");
    killed = true;
    [self performSelectorOnMainThread:@selector(removeFromSuperview) withObject:NULL waitUntilDone:TRUE];
}

@end