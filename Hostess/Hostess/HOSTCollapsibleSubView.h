//
//  HOSTCollapsibleSubView.h
//  Hostess
//
//  Created by Theodore Schnepper on 8/28/12.
//  Copyright (c) 2012 Theodore Schnepper. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HOSTCollapsibleSubView : UIView {
    NSObject* _target;
    SEL _selector;
}

@property (nonatomic, assign) UIView* view;
@property (nonatomic, readonly) UIButton* header;

- (id) initWithView: (UIView*) view parentFrame: (CGRect) frame;

- (void) setToggleTarget: (id) target selector: (SEL) selector;
- (bool) isCollapsed;
- (bool) isExpanded;
- (void) collapse;
- (void) expand;
- (void) toggle;
- (void) recalc;

@end
