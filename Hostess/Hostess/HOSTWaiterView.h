//
//  HOSTWaiterView.h
//  Hostess
//
//  Created by Theodore Schnepper on 1/4/13.
//  Copyright (c) 2013 Theodore Schnepper. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HOSTWaiter.h"
#import "HOSTWaiterInformation.h"

@interface HOSTWaiterView : UIControl

@property (atomic) UIImage* backgroundImage;
@property (atomic) HOSTWaiter* waiter;
@property (atomic) UILabel* waiterName;

- (id) initWithWaiter: (HOSTWaiter*) waiter;


@end
