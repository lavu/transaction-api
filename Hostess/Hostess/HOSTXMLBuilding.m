//
//  HOSTXMLBuilding.m
//  Hostess
//
//  Created by Theodore Schnepper on 8/17/12.
//  Copyright (c) 2012 Theodore Schnepper. All rights reserved.
//

#import "HOSTXMLBuilding.h"

@implementation HOSTXMLBuilding

BOOL verb = false;

- (id) initWithVerbose:(BOOL)verbose {
    verb = verbose;
    return self;
}

- (void)parseXMLFromData:(NSData *)xml {
    errorParsing = FALSE;
    articles = [[NSMutableArray alloc] init];
    parser = [[NSXMLParser alloc] initWithData:xml];
    
    id me = self;
    [parser setDelegate: me];
    
    [parser setShouldProcessNamespaces: FALSE];
    [parser setShouldReportNamespacePrefixes: FALSE];
    [parser setShouldResolveExternalEntities: FALSE];
    
    [parser parse];
    
}

- (void)parserDidStartDocument:(NSXMLParser *)parser {
    if(verb) {
        NSLog(@"Parser Started.");
    }
}

- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError {
    NSString *errorString = [NSString stringWithFormat:@"Error code %i", [parseError code]];
    NSLog(@"Error parsing XML: %@: %@", errorString, [parseError domain]);
    
    if([parseError userInfo]) {
        NSEnumerator* enumerator = [[parseError userInfo] keyEnumerator];
        NSString* key;
        while(( key = [enumerator nextObject])) {
            NSLog(@"%@: %@.", key, [[parseError userInfo] objectForKey: key]);
        }
    }
    
    errorParsing=TRUE;
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
    if(verb) {
        NSLog(@"Element Start: Name: %@, namespace: %@, qName: %@", elementName, namespaceURI, qName);
    }
    currentElement = [elementName copy];
    elementValue = [[NSMutableString alloc] init];
    if([elementName isEqualToString:@"row"]) {
        item = [[NSMutableDictionary alloc] init];
    }
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    [elementValue appendString: string];
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    if(verb) {
        NSLog(@"Element End: Name: %@, namespace %@, qName: %@", elementName, namespaceURI, qName);
    }
    if([elementName isEqualToString:@"row"]) {
        [articles addObject: item];
    } else {
        [item setObject:elementValue forKey:elementName];
    }
}

- (void)parserDidEndDocument:(NSXMLParser *)parser {
    if(verb) {
        if(errorParsing) {
            NSLog(@"Error Encountered Parsing XML!");
        } else {
            if(verb) {
                NSLog(@"XML Was Parsed Successfully!");
            }
        }
    }
}

- (NSArray*) result {
    return articles;
}

@end