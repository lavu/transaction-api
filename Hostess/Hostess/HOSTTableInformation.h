//
//  HOSTTableInformation.h
//  Hostess
//
//  Created by Theodore Schnepper on 8/14/12.
//  Copyright (c) 2012 Theodore Schnepper. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    TableStateEmpty = 0,
    TableStateOccupied = 1,
    TableStateOrderred = 2,
    TableStateSentToKitchen = 3,
    TableStateCheckPrinted = 4
} TableState;

@interface HOSTTableInformation : NSObject
+ (NSString*) getTableImageName: (int) tableType;
+ (UIImage*) getTableImage: (int) tableType;
//+ (UIImage*) multImgByColor: (UIImage*) img colors: (NSArray*) colors;
+ (CGPoint*) getVectorForTable: (int) tableType;
+ (UIImage*) getStateImage: (TableState) state;
@end
