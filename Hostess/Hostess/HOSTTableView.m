//
//  HOSTTableView.m
//  Hostess
//
//  Created by Theodore Schnepper on 1/3/13.
//  Copyright (c) 2013 Theodore Schnepper. All rights reserved.
//

#import "HOSTTableView.h"

@implementation HOSTTableView

@synthesize backgroundImage = _backgroundImage;
@synthesize table = _table;
@synthesize label = _label;

- (id) init {
    self = [super init];
    _label = [[UILabel alloc] init];
    [_label setFrame:[self frame]];
    UIColor* color = [UIColor colorWithRed:0.0f green:0.0f blue:0.0f alpha:0.0f];
    [self setBackgroundColor: color];
    [_label setBackgroundColor: color];
    [self addSubview:_label];
    [_label setTextAlignment: NSTextAlignmentCenter];
    return self;
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    _label = [[UILabel alloc] init];
    if (self) {
        // Initialization code
    }
    ;

    return self;
}

- (id) initWithBackgroundImage: (UIImage*) image {
    self = [self init];
    [self setBackgroundImage:image];
    
    UIColor* color = [UIColor colorWithRed:0.0f green:0.0f blue:0.0f alpha:0.0f];
    [self setBackgroundColor: color];
    
    return self;
}

- (id) initWithTable: (HOSTTable*) table {
    self = [self init];
    [self setBackgroundImage: [HOSTTableInformation getTableImage:[table tableType]]];
    _table = table;
    [_label setText:[[table tableName] copy]];
    
    return self;
}

- (void) setFrame:(CGRect)frame {
    [super setFrame: frame];
    [_label setFrame: CGRectMake(0.0f, 0.0f, frame.size.width, frame.size.height)];
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    
    [super drawRect:rect];
    
    CGContextRef c = UIGraphicsGetCurrentContext();
    
    CGRect frame = rect;
    CGAffineTransform identity = CGAffineTransformIdentity;
    
    if(_table) {
        /**
         * MASKING FOR VECTORS
         */
        if([_table useVector]) {
            CGPoint* points = [HOSTTableInformation getVectorForTable:[_table tableType]];
            if(points) {
                for(int i = 0; i < 4; i++) {
                    points[i].x = (points[i].x * rect.size.width) + rect.origin.x;
                    points[i].y = (points[i].y * rect.size.height) + rect.origin.y;
                }
                
                CGMutablePathRef path = CGPathCreateMutable();
                CGPathAddLines(path, &identity, points, 4);
                CGContextAddPath(c, path);
                CGContextClosePath(c);
            } else {
                CGContextAddEllipseInRect(c, rect);
            }
            CGContextClip(c);
        }
        
        NSArray* waiters = [_table assignedWaiters];
        float width =  rect.size.width / ((float) [waiters count]);
        CGMutablePathRef outline = CGPathCreateMutable();
        { //Draw a White square, just in case we don't have any assignments
            CGPoint square[4] = {
                CGPointMake(rect.origin.x, rect.origin.y),
                CGPointMake(rect.origin.x + rect.size.width, rect.origin.y),
                CGPointMake(rect.origin.x + rect.size.width, rect.size.height + rect.origin.y),
                CGPointMake(rect.origin.x, frame.size.height + rect.origin.y),
            };
            UIColor* drawColor;
            if([_table state] == TableStateEmpty){
                drawColor = [UIColor grayColor];
            } else {
                drawColor = [UIColor whiteColor];
            }
            
            CGContextSetStrokeColorWithColor(c, [drawColor CGColor]);
            CGContextSetFillColorWithColor(c, [drawColor CGColor]);
            CGPathAddLines(outline,&identity, square, 4);
            CGContextSetLineWidth(c, 1.0f);
            CGContextAddPath(c, outline);
            CGContextClosePath(c);
            CGContextDrawPath(c, kCGPathFill);
        }
        
        /**
         * Draw the Waiter Colors as squares
         */
        for(int i = 0; i < [waiters count]; i++) {
            float fi = (float) i;
            CGMutablePathRef path = CGPathCreateMutable();
            CGPoint square[4] = {
                CGPointMake(rect.origin.x + (fi * width), rect.origin.y),
                CGPointMake(rect.origin.x + ((fi + 1.0f) * width), rect.origin.y),
                CGPointMake(rect.origin.x + ((fi + 1.0f) * width), rect.origin.y + frame.size.height),
                CGPointMake(rect.origin.x + (fi * width), rect.origin.y + frame.size.height),
            };
            
            HOSTWaiter* waiter = [waiters objectAtIndex:i];
            //[waiter getColor]
            UIColor* color = [[waiter getColor] copy];
            CGFloat hsi[4];
            [color getHue:&hsi[0] saturation:&hsi[1] brightness:&hsi[2] alpha:&hsi[3]];
            if([_table state] == TableStateEmpty){
                hsi[1] *= 0.25f;
            }
            UIColor* drawColor = [UIColor colorWithHue:hsi[0] saturation:hsi[1] brightness:hsi[2] alpha:hsi[3]];
            
            CGContextSetStrokeColorWithColor(c, [drawColor CGColor]);
            CGContextSetFillColorWithColor(c, [drawColor CGColor]);
            
            CGPathAddLines(path, &identity, square, 4);
            CGContextSetLineWidth(c, 1.0f);
            CGContextAddPath(c, path);
            CGContextClosePath(c);
            CGContextDrawPath(c, kCGPathFill);
        }
        
        
        /*if([self isHighlighted])
        { //Darken if Hovering over with a touch
            CGContextAddPath(c, outline);
            CGContextClosePath(c);
            CGContextSetBlendMode(c, kCGBlendModeMultiply);
            CGContextSetFillColorWithColor(c, [[UIColor grayColor] CGColor]);
            CGContextDrawPath(c, kCGPathFill);
        }*/
        
        
        if([_table useVector]) {//The outline
            CGPoint* points = [HOSTTableInformation getVectorForTable:[_table tableType]];
            if(points) {
                for(int i = 0; i < 4; i++) {
                    points[i].x = (points[i].x * rect.size.width) + rect.origin.x;
                    points[i].y = (points[i].y * rect.size.height) + rect.origin.y;
                }
                
                CGMutablePathRef path = CGPathCreateMutable();
                CGPathAddLines(path, &identity, points, 4);
                CGContextAddPath(c, path);
                CGContextClosePath(c);
            } else {
                CGContextAddEllipseInRect(c, rect);
            }
            
            CGContextSetStrokeColorWithColor(c, [[UIColor colorWithRed:1.0f green:1.0f blue:1.0f alpha:1.0f] CGColor]);
            CGContextSetFillColorWithColor(c, [[UIColor colorWithRed:0.0f green:0.0f blue:0.0f alpha:0.0f] CGColor]);
            CGContextSetLineWidth(c, 4.0f);
            CGContextDrawPath(c, kCGPathFillStroke);
        }
        
        if(_backgroundImage && ![_table useVector]) {
            CGContextTranslateCTM(c, 0, rect.size.height);
            CGContextScaleCTM(c, 1.0f, -1.0f);
            CGContextSetBlendMode(c, kCGBlendModeMultiply);
            CGContextDrawImage(c, rect, [_backgroundImage CGImage]);
            
            CGContextSetBlendMode(c, kCGBlendModeDestinationIn);
            CGContextDrawImage(c, rect, [_backgroundImage CGImage]);
        }
        
        if([_table state] != TableStateEmpty) { //State Image
            CGContextSetBlendMode(c, kCGBlendModeCopy);
            CGFloat width, height;
            width = height = 0.20 * frame.size.height;
            CGFloat left = ((frame.size.width - width) / 2.0f) + frame.origin.x;
            CGFloat top = frame.origin.y + (1.0 * height);
            CGRect status = CGRectMake( left, top, width, height);
            
            CGContextTranslateCTM(c, 0, frame.size.height);
            CGContextScaleCTM(c, 1.0f, -1.0f);
            
            UIImage* status_image = [HOSTTableInformation getStateImage: [_table state]];
            CGContextDrawImage(c, status, [status_image CGImage]);
        }
    }
}

@end
