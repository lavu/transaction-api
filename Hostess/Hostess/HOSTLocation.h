//
//  HOSTLocation.h
//  Hostess
//
//  Created by Theodore Schnepper on 8/13/12.
//  Copyright (c) 2012 Theodore Schnepper. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HOSTModel.h"
#import "HOSTXMLBuilding.h"
#import "HOSTLavuPOST.h"
#import "HOSTRoom.h"
#import "HOSTWaiter.h"
#import "HOSTTable.h"

@interface HOSTLocation : HOSTModel
@property (nonatomic, copy) NSMutableArray* roomList;
@property (nonatomic, copy) NSMutableArray* employees;
@property (nonatomic, copy) NSMutableDictionary* eidToEmployees;
@property (nonatomic, copy) NSMutableDictionary* tableList;
@property (nonatomic, copy) NSString* name;

- (id) init;
- (int) getRoomCount;
- (void) createEmployees;
- (void) updateClockPunches;
+ (id) createLocationName: (NSString*) name roomList: (NSArray*) rooms;
+ (id) createLocationFromArray: (NSArray*) array;
+ (id) createLocationFromLogin;
@end
