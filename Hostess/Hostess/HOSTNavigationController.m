//
//  HOSTNavigationController.m
//  Hostess
//
//  Created by Theodore Schnepper on 12/28/12.
//  Copyright (c) 2012 Theodore Schnepper. All rights reserved.
//

#import "HOSTNavigationController.h"

@interface HOSTNavigationController ()

@end

@implementation HOSTNavigationController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
