//
//  HOSTWaiterDetailViewController.h
//  Hostess
//
//  Created by Theodore Schnepper on 1/22/13.
//  Copyright (c) 2013 Theodore Schnepper. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HOSTWaiter.h"
#import "HOSTWaiterDetailView.h"
#import "HOSTTable.h"
#import "HOSTTableView.h"
#import "HOSTTableDetailView.h"
#import "HOSTWaiterView.h"
#import "HOSTTimecardViewController.h"

@interface HOSTWaiterDetailViewController : UIViewController
@property HOSTWaiter* waiter;
@property (strong, nonatomic) IBOutlet UIView *mainView;

- (void) setWaiter: (HOSTWaiter*) waiter;
- (IBAction)showTimeCard:(id)sender forEvent:(UIEvent *)event;
@end
