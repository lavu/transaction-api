//
//  HOSTViewControllerLoginScreen.m
//  Hostess
//
//  Created by Theodore Schnepper on 12/28/12.
//  Copyright (c) 2012 Theodore Schnepper. All rights reserved.
//

#import "HOSTViewControllerLoginScreen.h"

@implementation HOSTViewControllerLoginScreen

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setUserNameTextBox:nil];
    [self setPasswordTextBox:nil];
    [self setLoginButton:nil];
    [super viewDidUnload];
}

-(IBAction) tryLogin: (UIControl*) sender withEvents: (UIEvent*) events {
    if(![[[self userNameTextBox] text] isEqual: @""] || ![[[self passwordTextBox] text] isEqual: @""]) {
        NSMutableDictionary* arguments = [[NSMutableDictionary alloc] init];
        [arguments setObject:[NSString stringWithFormat:@"%i", CommInitialLogin] forKey:@"cmd"];
        [arguments setObject:[[self userNameTextBox] text] forKey:@"username"];
        [arguments setObject:[[self passwordTextBox] text] forKey:@"password"];
        id result = [HOSTLavuPOST json_decode:[HOSTLavuPOST queryURL:[HOSTLavuPOST getPOSTURL] withData:arguments]];
        if(result) {
            //Do Stuff with the result
            HOSTLoginInformation* loginInfo = [HOSTLavuPOST getLoginInformation];
            
            [loginInfo setLocations: [result objectForKey:@"locations"]];
            [loginInfo setUserDetails:[result objectForKey:@"user"]];
            [loginInfo setUserDetails:[result objectForKey:@"dataname"]];
            
            //HOSTLoginInformation* loginInfo = [HOSTLavuPOST getLoginInformation];
            NSArray* locations = [loginInfo locations];
            [loginInfo setActiveLocation:[NSString stringWithFormat:@"%@", [[locations objectAtIndex:0] objectForKey:@"id"]]];
            if(locations.count == 1) {
                //If we have only one location, then load that location
                NSMutableDictionary* arguments = [[NSMutableDictionary alloc] init];
                
                [arguments setObject: [NSNumber numberWithInt: CommSetLocation] forKey:@"cmd"];
                [arguments setObject: [[HOSTLavuPOST getLoginInformation] activeLocation] forKey:@"set_locationid"];
                
                id result = [HOSTLavuPOST json_decode:[HOSTLavuPOST queryURL:[HOSTLavuPOST getPOSTURL] withData:arguments]];
                NSLog(@"%@", result);
                
                [self performSegueWithIdentifier:@"SkipLocation" sender:sender];
            } else {
                //Next Screen
                [self  performSegueWithIdentifier:@"Login" sender:sender];
                //[[self navigationController] pushViewController:<#(UIViewController *)#> animated:<#(BOOL)#>];
            }
        }
    }
}

- (IBAction)edittingEnded:(id)sender {
    [self tryLogin: Nil withEvents: Nil];
}


- (void) viewWillAppear:(BOOL)animated {
    [self LoginButton].enabled = false;
    [_userNameTextBox setText:@""];
    [_passwordTextBox setText:@""];

    { //Ping
        NSMutableDictionary* arguments = [[NSMutableDictionary alloc] init];
        [arguments setObject:[NSString stringWithFormat:@"%i", CommPing] forKey:@"cmd"];
        
        id result = [HOSTLavuPOST json_decode:[HOSTLavuPOST queryURL:[HOSTLavuPOST getPOSTURL] withData:arguments]];
        if(result) {
            if([@"ping" isEqual: result]) { //Test
                [arguments setObject:[NSString stringWithFormat:@"%i", CommTest] forKey:@"cmd"];
                
                result = [HOSTLavuPOST json_decode:[HOSTLavuPOST queryURL:[HOSTLavuPOST getPOSTURL] withData:arguments]];
                if(result) {
                    if([result isKindOfClass:[NSDictionary class]]) {
                        NSDictionary* dictionaryResult = result;
                        bool good = true;
                        NSEnumerator* resultEnumerator = [arguments keyEnumerator];
                        id key;
                        while(key = [resultEnumerator nextObject]) {
                            id arg = [[NSString alloc] initWithFormat:@"%@", [arguments objectForKey:key]];
                            id res = [[NSString alloc] initWithFormat:@"%@", [dictionaryResult objectForKey:key]];
                            good = good && ([arg isEqual: res]);
                        }
                        
                        NSArray* array = [result objectForKey:@"test"];
                        for(int i = 0; i < [array count]; i++) {
                            id num = [[NSString alloc] initWithFormat:@"%d", i+1];
                            id arr = [[NSString alloc] initWithFormat:@"%@", [array objectAtIndex:i]];
                            good = good && ([num isEqual: arr]);
                        }
                        
                        [self LoginButton].enabled = good;
                    }
                }
            }
        }
    }

}
@end