//
//  HOSTViewControllerLoginScreen.h
//  Hostess
//
//  Created by Theodore Schnepper on 12/28/12.
//  Copyright (c) 2012 Theodore Schnepper. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HOSTViewController.h"
#import "HOSTLavuPOST.h"
#import "HOSTLoginInformation.h"

@interface HOSTViewControllerLoginScreen : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *userNameTextBox;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextBox;
@property (weak, nonatomic) IBOutlet UIButton *LoginButton;

-(IBAction) tryLogin: (UIControl*) sender withEvents: (UIEvent*) events;
- (IBAction)edittingEnded:(id)sender;


@end
