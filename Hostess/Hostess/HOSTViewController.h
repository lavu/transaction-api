//
//  HOSTViewController.h
//  Hostess
//
//  Created by Theodore Schnepper on 8/11/12.
//  Copyright (c) 2012 Theodore Schnepper. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreGraphics/CoreGraphics.h>
#import "HOSTRoom.h"
#import "HOSTWaiter.h"
#import "HOSTTable.h"
#import "HOSTTableInformation.h"
#import "HOSTLassoView.h"
#import "HOSTScrollView.h"
#import "HOSTWaiterDetailView.h"
#import "HOSTUIScrollViewDelegate.h"
#import "HOSTTableView.h"
#import "HOSTWaiterView.h"
#import "HOSTTableDetailViewController.h"
#import "HOSTWaiterDetailViewController.h"

@class HOSTLocation;

@interface HOSTViewController : UIViewController <UIScrollViewDelegate> {
    UIView* hereView;
    UIView* notHereView;
    UIView* shouldBeHereView;
}

@property (strong, nonatomic) IBOutlet HOSTLassoView *mainView;
@property (weak, nonatomic) IBOutlet HOSTCollapsibleView *EmployeeView;
@property (weak, nonatomic) IBOutlet UIScrollView *RoomView;
@property (weak, nonatomic) IBOutlet UIScrollView *titleView;
//@property (nonatomic) HOSTLocation* location;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;

- (void) refreshRoom;
- (void) handleLongTouchGesture:(UIGestureRecognizer*)gestureRecognizer;

- (IBAction) buttonPressed: (UIButton *) sender withEvent: (UIEvent*) ev;
- (void) testSelection: (HOSTWaiter*) waiter touch: (UITouch*) touch;
- (IBAction) exitLocation;
- (IBAction) refreshInformation;

@end
