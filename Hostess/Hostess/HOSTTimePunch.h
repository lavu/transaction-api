//
//  HOSTTimePunch.h
//  Hostess
//
//  Created by Theodore Schnepper on 8/21/12.
//  Copyright (c) 2012 Theodore Schnepper. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HOSTModel.h"

@interface HOSTTimePunch : HOSTModel

@property NSDate* startPunch;
@property NSDate* endPunch;
@property int pID;

- (id) initWithDictionary: (NSDictionary*) punchInfo;
- (BOOL) timeWithin: (NSDate*) date;
- (BOOL) hasEndingPunch;
- (NSTimeInterval) getTimeBetweenPunches;
- (void) updatePunch: (NSDictionary*) dict;

@end
