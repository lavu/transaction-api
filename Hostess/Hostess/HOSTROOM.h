//
//  HOSTROOM.h
//  Hostess
//
//  Created by Theodore Schnepper on 8/13/12.
//  Copyright (c) 2012 Theodore Schnepper. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HOSTModel.h"
//#include <math.h>

@interface HOSTRoom : HOSTModel
    
@property (nonatomic, copy) NSMutableArray* tableList;
@property (nonatomic, copy) NSString* name;

-(id) init;
-(int) getTableCount;

+ (id) parseFromString: (NSString*) name tables:(NSString *)tableStr tableDelim: (NSString*) delim1 detailDelim: (NSString*) delim2;
+ (id) createFromDictionary: (NSDictionary*) dict;
@end
