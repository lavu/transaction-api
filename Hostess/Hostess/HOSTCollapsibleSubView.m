//
//  HOSTCollapsibleSubView.m
//  Hostess
//
//  Created by Theodore Schnepper on 8/28/12.
//  Copyright (c) 2012 Theodore Schnepper. All rights reserved.
//

#import "HOSTCollapsibleSubView.h"

@implementation HOSTCollapsibleSubView

@synthesize view = _view;
@synthesize header = _header;

- (id) initWithView: (UIView*) view parentFrame: (CGRect) frame {
    self = [super init];
    CGRect f;
    f.origin.x = 0.0f;
    f.origin.y = 0.0f;
    f.size.width = frame.size.width;
    {
        f.size.height = 40.0f;
        UIButton* header = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [header setBackgroundColor:[UIColor clearColor]];
        [header addTarget:self action:@selector(toggle) forControlEvents:UIControlEventTouchUpInside];
        [header setFrame:f];
        [header setTitle:[view accessibilityLabel] forState:UIControlStateNormal];
        _header = header;
    }
    
    
    f = [view frame];
    f.size.width = frame.size.width;
    f.origin.y = 40.0f;
    [view setFrame: f];
    _view = view;
    
    [self addSubview: _header];
    [self addSubview: _view];
    [self setBackgroundColor: [UIColor clearColor]];
    [self setContentMode:UIViewContentModeRedraw];
    
    return self;
}

- (void) setToggleTarget: (id) target selector: (SEL) selector {
    if(_target) {
        _target = Nil;
    }
    if(_selector) {
        free(_selector);
        _selector = Nil;
    }
    
    if(target && selector) {
        _target = target;
        _selector = selector;
    }
}

- (bool) isCollapsed {
    bool result = _view.isHidden;
    return result;
}

- (bool) isExpanded {
    bool result = !_view.isHidden;
    return result;
}

- (void) collapse {
    [_view setHidden: true];
    [self recalc];
}

- (void) expand {
    [_view setHidden: false];
    [self recalc];

}

- (void) recalc {
    if(_target && _selector) {
        if([_target respondsToSelector: _selector]) {
            [_target performSelector: _selector];
        }
    }
}

- (void) toggle {
    if( [self isExpanded]) {
        [self collapse];
    } else {
        [self expand];
    }
}

- (CGRect) frame {
    CGRect result = [super frame]; //Ensures that it's properly positioned vertically
    if(![self superview]) {
        return result;
    }
    
    result.size.width = [self superview].frame.size.width;
    result.origin.x = 0;
    
    result.size.height = _header.frame.size.height;
    
    if( ![self isCollapsed] ) {
        result.size.height += _view.frame.size.height;
    }

    return result;
}

- (void) setFrame:(CGRect)frame {
    CGRect result = [self frame];
    result.origin.x = frame.origin.x;
    result.origin.y = frame.origin.y;
    
    [super setFrame: result];
}

@end
