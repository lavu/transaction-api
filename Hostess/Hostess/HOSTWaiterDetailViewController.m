//
//  HOSTWaiterDetailViewController.m
//  Hostess
//
//  Created by Theodore Schnepper on 1/22/13.
//  Copyright (c) 2013 Theodore Schnepper. All rights reserved.
//

#import "HOSTWaiterDetailViewController.h"

@interface HOSTWaiterDetailViewController ()

@end

@implementation HOSTWaiterDetailViewController

@synthesize waiter = _waiter;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

typedef enum {
    WaiterDetailViewIcon = 1,
    WaiterDetailViewTables = 2,
    WaiterDetailViewName = 3
} WaiterDetailViewTags;

- (void)viewDidLoad {
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    UIView* waiterViewIcon = [[self mainView] viewWithTag: WaiterDetailViewIcon];
    [waiterViewIcon setBackgroundColor: [UIColor clearColor]];
    
    HOSTWaiterView* waiterView = [[HOSTWaiterView alloc] initWithWaiter: _waiter];
    CGRect iconFrame = [waiterViewIcon frame];
    iconFrame.origin.x = 0;
    iconFrame.origin.y = 0;
    [waiterViewIcon addSubview: waiterView];
    [waiterView setFrame: iconFrame];
    [waiterView setNeedsDisplay];
    
    UIScrollView* tablesViewContainer = (UIScrollView*)[[self mainView] viewWithTag: WaiterDetailViewTables];
    CGSize tablesViewContainerSize = [tablesViewContainer frame].size;
    float dim = tablesViewContainerSize.width;
    
    tablesViewContainerSize.height = MAX(tablesViewContainerSize.height, [[_waiter assignedTables] count] * dim);
    [tablesViewContainer setContentSize: tablesViewContainerSize];
   
    CGRect tableFrame;
    tableFrame.origin.x = 0.0f;
    tableFrame.origin.y = 0.0f;
    tableFrame.size.width = dim;
    tableFrame.size.height = dim;
    for(float i = 0.0f; i < [[_waiter assignedTables] count]; i++){
        tableFrame.origin.y = i * dim;
        HOSTTable* table = [[_waiter assignedTables] objectAtIndex: i];
        HOSTTableView* tableView = [[HOSTTableView alloc] initWithTable: table];
        [tableView setSelected: true];
        [tablesViewContainer addSubview: tableView];
        [tableView setFrame: tableFrame];
        [tableView setNeedsDisplay];
    }
    UILabel* nameLabel = (UILabel*)[[self mainView] viewWithTag: WaiterDetailViewName];
    NSString* name = [_waiter fullName];
    [nameLabel setText: name];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setMainView:nil];
    [super viewDidUnload];
}

- (void) spawnTimeCard {
    UIView* fromView = [[self mainView] viewWithTag: WaiterDetailViewIcon];
    
    HOSTTimecardViewController* viewCont = [self.storyboard instantiateViewControllerWithIdentifier: @"TimeCardDetail"];
    [viewCont setWaiter: _waiter];
    UIStoryboardPopoverSegue* seque = [[UIStoryboardPopoverSegue alloc] initWithIdentifier:@"timeCardTransition" source: self destination: viewCont];
    
    [[seque popoverController] presentPopoverFromRect: [fromView frame] inView: [self mainView] permittedArrowDirections: UIPopoverArrowDirectionAny animated:true];
}

- (IBAction)showTimeCard:(id)sender forEvent:(UIEvent *)event {
    [self spawnTimeCard];
}
@end
