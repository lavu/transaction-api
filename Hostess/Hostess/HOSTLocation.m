//
//  HOSTLocation.m
//  Hostess
//
//  Created by Theodore Schnepper on 8/13/12.
//  Copyright (c) 2012 Theodore Schnepper. All rights reserved.
//

#import "HOSTLocation.h"

@implementation HOSTLocation

@synthesize roomList = _roomList;
@synthesize name = _name;
@synthesize employees = _employees;
@synthesize eidToEmployees = _eidToEmployees;
@synthesize tableList = _tableList;

NSString* _url;
NSString* _dataName;
NSString* _key;
NSString* _token;

const int WAIT = 30;

- (id) init {
    _roomList = [[NSMutableArray alloc] init];
    _employees = [[NSMutableArray alloc] init];
    _eidToEmployees = [[NSMutableDictionary alloc] init];
    _tableList = [[NSMutableDictionary alloc] init];
    return self;
}

- (int) getRoomCount {
    return _roomList.count;
}

- (void) updateEmployees: (NSArray*) list {
    if(!list) {
        return;
    }
    
    NSMutableArray* tempEMPArray = [NSMutableArray arrayWithArray:_employees];
    for(NSDictionary* dict in list) {
    }
    @synchronized(self) {
        for(HOSTWaiter* waiter in tempEMPArray)
            [_employees removeObject:waiter];
    }
    
}

+ (id) createLocationName: (NSString*) name roomList: (NSArray*) rooms {
    HOSTLocation* loc = [[HOSTLocation alloc] init];
    
    for(NSArray* room in rooms) {
        NSString* name = [room objectAtIndex:0];
        NSString* tables = [room objectAtIndex: 1];
        HOSTRoom* room = [HOSTRoom parseFromString: name tables:tables tableDelim:@"|" detailDelim:@","];
        
        [[loc roomList] addObject:room];
    }
    
    
    return loc;
}

+ (id) createLocationFromArray:(NSArray *)array {
    HOSTLocation* loc = [[HOSTLocation alloc] init];
    
    for(NSDictionary* dict in array) {
        HOSTRoom* room = [HOSTRoom createFromDictionary: dict];
        [[loc roomList] addObject: room];
        
        for(HOSTTable* table in [room tableList]) {
            [[loc tableList] setObject:table forKey:[table tableName]];
        }
        
    }
    
    return loc;
}
+ (id) createLocationFromLogin {
    NSMutableDictionary* arguments = [[NSMutableDictionary alloc] init];
    [arguments setObject:[NSNumber numberWithInt:CommGetRooms] forKey:@"cmd"];
    NSArray* result = [HOSTLavuPOST json_decode:[HOSTLavuPOST queryURL:[HOSTLavuPOST getPOSTURL] withData:arguments]];
    
    HOSTLocation* loc;
    if(result) {
        loc = [HOSTLocation createLocationFromArray:result];
        [loc createEmployees];
    }
    
    return loc;
}

- (void) createEmployees {
    NSMutableDictionary* arguments = [[NSMutableDictionary alloc] init];
    [arguments setObject:[NSNumber numberWithInt:CommGetEmployees] forKey:@"cmd"];
    id result = [HOSTLavuPOST json_decode: [HOSTLavuPOST queryURL:[HOSTLavuPOST getPOSTURL] withData:arguments]];
    if(result && [result isKindOfClass:[NSArray class]]) {
        for(NSDictionary* emp in result) {
            NSString* firstname = [emp objectForKey: @"f_name"];
            NSString* lastname = [emp objectForKey: @"l_name"];
            NSNumber* eid = [emp objectForKey: @"id"];
            HOSTWaiter* waiter = [[HOSTWaiter alloc] initWithID:[eid integerValue] firstName:firstname lastName:lastname];
            [_employees addObject: waiter];
            [_eidToEmployees setObject:waiter forKey: eid];
        }
    }
    
    [self updateClockPunches];
    [self updateSchedules];
}

- (void) updateClockPunches {
    NSMutableDictionary* arguments = [[NSMutableDictionary alloc] init];
    [arguments setObject:[NSNumber numberWithInt: CommGetTimeCards] forKey: @"cmd"];    
    
    id result = [HOSTLavuPOST json_decode: [HOSTLavuPOST queryURL:[HOSTLavuPOST getPOSTURL] withData:arguments]];
    if(result && [result isKindOfClass:[NSArray class]]) {
        for(NSDictionary* time_card in result) {
            NSNumber* eid = [time_card objectForKey: @"server_id"];
            HOSTWaiter* waiter = [_eidToEmployees objectForKey: eid];
            [waiter updatePunches: time_card];
        }
    }
}

- (void) updateSchedules {
    NSMutableDictionary* arguments = [[NSMutableDictionary alloc] init];
    [arguments setObject: [NSNumber numberWithInt: CommGetSchedules] forKey: @"cmd"];
    
    id result = [HOSTLavuPOST json_decode: [HOSTLavuPOST queryURL:[HOSTLavuPOST getPOSTURL] withData:arguments]];
    if(result && [result isKindOfClass: [NSArray class]]){
        for(NSDictionary* schedule in result){
            NSNumber* eid = [schedule objectForKey: @"user_id"];
            HOSTWaiter* waiter = [_eidToEmployees objectForKey: eid];
            [waiter updateSchedules: schedule];
        }
    }
}

@end