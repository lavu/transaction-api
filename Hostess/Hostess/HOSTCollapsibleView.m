//
//  HOSTCollapsibleView.m
//  Hostess
//
//  Created by Theodore Schnepper on 8/28/12.
//  Copyright (c) 2012 Theodore Schnepper. All rights reserved.
//

#import "HOSTCollapsibleView.h"

@implementation HOSTCollapsibleView

- (void) addSubview:(UIView *)view {
    if([[view class] isSubclassOfClass:[UIImageView class]]) {
        [super addSubview:view];
        return;
    }
    
    if(!_recalc) {
         _recalc = @selector(recalculateContentArea);
    }
    
    if( ! [view accessibilityLabel] ) {
        [view setAccessibilityLabel: @"Untitled"];
    }
    HOSTCollapsibleSubView* child = [[HOSTCollapsibleSubView alloc] initWithView:view parentFrame:[self bounds]]; //wrap it to ensure we have the right type of implementation
    [child setToggleTarget:self selector:_recalc];
    [child setContentMode: UIViewContentModeRedraw];
    
    [super addSubview: child];
    
    [self recalculateContentArea];
}

- (void) recalculateContentArea {
    float y = 0.0f;
    for(UIView* view in [self subviews]) {
        if(![[view class] isSubclassOfClass:[HOSTCollapsibleSubView class]]) {
            continue;
        }
        
        CGRect rect = [view frame];
        rect.origin.y = y; //repositions
        
        [view setFrame: rect];
        
        y += rect.size.height;
        [view setNeedsDisplay];
        [view setNeedsLayout];
    }
    CGSize size = [self contentSize];
    size.height = y;
    size.width = [self frame].size.width;
    
    [self setContentSize:size];
    [self setNeedsDisplay];
    [self setNeedsLayout];
}

@end