//
//  HOSTOrder.m
//  Hostess
//
//  Created by Theodore Schnepper on 1/17/13.
//  Copyright (c) 2013 Theodore Schnepper. All rights reserved.
//

#import "HOSTOrder.h"

@implementation HOSTOrder

@synthesize orderid = _orderid;
@synthesize dateOpened = _dateOpened;
@synthesize dateClosed = _dateClosed;
@synthesize tableName = _tableName;
@synthesize checkHasPrinted = _checkHasPrinted;
@synthesize numberOfGuests = _numberOfGuests;

+ (HOSTOrder*) orderFromDictionary: (NSDictionary*) dictionary {
    HOSTOrder* result = [[HOSTOrder alloc] init];
    
    if( [[HOSTOrder orders] containsObject: result] ){
        NSInteger index = [[HOSTOrder orders] indexOfObject: result];
        result = [[HOSTOrder orders] objectAtIndex: index];
    } else {
        [((NSMutableArray*)[HOSTOrder orders]) addObject: result];
    }
    
    [result updateOrderWithDictionary: dictionary];
    [result updateViews];
    return result;
}

+ (NSArray*) orders {
    static NSMutableArray* _orders;
    if(!_orders){
        _orders = [[NSMutableArray alloc] init];
    }
    
    return _orders;
}

- (void) updateOrderWithDictionary: (NSDictionary *) dictionary {
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    NSLocale *enUSPOSIXLocale = [[NSLocale alloc] initWithLocaleIdentifier: @"en_US_POSIX"];
    
    [formatter setLocale: enUSPOSIXLocale];
    [formatter setDateFormat: @"yyyy-MM-dd HH:mm:ss"];
    
    NSString* openedString = (NSString*)[dictionary objectForKey: @"opened"];
    NSString* closedString = (NSString*)[dictionary objectForKey: @"closed"];
    [self setDateOpened: [formatter dateFromString: openedString ]];
    [self setDateClosed: [formatter dateFromString: closedString]];
    [self setTableName: [dictionary objectForKey: @"tablename"]];
    [self setOrderid: [dictionary objectForKey: @"order_id"]];
    [self setNumberOfGuests: [dictionary objectForKey: @"guests"]];
    [self setCheckHasPrinted: [dictionary objectForKey: @"check_has_printed"]];
}

- (NSTimeInterval) currentOrderDuration {
    NSTimeInterval duration;
    
    if(_dateClosed) {
        duration = [_dateClosed timeIntervalSince1970] - [_dateOpened timeIntervalSince1970];
    } else {
        duration = [[NSDate date] timeIntervalSince1970] - [_dateOpened timeIntervalSince1970];
    }
    
    return duration;
}

@end
