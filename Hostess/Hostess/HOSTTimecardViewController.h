//
//  HOSTTimecardViewController.h
//  Hostess
//
//  Created by Theodore Schnepper on 5/24/13.
//  Copyright (c) 2013 Theodore Schnepper. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HOSTWaiter.h"
#import "HOSTTimeCard.h"
#import "HOSTTimePunch.h"

@interface HOSTTimecardViewController : UIViewController
@property (nonatomic, strong) HOSTWaiter* waiter;
@property (strong, nonatomic) IBOutlet UIView *mainView;

- (void) setWaiter: (HOSTWaiter*) waiter;
@end
