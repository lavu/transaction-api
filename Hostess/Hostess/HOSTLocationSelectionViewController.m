//
//  HOSTLocationSelectionViewController.m
//  Hostess
//
//  Created by Theodore Schnepper on 1/14/13.
//  Copyright (c) 2013 Theodore Schnepper. All rights reserved.
//

#import "HOSTLocationSelectionViewController.h"

@interface HOSTLocationSelectionViewController ()

@end

@implementation HOSTLocationSelectionViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    HOSTLoginInformation* loginInfo = [HOSTLavuPOST getLoginInformation];
    NSArray* locations = [loginInfo locations];
    [loginInfo setActiveLocation:[NSString stringWithFormat:@"%@", [[locations objectAtIndex:0] objectForKey:@"id"]]];
    if(locations.count == 1) {
        //If we have only one location, then load that location
        [self selectLocation:self withEvents:NULL];
    }
    
    [[self LocationSelector] setDataSource:self];
    [[self LocationSelector] setDelegate:self];
    
    /*for(int i = 0; i < locations.count; i++) {
        //[[locations objectAtIndex:i] objectForKey:@"id"];
        [[self LocationSelector] set
        [[self LocationSelector] setValue:[[locations objectAtIndex:i] objectForKey:@"id"] forKey:[[locations objectAtIndex:i] objectForKey:@"title"]];
    }*/
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setLocationSelector:nil];
    [self setLocationSelectionButton:nil];
    [super viewDidUnload];
}

- (IBAction) selectLocation: (id) sender withEvents: (UIEvent*) event {
    NSMutableDictionary* arguments = [[NSMutableDictionary alloc] init];
    
    [arguments setObject: [NSNumber numberWithInt: CommSetLocation] forKey:@"cmd"];
    [arguments setObject: [[HOSTLavuPOST getLoginInformation] activeLocation] forKey:@"set_locationid"];
    
    id result = [HOSTLavuPOST json_decode:[HOSTLavuPOST queryURL:[HOSTLavuPOST getPOSTURL] withData:arguments]];
    NSLog(@"%@", result);
    
    //arguments = [[NSMutableDictionary alloc] init];
    //[arguments setObject:[NSNumber numberWithInt:CommGetRooms] forKey:@"cmd"];
    
    //result = [HOSTLavuPOST json_decode:[HOSTLavuPOST queryURL:[HOSTLavuPOST getPOSTURL] withData:arguments]];
    
    //NSLog(@"%@", result);
    [self performSegueWithIdentifier:@"SetLocation" sender:sender];
}

-(IBAction) logout {
    NSMutableDictionary* arguments = [[NSMutableDictionary alloc] init];
    [arguments setObject:[NSNumber numberWithInt:CommLogout] forKey:@"cmd"];
    id result = [HOSTLavuPOST json_decode:[HOSTLavuPOST queryURL:[HOSTLavuPOST getPOSTURL] withData:arguments]];
    if(result) {
        [[self navigationController] popToRootViewControllerAnimated:true];
    }
}

#pragma mark UIPickerViewDataSource Begin

- (NSInteger) numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return [[NSNumber numberWithInt:1] integerValue];
}

- (NSInteger) pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return [[HOSTLavuPOST getLoginInformation] locations].count;
}

#pragma mark UIPickerViewDataSource End

#pragma mark UIPickerViewDelegate Begin

- (NSString *) pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if(pickerView != [self LocationSelector]) {
        return NULL;
    }
    
    return [[[[HOSTLavuPOST getLoginInformation] locations] objectAtIndex:row] objectForKey:@"title"];
}

- (NSAttributedString*) pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if(pickerView != [self LocationSelector])
        return NULL;
    return [[NSAttributedString alloc] initWithString:[[[[HOSTLavuPOST getLoginInformation] locations] objectAtIndex:row] objectForKey:@"title"]];
}

- (void) pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    HOSTLoginInformation* loginInfo = [HOSTLavuPOST getLoginInformation];
    
    [loginInfo setActiveLocation:[NSString stringWithFormat:@"%@", [[[loginInfo locations] objectAtIndex:row] objectForKey:@"id"]]];
}

#pragma mark UIPickerViewDelegate End

@end