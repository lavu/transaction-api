//
//  HOSTWaiterDetailView.h
//  Hostess
//
//  Created by Theodore Schnepper on 8/27/12.
//  Copyright (c) 2012 Theodore Schnepper. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HOSTWaiter.h"
#import "HOSTCollapsibleView.h"
#import "HOSTTable.h"
#import "HOSTTimeCard.h"
#import "HOSTTimePunch.h"

@interface HOSTWaiterDetailView : UIView

@property (nonatomic, assign) HOSTWaiter* waiter;

+ (id) populateWithWaiter:(HOSTWaiter *)waiter parentView: (UIView*) parent;
- (id) initWithWaiter: (HOSTWaiter *)waiter;
- (void) kill;
@end
