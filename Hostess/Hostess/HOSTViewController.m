//
//  HOSTViewController.m
//  Hostess
//
//  Created by Theodore Schnepper on 8/11/12.
//  Copyright (c) 2012 Theodore Schnepper. All rights reserved.
//

#import "HOSTViewController.h"
#import "HOSTLocation.h"
#import "HOSTPOSTHandling.h"
#import "HOSTXMLBuilding.h"
#import "HOSTLavuPOST.h"

@interface HOSTViewController ()

@end

@implementation HOSTViewController
@synthesize pageControl = _pageControl;
@synthesize mainView = _mainView;
@synthesize EmployeeView = _EmployeeView;
@synthesize RoomView = _RoomView;
HOSTUIScrollViewDelegate* _zoomDelegate;
//@synthesize location = _location;



NSMutableArray* waitersHere;

HOSTWaiter* activeAssignment;
HOSTLocation* location;

#pragma mark initMethods


- (id) initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    return [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
}

- (id) initWithCoder:(NSCoder *)aDecoder {
    return [super initWithCoder:aDecoder];
}

- (id) init {
    return [super init];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"test");
    //[UIDevice currentDevice].orientation = UIInterfaceOrientationLandscapeRight;
	// Do any additional setup after loading the view, typically from a nib.
    _zoomDelegate = [[HOSTUIScrollViewDelegate alloc] init];
    [_zoomDelegate viewForZoomingInScrollView:nil];
    
    hereView = [[UIView alloc] init];
    notHereView = [[UIView alloc] init];
    
    [hereView setAccessibilityLabel: @"Here:"];
    [notHereView setAccessibilityLabel: @"Not Here:"];
    [_EmployeeView addSubview:hereView];
    [_EmployeeView addSubview:notHereView];
    
    [_mainView setBackgroundColor: [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_floor_x1_tile.png"]]];
    
    waitersHere = [[NSMutableArray alloc] init];
    
    [_RoomView setDelegate:self];
    
    location = [HOSTLocation createLocationFromLogin];
    [self loadRooms];
    [self loadEmployees];
    
    NSThread* refreshEmpList = [[NSThread alloc] initWithTarget:self selector:@selector(refreshRoom) object:NULL];
    [refreshEmpList start];
}

- (void) refreshRoom {
    int WAIT = 5;
    while(true) {
        sleep(WAIT);
        [self loadEmployees];
    }
}

- (void) loadRooms {
    const float incScale = 2.0f;
    
    CGRect frame = [_RoomView frame];
    
    float width = frame.size.height;
    float height = frame.size.width;
    
    frame.size.width = width;
    frame.size.height = height;
    
    frame.origin.x = 0.0f;
    int i = 0;
    for(HOSTRoom* room in [location roomList]) {
        frame.origin.x = frame.size.width * i++;
        HOSTScrollView* roomView = [[HOSTScrollView alloc] initWithFrame: frame];
        [roomView setParent:_mainView];
        UIView* container = [[UIView alloc] initWithFrame:[roomView frame]];
        [container setTag: -1];
        
        CGRect labelFrame;
        labelFrame.size.width =150.0f;
        labelFrame.size.height = 25.0f;
        labelFrame.origin.x = (frame.size.width - labelFrame.size.width) / 2.0f;
        labelFrame.origin.y = 0.0f;
        
        UILabel* titleLabel = [[UILabel alloc] initWithFrame:labelFrame];
        [titleLabel setText: [room name]];
        [titleLabel setBackgroundColor: [UIColor colorWithRed:0.0f green:0.0f blue:0.0f alpha:0.0f]];
        [titleLabel setTextColor: [UIColor whiteColor]];
        
        float maxWidth = 0.0f;
        float maxHeight = 0.0f;
        float minWidth = MAXFLOAT;
        float minHeight = MAXFLOAT;
        
        for(HOSTTable* table in [room tableList]) {
            maxWidth = MAX(maxWidth, [table loc].origin.x + [table loc].size.width + 100.0f);
            maxHeight = MAX(maxHeight, [table loc].origin.y + [table loc].size.height + 100.0f);
            
            minWidth = MIN(minWidth, [table loc].origin.x);
            minHeight = MIN(minHeight, [table loc].origin.y);
        } 
        
        float scale = frame.size.width / (maxWidth - minWidth);
        scale = MIN(scale, (frame.size.height / (maxHeight - minHeight)));
        
        CGRect rect;
        rect.origin.x = minWidth;
        rect.origin.y = minHeight;
        rect.size.width = maxWidth;
        rect.size.height = maxHeight;

        float maxScale = MAX(maxWidth / frame.size.width, maxHeight / frame.size.height);
        float minScale = (0.65f / maxScale) * 0.9f;
        maxScale = (maxScale * 5.0f) * 0.9f;
        
        [roomView setMaximumZoomScale: maxScale/*10.0f*/];
        [roomView setMinimumZoomScale: minScale/*0.5f*/];
        
        [roomView setDelegate: _zoomDelegate];
        [roomView setSelector:@selector(testSelection:touch:) target:self];
        
        for(HOSTTable* table in [room tableList]) {
            HOSTTableView* button = [[HOSTTableView alloc] initWithTable:table];
            [table setView: button];
            CGRect frame = [table loc];
            frame.origin.x = (frame.origin.x - minWidth + 100.0f) * incScale;
            frame.origin.y = (frame.origin.y - minHeight + 12.5f + 100.0f) * incScale;
            frame.size.width *= incScale;
            frame.size.height *= incScale;
            
            [button setFrame: frame];
            
            [button setTransform: CGAffineTransformMakeRotation([table radians])];
            
            UILongPressGestureRecognizer* gesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongTouchGesture:)];
            
            [button addGestureRecognizer:gesture];
            
            [button setContentMode:UIViewContentModeRedraw];
            
            [button setNeedsDisplay];
        
            [container addSubview:button];
            [button addTarget:self action:@selector(buttonPressed:withEvent:) forControlEvents:UIControlEventTouchUpInside];
        
        }
        [container setFrame:CGRectMake(0.0f, 0.0f, (maxWidth - minWidth) * incScale, (maxHeight - minHeight + 12.5f) * incScale)];
        
        [roomView setContentSize: CGSizeMake((maxWidth - minWidth) * incScale, (maxHeight - minHeight + 12.5f) * incScale)];
        [roomView setZoomScale: minScale];
        
        [roomView addSubview:container];
        [_RoomView addSubview: roomView];
    }
    
    [_RoomView setContentSize: CGSizeMake(frame.size.width * location.roomList.count , frame.size.height)];
    [_pageControl setNumberOfPages: location.roomList.count];
}

- (void) loadEmployees {
    if(!location) {
        return;
    }
    
    CGRect placement;
    placement.origin.x = 0.0f;
    placement.origin.y = 0.0f;
    placement.size.height = 80.0f;
    placement.size.width = 80.0f;
    
    int hereCount = 0;
    int notHereCount = 0;
    
    bool needsReposition = false;
    for(HOSTWaiter* waiter in location.employees) {
        UIView* button = [waiter view];
        if(!button) {
            needsReposition = true;
            break;
        }
        
        if([button superview] == hereView && !waiter.isHere) {
            needsReposition = true;
            break;
        }
        
        if ([button superview] == notHereView && waiter.isHere) {
            needsReposition = true;
            break;
        }
    }
    
    if(needsReposition) { //Creates a Button
        for(HOSTWaiter* waiter in location.employees) {
            HOSTWaiterView* button = (HOSTWaiterView*)[waiter view];
            if(!button) { //Only Create the Button if the Waiter is here.
                //Button Creation
                button = [[HOSTWaiterView alloc] initWithWaiter: waiter];
                [waiter setView: button];
                [button addTarget:self action:@selector(buttonPressed:withEvent:) forControlEvents:UIControlEventTouchUpInside];
                [button addTarget:self action:@selector(dragEnd:withEvent:) forControlEvents:UIControlEventTouchUpOutside];
                [button addTarget:self action:@selector(drag:withEvent:) forControlEvents:UIControlEventTouchDragOutside];
                
                UILongPressGestureRecognizer* gesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongTouchGesture:)];
                
                [button addGestureRecognizer:gesture];
                
                [button setContentMode:UIViewContentModeRedraw];
                
                [button setNeedsDisplay];
            }
            
            if(waiter.isHere) {
                CGRect frame = CGRectMake(0, hereCount++ * placement.size.height, placement.size.width, placement.size.height);
                [button setFrame:frame];
                
                [hereView performSelectorOnMainThread:@selector(addSubview:) withObject:button waitUntilDone:TRUE];
                [hereView performSelectorOnMainThread:@selector(bringSubviewToFront:) withObject:button waitUntilDone:TRUE];
            } else {
                CGRect frame = CGRectMake(0, notHereCount++ * placement.size.height, placement.size.width, placement.size.height);
                [button setFrame:frame];
                
                [notHereView performSelectorOnMainThread:@selector(addSubview:) withObject:button waitUntilDone:TRUE];
                [notHereView performSelectorOnMainThread:@selector(bringSubviewToFront:) withObject:button waitUntilDone:TRUE];
            }
            
            {
                CGRect frame = [hereView frame];
                frame.size.height = hereCount * placement.size.height;
                [hereView setFrame:frame];
                
                frame = [notHereView frame];
                frame.size.height = notHereCount * placement.size.height;
                [notHereView setFrame:frame];
                
                [_EmployeeView performSelectorOnMainThread:@selector(recalculateContentArea) withObject:NULL waitUntilDone:TRUE];
            }
        }
    }
}

- (void)viewDidUnload {
    [self setEmployeeView:nil];
    [self setRoomView:nil];
    [self setPageControl:nil];
    [self setMainView:nil];
    [self setTitleView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
    } else {
        return (interfaceOrientation != UIInterfaceOrientationPortrait && interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
        //return YES;
    }
}

#pragma mark touchEvents

/**
 Opens a context menu for tables, or sets the assignment as active for the waiter
 */
- (void) handleLongTouchGesture:(UIGestureRecognizer *)gestureRecognizer {
    UIButton* button = (UIButton*)[gestureRecognizer view];
    if([gestureRecognizer state] != UIGestureRecognizerStateEnded) {
        return;
    }
    
    //Trigger a context-based pop-up.
    if( [button isKindOfClass:[HOSTWaiterView class]] ) {  //Waiter
        HOSTWaiterView* view = (HOSTWaiterView*) button;
        
        HOSTWaiterDetailViewController* viewCont = [self.storyboard instantiateViewControllerWithIdentifier: @"WaiterDetail"];
        [viewCont setWaiter: [view waiter]];
        UIStoryboardPopoverSegue* segue = [[UIStoryboardPopoverSegue alloc] initWithIdentifier:@"waiterTransition" source:self destination: viewCont];
        
        [[segue popoverController] presentPopoverFromRect:[_mainView convertRect: [button frame] fromView: [button superview]] inView: [self mainView] permittedArrowDirections:UIPopoverArrowDirectionAny animated: true];
        
    } else if( [button isKindOfClass:[HOSTTableView class]] ) { //Table
        HOSTTableView* view = (HOSTTableView*) button;
        
        HOSTTableDetailViewController* viewCont = [self.storyboard instantiateViewControllerWithIdentifier: @"TableDetail" ];
        [viewCont setTable: [view table]];
        UIStoryboardPopoverSegue* segue = [[UIStoryboardPopoverSegue alloc] initWithIdentifier:@"tableTransition" source:self destination: viewCont];
        
        [[segue popoverController] presentPopoverFromRect:[_mainView convertRect:[button frame] fromView:[button superview]] inView: [self mainView] permittedArrowDirections:UIPopoverArrowDirectionAny animated: true];
    }
}

UIColor* previousColor;
- (IBAction) drag: (UIButton *) sender withEvent: (UIEvent*) ev {
    UITouch* touch = [[ev allTouches] anyObject];
    HOSTWaiterView* view = (HOSTWaiterView*) sender;
    
    HOSTWaiter* waiter = [view waiter];
    if([_mainView drawColor] != [waiter color]) {
        previousColor = [_mainView drawColor];
        [_mainView setDrawColor: [waiter color]];
    }
    
    [_mainView addTouchPoint:touch];
    [_mainView setNeedsDisplay];
}

- (void) testSelection: (HOSTWaiter*) waiter touch: (UITouch*) touch {
    HOSTWaiter* w;
    if(waiter) {
        w = waiter;
    } else {
        w = activeAssignment;
    }
    
    if(!w) {
        return;
    }
    CGPoint point;
    for(UIView* subView in [_RoomView subviews]) {
        if(touch) {
            point = [touch locationInView:[subView viewWithTag:-1]]; //translating the point to the same frame-space
        }
        
        for(UIView* t in [[subView viewWithTag:-1] subviews]) {
            if([_mainView isRectWithinSelection:t] || (touch && CGRectContainsPoint([t frame], point))) {
                HOSTTableView* view = (HOSTTableView*) t;
                HOSTTable* table = [view table];
                
                if(table) {
                    [w toggleAssignment: table];
                    [table toggleAssignment: w];
                    [t setNeedsDisplay];
                }
            }
        }
    }
}

- (IBAction) dragEnd: (UIButton *) sender withEvent: (UIEvent*) ev {  //Primarily Written to Detect the point at which the dragging ended
    UITouch* touch = [[ev allTouches] anyObject];
    
    HOSTWaiterView* view = (HOSTWaiterView*) sender;
    [self testSelection: [view waiter] touch:touch];
    
    [_mainView clearPoints];
    [_mainView setNeedsDisplay];
    [_mainView setDrawColor: previousColor];
    
}

- (IBAction) buttonPressed: (UIControl *) sender withEvent: (UIEvent*)ev {
  
    if([sender isKindOfClass:[HOSTWaiterView class]])  {
        HOSTWaiterView* view = (HOSTWaiterView*) sender;
        HOSTWaiter *waiter = [view waiter];
        //NSLog(@"Pressed");
        if(activeAssignment == waiter) {
            [sender setSelected:![sender isSelected]];
            [sender setNeedsDisplay];
            activeAssignment = NULL;
            sender.selected = FALSE;
            
            [_RoomView setScrollEnabled: TRUE];
            [_mainView setDrawColor:[UIColor redColor]];
            for(HOSTScrollView* view in [_RoomView subviews]) {
                [view setLocked: FALSE];
            }
        } else {
            if(activeAssignment) {
                HOSTWaiterView* oldSelection = (HOSTWaiterView*) [activeAssignment view];
                if(oldSelection){
                    [oldSelection setSelected: FALSE];
                    [oldSelection setNeedsDisplay];
                }
            }
            activeAssignment = waiter;
            [sender setSelected: TRUE];
            [sender setNeedsDisplay];

            [_mainView setDrawColor:[waiter color]];

            [_RoomView setScrollEnabled: FALSE];
            for(HOSTScrollView* view in [_RoomView subviews]) {
                [view setLocked: TRUE];
            }
        }
        return;
    }
    
    if([sender isKindOfClass:[HOSTTableView class]]) {
        HOSTTableView* view = (HOSTTableView*) sender;
        HOSTTable *table = [view table];
        if(activeAssignment) {
            //Waiter is currently selected.  It is time to change the color of the table
        
            //need to check to see if waiter it currently assigned to Table
            [table toggleAssignment:activeAssignment];
            [activeAssignment toggleAssignment:table];
            
            [sender setNeedsDisplay];
        } else {
            HOSTTableView* view = (HOSTTableView*) sender;
            
            HOSTTableDetailViewController* viewCont = [self.storyboard instantiateViewControllerWithIdentifier: @"TableDetail" ];
            [viewCont setTable: [view table]];
            UIStoryboardPopoverSegue* segue = [[UIStoryboardPopoverSegue alloc] initWithIdentifier:@"tableTransition" source:self destination: viewCont];
            
            [[segue popoverController] presentPopoverFromRect:[_mainView convertRect:[sender frame] fromView:[sender superview]] inView: [self mainView] permittedArrowDirections:UIPopoverArrowDirectionAny animated: true];
        }
    }
}

- (void) scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat frameWidth = _RoomView.frame.size.width;
    int page = floor( (scrollView.contentOffset.x - (frameWidth/2.0f)) / frameWidth) + 1;
    
    [_pageControl setCurrentPage: page];
}

- (IBAction) exitLocation {
    if([[HOSTLavuPOST getLoginInformation] locations].count > 1) {
        [[self navigationController] popViewControllerAnimated:true];
    } else {  //Logout Entirely, bypassing the Location Selector
        NSMutableDictionary* arguments = [[NSMutableDictionary alloc] init];
        [arguments setObject:[NSNumber numberWithInt:CommLogout] forKey:@"cmd"];
        id result = [HOSTLavuPOST json_decode:[HOSTLavuPOST queryURL:[HOSTLavuPOST getPOSTURL] withData:arguments]];
        if(result) {
            [[self navigationController] popToRootViewControllerAnimated:true];
        }
    }
}

- (IBAction) refreshInformation {
    NSMutableDictionary* arguments = [[NSMutableDictionary alloc] init];
    [arguments setObject:[NSNumber numberWithInt:CommGetOrders] forKey:@"cmd"];
    
    id result = [HOSTLavuPOST json_decode: [HOSTLavuPOST queryURL:[HOSTLavuPOST getPOSTURL] withData:arguments]];
    if(result) {
        if([result isKindOfClass: [NSArray class]]) {
            for(NSDictionary* order in result) {
                NSString* tablename = [order objectForKey:@"tablename"];
                NSDictionary* tableArray = [location tableList];
                HOSTTable* table = [tableArray objectForKey: tablename];
                if(table) {
                    [table setCurrentOrder:[HOSTOrder orderFromDictionary: order]];
                }
            }
        }
    }
}

@end