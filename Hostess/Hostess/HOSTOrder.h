//
//  HOSTOrder.h
//  Hostess
//
//  Created by Theodore Schnepper on 1/17/13.
//  Copyright (c) 2013 Theodore Schnepper. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HOSTModel.h"

@interface HOSTOrder : HOSTModel
@property (nonatomic, copy) NSString* orderid;
@property (nonatomic, copy) NSDate* dateOpened;
@property (nonatomic, copy) NSDate* dateClosed;
@property (nonatomic, copy) NSString* tableName;
@property (nonatomic, copy) NSNumber* checkHasPrinted;
@property (nonatomic, copy) NSNumber* numberOfGuests;

+ (NSArray*) orders;

+ (HOSTOrder*) orderFromDictionary: (NSDictionary*) dictionary;
- (void) updateOrderWithDictionary: (NSDictionary*) dictionary;
- (NSTimeInterval) currentOrderDuration;
@end
