//
//  HOSTTableDetailViewController.h
//  Hostess
//
//  Created by Theodore Schnepper on 1/18/13.
//  Copyright (c) 2013 Theodore Schnepper. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HOSTTable.h"
#import "HOSTTableView.h"
#import "HOSTWaiter.h"
#import "HOSTWaiterView.h"
#import "HOSTOrder.h"

@interface HOSTTableDetailViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIView *mainView;
@property (strong, nonatomic) HOSTTable* table;
- (void) setTable: (HOSTTable*) table;

@end
