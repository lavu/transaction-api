//
//  HOSTScheduleTime.h
//  Hostess
//
//  Created by Theodore Schnepper on 5/30/13.
//  Copyright (c) 2013 Theodore Schnepper. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HOSTModel.h"

@interface HOSTScheduleTime : HOSTModel
@property NSDate* startDate;
@property NSDate* endDate;
@property int sID;

- (id) initWithDictionary: (NSDictionary*) schedule;
- (void) updateWithDictionary: (NSDictionary*) dict;

@end
