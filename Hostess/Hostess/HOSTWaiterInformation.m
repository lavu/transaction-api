//
//  HOSTWaiterInformation.m
//  Hostess
//
//  Created by Theodore Schnepper on 1/8/13.
//  Copyright (c) 2013 Theodore Schnepper. All rights reserved.
//

#import "HOSTWaiterInformation.h"

@implementation HOSTWaiterInformation


static UIImage* _waiterImage;


+ (UIImage*) getWaiterImage {
    if(!_waiterImage) {
        _waiterImage = [UIImage imageNamed:@"i_people_grade.png"];
    }
    
    return _waiterImage;
}

@end
