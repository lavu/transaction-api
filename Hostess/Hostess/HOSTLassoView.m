//
//  HOSTLassoView.m
//  Hostess
//
//  Created by Theodore Schnepper on 8/22/12.
//  Copyright (c) 2012 Theodore Schnepper. All rights reserved.
//

#import "HOSTLassoView.h"
#import "HOSTTable.h"
#import "HOSTTableInformation.h"
#import "HOSTTableView.h"

@implementation HOSTLassoView

@synthesize points = _points;
@synthesize drawColor = _drawColor;

CGMutablePathRef currentPath;

- (id) init {
    //_points = [[NSMutableArray alloc] init];
    
    _drawColor = [UIColor redColor];
    return [super init];
    
}

CGContextRef c;

- (void) drawRect:(CGRect)rect {
    [super drawRect:rect];

    c = UIGraphicsGetCurrentContext();
    
    CGContextSetStrokeColorWithColor(c, _drawColor.CGColor);
    
    CGContextSetFillColorWithColor(c, _drawColor.CGColor);
    
    //[self buildContextPath];
    CGContextSetLineWidth(c, 4.0f);
    if(currentPath){
        CGContextAddPath(c, currentPath);
        if(_points.count > 1)
            CGContextClosePath(c);
        CGContextDrawPath(c, kCGPathStroke);
    }
}

/**
 * This takes a UITouch, and stores it's CGPoint within
 * the points array, using an NSValue
 */
- (void) addTouchPoint: (UITouch*) touch {
    if(!_points) {
        _points = [[NSMutableArray alloc] init];
    }

    CGPoint point = [touch locationInView:self];
    NSValue* val = [NSValue valueWithCGPoint: point];
    [_points addObject: val];
    
    if(!currentPath) {
        [self buildContextPath];
    } else {
        CGAffineTransform identity = CGAffineTransformMakeScale(1.0f, 1.0f);
        CGPathAddLineToPoint(currentPath, &identity, point.x, point.y);
    }
    //NSLog(@"Added 1 Touch now has %i", [_points count]);
}

/**
 * clearPoints clears the current list of points for the path, and resets the current Path.
 */
- (void) clearPoints {
    [_points removeAllObjects];
    currentPath = NULL;
}


/**
 * This method takes all of the current points stored within points, and
 * creates a Path utilizing the points.  The points are assumed to be in
 * sequential order.
 */
- (void) buildContextPath {
    currentPath = CGPathCreateMutable();

    //NSLog(@"total points: %i", _points.count);
    CGPoint* path = malloc(sizeof(CGPoint) * _points.count);
    
    
    for(int i = 0; i < _points.count; i++) {
        CGPoint* point = malloc(sizeof(CGPoint));
        [[_points objectAtIndex:i] getValue: point];
        path[i] = *point;
    }
    CGAffineTransform identity = CGAffineTransformMakeScale(1.0f, 1.0f);
    CGPathAddLines(currentPath, &identity, path, _points.count);

    //if(_points.count > 1)
    //    CGPathCloseSubpath(currentPath);
}

/**
 * This method tests whether the given button has all four corners within the currentPath,
 * or if the center of the button is within the Path
 */
- (BOOL) isRectWithinSelection: (UIView*) button {
    
    HOSTTableView* view = (HOSTTableView*) button;
    double iterations = 100.0f;
    CGPoint* testPoints = (CGPoint*) malloc(sizeof(CGPoint) * iterations);
    if( [[view table] tableType] == 0 ){
        float height = [button frame].size.height / 2.0f;
        float width = [button frame].size.width / 2.0f;
        float x = [button frame].origin.x + width;
        float y = [button frame].origin.y + height;
        
        double inc = 2.0 * M_PI / iterations;
        int j = 0;
        for(double i = 0; i < 2.0 * M_PI; i += inc ){
            CGPoint point = CGPointMake(x + (cos(i) * width), y + (sin(i) * height));
            point = [self convertPoint: point fromView: [button superview]];
            testPoints[j++] = point;
        }
    } else {
        CGPoint* points = [HOSTTableInformation getVectorForTable: [[view table] tableType]];
        float height = [button frame].size.height;
        float width = [button frame].size.width;
        float x = [button frame].origin.x;
        float y = [button frame].origin.y;
        
        int index = 0;
        for(int i = 0; i < 4; i++){
            CGPoint point1 = points[i];
            CGPoint point2;
            if(i == 3){
                point2 = points[0];
            } else {
                point2 = points[i+1];
            }
            
            double max_iterations = iterations/4.0f;
            for(double j = 0; j < max_iterations; j++){
                double weight = j / max_iterations;
                double px = (weight * point1.x + ((1 - weight) * point2.x)) * width + x;
                double py = (weight * point1.y + ((1 - weight) * point2.y)) * height + y;
                CGPoint point = CGPointMake(px, py);
                point = [self convertPoint: point fromView: [button superview]];
                testPoints[index++] = point;
            }
        }
    }
    
    CGAffineTransform identity = CGAffineTransformIdentity;
    for(int i = 0; i < iterations; i++){
        CGPoint point = testPoints[i];
        if(CGPathContainsPoint( currentPath, &identity, point, true)){
            free(testPoints);
            return true;
        }
    }
    
    return false;
    
    /*CGRect rect = [button frame];

    CGPoint p1 = [self convertPoint: rect.origin fromView:[button superview]];
    CGPoint p2 = CGPointMake(p1.x + rect.size.width, p1.y);
    CGPoint p3 = CGPointMake(p1.x, p1.y + rect.size.height);
    CGPoint p4 = CGPointMake(p1.x + rect.size.width, p1.y + rect.size.height);
    
    //[self buildContextPath];
    CGPoint center = CGPointMake(p1.x + rect.size.width / 2, p1.y + rect.size.height / 2);
    if(currentPath) {
        return (CGPathContainsPoint(currentPath, &identity, p1, true) &&
             CGPathContainsPoint(currentPath, &identity, p2, true) &&
             CGPathContainsPoint(currentPath, &identity, p3, true) &&
            CGPathContainsPoint(currentPath, &identity, p4, true)) ||
            CGPathContainsPoint(currentPath, &identity, center, true);
    }
    
    return FALSE;*/
}

@end