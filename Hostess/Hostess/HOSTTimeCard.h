//
//  HOSTTimeCard.h
//  Hostess
//
//  Created by Theodore Schnepper on 8/21/12.
//  Copyright (c) 2012 Theodore Schnepper. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HOSTModel.h"
#import "HOSTTimePunch.h"

@interface HOSTTimeCard : HOSTModel

@property NSMutableArray* punches;

- (BOOL) isWithinTime: (NSDate*) date;
- (void) update: (NSDictionary*) dict;

@end
