//
//  HOSTUIScrollViewDelegate.m
//  Hostess
//
//  Created by Theodore Schnepper on 1/3/13.
//  Copyright (c) 2013 Theodore Schnepper. All rights reserved.
//

#import "HOSTUIScrollViewDelegate.h"

@implementation HOSTUIScrollViewDelegate


- (id) init {
    return [super init];
}

#pragma mark Scroll Events
-(void)scrollViewDidScroll:(UIScrollView *)scrollView {
}

-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
}

-(void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset {
}

-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
}

-(void)scrollViewDidScrollToTop:(UIScrollView *)scrollView {
}

-(void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView {
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
}

- (BOOL)scrollViewShouldScrollToTop:(UIScrollView *)scrollView {
    return false;
}

#pragma mark Managing Zoom
-(void)scrollViewWillBeginZooming:(UIScrollView *)scrollView withView:(UIView *)view {
}

-(void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(float)scale {
}

-(void)scrollViewDidZoom:(UIScrollView *)scrollView {
}

-(UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    if(scrollView) {
        return [scrollView viewWithTag:-1];
    }
    return NULL;
}
#pragma mark Responding to Scrolling Animations

-(void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView {
}

@end