//
//  HOSTSelectionCalculation.h
//  Hostess
//
//  Created by Theodore Schnepper on 8/23/12.
//  Copyright (c) 2012 Theodore Schnepper. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <math.h>

@interface HOSTSelectionCalculation : NSObject

float windingNumber(NSArray* points, CGPoint point);
CGPoint normalize(CGPoint point);
BOOL rectWithinSelection(CGRect rect, NSArray* points);

@end
