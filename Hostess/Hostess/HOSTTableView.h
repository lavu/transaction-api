//
//  HOSTTableView.h
//  Hostess
//
//  Created by Theodore Schnepper on 1/3/13.
//  Copyright (c) 2013 Theodore Schnepper. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HOSTTable.h"
#import "HOSTTableInformation.h"
#import "HOSTWaiter.h"

@interface HOSTTableView : UIControl

@property (atomic) UIImage* backgroundImage;
@property (atomic) HOSTTable* table;
@property (atomic) UILabel* label;

- (id) initWithBackgroundImage: (UIImage*) image;
- (id) initWithTable: (HOSTTable*) table;

@end
