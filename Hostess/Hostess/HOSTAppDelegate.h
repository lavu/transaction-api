//
//  HOSTAppDelegate.h
//  Hostess
//
//  Created by Theodore Schnepper on 8/11/12.
//  Copyright (c) 2012 Theodore Schnepper. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HOSTAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
