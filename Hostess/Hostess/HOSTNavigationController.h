//
//  HOSTNavigationController.h
//  Hostess
//
//  Created by Theodore Schnepper on 12/28/12.
//  Copyright (c) 2012 Theodore Schnepper. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HOSTNavigationController : UINavigationController

@end
