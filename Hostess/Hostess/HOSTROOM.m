//
//  HOSTROOM.m
//  Hostess
//
//  Created by Theodore Schnepper on 8/13/12.
//  Copyright (c) 2012 Theodore Schnepper. All rights reserved.
//

#import "HOSTRoom.h"
#import "HOSTTable.h"

@implementation HOSTRoom

@synthesize tableList = _tableList;
@synthesize name = _name;

- (id) init {
    _tableList = [[NSMutableArray alloc] init];
    return self;
}

- (int) getTableCount {
    return _tableList.count;
}

+ (id) parseFromString: (NSString*) name tables:(NSString *)tableStr tableDelim: (NSString*) delim1 detailDelim: (NSString*) delim2 {
    
    HOSTRoom* room = [[HOSTRoom alloc] init];
    [room setName: name];
    
    NSArray* tableInfo = [tableStr componentsSeparatedByString:delim1];
    
    for( NSString* table in tableInfo) {
        NSArray* details = [table componentsSeparatedByString:delim2];
        //ideally, tables represented by ...|type,name,x,y,w,h,r|...
        if(details.count >= 7) { //Make sure we have at least as many details as required
            int type = [((NSString*)[details objectAtIndex:0]) intValue];
            NSString* name = [details objectAtIndex:1];
            float x = [((NSString*)[details objectAtIndex:2]) floatValue];
            float y = [((NSString*)[details objectAtIndex:3]) floatValue];
            float w = [((NSString*)[details objectAtIndex:4]) floatValue];
            float h = [((NSString*)[details objectAtIndex:5]) floatValue];
            float r = [((NSString*)[details objectAtIndex:6]) floatValue];
            
            HOSTTable* table = [HOSTTable createTable:type name:name xPos:x yPos:y width:w height:h radians:r];
            [[room tableList] addObject:table];
        }
    }
    
    return room;
    
}

+ (id) createFromDictionary:(NSDictionary *)dict {
    if(!dict) {
        return NULL;
    }
    
    //const NSString* tag_id = @"id";
    //const NSString* tag_loc_id = @"loc_id";
    const NSString* tag_title = @"title";
    const NSString* tag_shapes = @"shapes";
    const NSString* tag_names = @"names";
    const NSString* tag_coord_x = @"coord_x";
    const NSString* tag_coord_y = @"coord_y";
    //const NSString* tag_centerX = @"centerX";
    //const NSString* tag_centerY = @"centerY";
    const NSString* tag_widths = @"widths";
    const NSString* tag_heights = @"heights";
    const NSString* tag_rotate = @"rotate";
    
    NSString* delim = @"|";
    
    const NSString* s_shapes = [NSString stringWithFormat: @"%@", [dict objectForKey:tag_shapes]];
    const NSString* s_names = [NSString stringWithFormat: @"%@", [dict objectForKey:tag_names]];
    const NSString* s_coord_x = [NSString stringWithFormat: @"%@", [dict objectForKey:tag_coord_x]];
    const NSString* s_coord_y = [NSString stringWithFormat: @"%@", [dict objectForKey:tag_coord_y]];
    const NSString* s_heights = [NSString stringWithFormat: @"%@", [dict objectForKey:tag_heights]];
    const NSString* s_widths = [NSString stringWithFormat: @"%@", [dict objectForKey:tag_widths]];
    const NSString* s_rotates = [NSString stringWithFormat: @"%@", [dict objectForKey:tag_rotate]];
    
    NSArray* a_shapes = [s_shapes componentsSeparatedByString: delim];
    NSArray* a_names = [s_names componentsSeparatedByString: delim];
    NSArray* a_coord_x = [s_coord_x componentsSeparatedByString: delim];
    NSArray* a_coord_y = [s_coord_y componentsSeparatedByString: delim];
    NSArray* a_heights = [s_heights componentsSeparatedByString: delim];
    NSArray* a_widths = [s_widths componentsSeparatedByString: delim];
    NSArray* a_rotate = [s_rotates componentsSeparatedByString: delim];
    
    //NSString* s_id = [dict objectForKey:tag_id];
    //NSString* s_loc_id = [dict objectForKey:tag_loc_id];
    NSString* s_title = [dict objectForKey:tag_title];
    
    HOSTRoom* room = [[HOSTRoom alloc] init];
    [room setName: s_title];
    
    int min_len = MIN(MIN([a_shapes count], [a_names count]),MIN(MIN([a_coord_x count], [a_coord_y count]),MIN([a_heights count], [a_widths count])));
    
    for(int i = 0; i < min_len; i++) {
        NSString* name = [a_names objectAtIndex:i];
        NSString* shapeName = [a_shapes objectAtIndex:i];
        shapeName = [shapeName stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        int shape = [HOSTTable tableType:shapeName];
        float coordX = [[a_coord_x objectAtIndex:i] floatValue];
        float coordY = [[a_coord_y objectAtIndex:i] floatValue];
        float width = [[a_widths objectAtIndex:i] floatValue];
        float height = [[a_heights objectAtIndex:i] floatValue];
        float radians = 0 ;
        if(i < [a_rotate count]) {
            radians = [[a_rotate objectAtIndex:i] floatValue];
        }
        
        radians *= M_PI/180.0f;
        
        [[room tableList] addObject:[HOSTTable createTable:shape name:name xPos:coordX yPos:coordY width:width height:height radians:radians]];
        
    }
    
    return room;
}
@end