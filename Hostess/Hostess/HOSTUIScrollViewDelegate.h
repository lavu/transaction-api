//
//  HOSTUIScrollViewDelegate.h
//  Hostess
//
//  Created by Theodore Schnepper on 1/3/13.
//  Copyright (c) 2013 Theodore Schnepper. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HOSTUIScrollViewDelegate : NSObject <UIScrollViewDelegate>

-(id) init;

#pragma mark Scroll Events
-(void)scrollViewDidScroll:(UIScrollView *)scrollView;

-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView;

-(void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset;

-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate;

-(void)scrollViewDidScrollToTop:(UIScrollView *)scrollView;

-(void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView;

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView;

- (BOOL)scrollViewShouldScrollToTop:(UIScrollView *)scrollView;

#pragma mark Managing Zoom
-(void)scrollViewWillBeginZooming:(UIScrollView *)scrollView withView:(UIView *)view;

-(void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(float)scale;

-(void)scrollViewDidZoom:(UIScrollView *)scrollView;

-(UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView;
#pragma mark Responding to Scrolling Animations

-(void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView;

@end
