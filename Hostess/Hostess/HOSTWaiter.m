//
//  HOSTWaiter.m
//  Hostess
//
//  Created by Theodore Schnepper on 8/12/12.
//  Copyright (c) 2012 Theodore Schnepper. All rights reserved.
//

#import "HOSTWaiter.h"
#import "HOSTTable.h"

@implementation HOSTWaiter

@synthesize eID = _eID;
@synthesize index = _index;
@synthesize fullName = _name;
@synthesize firstName = _firstName;
@synthesize lastName = _lastName;
@synthesize color = _color;
@synthesize assignedTables = _assignedTables;
@synthesize timeCard = _timeCard;
@synthesize schedule = _schedule;
@synthesize view = _view;

- (id) init {
    _index = -1;
    _assignedTables = [[NSMutableArray alloc] init];
    static float hue = 0.0f;
    hue += 0.05f;  //208
    
    if(hue > 1.0f) {
        hue -= 1.0f;
    }
    float sat = 1.0f;
    float bri = 1.0f;
    
    _timeCard = [[HOSTTimeCard alloc] init];
    _schedule = [[HOSTSchedule alloc] init];
    

    /* Assigning a Random Color to each Worker */
    _color = [[UIColor alloc] initWithHue:hue saturation:sat brightness:bri alpha:1.0f];
    
    [self setColor: _color];
    
    return self;
}

- (id) initWithID: (int) eID firstName: (NSString*) fname lastName: (NSString*) lname {
    _eID = eID;
    
    if([fname isKindOfClass: [NSString class]] ){
        _firstName = fname;
    } else {
        _firstName = [NSString stringWithFormat: @"%@", fname];
    }
    
    if([lname isKindOfClass: [NSString class]]){
        _lastName = lname;
    } else {
        _firstName = [NSString stringWithFormat: @"%@", lname];
    }
    _name = [NSString stringWithFormat:@"%@ %@", fname, lname];
    
    return [self init];
}

- (BOOL) setColor:(UIColor *)color {
    if(color == NULL) {
        return FALSE;
    }
    _color = color;
    [_view setNeedsDisplay];
    
    return TRUE;
}

- (UIColor*) getColor {
    return _color;
}

- (void) toggleAssignment:(id)table {
    bool found = false;
    for(HOSTTable* t in _assignedTables) {
        if(t == table) {
            found = true;
            break;
        }
    }
    
    if(found) {
        [_assignedTables removeObject: table];
    } else {
        [_assignedTables addObject: table];
    }
    [_view setNeedsDisplay];
}


/**
 *  Defines a HOSTWaiter to be equal to another, only if the Employee ID's match.
 */
- (BOOL) isEqual:(id)object {
    if( self == object) {
        return TRUE;
    }
    
    if([object isKindOfClass:[HOSTWaiter class]]) {
        return [((HOSTWaiter*) object) eID] == [self eID];
    }
    
    return FALSE;
}

- (BOOL) isHere {
    if(_timeCard) {
        return [_timeCard isWithinTime: [NSDate date]];
    }
    return FALSE;
}

- (void) updatePunches: (NSDictionary*) dict {
    if(!dict) {
        return;
    }
    
    [_timeCard update:dict];
}

- (void) updateSchedules: (NSDictionary*) dict {
    if(!dict) {
        return;
    }
    
    [_schedule update: dict];
}

@end