//
//  HOSTTable.h
//  Hostess
//
//  Created by Theodore Schnepper on 8/12/12.
//  Copyright (c) 2012 Theodore Schnepper. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HOSTModel.h"
#import "HostTableInformation.h"
#import "HOSTOrder.h"

@interface HOSTTable : HOSTModel

@property (nonatomic, assign) int tableType;
@property (nonatomic, retain) NSString* tableName;
@property (nonatomic, assign) CGRect loc;
@property (nonatomic, assign) float radians;
@property (nonatomic, retain, readonly) NSMutableArray* assignedWaiters;
@property (nonatomic, assign)int waiterCount;
@property (nonatomic, assign, readonly) UIImage* image; //The image for this particular table. (may be unique)
@property (nonatomic, assign) bool useVector;
@property (nonatomic, assign) TableState state;
@property (nonatomic, assign) UIView* view; //The View Associated With this Table
@property (nonatomic, copy) HOSTOrder* currentOrder;


- (void) toggleAssignment: (id) waiter;
- (BOOL) assignWaiter: (id) waiter;
- (BOOL) removeWaiter: (id) waiter;
- (int) getWaiterCount;

+ (id) createTable: (int) type name: (NSString*) name xPos: (float) x yPos: (float) y width: (float) w height: (float) h radians: (float) r;

+ (int) tableType: (NSString*) shapeName;

@end
