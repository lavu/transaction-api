//
//  HOSTSchedule.h
//  Hostess
//
//  Created by Theodore Schnepper on 5/30/13.
//  Copyright (c) 2013 Theodore Schnepper. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HOSTModel.h"
#import "HOSTScheduleTime.h"

@interface HOSTSchedule : HOSTModel

@property NSMutableArray* schedules;

- (void) update: (NSDictionary*) dict;

@end
