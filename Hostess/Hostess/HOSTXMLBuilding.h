//
//  HOSTXMLBuilding.h
//  Hostess
//
//  Created by Theodore Schnepper on 8/17/12.
//  Copyright (c) 2012 Theodore Schnepper. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HOSTXMLBuilding : NSObject {
    NSXMLParser* parser;
    NSMutableArray* articles;
    NSMutableDictionary* item;
    NSString* currentElement;
    NSMutableString* elementValue;
    BOOL errorParsing;
}

- (id) initWithVerbose: (BOOL) verbose;
- (void)parseXMLFromData:(NSData *)xml;
- (NSArray *) result;

@end
