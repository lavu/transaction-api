//
//  HOSTLassoView.h
//  Hostess
//
//  Created by Theodore Schnepper on 8/22/12.
//  Copyright (c) 2012 Theodore Schnepper. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HOSTLassoView : UIView

@property (nonatomic, copy) NSMutableArray* points;
@property (nonatomic, assign) UIColor* drawColor;

- (void) addTouchPoint: (UITouch*) touch;
- (void) clearPoints;
- (BOOL) isRectWithinSelection: (UIView*) button;

@end
