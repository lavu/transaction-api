//
//  HOSTModel.m
//  Hostess
//
//  Created by Theodore Schnepper on 6/10/13.
//  Copyright (c) 2013 Theodore Schnepper. All rights reserved.
//

#import "HOSTModel.h"

@implementation HOSTModel

@synthesize views = _views;

- (id) init {
    _views = [[NSMutableArray alloc] init];
    return self;
}

- (BOOL) addView:(UIView *)view {
    if( [_views containsObject: view]){
        return false;
    }
    [((NSMutableArray*)_views) addObject: view];
    return true;
}

- (BOOL) removeView:(UIView *)view {
    if( ![_views containsObject: view]){
        return false;
    }
    
    [((NSMutableArray*) _views) removeObject: view];
    return true;
}

- (void) updateViews {
    for (UIView* view in _views) {
        [view setNeedsDisplay];
    }
}

@end
