//
//  HOSTScrollView.h
//  Hostess
//
//  Created by Theodore Schnepper on 8/23/12.
//  Copyright (c) 2012 Theodore Schnepper. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HOSTLassoView.h"

@interface HOSTScrollView : UIScrollView

@property (nonatomic, assign) HOSTLassoView* parent;

- (BOOL) locked;
- (void) setLocked:(BOOL) locked;
- (void) setSelector: (SEL) selector target: (id) target;

@end
