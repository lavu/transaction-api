//
//  HOSTLavuPOST.h
//  Hostess
//
//  Created by Theodore Schnepper on 8/17/12.
//  Copyright (c) 2012 Theodore Schnepper. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HOSTLoginInformation.h"

/**
 * 0x reserved for initial Testing
 * 10 - 29 reserved for Retrieving Initial Data
 * 30 - 49 reserved for Updating Data
 * 50 - 69 reserved for Communcation Updates
 *
 */
typedef enum {
    CommPing = 0,
    CommTest = 1,
    CommInitialLogin = 3,
    CommLogout = 4,
    CommSetLocation = 5,
    
    CommGetRooms = 11,
    CommGetTables = 12,
    CommGetEmployees = 13,
    CommGetTimeCards = 14,
    CommGetSchedules = 15,
    CommGetOrders = 16,
    CommGetOrderContents = 17,
    
    CommUpdateRooms = 111,
    CommUpdateTables = 112,
    CommUpdateEmployees = 113,
    CommUpdateTimeCards = 114,
    CommUpdateSchedules = 115,
    CommUpdateOrders = 116,
    CommUpdateOrderContents = 117,
    
    CommWriteAssignments = 301,
    CommWriteCreateOrder = 302,
    
    CommUnknown = 999999
} CommMode;

@interface HOSTLavuPOST : NSObject

/**
 * This method queries information from the lavu server's API.  By Providing the method the direct
 * information to pull store information, we are able to get any table that is specified.  Beyond
 * directly providing Date Name, Key, Token, Table and whether or not to pull valid XML, this method
 * allows a user to provide extra Tags and extra Values for anything else they may provide.
 * if either is NULL, then it will run without them.  However, any tag/value combination will be
 * appended as a POST variable in the form of @"%@=%@" where tag=value.  
 *
 * This allows for customization of filters, or to pull extra data fields as neccessary
 *
 */
+ (NSData*) pullInformationFromServer: (NSString*) server dataName: (NSString*) dataName key: (NSString*) key token: (NSString*) token table: (NSString*) table validXML: (BOOL) valid extraTags: (NSArray*) tags extraValues: (NSArray*) values;

+ (NSData*) queryURL: (NSString*) urlstring withData: (NSDictionary*) data;
+ (NSString*) getSessionID;

+ (void) setSessionID: (NSString*) session_id;
+ (id) json_decode: (NSData*) data;

+ (HOSTLoginInformation*) getLoginInformation;
+ (NSString*) getPOSTURL;
+ (void) resetLastUpdates;

@end
