//
//  HOSTTableInformation.m
//  Hostess
//
//  Created by Theodore Schnepper on 8/14/12.
//  Copyright (c) 2012 Theodore Schnepper. All rights reserved.
//

#import "HOSTTableInformation.h"

@implementation HOSTTableInformation

NSMutableArray* tableList;
NSMutableArray* imageList;
NSMutableArray* stateImageList;


NSString *tableNames[] = { @"table_circle.png",@"table_square.png", @"table_diamond.png",@"table_slant_right.png",@"table_slant_left.png",nil };

+ (NSString*) getTableImageName:(int) tableType {
    if(!tableList) {
        tableList = [[NSMutableArray alloc] init];
        [tableList addObject: @"table_circle.png"];
        [tableList addObject: @"table_square.png"];
        [tableList addObject: @"table_diamond.png"];
        [tableList addObject: @"table_slant_right.png"];
        [tableList addObject: @"table_slant_left.png"];
    }
    return tableNames[tableType];
}

+ (UIImage*) getTableImage:(int)tableType {
    if(!imageList) {
        imageList = [[NSMutableArray alloc] init];
        [self getTableImageName: 0];
        for(int i = 0; i < [tableList count]; i++){
            [imageList addObject: [UIImage imageNamed: [HOSTTableInformation getTableImageName:i]]];
        }
    }
    
    return [imageList objectAtIndex:tableType];
}

+ (CGPoint*) getVectorForTable: (int) tableType {
    CGPoint* result = malloc(sizeof(CGPoint) * 4);
    switch (tableType) {
        case 0: { //Circle
            free(result);
            return NULL;
            break;
        }
        case 1: { //Square
            result[0] = CGPointMake(0.0f, 0.0f);
            result[1] = CGPointMake(1.0f, 0.0f);
            result[2] = CGPointMake(1.0f, 1.0f);
            result[3] = CGPointMake(0.0f, 1.0f);
            break;
        }
        case 2: { //Diamond
            result[0] = CGPointMake(0.5f, 0.0f);
            result[1] = CGPointMake(1.0f, 0.5f);
            result[2] = CGPointMake(0.5f, 1.0f);
            result[3] = CGPointMake(0.0f, 0.5f);
            break;
        }
        case 3: { //Slant_Right
            result[0] = CGPointMake(0.65f, 0.0f);
            result[1] = CGPointMake(1.0f, 0.35f);
            result[2] = CGPointMake(0.35f, 1.0f);
            result[3] = CGPointMake(0.0f, 0.65f);
            break;
        }
        case 4: { //Slant_Left
            result[0] = CGPointMake(0.35f, 0.0f);
            result[1] = CGPointMake(1.0f, 0.65f);
            result[2] = CGPointMake(0.65f, 1.0f);
            result[3] = CGPointMake(0.0f, 0.35f);
            break;
        }
            
        default: {
            free(result);
            return NULL;
            break;
        }
    }
    
    return result;
}

+ (UIImage*) getStateImage: (TableState) state {
    if(!stateImageList) {
        stateImageList = [[NSMutableArray alloc] init];
        [stateImageList addObject: [UIImage imageNamed:@"i_states_empty.png"]];
        [stateImageList addObject: [UIImage imageNamed:@"i_states_occupied.png"]];
        [stateImageList addObject: [UIImage imageNamed:@"i_states_ordered.png"]];
        [stateImageList addObject: [UIImage imageNamed:@"i_states_kitchen.png"]];
        [stateImageList addObject: [UIImage imageNamed:@"i_states_checkprinted.png"]];
    }
    
    return [stateImageList objectAtIndex: state];
}

@end
