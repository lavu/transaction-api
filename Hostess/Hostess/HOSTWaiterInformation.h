//
//  HOSTWaiterInformation.h
//  Hostess
//
//  Created by Theodore Schnepper on 1/8/13.
//  Copyright (c) 2013 Theodore Schnepper. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HOSTWaiterInformation : NSObject

+ (UIImage*) getWaiterImage;

@end
