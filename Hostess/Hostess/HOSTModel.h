//
//  HOSTModel.h
//  Hostess
//
//  Created by Theodore Schnepper on 6/10/13.
//  Copyright (c) 2013 Theodore Schnepper. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HOSTModel : NSObject
@property NSArray* views;

- (void) updateViews;
- (BOOL) addView: (UIView*) view;
- (BOOL) removeView: (UIView*) view;

@end
