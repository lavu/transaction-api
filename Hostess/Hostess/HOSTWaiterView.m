//
//  HOSTWaiterView.m
//  Hostess
//
//  Created by Theodore Schnepper on 1/4/13.
//  Copyright (c) 2013 Theodore Schnepper. All rights reserved.
//

#import "HOSTWaiterView.h"

@implementation HOSTWaiterView

@synthesize backgroundImage = _backgroundImage;
@synthesize waiter = _waiter;
@synthesize waiterName = _waiterName;


- (id) init {
    self = [super init];
    [self setBackgroundColor: [UIColor colorWithRed:0.0f green:0.0f blue:0.0f alpha:0.0f]];
    _waiterName = [[UILabel alloc] init];
    [_waiterName setBackgroundColor: [UIColor colorWithRed:0.0f green:0.0f blue:0.0f alpha:0.0f]];
    
    _backgroundImage = [HOSTWaiterInformation getWaiterImage];
    [_waiterName setTextColor: [UIColor whiteColor]];
    [_waiterName setTextAlignment: NSTextAlignmentCenter];
    [self addSubview: _waiterName];
    
    return self;
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    [self setBackgroundColor: [UIColor colorWithRed:0.0f green:0.0f blue:0.0f alpha:0.0f]];
    _waiterName = [[UILabel alloc] init];
    _backgroundImage = [HOSTWaiterInformation getWaiterImage];
    [self addSubview: _waiterName];
    return self;
}

- (id) initWithBackgroundImage: (UIImage*) image {
    self = [self init];
    _backgroundImage = image;
    return self;
}

- (id) initWithWaiter: (HOSTWaiter*) waiter {
    self = [self init];
    _waiter = waiter;
    [_waiterName setText: [waiter firstName]];
    return self;
}

- (void) setFrame:(CGRect)frame {
    [super setFrame: frame];
    
    [_waiterName setFrame: CGRectMake(0.0f, frame.size.height - 25.0f, frame.size.width, 25.0f)];
}

- (void) setBackgroundColor:(UIColor *)backgroundColor {
    [super setBackgroundColor:backgroundColor];
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    
    CGContextRef c = UIGraphicsGetCurrentContext();
    CGRect frame = rect;
    
    //110 x 161
    //0.683
    frame.size.height = rect.size.height - 25.0f;
    frame.size.width = frame.size.height * 0.683f;
    frame.origin.x = rect.origin.x + ((rect.size.width - frame.size.width) / 2.0f);
    frame.origin.y = rect.origin.y;
    
    
    CGAffineTransform identity = CGAffineTransformIdentity;
    CGMutablePathRef path = CGPathCreateMutable();
    CGPoint square[4] = {
        CGPointMake(frame.origin.x, frame.origin.y),
        CGPointMake(frame.origin.x + frame.size.width, frame.origin.y),
        CGPointMake(frame.origin.x + frame.size.width, frame.origin.y + frame.size.height),
        CGPointMake(frame.origin.x, frame.origin.y + frame.size.height),
    };
    
    UIColor* waiterColor = [[_waiter getColor] copy];
    
    if([self backgroundImage]) {
        CGContextTranslateCTM(c, 0, frame.size.height);
        CGContextScaleCTM(c, 1.0f, -1.0f);
        
        CGContextDrawImage(c, frame, [[self backgroundImage] CGImage]);
    }
    
    if(![self isSelected]) {
        CGFloat colors[4];
        [waiterColor getHue:&colors[0] saturation:&colors[1] brightness:&colors[2] alpha:&colors[3]];
        
        //colors[1] *= 0.25f;
        colors[1] *= 0.10f;
        waiterColor = [UIColor colorWithHue:colors[0] saturation:colors[1] brightness:colors[2] alpha:colors[3]];
    }
    
    CGContextSetStrokeColorWithColor(c, [waiterColor CGColor]);
    CGContextSetFillColorWithColor(c, [waiterColor CGColor]);
            
    CGPathAddLines(path,&identity, square, 4);
    CGContextSetLineWidth(c, 1.0f);
    CGContextAddPath(c, path);
    CGContextSetBlendMode(c, kCGBlendModeColor);
    CGContextDrawPath(c, kCGPathFill);
    
    if([self backgroundImage]) {
        //CGContextTranslateCTM(c, 0, rect.size.height);
        //CGContextScaleCTM(c, 1.0f, -1.0f);
        
        CGContextSetBlendMode(c, kCGBlendModeDestinationIn);
        CGContextDrawImage(c, frame, [[self backgroundImage] CGImage]);
    }
}

@end