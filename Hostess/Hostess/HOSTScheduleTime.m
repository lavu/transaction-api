//
//  HOSTScheduleTime.m
//  Hostess
//
//  Created by Theodore Schnepper on 5/30/13.
//  Copyright (c) 2013 Theodore Schnepper. All rights reserved.
//

#import "HOSTScheduleTime.h"

@implementation HOSTScheduleTime

@synthesize startDate = _startDate;
@synthesize endDate = _endDate;
@synthesize sID = _sID;

- (id) initWithDictionary: (NSDictionary*) schedule {
    if(!schedule){
        return self;
    }
    
    NSString* idTag = @"id";
    NSString* timeInTag = @"start_datetime";
    NSString* timeOutTag = @"end_datetime";
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSString* start = [schedule objectForKey: timeInTag];
    start = [start stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    NSString* end = [schedule objectForKey: timeOutTag];
    end = [end stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    _sID = [[schedule objectForKey:idTag] integerValue];
    
    if(![start isEqualToString:@""]) {
        _startDate = [dateFormatter dateFromString:start];
    }
    
    if(![end isEqualToString:@""]) {
        _endDate = [dateFormatter dateFromString:end];
    }
    
    return self;
}

- (void) updateWithDictionary: (NSDictionary*) dict {
    if(!dict){
        return;
    }

    NSString* idTag = @"id";

    if([[dict objectForKey: idTag] integerValue] != _sID){
        return;
    }

    NSString* timeInTag = @"start_datetime";
    NSString* timeOutTag = @"end_datetime";

    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];

    NSString* start = [dict objectForKey: timeInTag];
    start = [start stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    NSString* end = [dict objectForKey: timeOutTag];
    end = [end stringByReplacingOccurrencesOfString:@"\n" withString:@""];

    if(![start isEqualToString:@""]) {
        _startDate = [dateFormatter dateFromString:start];
    }

    if(![end isEqualToString:@""]) {
        _endDate = [dateFormatter dateFromString:end];
    }
}

/**
 * Determines equality utilizing the Punch ID.
 */
- (BOOL) isEqual:(id)object {
    if(object == self) {
        return TRUE;
    }
    if(![object isKindOfClass:[HOSTScheduleTime class]]) {
        return FALSE;
    }
    return [object sID] == [self sID];
}
@end
