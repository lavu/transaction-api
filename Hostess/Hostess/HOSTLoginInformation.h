//
//  HOSTLoginInformation.h
//  Hostess
//
//  Created by Theodore Schnepper on 1/14/13.
//  Copyright (c) 2013 Theodore Schnepper. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HOSTLoginInformation : NSObject

@property NSDictionary* userDetails;
@property NSArray* locations;
@property NSString* activeLocation;
@property NSString* dataname;

@end
