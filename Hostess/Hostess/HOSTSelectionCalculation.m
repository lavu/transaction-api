//
//  HOSTSelectionCalculation.m
//  Hostess
//
//  Created by Theodore Schnepper on 8/23/12.
//  Copyright (c) 2012 Theodore Schnepper. All rights reserved.
//

#import "HOSTSelectionCalculation.h"

@implementation HOSTSelectionCalculation


float windingNumber(NSArray* points, CGPoint point) {
    CGPoint* previous = malloc(sizeof(CGPoint));
    BOOL first = true;
    
    float acc = 0.0f;
    for(NSValue* val in points) {
        if(first) {
            [val getValue:previous];
            first = ! first;
        } else {
            CGPoint* newpoint = malloc(sizeof(CGPoint));
            [val getValue:newpoint];
            
            CGPoint vec1;
            vec1.x = (*newpoint).x - point.x;
            vec1.y = (*newpoint ).y - point.y;
            
            vec1 = normalize(vec1);
            
            CGPoint vec2;
            vec2.x = (*previous).x - point.x;
            vec2.y = (*previous).y - point.y;
            
            vec2 = normalize(vec2);
            
            float an1 = atan2(vec1.y, vec1.x);
            float an2 = atan2(vec2.y, vec2.x);
            
            acc += (an2 - an1);
            //soh cah toa
            
            previous = newpoint;
        }
    }
    return acc / (2.0f * 3.14159f);
}

CGPoint normalize(CGPoint point) {
    CGPoint result;
    
    float d = sqrtf( powf(point.x, 2.0f) + powf(point.y, 2.0f));
    
    result.x = point.x / d;
    result.y = point.y / d;
    return result;
}

BOOL rectWithinSelection(CGRect rect, NSArray* points) {
    CGPoint p1 = rect.origin;
    CGPoint p2 = rect.origin;
    p2.x += rect.size.width;
    CGPoint p3 = rect.origin;
    p3.y += rect.size.height;
    CGPoint p4 = rect.origin;
    p4.x += rect.size.width;
    p4.y += rect.size.height;
    
 
    return windingNumber(points, p1) != 0.0f &&
        windingNumber(points, p2) != 0.0f &&
        windingNumber(points, p3) != 0.0f &&
        windingNumber(points, p4) != 0.0f;
}

@end