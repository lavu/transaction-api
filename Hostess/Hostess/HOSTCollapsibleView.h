//
//  HOSTCollapsibleView.h
//  Hostess
//
//  Created by Theodore Schnepper on 8/28/12.
//  Copyright (c) 2012 Theodore Schnepper. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HOSTCollapsibleSubView.h"

/**
 * This is meant to hold a collection of views, in this case, they will be "CollapsibleSubViews".
 * This view is meant to recieve signals from CollapsibleSubViews when They shrink or enlarge.
 *
 * After-which, it will automatically go and resize everything that's there, and adjust it's content
 * size accordingly.
 *
 */
@class HOSTCollapsibleSubView;

@interface HOSTCollapsibleView : UIScrollView {
    SEL _recalc;
}

- (void) recalculateContentArea;

@end
