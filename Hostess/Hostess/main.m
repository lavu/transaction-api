//
//  main.m
//  Hostess
//
//  Created by Theodore Schnepper on 8/11/12.
//  Copyright (c) 2012 Theodore Schnepper. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "HOSTAppDelegate.h"

int main(int argc, char *argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([HOSTAppDelegate class]));
    }
}
