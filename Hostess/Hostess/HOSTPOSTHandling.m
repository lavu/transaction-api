//
//  HOSTPOSTHandling.m
//  Hostess
//
//  Created by Theodore Schnepper on 8/16/12.
//  Copyright (c) 2012 Theodore Schnepper. All rights reserved.
//

#import "HOSTPOSTHandling.h"

@implementation HOSTPOSTHandling

- (id)initWithURL:(NSURL *)URL MIMEType:(NSString *)MIMEType expectedContentLength:(NSInteger)length textEncodingName:(NSString *)name
{
    NSLog(@"Response....");
    NSLog(@"MIMEType: %s", [MIMEType UTF8String]);
    NSLog(@"expected Length: %x", length);
    NSLog(@"Encoding: %s", [name UTF8String]);
    
    return self;
}

@end
