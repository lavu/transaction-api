//
//  HOSTTimecardViewController.m
//  Hostess
//
//  Created by Theodore Schnepper on 5/24/13.
//  Copyright (c) 2013 Theodore Schnepper. All rights reserved.
//

#import "HOSTTimecardViewController.h"

@interface HOSTTimecardViewController ()

@end

@implementation HOSTTimecardViewController

@synthesize waiter = _waiter;

- (void) setWaiter:(HOSTWaiter *)waiter {
    _waiter = waiter;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

typedef enum {
    TimeCardMainScrollView = 1,
    TimeCardDateView = 2,
    TimeCardTimeLineView = 3
} TimeCardViews;

- (void)viewDidLoad {
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    if(!_waiter){
        return;
    }
    
    HOSTTimeCard* timeCard = [_waiter timeCard];
    
    if(!timeCard){
        return;
    }
    
    int paddingTop = 5;
    int paddingBot = 5;
    
    UIScrollView* mainScrollView = (UIScrollView*) [[self mainView] viewWithTag: TimeCardMainScrollView];
    UIView* dateListView = [[self mainView] viewWithTag: TimeCardDateView];
    UIScrollView* timeLineScrollView = (UIScrollView*) [[self mainView] viewWithTag: TimeCardTimeLineView];
    
    [dateListView setBackgroundColor: [UIColor clearColor]];
    CGRect dateListViewFrame = [dateListView frame];
    NSMutableArray* dates = [[NSMutableArray alloc] init];
    NSMutableDictionary* punchesForDates = [[NSMutableDictionary alloc] init];
    
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    int allUnits = NSEraCalendarUnit |  NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit |
    NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit | NSWeekCalendarUnit | NSWeekdayCalendarUnit |
    NSWeekdayOrdinalCalendarUnit | NSQuarterCalendarUnit | NSWeekOfMonthCalendarUnit | NSWeekOfYearCalendarUnit |
    NSYearForWeekOfYearCalendarUnit | NSCalendarCalendarUnit | NSTimeZoneCalendarUnit ;
    
    NSArray* punches = [timeCard punches];
    for( int i = 0; i < [punches count]; i++ ){
        HOSTTimePunch* punch = [punches objectAtIndex: i ];
        
        if(![punch startPunch]){
            continue;
        }
        
        NSDateComponents* startPunch = [gregorian components: allUnits fromDate: [punch startPunch]];
        NSDateComponents* endPunch;
        if([punch endPunch]){
            endPunch = [gregorian components: allUnits fromDate: [punch endPunch]];
        } else {
            endPunch = [gregorian components: allUnits fromDate: [NSDate date]];
        }
        
        NSString* startPunchStr = [NSString stringWithFormat: @"%D-%D-%D", [startPunch year], [startPunch month], [startPunch day]];
        NSString* endPunchStr = [NSString stringWithFormat: @"%D-%D-%D", [endPunch year], [endPunch month], [endPunch day]];
        
        if(![dates containsObject: startPunchStr]){
            [dates addObject: startPunchStr];
        }
        
        if(![dates containsObject: endPunchStr]){
            [dates addObject: endPunchStr];
        }
        
        if( [startPunchStr isEqualToString: endPunchStr]){
            NSMutableArray* arr = [punchesForDates objectForKey: startPunchStr];
            if(!arr){
                arr = [[NSMutableArray alloc] init];
                [punchesForDates setObject: arr forKey:startPunchStr];
            }
            [arr addObject: startPunch];
            [arr addObject: endPunch];
        } else {
            NSMutableArray* arr = [punchesForDates objectForKey: startPunchStr];
            if(!arr){
                arr = [[NSMutableArray alloc] init];
                [punchesForDates setObject: arr forKey:startPunchStr];
            }
            
            NSDateComponents* newEnd = [[NSDateComponents alloc] init];
            [newEnd setHour: 23];
            [newEnd setMinute: 59];
            [newEnd setSecond: 59];
            [newEnd setDay: [startPunch day]];
            [newEnd setYear: [startPunch year]];
            [newEnd setMonth: [startPunch month]];
            
            [arr addObject: startPunch];
            [arr addObject: newEnd];
            
            NSDateComponents* newStart = [[NSDateComponents alloc] init];
            [newStart setHour: 0];
            [newStart setMinute: 0];
            [newStart setSecond: 0];
            [newStart setDay: [endPunch day]];
            [newStart setYear: [endPunch year]];
            [newStart setMonth: [endPunch month]];
            
            [arr addObject: newStart];
            [arr addObject: endPunch];
        }
    }
    
    float scale = 1.0f / 36.0f;
    
    CGRect frame = CGRectMake(0.0f, 0.0f, [dateListView frame].size.width, 25.0f);
    for( int i = 0; i < [dates count]; i++){
        frame.origin.y += paddingTop;
        frame.origin.y += frame.size.height;
        NSString* str = [dates objectAtIndex: i];
        UILabel* label = [[UILabel alloc] init];
        [label setFrame: frame];
        [label setText: str];
        [label setBackgroundColor: [UIColor clearColor]];
        [label setTextColor: [UIColor whiteColor]];
        [dateListView addSubview: label];
        [label setNeedsDisplay];
        
        NSArray* timecomponenets = [punchesForDates objectForKey: str];
        for( int j = 0; j < [timecomponenets count]; j+= 2){
            NSDateComponents* start = [timecomponenets objectAtIndex: j];
            NSDateComponents* end = [timecomponenets objectAtIndex: j + 1];
        
            int istart = [start second] + (60 * [start minute] + (3600 * [start hour]));
            int iend = [end second] + (60 * [end minute] + (3600 * [end hour]));
        
            istart *= scale;
            iend *= scale;
        
            CGRect timeRangeFrame = CGRectMake(istart, frame.origin.y, iend - istart, frame.size.height);
            UIView* range = [[UIView alloc] initWithFrame: timeRangeFrame];
            [range setBackgroundColor: [UIColor greenColor]];
            [range setBackgroundColor: [UIColor colorWithPatternImage: [UIImage imageNamed: @"time_card_punch_bg_pattern.png"]]];
            [timeLineScrollView addSubview: range];
            [range setNeedsDisplay];
        }
        frame.origin.y += paddingBot;
    }
    
    for(float i = 1; i < 24; i++){
        UILabel* timeHeader = [[UILabel alloc] init];
        [timeHeader setBackgroundColor: [UIColor clearColor]];
        [timeHeader setTextColor: [UIColor whiteColor]];
        [timeHeader setText: [NSString stringWithFormat: @"%d", (int)i]];
        [timeHeader setTextAlignment: NSTextAlignmentLeft];
        CGRect timeHeaderFrame = CGRectMake(i * 3600.0f * scale, 0.0f, 3600.0f * scale, 25.0f );
        
        [timeHeader setFrame: timeHeaderFrame];
        
        [timeLineScrollView addSubview: timeHeader];
        [timeHeader setNeedsDisplay];
    }

    dateListViewFrame.size.height = MAX(dateListViewFrame.size.height, frame.size.height + frame.origin.y);
    [dateListView setFrame: dateListViewFrame];
    CGSize mainScrollViewSize = [mainScrollView contentSize];
    mainScrollViewSize.height = MAX(mainScrollViewSize.height, dateListViewFrame.size.height);
    [mainScrollView setContentSize: mainScrollViewSize];
    
    CGSize timeLineScrollViewSize = [timeLineScrollView contentSize];
    timeLineScrollViewSize.height = mainScrollViewSize.height;
    timeLineScrollViewSize.width = 3600 * 24 * scale;
    [timeLineScrollView setContentSize: timeLineScrollViewSize];
    [timeLineScrollView setBackgroundColor: [UIColor colorWithPatternImage: [UIImage imageNamed: @"timeline_bg_pattern"]]];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setMainView:nil];
    [super viewDidUnload];
}
@end
