//
//  HOSTTimePunch.m
//  Hostess
//
//  Created by Theodore Schnepper on 8/21/12.
//  Copyright (c) 2012 Theodore Schnepper. All rights reserved.
//

#import "HOSTTimePunch.h"

@implementation HOSTTimePunch

@synthesize startPunch = _startPunch;
@synthesize endPunch = _endPunch;
@synthesize pID = _pID;

- (id) initWithDictionary: (NSDictionary*) punchInfo {
    if(!punchInfo) {
        return self;
    }
    
    NSString* idTag = @"id";
    NSString* timeInTag = @"time";
    NSString* timeOutTag = @"time_out";
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSString* start = [punchInfo objectForKey: timeInTag];
    start = [start stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    NSString* end = [punchInfo objectForKey: timeOutTag];
    end = [end stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    _pID = [[punchInfo objectForKey:idTag] integerValue];
    
    if(![start isEqualToString:@""]) {
        _startPunch = [dateFormatter dateFromString:start];
    }
    
    if(![end isEqualToString:@""]) {
        _endPunch = [dateFormatter dateFromString:end];
    }
    
    return self;
}

- (BOOL) timeWithin: (NSDate*) date {
    if(_endPunch && _startPunch) {
        return [date timeIntervalSince1970] >= [_startPunch timeIntervalSince1970] && [date timeIntervalSince1970] <= [_endPunch timeIntervalSince1970];
    }
    if(_startPunch) {
        return [date timeIntervalSince1970] >= [_startPunch timeIntervalSince1970];
    }
    return FALSE;
}

- (BOOL) hasEndingPunch {
    if(!_endPunch) {
        return FALSE;
    }
    return TRUE;
}

- (NSTimeInterval) getTimeBetweenPunches {
    if(_endPunch && _startPunch)
        return [_endPunch timeIntervalSince1970] - [_startPunch timeIntervalSince1970];
    return 0;
    
}

/**
 * Determines equality utilizing the Punch ID.
 */
- (BOOL) isEqual:(id)object {
    if(object == self) {
        return TRUE;
    }
    if(![object isKindOfClass:[HOSTTimePunch class]]) {
        return FALSE;
    }
    return [object pID] == [self pID];
}

- (void) updatePunch: (NSDictionary*) dict {
    NSString* timeInTag = @"time";
    NSString* timeOutTag = @"time_out";
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSString* start = [dict objectForKey: timeInTag];
    start = [start stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    NSString* end = [dict objectForKey: timeOutTag];
    end = [end stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    
    if(![start isEqualToString:@""]) {
        _startPunch = [dateFormatter dateFromString:start];
    }
    
    if(![end isEqualToString:@""]) {
        _endPunch = [dateFormatter dateFromString: end];
    }
}

@end