//
//  HOSTLavuPOST.m
//  Hostess
//
//  Created by Theodore Schnepper on 8/17/12.
//  Copyright (c) 2012 Theodore Schnepper. All rights reserved.
//

#import "HOSTLavuPOST.h"
#include <asl.h>

const bool verbose = true;
NSString* sessionid;
NSMutableArray* lastUpdateList;
//HOSTLoginInformation* loginDetails;



@implementation HOSTLavuPOST

+ (NSData*) pullInformationFromServer: (NSString*) server dataName: (NSString*) dataName key: (NSString*) key token: (NSString*) token table: (NSString*) table validXML: (BOOL) valid extraTags: (NSArray*) tags extraValues: (NSArray*) values {
    //************************************************************************
    // Getting Information from POST
    //************************************************************************
    NSURL* url = [NSURL URLWithString: server];
    
    NSMutableString* params = [[NSMutableString alloc] init];
    [params appendFormat:@"dataname=%@", dataName];
    [params appendFormat:@"&key=%@", key];
    [params appendFormat:@"&token=%@", token];
    [params appendFormat:@"&table=%@", table];
    
    if(valid) {
        [params appendFormat:@"&valid_xml=%@",@"1"];
    }
    
    if((tags && values) && (tags.count == values.count)) { // Ensure that we have both, and that they're the same size
        for(int i = 0; i < tags.count; i++) {
            if([[tags objectAtIndex:i] isKindOfClass:[NSString class]]) {
                if([[values objectAtIndex:i] isKindOfClass:[NSString class]]) { //Makes sure they're both Strings!
                    [params appendFormat:@"&%@=%@",[tags objectAtIndex:i], [values objectAtIndex:i]];
                }
            }
        }
    }
    
    //NSLog(@"%@", params);
    
    NSMutableURLRequest* request = [[NSMutableURLRequest alloc] initWithURL: url];
    [request setHTTPMethod:@"POST"];
    
    [request setHTTPBody:[params dataUsingEncoding: NSUTF8StringEncoding]];
    
    
    NSError* error = [[NSError alloc] init];
    NSHTTPURLResponse* urlResponse = nil;
                      
    NSData* responseData = [NSURLConnection sendSynchronousRequest:request returningResponse: &urlResponse error: &error];
    
    //NSLog(@"Response code: %d", [urlResponse statusCode]);
    
    if([urlResponse statusCode] >= 200 && [urlResponse statusCode] < 300){
        if(responseData) {
            //NSString* result = [[NSString alloc] initWithData:responseData encoding: NSUTF8StringEncoding];
            //NSLog(@"%@", [[NSString alloc] initWithData:responseData encoding: NSUTF8StringEncoding]);
            return responseData;
        }
    }
    //************************************************************************
    //
    //  End Getting Information from POST
    //************************************************************************

    return NULL;
}

+ (NSData*) queryURL: (NSString*) urlstring withData: (NSDictionary*) data {
    NSDate* now = [NSDate date];
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
    
    int lastUpdateIndex = [[data objectForKey: @"cmd"] integerValue];
    if(!lastUpdateList) {
        [HOSTLavuPOST resetLastUpdates];
    }
    NSURL* url = [NSURL URLWithString: urlstring];
    NSMutableString* params = [[NSMutableString alloc] init];
    
    NSMutableURLRequest* request = [[NSMutableURLRequest alloc] initWithURL: url];

    [request setHTTPMethod:@"POST"];
    {//Convert the Dictionary to GET style Variables
        NSMutableArray* array = [[NSMutableArray alloc] init];
        NSEnumerator* enumerator = [data keyEnumerator];
        NSString* next = [enumerator nextObject];
        while(next) {
            NSString* value = [data objectForKey:next];
            [array addObject:[NSString stringWithFormat:@"%@=%@", next, value]];
            next = [enumerator nextObject];
        }
        if(sessionid) {
            [array addObject:[NSString stringWithFormat:@"session_id=%@", sessionid]];
        }
        
        [array addObject: [NSString stringWithFormat:@"device_time=%@", [formatter stringFromDate: now]]];
        if([lastUpdateList objectAtIndex: lastUpdateIndex]) {
            [array addObject: [NSString stringWithFormat: @"last_update=%@", [formatter stringFromDate: [lastUpdateList objectAtIndex: lastUpdateIndex]]]];
        }
        
        [params appendString: [array componentsJoinedByString:@"&"]];
    }
    [request setHTTPBody: [params dataUsingEncoding:NSUTF8StringEncoding]];
    
    if(verbose) {
        NSLog(@"%@?%@", urlstring,params);
    }
    
    NSError* error = [[NSError alloc] init];
    NSHTTPURLResponse* urlResponse = nil;
    
    NSData* responseData = [NSURLConnection sendSynchronousRequest:request returningResponse: &urlResponse error: &error];
    
    if([urlResponse statusCode] >= 200 && [urlResponse statusCode] < 300) {
        if(responseData) {
            NSString* responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
            if(verbose) {
                NSLog(@"%@", responseString);
            }
            
            if([responseString rangeOfString:@"\"status\" : \"success\""].location != NSNotFound) {
                [lastUpdateList setObject:now atIndexedSubscript: lastUpdateIndex];
            }
            
            if(lastUpdateIndex == CommLogout) {
                [HOSTLavuPOST resetLastUpdates];
            }
            return responseData;
        }
    }
    return NULL;
}

+ (id) json_decode: (NSData*) data {
    NSError* json_error = NULL;
    
    id deserialized = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers | NSJSONReadingMutableLeaves | NSJSONReadingAllowFragments error: &json_error];
    
    if(json_error) {
        NSLog(@"Error Parsing Respone from Request\n Error:\n %d | %@: %@", [json_error code], [json_error domain], [json_error helpAnchor]);
    } else {
        if([deserialized isKindOfClass:[NSDictionary class]]) {
            NSDictionary* dict = deserialized;
            if([@"success" isEqual:[dict objectForKey:@"status"]]) {
                id result = [dict objectForKey:@"result"];
                return result;
            } else {
                NSString* result = [dict objectForKey:@"result"];
                NSLog(@"Request Could Not Be Fufilled\nReason: %@\n", result);
            }
        }
    }
    return NULL;
}

+ (NSString*) getSessionID {
    return sessionid;
}

+ (void) setSessionID: (NSString*) session_id {
    sessionid = session_id;
}

+ (HOSTLoginInformation*) getLoginInformation {
    static HOSTLoginInformation* loginDetails;
    if(!loginDetails) {
        loginDetails = [[HOSTLoginInformation alloc] init];
    }
    return loginDetails;
}

+ (NSString*) getPOSTURL {
    static NSString* urlstring;
    if(!urlstring) {
        urlstring = @"https://admin.poslavu.com/dev/lib/hostess/test.php";
    }
    return urlstring;
}

+ (void) resetLastUpdates {
    lastUpdateList = [[NSMutableArray alloc] init];
    for(int i = 0; i < 100; i++) {
        NSDate* date = [NSDate dateWithTimeIntervalSince1970: 0.0f];
        [lastUpdateList addObject: date];
    }
}

@end