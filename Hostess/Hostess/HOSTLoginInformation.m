//
//  HOSTLoginInformation.m
//  Hostess
//
//  Created by Theodore Schnepper on 1/14/13.
//  Copyright (c) 2013 Theodore Schnepper. All rights reserved.
//

#import "HOSTLoginInformation.h"

@implementation HOSTLoginInformation

@synthesize userDetails;
@synthesize locations;
@synthesize activeLocation;
@synthesize dataname;

@end
