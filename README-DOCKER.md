## To be able to build
First we need to mount the secrets folder which should include the following 3 files:
```bash
External/secrets/config_files/myinfo.php
External/secrets/config_files/salesforceCredentials.php
External/secrets/config_files/ZuoraConfig.php
External/secrets/config_files/sasl_passwd
```

## Mount and Build it
```bash
mkdir -p ~/lavu_repo/External/devops
mkdir -p ~/lavu_repo/External/secrets/config_files

sudo mount -o bind ~/DevOps/ ~/lavu_repo/External/devops
sudo mount -o bind ~/repo_secrets ~/lavu_repo/External/secrets/config_files

docker build . --build-arg SSH_PRIVATE_KEY="$(cat ~/.ssh/deployment)" \
               --build-arg APP_VERSION=3.8.4.6 \
               --build-arg INVENTORY_VERSION=2.0.3 \
               --build-arg DASHBOARD_VERSION=1.0.1 \
               --build-arg BASTION_HOST=officelink.lavu
```

Note that BASTION_HOST was added, for local environments use: officelink.poslavu.com and for CI envs officelink.lavu
