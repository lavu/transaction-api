package apns

const (
	APNNotificationCommandSend  = 0x02
	APNNotificationCommandError = 0x08
)

const (
	APNErrorResponseStatusNoErrors           = 0x00
	APNErrorResponseStatusProcessingError    = 0x01
	APNErrorResponseStatusMissingDeviceToken = 0x02
	APNErrorResponseStatusMissingTopic       = 0x03
	APNErrorResponseStatusMissingPayload     = 0x04
	APNErrorResponseStatusInvalidTokenSize   = 0x05
	APNErrorResponseStatusInvalidTopicSize   = 0x06
	APNErrorResponseStatusInvalidPayloadSize = 0x07
	APNErrorResponseStatusInvalidToken       = 0x08
	APNErrorResponseStatusShutdown           = 0x0A
	APNErrorResponseStatusUnknown            = 0xFF
)

const (
	APNFrameDataItemIDDeviceToken            = 0x01
	APNFrameDataItemIDDPayload               = 0x02
	APNFrameDataItemIDNotificationIdentifier = 0x03
	APNFrameDataItemIDExpirationDate         = 0x04
	APNFrameDataItemIDPriority               = 0x05
)

const (
	APNFrameDataItemDataLengthDevicetoken            = 32
	APNFrameDataItemDataLengthPayload                = 256 //upper limit
	APNFrameDataItemDataLengthNotificationIdentifier = 4
	APNFrameDataItemDataLengthExpirationDate         = 4
	APNFrameDataItemDataLengthPriority               = 1
)

type APNErrorResponseStatus byte

func (err APNErrorResponseStatus) Error() string {
	switch err {
	case APNErrorResponseStatusNoErrors:
		return ""
	case APNErrorResponseStatusProcessingError:
		return "Error Processing the Message"
	case APNErrorResponseStatusMissingDeviceToken:
		return "Missing Device Token"
	case APNErrorResponseStatusMissingTopic:
		return "Missing Notification Topic"
	case APNErrorResponseStatusMissingPayload:
		return "Missing Payload"
	case APNErrorResponseStatusInvalidTokenSize:
		return "Provided Token Size is Invalid"
	case APNErrorResponseStatusInvalidTopicSize:
		return "Provided Topic Size is Invalid"
	case APNErrorResponseStatusInvalidPayloadSize:
		return "Provided Payload Size is Invalid"
	case APNErrorResponseStatusInvalidToken:
		return "Provided Token is Invalid"
	case APNErrorResponseStatusShutdown:
		return "Need to Shutdown"
	case APNErrorResponseStatusUnknown:
	default:
	}
	return "Unknown Error"
}

type APNNotificationReadWriteError byte

func (err APNNotificationReadWriteError) Error() string {
	switch err {
	case 0x0:
		return ""
	case 0x1:
		return "Byte buffer too small to Read"
	case 0x2:
		return "Error Command was not What was Expected"
	case 0x3:
		return "Not enough capacity in Bytes buffer"
	case 0x4:
		return "Device Token Length Does Not Match What was Expected"
	case 0x5:
		return "Notification Identifier Length Does Not Match What was Expected"
	case 0x6:
		return "Expiration Date Length Does Not Match What was Expected"
	case 0x7:
		return "Priority Length Does Not Match What was Expected"
	case 0x8:
		return "Payload Length Was Greater than it Should be"
	case 0xA:
		return "Byte Buffer was smaller than specified Frame Length"
	default:
		return "Unknown Error"
	}
}

type APNErrorResponse struct {
	Command    byte
	Status     APNErrorResponseStatus
	Identifier uint
}

func (apnErrorResponse *APNErrorResponse) Write(p []byte) (n int, err error) {
	if len(p) < 6 {
		n = 0
		err = APNNotificationReadWriteError(0x1)
		return
	}

	if p[0] != APNNotificationCommandError {
		n = 1
		err = APNNotificationReadWriteError(0x2)
	}
	apnErrorResponse.Command = p[0]
	apnErrorResponse.Status = APNErrorResponseStatus(p[1])
	apnErrorResponse.Identifier = uint(p[2])<<24 | uint(p[3])<<16 | uint(p[4])<<8 | uint(p[5])

	n = 6
	err = nil
	return
}

func (apnErrorResponse *APNErrorResponse) Read(p []byte) (n int, err error) {
	if len(p) < 6 {
		n = 0
		err = APNNotificationReadWriteError(0x3)
		return
	}

	if apnErrorResponse.Command != APNNotificationCommandError {
		n = 1
		err = APNNotificationReadWriteError(0x2)
		return
	}
	p[0] = apnErrorResponse.Command
	p[1] = byte(apnErrorResponse.Status)
	p[2] = (byte(apnErrorResponse.Identifier) >> 24) & 0xFF
	p[3] = (byte(apnErrorResponse.Identifier) >> 16) & 0xFF
	p[4] = (byte(apnErrorResponse.Identifier) >> 8) & 0xFF
	p[5] = (byte(apnErrorResponse.Identifier)) & 0xFF
	// p[2:5] = apnErrorResponse.Identifier

	n = 6
	err = nil
	return
}

type APNFrameData struct {
	ItemID         byte
	ItemDataLength uint //2 Bytes
	ItemData       []byte
}

func (apnFrameData *APNFrameData) Write(p []byte) (n int, err error) {
	if len(p) < 3 {
		n = 0
		err = APNNotificationReadWriteError(0x1)
		return
	}

	apnFrameData.ItemID = p[0]
	apnFrameData.ItemDataLength = uint(p[1])<<8 | uint(p[2])

	switch apnFrameData.ItemID {
	case APNFrameDataItemIDDeviceToken:
		if apnFrameData.ItemDataLength != APNFrameDataItemDataLengthDevicetoken {
			n = 2
			err = APNNotificationReadWriteError(0x4)
			return
		}
		break
	case APNFrameDataItemIDNotificationIdentifier:
		if apnFrameData.ItemDataLength != APNFrameDataItemDataLengthNotificationIdentifier {
			n = 2
			err = APNNotificationReadWriteError(0x5)
			return
		}
		break
	case APNFrameDataItemIDExpirationDate:
		if apnFrameData.ItemDataLength != APNFrameDataItemDataLengthExpirationDate {
			n = 2
			err = APNNotificationReadWriteError(0x6)
			return
		}
		break
	case APNFrameDataItemIDPriority:
		if apnFrameData.ItemDataLength != APNFrameDataItemDataLengthPriority {
			n = 2
			err = APNNotificationReadWriteError(0x7)
			return
		}
		break
	case APNFrameDataItemIDDPayload:
		if apnFrameData.ItemDataLength > APNFrameDataItemDataLengthPayload {
			n = 2
			err = APNNotificationReadWriteError(0x8)
			return
		}
		break
	default:
		n = 2
		err = APNNotificationReadWriteError(0x9)
		return
	}

	endingLen := 3 + apnFrameData.ItemDataLength

	if endingLen > uint(len(p)) {
		n = 2
		err = APNNotificationReadWriteError(0xA)
		return
	}

	apnFrameData.ItemData = p[3:endingLen]

	n = int(endingLen)
	err = nil
	return
}

func (apnFrameData *APNFrameData) Read(p []byte) (n int, err error) {
	if len(p) < 3+len(apnFrameData.ItemData) {
		n = 0
		err = APNNotificationReadWriteError(0x3)
	}

	switch apnFrameData.ItemID {
	case APNFrameDataItemIDDeviceToken:
		if apnFrameData.ItemDataLength != APNFrameDataItemDataLengthDevicetoken {
			n = 0
			err = APNNotificationReadWriteError(0x4)
			return
		}
		break
	case APNFrameDataItemIDNotificationIdentifier:
		if apnFrameData.ItemDataLength != APNFrameDataItemDataLengthNotificationIdentifier {
			n = 0
			err = APNNotificationReadWriteError(0x5)
			return
		}
		break
	case APNFrameDataItemIDExpirationDate:
		if apnFrameData.ItemDataLength != APNFrameDataItemDataLengthExpirationDate {
			n = 0
			err = APNNotificationReadWriteError(0x6)
			return
		}
		break
	case APNFrameDataItemIDPriority:
		if apnFrameData.ItemDataLength != APNFrameDataItemDataLengthPriority {
			n = 0
			err = APNNotificationReadWriteError(0x7)
			return
		}
		break
	case APNFrameDataItemIDDPayload:
		if apnFrameData.ItemDataLength > APNFrameDataItemDataLengthPayload {
			n = 0
			err = APNNotificationReadWriteError(0x8)
			return
		}
		break
	default:
		n = 0
		err = APNNotificationReadWriteError(0x9)
		return
	}

	p[0] = apnFrameData.ItemID
	p[1] = byte(apnFrameData.ItemDataLength >> 8 & 0xFF)
	p[2] = byte(apnFrameData.ItemDataLength & 0xFF)
	// p[1] = []byte(apnFrameData.ItemDataLength)[2]
	// p[2] = []byte(apnFrameData.ItemDataLength)[3]

	endingLen := 3
	for i := 0; i < int(apnFrameData.ItemDataLength); i++ {
		p[3+i] = apnFrameData.ItemData[i]
		endingLen++
	}

	n = endingLen
	err = nil
	return
}

type APNNotification struct {
	Command     byte
	FrameLength uint
	FrameData   []APNFrameData
}

func (apnNotification *APNNotification) Write(p []byte) (n int, err error) {
	apnNotification.Command = p[0]
	apnNotification.FrameLength = uint(p[1]<<24)&0xFF | uint(p[2]<<16)&0xFF | uint(p[3]<<8)&0xFF | uint(p[4])&0xFF
	apnNotification.FrameData = make([]APNFrameData, 0)

	if uint(len(p)-5) < apnNotification.FrameLength {
		n = 5
		err = APNNotificationReadWriteError(0x1)
		return
	}

	totalWrite := 5
	inc := 0
	for uint(totalWrite-5) < apnNotification.FrameLength {
		slice := p[(totalWrite):len(p)]

		dataFrame := APNFrameData{}
		writeLen, writeErr := dataFrame.Write(slice)
		if writeErr != nil {
			n = totalWrite + writeLen
			err = writeErr
			return
		}
		totalWrite += writeLen

		if len(apnNotification.FrameData) <= inc {
			tempArray := make([]APNFrameData, len(apnNotification.FrameData)+1)
			copy(tempArray, apnNotification.FrameData)
			apnNotification.FrameData = tempArray

		}
		apnNotification.FrameData[inc] = dataFrame
		inc++
	}

	return totalWrite, nil
}

func (apnNotification *APNNotification) Read(p []byte) (n int, err error) {
	if len(p) < 5 {
		n = 0
		err = APNNotificationReadWriteError(0x3)
		return
	}

	p[0] = apnNotification.Command
	p[1] = byte((apnNotification.FrameLength >> 24) & 0xFF)
	p[2] = byte((apnNotification.FrameLength >> 16) & 0xFF)
	p[3] = byte((apnNotification.FrameLength >> 8) & 0xFF)
	p[4] = byte((apnNotification.FrameLength) & 0xFF)
	totalRead := 5

	for i := 0; i < len(apnNotification.FrameData); i++ {
		byteBuffer := make([]byte, len(p))
		readLen, readErr := apnNotification.FrameData[i].Read(byteBuffer)
		if readErr != nil {
			n = totalRead + readLen
			err = readErr
			return
		}
		for j := 0; j < readLen; j++ {
			p[totalRead] = byteBuffer[j]
			totalRead++
		}
		// p[(i + 5):(len(byteBuffer) + 5 + i)] = byteBuffer
		// totalRead += readLen
	}

	n = totalRead
	err = nil
	return
}

// "aps" : {
// 	"alert" : "\w+" | {
// 		"body" : "\w+", text part of message
// 		"action-loc-key" : null | "\w+",
// 		"loc-key" : "\w+",
// 		"loc-args" : [("\w+",)*"\w+"],
// 		"launch-image" : "\w+" filename of image file
// 	},
// 	"badge" : \d+,
// 	"sound" : "soundfilename",
// 	"content-available" : [0-1]
// },
// extra data...
