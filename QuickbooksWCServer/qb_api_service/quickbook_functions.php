<?php

    require_once('/home/eventrentalsystems/public_html/cp/functions.php');
    require_once(dirname(__FILE__).'/QBXMLClasses.php');
    require_once(dirname(__FILE__).'/json.php');
    require_once(dirname(__FILE__).'/logging/bd_logging.php');
    $Conn = ers_connect_db();

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //
    //  F U N C T I O N S     F O R    A C C O U N T    M A N A G E M E N T    A N D    G E N E R A L    C O N N E C T I O N    M A N A G E M E N T.
    //
    function tryToInsertNewAccountForQBIntegration($user_name, $password_2be_hashed, $dataname, $qbw_file_path){
        $doesRowAlreadyExist = getAccountWithMatchedUsernamePassword($user_name, $password_2be_hashed);
        $doesRowAlreadyExist = $doesRowAlreadyExist ? true : false;
        if($doesRowAlreadyExist){ return array('status' => 'failed', 'fail_reason' => 'username_password_pair_already_exists'); }
        $password_hash = sha1($password_2be_hashed.'JosephStalin');
        $ticket_session_identifier = generateUniqueQBWCTicket();
        $insertArr = array('user_name' => $user_name, 
                           'user_password_hash' => $password_hash, 
                           'data_name' => $dataname, 
                           'ticket_session_identifier' => $ticket_session_identifier,
                           'qbw_file_path' => $qbw_file_path);
        $insertQuery  = "INSERT INTO `accounts` (`user_name`,`user_password_hash`, `data_name`,`ticket_session_identifier`,`qbw_file_path`) ";
        $insertQuery .= "VALUES ('[user_name]','[user_password_hash]','[data_name]','[ticket_session_identifier]','[qbw_file_path]')";
        $result = ers_query($insertQuery, $insertArr);
        if(!$result){ echo mysql_error(); error_log("MySQL error in ".__FILE__.' error:' . mysql_error()); return array('status'=>'failed','fail_reason'=>'db_error'); }
        if(!mysql_insert_id()){ 
            echo 'failed insert'; error_log("MySQL error in ".__FILE__.' error:' . mysql_error()); 
            return array('status'=>'failed','fail_reason'=>'db_error'); 
        }
        return array('status'=>'success', 'insert_id'=>mysql_insert_id());
    }
    
    function getAccountWithMatchedUsernamePassword($user_name, $password){
        $result = ers_query("SELECT * FROM `accounts` where `user_name`='[1]' AND `user_password_hash`='[2]'", $user_name, sha1($password.'JosephStalin'));
        if(!$result){
            error_log("Error in quickbooks/BrianQBInterface/qb_utility_functions.php mysql error:".mysql_error());
            return false;
        }
        if(mysqli_num_rows($result)){
            return mysqli_fetch_assoc($result);
        }
        return false;
    }
    
    function getAccountRowForTicket($ticket){
        $result = ers_query("SELECT * FROM `accounts` where `ticket_session_identifier`='[1]'", $ticket);
        if(!$result){
            error_log("MySQL Error in ".__FILE__." error: " . mysql_error());
            return false;
        }
        if(mysqli_num_rows($result)){
            return mysqli_fetch_assoc($result);
        }
        return false;
    }
    
    function createAndInsertTicketSequenceIdentifier($accountsRowID, $dataname){
        $newTicketSequenceIdentifier = generateUniqueQBWCTicket();
        if(empty($newTicketSequenceIdentifier)){
            return false;
        }
        $result = ers_query("UPDATE `accounts` SET `ticket_session_identifier`='[1]' WHERE `id`='[2]'", $newTicketSequenceIdentifier,$accountsRowID);
        if(!$result){ error_log('MySQL error in'.__FILE__.' mysql_error:'.mysql_error()); }
        return $newTicketSequenceIdentifier;
    }
    
    function clearTicketIdentifierOnAccount($ticket){
        $account = getAccountRowForTicket($ticket);
        $result = ers_query("UPDATE `accounts` SET `ticket_session_identifier`='' WHERE `id`='[1]'", $account['id']);
        if(!$result){ error_log('MySQL error in '.__FILE__.' mysql error:'.mysql_error()); return false; }
        return mysql_affected_rows();
    }
    
    function generateUniqueQBWCTicket(){
        $hashPart1 = date('Y-m-d H:m:s:').mt_rand();
        $potentialTicketID = sha1($hashPart1);
        $accounts_result = ers_query("SELECT * FROM `accounts` WHERE `ticket_session_identifier`='[1]'",$potentialTicketID);
        $queue_result    = ers_query("SELECT * FROM `queue` WHERE `ticket_session_identifier`='[1]'",$potentialTicketID);
        if(!$accounts_result){error_log('Error -af- in generateUniqueQBWCTicket() in '.__FILE__.' mysql error:' . mysql_error()); return false;}
        if(!$queue_result){   error_log('Error -fe- in generateUniqueQBWCTicket() in '.__FILE__.' mysql error:' . mysql_error()); return false;}
        while(mysqli_num_rows($accounts_result) || mysqli_num_rows($queue_result)){
            $hashPart1 .= 'j';
            $potentialTicketID = sha1($hashPart1);
            $accounts_result = ers_query("SELECT * FROM `accounts` WHERE `ticket_session_identifier`='[1]'",$potentialTicketID);
            $queue_result    = ers_query("SELECT * FROM `queue` WHERE `ticket_session_identifier`='[1]'",   $potentialTicketID);
            if(!$accounts_result){error_log('Error -sdf- in generateUniqueQBWCTicket() in '.__FILE__.' mysql error:' . mysql_error()); return false;}
            if(!$queue_result){   error_log('Error -few- in generateUniqueQBWCTicket() in '.__FILE__.' mysql error:' . mysql_error()); return false;}
        }
        return $potentialTicketID;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //
    //  G E N E R A L  (NOT SPECIFIC TO ANY FUNCTION/ALG.)  F U N C T I O N S    F O R    Q U E U E    P R O C E S S I N G . . . 
    //

    function pushRequestOntoQueue($sanitizedXMLsArr, $partialTransactionResponseHandlers, $data_name, &$error){
        $sanitizedXMLsDelimStr = getTransactionDelimiter() . implode(getTransactionDelimiter(), $sanitizedXMLsArr);
        $handlingFunctionsStr  = getTransactionDelimiter() . implode("\n".getTransactionDelimiter(), $partialTransactionResponseHandlers);
        $qStr  = "INSERT INTO `queue` (`ticket_session_identifier`,`data_name`,`sendRequestXMLs`,`server_date_time_created`,`receivedResponseHandlers`) ";
        $qStr .= "VALUES (null, '[1]','[2]','[3]','[4]')";
        $result = ers_query($qStr, $data_name, $sanitizedXMLsDelimStr, date('Y-m-d H:i:s'), $handlingFunctionsStr);
        if(!$result){ $error = "MySQL error in ".__FILE__." -h5wiuh- mysql_error():".mysql_error(); error_log($error); return false; }
        return mysql_insert_id();
    }
    
    function getTransactionDelimiter(){
        return "::::::::PARTIAL_START::::::::\n";
    }
    
    function getMetadataDelimiter(){
        return "::::::::PARTIAL_METADATA:::::\n";
    }
    
    function getNumberOfQueueRowsThatNeedProcessing($data_name){
        $result = ers_query("SELECT COUNT(*) as `count` FROM `queue` WHERE `data_name`='[1]'", $data_name);
        if(!$result || !mysqli_num_rows($result)){error_log('MySQL error -fea- in '.__FILE__.' mysql_error():'.mysql_error()); return false;}
        $countArr = mysqli_fetch_assoc($result);
        return $countArr['count'];
    }
    
    function pullFromQueue($data_name){//Called when 'sendRequestXMLImpl' is reached meaning to pull the job to send to QBs.
        $query = "SELECT * FROM `queue` WHERE `data_name`='[1]' AND `server_date_time_pulled`='' order by `id` ASC LIMIT 1";
        $result = ers_query($query, $data_name);
        if(!$result){
            error_log('MySQL error -f36- in '.__FILE__.' mysql_error():'.mysql_error()); 
            bd_appendToLog("MYSQL ERROR:".mysql_error(), 'learning_soap.log');
            return false;}
        if(!mysqli_num_rows($result)){ bd_appendToLog("No Row To Pull For $data_name", 'learning_soap.log'); return false; }//No Jobs To Perform.
        return mysqli_fetch_assoc($result);
    }
    
    function getQueueRowWithTicket($ticket){
        $result = ers_query("SELECT * FROM `queue` WHERE `ticket_session_identifier`='[1]'", $ticket);
        if(!$result){ error_log('MySQL error -fk53- in '.__FILE__.' mysql_error():'.mysql_error()); return false; }
        if(!mysqli_num_rows($result)){ return false; }//Bad Ticket
        return mysqli_fetch_assoc($result);
    }
    
    function updateRowOnQueue($specified_columns = false){
        global $queueRow;
        $id = $queueRow['id'];
        $prevRowArr = getQueueRowForID($id);
        if(empty($id)){ error_log("Error: Row array does not have id in ".__FILE__); return false; }
        $queryStr = "UPDATE `queue` SET ";
        if(empty($specified_columns)){
            foreach($queueRow as $key => $val){
                if($key == 'id') continue;
                $queryStr .= "`$key`='[$key]',";
            }
        }else{
            $columnsUpdatedInt = 0;
            foreach($queueRow as $key => $val){
                if($key == 'id') continue;
                if(in_array($key, $specified_columns)){
                    $queryStr .= "`$key`='[$key]',";
                    $columnsUpdatedInt++;
                }
            }
            if($columnsUpdatedInt != count($specified_columns)){
                error_log('updateRowOnQueue has been called with arg:$specified_columns containing: '.
                    print_r($specified_columns,1).
                    ' and all columns from $specified_columns do not exist in the queue column list: '.
                     print_r(array_keys($queueRow),1) );
            }
        }
        $queryStr = substr($queryStr, 0, -1).' ';
        $queryStr .= "WHERE `id` = '[id]' ";
        $result = ers_query($queryStr, $queueRow);
        if(!$result){
            bd_appendToLog('Mysql error in '.__FILE__.' -3gr- mysql_error:'.mysql_error(), 'learning_soap.log');
            bd_appendToLog('Query string built for error -3gr-:'.$queryStr, 'learning_soap.log');
            error_log('Mysql error in '.__FILE__.' -3gr- mysql_error:'.mysql_error());
            return false;         
        }
        return true;
    }
    
    function setServerTimeOnQueueField($field){
        global $queueRow;

        $row_id = $queueRow['id'];
        $dateTime = date('Y-m-d H:i:s');
        $result = ers_query("UPDATE `queue` SET `[1]`='[2]' WHERE `id`='[3]'", $field, $dateTime, $row_id);
        if(!$result){ 
            $errorMsg = 'MySQL error -kc02- in '.__FILE__.' mysql_error():'.mysql_error();
            error_log($errorMsg);
            echo 'ERROR:' . $errorMsg;
            return false;
        }
        
        return mysql_affected_rows();
    }
    
    function getQueueRowForTicket($ticket){
        $result = ers_query("SELECT * FROM `queue` where `ticket_session_identifier`='[1]'", $ticket);
        if(!$result){
            error_log("MySQL Error in ".__FILE__." -asf3- error: " . mysql_error());
            return false;
        }
        if(mysqli_num_rows($result)){
            return mysqli_fetch_assoc($result);
        }
        return false;
    }

    function throwExceptionOntoQueue($exception){
        global $isLogging, $queueRow;
        $appendText = "EXCEPTION AT ACTION_INDEX:".$queueRow['action_index']."\n".$exception . "\n\n";
        $queueRow['exceptions'] .= $appendText;
        if($isLogging){
            bd_appendToLog("Appending Exception:".$appendText, 'learning_soap.log');
        }
        updateRowOnQueue(array('exceptions'));
    }
    
    function appendToPhpErrorLogColumn($error_log_messages){
        global $isLogging, $queueRow;
        if($isLogging){
            bd_appendToLog("Appending to phpErrorLogColumn".$error_log_messages, 'learning_soap.log');
        }
        $queueRow['php_error_log'] .= "\n" . $exception . "\n";
        updateRowOnQueue(array('php_error_log'));
    }
    
    function getQueueRowForID($rowID){
        $result = ers_query("SELECT * FROM `queue` WHERE `id`='[1]'", $rowID);
        if(!$result){ 
            bd_appendToLog('MySQL error -f7hagi- in '.__FILE__.' mysql_error():'.mysql_error(), 'learning_soap.log');
            error_log('MySQL error -f7hagi- in '.__FILE__.' mysql_error():'.mysql_error()); return false; 
        }
        if(!mysqli_num_rows($result)){ return false; }//Bad Ticket
        return mysqli_fetch_assoc($result);
    }
    
    function appendRowToBackreferenceSubstitutionHistory($backreferenceTokenMapForThisIndex){
        global $queueRow;
        if(empty($backreferenceTokenMapForThisIndex)){ return; }//limit output where necessary.
        $queueRow = getQueueRowForID($queueRow['id']);
        $textToAppend = "Back Substitution Token Map for action_index:".$queueRow['action_index'].":\n".print_r($backreferenceTokenMapForThisIndex,1)."\n\n";
        $fullText = $queueRow['back_reference_substitution_history'] . $textToAppend;
        $result = ers_query("UPDATE `queue` SET `back_reference_substitution_history`='[1]' WHERE `id`='[2]'", $fullText, $queueRow['id']);
        if(!$result){
            error_log("MySQL Error in ".__FILE__." -hjfut- error: " . mysql_error());
            echo "MySQL Error in ".__FILE__." -hjfut- error: " . mysql_error();
            return false;
        }
    }
    
    function appendInfoLineToBackreferenceSubstitutionHistory($infoLine){
        global $queueRow;
        //$queueRow = getQueueRowForID($queueRow['id']);
        $textToAppend = "NOTICE FOR ACTION_INDEX:".$queueRow['action_index']."\n{\n".$infoLine."\n}\n\n";
        $fullText = $queueRow['back_reference_substitution_history'] . $textToAppend;
        $result = ers_query("UPDATE `queue` SET `back_reference_substitution_history`='[1]' WHERE `id`='[2]'", $fullText, $queueRow['id']);
        if(!$result){
            error_log("MySQL Error in ".__FILE__." -5e5j5- error: " . mysql_error());
            echo "MySQL Error in ".__FILE__." -5e5j5- error: " . mysql_error();
            return false;
        }
    }
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //
    //  G E N E R A L     H E L P E R    S U B R O U T I N E S    F O R    Q U E U E    P R O C E S S I N G . . . 
    //
    function splitColumnDataIntoValuesAndMetadataArr($columnDataStr){
        $valuesOnlyArr = array();
        $metadataOnlyArr = array();
        $valMetaPairs = explode(getTransactionDelimiter(), $columnDataStr);
        $valMetaPairs = array_splice($valMetaPairs, 1);
        foreach($valMetaPairs as $currPairStr){
            $currPairArr = explode(getMetadataDelimiter(), $currPairStr);
            $currValue = $currPairArr[0]; 
            $currMetad = count($currPairArr) == 2 ? $currPairArr[1] : "";
            $valuesOnlyArr[] = $currValue;
            $metadataOnlyArr[] = $currMetad;
        }
        return array("values_arr" => $valuesOnlyArr, 'metadata_arr' => $metadataOnlyArr);
    }
    
    function combineValueMetadataSplitArraysIntoColumnText(&$splitArr){
        $metadataArr = $splitArr['metadata_arr'];
        $valuesArr = $splitArr['values_arr'];
        $indexCount = count($valuesArr);//SHOULD EQUAL metadataCount raise exception if doesnt.
        $stringBuilder = '';
        for($i = 0; $i < $indexCount; $i++){
            $stringBuilder .= getTransactionDelimiter();
            $stringBuilder .= $valuesArr[$i];
            $stringBuilder .= getMetadataDelimiter();
            $stringBuilder .= $metadataArr[$i];
        }
        return $stringBuilder;
    }
    
    function updateDatabaseAppendMetadataIntoColumnAtIndex($metaData, $columnName, $index){
        global $queueRow;
        $columnDataStr = $queueRow[$columnName];
        $columnDataValueMetaArrs = splitColumnDataIntoValuesAndMetadataArr($columnDataStr);
        $columnDataValueMetaArrs['metadata_arr'][$index] .= $metaData;
        $updatedColumnStr = combineValueMetadataSplitArraysIntoColumnText($columnDataValueMetaArrs);
        $queueRow[$columnName] = $updatedColumnStr;
        return updateRowOnQueue($columnName);
    }
    
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //
    //  F U N C T I O N S     F O R     B A C K _ R E F E R E N C E _ E X P R E S S I O N    S U B S T I T U T I O N S
    //
    
    //Looks at current qbXMLRequest by the current `action_index`, detects any back-reference-expressions and performs substitution by evaluation on qbXMLResponses.
    //This is called at sendXMLRequest time first thing so we may perform the lookup-substitutions relative to former qbXMLResponses on this same queue row.
    $global_back_reference_token_map; //Global Access Variable.
    $global_qbxml_responses_array;
    function getBackReferenceSubstitutionOfCurrentQBXMLRequestByActionIndex(){
        global $queueRow;
        global $global_back_reference_token_map;
        global $global_qbxml_responses_array;
        global $logDebuggingCallbackReferenceSubstitution;

        //inits
        $global_back_reference_token_map = array();
        $global_qbxml_responses_array = array();
        
        //Retrieve the dynmically returned qbXMLResponses
        $xmlResponsesDelimitedStr = $queueRow['receiveResponseXMLs'];
        $responsesValsMetasArr = splitColumnDataIntoValuesAndMetadataArr($xmlResponsesDelimitedStr);
        $global_qbxml_responses_array = $responsesValsMetasArr['values_arr'];
        
        //Retrieve the current qbXMLRequest
        $xmlRequestsDelimitedStr = $queueRow['sendRequestXMLs'];
        $requestsValsMetasArr = splitColumnDataIntoValuesAndMetadataArr($xmlRequestsDelimitedStr);
        $requestsXMLs = $requestsValsMetasArr['values_arr'];
        $currentXMLRequest = $requestsXMLs[$queueRow['action_index']];
        
        //Finally we do the processing
        //Notice grabBackReferenceKeys performs a global access mutation on $global_back_reference_token_map adding all the backreference expressions as key, '' vals.
        preg_replace_callback('/\{\[\(.*?\)\]\}/', 'grabBackReferenceKeys', $currentXMLRequest);
        $global_back_reference_token_map = loadBackReferenceValuesIntoTokenMap($global_qbxml_responses_array, $global_back_reference_token_map);
        $currentXMLRequest_xmlOnly = performStringReplaceOnQBXMLRequestStr($currentXMLRequest, $global_back_reference_token_map);
        appendRowToBackreferenceSubstitutionHistory($global_back_reference_token_map);

        return $currentXMLRequest_xmlOnly;
    }
    
    //1.)
    function grabBackReferenceKeys($callbackArr){
        global $global_qbxml_responses_array;
        global $global_back_reference_token_map;
        $global_back_reference_token_map[$callbackArr[0]] = '';
    }
    //2.)
    function loadBackReferenceValuesIntoTokenMap($global_qbxml_responses_array, $global_back_reference_token_map){
        global $queueRow;
        foreach($global_back_reference_token_map as $key => $val){
            $innerContentsOfCurrBackRefExp = substr($key, 3, -3);
            $innerContentPartsOfBackRefExp = explode("||",$innerContentsOfCurrBackRefExp);
            $backreferenceFound = false;
            foreach($innerContentPartsOfBackRefExp as $currBackRefOptionStr){
                $currOptionsDerivedValue = backreferenceExpressionOptionToValue($currBackRefOptionStr, $global_qbxml_responses_array);
                if($currOptionsDerivedValue !== false){
                    $global_back_reference_token_map[$key] = $currOptionsDerivedValue;
                    $backreferenceFound = true; break;
                }
            }
            if(!$backreferenceFound){ throwExceptionOntoQueue("All options have failed for back-reference expression:$key");}
        }
        return $global_back_reference_token_map;
    }
    //2.a)
    function backreferenceExpressionOptionToValue($back_reference_option_str, $global_qbxml_responses_array){
        global $queueRow;
        $currBackRefOptionSplit = explode(':', $back_reference_option_str);
        if(count($currBackRefOptionSplit) == 1){
            if(preg_match('/^((&apos;)|(&quot;)).*((&apos;)|(&quot;))$/', $back_reference_option_str)){
                $currBackOptionStr = substr($currBackOptionStr, 6, -6);
                //$global_back_reference_token_map[$key] = $currBackOptionStr;
                return $currBackOptionStr;
            }
            return false;
        }
        else{
            $responseArrIndex = trim($currBackRefOptionSplit[0]);
            $firstColonIndex  = strpos($back_reference_option_str,':');
            $drillDownPartStr = trim(substr($back_reference_option_str, $firstColonIndex+1));
            $drillDownPartStr = str_replace('&apos;',"'",$drillDownPartStr);
            $drillDownPartStr = str_replace('&quot;',"'",$drillDownPartStr);
            $drillDownPartStr = substr($drillDownPartStr, 2, -2);//Cut off [' and '] from ends.
            $drillDownPartsArr = explode("']['", $drillDownPartStr);
            $final_val = getDrillDownValueFromXML_orFalse($global_qbxml_responses_array[$responseArrIndex], $drillDownPartsArr, $responseArrIndex);
            return $final_val;
        }
    }
    //2.a.1)
    function getDrillDownValueFromXML_orFalse($xmlStr, $drillDownSequenceArr, $responseArrIndex){
        global $queueRow;
        $xmlObj = new SimpleXMLElementEnhanced($xmlStr);
        if(!$xmlObj->parsedSuccessfully()){
            $exceptionString = "SimpleXMLElementEnhanced could not parse the xml string:\n$xmlStr";
            throwExceptionOntoQueue($exceptionString);
            return false;
        }
        $nodeDerivedByDrillDownSequence = $xmlObj->getNodeAtPathOrFalse($drillDownSequenceArr);
        $drillDownDebug = $xmlObj->getLastPathDrilldownDebug();
        appendInfoLineToBackreferenceSubstitutionHistory("Drilling on response index:".$responseArrIndex."\n"."Index drill down info:".$drillDownDebug);
        $value = $nodeDerivedByDrillDownSequence ? $nodeDerivedByDrillDownSequence->getInnerText() : false;
        return $value;
    }
    //3.)
    function performStringReplaceOnQBXMLRequestStr($qbXMLRequestStr, $backReferenceTokenMap){
        foreach($backReferenceTokenMap as $key => $val){
            $qbXMLRequestStr = str_replace($key, $val, $qbXMLRequestStr);
        }
        return $qbXMLRequestStr;
    }
    
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //
    //  F A C T O R E D - S U B R O U T I N E S  (MOSTLY SPECIFIC TO DIFFERING PROCESS/FUNCTION/ALGORYTHM(s), 
    //                                                                       BUT EXTRACTED OUT FOR CLEANLINESS/INTELLIGIBILITY)
    //
    //
    // Subroutine functions for the QBWC interface method: receiveResponseXMLImpl() 
    //
    function append_qbxmlresponse_and_meta_to_string($ticket, $response, $hresult, $message, $textToAppendTo){
        global $verboseLogging, $logging;
        $textToAppendTo .= getTransactionDelimiter();
        $textToAppendTo .= $response;
        $textToAppendTo .= getMetadataDelimiter();
        $textToAppendTo .= "TICKET[".$ticket."]\n";
        $textToAppendTo .= "HRESULT[".$hresult."]\n";
        $textToAppendTo .= "MESSAGE[".$message."]\n";

        return $textToAppendTo;
    }
    function getCorrespondingResponseHandlerFromRow(){
        global $verboseLogging, $logging, $queueRow;
        $handler_functions_str = $queueRow['receivedResponseHandlers'];
        $handler_functions_arr = explode(getTransactionDelimiter(),$handler_functions_str);
        $handler_functions_arr = array_slice($handler_functions_arr, 1);
        $handler_function_pre  = $handler_functions_arr[$queueRow['action_index']];
        $handler_function = $handler_function_pre.'_TransactionHandler';
        if($logging){
            bd_appendToLog('Handler function found as:'.$handler_function,'learning_soap.log');
        }
        return $handler_function;
    }
    function checkIfHandlingFunctionExistsPutExceptionOnQueueIfDoesnt($responseHandlingFunction){
        global $queueRow;
        if(!function_exists($responseHandlingFunction)){
            throwExceptionOntoQueue('Could not find the response handling function when `action_index` was:'.$queueRow['action_index']);
            return false;
        }
        return true;
    }
    function calculatePercentFinishedWithDataname($accountRowFromTicket){
        //TODO FINISH THIS PART
        return 22;
    }
    //
    // Subroutine functions for the QBWC interface method: receiveResponseXMLImpl()
    //
    function initializeRowIfActionIndexEqZero($accountRow){
        global $queueRow;
        //If this is the first QBXML for this queue row.
        if($queueRow['action_index'] == 0){
            $queueRow['server_date_time_pulled'] = date('Y-m-d H:i:s');//We set the new fields and update
            $queueRow['ticket_session_identifier'] = $accountRow['ticket_session_identifier'];
            updateRowOnQueue();//We pulled this array added fields now updating it.
        }
    }
    function getSendRequestXMLFromQueueRowAtActionIndex(){
        global $queueRow;
        //We pull the xml element from the xml array stored in receiveResponseXMLs.
        $XMLArrayDelimitedStr = $queueRow['sendRequestXMLs'];
        $XMLArrayMetaWithDataArr = array_slice(explode(getTransactionDelimiter(),$XMLArrayDelimitedStr),1);
        $XMLMetaWithDataStr = $XMLArrayMetaWithDataArr['action_index'];
        $XMLMetaWithDataArr = array_slice(explode(getMetadataDelimiter(),$XMLArrayMetaWithDataStr),1);
        $sendXMLDataStr = $XMLMetaWithDataArr[0];
        return $sendXMLDataStr;
    }
    //////////////////////////////////////////////////////////////////////////////////
    // O R D E R    I D    B A S E 6 4    E N C O D I N G   T H I N G S.
    // Order id base 64 encoding functions.
    // 0-9, then a-z, then A-Z, then +, then /.  e.g. 0=0, a=10,A=a+26
    function lavu_int_2_base64str($number){
        if(!is_numeric($number)) return false;
        $intVal = intval($number);
        $builderStr = "";
        while($intVal > 63){
            $remainder = $intVal % 64;
            $builderStr = lavu_base64_int_2_char($remainder) . $builderStr;
            $intVal = intval($intVal/64);
        }
        $builderStr = lavu_base64_int_2_char($intVal) . $builderStr;
        return $builderStr;
    }
    function lavu_base64str_2_int($base64Str){
        $base64Str = ''.$base64Str;
        $intBuilder = 0;
        for($i = 0; $i < strlen($base64Str); $i++){
            $currChar = substr($base64Str, $i, 1);
            $intBuilder *= 64;
            $intBuilder += lavu_base64_char_2_int($currChar);
        }
        return $intBuilder;
    }
    function lavu_base64_int_2_char($lt64int){
        $lt64int = intval($lt64int);
        if($lt64int < 10)  return '' . $lt64int;
        if($lt64int < 36)  return '' . chr( $lt64int - 10 + ord('a') );
        if($lt64int < 62)  return '' . chr( $lt64int - 36 + ord('A') );
        if($lt64int == 62) return '+';
        if($lt64int == 63) return '/';
    }
    function lavu_base64_char_2_int($char){
        $asciiValue = ord($char);
        if($char == '+') return 62;
        if($char == '/') return 63;
        if($asciiValue >= ord('0') && $asciiValue <= ord('9')) return $asciiValue - ord('0');
        if($asciiValue >= ord('a') && $asciiValue <= ord('z')) return $asciiValue - ord('a') + 10;
        if($asciiValue >= ord('A') && $asciiValue <= ord('Z')) return $asciiValue - ord('A') + 36;
        
    }
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //
    //  Q B A P I - F U N C T I O N S
    //  
    //  F U N C T I O N S   A S   P E R T A I N S   O T H E R   S E R V E R S
    //
    //  I N T E G R A T I N G   W I T H   T H I S   Q B   S E R V E R.
    //
    //  Called as a precaution for all qbapi.php calls.
    function validateRequestSecurityAndFunction(){
        //Validate existence of needed parameters.
        if(  empty($_REQUEST)                  ) return '{"status":"failed","reason":"missing all arguments"}';
        if(  empty($_REQUEST['server_name'])   ) return '{"status":"failed","reason":"missing server_name argument"}';
        if( !isset($_REQUEST['data_name'])     ) return '{"status":"failed","reason":"missing data_name argument"}';//Left blank if not needed, but should still be set.
        if(  empty($_REQUEST['security_key'])  ) return '{"status":"failed","reason":"missing security_key argument"}';
        if(  empty($_REQUEST['security_hash']) ) return '{"status":"failed","reason":"missing security_hash argument"}';
        if(  empty($_REQUEST['function'])      ) return '{"status":"failed","reason":"missing function argument"}';
        if( !isset($_REQUEST['response_url'])  ) return '{"status":"failed","reason":"missing response_url argument"}';//Left blank for no response, but should still be set.
        if( !empty($_REQUEST['function'])  && empty($_REQUEST['func_args'])) return '{"status":"failed","reason":"if function is specified, must provide func_args JSON string."}';
        if( !empty($_REQUEST['func_args']) && LavuJson::json_decode($_REQUEST['func_args'], 1) === null) return '{"status":"failed","reason":"func_args json deserialization has failed -3fw-."}';
        $serverRow = getServerRow($_REQUEST['server_name'], $_REQUEST['security_key']);
        if( !$serverRow ) return '{"status":"failed","reason":"invalid server_name security_key pair"}';
        //All Servers and Clients do not have enough info to configure a bullshit sec_key.
        $dynHashEmbryo=$_REQUEST['server_name'].$_REQUEST['function'].$_REQUEST['data_name'].$serverRow['server_sec_token'];
        $expSecHash = sha1($dynHashEmbryo);

        if($_REQUEST['security_hash'] != $expSecHash) return '{"status":"failed","reason":"incorrect security hash"}';
        
        $api_function = $_REQUEST['function'];
        $api_function = 'qbapi_'.$api_function;
        if(!function_exists($api_function)){
            return "qbapi function:[$api_function] does not exist";
        }
        
        return true;
    }
    //Server from the `subscribing_servers` table.
    function getServerRow($serverName, $serverSecKey){
        global $logging;
        $queryStr = "SELECT * FROM `subscribing_servers` WHERE `server_name`='[1]' AND `server_sec_key`='[2]'";
        $result = ers_query($queryStr, $serverName, $serverSecKey);
        if(!$result){
            error_log("Mysql error in ".__FILE__." -3hrg749- mysql error:".mysql_error());
            bd_appendToLog("Mysql error in ".__FILE__." -3hrg749- mysql error:".mysql_error(), 'learning_soap.log');
            return false;
        }
        if(mysqli_num_rows($result)==0){
            if($logging)
                bd_appendToLog("Mysql error in ".__FILE__." -3hrg749- mysql error:".mysql_error(), 'learning_soap.log');
            return false;
        }
        else{
            return mysqli_fetch_assoc($result);
        }
    }
    function getFunctionArgumentsFromRequest(){
        $returnArr = LavuJson::json_decode($_REQUEST['func_args'],1);
        if($returnArr == null){
            echo '{"status":"failed","reason":"func_args json deserialization has failed -8g4-."}';
            exit;
        }
        return $returnArr;
    }
    function returnFrom_qbapi_queueRowInsertRequest($insertID){
        global $script_start_time, $script_start_micro;
        $script_end_time = date('Y-m-d H:i:s');
        $script_end_micro = microtime(true);
        $totalMicroTime = $script_end_micro - $script_start_micro;
        $startTimeEndTimeJSONStr = '"api_start_time":"'.$script_start_time.'","api_end_time":"'.$script_end_time.'",';
        if(isset($_REQUEST['debug_show_full_row']) && $_REQUEST['debug_show_full_row']){
            $queueRow = getQueueRowForID($insertID);
            $queueRowJSON = LavuJson::json_encode($queueRow);
            $queueRowJSON = str_replace('"', '\\"', $queueRowJSON);
            echo '{"status":"success",'.$startTimeEndTimeJSONStr.'"total_exec_time_micro":"'.$totalMicroTime.'","queue_row":"'.$queueRowJSON.'"}';
        }else{
            echo '{"status":"success",'.$startTimeEndTimeJSONStr.'"total_exec_time_micro":"'.$totalMicroTime.'","queue_row":"'.$insertID.'"}';
        }
        exit;
    }
    function checkInsertRow_IfEmpty_returnErrorToSubscribingServerAndExit($insertID,$failInfo){
        global $script_start_time, $script_start_micro;
        $script_end_time = date('Y-m-d H:i:s');
        $script_end_micro = microtime(true);
        if(empty($insertID)){
            $returnJSON = '{"status":"failed",'.
                           '"fail_reason":"returned insert id was empty",'.
                           '"more_info":"'.$failInfo.'",'.
                           '"api_start_time":"'.$script_start_time.'",'.
                           '"api_end_time":"'.$script_end_time.'",'.
                           '"total_exec_time_micro":"'.$totalMicroTime.'"'.
                           '}';
            
            echo $returnJSON;
            exit;
        }
        return;
    }
?>