<?php


        /*
    function qbapi_ensureItems_createInvoice_addPayments($funcArgs){
        global $serverRow;
        
        $dataName = $serverRow['server_name'].'_'.$_REQUEST['data_name'];
        //TODO VALIDATIONS of funcArgs
        // IMPORTANT check for po_number!!! And everything else.
        
        
        
        //We make sure that this order is currently being processed, if it is we send back an wait status.
        checkIfOrderInvoiceForCurrentOrderIDIsInProgressSendWaitMessageAndExitIfIs($dataName, $funcArgs['po_number']);
        //Row(s should really be only 1, but in case) that have not been pulled by QBWC and are same order_id.  
        //This insert will render it invalidated.
        $rowsToInvalidate = getUnprocessedQueueRowsWithSameDatanameAndOrderID($dataName, $funcArgs['po_number']);
        $idsOfRowsToInvalidate = getIDsFromRows($rowsToInvalidate);
        invalidateUnprocessedQueueRowsWithSameDatanameAndOrderID($dataName, $funcArgs['po_number']);  
        set_overridden_by_toZeroWherePointedToRecentlyInvalidatedRow($dataName, $funcArgs['po_number'], $idsOfRowsToInvalidate);
    
        
        //// //We mark any order invoice rows that have not yet been processed as invalid since they both are no longer relevant
        //// //and also would cause an error (they would move/create payments that this row would not know about etc.).
        //// invalidateNotYetProcessedInvoiceQueueRowsWithSameOrderIDBacktrackOverriddenByOnPreviousRows($serverRow['server_name'].'_'.$_REQUEST['data_name'], $funcArgs['po_number']);
        

        //If we sync the same order id, we send orders to delete the old one, then we proceed sending the new one.
        $queueRowsWithSameOrderIDHasNotBeenOverriddenDesc = 
            getOtherQueueRowsWithSameOrderIDOrderedByIDDesc($serverRow['server_name'].'_'.$_REQUEST['data_name'], 
                                                            $funcArgs['po_number']);
        
        //We create a delete xml for every queue row with the same order_id and no `overridden_by` set.
        $deleteInvoiceXMLs = array();
        foreach($queueRowsWithSameOrderIDHasNotBeenOverriddenDesc as $currRowToMarkOverridden){
            $txnID2Delete = $currRowToMarkOverridden['txn_id'];
            $deleteInvoiceXML = get_deleteInvoiceXML($txnID2Delete, false);
            $deleteInvoiceXMLs[] = $deleteInvoiceXML;
        }
        
        //All server side payment ids that are being passed.
        $allAPIPassedServerSidePaymentIDs = getAllServerSidePaymentIDsPassedInFuncArgs($funcArgs);
    
        //Finding payments that will need to be modified.
        $allPreviouslySavedServerSidePaymentIDs = 
            getAllServerSidePaymentIDsFromRowsOverridden($queueRowsWithSameOrderIDHasNotBeenOverriddenDesc);
        
        //2 overlapping sets of: trans_ids sent from server (set A), and 'trans_ids' saved (set B)
        // A/B -> ReceivePaymentAdd(s)
        // A^B -> ReceivePaymentMod(s)
        // B/(A^B) -> Payments saved, but not included by api call... these are in error.
        
        
        //IDs of payments that will be added. A/B
        $serverSidePaymentIDs_4_ReceivePaymentAdd = array_diff($allAPIPassedServerSidePaymentIDs, $allPreviouslySavedServerSidePaymentIDs);
        $receivePaymentsAddFilterArr = arrayOfFilterValuesToKeyedArrayEqualToSameValues_falseIfNotArray($serverSidePaymentIDs_4_ReceivePaymentAdd);
        //IDs of payments that will be modified. A^B
        $serverSidePaymentIDs_4_ReceivePaymentMod = array_diff($allAPIPassedServerSidePaymentIDs, $serverSidePaymentIDs_4_ReceivePaymentAdd);
        $receivePaymentsModFilterArr = arrayOfFilterValuesToKeyedArrayEqualToSameValues_falseIfNotArray($serverSidePaymentIDs_4_ReceivePaymentMod);
        //IDs of payments that were saved but not in current api call. B/(A^B)
        $serverSidePaymentIDs_savedButNotInAPICall_error = array_diff($allPreviouslySavedServerSidePaymentIDs, $allAPIPassedServerSidePaymentIDs);


        
        //--------------THE QBXMLS
        $sendRequestXMLsArr = array();
        // 1.) Delete any with same order_id that has not been overridden.
        $sendRequestXMLsArr = array_merge($sendRequestXMLsArr, $deleteInvoiceXMLs); 
        $sendRequestXMLsArr[] = get_CustomerAdd_XML($funcArgs, false);
        // --- 2.) The customer, add.  If already exists, then no harm done.
        $inventoryAddXMLs = get_InventoryItemAdd_xmls($funcArgs, false);
        // --- 3.) The inventory items add.  If already exist, then no harm done.
        $sendRequestXMLsArr = array_merge($sendRequestXMLsArr, $inventoryAddXMLs);
        $invoiceAddActionIndex = count($sendRequestXMLsArr); 
        // --- 4.) The Invoice Add.  This one should not fail.
        $sendRequestXMLsArr[] = get_InvoiceAdd_xml($funcArgs, false);
        // --- 5.) ReceivePaymentMod(s).
        $serverPaymentIDsMods_OUT = array();
        $serverPaymentID_2_map_variables = get_server_paymentID_qb_vars($queueRowsWithSameOrderIDHasNotBeenOverriddenDesc);
        $receivePaymentsModXMLsArr = get_mult_ReceivePaymentMod_xmls($funcArgs, $serverPaymentID_2_map_variables, 
                                                $receivePaymentsModFilterArr, $invoiceAddActionIndex, false, $serverPaymentIDsMods_OUT);
        $sendRequestXMLsArr = array_merge($sendRequestXMLsArr, $receivePaymentsModXMLsArr);
        // --- 6.) ReceivePaymentAdd(s) on invoice.
        $serverPaymentIDsAdded_OUT = array(); //Out variable will contain (in the case of lavu) the cc_transaction ids. (the id of the payments server side).
        $receivePaymentsAddXMLsArr = get_mult_ReceivePaymentAdd_xmls($funcArgs, $receivePaymentsAddFilterArr, 
                                                $invoiceAddActionIndex, false, $serverPaymentIDsAdded_OUT);
        $sendRequestXMLsArr = array_merge($sendRequestXMLsArr, $receivePaymentsAddXMLsArr);
        
        
        
        //--------------THE RESPONSE METHODS
        $responseHandlerFunctions = array();
        // 1.) For the delete qbxml.
        foreach($deleteInvoiceXMLs as $v){ $responseHandlerFunctions[] = 'doNothing'; }
        // 2.) The customer add.
        $responseHandlerFunctions[] = 'doNothing';
        // 3.) Inventory items add.
        foreach($inventoryAddXMLs as $v){ $responseHandlerFunctions[] = 'doNothing'; }
        // 4.) InvoiceAdd
        $responseHandlerFunctions[] = (count($receivePaymentsModXMLsArr) == 0 && count($receivePaymentsAddXMLsArr) == 0) ? 'handleInvoiceAddAndClose' : 'handleInvoiceAdd';
        // 5.) ReceivePaymentMods
        for($i = 0; $i < count($receivePaymentsModXMLsArr) - 1; $i++){ $responseHandlerFunctions[] = 'handleReceivePaymentMod';}
        if(count($receivePaymentsModXMLsArr) > 0){
            $responseHandlerFunctions[] = count($receivePaymentsAddXMLsArr) == 0 ? "handleReceivePaymentModAndClose" : "handleReceivePaymentMod";
        }
        // 6.) ReceivePaymentAdd
        for($i = 0; $i < (count($receivePaymentsAddXMLsArr) - 1); $i++){ $responseHandlerFunctions[] = 'handleReceivePaymentAdd'; }
        if(count($receivePaymentsAddXMLsArr) > 0){ 
            $responseHandlerFunctions[] = 'handleReceivePaymentAddAndClose'; 
        }
        
        //Create Queue Row.
        $serverSidePaymentIDsJsonMap = createServerQuickbooksIDJoinedMap(array_merge($serverPaymentIDsMods_OUT, $serverPaymentIDsAdded_OUT), $invoiceAddActionIndex+1);
        $insertID = pushRequestOntoQueue($sendRequestXMLsArr,
                                         $responseHandlerFunctions,
                                         $serverRow['server_name'].'_'.$_REQUEST['data_name'], 
                                         $errorOut, 
                                         $funcArgs['po_number'],
                                         $serverSidePaymentIDsJsonMap);
                                         
        //TODO MAKE SURE THIS PART WORKS.
        //All previous queue rows with same dataname and orderID have been deleted through the qbxml, now we mark they have been overridden by this invoice.
        markAllOverriddenRowsWithOverridersID($queueRowsWithSameOrderIDHasNotBeenOverriddenDesc, $insertID);
        
        checkInsertRow_IfEmpty_returnErrorToSubscribingServerAndExit($insertID, $errorOut);
        returnFrom_qbapi_queueRowInsertRequest($insertID);
    }
    */


    require_once(dirname(__FILE__).'/qbapi_qbxml_factory.php');
    function unfold_ensureItems_createInvoice_addPayments(&$queueRow, &$deserializedInitAPICall){

        //Needed data from original api call to update the rest of the queue row.
        $data_name = $deserializedInitAPICall['data_name'];//This is the full one with the server name prepended.
        $originalPost = $deserializedInitAPICall['_POST'];
        $funcArgs = $deserializedInitAPICall['funcArgs'];


        
        
        //Now we create load the queue with the qbxmls, response handlers, order_id...
                //If we sync the same order id, we send orders to delete the old one, then we proceed sending the new one.
        $queueRowsWithSameOrderIDHasNotBeenOverriddenDesc = 
            getOtherQueueRowsWithSameOrderIDOrderedByIDDesc($data_name, $funcArgs['po_number']);
        
        //We create a delete xml for every queue row with the same order_id and no `overridden_by` set.
        $deleteInvoiceXMLs = array();
        foreach($queueRowsWithSameOrderIDHasNotBeenOverriddenDesc as $currRowToMarkOverridden){
            $txnID2Delete = $currRowToMarkOverridden['txn_id'];
            $deleteInvoiceXML = get_deleteInvoiceXML($txnID2Delete, false);
            $deleteInvoiceXMLs[] = $deleteInvoiceXML;
        }
        
        //All server side payment ids (cc_transaction ids) that were passed to the api.
        $allAPIPassedServerSidePaymentIDs = getAllServerSidePaymentIDsPassedInFuncArgs($funcArgs);
    
        //Finding payments that will need to be modified.
        $allPreviouslySavedServerSidePaymentIDs = 
            getAllServerSidePaymentIDsFromRowsOverridden($queueRowsWithSameOrderIDHasNotBeenOverriddenDesc);
        
        //2 overlapping sets of: trans_ids sent from server (set A), and 'trans_ids' saved (set B)
        // A/B -> ReceivePaymentAdd(s)
        // A^B -> ReceivePaymentMod(s)
        // B/(A^B) -> Payments saved, but not included by api call... these are in error.
        
        //IDs of payments that will be added. A/B
        $serverSidePaymentIDs_4_ReceivePaymentAdd = array_diff($allAPIPassedServerSidePaymentIDs, $allPreviouslySavedServerSidePaymentIDs);
        $receivePaymentsAddFilterArr = arrayOfFilterValuesToKeyedArrayEqualToSameValues_falseIfNotArray($serverSidePaymentIDs_4_ReceivePaymentAdd);
        //IDs of payments that will be modified. A^B
        $serverSidePaymentIDs_4_ReceivePaymentMod = array_diff($allAPIPassedServerSidePaymentIDs, $serverSidePaymentIDs_4_ReceivePaymentAdd);
        $receivePaymentsModFilterArr = arrayOfFilterValuesToKeyedArrayEqualToSameValues_falseIfNotArray($serverSidePaymentIDs_4_ReceivePaymentMod);
        //IDs of payments that were saved but not in current api call. B/(A^B)
        $serverSidePaymentIDs_savedButNotInAPICall_error = array_diff($allPreviouslySavedServerSidePaymentIDs, $allAPIPassedServerSidePaymentIDs);


        
        //--------------THE QBXMLS
        $sendRequestXMLsArr = array();
        // 1.) Delete any with same order_id that has not been overridden.
        $sendRequestXMLsArr = array_merge($sendRequestXMLsArr, $deleteInvoiceXMLs); 
        $sendRequestXMLsArr[] = get_CustomerAdd_XML($funcArgs, false);
        // --- 2.) The customer, add.  If already exists, then no harm done.
        $inventoryAddXMLs = get_InventoryItemAdd_xmls($funcArgs, false);
        // --- 3.) The inventory items add.  If already exist, then no harm done.
        $sendRequestXMLsArr = array_merge($sendRequestXMLsArr, $inventoryAddXMLs);
        $invoiceAddActionIndex = count($sendRequestXMLsArr); 
        // --- 4.) The Invoice Add.  This one should not fail.
        $sendRequestXMLsArr[] = get_InvoiceAdd_xml($funcArgs, false);
        // --- 5.) ReceivePaymentMod(s).
        $serverPaymentIDsMods_OUT = array();
        $serverPaymentID_2_map_variables = get_server_paymentID_qb_vars($queueRowsWithSameOrderIDHasNotBeenOverriddenDesc);
        $receivePaymentsModXMLsArr = get_mult_ReceivePaymentMod_xmls($funcArgs, $serverPaymentID_2_map_variables, 
                                                $receivePaymentsModFilterArr, $invoiceAddActionIndex, false, $serverPaymentIDsMods_OUT);
        $sendRequestXMLsArr = array_merge($sendRequestXMLsArr, $receivePaymentsModXMLsArr);
        // --- 6.) ReceivePaymentAdd(s) on invoice.
        $serverPaymentIDsAdded_OUT = array(); //Out variable will contain (in the case of lavu) the cc_transaction ids. (the id of the payments server side).
        $receivePaymentsAddXMLsArr = get_mult_ReceivePaymentAdd_xmls($funcArgs, $receivePaymentsAddFilterArr, 
                                                $invoiceAddActionIndex, false, $serverPaymentIDsAdded_OUT);
        $sendRequestXMLsArr = array_merge($sendRequestXMLsArr, $receivePaymentsAddXMLsArr);
        
        
        
        //--------------THE RESPONSE METHODS
        $responseHandlerFunctions = array();
        // 1.) For the delete qbxml.
        foreach($deleteInvoiceXMLs as $v){ $responseHandlerFunctions[] = 'doNothing'; }
        // 2.) The customer add.
        $responseHandlerFunctions[] = 'doNothing';
        // 3.) Inventory items add.
        foreach($inventoryAddXMLs as $v){ $responseHandlerFunctions[] = 'doNothing'; }
        // 4.) InvoiceAdd
        $responseHandlerFunctions[] = (count($receivePaymentsModXMLsArr) == 0 && count($receivePaymentsAddXMLsArr) == 0) ? 'handleInvoiceAddAndClose' : 'handleInvoiceAdd';
        // 5.) ReceivePaymentMods
        for($i = 0; $i < count($receivePaymentsModXMLsArr) - 1; $i++){ $responseHandlerFunctions[] = 'handleReceivePaymentMod';}
        if(count($receivePaymentsModXMLsArr) > 0){
            $responseHandlerFunctions[] = count($receivePaymentsAddXMLsArr) == 0 ? "handleReceivePaymentModAndClose" : "handleReceivePaymentMod";
        }
        // 6.) ReceivePaymentAdd
        for($i = 0; $i < (count($receivePaymentsAddXMLsArr) - 1); $i++){ $responseHandlerFunctions[] = 'handleReceivePaymentAdd'; }
        if(count($receivePaymentsAddXMLsArr) > 0){
            $responseHandlerFunctions[] = 'handleReceivePaymentAddAndClose'; 
        }
        
        //This is what gets stored in `server2QBsPaymentIDMap`
        $serverSidePaymentIDsJsonMap = createServerQuickbooksIDJoinedMap(array_merge($serverPaymentIDsMods_OUT, $serverPaymentIDsAdded_OUT), $invoiceAddActionIndex+1);
        
        /*
        // Row already exists, so we actually want to just update instead.
        $insertID = pushRequestOntoQueue($sendRequestXMLsArr,
                                         $responseHandlerFunctions,
                                         $serverRow['server_name'].'_'.$_REQUEST['data_name'], 
                                         $errorOut, 
                                         $funcArgs['po_number'],
                                         $serverSidePaymentIDsJsonMap);
        */

        //Finally we set the fields on the row and save the row.
        $result = mutativelyUpdateRowWithFinalUnfoldingSave2DB($queueRow, $sendRequestXMLsArr, $responseHandlerFunctions, $funcArgs['po_number'], $serverSidePaymentIDsJsonMap);
        
        //All previous queue rows with same dataname and orderID have been deleted through the qbxml, now we mark they have been overridden by this invoice.
        markAllOverriddenRowsWithOverridersID($queueRowsWithSameOrderIDHasNotBeenOverriddenDesc, $insertID);
        
        //checkInsertRow_IfEmpty_returnErrorToSubscribingServerAndExit($insertID, $errorOut);
        //returnFrom_qbapi_queueRowInsertRequest($insertID);
    }
?>