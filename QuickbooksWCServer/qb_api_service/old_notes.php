<?php
/*
        header('Content-type: text/xml');
        $consolibyteValidResponse = '<?xml version="1.0" encoding="UTF-8"?>'.
                                    '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns1="http://developer.intuit.com/">'.
                                        '<SOAP-ENV:Body>'.
                                                '<ns1:authenticateResponse>'.
                                                    '<ns1:authenticateResult>'.
                                                        '<ns1:string>15c9ce293bd3f41b761c21635b14fa06</ns1:string>'.
                                                        '<ns1:string>brian_test_company</ns1:string>'.
                                                    '</ns1:authenticateResult>'.
                                            '</ns1:authenticateResponse>'.
                                        '</SOAP-ENV:Body>'.
                                    '</SOAP-ENV:Envelope>';
        echo $consolibyteValidResponse;
        exit;
*/
/*
        header('Content-type: text/xml');
        $how_NUSOAP_RETURNS = '<?xml version="1.0" encoding="utf-8"?>
            <SOAP-ENV:Envelope SOAP-ENV:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/">'.
                '<SOAP-ENV:Body>'.
                    '<ns1:authenticateResponse xmlns:ns1="http://developer.intuit.com/">'.
                        '<return xsi:type="SOAP-ENC:Array" SOAP-ENC:arrayType="xsd:string[2]">'.
                            '<item xsi:type="xsd:string">brian_test_company</item>'.
                            '<item xsi:type="xsd:string">1</item>'.
                        '</return>'.
                    '</ns1:authenticateResponse>'.
                '</SOAP-ENV:Body>'.
            '</SOAP-ENV:Envelope>';
        echo $how_NUSOAP_RETURNS;
        exit;
*/
        
/* //The following works.
        header('Content-type: text/xml');
        $how_NUSOAP_RETURNS_tinkering = '<?xml version="1.0" encoding="utf-8"?>
            <SOAP-ENV:Envelope SOAP-ENV:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/">'.
                '<SOAP-ENV:Body>'.
                    '<ns1:authenticateResponse xmlns:ns1="http://developer.intuit.com/">'.
                        '<ns1:authenticateResult>'.
                            '<ns1:string>15c9ce293bd3f41b761c21635b14fa06</ns1:string>'.
                            '<ns1:string>C:\Qkbks back up\brian_test_company.qbw</ns1:string>'.
                            //'<ns1:string>brian_test_company.qbw</ns1:string>'.
                        '</ns1:authenticateResult>'.
                        //'<return xsi:type="SOAP-ENC:Array" SOAP-ENC:arrayType="xsd:string[2]">'.
                        //    '<item xsi:type="xsd:string">brian_test_company</item>'.
                        //    '<item xsi:type="xsd:string">1</item>'.
                        //'</return>'.
                    '</ns1:authenticateResponse>'.
                '</SOAP-ENV:Body>'.
            '</SOAP-ENV:Envelope>';
        echo $how_NUSOAP_RETURNS_tinkering;
        exit;
*/

    /*//Example code 
        $xmlObject = simplexml_load_string($strHCPResponse);
        $xmlParsed = $xmlObject ? 'built':'errored';
        bd_appendToLog("sendRequestXML called... XML object:[" .$xmlParsed."]", 'learning_soap.log');

    	
    	$addCustomerExample = new AddCustomer(43, "2.0");
    	$addCustomerArr = $addCustomerExample->getInnerArray();
    	$addCustomerArr['CustomerAdd']['Name'] = "Brian Dennedy_42";//Seems to be the main identifier.
    	$addCustomerArr['CustomerAdd']['CompanyName'] = "Lavu";
    	$addCustomerArr['CustomerAdd']['FirstName'] = "Brian";
    	$addCustomerArr['CustomerAdd']['LastName']  = "Dennedy";
    	$addCustomerArr['CustomerAdd']['BillAddress']['Addr1'] = "1234 Street st.";
    	$addCustomerArr['CustomerAdd']['BillAddress']['City']  = "Albuquerque";
    	$addCustomerArr['CustomerAdd']['BillAddress']['State'] = "NM";
    	$addCustomerArr['CustomerAdd']['BillAddress']['PostalCode'] = "87109";
    	$addCustomerArr['CustomerAdd']['BillAddress']['Country'] = "United States";
    	$addCustomerArr['CustomerAdd']['Phone'] = "505-525-5555";
    	$addCustomerArr['CustomerAdd']['Email'] = "Brian@poslavu.com";
    	$addCustomerExample->setInnerArray($addCustomerArr);
    	$addCustomerXML = $addCustomerExample->asSanitizedXML();
    */
    
    /*
            //DEBUGGING
        if($ticket == 'string' && $response == 'string' && $hresult == 'string' && $message=='string'){
            return 42;
        }
        //
    */
    
    
    
    
    /*
    
        function stringReplaceReferenceCallbacks($callbacksArr){
        global $responseArr;
        global $back_reference_token_map;
        echo "<pre>";
        echo "The regular expression array:" . print_r($callbacksArr,1);
        
        foreach($callbacksArr as $value){
            echo "Found callback expression:[$value]\n";
            $innerContents = trim(substr($value, 3, -3));
            echo "Inner contents of callback expression:[$innerContents]\n";
            $innerContentsParts = explode("||", $innerContents);
            echo "Callback Expression's chained ORs:\n";
            foreach($innerContentsParts as $currOption){
                $currOptionParts = explode(":", $currOption);
                if(count($currOptionParts) == 1){
                    echo "----Constant:".trim($currOptionParts[0]);
                }
                else if(count($currOptionParts) == 2){//Looking through the replacement options.
                    $index = trim($currOptionParts[0]);
                    $arrayPath = trim($currOptionParts[1]);
                    $arrayPath = str_replace('&apos;', "'", $arrayPath);
                    echo "----XMLIndex:".trim($currOptionParts[0])."   ArrayPath:".$arrayPath."\n";
                    echo "--------searching for value....\n";
                    $xmlToObject = $responseArr[$index];
                    $xmlToObject = str_replace("\n","",$xmlToObject);
                    $xmlObj = new SimpleXMLElementEnhanced($xmlToObject);
                    echo "Examining Node:".$xmlObj->getName();
                    echo "Printing out node list:" . print_r($xmlObj->getChildrenNodeList(),1) . "\n";
                    echo "Now going split the path into an array drill down:\n";
                    $arrayPathStr = substr($arrayPath,2,-2);
                    echo "Splitting on: " . $arrayPathStr . "\n";
                    //$arrayPathArr = explode("&apos;][&apos;",$arrayPathStr);
                    $arrayPathArr = explode("']['",$arrayPathStr);
                    print_r($arrayPathArr);
                    echo "\n";
                    
                    $currVal = $xmlObj;

                    foreach($arrayPathArr as $currDrillDown){
                        echo "Current drill down:$currDrillDown\n";
                        $currVal = $currVal[$currDrillDown];
                        if($currVal){
                           echo "--Got Node:" . $currVal->getName() . "\n";
                           echo "--Children Nodes:" . print_r($currVal->getChildrenNodeList(),1) . "\n";  
                        } 
                    }
                    echo "--------found value as:".$currVal->getInnerText()."\n";
                    echo "\n\n\n\n\n\n";
                }
            }
        }
        
        echo "</pre>";
        return;
    }

*/
?>