<?php
        ini_set('display_errors',1);
        
        //enabling db
        require_once('/home/eventrentalsystems/public_html/cp/functions.php');
        $Conn = ers_connect_db();
        
        //The file we are testing
        require_once(dirname(__FILE__).'/../quickbook_functions.php');
        
        $addCustomerExample = new AddCustomer(43, "2.0");
    	//$addCustomerArr = $addCustomerExample->getInnerArray();
    	$addCustomerArr = array();
    	$addCustomerArr['CustomerAdd']['Name'] = "John_Doe_".$_GET['a']."_0";//Seems to be the main identifier.
    	$addCustomerArr['CustomerAdd']['CompanyName'] = "Lavu";
    	$addCustomerArr['CustomerAdd']['FirstName'] = "John";
    	$addCustomerArr['CustomerAdd']['LastName']  = "Doe";
    	$addCustomerArr['CustomerAdd']['BillAddress']['Addr1'] = "1234 Street st.";
    	$addCustomerArr['CustomerAdd']['BillAddress']['City']  = "Albuquerque";
    	$addCustomerArr['CustomerAdd']['BillAddress']['State'] = "NM";
    	$addCustomerArr['CustomerAdd']['BillAddress']['PostalCode'] = "87109";
    	$addCustomerArr['CustomerAdd']['BillAddress']['Country'] = "United States";
    	$addCustomerArr['CustomerAdd']['Phone'] = "505-555-5555";
    	$addCustomerArr['CustomerAdd']['Email'] = "JDoe@gmail.com.com";
    	$addCustomerExample->setInnerArray($addCustomerArr);
    	$addCustomerXML = $addCustomerExample->asSanitizedXML();


        $addCustomerExample2 = new AddCustomer(43, "2.0");
    	//$addCustomerArr = $addCustomerExample->getInnerArray();
    	$addCustomerArr2 = array();
    	$addCustomerArr2['CustomerAdd']['Name'] = "John_Doe_".$_GET['a']."_1";//Seems to be the main identifier.
    	$addCustomerArr2['CustomerAdd']['CompanyName'] = "Lavu";
    	$addCustomerArr2['CustomerAdd']['FirstName'] = "John";
    	$addCustomerArr2['CustomerAdd']['LastName']  = "Doe";
    	$addCustomerArr2['CustomerAdd']['BillAddress']['Addr1'] = "1234 Street st.";
    	$addCustomerArr2['CustomerAdd']['BillAddress']['City']  = "Albuquerque";
    	$addCustomerArr2['CustomerAdd']['BillAddress']['State'] = "NM";
    	$addCustomerArr2['CustomerAdd']['BillAddress']['PostalCode'] = "87109";
    	$addCustomerArr2['CustomerAdd']['BillAddress']['Country'] = "United States";
    	$addCustomerArr2['CustomerAdd']['Phone'] = "505-555-5555";
    	$addCustomerArr2['CustomerAdd']['Email'] = "JDoe@gmail.com.com";
    	$addCustomerExample2->setInnerArray($addCustomerArr2);
    	$addCustomerXML2 = $addCustomerExample2->asSanitizedXML();
    	

    	$addCustomerExample3 = new AddCustomer(43, "2.0");
    	//$addCustomerArr = $addCustomerExample->getInnerArray();
    	$addCustomerArr3 = array();
    	$addCustomerArr3['CustomerAdd']['Name'] = "John_Doe_".$_GET['a']."_2";//Seems to be the main identifier.
    	$addCustomerArr3['CustomerAdd']['CompanyName'] = "Lavu";
    	$addCustomerArr3['CustomerAdd']['FirstName'] = "John";
    	$addCustomerArr3['CustomerAdd']['LastName']  = "Doe";
    	$addCustomerArr3['CustomerAdd']['BillAddress']['Addr1'] = "1234 Street st.";
    	$addCustomerArr3['CustomerAdd']['BillAddress']['City']  = "Albuquerque";
    	$addCustomerArr3['CustomerAdd']['BillAddress']['State'] = "NM";
    	$addCustomerArr3['CustomerAdd']['BillAddress']['PostalCode'] = "87109";
    	$addCustomerArr3['CustomerAdd']['BillAddress']['Country'] = "United States";
    	$addCustomerArr3['CustomerAdd']['Phone'] = "505-555-5555";
    	$addCustomerArr3['CustomerAdd']['Email'] = "JDoe@gmail.com.com";
    	$addCustomerExample3->setInnerArray($addCustomerArr3);
    	$addCustomerXML3 = $addCustomerExample3->asSanitizedXML();
    	return pushRequestOntoQueue( 
    	           array($addCustomerXML,$addCustomerXML2,$addCustomerXML3), 
    	           array('handleAddCustomer','handleAddCustomer','handleAddCustomer'), 
    	           'demo_brian_d');
?>