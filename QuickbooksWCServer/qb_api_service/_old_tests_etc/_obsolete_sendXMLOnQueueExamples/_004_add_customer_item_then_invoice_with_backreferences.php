<?php
        ini_set('display_errors',1);
        
        //enabling db
        require_once('/home/eventrentalsystems/public_html/cp/functions.php');
        $Conn = ers_connect_db();
        
        //The file we are testing
        require_once(dirname(__FILE__).'/../quickbook_functions.php');
        
        //Add Customer
        $addCustomerExample = new AddCustomer(43, "2.0");
    	$addCustomerArr = array();
    	$addCustomerArr['CustomerAdd']['Name'] = "Steve_Jobless_512";//Seems to be the main identifier.
    	$addCustomerArr['CustomerAdd']['CompanyName'] = "Lavu";
    	$addCustomerArr['CustomerAdd']['FirstName'] = "Steve";
    	$addCustomerArr['CustomerAdd']['LastName']  = "Jobless";
    	$addCustomerArr['CustomerAdd']['BillAddress']['Addr1'] = "1234 Street st.";
    	$addCustomerArr['CustomerAdd']['BillAddress']['City']  = "Albuquerque";
    	$addCustomerArr['CustomerAdd']['BillAddress']['State'] = "NM";
    	$addCustomerArr['CustomerAdd']['BillAddress']['PostalCode'] = "87109";
    	$addCustomerArr['CustomerAdd']['BillAddress']['Country'] = "United States";
    	$addCustomerArr['CustomerAdd']['Phone'] = "505-555-5555";
    	$addCustomerArr['CustomerAdd']['Email'] = "SJobless@gmail.com.com";
    	$addCustomerExample->setInnerArray($addCustomerArr);
    	$addCustomerXML = $addCustomerExample->asSanitizedXML();
    	//Add Item
    	$addInventoryObject = new ItemInventoryAdd(43, "7.0");
    	$addInventoryArr = array();
    	$addInventoryArr['ItemInventoryAdd'] = array();
    	$addInventoryArr['ItemInventoryAdd']['Name'] = "test_item_for_callback_example";
    	$addInventoryArr['ItemInventoryAdd']['SalesDesc'] = "Hot and delicious";
    	$addInventoryArr['ItemInventoryAdd']['SalesPrice'] = "14.25";
    	$addInventoryArr['ItemInventoryAdd']['IncomeAccountRef'] = array();
    	$addInventoryArr['ItemInventoryAdd']['IncomeAccountRef']['FullName'] = "Sales";//Don't know if we need yet.
    	$addInventoryArr['ItemInventoryAdd']['COGSAccountRef']['FullName'] = "Sales";
    	$addInventoryArr['ItemInventoryAdd']['AssetAccountRef'] = array();
    	$addInventoryArr['ItemInventoryAdd']['AssetAccountRef']['FullName'] = "Sales";
    	$addInventoryObject->setInnerArray($addInventoryArr);
    	$addInventorySanitizedXML = $addInventoryObject->asSanitizedXML();
    	//Add Invoice With Backreferences
    	$custumerListIDBackreference = "{[( 0 : ['QBXMLMsgsRs']['CustomerAddRs']['CustomerRet']['ListID'] )]}";
    	$itemFullNameBackreference   = "{[( 1 : ['QBXMLMsgsRs']['ItemInventoryAddRs']['ItemInventoryRet']['FullName'] )]}";
    	$invoiceAddObj = new InvoiceAdd('1','2.0');
    	$invoiceAddTestArr = array(); 
        $invoiceAddTestArr['InvoiceAdd'] = array();
        $invoiceAddTestArr['InvoiceAdd']['CustomerRef'] = array(); 
        $invoiceAddTestArr['InvoiceAdd']['CustomerRef']['ListID'] = $custumerListIDBackreference;
        $invoiceAddTestArr['InvoiceAdd']['TxnDate'] = '2014-01-01'; 
        $invoiceAddTestArr['InvoiceAdd']['RefNumber'] = '1234';
        $invoiceAddTestArr['InvoiceAdd']['BillAddress'] = array(); //We probably don't need this
        $invoiceAddTestArr['InvoiceAdd']['BillAddress']['Addr1'] = '1234 Street st';
        $invoiceAddTestArr['InvoiceAdd']['BillAddress']['City'] = 'Albuquerque';
        $invoiceAddTestArr['InvoiceAdd']['BillAddress']['State'] = 'NM';
        $invoiceAddTestArr['InvoiceAdd']['BillAddress']['PostalCode'] = '87109';
        $invoiceAddTestArr['InvoiceAdd']['BillAddress']['Country'] = 'United States';
        $invoiceAddTestArr['InvoiceAdd']['PONumber'] = '';
        $invoiceAddTestArr['InvoiceAdd']['Memo'] = '';
        $invoiceAddTestArr['InvoiceAdd']['InvoiceLineAdd'] = array(); 
        $invoiceAddTestArr['InvoiceAdd']['InvoiceLineAdd']['ItemRef'] = array(); 
        $invoiceAddTestArr['InvoiceAdd']['InvoiceLineAdd']['ItemRef']['FullName'] = $itemFullNameBackreference;
        $invoiceAddTestArr['InvoiceAdd']['InvoiceLineAdd']['Desc'] = 'Heres item ones description';
        $invoiceAddTestArr['InvoiceAdd']['InvoiceLineAdd']['Quantity'] = '1';
        $invoiceAddTestArr['InvoiceAdd']['InvoiceLineAdd']['Rate'] = '295';
        $invoiceAddObj->setInnerArray($invoiceAddTestArr);
    	$invoiceSanitizedXML = $invoiceAddObj->asSanitizedXML();
    	
    	return pushRequestOntoQueue( array($addCustomerXML,$addInventorySanitizedXML,$invoiceSanitizedXML), 
    	                             array('handleAddCustomer','handleItemInventoryAdd','handleItemInventoryAdd'), 
    	                               'demo_brian_d');
?>