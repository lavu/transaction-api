<?php
    //enabling db
    require_once('/home/eventrentalsystems/public_html/cp/functions.php');
    $Conn = ers_connect_db();
        
    //The file we are testing
    require_once(dirname(__FILE__).'/../quickbook_functions.php');
    
    /*
    $itemQueryObj = new ItemQuery(43, "7.0");
    $queryItemArr = array();
    $queryItemArr['FullName']='Good Time';
    $itemQueryObj->setInnerArray($queryItemArr);
    $itemQueryXML = $itemQueryObj->asSanitizedXML();
    */
    
    $itemSalesTaxAdd = new QBXMLGenericRequest(43, "12.0", 'ItemSalesTaxAdd');
    $ista = array();
    $ista['ItemSalesTaxAdd'] = array();
    $ista['ItemSalesTaxAdd']['Name'] = 'State_Tax';
    $ista['ItemSalesTaxAdd']['TaxVendorRef'] = array();
    $ista['ItemSalesTaxAdd']['TaxVendorRef']['FullName'] = 'NM State Tax Collector';
    $itemSalesTaxAdd->setInnerArray($ista);
    $itemSalesTaxAddXML = $itemSalesTaxAdd->asSanitizedXML();
    
    
    return pushRequestOntoQueue(array($itemSalesTaxAddXML), array('simplyClose'), 'demo_brian_d');
?>