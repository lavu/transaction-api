<?php
        ini_set("display_errors",1);
        //enabling db
        require_once('/home/eventrentalsystems/public_html/cp/functions.php');
        $Conn = ers_connect_db();
        
        //The file we are testing
        require_once(dirname(__FILE__).'/../quickbook_functions.php');
    	
    	$addInventoryObject = new ItemInventoryAdd(43, "7.0");
    	$addInventoryArr = array();
    	$addInventoryArr['ItemInventoryAdd'] = array();
    	$addInventoryArr['ItemInventoryAdd']['Name'] = "test_item_".$_GET['a'].'_0';
    	$addInventoryArr['ItemInventoryAdd']['SalesDesc'] = "Hot and delicious";
    	$addInventoryArr['ItemInventoryAdd']['SalesPrice'] = "14.25";
    	$addInventoryArr['ItemInventoryAdd']['IncomeAccountRef'] = array();
    	$addInventoryArr['ItemInventoryAdd']['IncomeAccountRef']['FullName'] = "Sales";//Don't know if we need yet.
    	$addInventoryArr['ItemInventoryAdd']['COGSAccountRef']['FullName'] = "Sales";
    	$addInventoryArr['ItemInventoryAdd']['AssetAccountRef'] = array();
    	$addInventoryArr['ItemInventoryAdd']['AssetAccountRef']['FullName'] = "Sales";
    	$addInventoryObject->setInnerArray($addInventoryArr);
    	$addInventorySanitizedXML = $addInventoryObject->asSanitizedXML();
    	
    	$addInventoryObject2 = new ItemInventoryAdd(43, "7.0");
    	$addInventoryArr2 = array();
    	$addInventoryArr2['ItemInventoryAdd'] = array();
    	$addInventoryArr2['ItemInventoryAdd']['Name'] = "test_item_".$_GET['a'].'_1';
    	$addInventoryArr2['ItemInventoryAdd']['SalesDesc'] = "Hot and delicious";
    	$addInventoryArr2['ItemInventoryAdd']['SalesPrice'] = "14.25";
    	$addInventoryArr2['ItemInventoryAdd']['IncomeAccountRef'] = array();
    	$addInventoryArr2['ItemInventoryAdd']['IncomeAccountRef']['FullName'] = "Sales";//Don't know if we need yet.
    	$addInventoryArr2['ItemInventoryAdd']['COGSAccountRef']['FullName'] = "Sales";
    	$addInventoryArr2['ItemInventoryAdd']['AssetAccountRef'] = array();
    	$addInventoryArr2['ItemInventoryAdd']['AssetAccountRef']['FullName'] = "Sales";
    	$addInventoryObject2->setInnerArray($addInventoryArr2);
    	$addInventorySanitizedXML2 = $addInventoryObject2->asSanitizedXML();
    	
    	$addInventoryObject3 = new ItemInventoryAdd(43, "7.0");
    	$addInventoryArr3 = array();
    	$addInventoryArr3['ItemInventoryAdd'] = array();
    	$addInventoryArr3['ItemInventoryAdd']['Name'] = "test_item_".$_GET['a'].'_2';
    	$addInventoryArr3['ItemInventoryAdd']['SalesDesc'] = "Hot and delicious";
    	$addInventoryArr3['ItemInventoryAdd']['SalesPrice'] = "14.25";
    	$addInventoryArr3['ItemInventoryAdd']['IncomeAccountRef'] = array();
    	$addInventoryArr3['ItemInventoryAdd']['IncomeAccountRef']['FullName'] = "Sales";//Don't know if we need yet.
    	$addInventoryArr3['ItemInventoryAdd']['COGSAccountRef']['FullName'] = "Sales";
    	$addInventoryArr3['ItemInventoryAdd']['AssetAccountRef'] = array();
    	$addInventoryArr3['ItemInventoryAdd']['AssetAccountRef']['FullName'] = "Sales";
    	$addInventoryObject3->setInnerArray($addInventoryArr3);
    	$addInventorySanitizedXML3 = $addInventoryObject3->asSanitizedXML();
        return pushRequestOntoQueue(
                    array($addInventorySanitizedXML,$addInventorySanitizedXML2,$addInventorySanitizedXML3), 
                    array('handleItemInventoryAdd','handleItemInventoryAdd','handleItemInventoryAdd'), 
                    'demo_brian_d');
?>