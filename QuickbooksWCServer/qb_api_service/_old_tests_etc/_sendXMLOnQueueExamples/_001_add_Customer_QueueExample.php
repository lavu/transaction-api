<?php
        ini_set('display_errors',1);
        
        //enabling db
        require_once('/home/eventrentalsystems/public_html/cp/functions.php');
        $Conn = ers_connect_db();
        
        //The file we are testing
        require_once(dirname(__FILE__).'/../quickbook_functions.php');
        
        $addCustomerExample = new AddCustomer(43, "2.0");
    	//$addCustomerArr = $addCustomerExample->getInnerArray();
    	$addCustomerArr = array();
    	$addCustomerArr['CustomerAdd']['Name'] = "John_Doe_".$_GET['a'];//Seems to be the main identifier.
    	$addCustomerArr['CustomerAdd']['CompanyName'] = "Lavu";
    	$addCustomerArr['CustomerAdd']['FirstName'] = "John";
    	$addCustomerArr['CustomerAdd']['LastName']  = "Doe";
    	$addCustomerArr['CustomerAdd']['BillAddress']['Addr1'] = "1234 Street st.";
    	$addCustomerArr['CustomerAdd']['BillAddress']['City']  = "Albuquerque";
    	$addCustomerArr['CustomerAdd']['BillAddress']['State'] = "NM";
    	$addCustomerArr['CustomerAdd']['BillAddress']['PostalCode'] = "87109";
    	$addCustomerArr['CustomerAdd']['BillAddress']['Country'] = "United States";
    	$addCustomerArr['CustomerAdd']['Phone'] = "505-555-5555";
    	$addCustomerArr['CustomerAdd']['Email'] = "JDoe@gmail.com.com";
    	
    	$addCustomerExample->setInnerArray($addCustomerArr);
    	$addCustomerXML = $addCustomerExample->asSanitizedXML();

    	return pushRequestOntoQueue( array($addCustomerXML), array('handleAddCustomer'), 'ers_demo_brian_d');
?>