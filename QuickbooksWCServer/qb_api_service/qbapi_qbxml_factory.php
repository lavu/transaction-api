<?php

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //
    //  Q B X M L - F A C T O R Y
    //
    //  FOR EACH FUNCTION $funcArgs should be validated prior to sending for its xml, but whatever.
    //
    
    
    //    -------------InvoiceAdd
    //   
    //    TOP LEVEL
    //    keys: customer_ref_full_name
    //          customer_ref_list_id
    //          date
    //          po_number (becomes order id)
    //          item_sales_tax_ref_full_name
    //          item_sales_tax_ref_list_id
    //          invoice_lines #array
    //              ->item_full_name
    //              ->item_list_id
    //              ->description
    //              ->quantity
    //              ->rate
    function get_InvoiceAdd_xml($funcArgs, $sanitizedXML = true){
        global $serverRow;
        
        //For when we employ macros.
        //$TxnMacroStr = !empty($funcArgs['txn_macro']) ? ':attribute_delim:defMacro=' . $funcArgs['txn_macro'] : "";//txt_macro field should be 'order_id'.
        //We build the XML object.
        
        $invoiceAddQBXMLObj = new QBXMLGenericRequest(1, "12.0", 'InvoiceAdd');
        
        $invoiceAddArr = array();
        
        
        //$invoiceAddKey = 'InvoiceAdd'.$TxnMacroStr;//We append the macro if it exists, else append "".
        //$invoiceAddKey = 'InvoiceAdd:attribute_delim:defMacro=TxnID:iwonder';//We append the macro if it exists, else append "".
        $invoiceAddKey = 'InvoiceAdd';
        
        $invoiceAddArr[$invoiceAddKey] = array();
        $invoiceAddArr[$invoiceAddKey]['CustomerRef'] = array();
        
        if(!empty($funcArgs['customer_ref_full_name']))
            $invoiceAddArr[$invoiceAddKey]['CustomerRef']['FullName'] = $funcArgs['customer_ref_full_name'];
        if(!empty($funcArgs['customer_ref_list_id']))
            $invoiceAddArr[$invoiceAddKey]['CustomerRef']['ListID'] = $funcArgs['customer_ref_list_id'];
        if(!empty($funcArgs['date']))
            $invoiceAddArr[$invoiceAddKey]['TxnDate'] = $funcArgs['date'];
        if(!empty($funcArgs['po_number']))
            $invoiceAddArr[$invoiceAddKey]['PONumber'] = $funcArgs['po_number'];
        /* //We'll implement this later.
        $invoiceAddArr['InvoiceAdd']['BillAddress'] = array(); //We probably don't need this
        $invoiceAddArr['InvoiceAdd']['BillAddress']['Addr1'] = '1234 Street st';
        $invoiceAddArr['InvoiceAdd']['BillAddress']['City'] = 'Albuquerque';
        $invoiceAddArr['InvoiceAdd']['BillAddress']['State'] = 'NM';
        $invoiceAddArr['InvoiceAdd']['BillAddress']['PostalCode'] = '87109';
        $invoiceAddArr['InvoiceAdd']['BillAddress']['Country'] = 'United States';
        */
        //Item Sales Tax
        $invoiceAddArr[$invoiceAddKey]['ItemSalesTaxRef'] = array();
        if(!empty($funcArgs['item_sales_tax_ref_full_name']))
            $invoiceAddArr[$invoiceAddKey]['ItemSalesTaxRef']['FullName'] = $funcArgs['item_sales_tax_ref_full_name'];
        if(!empty($funcArgs['item_sales_tax_ref_list_id']))
            $invoiceAddArr[$invoiceAddKey]['ItemSalesTaxRef']['ListID'] = $funcArgs['item_sales_tax_ref_list_id'];
        //Invoice Lines
        $i = 0;
        foreach($funcArgs['invoice_lines'] as $currInvoiceLine){
            $i_disp = $i==0 ? "" : "||".$i;//For multiple siblings of same type we need to append something to keep keys unique.
            $invoiceAddArr[$invoiceAddKey]['InvoiceLineAdd'.$i_disp] = array();
            $invoiceAddArr[$invoiceAddKey]['InvoiceLineAdd'.$i_disp]['ItemRef'] = array();
            if(!empty($currInvoiceLine['item_full_name']))
                $invoiceAddArr[$invoiceAddKey]['InvoiceLineAdd'.$i_disp]['ItemRef']['FullName'] = $currInvoiceLine['item_full_name'];
            if(!empty($currInvoiceLine['item_list_id']))
                $invoiceAddArr[$invoiceAddKey]['InvoiceLineAdd'.$i_disp]['ItemRef']['ListID'] = $currInvoiceLine['item_list_id'];
            if(!empty($currInvoiceLine['description']))
                $invoiceAddArr[$invoiceAddKey]['InvoiceLineAdd'.$i_disp]['Desc'] = $currInvoiceLine['description'];
                   
            if(isset($currInvoiceLine['quantity'])){
                $invoiceAddArr[$invoiceAddKey]['InvoiceLineAdd'.$i_disp]['Quantity'] = $currInvoiceLine['quantity'];
            }
            if(isset($currInvoiceLine['rate_percent'])){
                $invoiceAddArr[$invoiceAddKey]['InvoiceLineAdd'.$i_disp]['RatePercent'] = $currInvoiceLine['rate_percent'];
            }
            if(isset($currInvoiceLine['rate'])){
                $invoiceAddArr[$invoiceAddKey]['InvoiceLineAdd'.$i_disp]['Rate'] = $currInvoiceLine['rate'];
            }
            if(isset($currInvoiceLine['amount'])){
                $invoiceAddArr[$invoiceAddKey]['InvoiceLineAdd'.$i_disp]['Amount'] = $currInvoiceLine['amount'];
            }
            $i++;
        }
        $invoiceAddQBXMLObj->setInnerArray($invoiceAddArr);
        
        //echo $invoiceAddQBXMLObj->asXML();
        
        return $sanitizedXML ? $invoiceAddQBXMLObj->asSanitizedXML() : $invoiceAddQBXMLObj->asXML();
        //echo $invoiceAddQBXMLObj->asXML();
        //exit;
    }
    
    


    //    -------------Receive Payment Add
    //   
    //    TOP LEVEL
    //    keys: customer_ref_full_name
    //          ar_account_ref_full_name
    //          total_amount
    //          ref_number
    //          payment_method_ref_full_name
    /*
    IS TOTALLY VALID, BUT WE SHOULD WANT TO USE THE MACRO FOR REFERENCING THE INVOICE.
    function get_ReceivePaymentAdd_xml($funcArgs, $sanitizedXML = true){
        $receivePaymentAddQBXMLObj = new QBXMLGenericRequest(1, "12.0", 'ReceivePaymentAdd');
        $receivePaymentArr = array();
        $receivePaymentArr['ReceivePaymentAdd']['CustomerRef']['FullName'] = $funcArgs['customer_ref_full_name'];
        $receivePaymentArr['ReceivePaymentAdd']['ARAccountRef']['FullName'] = $funcArgs['ar_account_ref_full_name'];
        $receivePaymentArr['ReceivePaymentAdd']['RefNumber'] = $funcArgs['ref_number'];//Referent to the invoice #
        $receivePaymentArr['ReceivePaymentAdd']['TotalAmount'] = $funcArgs['total_amount'];
        $receivePaymentArr['ReceivePaymentAdd']['PaymentMethodRef']['FullName'] = $funcArgs['payment_method_ref_full_name'];
        $receivePaymentArr['ReceivePaymentAdd']['IsAutoApply'] = 'true';
        $receivePaymentAddQBXMLObj->setInnerArray($receivePaymentArr);
        return $sanitizedXML ? $receivePaymentAddQBXMLObj->asSanitizedXML() : $receivePaymentAddQBXMLObj->asXML();
    }
    */
    function get_ReceivePaymentAdd_xml($funcArgs, $TxnID_action_index, $sanitizedXML = true){
        //$TxnMacroStr = !empty($funcArgs['txn_macro']) ? ':attribute_delim:defMacro=' . $funcArgs['txn_macro'] : "";//txt_macro field should be 'order_id'.
        //$receivePaymentAddKey = 'ReceivePaymentAdd'.$TxnMacroStr;//We append the macro if it exists, else append "".
        $receivePaymentAddKey = 'ReceivePaymentAdd';
        $receivePaymentAddQBXMLObj = new QBXMLGenericRequest(1, "12.0", 'ReceivePaymentAdd');
        $receivePaymentArr = array();
        $receivePaymentArr[$receivePaymentAddKey]['CustomerRef']['FullName'] = $funcArgs['customer_ref_full_name'];//1
        $receivePaymentArr[$receivePaymentAddKey]['ARAccountRef']['FullName'] = $funcArgs['ar_account_ref_full_name'];//2
        //$receivePaymentArr[$receivePaymentAddKey]['RefNumber'] = $funcArgs['ref_number'];
        $receivePaymentArr[$receivePaymentAddKey]['TotalAmount'] = $funcArgs['total_amount'];//3
        $receivePaymentArr[$receivePaymentAddKey]['PaymentMethodRef']['FullName'] = $funcArgs['payment_method_ref_full_name'];//4
        
        //$receivePaymentArr['ReceivePaymentAdd'][''] = '';
        //$useMacroOrEmpty = !empty($funcArgs['txn_macro']) ? ':attribute_delim:useMacro=' . $funcArgs['txn_macro'] : "";//txt_macro field should be 'order_id'.
        
        //$receivePaymentArr[$receivePaymentAddKey]['AppliedToTxnAdd']['TxnID'.$useMacroOrEmpty] = "";
        //$receivePaymentArr[$receivePaymentAddKey]['AppliedToTxnAdd']['TxnID:attribute_delim:useMacro=TxnID:iwonder'] = null;
        //    {[( 0 : ['QBXMLMsgsRs']['CustomerAddRs']['CustomerRet']['ListID'] )]}
        $receivePaymentArr[$receivePaymentAddKey]['AppliedToTxnAdd']['TxnID'] = "{[( $TxnID_action_index : ['QBXMLMsgsRs']['InvoiceAddRs']['InvoiceRet']['TxnID'] )]}";//5
        $receivePaymentArr[$receivePaymentAddKey]['AppliedToTxnAdd']['PaymentAmount'] = $funcArgs['total_amount'];//6
        
        $receivePaymentAddQBXMLObj->setInnerArray($receivePaymentArr);
        //echo $receivePaymentAddQBXMLObj->asXML();
        
        return $sanitizedXML ? $receivePaymentAddQBXMLObj->asSanitizedXML() : $receivePaymentAddQBXMLObj->asXML();
        //echo $receivePaymentAddQBXMLObj->asXML();
        //exit;
    }
    //Same thing but for multiple payments.
    function get_mult_ReceivePaymentAdd_xmls($funcArgs, $useIDFilter, $TxnID_action_index, $sanitizedXML = true, &$serverPaymentIDs_Out){
        if(!$useIDFilter || !is_array($useIDFilter)){
            /* TODO THROW EXCEPTION */
            return array();
        }

        $allReceivePaymentXMLs = array();
        //Array containing => 'total_amount' and => 'payment_method_ref_full_name'.
        $multiPaymentAmountAndMethodArr = $funcArgs['receive_payment_and_method_amounts_arr'];
        
        //echo "<pre>\n\n\nget_mult_ReceivePaymentAdd_xmls\n" . print_r($multiPaymentAmountAndMethodArr,1) . "\n\n\n";
        
        $currReceivePaymentArray = array();
        $serverPaymentIDs_Out = array();//Out var for cc_transaction ids array.
        foreach($multiPaymentAmountAndMethodArr as $currPaymentAmountAndType){
        
            //We limit the creation of 'ReceivePaymentAdd' xmls to those that are listed in the filter array by key and value. (e.g. array(24=>24, 58=>58,...) ).

            if(!isset($useIDFilter[$currPaymentAmountAndType['servers_payment_id']]) || empty($useIDFilter[$currPaymentAmountAndType['servers_payment_id']])){
                continue;
            }
            
            $serverPaymentIDs_Out[] = $currPaymentAmountAndType['servers_payment_id'];
            $currReceivePaymentArray['ReceivePaymentAdd']['CustomerRef']['FullName'] = $funcArgs['customer_ref_full_name'];//1
            $currReceivePaymentArray['ReceivePaymentAdd']['ARAccountRef']['FullName'] = $funcArgs['ar_account_ref_full_name'];//2
            $currReceivePaymentArray['ReceivePaymentAdd']['TotalAmount'] = $currPaymentAmountAndType['total_amount'];//3
            $currReceivePaymentArray['ReceivePaymentAdd']['PaymentMethodRef']['FullName'] = $currPaymentAmountAndType['payment_method_ref_full_name'];//4
            $currReceivePaymentArray['ReceivePaymentAdd']['AppliedToTxnAdd']['TxnID'] = "{[( $TxnID_action_index : ['QBXMLMsgsRs']['InvoiceAddRs']['InvoiceRet']['TxnID'] )]}";//5
            //The individual payment aspects that may differ.
            $currReceivePaymentArray['ReceivePaymentAdd']['AppliedToTxnAdd']['PaymentAmount'] = $currPaymentAmountAndType['total_amount'];//6
            
            $receivePaymentAddQBXMLObj = new QBXMLGenericRequest(1, "12.0", 'ReceivePaymentAdd');
            $receivePaymentAddQBXMLObj->setInnerArray($currReceivePaymentArray);
            $allReceivePaymentXMLs[] = $sanitizedXML ? $receivePaymentAddQBXMLObj->asSanitizedXML() : $receivePaymentAddQBXMLObj->asXML();
        }
        
        //echo "<pre>----------------\n".print_r($allReceivePaymentXMLs,1)."\n--------------------</pre>";
        //exit;
        return $allReceivePaymentXMLs;
    }
    
    function get_mult_ReceivePaymentMod_xmls($funcArgs, $serverPaymentID_2_map_variables, //$serverPaymentID_2_map_variables => qb_txn, edit_sequence
                                                $useIDFilter, $TxnID_action_index, $sanitizedXML = true, &$serverPaymentIDs_Out){
        if(!$useIDFilter || !is_array($useIDFilter)){
            /*TODO THROW EXCEPTION*/
            return array();
        }
        $allReceivePaymentModXMLs = array();
        $multiPaymentAmountAndMethodArr = $funcArgs['receive_payment_and_method_amounts_arr'];
        
        //echo "<pre>\n\n\nget_mult_ReceivePaymentMod_xmls\n" . print_r($multiPaymentAmountAndMethodArr) . "\n\n\n";
        
        $currReceivePaymentModArray = array();
        $serverPaymentIDs_Out = array();
        foreach($multiPaymentAmountAndMethodArr as $currPaymentAmountAndType){
            //If using the filter, then only allow those transactions to get modified that are in the $useIDFilter array.
            
            if(!isset($useIDFilter[$currPaymentAmountAndType['servers_payment_id']]) || empty($useIDFilter[$currPaymentAmountAndType['servers_payment_id']])){
                continue;
            }
            
            $serverPaymentIDs_Out[] = $currPaymentAmountAndType['servers_payment_id'];

            $currReceivePaymentModArray['ReceivePaymentMod']['TxnID'] = 
                $serverPaymentID_2_map_variables[$currPaymentAmountAndType['servers_payment_id']]['qb_txn'];
            $currReceivePaymentModArray['ReceivePaymentMod']['EditSequence'] = 
                $serverPaymentID_2_map_variables[$currPaymentAmountAndType['servers_payment_id']]['edit_sequence'];
                    
            
            $currReceivePaymentModArray['ReceivePaymentMod']['CustomerRef']['FullName'] = $funcArgs['customer_ref_full_name'];//1
            $currReceivePaymentModArray['ReceivePaymentMod']['ARAccountRef']['FullName'] = $funcArgs['ar_account_ref_full_name'];//2
            $currReceivePaymentModArray['ReceivePaymentMod']['TotalAmount'] = $currPaymentAmountAndType['total_amount'];//3
            $currReceivePaymentModArray['ReceivePaymentMod']['PaymentMethodRef']['FullName'] = $currPaymentAmountAndType['payment_method_ref_full_name'];//4
            $currReceivePaymentModArray['ReceivePaymentMod']['AppliedToTxnMod']['TxnID'] = "{[( $TxnID_action_index : ['QBXMLMsgsRs']['InvoiceAddRs']['InvoiceRet']['TxnID'] )]}";//5
            $currReceivePaymentModArray['ReceivePaymentMod']['AppliedToTxnMod']['PaymentAmount'] = $currPaymentAmountAndType['total_amount'];//6
            
            $receivePaymentModQBXMLObj = new QBXMLGenericRequest(1, "12.0", 'ReceivePaymentMod');
            $receivePaymentModQBXMLObj->setInnerArray($currReceivePaymentModArray);
            $allReceivePaymentModXMLs[] = $sanitizedXML ? $receivePaymentModQBXMLObj->asSanitizedXML() : $receivePaymentModQBXMLObj->asXML();
        }
        return $allReceivePaymentModXMLs;
    }
    
    
    //    -------------CustomerAdd
    //   
    //    TOP LEVEL
    //    keys: customer_ref_full_name
    //          company_name
    //          first_name
    //          last_name
    //          bill_address #array
    //              ->addr1
    //              ->city
    //              ->state
    //              ->postal_code
    //              ->country
    //          phone
    //          email
    function get_CustomerAdd_xml($funcArgs, $sanitizedXML = true){
        $addCustomerQBXMLObj = new AddCustomer(43, "2.0");
    	//$addCustomerArr = $addCustomerExample->getInnerArray();
    	$addCustomerArr = array();
    	$addCustomerArr['CustomerAdd']['Name'] = $funcArgs['customer_ref_full_name'];
    	//company
    	if(isset($funcArgs['company_name']))
    	   $addCustomerArr['CustomerAdd']['CompanyName'] = $funcArgs['company_name'];
    	if(isset($funcArgs['first_name']))
    	   $addCustomerArr['CustomerAdd']['FirstName'] = $funcArgs['first_name'];
        if(isset($funcArgs['last_name']))
    	   $addCustomerArr['CustomerAdd']['LastName']  = $funcArgs['last_name'];
        if(isset($funcArgs['bill_address']) && is_array($funcArgs['bill_address'])){
            if(isset($funcArgs['bill_address']['addr1']))
                $addCustomerArr['CustomerAdd']['BillAddress']['Addr1'] = $funcArgs['bill_address']['addr1'];
            if(isset($funcArgs['bill_address']['city']))
                $addCustomerArr['CustomerAdd']['BillAddress']['City']  = $funcArgs['bill_address']['city'];
            if(isset($funcArgs['bill_address']['state']))
                $addCustomerArr['CustomerAdd']['BillAddress']['State'] = $funcArgs['bill_address']['state'];
            if(isset($funcArgs['bill_address']['postal_code']))
                $addCustomerArr['CustomerAdd']['BillAddress']['PostalCode'] = $funcArgs['bill_address']['postal_code'];
            if(isset($funcArgs['bill_address']['country']))
                $addCustomerArr['CustomerAdd']['BillAddress']['Country'] = $funcArgs['bill_address']['country'];            
        }
        if(isset($funcArgs['phone']))
    	   $addCustomerArr['CustomerAdd']['Phone'] = $funcArgs['phone'];
    	if(isset($funcArgs['email']))
    	   $addCustomerArr['CustomerAdd']['Email'] = $funcArgs['email'];
        $addCustomerQBXMLObj->setInnerArray($addCustomerArr);
        return $sanitizedXML ? $addCustomerQBXMLObj->asSanitizedXML() : $addCustomerQBXMLObj->asXML();
    }
    
    //    -------------InventoryItemAdd
    //   
    //    TOP LEVEL
    //    keys: inventory_item_ref_full_name
    //          sales_description
    //          sales_price
    //          income_account_ref_full_name
    //          income_account_ref_list_id
    //          cogs_account_ref_full_name
    //          cogs_account_ref_list_id
    //          asset_account_ref_full_name
    //          asset_account_ref_list_id
    //
    function get_InventoryItemAdd_xml($funcArgs, $sanitizedXML = true){
    	$addInventoryObject = new ItemInventoryAdd(43, "12.0");
    	$addInventoryArr = array();
    	$addInventoryArr['ItemInventoryAdd'] = array();
    	$addInventoryArr['ItemInventoryAdd']['Name'] = $funcArgs['inventory_item_ref_full_name'];
        //error_log("Parent:".$funcArgs['inventory_items_parent']);
    	if(!$empty($funcArgs['inventory_items_parent'])){
        	$addInventoryArr['ItemInventoryAdd']['ParentRef']['FullName'] = $funcArgs['inventory_items_parent'];
    	}
    	if(isset($funcArgs['sales_description']))
    	   $addInventoryArr['ItemInventoryAdd']['SalesDesc'] = $funcArgs['sales_description'];
        if(isset($funcArgs['sales_price']))
    	   $addInventoryArr['ItemInventoryAdd']['SalesPrice'] = $funcArgs['sales_price'];
        
        $addInventoryArr['ItemInventoryAdd']['IncomeAccountRef'] = array();
        if(isset($funcArgs['income_account_ref_full_name']))
    	   $addInventoryArr['ItemInventoryAdd']['IncomeAccountRef']['FullName'] = $funcArgs['income_account_ref_full_name'];
        
        $addInventoryArr['ItemInventoryAdd']['COGSAccountRef'] = array();
        if(isset($funcArgs['cogs_account_ref_full_name']))
    	   $addInventoryArr['ItemInventoryAdd']['COGSAccountRef']['FullName'] = $funcArgs['cogs_account_ref_full_name'];
        
    	$addInventoryArr['ItemInventoryAdd']['AssetAccountRef'] = array();
    	$addInventoryArr['ItemInventoryAdd']['AssetAccountRef']['FullName'] = $funcArgs['asset_account_ref_full_name'];
    	

    	
    	$addInventoryObject->setInnerArray($addInventoryArr);
    	
    	$addInventoryReturnXML = $sanitizedXML ? $addInventoryObject->asSanitizedXML() : $addInventoryObject->asXML();
    	return $addInventoryReturnXML;
    }
    
    //    -------------MultipleInventoryItemsAdd
    //   
    //    TOP LEVEL
    //    Held in top level array: inventory_items
    //    [
    //         {
    //           "inventory_item_ref_full_name":"",
    //           "sales_description":"",
    //           "sales_price":"",
    //           "income_account_ref_full_name":"",
    //           "income_account_ref_list_id":"",
    //           "cogs_account_ref_full_name":"",
    //           "cogs_account_ref_list_id":"",
    //           "asset_account_ref_full_name":"",
    //           "asset_account_ref_list_id":""
    //         },
    //         { ... }, ...
    //    ]
    //
    function get_InventoryItemAdd_xmls($funcArgs, $sanitizedXML = true){
    
        $arrayOfInventoryItems = $funcArgs['inventory_items'];
        $arrayOfItemInventoryAddXMLs = array();//Parallel to arrayOfInventoryItems.
        
        foreach($arrayOfInventoryItems as $currInventoryItem){
            $addInventoryObject = new ItemInventoryAdd(43, "12.0");
        	$addInventoryArr = array();
        	$addInventoryArr['ItemInventoryAdd'] = array();
        	
        	$addInventoryArr['ItemInventoryAdd']['Name'] = $currInventoryItem['inventory_item_ref_full_name'];
        	
            if(!empty($funcArgs['inventory_items_parent'])){
            	$addInventoryArr['ItemInventoryAdd']['ParentRef']['FullName'] = $funcArgs['inventory_items_parent'];
        	}
        	
        	if(isset($currInventoryItem['sales_description'])){
        	   $addInventoryArr['ItemInventoryAdd']['SalesDesc'] = $currInventoryItem['sales_description'];
            }
            

            
            if(isset($currInventoryItem['sales_price'])){
        	   $addInventoryArr['ItemInventoryAdd']['SalesPrice'] = $currInventoryItem['sales_price'];
            }
            $addInventoryArr['ItemInventoryAdd']['IncomeAccountRef'] = array();
            if(isset($currInventoryItem['income_account_ref_full_name'])){
        	   $addInventoryArr['ItemInventoryAdd']['IncomeAccountRef']['FullName'] = $currInventoryItem['income_account_ref_full_name'];
            }
            
            $addInventoryArr['ItemInventoryAdd']['COGSAccountRef'] = array();
            if(isset($currInventoryItem['cogs_account_ref_full_name'])){
        	   $addInventoryArr['ItemInventoryAdd']['COGSAccountRef']['FullName'] = $currInventoryItem['cogs_account_ref_full_name'];
            }
        	$addInventoryArr['ItemInventoryAdd']['AssetAccountRef'] = array();
        	$addInventoryArr['ItemInventoryAdd']['AssetAccountRef']['FullName'] = $currInventoryItem['asset_account_ref_full_name'];



        	$addInventoryObject->setInnerArray($addInventoryArr);
        	
        	$addInventoryXML = $sanitizedXML ? $addInventoryObject->asSanitizedXML() : $addInventoryObject->asXML();
        	$arrayOfItemInventoryAddXMLs[] = $addInventoryXML;
        }

    	return $arrayOfItemInventoryAddXMLs;
    }
    
    
    
    
    function get_deleteInvoiceXML($txnID, $sanitizedXML = true){
        $deleteInvoiceXML = '<?xml version="1.0" encoding="utf-8"?><?qbxml version="4.0"?><QBXML><QBXMLMsgsRq onError="stopOnError"><TxnDelRq><TxnDelType>Invoice</TxnDelType><TxnID>'.$txnID.'</TxnID></TxnDelRq></QBXMLMsgsRq></QBXML>';
        return $sanitizedXML ? SimpleXMLElementEnhanced::xml_sanitize($deleteInvoiceXML) : $deleteInvoiceXML;
    }
    
    
    
    function get_testXMLForMacros(){
        $xmlText = '
            <?xml version="1.0" encoding="utf-8"?>
            <?qbxml version="12.0"?>
            <QBXML>
                <QBXMLMsgsRq onError="stopOnError">
                    <InvoiceAddRq requestID="1">
                        <InvoiceAdd defMacro="TxnID:iwonder">
                            <CustomerRef>
                                <FullName>General Customer</FullName>
                            </CustomerRef>
                            <TxnDate>2014-06-05</TxnDate>
                            <ItemSalesTaxRef>
                                <FullName>SalesTax</FullName>
                            </ItemSalesTaxRef>
                            <InvoiceLineAdd>
                                <ItemRef>
                                    <FullName>Crostini Tricolore</FullName>
                                </ItemRef>
                                <Desc>Hot and Spicy</Desc>
                                <Quantity>3</Quantity>
                                <Rate>215</Rate>
                            </InvoiceLineAdd>
                        </InvoiceAdd>
                    </InvoiceAddRq>
                </QBXMLMsgsRq>
                <ReceivePaymentAddRq requestID="1">
                        <ReceivePaymentAdd>
                            <CustomerRef>
                                <FullName>General Customer</FullName>
                            </CustomerRef>
                            <ARAccountRef>
                                <FullName>Accounts Receivable</FullName>
                            </ARAccountRef>
                            <TotalAmount>10.00</TotalAmount>
                            <PaymentMethodRef>
                                <FullName>Cash</FullName>
                            </PaymentMethodRef>
                            <AppliedToTxnAdd>
                                <TxnID useMacro="TxnID:iwonder" />
                                <PaymentAmount>10.00</PaymentAmount>
                            </AppliedToTxnAdd>
                        </ReceivePaymentAdd>
                </ReceivePaymentAddRq>
            </QBXML>
        ';

        $xmlText = '<?xml version="1.0" encoding="utf-8"?><?qbxml version="12.0"?><QBXML><QBXMLMsgsRq onError="stopOnError"><InvoiceAddRq requestID="1"><InvoiceAdd defMacro="TxnID:iwonder"><CustomerRef><FullName>General Customer</FullName></CustomerRef><TxnDate>2014-06-05</TxnDate><ItemSalesTaxRef><FullName>SalesTax</FullName></ItemSalesTaxRef><InvoiceLineAdd><ItemRef><FullName>Crostini Tricolore</FullName></ItemRef><Desc>Hot and Spicy</Desc><Quantity>3</Quantity><Rate>215</Rate></InvoiceLineAdd></InvoiceAdd></InvoiceAddRq></QBXMLMsgsRq><ReceivePaymentAddRq requestID="1"><ReceivePaymentAdd><CustomerRef><FullName>General Customer</FullName></CustomerRef><ARAccountRef><FullName>Accounts Receivable</FullName></ARAccountRef><TotalAmount>10.00</TotalAmount><PaymentMethodRef><FullName>Cash</FullName></PaymentMethodRef><AppliedToTxnAdd><TxnID useMacro="TxnID:iwonder" /><PaymentAmount>10.00</PaymentAmount></AppliedToTxnAdd></ReceivePaymentAdd></ReceivePaymentAddRq></QBXML>';

        //echo $xmlText;
        return SimpleXMLElementEnhanced::xml_sanitize($xmlText);
    }
    
?>