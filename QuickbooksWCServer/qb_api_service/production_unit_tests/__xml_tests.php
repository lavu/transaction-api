<?php

    ini_set('display_errors', 1);
    require_once(dirname(__FILE__).'/../QBXMLClasses.php');
    
    $sampleQBXMAddCustomerStatusOKResponse = '<?xml version="1.0" ?>
<QBXML>
<QBXMLMsgsRs>
<CustomerAddRs requestID="43" statusCode="0" statusSeverity="Info" statusMessage="Status OK">
<CustomerRet>
<ListID>80000054-1393631113</ListID>
<TimeCreated>2014-02-28T16:45:13-07:00</TimeCreated>
<TimeModified>2014-02-28T16:45:13-07:00</TimeModified>
<EditSequence>1393631113</EditSequence>
<Name>Steve_Jobless_121</Name>
<FullName>Steve_Jobless_121</FullName>
<IsActive>true</IsActive>
<Sublevel>0</Sublevel>
<CompanyName>Lavu</CompanyName>
<FirstName>John</FirstName>
<LastName>Doe</LastName>
<BillAddress>
<Addr1>1234 Street st.</Addr1>
<City>Albuquerque</City>
<State>NM</State>
<PostalCode>87109</PostalCode>
<Country>USA</Country>
</BillAddress>
<Phone>505-555-5555</Phone>
<Email>JDoe@gmail.com.com</Email>
<Balance>0.00</Balance>
<TotalBalance>0.00</TotalBalance>
<JobStatus>None</JobStatus>
</CustomerRet>
</CustomerAddRs>
</QBXMLMsgsRs>
</QBXML>';

    runTests();
    function runTests(){
        xml_drilldown_should_succeed();
        xml_drilldown_should_fail();
    }

    function xml_drilldown_should_succeed(){
        global $sampleQBXMAddCustomerStatusOKResponse;
        $addCustomerStatusOK_xml_obj = new SimpleXMLElementEnhanced($sampleQBXMAddCustomerStatusOKResponse);
        $drillDownArr = array('QBXMLMsgsRs','CustomerAddRs','CustomerRet','BillAddress','City');
        $cityNode = $addCustomerStatusOK_xml_obj->getNodeAtPathOrFalse($drillDownArr);
        $debugLine = $addCustomerStatusOK_xml_obj->getLastPathDrilldownDebug();
        echo $debugLine."<br><br>";
        echo "Value is: " . $cityNode->getInnerText()."<br><br>";
    }
    
    function xml_drilldown_should_fail(){
        global $sampleQBXMAddCustomerStatusOKResponse;
        $addCustomerStatusOK_xml_obj = new SimpleXMLElementEnhanced($sampleQBXMAddCustomerStatusOKResponse);
        $drillDownArr = array('QBXMLMsgsRs','CustomerAddRs','CustomerRet','BillAddress','foo');
        $cityNode = $addCustomerStatusOK_xml_obj->getNodeAtPathOrFalse($drillDownArr);
        $debugLine = $addCustomerStatusOK_xml_obj->getLastPathDrilldownDebug();
        echo $debugLine;
    }
?>