<?php
    ini_set('display_errors',1);
    
    //enabling db
    require_once('/home/eventrentalsystems/public_html/cp/functions.php');
    $Conn = ers_connect_db();
    
    //The file we are testing
    require_once(dirname(__FILE__).'/../quickbook_functions.php');

    //Add Customer
    $addCustomerExample = new AddCustomer(43, "2.0");
	$addCustomerArr = array();
	$addCustomerArr['CustomerAdd']['Name'] = "Steve_Jobless_121";//Seems to be the main identifier.
	$addCustomerArr['CustomerAdd']['CompanyName'] = "Lavu";
	$addCustomerArr['CustomerAdd']['FirstName'] = "John";
	$addCustomerArr['CustomerAdd']['LastName']  = "Doe";
	$addCustomerArr['CustomerAdd']['BillAddress']['Addr1'] = "1234 Street st.";
	$addCustomerArr['CustomerAdd']['BillAddress']['City']  = "Albuquerque";
	$addCustomerArr['CustomerAdd']['BillAddress']['State'] = "NM";
	$addCustomerArr['CustomerAdd']['BillAddress']['PostalCode'] = "87109";
	$addCustomerArr['CustomerAdd']['BillAddress']['Country'] = "United States";
	$addCustomerArr['CustomerAdd']['Phone'] = "505-555-5555";
	$addCustomerArr['CustomerAdd']['Email'] = "JDoe@gmail.com.com";
	$addCustomerExample->setInnerArray($addCustomerArr);
	$addCustomerXML = $addCustomerExample->asSanitizedXML(); 
	
	//Customer Query (used twice)
	$customerQueryByName = new CustomerQuery_by_name(43, "5.0");
	$customerQueryArr = array();
	$customerQueryArr['FullName'] = 'Steve_Jobless_121';
	$customerQueryByName->setInnerArray($customerQueryArr);
	$customerQueryXML = $customerQueryByName->asSanitizedXML();

	//Remove Customer
	$removeCustomerExample = new ListDel(43, "12.0");//"1.1");
	$removeCustomerArr = array();
	$removeCustomerArr['ListDelType'] = 'Customer';//ldtCustomer
	$customerCallback = "{[( 1 : ['QBXMLMsgsRs']['CustomerQueryRs']['CustomerRet']['ListID'] )]}";
    $removeCustomerArr['ListID'] = $customerCallback;
	$removeCustomerExample->setInnerArray($removeCustomerArr);
	$removeCustomerXML = $removeCustomerExample->asSanitizedXML();
	echo $removeCustomerXML;
    
    pushRequestOntoQueue(array($addCustomerXML, $customerQueryXML, $removeCustomerXML, $customerQueryXML), 
            array('_0001_add_customer_part',    '_0001_query_customer_ensure_exists',
                  '_0001_delete_customer_part', '_0001_query_customer_ensure_deleted'),
                            'demo_brian_d');
?>