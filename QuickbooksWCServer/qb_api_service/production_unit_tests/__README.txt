These unit tests are to be ran in the production environment within a testing account.
Each unit test will make a one or more transactions, validate that they were made, 
undo the changes it has done, verify it has changed it back, and finally make a live
report to the monitoring engine if anything wrong is noticed.