<?php
    //enabling db
    require_once('/home/eventrentalsystems/public_html/cp/functions.php');
    $Conn = ers_connect_db();
        
    //The file we are testing
    require_once(dirname(__FILE__).'/../quickbook_functions.php');
    
    $invoiceAddQBXMLObj = new QBXMLGenericRequest(1, "12.0", 'InvoiceAdd');
    $invoiceAddTestArr = array(); 
    $invoiceAddTestArr['InvoiceAdd'] = array();

    $invoiceAddTestArr['InvoiceAdd']['CustomerRef'] = array(); 
    $invoiceAddTestArr['InvoiceAdd']['CustomerRef']['FullName'] = "Hilda_Anderson";
    $invoiceAddTestArr['InvoiceAdd']['TxnDate'] = '2014-01-01'; 
    $invoiceAddTestArr['InvoiceAdd']['RefNumber'] = '01289abcdef';
    $invoiceAddTestArr['InvoiceAdd']['BillAddress'] = array(); //We probably don't need this
    $invoiceAddTestArr['InvoiceAdd']['BillAddress']['Addr1'] = '1234 Street st';
    $invoiceAddTestArr['InvoiceAdd']['BillAddress']['City'] = 'Albuquerque';
    $invoiceAddTestArr['InvoiceAdd']['BillAddress']['State'] = 'NM';
    $invoiceAddTestArr['InvoiceAdd']['BillAddress']['PostalCode'] = '87109';
    $invoiceAddTestArr['InvoiceAdd']['BillAddress']['Country'] = 'United States';
    
    
    // I M P O R T A N T--  Either Area1 or Area2 has to be commented out, if both are not then parse error, why?
    
    // Area1
    //$invoiceAddTestArr['InvoiceAdd']['PONumber'] = '';
    //$invoiceAddTestArr['InvoiceAdd']['Memo'] = '';
    
    // Area2
    $invoiceAddTestArr['InvoiceAdd']['ItemSalesTaxRef'] = array();
    $invoiceAddTestArr['InvoiceAdd']['ItemSalesTaxRef']['FullName'] = 'SalesTax';
    //$invoiceAddTestArr['InvoiceAdd']['ItemSalesTaxRef']['ListID'] = '8000000C-1394234458';
    
    //
    $invoiceAddTestArr['InvoiceAdd']['InvoiceLineAdd'] = array(); 
    
    //Testing if we do not need this.
    //We absolutely need to specify an item if there's an amount.
    $invoiceAddTestArr['InvoiceAdd']['InvoiceLineAdd']['ItemRef'] = array(); 
    $invoiceAddTestArr['InvoiceAdd']['InvoiceLineAdd']['ItemRef']['FullName'] = 'Hamburger';
    
    $invoiceAddTestArr['InvoiceAdd']['InvoiceLineAdd']['Desc'] = 'Heres item ones description';
    $invoiceAddTestArr['InvoiceAdd']['InvoiceLineAdd']['Quantity'] = '1';
    $invoiceAddTestArr['InvoiceAdd']['InvoiceLineAdd']['Rate'] = '123';
    $invoiceAddQBXMLObj->setInnerArray($invoiceAddTestArr);
    $invoiceAddXML = $invoiceAddQBXMLObj->asSanitizedXML();
    
    //header('Content-Type:text/xml');
    //$invoiceAddXML = str_replace("&lt;InvoiceAdd&gt;","&lt;InvoiceAdd defMacro=\"MACROTYPE\"&gt;", $invoiceAddXML);
    //echo $invoiceAddQBXMLObj->asSanitizedXML();
    return pushRequestOntoQueue(array($invoiceAddXML), array('simplyClose'), 'ers_demo_brian_d');
?>