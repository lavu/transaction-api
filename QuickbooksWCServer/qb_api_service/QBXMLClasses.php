<?php 

    //ini_set('display_errors', 1);
    class SimpleXMLElementEnhanced implements arrayaccess{
        protected $_SimpleXMLElement;
        protected $_ChildrenArr;
        protected $_isValidXML;
        protected $_lastDrillDownDebug;
        public function __construct($initXML){
            if($initXML instanceof SimpleXMLElement){
                $this->_SimpleXMLElement = $initXML; 
            }else{ 
                @$this->_SimpleXMLElement = simplexml_load_string($initXML);
                $this->_isValidXML = $this->_SimpleXMLElement ? true : false;
            }
            $this->_ChildrenArr = array();
            $this->_lastDrillDownDebug = '';
        }
        public function parsedSuccessfully(){
            return $this->_isValidXML;
        }
        public function asSanitizedXML(){
            return SimpleXMLElementEnhanced::xml_sanitize($this->_SimpleXMLElement->asXML());
        }
        public function asXML(){
            return $this->_SimpleXMLElement->asXML();
        }
        public function getName(){
            return $this->_SimpleXMLElement->getName();
        }
        static function xml_sanitize($string) {
            return strtr( $string, array("<" => "&lt;", ">" => "&gt;",'"' => "&quot;","'" => "&apos;", "&" => "&amp;"));
        }
        static function embedded_arrays_to_xml($SimpleXMLElementRoot, $deep_array){
            //$SimpleXMLElementRoot->addAttribute('a','b');
            if($deep_array == null){ return; }
            foreach($deep_array as $key => $val){
                if( strstr($key, '||') ){
                    $key_parts = explode('||', $key);
                    $key = $key_parts[0];
                }
                $attributesArr = array();
                if( strstr($key, ':attribute_delim:') ){
                    $key_parts = explode(':attribute_delim:',$key);
                    $key = $key_parts[0];
                    $attributesAllStrNonDelim = $key_parts[1];
                    $attributesAllStrArr = explode(';',$attributesAllStrNonDelim);
                    foreach($attributesAllStrArr as $currAttrExpr){
                        $currAttrExprParts = explode("=", $currAttrExpr);
                        $currAttributeKey = trim($currAttrExprParts[0]);
                        $currAttributeVal = trim($currAttrExprParts[1]);
                        if(!empty($currAttributeKey))
                            $attributesArr[$currAttributeKey] = $currAttributeVal;
                    }
                }
                if(is_array($val)){
                    $newElement = $SimpleXMLElementRoot->addChild($key);
                    SimpleXMLElementEnhanced::embedded_arrays_to_xml($newElement, $val);
                    foreach($attributesArr as $attrKey => $attrVal){
                        $newElement->addAttribute($attrKey, $attrVal);
                    }
                }
                else{
                    if($val){
                        $newElement = $SimpleXMLElementRoot->addChild($key,''.$val);
                    }
                    else{
                        $newElement = $SimpleXMLElementRoot->addChild($key);
                    }
                    foreach($attributesArr as $attrKey => $attrVal){
                        $newElement->addAttribute($attrKey, $attrVal);
                    }
                }
            }
        }
        //The array of children nodes (for the 'arrayaccess' interface) is set only on the first time it needs to be filled.
        protected function setChildrenNodes(){
            $SimpleXMLElement = $this->_SimpleXMLElement;
            $returnArr = array( "attributes" => array(), "nodes" => array() );
            $SimpleXMLElementChildren = $SimpleXMLElement->children();
            //Returns a 2D array, one has the children nodes, the other has their attributes, both indexed by the name.
            foreach ($SimpleXMLElementChildren as $val){
                $returnArr['nodes'][$val->getName()] = new SimpleXMLElementEnhanced($val);
                //Build Attribute Array
                $attributes = $val->attributes();
                $attributesArr = array();
                foreach($attributes as $key_ => $val_){
                    $attributesArr[$key_] = ''.$val_;
                }
            }
            $this->_ChildrenArr = $returnArr;
        }
        function getInnerText(){
            $SimpleXMLElement = $this->_SimpleXMLElement;
            if(count($SimpleXMLElement) == 0){
                return ''.$SimpleXMLElement;
            }
        }
        // arrayaccess interface methods
        function offsetExists($offset){
            if(empty($this->_ChildrenArr)){
                $this->setChildrenNodes();
            }
            return $this->_ChildrenArr['nodes'][$offset] ? true : false;
        }
        function offsetGet($offset){
            if(empty($this->_ChildrenArr)){
                $this->setChildrenNodes();
            }
            if(isset($this->_ChildrenArr['nodes'][$offset])){
                $childElement = $this->_ChildrenArr['nodes'][$offset];
                return $childElement;
            }
            else{
                return null;
            }
        }
        function offsetSet( $offset, $value){
            /* NOTHING FOR NOW */
        }
        //abstract public void offsetUnset ( mixed $offset )
        function offsetUnset($offset){
            /* NOTHING FOR NOW */
        }
        function getAttribute($offset){
            return $this->_SimpleXMLElement[$offset];
        }
        function getAttributes(){
            $attributes = $this->_SimpleXMLElement->attributes();
            $returnArr = array();
            foreach($attributes as $key => $val){
                $returnArr[$key] = ''.$val;
            }
            return $returnArr;
        }
        function getChildrenNodeList(){
            if(empty($this->_ChildrenArr)){
                $this->setChildrenNodes();
            }
            return array_keys($this->_ChildrenArr['nodes']);
        }
        function getNodeAtPathOrFalse($drillDownPathArr){
            $debugLine = 'Starting node '.$this->getName().'';
            $currNode = $this;
            foreach($drillDownPathArr as $currChildsName){
                $childrenOfCurrentNode = $currNode->getChildrenNodeList();
                $debugLine .= '->'.$currChildsName;
                if(in_array($currChildsName, $childrenOfCurrentNode)){
                    $currNode = $currNode[$currChildsName];
                    $debugLine .= "(success)";
                    $this->_lastDrillDownDebug = $debugLine;
                }else{
                    $debugLine .= "(failed) current index not in list of children:".
                                        print_r($childrenOfCurrentNode,1);
                    $this->_lastDrillDownDebug = $debugLine;
                    return false;
                }
            }
            $debugLine .= "::::returning node->" . $currNode->getName();
            $this->_lastDrillDownDebug = $debugLine;
            return $currNode;
        }
        function getLastPathDrilldownDebug(){
            return $this->_lastDrillDownDebug;
        }
    }
    
    
    
    //All quick book xml objects are either a request or a response.
    //This is the abstract parent class for building a request to send from
    //the server to the quick books program.
    abstract class SimpleXMLElementEnhancedRequestAbstract extends SimpleXMLElementEnhanced{
        protected $_requestID;
        protected $_innerArray;
        protected $_top_node_name;
        protected $_top_request_node;
        public function __construct($requestID, $QBXML_version, $pNodeName=null){ 
            parent::__construct("<?xml version=\"1.0\" encoding=\"utf-8\"?><?qbxml version=\"$QBXML_version\"?><QBXML></QBXML>"); 
            $_requestID = $requestID;
            $QBXML_Msgs_Rq = $this->_SimpleXMLElement->addChild("QBXMLMsgsRq");
            $QBXML_Msgs_Rq->addAttribute("onError","stopOnError");
            $innerStructureArr = $this->getInnerStructureArray();
            if(!empty($pNodeName) && is_string($pNodeName)){
                $this->_top_node_name = $pNodeName;
            }
            $nodeName = '';
            if(empty($this->_top_node_name)){
                $nodeName = array_keys($innerStructureArr);
                $nodeName = $nodeName[0];
            }
            else{
                $nodeName = $this->_top_node_name;
            }
            
            $request_node = $QBXML_Msgs_Rq->addChild($nodeName."Rq");
            $request_node->addAttribute('requestID',''.$_requestID);
            $this->_top_request_node = $request_node;
            SimpleXMLElementEnhanced::embedded_arrays_to_xml($request_node, $this->_innerArray);
        }
        public function getInnerArray(){ return $this->_innerArray; }
        public function setInnerArray($innerArray){ 
            /* TODO MAKE SURE WORKS THE INNER ARRAY HAS THE IMPERATIVE VARS SET */
            $this->_innerArray = $innerArray;
            SimpleXMLElementEnhanced::embedded_arrays_to_xml($this->_top_request_node, $this->_innerArray);
        }
        abstract protected function getInnerStructureArray();
    }
    
    //For handling the SimpleXMLElementEnhanced data.
    class SimpleXMLElementEnhancedResponse extends SimpleXMLElementEnhanced{
        public function __construct($SDK_Response){ parent::__construct($SDK_Response); }
    }
    
    
    //SimpleXMLElementEnhanced REQUEST OBJECTS  ......................................................................................
    class QBXMLGenericRequest extends SimpleXMLElementEnhancedRequestAbstract{
        protected function getInnerStructureArray(){
            return array();
        }
    }
     
    class AddCustomer extends SimpleXMLElementEnhancedRequestAbstract{
        protected function getInnerStructureArray(){
            return array(
                "CustomerAdd" => array(
                    "Name" => "",
                    "CompanyName" => "",
                    "FirstName" => "",
                    "LastName" => "",
                    "BillAddress" => array(
                        "Addr1" => "",
                        "Addr2" => "",
                        "Addr3" => "",
                        "City" => "",
                        "State" => "",
                        "PostalCode" => "",
                        "Country" => ""
                    ),
                    "Phone" => "",
                    "AltPhone" => "",
                    "Fax" => "",
                    "Email" => "",
                    "Contact" => ""
                )
            );
        }
    }
    
    class AddCustomer_child extends SimpleXMLElementEnhancedRequestAbstract{
        protected function getInnerStructureArray(){
            return array(
                "CustomerAdd" => array(
                    "Name" => "",
                    "ParentRef" => array(
                        "FullName" => ""
                    ),
                    "FirstName" => "",
                    "LastName" => "",
                    "Phone" => "",
                    "Email" => ""
                )
            );
        }
    }
    
    class EstimateAdd extends SimpleXMLElementEnhancedRequestAbstract{
        protected function getInnerStructureArray(){
            return array(
                "EstimateAdd" => array(
                    "CustomerRef" => array(
                        "FullName" => ""
                    ),
                    "TxnDate" => "",
                    "RefNumber" => "",
                    "BillAddress" => array(
                        "Addr1" => "",
                        "City" => "",
                        "State" => "",
                        "PostalCode" => "",
                        "Country" => ""
                    ),
                    "EstimateLineAdd" => array(
                        "ItemRef" => array(
                            "FullName" => ""
                        ),
                        "Desc" => "",
                        "Quantity" => "",
                        "Rate" => ""
                    ),
                    "EstimateLineAdd" => array(
                        "ItemRef" => array(
                            "FullName" => ""
                        ),
                        "Desc" => "",
                        "Quantity" => "",
                        "Rate" => ""
                    )
                )
            );
        }
    }
    
    class InvoiceAdd extends SimpleXMLElementEnhancedRequestAbstract{
        protected function getInnerStructureArray(){
            return array(
                "InvoiceAdd" => array(
                    "CustomerRef" => array(
                        "ListID" => ""
                    ),
                    "TxnDate" => "",
                    "RefNumber" => "",
                    "BillAddress" => array(
                        "Addr1" => "",
                        "City" => "",
                        "State" => "",
                        "PostalCode" => "",
                        "Country" => ""
                    ),
                    "PONumber" => "",
                    "Memo" => "",
                    "InvoiceLineAdd" => array(
                        "ItemRef" => array(
                            "FullName" => ""
                        ),
                        "Desc" => "",
                        "Quantity" => "",
                        "Rate" => ""
                    ),
                    "InvoiceLineAdd" => array(
                        "ItemRef" => array(
                            "FullName" => ""
                        ),
                        "Desc" => "",
                        "Quantity" => "",
                        "Rate" => ""
                    )
                )
            );
        }
    }
    
    
    class InvoiceAdd_with_discount extends SimpleXMLElementEnhancedRequestAbstract{
        protected function getInnerStructureArray(){
            return array(
                "InvoiceAdd" => array(
                    "CustomerRef" => array(
                        "ListID" => ""
                    ),
                    "RefNumber" => "",
                    "BillAddress" => array(
                        "Addr1" => "",
                        "PostalCode" => ""
                    ),
                    "InvoiceLineAdd" => array(
                        "ItemRef" => array(
                            "FullName" => ""
                        ),
                        "Desc" => "",
                        "Quantity" => "",
                        "Rate" => ""
                    ),
                    "InvoiceLineAdd" => array(
                        "ItemRef" => array(
                            "FullName" => ""
                        ),
                        "Desc" => "",
                        "Amount" => ""
                    ),
                    "InvoiceLineAdd" => array(
                        "Desc" => ""
                    )
                )
            );
        }
    }
    
    class InvoiceAdd_complex extends SimpleXMLElementEnhancedRequestAbstract{
        protected function getInnerStructureArray(){
            return array(
                "InvoiceAdd" => array(
                    "CustomerRef" => array(
                        "ListID" => "",
                        "FullName" => ""
                    ),
                    "TxnDate" => "",
                    "RefNumber" => "",
                    "BillAddress" => array(
                        "Addr1" => "",
                        "Addr2" => "",
                        "Addr3" => "",
                        "City" => "",
                        "State" => "",
                        "PostalCode" => "",
                        "Country" => ""
                    ),
                    "ShipAddress" => array(
                        "Addr1" => "",
                        "Addr2" => "",
                        "Addr3" => "",
                        "City" => "",
                        "State" => "",
                        "PostalCode" => "",
                        "Country" => ""
                    ),
                    "TermsRef" => array(
                        "FullName" => ""
                    ),
                    "SalesRepRef" => array(
                        "FullName" => ""
                    ),
                    "Memo" => "",
                    "InvoiceLineAdd" => array(
                        "ItemRef" => array(
                            "FullName" => ""
                        ),
                        "Desc" => "",
                        "Quantity" => "",
                        "Rate" => ""
                    )
                )
            );
        }
    }
    
    class ItemServiceAdd extends SimpleXMLElementEnhancedRequestAbstract{
        protected function getInnerStructureArray(){
            return array(
                "ItemServiceAdd" => array(
                    "Name" => "",
                    "SalesOrPurchase" => array(
                        "Desc" => "",
                        "Price" => "",
                        "AccountRef" => array(
                            "FullName" => ""
                        )
                    )
                )
            );
        }
    }
    
    class ItemNonInventoryAdd extends SimpleXMLElementEnhancedRequestAbstract{
        protected function getInnerStructureArray(){
            return array(
                "ItemNonInventoryAdd" => array(
                    "Name" => "",
                    "SalesOrPurchase" => array(
                        "Desc" => "",
                        "Price" => "",
                        "AccountRef" => array(
                            "FullName" => ""
                        )
                    )
                )
            );
        }
    }
    
    class ItemInventoryAdd extends SimpleXMLElementEnhancedRequestAbstract{
        protected function getInnerStructureArray(){
            return array(
                "ItemInventoryAdd" => array(
                    "Name" => "",
                    "SalesDesc" => "",
                    "SalesPrice" => "",
                    "IncomeAccountRef" => array(
                        "FullName" => ""
                    ),
                    "COGSAccountRef" => array(
                        "FullName" => ""
                    ),
                    "AssetAccountRef" => array(
                        "FullName" => ""
                    )
                )
            );
        }
    }
    
    class ReceivePaymentAdd extends SimpleXMLElementEnhancedRequestAbstract{
        protected function getInnerStructureArray(){
            return array(
                "ReceivePaymentAdd" => array(
                    "CustomerRef" => array(
                        "ListID" => "",
                        "FullName" => ""
                    ),
                    "ARAccountRef" => array(
                        "ListID" => "",
                        "FullName" => ""
                    ),
                    "TxnDate" => "",
                    "RefNumber" => "",
                    "TotalAmount" => "",
                    "PaymentMethodRef" => array(
                        "FullName" => ""
                    ),
                    "Memo" => "",
                    "DepositToAccountRef" => array(
                        "ListID" => "",
                        "FullName" => ""
                    ),
                    "AppliedToTxnAdd" => array(
                        "TxnID" => "",
                        "PaymentAmount" => ""
                    )
                )
            );
        }
    }
    
    class SalesReceiptAdd extends SimpleXMLElementEnhancedRequestAbstract{
        protected function getInnerStructureArray(){
            return array(
                "SalesReceiptAdd" => array(
                    "CustomerRef" => array(
                        "ListID" => ""
                    ),
                    "TxnDate" => "",
                    "RefNumber" => "",
                    "BillAddress" => array(
                        "Addr1" => "",
                        "Addr3" => "",
                        "City" => "",
                        "State" => "",
                        "PostalCode" => "",
                        "Country" => ""
                    ),
                    "ShipAddress" => "",
                    "IsPending" => "",
                    "IsToBePrinted" => "",
                    "IsToBeEmailed" => "",
                    "SalesReceiptLineAdd" => array(
                        "ItemRef" => array(
                            "ListID" => ""
                        ),
                        "Desc" => "",
                        "Quantity" => "",
                        "Rate" => "",
                        "SalesTaxCodeRef" => array(
                            "FullName" => ""
                        )
                    ),
                    "SalesReceiptLineAdd" => array(
                        "ItemRef" => array(
                            "ListID" => ""
                        ),
                        "Desc" => "",
                        "Amount" => "",
                        "SalesTaxCodeRef" => array(
                            "FullName" => ""
                        )
                    )
                )
            );
        }
    }
    
    class ItemReceiptAdd extends SimpleXMLElementEnhancedRequestAbstract{
        protected function getInnerStructureArray(){
            return array(
                "ItemReceiptAdd" => array(
                    "VendorRef" => array(
                        "ListID" => ""
                    ),
                    "TxnDate" => "",
                    "LinkToTxnID" => "",
                    "ItemLineAdd" => array(
                        "ItemRef" => array(
                            "ListID" => ""
                        ),
                        "Desc" => "",
                        "Quantity" => ""
                    )
                )
            );
        }
    }
    
    //Modify commands:
    //For the modify commands, such as modify customer or invoice...
    //These objects will be built as we need them
    
    
    //Query Commands:
    class AccountQuery extends SimpleXMLElementEnhancedRequestAbstract{
        protected function getInnerStructureArray(){
            $this->_top_node_name = "AccountQuery";
            return null;
        }
    }
    class CustomerQuery extends SimpleXMLElementEnhancedRequestAbstract{
        protected function getInnerStructureArray(){
            $this->_top_node_name = "CustomerQuery";
            return null;
        }
    }
    class CustomerQuery_by_name extends SimpleXMLElementEnhancedRequestAbstract{
        protected function getInnerStructureArray(){
            $this->_top_node_name = "CustomerQuery";
            return array("FullName" => "");
        }
    }
    /*
    //NOT HARD, BUT WILL TAKE SOME CUSTOM WORK AND POSSIBLE WORK TO BASE CLASS
    class CustomerQuery_with_iterator extends SimpleXMLElementEnhancedRequestAbstract{
        protected function getInnerStructureArray(){
            
        }
    }
    */
    class ItemQuery extends SimpleXMLElementEnhancedRequestAbstract{
        protected function getInnerStructureArray(){
            $this->_top_node_name = "ItemQuery";
            return array("FullName" => "");//NOTE THIS IS THE ITEM'S NAME.
        }
    }
    /*
    //ALSO NOT HARD, BUT WILL TAKE SAME WORK AS CustomerQuery_with_iterator TO ADD EXTRA
    //  ATTRIBUTES TO THE ROOT REQUEST NODE.
    class ItemInventoryQuery extends SimpleXMLElementEnhancedRequestAbstract{
        protected function getInnerStructureArray(){
            $this->_top_node_name = "ItemInventoryQuery";
            return array("IncludeRetElement" => "");    
        }
    }
    */
    
    /* Needs same work of having extra attributes for iterator, see CustomerQuery_with_iterator
    //Also known as "Querying for Received Payments"
    class PurchaseOrderQuery extends SimpleXMLElementEnhancedRequestAbstract{
        protected function getInnerStructureArray(){
            $this->_top_node_name = "PurchaseOrderQuery";  
        }
    }
    */
    
    class ReceivePaymentQuery extends SimpleXMLElementEnhancedRequestAbstract{
        protected function getInnerStructureArray(){
            $this->_top_node_name = "ReceivePaymentQuery";
            return array("TxnID" => "");
        }
    }
    
    //DELETING
    /*  It would be smart to further look into how delete's work, 
        We may just use a SimpleXMLElementEnhancedGenericRequest.
    */
    
    
    
    
    
    //TxnDelType may have one of the following values: ARRefundCreditCard, Bill, BillPaymentCheck,
    //BillPaymentCreditCard, BuildAssembly, Charge, Check, CreditCardCharge, CreditCardCredit, CreditMemo,
    //Deposit, Estimate, InventoryAdjustment, Invoice, ItemReceipt, JournalEntry,
    //PayrollLiabilityAdjustment [PRIVATE], PayrollPriorPayment [PRIVATE], PayrollYearToDateAdjustment
    //[PRIVATE], PurchaseOrder, ReceivePayment, SalesOrder, SalesReceipt, SalesTaxPaymentCheck,
    //TimeTracking, VehicleMileage, VendorCredit
    class TxnDelRq extends SimpleXMLElementEnhancedRequestAbstract{
        protected function getInnerStructureArray(){
            $this->_top_node_name = "TxnDelRq";
            return array("TxnDelType" => "", "TxnID" => "");
        }
    }
    
    
    //ldtAccount, ldtBillingRate, ldtClass, ldtCurrency, ldtCustomer, ldtCustomerMsg, ldtCustomerType,
    //ldtDateDrivenTerms, ldtEmployee, ldtInventorySite, ldtItemDiscount, ldtItemFixedAsset, ldtItemGroup,
    //ldtItemInventory, ldtItemInventoryAssembly, ldtItemNonInventory, ldtItemOtherCharge, ldtItemPayment,
    //ldtItemSalesTax, ldtItemSalesTaxGroup, ldtItemService, ldtItemSubtotal, ldtJobType, ldtOtherName,
    //ldtPaymentMethod, ldtPayrollItemNonWage, ldtPayrollItemWage, ldtPriceLevel, ldtSalesRep,
    //ldtSalesTaxCode, ldtShipMethod, ldtStandardTerms, ldtToDo, ldtUnitOfMeasureSet, ldtVehicle,
    //ldtVendor, ldtVendorType, ldtWorkersCompCode
    class ListDel extends SimpleXMLElementEnhancedRequestAbstract{
        protected function getInnerStructureArray(){
            $this->_top_node_name = "ListDel";
            return array("ListDelType" => "", "ListDel" => "");
        }
    }
    
    
    
    //Add customer example:
    /*  HAS INNER_ARRAY:
            array(
                "CustomerAdd" => array(
                    "Name" => "",
                    "CompanyName" => "",
                    "FirstName" => "Brian",
                    "LastName" => "Dennedy",
                    "BillAddress" => array(
                        "Addr1" => "",
                        "Addr2" => "",
                        "Addr3" => "",
                        "City" => "",
                        "State" => "",
                        "PostalCode" => "",
                        "Country" => ""
                    ),
                    "Phone" => "",
                    "AltPhone" => "",
                    "Fax" => "",
                    "Email" => "",
                    "Contact" => ""
                )
            );
    */
    
    
    //
    /*
    //header('Content-Type: text/xml');
    $addCustomerExample = new AddCustomer(42, "2.0");
    $addCustomerArr = $addCustomerExample->getInnerArray();
    $addCustomerArr['CustomerAdd']['Name'] = "Brian Dennedy";
    $addCustomerArr['CustomerAdd']['CompanyName'] = "Lavu";
    $addCustomerArr['CustomerAdd']['FirstName'] = "Brian";
    $addCustomerArr['CustomerAdd']['LastName']  = "Dennedy";
    $addCustomerArr['CustomerAdd']['BillAddress']['Addr1'] = "1234 Street st.";
    $addCustomerArr['CustomerAdd']['BillAddress']['City']  = "Albuquerque";
    $addCustomerArr['CustomerAdd']['BillAddress']['State'] = "NM";
    $addCustomerArr['CustomerAdd']['BillAddress']['PostalCode'] = "87109";
    $addCustomerArr['CustomerAdd']['BillAddress']['Country'] = "United States";
    $addCustomerArr['CustomerAdd']['Phone'] = "505-555-5555";
    $addCustomerArr['CustomerAdd']['Email'] = "Brian@poslavu.com";
    $addCustomerExample->setInnerArray($addCustomerArr);
    
    
    
    
    
    //$accountQueryExample = new AccountQuery(42, "2.0");
    //echo $addCustomerExample->asXML();
    echo $addCustomerExample->asSanitizedXML();
    //echo $accountQueryExample->asXML();
    */
?>