<?php
    
    //Superglobal vars that may be used:
    //   $_REQUEST[... server_name, data_name, security_key, security_has, function, response_url, function, func_args
    function validate_parameters_exit_on_fail_invoiceAdd(&$args){
        if(empty($args['customer_ref_full_name']) && empty($args['customer_ref_list_id'])){
            echo '{"status":"failed","reason":"For invoiceAdd must specify customer by either argument \'customer_ref_full_name\' or \'customer_ref_list_id\'"}';
            exit;
        }
        if(empty($args['invoice_lines'])){
            echo '{"status":"failed","reason":"For invoiceAdd must specify at least one invoice line."}';
            exit;
        }
        if(!is_array($args['invoice_lines'])){
            echo '{"status":"failed","reason":"\'invoice_lines\' argument must be an array."}';
            exit;
        }
        foreach($args['invoice_lines'] as $currInvoiceLine){
            if(empty($currInvoiceLine['item_full_name']) && empty($currInvoiceLine['item_list_id'])){
                echo '{"status":"failed","reason":"\'invoice_lines\' items must be specified.  There was no \'item_full_name\' nor \'item_list_id\' specified in current \'invoice_lines\' ."}';
                exit;
            }
        }
    }
    
    function validate_parameters_exit_on_fail_customer_ref(&$args){
        if(empty($args['customer_ref_full_name']) && empty($args['customer_ref_list_id'])){
            echo '{"status":"failed","reason":"Must specify either argument \'customer_ref_full_name\' or \'customer_ref_list_id\'"}';
            exit;
        }
    }
    
    function validate_parameters_exit_on_fail_inventory_item_add(&$args){
        if(empty($args['income_account_ref_full_name']) && empty($args['income_account_ref_list_id'])){
            echo '{"status":"failed","reason":"Must specify either argument \'income_account_ref_full_name\' or \'income_account_ref_list_id\'"}';
            exit;
        }
        if(empty($args['cogs_account_ref_full_name']) && empty($args['cogs_account_ref_list_id'])){
            echo '{"status":"failed","reason":"Must specify either argument \'cogs_account_ref_full_name\' or \'cogs_account_ref_list_id\'"}';
            exit;
        }
        if(empty($args['asset_account_ref_full_name']) && empty($args['asset_account_ref_list_id'])){
            echo '{"status":"failed","reason":"Must specify either argument \'asset_account_ref_full_name\' or \'asset_account_ref_list_id\'"}';
            exit;
        }
    }

?>