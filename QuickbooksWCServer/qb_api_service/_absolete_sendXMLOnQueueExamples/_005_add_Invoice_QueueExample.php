<?php

    //enabling db
    require_once('/home/eventrentalsystems/public_html/cp/functions.php');
    $Conn = ers_connect_db();
        
    //The file we are testing
    require_once(dirname(__FILE__).'/../quickbook_functions.php');

    $invoiceAddTestArr = array(); 
    $invoiceAddTestArr['InvoiceAdd'] = array();
    $invoiceAddTestArr['InvoiceAdd']['CustomerRef'] = array(); 
    $invoiceAddTestArr['InvoiceAdd']['CustomerRef']['FullName'] = "TheosMom";//"80000007-1392334339";
    $invoiceAddTestArr['InvoiceAdd']['TxnDate'] = '2014-01-01'; 
    $invoiceAddTestArr['InvoiceAdd']['RefNumber'] = '1234';
    $invoiceAddTestArr['InvoiceAdd']['BillAddress'] = array(); //We probably don't need this
    $invoiceAddTestArr['InvoiceAdd']['BillAddress']['Addr1'] = '1234 Street st';
    $invoiceAddTestArr['InvoiceAdd']['BillAddress']['City'] = 'Albuquerque';
    $invoiceAddTestArr['InvoiceAdd']['BillAddress']['State'] = 'NM';
    $invoiceAddTestArr['InvoiceAdd']['BillAddress']['PostalCode'] = '87109';
    $invoiceAddTestArr['InvoiceAdd']['BillAddress']['Country'] = 'United States';
    $invoiceAddTestArr['InvoiceAdd']['PONumber'] = '';
    $invoiceAddTestArr['InvoiceAdd']['Memo'] = '';
    
    //  What's below causes an empty response with the message:
    //    QuickBooks found an error when parsing the provided XML text stream.
    //$invoiceAddTestArr['InvoiceAdd']['ItemSalesTaxRef'] = array();
    //$invoiceAddTestArr['InvoiceAdd']['ItemSalesTaxRef']['ListID']='8000000C-1394234458';
    //
    
    $invoiceAddTestArr['InvoiceAdd']['InvoiceLineAdd'] = array(); 
    $invoiceAddTestArr['InvoiceAdd']['InvoiceLineAdd']['ItemRef'] = array(); 
    $invoiceAddTestArr['InvoiceAdd']['InvoiceLineAdd']['ItemRef']['FullName'] = 'Good Time';
    $invoiceAddTestArr['InvoiceAdd']['InvoiceLineAdd']['Desc'] = 'Heres item ones description';
    $invoiceAddTestArr['InvoiceAdd']['InvoiceLineAdd']['Quantity'] = '1';
    $invoiceAddTestArr['InvoiceAdd']['InvoiceLineAdd']['Rate'] = '295';
    /*
    $invoiceAddTestArr['InvoiceAdd']['InvoiceLineAdd||2'] = array(); 
    $invoiceAddTestArr['InvoiceAdd']['InvoiceLineAdd||2']['ItemRef'] = array(); 
    $invoiceAddTestArr['InvoiceAdd']['InvoiceLineAdd||2']['ItemRef']['FullName'] = 'Schlamile';
    $invoiceAddTestArr['InvoiceAdd']['InvoiceLineAdd||2']['Desc'] = 'Heres item twos description';
    $invoiceAddTestArr['InvoiceAdd']['InvoiceLineAdd||2']['Quantity'] = '3';
    $invoiceAddTestArr['InvoiceAdd']['InvoiceLineAdd||2']['Rate'] = '25';
    */
    $invoiceAddObj = new InvoiceAdd('1','2.0');
    
    //header("Content-Type:text/xml");
    $invoiceAddIsValidInnerArrResponse = $invoiceAddObj->setInnerArray($invoiceAddTestArr);
    $sanitizedXML = $invoiceAddObj->asSanitizedXML();
    //$xml = $invoiceAddObj->asXML();
    //echo $xml;
    return pushRequestOntoQueue(array($sanitizedXML), array('handleInvoiceAdd'), 'demo_brian_d');
?>