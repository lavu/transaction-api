<?php
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // T R A N S A C T I O N     H A N D L E R S
    // 
    //
    function simplyClose_TransactionHandler($accountRow, &$queueRow, $ticket, $response, $hresult, $message){
        $queueRow['server_date_time_closed'] = date('Y-m-d H:i:s');
        return;
    }
    function doNothing_TransactionHandler($accountRow, &$queueRow, $ticket, $response, $hresult, $message){
        return;
    }


    //InvoiceAdd
    function handleInvoiceAdd_TransactionHandler($accountRow, &$queueRow, $ticket, $response, $hresult, $message){
        //Handle any errors.
        if(empty($response)){
            handleQBXMLParseError($queueRow, $message);
            return;
        }
        $responseObj = new SimpleXMLElementEnhanced($response);
        if(isErrorResponse($responseObj['QBXMLMsgsRs']['InvoiceAddRs']) ){
            handleResponseErrorType($queueRow);
            return;
        }
        //
        $responseObj = new SimpleXMLElementEnhanced($response);
        if(!$responseObj){
            error_log("ERROR: " . $message);
        }
        $txnIDNode = $responseObj['QBXMLMsgsRs']['InvoiceAddRs']['InvoiceRet']['TxnID'];
        $queueRow['txn_id'] = $txnIDNode->getInnerText();
        $invoiceNumberNode = $responseObj['QBXMLMsgsRs']['InvoiceAddRs']['InvoiceRet']['RefNumber'];
        $queueRow['invoice_number'] = $invoiceNumberNode->getInnerText();
        return;
    }
    function handleInvoiceAddAndClose_TransactionHandler($accountRow, &$queueRow, $ticket, $response, $hresult, $message){
        handleInvoiceAdd_TransactionHandler($accountRow, $queueRow, $ticket, $response, $hresult, $message);
        $queueRow['server_date_time_closed'] = date('Y-m-d H:i:s');
    }
    
    
    
    
    //ReceivePaymentAdd
    function handleReceivePaymentAdd_TransactionHandler($accountRow, &$queueRow, $ticket, $response, $hresult, $message){
        //Handle any errors.
        if(empty($response)){
            handleQBXMLParseError($queueRow, $message);
            return;
        }
        $responseObj = new SimpleXMLElementEnhanced($response);
        if(isErrorResponse($responseObj['QBXMLMsgsRs']['ReceivePaymentAddRs']) ){
            handleResponseErrorType($queueRow);
            return;
        }
        //
        $responseObj = new SimpleXMLElementEnhanced($response);
        $paymentTXNIDXMLNode = $responseObj['QBXMLMsgsRs']['ReceivePaymentAddRs']['ReceivePaymentRet']['TxnID'];
        $paymentTXNID = $paymentTXNIDXMLNode->getInnerText();
        $paymentEditSequenceNode = $responseObj['QBXMLMsgsRs']['ReceivePaymentAddRs']['ReceivePaymentRet']['EditSequence'];
        $paymentEditSequence = $paymentEditSequenceNode->getInnerText();
        //TODO IF NULL: $paymentTXNID  THROW EXCEPTION.
        
        //Get/Create the map of serverID (of payment) -> QBs ID (of payment).
        $paymentIDsServer2QBsMap = LavuJson::json_decode($queueRow['server2QBsPaymentIDMap'],1);
        if(empty($paymentIDsServer2QBsMap)){
            //TODO Throw exception. There should at least be the key of which this will be set to. e.g. 24='current_to_be_set'
        }
        
        //Add new entry to the map.  The key has to be found based on the inline value='current_to_be_set'.
        $paymentIDsServer2QBsMap['_'.$queueRow['action_index']]['qb_txn'] = $paymentTXNID;
        $paymentIDsServer2QBsMap['_'.$queueRow['action_index']]['edit_sequence'] = $paymentEditSequence;
        $jsonStr = LavuJson::json_encode($paymentIDsServer2QBsMap);
        $queueRow['server2QBsPaymentIDMap'] = $jsonStr;
        return;
    }
    function handleReceivePaymentAddAndClose_TransactionHandler($accountRow, &$queueRow, $ticket, $response, $hresult, $message){
        handleReceivePaymentAdd_TransactionHandler($accountRow, $queueRow, $ticket, $response, $hresult, $message);
        $queueRow['server_date_time_closed'] = date('Y-m-d H:i:s');
        return;
    } 
    
    
    
    
    //ReceivePaymentMod
    function handleReceivePaymentMod_TransactionHandler($accountRow, &$queueRow, $ticket, $response, $hresult, $message){
        //Handle any errors.
        if(empty($response)){
            handleQBXMLParseError($queueRow, $message);
            return;
        }
        $responseObj = new SimpleXMLElementEnhanced($response);
        if(isErrorResponse($responseObj['QBXMLMsgsRs']['ReceivePaymentModRs']) ){
            handleResponseErrorType($queueRow);
            return;
        }
        //
        $paymentTXNIDXMLNode = $responseObj['QBXMLMsgsRs']['ReceivePaymentModRs']['ReceivePaymentRet']['TxnID'];
        $paymentTXNID = $paymentTXNIDXMLNode->getInnerText();
        $paymentEditSequenceNode = $responseObj['QBXMLMsgsRs']['ReceivePaymentModRs']['ReceivePaymentRet']['EditSequence'];
        $paymentEditSequence = $paymentEditSequenceNode->getInnerText();
        //TODO IF NULL: $paymentTXNID  THROW EXCEPTION.
        $paymentIDsServer2QBsMap = LavuJson::json_decode($queueRow['server2QBsPaymentIDMap'],1);
        if(empty($paymentIDsServer2QBsMap)){
            //TODO Throw exception. There should at least be the key of which this will be set to. e.g. 24='current_to_be_set'
        }
        $paymentIDsServer2QBsMap['_'.$queueRow['action_index']]['qb_txn'] = $paymentTXNID;
        $paymentIDsServer2QBsMap['_'.$queueRow['action_index']]['edit_sequence'] = $paymentEditSequence;
        $jsonStr = LavuJson::json_encode($paymentIDsServer2QBsMap);
        $queueRow['server2QBsPaymentIDMap'] = $jsonStr;
        return;
    }
    function handleReceivePaymentModAndClose_TransactionHandler($accountRow, &$queueRow, $ticket, $response, $hresult, $message){
        handleReceivePaymentMod_TransactionHandler($accountRow, $queueRow, $ticket, $response, $hresult, $message);
        $queueRow['server_date_time_closed'] = date('Y-m-d H:i:s');
    }
    
    
    
    
    
    
    //////Common functions to response handlers.
    function isErrorResponse($node){
        $either_Error_or_Info_str = $node->getAttribute('statusSeverity');
        if($either_Error_or_Info_str == "Error"){
            return true;
        }
        return false;
    }
    function handleResponseErrorType(&$queueRow){//For when xml is returned, but is an error from quickbooks.
        $currErrorIndices;
        if(trim($queueRow['response_xml_error_action_indices']) == ''){
            $currErrorIndices = array();
        }else{
            $currErrorIndices = explode(',',$queueRow['response_xml_error_action_indices']);
        }
        $currErrorIndices[] = $queueRow['action_index'];
        $queueRow['response_xml_error_action_indices'] = implode(',', $currErrorIndices);
    }
    function handleQBXMLParseError(&$queueRow, $message){//For when xml parsing error occured, no response xml is returned.
       $currErrorIndices;
       if(trim($queueRow['response_parse_error_action_indices']) == ''){
           $currErrorIndices = array();
       }else{
           $currErrorIndices = explode(',',$queueRow['response_parse_error_action_indices']);
       }
       $currErrorIndices[] = $queueRow['action_index'];
       $queueRow['response_parse_error_action_indices'] = implode(',', $currErrorIndices);
    }
    
    
    

?>