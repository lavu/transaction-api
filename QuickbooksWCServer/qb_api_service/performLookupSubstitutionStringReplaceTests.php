<?php
    require_once(dirname(__FILE__).'/QBXMLClasses.php');
    $sampleString = "&lt;?xml version=&quot;1.0&quot; encoding=&quot;utf-8&quot;?&gt;
&lt;?qbxml version=&quot;2.0&quot;?&gt;
&lt;QBXML&gt;&lt;QBXMLMsgsRq onError=&quot;stopOnError&quot;&gt;&lt;InvoiceAddRq requestID=&quot;1&quot;&gt;&lt;InvoiceAdd&gt;&lt;CustomerRef&gt;&lt;ListID&gt;80000007-1392334339&lt;/ListID&gt;&lt;/CustomerRef&gt;&lt;TxnDate&gt;2014-01-01&lt;/TxnDate&gt;&lt;RefNumber&gt;1234&lt;/RefNumber&gt;&lt;BillAddress&gt;&lt;Addr1&gt;1234 Street st&lt;/Addr1&gt;&lt;City&gt;Albuquerque&lt;/City&gt;&lt;State&gt;NM&lt;/State&gt;&lt;PostalCode&gt;87109&lt;/PostalCode&gt;&lt;Country&gt;United States&lt;/Country&gt;&lt;/BillAddress&gt;&lt;PONumber&gt;&lt;/PONumber&gt;&lt;Memo&gt;&lt;/Memo&gt;&lt;InvoiceLineAdd&gt;&lt;ItemRef&gt;&lt;FullName&gt;Good Time&lt;/FullName&gt;&lt;/ItemRef&gt;&lt;Desc&gt;Heres item ones description&lt;/Desc&gt;&lt;Quantity&gt;1&lt;/Quantity&gt;&lt;Rate&gt;295&lt;/Rate&gt;&lt;/InvoiceLineAdd&gt;&lt;InvoiceLineAdd&gt;&lt;ItemRef&gt;&lt;FullName&gt;Other Test&lt;/FullName&gt;&lt;/ItemRef&gt;&lt;Desc&gt;Heres item twos description&lt;/Desc&gt;&lt;Quantity&gt;3&lt;/Quantity&gt;&lt;Rate&gt;25&lt;/Rate&gt;&lt;/InvoiceLineAdd&gt;&lt;/InvoiceAdd&gt;&lt;/InvoiceAddRq&gt;&lt;/QBXMLMsgsRq&gt;&lt;/QBXML&gt;";
    
    
    //TODO----- ADD FULL XML DRILLDOWN TO THIS EXAMPLE
    //Callback example...  notice that this callback will be encoded/sanitized to exist within embedded XML.
    //So we must call SanitizeXML on it.
    //$callbackExample1 = "{[( 0 : ['FullName'] || 'some_constant' )]}";
    //Root node is <QBXML>, so we start there in the drilldown.
    $callbackExample1 = "{[( 0 : ['QBXMLMsgsRs']['ItemQueryRs']['ItemServiceRet']['FullName'] || 'some_constant')]}";   
    $callbackExample1Sanitized = SimpleXMLElementEnhanced::xml_sanitize($callbackExample1);
    
    
    $callbackExample2 = "{[( 1 : ['QBXMLMsgsRs']['ItemQueryRs']['ItemServiceRet']['ListID'] || 'some_constant')]}"; 
    $callbackExample2Sanitized = SimpleXMLElementEnhanced::xml_sanitize($callbackExample2);
    
    echo "CallbackExample1 Sanitized:" . $callbackExample1Sanitized;
    echo "CallbackExample2 Sanitized:" . $callbackExample2Sanitized;
    
/*  // Text for example.
&lt;?xml version=&quot;1.0&quot; encoding=&quot;utf-8&quot;?&gt;
&lt;?qbxml version=&quot;2.0&quot;?&gt;
&lt;QBXML&gt;&lt;QBXMLMsgsRq onError=&quot;stopOnError&quot;&gt;&lt;InvoiceAddRq requestID=&quot;1&quot;&gt;&lt;InvoiceAdd&gt;&lt;CustomerRef&gt;&lt;ListID&gt;{[( 0 : [&apos;QBXMLMsgsRs&apos;][&apos;ItemQueryRs&apos;][&apos;ItemServiceRet&apos;][&apos;FullName&apos;] || &apos;some_constant&apos;)]}&lt;/ListID&gt;&lt;/CustomerRef&gt;&lt;TxnDate&gt;2014-01-01&lt;/TxnDate&gt;&lt;RefNumber&gt;1234&lt;/RefNumber&gt;&lt;BillAddress&gt;&lt;Addr1&gt;1234 Street st&lt;/Addr1&gt;&lt;City&gt;Albuquerque&lt;/City&gt;&lt;State&gt;NM&lt;/State&gt;&lt;PostalCode&gt;87109&lt;/PostalCode&gt;&lt;Country&gt;United States&lt;/Country&gt;&lt;/BillAddress&gt;&lt;PONumber&gt;&lt;/PONumber&gt;&lt;Memo&gt;&lt;/Memo&gt;&lt;InvoiceLineAdd&gt;&lt;ItemRef&gt;&lt;FullName&gt;{[( 1 : [&apos;QBXMLMsgsRs&apos;][&apos;ItemQueryRs&apos;][&apos;ItemServiceRet&apos;][&apos;ListID&apos;] || &apos;some_constant&apos;)]}&lt;/FullName&gt;&lt;/ItemRef&gt;&lt;Desc&gt;Heres item ones description&lt;/Desc&gt;&lt;Quantity&gt;1&lt;/Quantity&gt;&lt;Rate&gt;295&lt;/Rate&gt;&lt;/InvoiceLineAdd&gt;&lt;InvoiceLineAdd&gt;&lt;ItemRef&gt;&lt;FullName&gt;Other Test&lt;/FullName&gt;&lt;/ItemRef&gt;&lt;Desc&gt;Heres item twos description&lt;/Desc&gt;&lt;Quantity&gt;3&lt;/Quantity&gt;&lt;Rate&gt;25&lt;/Rate&gt;&lt;/InvoiceLineAdd&gt;&lt;/InvoiceAdd&gt;&lt;/InvoiceAddRq&gt;&lt;/QBXMLMsgsRq&gt;&lt;/QBXML&gt;

*/
     
    $sampleStringWithCallback = "&lt;?xml version=&quot;1.0&quot; encoding=&quot;utf-8&quot;?&gt;
&lt;?qbxml version=&quot;2.0&quot;?&gt;
&lt;QBXML&gt;&lt;QBXMLMsgsRq onError=&quot;stopOnError&quot;&gt;&lt;InvoiceAddRq requestID=&quot;1&quot;&gt;&lt;InvoiceAdd&gt;&lt;CustomerRef&gt;&lt;ListID&gt;$callbackExample2Sanitized&lt;/ListID&gt;&lt;/CustomerRef&gt;&lt;TxnDate&gt;2014-01-01&lt;/TxnDate&gt;&lt;RefNumber&gt;1234&lt;/RefNumber&gt;&lt;BillAddress&gt;&lt;Addr1&gt;1234 Street st&lt;/Addr1&gt;&lt;City&gt;Albuquerque&lt;/City&gt;&lt;State&gt;NM&lt;/State&gt;&lt;PostalCode&gt;87109&lt;/PostalCode&gt;&lt;Country&gt;United States&lt;/Country&gt;&lt;/BillAddress&gt;&lt;PONumber&gt;&lt;/PONumber&gt;&lt;Memo&gt;&lt;/Memo&gt;&lt;InvoiceLineAdd&gt;&lt;ItemRef&gt;&lt;FullName&gt;$callbackExample1Sanitized&lt;/FullName&gt;&lt;/ItemRef&gt;&lt;Desc&gt;Heres item ones description&lt;/Desc&gt;&lt;Quantity&gt;1&lt;/Quantity&gt;&lt;Rate&gt;295&lt;/Rate&gt;&lt;/InvoiceLineAdd&gt;&lt;InvoiceLineAdd&gt;&lt;ItemRef&gt;&lt;FullName&gt;Other Test&lt;/FullName&gt;&lt;/ItemRef&gt;&lt;Desc&gt;Heres item twos description&lt;/Desc&gt;&lt;Quantity&gt;3&lt;/Quantity&gt;&lt;Rate&gt;25&lt;/Rate&gt;&lt;/InvoiceLineAdd&gt;&lt;/InvoiceAdd&gt;&lt;/InvoiceAddRq&gt;&lt;/QBXMLMsgsRq&gt;&lt;/QBXML&gt;";



        $global_qbxml_responses_array = array();
        $global_qbxml_responses_array[] = '<?xml version="1.0" ?>
<QBXML>
<QBXMLMsgsRs>
<ItemQueryRs requestID="43" statusCode="0" statusSeverity="Info" statusMessage="Status OK">
<ItemServiceRet>
<ListID>80000001-1392340608</ListID>
<TimeCreated>2014-02-13T18:16:48-07:00</TimeCreated>
<TimeModified>2014-02-13T18:16:48-07:00</TimeModified>
<EditSequence>1392340608</EditSequence>
<Name>Good Time</Name>
<FullName>Good Time</FullName>
<IsActive>true</IsActive>
<Sublevel>0</Sublevel>
<SalesOrPurchase>
<Price>0.00</Price>
<AccountRef>
<ListID>80000016-1390258288</ListID>
<FullName>Ask My Accountant</FullName>
</AccountRef>
</SalesOrPurchase>
</ItemServiceRet>
</ItemQueryRs>
</QBXMLMsgsRs>
</QBXML>
';
        $global_qbxml_responses_array[] = '<?xml version="1.0" ?>
<QBXML>
<QBXMLMsgsRs>
<ItemQueryRs requestID="43" statusCode="0" statusSeverity="Info" statusMessage="Status OK">
<ItemServiceRet>
<ListID>Fictional_List_ID_From_Index_1</ListID>
<TimeCreated>2014-02-13T18:16:48-07:00</TimeCreated>
<TimeModified>2014-02-13T18:16:48-07:00</TimeModified>
<EditSequence>1392340608</EditSequence>
<Name>Good Time</Name>
<FullName>Full_Name_From_Index_1</FullName>
<IsActive>true</IsActive>
<Sublevel>0</Sublevel>
<SalesOrPurchase>
<Price>0.00</Price>
<AccountRef>
<ListID>80000016-1390258288</ListID>
<FullName>Ask My Accountant</FullName>
</AccountRef>
</SalesOrPurchase>
</ItemServiceRet>
</ItemQueryRs>
</QBXMLMsgsRs>
</QBXML>
';

    //1.)
    function grabBackReferenceKeys($callbackArr){
        global $global_qbxml_responses_array;
        global $back_reference_token_map;
        $back_reference_token_map[$callbackArr[0]] = '';
    }
    //2.)
    function loadBackReferenceValuesIntoTokenMap($global_qbxml_responses_array, $back_reference_token_map){
        foreach($back_reference_token_map as $key => $val){
            $innerContentsOfCurrBackRefExp = substr($key, 3, -3);
            $innerContentPartsOfBackRefExp = explode("||",$innerContentsOfCurrBackRefExp);
            foreach($innerContentPartsOfBackRefExp as $currBackRefOptionStr){
                $currOptionsDerivedValue = backreferenceExpressionOptionToValue($currBackRefOptionStr, $global_qbxml_responses_array);
                if($currOptionsDerivedValue !== false){
                    $back_reference_token_map[$key] = $currOptionsDerivedValue;
                    break;
                }
            }
        }
        return $back_reference_token_map;
    }
    //2.a)
    function backreferenceExpressionOptionToValue($back_reference_option_str, $global_qbxml_responses_array){
        $currBackRefOptionSplit = explode(':', $back_reference_option_str);
        if(count($currBackRefOptionSplit) == 1){
            if(preg_match('/^((&apos;)|(&quot;)).*((&apos;)|(&quot;))$/', $back_reference_option_str)){
                $currBackOptionStr = substr($currBackOptionStr, 6, -6);
                //$back_reference_token_map[$key] = $currBackOptionStr;
                return $currBackOptionStr;
            }
            return false;
        }
        else{
            $respArrIndex = trim($currBackRefOptionSplit[0]);
            $firstColonIndex  = strpos($back_reference_option_str,':');
            $drillDownPartStr = trim(substr($back_reference_option_str, $firstColonIndex+1));
            $drillDownPartStr = str_replace('&apos;',"'",$drillDownPartStr);
            $drillDownPartStr = str_replace('&quot;',"'",$drillDownPartStr);
            $drillDownPartStr = substr($drillDownPartStr, 2, -2);//Cut off [' and '] from ends.
            $drillDownPartsArr = explode("']['", $drillDownPartStr);
            $final_val = getDrillDownValueFromXML_orFalse($global_qbxml_responses_array[$respArrIndex], $drillDownPartsArr);
            return $final_val;
        }
    }
    //2.a.1)
    function getDrillDownValueFromXML_orFalse($xmlStr, $drillDownSequenceArr){
        $xmlObj = new SimpleXMLElementEnhanced($xmlStr);
        if(!$xmlObj->parsedSuccessfully()){
            //TODO ADD EXCEPTION TO THE QUEUE ROW AND EXIT.
            return false;
        }
        $currDrillDownXMLNode = $xmlObj;
        foreach($drillDownSequenceArr as $currDrillDownKey){      
            $currDrillDownXMLNode = $currDrillDownXMLNode[$currDrillDownKey];
            if(empty($currDrillDownXMLNode)){
                return false;
            }
        }
        $value = $currDrillDownXMLNode->getInnerText();
        return $value;
    }
    //3.)
    function performStringReplaceOnQBXMLRequest($qbXMLRequestStr, $backReferenceTokenMap){
        foreach($backReferenceTokenMap as $key => $val){
            $qbXMLRequestStr = str_replace($key, $val, $qbXMLRequestStr);
        }
        return $qbXMLRequestStr;
    }
    

    //preg_replace_callback('/\{\[\(.*\)\]\}/', 'stringReplaceReferenceCallbacks', $sampleStringWithCallback);
    
    //This will be an array where the key would be a full back reference expression such as:
    //   {[( 0 : ['QBXMLMsgsRs']['ItemQueryRs']['ItemServiceRet']['ListID'] || 'some_constant')]}
    //   And the value will be the back reference value, and therefore replacement of said key.
    //   We begin by just loading the keys into the array.
    //   Notice that the callback function should have all necessary containers as GLOBAL.
    $back_reference_token_map = array();
    preg_replace_callback('/\{\[\(.*?\)\]\}/', 'grabBackReferenceKeys', $sampleStringWithCallback);
    echo "<pre>";
    //Take a look at the token/symbol replacement map.
    echo "Back reference expressions (having loaded just keys): " . print_r($back_reference_token_map,1);
    $back_reference_token_map = loadBackReferenceValuesIntoTokenMap($global_qbxml_responses_array, $back_reference_token_map);//$global_qbxml_responses_array, $back_reference_token_map
    echo "Back reference expressions (after loaded values): " . print_r($back_reference_token_map,1) . "<br><br>";
    echo performStringReplaceOnQBXMLRequest($sampleStringWithCallback, $back_reference_token_map);
    echo "</pre>";
?>