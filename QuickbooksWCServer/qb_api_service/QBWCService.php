<?php
    
    ob_start();
    ini_set('display_errors',1);
	require_once(dirname(__FILE__)."/../common_code/QBXMLClasses.php");
	require_once(dirname(__FILE__)."/../common_code/logging/bd_logging.php");
	require_once(dirname(__FILE__)."/../common_code/nusoap-0.9.5/lib/nusoap.php");
	require_once(dirname(__FILE__)."/QBWCImpl.php");
    
	$server = new soap_server;
    $server->soap_defencoding = 'utf-8';

	$server->register('authenticate');
	$server->register('clientVersion');
	$server->register('closeConnection');
	$server->register('connectionError');
	
	//$server->register('getInteractiveURL');
	$server->register('getLastError');
	//$server->register('getServerVersion');
	//$server->register('interactiveDone');
	//$server->register('interactiveRejected');
	$server->register('receiveResponseXML');
	$server->register('sendRequestXML');

    //To Get the Sent XML soap request... or just print out $HTTP_RAW_POST_DATA, same thing.
    //bd_appendToLog($soapContents = file_get_contents('php://input'), 'learning_soap.log');

	$HTTP_RAW_POST_DATA = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : '';//Checked, is same as file_get_contents('php:/input');
    $server->service($HTTP_RAW_POST_DATA);
    
    
    
    
    
    //<>~-~<>-----------------  I N T E R F A C E    M E T H O D S  -----------------------------------------------------------------------
	//p.62
    function authenticate($strUserName, $strPassword){
        returnFromSoapFunctionAndExit('authenticate', authenticateImpl($strUserName, $strPassword));
    }

    //p.66
    function clientVersion($version){
    	returnFromSoapFunctionAndExit('clientVersion', clientVersionImpl($version));
    }

    //p.68
    function closeConnection($ticket){
    	returnFromSoapFunctionAndExit('closeConnection', closeConnectionImpl($ticket));
    }

    //p.69
    function connectionError($ticket, $hresult, $message){
    	returnFromSoapFunctionAndExit('connectionError', connectionErrorImpl($ticket, $hresult, $message));
    }

    /*//p.71
    function getInteractiveURL(){
    	returnFromSoapFunctionAndExit('getInteractiveURL', "");
    }*/
    
    //p.72
    function getLastError($ticket){
    	returnFromSoapFunctionAndExit('getLastError', getLastErrorImpl($ticket));
    }

    /*//p.74
    function getServerVersion($ticket){
    	bd_appendToLog("getServerVersion called... ticket:" . $version, 'learning_soap.log');
    	returnFromSoapFunctionAndExit('getServerVersion', "");
    }*/

    /*//p.75
    function interactiveDone(){
    	returnFromSoapFunctionAndExit('interactiveDone', "");
    }*/

    /*//p.76
    function interactiveRejected(){
    	returnFromSoapFunctionAndExit('interactiveRejected', "");
    }*/
    
    //p.77
    //Returns -1 if error, else a number 0-100 of the percentage done on the server's part.
    function receiveResponseXML($ticket, $response, $hresult, $message){
    	returnFromSoapFunctionAndExit('receiveResponseXML', receiveResponseXMLImpl($ticket, $response, $hresult, $message));
    	//return receiveResponseXMLImpl($ticket, $response, $hresult, $message);
    }

    //p.80
    function sendRequestXML($ticket, $strHCPResponse, $strCompanyFileName, $qbXMLCountry, $qbXMLMajorVers, $qbXMLMinorVers){
    	returnFromSoapFunctionAndExit('sendRequestXML', sendRequestXMLImpl($ticket,$strHCPResponse,$strCompanyFileName,$qbXMLCountry,$qbXMLMajorVers,$qbXMLMinorVers));
    }
    
    function returnFromSoapFunctionAndExit($functionName, $value){
    
        header('Content-type: text/xml; charset=utf-8');
        $php_errors_ob_etc = trim(ob_get_clean());
        
        //header('KeepAlive: true');
        $soapReturnValue = '';
        if(is_string($value)) {
            $value = array($value);
        }      
        if(is_array($value)) {
           foreach($value as $currString){
               $soapReturnValue .= '<ns1:string>'.$currString.'</ns1:string>';
           } 
        }
        else if(is_int($value) || $functionName == 'receiveResponseXML'){
            $soapReturnValue = ''.$value;
        }

        $soapResponse = '';
        if($functionName == 'receiveResponseXML'){

            $soapResponse = '<?xml version="1.0" encoding="UTF-8"?>
                                <SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns1="http://developer.intuit.com/">
                                	<SOAP-ENV:Body>
                                		<ns1:receiveResponseXMLResponse>
                                			<ns1:receiveResponseXMLResult>'.$value.'</ns1:receiveResponseXMLResult>
                                		</ns1:receiveResponseXMLResponse>
                                	</SOAP-ENV:Body>
                                </SOAP-ENV:Envelope>';

            //bd_appendToLog("Output buffer prior to override:[".$php_errors_ob_etc.']','learning_soap.log');
        }
        else{
            $soapResponse = '<?xml version="1.0" encoding="utf-8"?>'.
                    '<SOAP-ENV:Envelope SOAP-ENV:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" '.
                             'xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" '.
                             'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/">'.
                        '<SOAP-ENV:Body>'.
                            '<ns1:'.$functionName.'Response xmlns:ns1="http://developer.intuit.com/">'.
                                '<ns1:'.$functionName.'Result>'.
                                    $soapReturnValue.
                                '</ns1:'.$functionName.'Result>'.
                            '</ns1:'.$functionName.'Response>'.
                        '</SOAP-ENV:Body>'.
                    '</SOAP-ENV:Envelope>';
        }
        
        //The action index is purposefully variable but we need to know for logging purposes the index at which this all was called.
        global $queueRow, $actionIndexAtStart;
        if(isset($queueRow) && !empty($php_errors_ob_etc)){
            bd_appendToLog('Output buffer on exit was not empty, appending data to php_error_log','learning_soap.log');
            $errorsFormat = str_replace("<br />", "\n", $php_errors_ob_etc);
            $errorsFormat = str_replace("<b>","", $errorsFormat);
            $errorsFormat = str_replace("</b>","", $errorsFormat);
            $errorsFormat = str_replace("\n\n\n\n","\n\n",$errorsFormat);
            $attentionDelimiter = "::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::\n";
            $line = $queueRow['php_error_log']."\n\n\n"."At action_index:".$actionIndexAtStart.
                                    "     Interface Method:$functionName$attentionDelimiter{\n".trim($errorsFormat)."\n}";
            $queueRow['php_error_log'] = trim($line);
            updateRowOnQueue($specified_columns = array('php_error_log'));
        }
        
        echo $soapResponse;
        exit;
    }
?>