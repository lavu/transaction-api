<?php
    
    ini_set('display_errors', 1);
    
    require_once(dirname(__FILE__).'/QBXMLClasses.php');
    
    //Things that need to be tested.
    // 1.) Create SimpleXMLElementEnhanced with crap XML and make sure that it sets an internal exception parameter.
    // 2.) Make sure this internal exception parameter is set and is accessible after creation, so we may bail out if needed.
    // 
    // 3.) Make sure that drill down of valid SimpleXMLElementEnhanced object to node that doesn't exist returns null or false but no other junk/errors output.
    //
    // 4.) Make sure that 
    function testPassingInvalidXMLToConstructor(){
        $garbageSimpleXMLObjectEnhanced = new SimpleXMLElementEnhanced('Total garbage not valid xml');
        echo "Should fail: ";
        echo $garbageSimpleXMLObjectEnhanced->parsedSuccessfully() ? "Parsed Successfully" : "Failed Horribly" . "<br>";
    }
    
    function testGetInnerTextBehavior(){
        $xmlObj = new SimpleXMLElementEnhanced('<myobject><foo><gah>GAH!</gah></foo><bar></bar></myobject>');
        $fooXMLObj = $xmlObj['foo'];
        echo 'Should display no errors nor value:'.$fooXMLObj->getInnerText() . "<br>";
        $gahXMLObj = $fooXMLObj['gah'];
        echo 'Should be GAH!:'.$gahXMLObj->getInnerText();
    }
    
    function testValidXMLInvalidDrilldown(){
        $xmlObj = new SimpleXMLElementEnhanced('<myobject><foo><gah>GAH!</gah></foo><bar></bar></myobject>');
        $fooXMLObj = $xmlObj['foo'];
        $shouldntExist = $fooXMLObj['kio'];
    }
    
    testPassingInvalidXMLToConstructor();
    testGetInnerTextBehavior();
    testValidXMLInvalidDrilldown();
?>