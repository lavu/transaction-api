#! /bin/bash

if [[ $1 = *"d"* ]]; then
    echo "r-syncing source files downward";
    rsync -avz --exclude bd_logs qbook@qbook.eventrentalsystems.com:/home/qbook/public_html/qbv1/quickbooks/qb_api_service/ /Users/BrianAtPoslavu/git_repositories/Lavu/QuickbooksWCServer/qb_api_service/;
    #rsync -avz qbook@qbook.eventrentalsystems.com:/home/qbook/public_html/prod/quickbooks/qbapi_documentation_and_tests/ /Users/BrianAtPoslavu/git_repositories/Lavu/QuickbooksWCServer/qbapi_documentation_and_tests/;
elif [[ $1 = *"u"* ]]; then
    echo "r-syncing documentation and tests upward";
    #rsync -avz  --exclude bd_logs /Users/BrianAtPoslavu/git_repositories/Lavu/QuickbooksWCServer/qb_api_service/ qbook@qbook.eventrentalsystems.com:/home/qbook/public_html/prod/quickbooks/qb_api_service/;
    rsync -avz /Users/BrianAtPoslavu/git_repositories/Lavu/QuickbooksWCServer/qbapi_documentation_and_tests/ qbook@qbook.eventrentalsystems.com:/home/qbook/public_html/qbv1/quickbooks/qbapi_documentation_and_tests/;
    rsync -avz /Users/BrianAtPoslavu/git_repositories/Lavu/QuickbooksWCServer/qbwc_interface_soap_calls/ qbook@qbook.eventrentalsystems.com:/home/qbook/public_html/qbv1/quickbo\
oks/qbwc_interface_soap_calls/
else
    echo "missing only argument: -u for sync-up docs/tests, -d for sync-down source";
fi
