
argsJSON='{
"customer_ref_full_name":"Generic Customer",
"company_name":"compName",
"first_name":"John",
"last_name":"Doe",
"bill_address":{"addr1":"1234 st.","city":"albuquerque","state":"NM","postal_code":"87112","country":"United States"},
"phone":"555-555-5555",
"email":"gfys@gmail.com"
}'

func='customerAdd'
data_name='demo_brian_d'
server_name='ers'
security_key='736104432081791592004872271'
security_token='487272027191592036810104473'

security_hash=$(echo -n  "$server_name$func$data_name$security_token" | openssl sha1)
echo "Security hash:"$security_hash"\n"

argsJSONEnc="$(perl -MURI::Escape -e 'print uri_escape($ARGV[0]);' "$argsJSON")";

data_line="server_name="$server_name"&data_name="$data_name"&security_key="$security_key"&security_hash="$security_hash"&function="$func"&response_url="$respUrl"&func_args="$argsJSONEnc

returnData=$(curl --data "$data_line" "https://qbook.eventrentalsystems.com/prod/quickbooks/qb_api_service/qbapi.php")

echo "RETURN DATA:"$returnData;
