#    -------------InventoryItemAdd
#   
#    TOP LEVEL
#    keys: inventory_item_ref_full_name
#          sales_description
#          sales_price
#          income_account_ref_full_name
#          income_account_ref_list_id
#          cogs_account_ref_full_name
#          cogs_account_ref_list_id
#          asset_account_ref_full_name
#          asset_account_ref_list_id
#

#argsJSON='{
#"inventory_item_ref_full_name":"Hamburger",
#"sales_description":"Bobs Favorite",
#"sales_price":"6.99",
#"income_account_ref_full_name":"sales",
#"cogs_account_ref_full_name":"sales",
#"asset_account_ref_full_name":"sales"
#}'
argsJSON='{
"inventory_item_ref_full_name":"soda",
"sales_description":"Just plane old soda",
"sales_price":"1.99",
"income_account_ref_full_name":"sales",
"cogs_account_ref_full_name":"sales",
"asset_account_ref_full_name":"sales"
}'



func='inventoryItemAdd'
data_name='demo_brian_d'
server_name='ers'
security_key='736104432081791592004872271'
security_token='487272027191592036810104473'

security_hash=$(echo -n "$server_name$func$data_name$security_token" | openssl sha1)
#echo "Security hash:"$security_hash

argsJSONEnc="$(perl -MURI::Escape -e 'print uri_escape($ARGV[0]);' "$argsJSON")";

data_line="server_name="$server_name"&data_name="$data_name"&security_key="$security_key"&security_hash="$security_hash"&function="$func"&response_url="$respUrl"&func_args="$argsJSONEnc

returnData=$(curl --data "$data_line" "https://qbook.eventrentalsystems.com/prod/quickbooks/qb_api_service/qbapi.php")

echo "RETURN DATA:"$returnData;
