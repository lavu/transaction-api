

# 3 main arguments
# 1.) General Customer
#    //    -------------CustomerAdd
#    //   
#    //    TOP LEVEL
#    //    keys: customer_ref_full_name
#    //          company_name
#    //          first_name
#    //          last_name
#    //          bill_address #array
#    //              ->addr1
#    //              ->city
#    //              ->state
#    //              ->postal_code
#    //              ->country
#    //          phone
#    //          email
#
# 2.) Items
#    //    USING FROM WHAT'S BELOW, WRAPPING IN AN ARRAY: 'inventory_items'
#    //    -------------InventoryItemAdd
#    //    //THIS IS THE ONE ITEM ADD..... WE NEED TO CUSTOMIZE FOR MANY ITEMS.
#    //    TOP LEVEL
#    //    keys: inventory_item_ref_full_name
#    //          sales_description
#    //          sales_price
#    //          income_account_ref_full_name
#    //          income_account_ref_list_id
#    //          cogs_account_ref_full_name
#    //          cogs_account_ref_list_id
#    //          asset_account_ref_full_name
#    //          asset_account_ref_list_id
#    //
#
# 3.) Invoice
#    //    -------------InvoiceAdd (complex) ----- This is what we want to call for syncing down orders.
#    //   
#    //    TOP LEVEL
#    //    keys: customer_ref_full_name (or)
#    //          customer_ref_list_id
#    //          inventory_items #array  (to make sure exist first, by full_name)
#    //          reference_number (if fits we use order-id, else we base64 encode)
#    //          date
#    //          item_sales_tax_ref_full_name (or)
#    //          item_sales_tax_ref_list_id
#    //          invoice_lines #array  
#    //              ->item_full_name  (or)
#    //              ->item_list_id
#    //              ->description
#    //              ->quantity
#    //              ->rate
#

argsJSON='{
"customer_ref_full_name":"General Customer",
"company_name":"",
"first_name":"",
"last_name":"",
"bill_address":{"addr1":"","city":"","state":"","postal_code":"","country":""},
"phone":"",
"email":"",
"inventory_items":[ {"inventory_item_ref_full_name":"hot_dog","sales_description":"entry level hot dog inv. desc","sales_price":"2.99","income_account_ref_full_name":"myIncomeAccount","cogs_account_ref_full_name":"myCostOfGoodsAccount","asset_account_ref_full_name":"myCurrentAssetsAccount"},
                    {"inventory_item_ref_full_name":"hamburger","sales_description":"Angus Beef inv. desc","sales_price":"6.99","income_account_ref_full_name":"myIncomeAccount","cogs_account_ref_full_name":"myCostOfGoodsAccount","asset_account_ref_full_name":"myCurrentAssetsAccount"},
                    {"inventory_item_ref_full_name":"soda","sales_description":"Medium Soda","sales_price":"1.99","income_account_ref_full_name":"myIncomeAccount","cogs_account_ref_full_name":"myCostOfGoodsAccount","asset_account_ref_full_name":"myCurrentAssetsAccount"}  ],
"reference_number":"123456",
"date":"2014-05-27",
"item_sales_tax_ref_full_name":"State_Tax",
"invoice_lines":[{"item_full_name":"hot_dog","description":"HD-Description for Invoice Line","quantity":"2","rate":"2.99"},
                 {"item_full_name":"hamburger","description":"Ham-Description for Invoice Line","quantity":"1","rate":"6.99"},
                 {"item_full_name":"soda","description":"Soda-Description for Invoice Line","quantity":"3","rate":"1.99"}]
}';



func='ensureItems_createInvoice'
data_name='demo_brian_d'
server_name='ers'
security_key='736104432081791592004872271'
security_token='487272027191592036810104473'

security_hash=$(echo -n "$server_name$func$data_name$security_token" | openssl sha1)
echo "Security hash:"$security_hash"\n"

argsJSONEnc="$(perl -MURI::Escape -e 'print uri_escape($ARGV[0]);' "$argsJSON")";

data_line="server_name="$server_name"&data_name="$data_name"&security_key="$security_key"&security_hash="$security_hash"&function="$func"&response_url="$respUrl"&func_args="$argsJSONEnc

returnData=$(curl --data "$data_line" "https://qbook.eventrentalsystems.com/prod/quickbooks/qb_api_service/qbapi.php")

echo -e "RETURN DATA:$returnData";
