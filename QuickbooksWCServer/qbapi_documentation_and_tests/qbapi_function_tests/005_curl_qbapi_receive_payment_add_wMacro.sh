# keys: customer_ref_full_name
#       customer_ref_list_id
#       date
#       reference_number
#       item_sales_tax_ref_full_name
#       item_sales_tax_ref_list_id
#       invoice_lines #array
#           ->item_full_name
#           ->item_list_id
#           ->description
#           ->quantity
#           ->rate
argsJSON='{
"customer_ref_full_name":"General Customer",
"ar_account_ref_full_name":"Accounts Receivable",
"total_amount":"10.00",
"ref_number":"11",
"payment_method_ref_full_name":"Cash",
"txn_macro":"TxnID:81-98374-3412-2"
}'

func='receivePaymentAdd'
data_name='demo_brian_d'
server_name='lavu'
security_key='915204710443208127187736920' #validates which server it is.
security_token='271602091522081443877390471'

security_hash=$(echo -n  "$server_name$func$data_name$security_token" | openssl sha1)
#echo "Security hash:"$security_hash

argsJSONEnc="$(perl -MURI::Escape -e 'print uri_escape($ARGV[0]);' "$argsJSON")";

data_line="server_name="$server_name"&data_name="$data_name"&security_key="$security_key"&security_hash="$security_hash"&function="$func"&response_url="$respUrl"&func_args="$argsJSONEnc

returnData=$(curl --data "$data_line" "https://qbook.eventrentalsystems.com/prod/quickbooks/qb_api_service/qbapi.php");

echo "RETURN DATA:"$returnData;
