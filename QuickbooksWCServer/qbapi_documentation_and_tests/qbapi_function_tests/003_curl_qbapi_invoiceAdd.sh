# keys: customer_ref_full_name
#       customer_ref_list_id
#       date
#       reference_number
#       item_sales_tax_ref_full_name
#       item_sales_tax_ref_list_id
#       invoice_lines #array
#           ->item_full_name
#           ->item_list_id
#           ->description
#           ->quantity
#           ->rate
argsJSON='{
"customer_ref_full_name":"General Customer",
"date":"2014-03-13",
"reference_number":"1234",
"item_sales_tax_ref_full_name":"SalesTax",
"invoice_lines":[{"item_full_name":"Crostini Tricolore","description":"Hot and Spicy","quantity":"42","rate":"215"}],
"txn_macro":"81-98374-3412-2"
}'

func='invoiceAdd'
data_name='demo_brian_d'
server_name='ers'
security_key='736104432081791592004872271' #validates which server it is.
security_token='487272027191592036810104473'

security_hash=$(echo -n  "$server_name$func$data_name$security_token" | openssl sha1)
#echo "Security hash:"$security_hash

argsJSONEnc="$(perl -MURI::Escape -e 'print uri_escape($ARGV[0]);' "$argsJSON")";

data_line="server_name="$server_name"&data_name="$data_name"&security_key="$security_key"&security_hash="$security_hash"&function="$func"&response_url="$respUrl"&func_args="$argsJSONEnc

returnData=$(curl --data "$data_line" "https://qbook.eventrentalsystems.com/prod/quickbooks/qb_api_service/qbapi.php");

echo "RETURN DATA:"$returnData;
