#! /bin/bash
#
# imgExplosion.sh foo.jpg
# Takes an image file as it's first arguement and then compresses it if it is large
#

#
# If the File is empty then skip
#
filesize=$(du $1 | cut -f1)
if [ $(echo " $filesize == 0" | bc) -eq 1 ]
then
	exit
fi

#
# Compress files if they are large. Else print "."
#
imagesize=$(identify -format "%w\n" $1)
#echo "$1 $imagesize"
if [ "$imagesize" -gt "512" ]
then
	mogrify -monochrome -resize 512x100 $1 &
	echo -e "\n $1 Compressed"
	usleep 10000
else
	echo -n "."
fi

#
# Sleep based on load average. Wait for it to get below MaxLoad
#
MaxLoad=3.2
CurrLoad=$(cat /proc/loadavg | cut -f 1 -d " ")
while [ $(echo " $CurrLoad > $MaxLoad" | bc) -eq 1 ]
do
	echo -ne "\nLoad To High Sleeping "
	echo $CurrLoad
	sleep 1
	CurrLoad=$(cat /proc/loadavg | cut -f 1 -d " ")
done
