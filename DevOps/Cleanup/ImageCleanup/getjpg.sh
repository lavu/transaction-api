#! /bin/bash
#
# getjpg.sh /foo/
# Calls imgExplosion on every .jpg in a directory
#
find $1 -type f -iname "*.jpg" -exec bash /home/poslavu/maintinenceTools/imgExplosion.sh {} \;
