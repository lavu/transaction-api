#! /bin/bash

UT=`cat /proc/uptime  | cut -f 1 -d ' '`
LOADONE=`cat /proc/loadavg | cut -f 1 -d ' '`
LOADFIVE=`cat /proc/loadavg | cut -f 2 -d ' '`

echo -e "`hostname`\tUPTIME\t0\tUptime: `uptime` | uptime=$UT\n"                               | send_nsca -H gman.lavu.com -p 42895 -c /etc/nagios/send_nsca.cfg
echo -e "`hostname`\tLOADAVG1\t0\tOne Minute Load Average = $LOADONE | loadone=$LOADONE\n"     | send_nsca -H gman.lavu.com -p 42895 -c /etc/nagios/send_nsca.cfg
echo -e "`hostname`\tLOADAVG5\t0\tFive Minute Load Average = $LOADFIVE | loadfive=$LOADFIVE\n" | send_nsca -H gman.lavu.com -p 42895 -c /etc/nagios/send_nsca.cfg
