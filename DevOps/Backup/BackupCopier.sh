#!/bin/bash
REMOTESERVER=lavu@gman.lavu.com:
REMOTELOGPATH=./logdumps/$HOSTNAME
LOCALLOGS=/home/poslavu/private_html/tools/backup_data
rsync -avzWP -e ssh $LOCALLOGS $REMOTESERVER$REMOTELOGPATH
if [ 0 -eq $? ];
then
        logger "ERROR: Backup of logs to $REMOTESERVER succeeded"
else
        logger "ERROR: Backup of logs to $REMOTESERVER failed"
fi
