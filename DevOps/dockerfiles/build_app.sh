#!/bin/bash

set -x

rm /etc/httpd/conf.d/welcome.conf

# set timezone
TZ=CST6CDT
ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

# set up webroot
su poslavu -c 'install -d /home/poslavu/public_html'
su poslavu -c 'install -d /home/poslavu/private_html'

install -o poslavu -g apache -m 0775 -d /home/poslavu/logs
install -o poslavu -g apache -m 0775 -d /home/poslavu/logs/api
install -o poslavu -g apache -m 0775 -d /home/poslavu/logs/company
install -o poslavu -g poslavu -m 0700 -d /home/poslavu/.ssh

touch /home/poslavu/logs/dberrors.txt
touch /home/poslavu/logs/oauthdebug.txt

chown apache.apache /home/poslavu/logs/dberrors.txt
chown apache.apache /home/poslavu/logs/oauthdebug.txt

chmod -R 0775 /home/poslavu/logs

# Mount images and shared files
# Old mount for rackspace
# echo '//172.24.64.3/nfs				/mnt/poslavu-files	cifs	guest,uid=48,gid=48,iocharset=utf8	0 0' >> /etc/fstab
# New mount for AWS EFS
# echo "${IMAGES_MOUNT_PATH}:/ /mnt/poslavu-files  nfs4  nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2,noresvport" >> /etc/fstab

install -d /mnt/poslavu-files

if [ -d /home/poslavu/public_html/admin/distro/beta/Resources ]; then
	rm -rf /home/poslavu/public_html/admin/distro/beta/Resources
fi

if [ -d /home/poslavu/public_html/admin/distro/beta/Resources_new ]; then
	rm -rf /home/poslavu/public_html/admin/distro/beta/Resources_new
fi

if [ -d /home/poslavu/public_html/admin/distro/beta/distro_news ]; then
	rm -rf /home/poslavu/public_html/admin/distro/beta/distro_news
fi

if [ ! -L /home/poslavu/public_html/admin/distro/beta/Resources ]; then
	su poslavu -c 'ln -s /mnt/poslavu-files/Resources /home/poslavu/public_html/admin/distro/beta/Resources'
fi

if [ ! -L /home/poslavu/public_html/admin/distro/beta/Resources_new ]; then
	su poslavu -c 'ln -s /mnt/poslavu-files/Resources /home/poslavu/public_html/admin/distro/beta/Resources_new'
fi

if [ ! -L /home/poslavu/public_html/admin/distro/beta/distro_news ]; then
	su poslavu -c 'ln -s /mnt/poslavu-files/distro_news /home/poslavu/public_html/admin/distro/beta/distro_news'
fi

if [ ! -L /home/poslavu/public_html/admin/images ]; then
	su poslavu -c 'ln -s /mnt/poslavu-files/images /home/poslavu/public_html/admin/images'
fi

if [ ! -L /home/poslavu/public_html/admin/emails ]; then
	su poslavu -c 'ln -s /mnt/poslavu-files/emails /home/poslavu/public_html/admin/emails'
fi

##su poslavu -c 'cp /var/tmp/myinfo.php /home/poslavu/private_html'
##su poslavu -c 'cp /var/tmp/ZuoraConfig.php /home/poslavu/private_html'
##su poslavu -c 'cp /var/tmp/salesforceCredentials.php /home/poslavu/private_html'
#su poslavu -c 'echo "${APP_VERSION}" > /home/poslavu/public_html/admin/release.txt'
su poslavu -c 'mkdir /home/poslavu/temp'

##rm /var/tmp/ZuoraConfig.php
##rm /var/tmp/salesforceCredentials.php

# Install composer
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php composer-setup.php
php -r "unlink('composer-setup.php');"

chown poslavu:poslavu composer.phar
chmod +x composer.phar
mv composer.phar /home/poslavu/
cp /home/poslavu/composer.phar /usr/bin/composer

chown poslavu:poslavu /home/poslavu/composer.phar
su poslavu -c '/home/poslavu/composer.phar install'

if [ -e /home/poslavu/public_html/admin/olo_api ]; then
	su poslavu -c "cd /home/poslavu/public_html/admin/olo_api; /usr/bin/composer require firebase/php-jwt"
else
	su poslavu -c "mkdir /home/poslavu/public_html/admin/olo_api;cd /home/poslavu/public_html/admin/olo_api; /usr/bin/composer require firebase/php-jwt"
fi

su poslavu -c "cd /home/poslavu/public_html/admin/; /usr/bin/composer install"

cp /var/tmp/crontab /var/spool/cron/poslavu
echo "" >> /var/spool/cron/poslavu

su poslavu -c 'install -d ~/.ssh'

chmod +x /var/tmp/LogCopier.sh
su poslavu -c 'cp /var/tmp/LogCopier.sh ~'

rm -f /var/tmp/crontab
rm -f /var/tmp/LogCopier.sh

# create these after the tarball is extracted
rm -rf /home/poslavu/public_html/admin/sa_cp/dbbackup
install -o poslavu -g apache -m 0775 -d /home/poslavu/public_html/register/views/lavu88

su poslavu -c 'ln -s ../public_html/3rd_party/jpgraph /home/poslavu/private_html/jpgraph'
su poslavu -c 'ln -s /mnt/poslavu-files/sa-cp-dbbackup /home/poslavu/public_html/admin/sa_cp/dbbackup'

##rm /var/tmp/poslavu-*
##rm /var/tmp/myinfo.php
##rm /var/tmp/inventory-*
##rm /var/tmp/abi-dashboard-*

mkdir -p /usr/local/bin/
cp /var/tmp/wkhtmltopdf /usr/local/bin/wkhtmltopdf

chmod 755 /usr/local/bin/wkhtmltopdf
rm -f /var/tmp/wkhtmltopdf

cat > /etc/hosts << EOF
127.0.0.1 ${INGRESS_HOSTNAMES} prod-web.poslavu.com kartscore.poslavu.com ac2.lavusys.com lq.poslavu.com register.lavu.com ac4.lavu.com te4.lavusys.com ac3.lavu.com api.poslavu.com localhost register.poslavu.com www.admin.lavu.com localhost4.localdomain4 cg2.lavusys.com te2.lavusys.com cg4.lavusys.com qb.poslavu.com my.poslavu.com admin.poslavu.com ac1.lavusys.com api-crunchit.lavu.com autocloud.poslavu.com prod-web te3.lavusys.com dev.lavu.com cg1.lavusys.com localhost.localdomain te1.lavusys.com api-crunchitreports.lavu.com cg3.lavusys.com admin.lavu.com localhost4
EOF

# Mail config setup
postmap /etc/postfix/sasl_passwd
postmap /etc/postfix/virtual

usermod -G wheel poslavu

chmod +x /home/poslavu/private_html/scripts/tipAdjust.sh

# chmod +x /var/tmp/install-monitoring.sh
# /var/tmp/install-monitoring.sh
# rm /var/tmp/install-monitoring.sh
