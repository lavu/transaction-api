#!/bin/bash
set -e

# Set newrelic config
echo "Enabling APM metrics for ${NR_APP_NAME}"
adduser --system newrelic

cat > /etc/php.d/newrelic.ini << EOF
extension = "newrelic.so"
[newrelic]
newrelic.daemon.port = "@newrelic-daemon"
newrelic.license = "${NR_INSTALL_KEY}"
newrelic.logfile = "/var/log/newrelic/php_agent.log"
newrelic.appname = "${NR_APP_NAME}"
newrelic.daemon.logfile = "/var/log/newrelic/newrelic-daemon.log"
newrelic.transaction_tracer.record_sql = "raw"
newrelic.enabled = "${NR_STATUS}"
EOF

cat > /etc/newrelic/newrelic.cfg << EOF
port="@newrelic-daemon"
EOF

newrelic-install install


sed -i "s,session.save_path = \"memc-silver-penguin-backend.default.svc.cluster.local:11211\",session.save_path = \"$(printenv MEMCACHED_HOST):11211\"," /etc/php.ini
sed -i "s,error_reporting = E_ALL \& ~E_DEPRECATED \& ~E_STRICT \& ~E_NOTICE \& ~E_WARNING,error_reporting = $(printf %q "$(printenv PHP_ERROR_REPORTING)")," /etc/php.ini

# Extract the webapp and configure it to make it work
chmod +x /root/build_app.sh
/root/build_app.sh

/usr/sbin/postfix start

exec /sbin/httpd -D FOREGROUND
