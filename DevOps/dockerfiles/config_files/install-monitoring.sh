#!/bin/sh

# yum -y install epel-release
yum -y install http://www.percona.com/downloads/percona-release/redhat/0.1-3/percona-release-0.1-3.noarch.rpm
yum -y install nsca-client nagios-plugins-http nagios-plugins-load nagios-plugins-disk nagios-plugins-procs nagios-plugins-procs percona-nagios-plugins cronie

#systemctl start crond
#systemctl enable crond

echo "password=mv8CHyKzYWvvW0f3Z4fk183JG" > /etc/nagios/send_nsca.cfg
echo "encryption_method=14" >> /etc/nagios/send_nsca.cfg

echo "HOSTNAME=\`hostname\`" >> /root/NagiosStatus.sh
echo "HTTP=\`/usr/lib64/nagios/plugins/check_http -H localhost -u http://localhost/cp/index.php\`" >> /root/NagiosStatus.sh
echo "HTTPSTATUS=\$?" >> /root/NagiosStatus.sh
echo "MEMORY=\`/usr/lib64/nagios/plugins/pmp-check-unix-memory\`" >> /root/NagiosStatus.sh
echo "MEMORYSTATUS=\$?" >> /root/NagiosStatus.sh
echo "LOAD=\`/usr/lib64/nagios/plugins/check_load  -w 19,17,15 -c 22,19,17\`" >> /root/NagiosStatus.sh
echo "LOADSTATUS=\$?" >> /root/NagiosStatus.sh
echo "ROOTFREE=\`/usr/lib64/nagios/plugins/check_disk -w 10% -c 5% -p /\`" >> /root/NagiosStatus.sh
echo "ROOTFREESTATUS=\$?" >> /root/NagiosStatus.sh
echo "CHECKTIPS=\`/usr/lib64/nagios/plugins/check_procs -c 1: -C tipAdjust.sh\`" >> /root/NagiosStatus.sh
echo "CHECKTIPSSTATUS=\$?" >> /root/NagiosStatus.sh

echo "echo -e \"\$HOSTNAME\\tCHECK_HTTP\\t\$HTTPSTATUS\\t\$HTTP\\n\"                | /usr/sbin/send_nsca -H 216.243.114.158 -p 42895 -c /etc/nagios/send_nsca.cfg" >> /root/NagiosStatus.sh
echo "echo -e \"\$HOSTNAME\\tpmp-check-unix-memory\\t\$MEMORYSTATUS\\t\$MEMORY\\n\" | /usr/sbin/send_nsca -H 216.243.114.158 -p 42895 -c /etc/nagios/send_nsca.cfg" >> /root/NagiosStatus.sh
echo "echo -e \"\$HOSTNAME\\tROOTFREE\\t\$ROOTFREESTATUS\\t\$ROOTFREE\\n\"          | /usr/sbin/send_nsca -H 216.243.114.158 -p 42895 -c /etc/nagios/send_nsca.cfg" >> /root/NagiosStatus.sh
echo "echo -e \"\$HOSTNAME\\tcheck_load\\t\$LOADSTATUS\\t\$LOAD\\n\"                | /usr/sbin/send_nsca -H 216.243.114.158 -p 42895 -c /etc/nagios/send_nsca.cfg" >> /root/NagiosStatus.sh
echo "echo -e \"\$HOSTNAME\\tcheck_procs\\t\$CHECKTIPSSTATUS\\t\$CHECKTIPS\\n\"     | /usr/sbin/send_nsca -H 216.243.114.158 -p 42895 -c /etc/nagios/send_nsca.cfg" >> /root/NagiosStatus.sh
echo "" >> /root/NagiosStatus.sh

echo "echo -e \"\$HOSTNAME\\tCHECK_HTTP\\t\$HTTPSTATUS\\t\$HTTP\\n\"                | /usr/sbin/send_nsca -H 67.134.3.50 -p 42895 -c /etc/nagios/send_nsca.cfg" >> /root/NagiosStatus.sh
echo "echo -e \"\$HOSTNAME\\tpmp-check-unix-memory\\t\$MEMORYSTATUS\\t\$MEMORY\\n\" | /usr/sbin/send_nsca -H 67.134.3.50 -p 42895 -c /etc/nagios/send_nsca.cfg" >> /root/NagiosStatus.sh
echo "echo -e \"\$HOSTNAME\\tROOTFREE\\t\$ROOTFREESTATUS\\t\$ROOTFREE\\n\"          | /usr/sbin/send_nsca -H 67.134.3.50 -p 42895 -c /etc/nagios/send_nsca.cfg" >> /root/NagiosStatus.sh
echo "echo -e \"\$HOSTNAME\\tcheck_load\\t\$LOADSTATUS\\t\$LOAD\\n\"                | /usr/sbin/send_nsca -H 67.134.3.50 -p 42895 -c /etc/nagios/send_nsca.cfg" >> /root/NagiosStatus.sh
echo "echo -e \"\$HOSTNAME\\tcheck_procs\\t\$CHECKTIPSSTATUS\\t\$CHECKTIPS\\n\"     | /usr/sbin/send_nsca -H 67.134.3.50 -p 42895 -c /etc/nagios/send_nsca.cfg" >> /root/NagiosStatus.sh
chmod 700 /root/NagiosStatus.sh

echo "MAILTO=\"\"" >> /var/spool/cron/root
echo "" >> /var/spool/cron/root
echo "* * * * * /root/NagiosStatus.sh" >> /var/spool/cron/root
echo "* * * * * sh /root/monitor_tA.sh" >> /var/spool/cron/root
