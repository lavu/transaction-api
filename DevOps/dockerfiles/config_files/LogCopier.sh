#!/bin/bash
REMOTESERVER=lavu@gman.lavu.com:
REMOTELOGPATH=./logdumps2/$HOSTNAME
LOCALLOGS=/home/poslavu/logs
#rsync -avzW -e ssh $LOCALLOGS $REMOTESERVER$REMOTELOGPATH
rsync -lrtzPW --size-only -e ssh $LOCALLOGS $REMOTESERVER$REMOTELOGPATH
if [ 0 -eq $? ];
then
        logger "ERROR: Backup of logs to $REMOTESERVER succeeded"
else
        logger "ERROR: Backup of logs to $REMOTESERVER failed"
fi
