//
//  ExternalDeviceHub.m
//  POSLavu
//
//  Created by Richard Gieske on 3/28/13.
//
//

#import "ExternalDeviceHub.h"
#import "AppDelegate.h"
#import "ViewController.h"
#import "ExternalDeviceDelegate.h"
#import "magTekDelegate.h"

@implementation ExternalDeviceHub

@synthesize mtSCRALib;
@synthesize deviceCheckTimer;
@synthesize lastSwipeReaderName, lastSwipeString, lastSwipeDataString, lastSwipeIsEncrypted;
@synthesize lastScanScannerName, lastScanString;
@synthesize componentListeningForCardSwipe, componentListeningForScan;

- (id)init {
    
    self = [super init];
    if (self) {
        
        appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        smai = appDelegate;
        
        [smai rLog:@"init ExternalDeviceHub"];
        
        MTSCRA *mLib = [[MTSCRA alloc] init];
        self.mtSCRALib = mLib;
        [mLib release];
        
        connectedDevices = [[NSMutableDictionary alloc] init];
        
        self.lastSwipeReaderName = @"";
        self.lastSwipeString = @"";
        self.lastSwipeDataString = @"";
        
        self.lastScanScannerName = @"";
        self.lastScanString = @"";
        //[[NSNotificationCenter defaultCenter] addObserver: self selector:@selector(deviceConnected:) name: EAAccessoryDidConnectNotification object: NULL];
        //[[NSNotificationCenter defaultCenter] addObserver: self selector:@selector(deviceDisconnected:) name: EAAccessoryDidConnectNotification object: NULL];
        //[[EAAccessoryManager sharedAccessoryManager] registerForLocalNotifications];
        [self startDeviceCheckTimer];
    }
    
    return self;
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    
    if (doubleMagTekWarningIsShowing) doubleMagTekWarningIsShowing = NO;
}

- (void)startDeviceCheckTimer {
    
    //self.deviceCheckTimer = [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(checkExternalDevices) userInfo:nil repeats:YES];
}

- (void)stopDeviceCheckTimer {
    
    //if (self.deviceCheckTimer && [self.deviceCheckTimer isKindOfClass:[NSTimer class]]) [self.deviceCheckTimer invalidate];
}

- (void)appDidBecomeActive {
    
    [smai rLog:@"appDidBecomeActive (edh)"];
    
    self.lastSwipeReaderName = @"";
    self.lastSwipeString = @"";
    self.lastSwipeDataString = @"";
    
    [self initializeUniMag];
    [[EAAccessoryManager sharedAccessoryManager] registerForLocalNotifications];
    [self startDeviceCheckTimer];
}

- (void)appWillResignActive {
    
    [smai rLog:@"appWillResignActive (edh)"];
    
    [[EAAccessoryManager sharedAccessoryManager] unregisterForLocalNotifications];
    [self stopDeviceCheckTimer];
    [self stopReaderActivityIndicator];
    [self changeTabSearchButtonState:@"0"];
    [self cardIcon:@"down"];
    [self deinitializeUniMag];
    [self deinitializeAll];
}

- (void)deinitializeAll {
    
    NSArray *deviceNames = [connectedDevices allKeys];
    for (NSString *deviceName in deviceNames) {
        //id obj = [connectedDevices objectForKey:deviceName];
        //[obj release];
        [connectedDevices removeObjectForKey:deviceName];
    }
}

- (void)checkExternalDevices {
    
    [self performSelectorInBackground:@selector(checkExternalDevicesInBackground) withObject:nil];
}

- (void)checkExternalDevicesInBackground {
    
    NSMutableDictionary *foundDevices = [[NSMutableDictionary alloc] init];

	for (EAAccessory *obj in [[EAAccessoryManager sharedAccessoryManager] connectedAccessories]) {
		
        //[smai rLog:[NSString stringWithFormat:@"protocolStrings: %@", [obj protocolStrings]]];
        //[appDelegate performSelectorOnMainThread:@selector(dcLog:) withObject:[NSString stringWithFormat:@"protocolStrings: %@", [obj protocolStrings]] waitUntilDone:NO];
        
        NSString *protocolString = @"";
        NSString *deviceName = @"";
        
		if ([[obj protocolStrings] containsObject:@"com.bluebamboo.p25i"]) {
            protocolString = @"com.bluebamboo.p25i";
			deviceName = @"bluebamboo_p25i";
        } else if ([[obj protocolStrings] containsObject:@"com.bluebamboo.p200"]) {
            protocolString = @"com.bluebamboo.p200";
            deviceName = @"bluebamboo_p200";
		} else if ([[obj protocolStrings] containsObject:@"com.poslavu.POSLavu"]) {
            protocolString = @"com.poslavu.POSLavu";
			deviceName = @"idynamo";
        } else if ([[obj protocolStrings] containsObject:@"com.magtek.idynamo"]) {
			protocolString = @"com.magtek.idynamo";
			deviceName = @"idynamo";
        } else if ([[obj protocolStrings] containsObject:@"com.merchantwarehouse.merchantware"]) {
            protocolString = @"com.merchantwarehouse.merchantware";
            deviceName = @"idynamo";
        } else if ([[obj protocolStrings] containsObject:@"COM.MERCHANTWAREHOUSE.MERCHANTWARE"]) {
			protocolString = @"COM.MERCHANTWAREHOUSE.MERCHANTWARE";
			deviceName = @"idynamo";
        } else if ([[obj protocolStrings] containsObject:@"com.idtechproducts.reader"]) {
            protocolString = @"com.idtechproducts.reader";
            deviceName = @"idtech_imag";
            //} else if ([[obj protocolStrings] containsObject:@"com.idtechproducts.ismart"]) {
            //protocolString = @"com.idtechproducts.ismart";
        } else if ([[obj protocolStrings] containsObject:@"com.usaepay.com.sbrclip"]) {
            protocolString = @"com.usaepay.com.sbrclip";
            deviceName = @"paysaber";
		} else if ([[obj protocolStrings] containsObject:@"com.datecs.linea.pro.msr"] || [[obj protocolStrings] containsObject:@"com.datecs.linea.pro.bar"]) {
            //NSLog(@"lineaPro detected");
            if ([[obj protocolStrings] containsObject:@"com.datecs.linea.pro.msr"]) protocolString = @"com.datecs.linea.pro.msr";
            if ([[obj protocolStrings] containsObject:@"com.datecs.linea.pro.bar"]) {
                if (![protocolString isEqualToString:@""]) protocolString = [protocolString stringByAppendingString:@","];
                protocolString = [protocolString stringByAppendingString:@"com.datecs.linea.pro.bar"];
            }
            deviceName = @"linea";
		} else if ([[obj protocolStrings] containsObject:@"jp.star-m.starpro"]) {
            protocolString = @"jp.star-m.starpro";
            deviceName = @"tsp650ii";
        }
        
        if (![deviceName isEqualToString:@""]) {
            if (![foundDevices objectForKey:deviceName]) [foundDevices setObject:protocolString forKey:deviceName];
            if (![connectedDevices objectForKey:deviceName]) {
                if ([deviceName isEqualToString:@"idynamo"]) {
                    if ([connectedDevices objectForKey:@"udynamo"]) [self performSelectorOnMainThread:@selector(doubleMagTekWarning:) withObject:@"idynamo" waitUntilDone:NO];
                    else [self performSelectorOnMainThread:@selector(initializeMagTek:) withObject:[NSDictionary dictionaryWithObjectsAndKeys:@"idynamo", @"reader", protocolString, @"protocol", nil] waitUntilDone:NO];
                    //} else if ([deviceName isEqualToString:@"bluebamboo_p200"]) {
                    //[self performSelectorOnMainThread:@selector(initializeBlueBamboo:) withObject:[NSDictionary dictionaryWithObjectsAndKeys:deviceName, @"reader", protocolString, @"protocol", nil] waitUntilDone:NO];
                } else {
                    ExternalDeviceDelegate *newDeviceDelegate = [[ExternalDeviceDelegate alloc] init:deviceName protocol:protocolString accessory:obj];
                    if (newDeviceDelegate){
                        [connectedDevices setObject:newDeviceDelegate forKey:deviceName];
                        [newDeviceDelegate release];
                    }
                }
            }
        }
	}
    
    BOOL deviceWasRemoved = NO;
    NSArray *expectedDevices = [connectedDevices allKeys];
    for (NSString *deviceName in expectedDevices) {
        
        [appDelegate performSelectorOnMainThread:@selector(dcLog:) withObject:[NSString stringWithFormat:@"deviceName: %@, foundDevices: %@", deviceName, foundDevices] waitUntilDone:NO];
        
        if (![deviceName isEqualToString:@"idtech_unimag"] && ![deviceName isEqualToString:@"udynamo"] && ![foundDevices objectForKey:deviceName]) {
            //id obj = [connectedDevices objectForKey:deviceName];
            //[obj release];
            [connectedDevices removeObjectForKey:deviceName];
            deviceWasRemoved = YES;
        }
    }
    
    NSString *deviceList = [[connectedDevices allKeys] componentsJoinedByString:@", "];
    
    ViewController *viewController = (ViewController *)appDelegate.window.rootViewController;
    viewController.connectedDevices.text = deviceList;
    
    if (deviceWasRemoved) {
        if (![self hasCardReader]) {
            [self performSelectorOnMainThread:@selector(changeTabSearchButtonState:) withObject:@"0" waitUntilDone:NO];
            [self performSelectorOnMainThread:@selector(cardIcon:) withObject:@"down" waitUntilDone:NO];
        }
    }
    
    //[appDelegate performSelectorOnMainThread:@selector(dcLog:) withObject:[NSString stringWithFormat:@"connectedDevices: %@", connectedDevices] waitUntilDone:NO];
}

- (BOOL)shouldListen {
    
    //return ([smai.activePOSview isEqualToString:@"checkOut"] || ([smai.activePOSview isEqualToString:@"order"] && appDelegate.orderView.checkOutMode) || ([smai.activePOSview isEqualToString:@"diningRoom"] && ([smai.cardSwipeMode isEqualToString:@"Retrieve Tab"] || [smai.cardSwipeMode isEqualToString:@"Create Tab"])));
    
    ViewController *viewController = (ViewController *)appDelegate.window.rootViewController;
    
    return viewController.isListening;
}

- (void)startListening {
    
    [smai rLog:@"startListening (edHub)"];
    
    NSArray *devicesNames = [connectedDevices allKeys];
    for (NSString *deviceName in devicesNames) {
        if ([deviceName isEqualToString:@"idtech_unimag"]) [self requestUniMagSwipe];
        else {
            id obj = [connectedDevices objectForKey:deviceName];
            if ([obj isKindOfClass:[ExternalDeviceDelegate class]]) {
                ExternalDeviceDelegate *edd = (ExternalDeviceDelegate *)obj;
                [edd startListening];
            } else if ([obj isKindOfClass:[magTekDelegate class]]) {
                magTekDelegate *magTek = (magTekDelegate *)obj;
                if ([magTek.deviceName isEqualToString:@"udynamo"]) [magTek openDevice:@"listening"];
            }
        }
    }
}

- (void)stopListening {
    
    NSArray *devicesNames = [connectedDevices allKeys];
    for (NSString *deviceName in devicesNames) {
        if ([deviceName isEqualToString:@"idtech_unimag"]) [self cancelUniMagSwipe];
        else {
            id obj = [connectedDevices objectForKey:deviceName];
            if ([obj isKindOfClass:[ExternalDeviceDelegate class]]) {
                ExternalDeviceDelegate *edd = (ExternalDeviceDelegate *)obj;
                [edd stopListening];
            } else if ([obj isKindOfClass:[magTekDelegate class]]) {
                magTekDelegate *magTek = (magTekDelegate *)obj;
                if ([magTek.deviceName isEqualToString:@"udynamo"]) [magTek closeDevice];
            }
        }
    }
}

- (void)startReaderActivityIndicator {
    
    /*if ([smai.activePOSview isEqualToString:@"diningRoom"]) [appDelegate.diningRoomView startReaderActivityIndicator];
    if ([smai.activePOSview isEqualToString:@"order"]) {
        if (appDelegate.orderView.checkOutMode) [smai.checkOutPanel.readerActivityIndicator startAnimating];
    }*/
    
    ViewController *viewController = (ViewController *)appDelegate.window.rootViewController;
    [viewController.readerActivityIndicator startAnimating];
}

- (void)stopReaderActivityIndicator {
    
    /*if ([smai.activePOSview isEqualToString:@"diningRoom"]) [appDelegate.diningRoomView.readerActivityIndicator stopAnimating];
    if ([smai.activePOSview isEqualToString:@"order"]) {
        if (appDelegate.orderView.checkOutMode) [smai.checkOutPanel.readerActivityIndicator stopAnimating];
    }*/

    ViewController *viewController = (ViewController *)appDelegate.window.rootViewController;
    [viewController.readerActivityIndicator stopAnimating];
}

- (void)processSwipe:(NSString *)swipeString dataString:(NSString *)dataString reader:(NSString *)reader encrypted:(BOOL)encrypted {
    
    [smai rLog:[NSString stringWithFormat:@"processSwipe: %@ %i", reader, encrypted]];
    [appDelegate dcLog:[NSString stringWithFormat:@"processSwipe: %@ %i", reader, encrypted]];
    
    if (self.componentListeningForCardSwipe != nil) {
        
        if ([self.componentListeningForCardSwipe respondsToSelector:@selector(stringByEvaluatingJavaScriptFromString:)]) {
            
            //NSString *jsString = [NSString stringWithFormat:@"alert('%@');", swipeString];
            NSString *jsString = [NSString stringWithFormat:@"handleCardSwipe('%@', '%@', '%@', '%i');", swipeString, dataString, reader, encrypted];
            //NSLog(@"jsString: %@", jsString);
            NSString *js_result = [self.componentListeningForCardSwipe stringByEvaluatingJavaScriptFromString:jsString];
            //NSLog(@"js_result: %@", js_result);
            //NSLog(@"component: %@", [self.componentListeningForCardSwipe description]);
            //NSLog(@"component: %@", [self.componentListeningForCardSwipe request]);
            if (js_result == nil) [smai standardAlert:@"And error occurred while trying to send card swipe to active component." withTitle:@"Cannot Process Swipe"];
        }
        
    } else {
        
        self.lastSwipeString = swipeString;
        self.lastSwipeDataString = dataString;
        self.lastSwipeReaderName = reader;
        self.lastSwipeIsEncrypted = encrypted;

        [appDelegate rLog:[NSString stringWithFormat:@"reader: %@", reader]];
        [appDelegate rLog:[NSString stringWithFormat:@"swipeString: %@", swipeString]];
        [appDelegate rLog:[NSString stringWithFormat:@"dataString: %@", dataString]];
        
        ViewController *viewController = (ViewController *)appDelegate.window.rootViewController;
        viewController.lastSwipeReader.text = reader;
        viewController.lastSwipeString.text = swipeString;
        viewController.lastSwipeHex.text = dataString;

        //NSLog(@"lastSwipeIsEncrypted: %i", self.lastSwipeIsEncrypted);
        
        /*if ([smai.activePOSview isEqualToString:@"diningRoom"]) {
            
            if ([smai.cardSwipeMode isEqualToString:@"Retrieve Tab"]) [appDelegate.diningRoomView retrieveTabFromSwipe];
            else if ([smai.cardSwipeMode isEqualToString:@"Create Tab"]) [[NSNotificationCenter defaultCenter] postNotificationName:@"createTabFromSwipe" object:nil userInfo:nil];
            else [self clearSwipeData];
            
        } else if ([smai.activePOSview isEqualToString:@"categories"] || [smai.activePOSview isEqualToString:@"order"]) {
            
            if (!smai.nowSendingSwipeData) {
                if (appDelegate.orderView.checkOutMode) { // using new checkout
                    if ([smai.cardSwipeMode isEqualToString:@"Create Tab"]) [smai.checkOutPanel createTabFromSwipe];
                    else if ([smai.cardSwipeMode isEqualToString:@"Voice Auth"]) [smai.checkOutPanel applyCardSwipeForVoiceAuth];
                    else {
                        if (smai.offlineMode) {
                            [smai standardAlert:[smai ttal:@"Credit card transactions currently unavailable."] withTitle:[smai ttal:@"Offline Mode"]];
                            [self clearSwipeData];
                        } else {
                            smai.nowSendingSwipeData = YES;
                            [smai.checkOutPanel verifyAmountForSwipe];
                        }
                    }
                } else {
                    if (smai.offlineMode) {
                        [smai standardAlert:[smai ttal:@"Gift/loyalty card transactions currently unavailable."] withTitle:[smai ttal:@"Offline Mode"]];
                        [self clearSwipeData];
                    } else {
                        if ([smai.cardSwipeMode isEqualToString:@"Gift Card"] && [smai isIPad]) [appDelegate.orderView checkSwipedGiftCardInfo];
                        else if ([smai.cardSwipeMode isEqualToString:@"Gift Card"]) [appDelegate.categoriesView checkSwipedGiftCardInfo];
                        else [self clearSwipeData];
                    }
                }
            } else [self clearSwipeData];
        }*/
    }
}

- (void)clearSwipeData {
    
    //NSLog(@"clearSwipeData");
    
    self.lastSwipeString = @"";
    self.lastSwipeDataString = @"";
    self.lastSwipeIsEncrypted = NO;
}

- (BOOL)hasCardReader {
    
    BOOL hasReader = NO;
    NSArray *deviceNames = [connectedDevices allKeys];
    for (NSString *deviceName in deviceNames) {
        id obj = [connectedDevices objectForKey:deviceName];
        if ([obj isKindOfClass:[ExternalDeviceDelegate class]]) {
            ExternalDeviceDelegate *edd = (ExternalDeviceDelegate *)obj;
            //NSLog(@"edd: %@ - %i - %i", edd.deviceName, edd.connected, edd.hasCardReader);
            if ([edd connected] && [edd hasCardReader]) {
                hasReader = YES;
                break;
            }
        } else if ([obj isKindOfClass:[magTekDelegate class]]) {
            magTekDelegate *magTek = (magTekDelegate *)obj;
            //NSLog(@"magTek: %@ - %i - %i", magTek.deviceName, magTek.connected, magTek.hasCardReader);
            if ([magTek connected]) {
                hasReader = YES;
                break;
            }
        } /*else if ([obj isKindOfClass:[BlueBambooDelegate class]]) {
           BlueBambooDelegate *bbd = (BlueBambooDelegate *)obj;
           NSLog(@"bbd: %@ - %i - %i", bbd.deviceName, bbd.connected, bbd.hasCardReader);
           if ([bbd connected] && [bbd hasCardReader]) {
           hasReader = YES;
           break;
           }
           }*/
    }
    
    //NSLog(@"hasReader: %i", hasReader);
    
    return hasReader;
}

- (void)changeTabSearchButtonState:(NSString *)state {
    
    //if ([state isEqualToString:@"0"]) [appDelegate.diningRoomView hideTabSearchButton];
    //else if ([state isEqualToString:@"1"]) [appDelegate.diningRoomView showTabSearchButton];
}

- (void)cardIcon:(NSString *)state {
    
    [smai rLog:[NSString stringWithFormat:@"cardIcon: %@", state]];
    
    //UIImage *image = [UIImage imageNamed:[NSString stringWithFormat:@"cc_%@.png", state]];
    //[smai.checkOutPanel.readerStatusIcon setImage:image];
    
    ViewController *viewController = (ViewController *)appDelegate.window.rootViewController;
    viewController.readerStatusIcon.image = [UIImage imageNamed:[NSString stringWithFormat:@"cc_%@.png", state]];
}

- (BOOL)hasScanner {
    
    BOOL hasIt = NO;
    NSArray *deviceNames = [connectedDevices allKeys];
    for (NSString *deviceName in deviceNames) {
        id obj = [connectedDevices objectForKey:deviceName];
        if ([obj isKindOfClass:[ExternalDeviceDelegate class]]) {
            ExternalDeviceDelegate *edd = (ExternalDeviceDelegate *)obj;
            //NSLog(@"edd: %@ - %i - %i", edd.deviceName, edd.connected, edd.hasScanner);
            if ([edd connected] && [edd hasScanner]) {
                hasIt = YES;
                break;
            }
        }
    }
    
    //NSLog(@"hasScanner: %i", hasIt);
    
    return hasIt;
}

- (void)scanUp {
    
}

- (void)scanDown {
    
}

- (BOOL)deviceIsConnected:(NSString *)deviceName {
    
    BOOL connected = NO;
    
    if ([connectedDevices objectForKey:deviceName]) {
        id obj = [connectedDevices objectForKey:deviceName];
        if ([obj isKindOfClass:[ExternalDeviceDelegate class]]) connected = [(ExternalDeviceDelegate *)obj connected];
        else if ([obj isKindOfClass:[magTekDelegate class]]) connected = [(magTekDelegate *)obj connected];
        //else if ([obj isKindOfClass:[BlueBambooDelegate class]]) connected = [(BlueBambooDelegate *)obj connected];
    }
    
    return connected;
}

#pragma mark BlueBamboo

/*- (void)initializeBlueBamboo:(NSDictionary *)info {
 
 [smai rLog:[NSString stringWithFormat:@"initializeBlueBamboo: %@", info]];
 [appDelegate performSelectorOnMainThread:@selector(dcLog:) withObject:[NSString stringWithFormat:@"initializeBlueBamboo: %@", info] waitUntilDone:NO];
 
 BlueBambooDelegate *newBlueBambooDelegate = [[BlueBambooDelegate alloc] init:[info objectForKey:@"reader"] protocol:[info objectForKey:@"protocol"]];
 if (newBlueBambooDelegate) [connectedDevices setObject:newBlueBambooDelegate forKey:[info objectForKey:@"reader"]];
 }*/

- (void)BBprintText:(NSString *)prntStrng sender:(id)sender {
    
    if ([connectedDevices objectForKey:@"bluebamboo_p25i"]) {
        ExternalDeviceDelegate *edd = (ExternalDeviceDelegate *)[connectedDevices objectForKey:@"bluebamboo_p25i"];
        [edd BBprintText:prntStrng sender:sender];
    }
    
    /*if ([connectedDevices objectForKey:@"bluebamboo_p200"]) {
     BlueBambooDelegate *bbd = (BlueBambooDelegate *)[connectedDevices objectForKey:@"bluebamboo_p200"];
     [bbd print:prntStrng sender:sender];
     }*/
    
    if ([connectedDevices objectForKey:@"bluebamboo_p200"]) {
        ExternalDeviceDelegate *edd = (ExternalDeviceDelegate *)[connectedDevices objectForKey:@"bluebamboo_p200"];
        [edd BBprintText:prntStrng sender:sender];
    }
}

#pragma mark UniMag

- (void)addUniMagObservers {
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(uniAccessoryAttach:) name:@"uniMagAttachmentNotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(uniAccessoryDisconnected:) name:@"uniMagDetachmentNotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(uniInsufficientPower:) name:@"uniMagInsufficientPowerNotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(uniPowering:) name:@"uniMagPoweringNotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(uniTimeout:) name:@"uniMagTimeoutNotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(uniAccessoryConnected:) name:@"uniMagDidConnectNotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(uniAccessoryDisconnected:) name:@"uniMagDidDisconnectNotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(uniSwipe:) name:@"uniMagSwipeNotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(uniTimeoutSwipe:) name:@"uniMagTimeoutSwipe" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(uniDataProcessing:) name:@"uniMagDataProcessingNotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(uniInvalidSwipe:) name:@"uniMagInvalidSwipeNotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(accessoryData:) name:@"uniMagDidReceiveDataNotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(uniCommandSending:) name:@"uniMagCmdSendingNotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(uniCommandTimeout:) name:@"uniMagCommandTimeout" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(accessoryCommand:) name:@"uniMagDidReceiveCmdNotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(uniSystemMessage:) name:@"uniMagSystemMessageNotification" object:nil];
}

- (void)removeUniMagObservers {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"uniMagAttachmentNotification" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"uniMagDetachmentNotification" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"uniMagInsufficientPowerNotification" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"uniMagPoweringNotification" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"uniMagTimeoutNotification" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"uniMagDidConnectNotification" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"uniMagDidDisconnectNotification" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"uniMagSwipeNotification" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"uniMagTimeoutSwipe" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"uniMagDataProcessingNotification" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"uniMagInvalidSwipeNotification" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"uniMagDidReceiveDataNotification" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"uniMagCmdSendingNotification" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"uniMagCommandTimeout" object:nil];
	[[NSNotificationCenter defaultCenter] removeObserver:self name:@"uniMagDidReceiveCmdNotification" object:nil];
	[[NSNotificationCenter defaultCenter] removeObserver:self name:@"uniMagSystemMessageNotification" object:nil];
}

- (void)initializeUniMag {
    
    [smai rLog:@"initializeUniMag"];
    
    //if (!smai.audioJackReaderDisabled) {
        [self addUniMagObservers];
        uniReader = [[uniMag alloc] init];
        [uniReader setAutoAdjustVolume: TRUE];
        [uniReader setSwipeTimeoutDuration: 300];
    //}
}

- (void)deinitializeUniMag {
    
    [smai rLog:@"deinitializeUniMag"];
    
    //if (!smai.audioJackReaderDisabled) {
        if (uniReader != nil) {
            [uniReader startUniMag:NO];
            [self removeUniMagObservers];
            [uniReader release];
            uniReader = nil;
        }
    //}
}

- (void)uniAccessoryAttach:(NSNotification *)notification {
    //first
    [smai rLog:@"uniAccessoryAttach"];
    //[uniReader]
    [uniReader startUniMag:YES];
    [self startReaderActivityIndicator];
}

- (void)uniInsufficientPower:(NSNotification *)notification {
    
    [smai rLog:@"uniInsufficientPower"];
    
    [self stopReaderActivityIndicator];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[smai ttal:@"Audio Jack Device"] message:[NSString stringWithFormat:@"%@ %@\n\n%@\n%@\n%@", [smai ttal:@"Insufficient power."], [smai ttal:@"Please follow these steps:"], [smai ttal:@"1. Set headphone volume to maximum level"], [smai ttal:@"2. Disconnect the device"], [smai ttal:@"3. Reconnect the device"]] delegate:self cancelButtonTitle:[smai ttal:@"Continue"] otherButtonTitles:nil];
    [alert show];
    [alert release];
}

- (void)uniPowering:(NSNotification *)notification {
    //second
    
    [self.mtSCRALib setDeviceType:MAGTEKAUDIOREADER];
    if( [[self mtSCRALib] openDevice] ) {
        // We have a magtek device
        [uniReader startUniMag: NO];
        [[self mtSCRALib] closeDevice];
        [[self mtSCRALib] clearBuffers];
        
        [self stopReaderActivityIndicator];
        if (![connectedDevices objectForKey:@"udynamo"]) [self initializeMagTek:[NSDictionary dictionaryWithObjectsAndKeys:@"udynamo", @"reader", @"udynamo", @"protocol", nil]];
        //[self startListening];
    }
    [smai rLog:@"uniPowering"];
}

- (void)uniTimeout:(NSNotification *)notification {
    
    [smai rLog:@"uniTimeout"];
    [uniReader startUniMag: NO];
    //[uniReader startUniMag:YES];
    //NSString *audioJackReaderType = ([smai.configSettings objectForKey:@"audio_jack_reader_type"])?[smai.configSettings objectForKey:@"audio_jack_reader_type"]:@"UniMag";
    //if ([audioJackReaderType isEqualToString:@"uDynamo"]) {
        if ([connectedDevices objectForKey:@"idynamo"]) [self doubleMagTekWarning:@"udynamo"];
        else if (![connectedDevices objectForKey:@"udynamo"]) [self initializeMagTek:[NSDictionary dictionaryWithObjectsAndKeys:@"udynamo", @"reader", @"udynamo", @"protocol", nil]];
    /*} else {
        [self stopReaderActivityIndicator];
        if (![self hasCardReader]) [self changeTabSearchButtonState:@"0"];
        [smai standardAlert:[NSString stringWithFormat:@"%@ %@\n\n%@\n%@\n%@", [smai ttal:@"Timeout error."], [smai ttal:@"Please follow these steps:"], [smai ttal:@"1. Set headpohone volume to maximum level"], [smai ttal:@"2. Disconnect the device"], [smai ttal:@"3. Reconnect the device"]] withTitle:[smai ttal:@"Audio Jack Device"]];
    }*/
}

- (void)uniAccessoryConnected:(NSNotification *)notification {
    
    [smai rLog:@"uniAccessoryConnected"];
    
    //if ([[smai.activeLocation objectForKey:@"gateway"] isEqualToString:@"Mercury"]) {
    //    [smai rLog:@"should enable TDES for UniMag"];
    //    [uniReader sendCommandEnableTDES];
    //} else
        [self uniPostConnect];
}

- (void)uniPostConnect {
    // At this point, all we know for sure, is that we have a microphone jack device of some sort.
    // Can we find out more details about our connected device?
    [smai rLog:@"uniPostConnect"];
    
    ExternalDeviceDelegate *newDeviceDelgate = [[ExternalDeviceDelegate alloc] init:@"idtech_unimag" protocol:@"idtech_unimag" accessory:nil];
    if (newDeviceDelgate) [connectedDevices setObject:newDeviceDelgate forKey:@"idtech_unimag"];
    [self changeTabSearchButtonState:@"1"];
    [self cardIcon:@"up"];
    if ([self shouldListen]) [uniReader requestSwipe];
    [self stopReaderActivityIndicator];
}

- (void)uniAccessoryDisconnected:(NSNotification *)notification {
    
    [smai rLog:@"uniAccessoryDisconnected"];
    
    [self stopDeviceCheckTimer];
    [self stopListening];
    
    if ([connectedDevices objectForKey:@"idtech_unimag"]) {
        //id obj = [connectedDevices objectForKey:@"idtech_unimag"];
        //[obj release];
        [connectedDevices removeObjectForKey:@"idtech_unimag"];
    }
    
    if ([connectedDevices objectForKey:@"udynamo"]) {
        //id obj = [connectedDevices objectForKey:@"udynamo"];
        //[obj release];
        
        [connectedDevices removeObjectForKey:@"udynamo"];
    }
    
    [self stopReaderActivityIndicator];
    if (![self hasCardReader]) {
        [self changeTabSearchButtonState:@"0"];
        [self cardIcon:@"down"];
    }
    
    [uniReader cancelTask];
    [uniReader startUniMag:NO];
    
    [self startDeviceCheckTimer];
}

- (void)uniSwipe:(NSNotification *)notification {
    
    [smai rLog:@"uniSwipe"];
}

- (void)uniTimeoutSwipe:(NSNotification *)notification {
    
    [smai rLog:@"uniTimeoutSwipe"];
    
    [self stopReaderActivityIndicator];
    [self resetUniMagSwipe];
}

- (void)uniDataProcessing:(NSNotification *)notification {
    
    [smai rLog:@"uniDataProcessing"];
    
    [self startReaderActivityIndicator];
}

- (void)uniInvalidSwipe:(NSNotification *)notification {
    
    [smai rLog:@"uniInvalidSwipe"];
    
    [self stopReaderActivityIndicator];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[smai ttal:@"Audio Jack Device"] message:[NSString stringWithFormat:@"%@ %@", [smai ttal:@"Failed to decode the swipe data."], [smai ttal:@"Please try again."]] delegate:self cancelButtonTitle:[smai ttal:@"Continue"] otherButtonTitles:nil];
    [alert show];
    [alert release];
    
    [NSTimer scheduledTimerWithTimeInterval:0.05 target:uniReader selector:@selector(requestSwipe) userInfo:nil repeats:NO];
}

- (void)accessoryData:(NSNotification *)notification {
    
    [smai rLog:@"accessoryData"];
    
    [self stopReaderActivityIndicator];
    
	NSData *data = [notification object];
    NSString *swipeString = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
    NSString *sendString = swipeString;
    NSString *dataString = @"";
    BOOL encrypted = NO;
	
    if ([swipeString length] > 0) {
        if (![[swipeString substringToIndex:1] isEqualToString:@"%"]) {
            
            encrypted = YES;
            dataString = [NSString stringWithFormat:@"%@", data];
            dataString = [dataString stringByReplacingOccurrencesOfString:@"<" withString:@""];
            dataString = [dataString stringByReplacingOccurrencesOfString:@">" withString:@""];
            dataString = [dataString stringByReplacingOccurrencesOfString:@" " withString:@""];
            
            NSRange range = [swipeString rangeOfString:@"%*"];
            if (range.location != NSNotFound) sendString = [swipeString substringFromIndex:(range.location + 2)];
        }
    }
    
    if ([sendString length] > 20) [self processSwipe:sendString dataString:dataString reader:@"idtech_unimag" encrypted:encrypted];
    else {
        [smai standardAlert:[smai ttal:@"Please try again."] withTitle:[smai ttal:@"Card Read Failed"]];
        [self resetUniMagSwipe];
    }
    
    [swipeString release];
}

- (void)uniCommandSending:(NSNotification *)notification {
    
    [smai rLog:@"uniCommandSending"];
    
	NSData *data = [notification object];
	NSString *text = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
	[text release];
}

- (void)uniCommandTimeout:(NSNotification *)notification {
    
    [smai rLog:@"uniCommandTimeout"];
    
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Audio Jack Device" message:@"Command Timeout. Please try to send the command again." delegate:self cancelButtonTitle:[smai ttal:@"Continue"] otherButtonTitles:nil];
	[alert show];
	[alert release];
    
    [self uniPostConnect];
}

- (void)accessoryCommand:(NSNotification *)notification {
    
    [smai rLog:@"accessoryCommand"];
    
	NSData *data = [notification object];
	NSString *text = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
	[text release];
    
    [self uniPostConnect];
}

- (void)uniSystemMessage:(NSNotification *)notification {
    
    [smai rLog:@"uniSystemMessage"];
    
	NSError *err = nil;
	err = [notification object];
	
	[smai rLog:[NSString stringWithFormat:@"%d: %@", [err code], [[err userInfo] valueForKey:NSLocalizedDescriptionKey]]];
}

- (void)resetUniMagSwipe {
    
    [smai rLog:@"resetUniMagsSwipe"];
    
    [self clearSwipeData];
    [uniReader cancelSwipe];
    [self requestUniMagSwipe];
}

- (void)requestUniMagSwipe {
    
    // check for connected unimag device
    
    [NSTimer scheduledTimerWithTimeInterval:0.1 target:uniReader selector:@selector(requestSwipe) userInfo:nil repeats:NO];
}

- (void)cancelUniMagSwipe {
    
    [uniReader cancelTask];
}

#pragma mark MagTek

- (void)initializeMagTek:(NSDictionary *)info {
    
    [smai rLog:[NSString stringWithFormat:@"initializeMagTek: %@", info]];
    [appDelegate dcLog:[NSString stringWithFormat:@"initializeMagTek: %@", info]];
    
    magTekDelegate *magTek = [[magTekDelegate alloc] init:[info objectForKey:@"reader"] protocol:[info objectForKey:@"protocol"]];
    if (magTek){
        [connectedDevices setObject:magTek forKey:[info objectForKey:@"reader"]];
        [magTek release];
    }
}

- (void)deinitializeMagTek:(NSString *)reader {
    
    [smai rLog:[NSString stringWithFormat:@"deinitializeMagTek: %@", reader]];
    [appDelegate dcLog:[NSString stringWithFormat:@"deinitializeMagTek: %@", reader]];
    
    [self stopDeviceCheckTimer];
    
    if ([connectedDevices objectForKey:reader]) {
        //id obj = [connectedDevices objectForKey:reader];
        //[obj release];
        [connectedDevices removeObjectForKey:reader];
    }
    
    [self startDeviceCheckTimer];
}

- (void)magTekConnectionFailed:(NSString *)mode reader:(NSString *)reader {
    
    [smai rLog:[NSString stringWithFormat:@"magTekConnectionFailed: %@ %@", mode, reader]];
    
    [self stopReaderActivityIndicator];
    
    if ([reader isEqualToString:@"uDynamo"]) {
        NSString *title = @"Audio Jack Device";
        if ([mode isEqualToString:@"listening"]) title = @"uDynamo";
        [smai standardAlert:[NSString stringWithFormat:@"%@ %@\n\n%@\n%@\n%@", [smai ttal:@"Timeout error."], [smai ttal:@"Please follow these steps:"], [smai ttal:@"1. Set headphone volume to maximum level"], [smai ttal:@"2. Disconnect the device"], [smai ttal:@"3. Reconnect the device"]] withTitle:title delegate:self];
    } else [smai standardAlert:@"A connection cannot be established. Please try disconnecting and reconnecting the device." withTitle:@"iDynamo"];
    
    [self deinitializeMagTek:reader];
}

- (void)doubleMagTekWarning:(NSString *)reader {
    
    [self stopReaderActivityIndicator];
    if (!doubleMagTekWarningIsShowing) {
        doubleMagTekWarningIsShowing = YES;
        if ([reader isEqualToString:@"idynamo"]) [smai standardAlert:@"Please disconnect the uDyanmo if you wish to use the iDynamo." withTitle:@"iDynamo Unable to Connect" delegate:self];
        else if ([reader isEqualToString:@"udynamo"]) [smai standardAlert:@"Please disconnect the iDynamo if you wish to use the uDynamo." withTitle:@"uDyanmo Unable to Connect" delegate:self];
    }
}

#pragma mark

- (void)dealloc {
    
    [smai rLog:@"dealloc ExternalDeviceHub"];
    
    [connectedDevices release];
    
    [mtSCRALib release];
    [deviceCheckTimer release];
    [lastSwipeReaderName release];
    [lastSwipeString release];
    [lastSwipeDataString release];
    [componentListeningForCardSwipe release];
    [componentListeningForScan release];
    
    [super dealloc];
}

@end
