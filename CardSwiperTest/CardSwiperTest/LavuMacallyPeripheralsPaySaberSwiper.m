//
//  LavuPaySaberSwiper.m
//  CardSwiperTest
//
//  Created by Theodore Schnepper on 11/4/13.
//  Copyright (c) 2013 Lavu, Inc. All rights reserved.
//

#import "LavuMacallyPeripheralsPaySaberSwiper.h"

@implementation LavuMacallyPeripheralsPaySaberSwiper


- (id) init {
    self = [super init];
    return self;
}

- (void) handleReadStream:(NSInputStream *)inputStream {
    uint8_t buffer[1024];
    for( int i = 0; i < 1024; i++ ){
        buffer[i] = 0;
    }
    unsigned int len = [inputStream read: buffer maxLength: 1024];
    NSData* data = [NSData dataWithBytes: buffer length: len];
    
    [[NSNotificationCenter defaultCenter] postNotificationName: LavuCardSwipeDataNotification object: [[[LavuMacallyPeripheralsSwipeData alloc] initWithData: data] autorelease] userInfo: NULL];
    [self disconnect];
    
}

+ (NSString*) imageName {
    return @"macally_peripherals_pay_saber.png";
}

+ (NSString*) getName {
    return @"SBR_CLIP";
}

+ (NSString*) getManufacturer {
    return @"Macally Peripherals";
}

+ (NSString*) getProtocol {
    return @"com.usaepay.com.sbrclip";
}

+ (NSString*) deviceManufacturer {
    return  @"Macally Peripherals";
}

+ (NSString*) deviceName {
    return @"PaySaber Clip";
}

@end
