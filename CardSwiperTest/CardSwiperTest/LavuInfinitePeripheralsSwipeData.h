//
//  LavuInfinitePeripheralsSwipeData.h
//  CardSwiperTest
//
//  Created by Theodore Schnepper on 10/25/13.
//  Copyright (c) 2013 Lavu, Inc. All rights reserved.
//

#import "LavuNullSwipeData.h"

@interface LavuInfinitePeripheralsSwipeData : LavuNullSwipeData

@end
