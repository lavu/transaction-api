//
//  LavuPaySaberSwiper.h
//  CardSwiperTest
//
//  Created by Theodore Schnepper on 11/4/13.
//  Copyright (c) 2013 Lavu, Inc. All rights reserved.
//

#import "LavuEAAccessorySwiper.h"
#import "LavuMacallyPeripheralsSwipeData.h"

@interface LavuMacallyPeripheralsPaySaberSwiper : LavuEAAccessorySwiper

@end
