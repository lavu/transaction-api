 //
//  main.m
//  CardSwiperTest
//
//  Created by Richard Gieske on 10/4/13.
//  Copyright (c) 2013 Lavu, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
