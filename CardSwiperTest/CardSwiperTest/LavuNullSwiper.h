//
//  LavuNullSwipper.h
//  CardSwiperTest
//
//  Created by Theodore Schnepper on 10/15/13.
//  Copyright (c) 2013 Lavu, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LavuSwiperProtocol.h"

@interface LavuNullSwiper : NSObject <LavuSwiperProtocol> {
    NSObject<LavuSwiperDelegate>* _delegate;
}

@end
