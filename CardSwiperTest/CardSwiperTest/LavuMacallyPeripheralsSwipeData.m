//
//  LavuMacallyPeripheralsSwipeData.m
//  CardSwiperTest
//
//  Created by Theodore Schnepper on 11/4/13.
//  Copyright (c) 2013 Lavu, Inc. All rights reserved.
//

#import "LavuMacallyPeripheralsSwipeData.h"

@implementation LavuMacallyPeripheralsSwipeData

- (id) initWithData:(NSData *)swipeData {
    self = [super init];
    
    const char* bytes = [swipeData bytes];
    
    _encrypted = TRUE; // Should Always be the Case
    int pos = 0;
    int startpos = 0;
    if( 2 == bytes[2] ){
        pos = 2;
        startpos = 2;
    } else if( 2 == bytes[3] ){
        pos = 3;
        startpos = 3;
    }

    if( 2 == bytes[pos] ){
        pos += 3;
        //int encoding = bytes[pos++];
        //int decodingStatus = bytes[pos++];
        int track1Len = bytes[pos++];
        int track2Len = bytes[pos++];
        int track3Len = bytes[pos++];
        
        if( 3 == bytes[pos] ){
            pos += 2;
        }
        
        if( [swipeData length] <= pos + track1Len + track2Len + track3Len ){
            //send full track
        } else {
            //Should have masked tracks
            int encryptedLen = track1Len + track2Len + track3Len;
            NSString* maskedTrack = [[[NSString alloc] initWithData:[swipeData subdataWithRange: NSMakeRange(pos, encryptedLen)] encoding: NSASCIIStringEncoding] autorelease];
            pos += encryptedLen;
            
            if( encryptedLen % 8 ){
                encryptedLen += (8 - (encryptedLen%8));
            }
            
            if( [swipeData length] <= pos + encryptedLen ){
                //Send Masked Track (We don't have Masked Tracks)
            } else {
                [self parseTracks: maskedTrack];
                //We can break out the separate tracks
                pos += encryptedLen;
                NSString* track1Hash;
                if(track1Len){
                    if( [swipeData length] > pos+20 ){
                        track1Hash = [[[NSString alloc] initWithData:[swipeData subdataWithRange:NSMakeRange(pos, 20)] encoding: NSASCIIStringEncoding] autorelease];
                        [self setEncryptedTrack1: track1Hash];
                    }
                    pos += 20;
                }
                
                NSString* track2Hash;
                if(track2Len){
                    if( [swipeData length] > pos+20 ){
                        track2Hash = [[[NSString alloc] initWithData:[swipeData subdataWithRange:NSMakeRange(pos, 20)] encoding: NSASCIIStringEncoding] autorelease];
                        [self setEncryptedTrack2: track2Hash];
                    }
                    pos += 20;
                }
                
                NSString* track3Hash;
                if(track3Len){
                    if( [swipeData length] > pos+20 ){
                        track3Hash = [[[NSString alloc] initWithData:[swipeData subdataWithRange:NSMakeRange(pos, 20)] encoding: NSASCIIStringEncoding] autorelease];
                        [self setEncryptedTrack3: track3Hash];
                    }
                    pos += 20;
                }
                
                NSString* KSN;
                if( pos + 10 < [swipeData length] ){
                    KSN = [[[NSString alloc] initWithData:[swipeData subdataWithRange:NSMakeRange(pos, 10)] encoding: NSASCIIStringEncoding] autorelease];
                }
                
                if( startpos ){
                    //Sent Base64 Encoded Sub Track
                } else {
                    //Sent Base64 Encoded Full Track
                }
            }
        }
    } else if( 0 == bytes[1] ){
        NSString* tempStr = [[[NSString alloc] initWithData:[swipeData subdataWithRange: NSMakeRange(2, [swipeData length]-3)] encoding:NSASCIIStringEncoding] autorelease];
        NSArray* components = [tempStr componentsSeparatedByString: @"\r"];
        if([components count] >= 2){
            tempStr = [NSString stringWithFormat: @"%@%@", [components objectAtIndex:1], [components objectAtIndex:0]];
            NSLog(@"%@", tempStr);
        }
    }
    
    return self;
}

@end
