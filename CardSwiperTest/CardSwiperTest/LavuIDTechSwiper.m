//
//  IDTechSwipper.m
//  CardSwiperTest
//
//  Created by Theodore Schnepper on 10/15/13.
//  Copyright (c) 2013 Lavu, Inc. All rights reserved.
//

#import "LavuIDTechSwiper.h"

@implementation LavuIDTechSwiper
+ (NSString*) deviceManufacturer {
    return @"ID Tech";
}
@end
