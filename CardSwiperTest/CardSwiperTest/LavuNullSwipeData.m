//
//  LavuNullSwipeData.m
//  CardSwiperTest
//
//  Created by Theodore Schnepper on 10/24/13.
//  Copyright (c) 2013 Lavu, Inc. All rights reserved.
//

#import "LavuNullSwipeData.h"

@implementation LavuNullSwipeData

@synthesize totalTrack = _totalTrack;

@synthesize track1String = _track1String;
@synthesize formatCode = _formatCode;
@synthesize name = _name;
@synthesize track1Discretionary = _track1Discretionary;

@synthesize track2String = _track2String;
@synthesize track2Discretionary = _track2Discretionary;
@synthesize LRC = _LRC;

@synthesize PAN = _PAN;
@synthesize expiration = _expiration;
@synthesize serviceCode = _serviceCode;

@synthesize track3String = _track3String;

- (id) init {
    self = [super init];
    [self setTrack1String: @""];
    [self setTrack2String: @""];
    [self setTrack3String: @""];
    _encrypted = FALSE;
    return self;
}

- (void) dealloc {
    if( _track1String ){
        [_track1String release];
    }
    if( _formatCode ){
        [_formatCode release];
    }
    if( _name ){
        [_name release];
    }
    if( _track1Discretionary ){
        [_track1Discretionary release];
    }
    if( _track2String ){
        [_track2String release];
    }
    if( _track2Discretionary ){
        [_track2Discretionary release];
    }
    if( _LRC ){
        [_LRC release];
    }
    if( _PAN ){
        [_PAN release];
    }
    if( _expiration ){
        [_expiration release];
    }
    if( _serviceCode ){
        [_serviceCode release];
    }
    if( _track3String ){
        [_track3String release];
    }
    [super dealloc];
}

- (BOOL) isEncrypted {
    return _encrypted;
}

- (BOOL) validRange: (NSRange) range forString: (NSString*) str {
    return range.location <= [str length] && range.location + range.length <= [str length];
}

- (void) parseTracks: (NSString*) tracks123 {
    NSError* error = NULL;
    NSRegularExpression* trackRegexp = [[[NSRegularExpression alloc] initWithPattern:@"(%([A-Z\\*])([0-9\\*\\ ]{1,19})\\^([^\\^]{2,})\\^([0-9\\*]{4}|\\^)([0-9\\*]{3}|\\^)([^\\?]+)\\?[0-9\\*]?)?(;([0-9\\*]{1,19})=([\\d\\*]{4}|=)([\\d\\*]{3}|=)([\\d\\*]*)?\\?([\\d\\*]*))?" options:NSRegularExpressionAllowCommentsAndWhitespace error: &error] autorelease];
    if( error ){
        NSLog(@"RegExp Error %@ [%ld] %@", [error domain], (long)[error code], [error userInfo]);
        [trackRegexp release];
    } else {
        NSArray* matches = [trackRegexp matchesInString: tracks123 options: 0 range: NSMakeRange(0, [tracks123 length])];
        
        if( [matches count] && [[matches objectAtIndex: 0] numberOfRanges] >=  14){
            NSTextCheckingResult *match = [matches objectAtIndex: 0];
            
            if( [self validRange: [match rangeAtIndex: 0] forString: tracks123] ){
                if(![self totalTrack]){
                    [self setTotalTrack: [tracks123 substringWithRange: [match rangeAtIndex: 0]]];
                }
                if( [self validRange: [match rangeAtIndex: 1] forString: tracks123] ){
                    [self setTrack1String: [tracks123 substringWithRange: [match rangeAtIndex: 1]]];
                    [self setFormatCode: [tracks123 substringWithRange: [match rangeAtIndex: 2]]];
                    [self setPAN: [tracks123 substringWithRange: [match rangeAtIndex: 3]]];
                    
                    [self setPAN: [[self PAN] stringByReplacingOccurrencesOfString:@" " withString:@""]];
                    
                    [self setName: [tracks123 substringWithRange: [match rangeAtIndex: 4]]];
                    [self setExpiration: [tracks123 substringWithRange: [match rangeAtIndex: 5]]];
                    [self setServiceCode: [tracks123 substringWithRange: [match rangeAtIndex: 6]]];
                    [self setTrack1Discretionary: [tracks123 substringWithRange: [match rangeAtIndex: 6]]];
                }
                
                if( [self validRange: [match rangeAtIndex: 8] forString: tracks123] ){
                    [self setTrack2String: [tracks123 substringWithRange: [match rangeAtIndex: 8]]];
                    if( [self PAN] && ![[self PAN] isEqualToString: [tracks123 substringWithRange: [match rangeAtIndex: 9]]] ){
                        NSLog(@"PAN Doesn't Match");
                    }
                    if( [self expiration] && ![[self expiration] isEqualToString: [tracks123 substringWithRange: [match rangeAtIndex: 10]]] ){
                        NSLog(@"Expiration Doesn't Match");
                    }
                    if( [self serviceCode] && ![[self serviceCode] isEqualToString: [tracks123 substringWithRange: [match rangeAtIndex: 11]]] ){
                        NSLog(@"Service Code Doesn't Match");
                    }
                    [self setTrack2Discretionary: [tracks123 substringWithRange: [match rangeAtIndex: 12]]];
                    if( [self validRange: [match rangeAtIndex: 13] forString: tracks123] ){
                        [self setLRC: [tracks123 substringWithRange: [match rangeAtIndex: 13]]];
                    }
                }
            }
        }
    }
}

- (NSString*) toLavuGatewayString {
    return [NSString stringWithFormat: @"%@%@%@", [self track1String], [self track2String], [self track3String]];
}

@end
