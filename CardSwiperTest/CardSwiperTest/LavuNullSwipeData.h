//
//  LavuNullSwipeData.h
//  CardSwiperTest
//
//  Created by Theodore Schnepper on 10/24/13.
//  Copyright (c) 2013 Lavu, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LavuCardDataProtocol.h"

@interface LavuNullSwipeData : NSObject <LavuCardDataProtocol> {
    BOOL _encrypted;
}
@property (retain, nonatomic) NSString* totalTrack;

@property (retain, nonatomic) NSString* track1String;
@property (retain, nonatomic) NSString* formatCode;
@property (retain, nonatomic) NSString* name;
@property (retain, nonatomic) NSString* track1Discretionary;

@property (retain, nonatomic) NSString* track2String;
@property (retain, nonatomic) NSString* track2Discretionary;
@property (retain, nonatomic) NSString* LRC; //Longitudinal Redundancy Check (Usually Empty)

@property (retain, nonatomic) NSString* PAN;
@property (retain, nonatomic) NSString* expiration;
@property (retain, nonatomic) NSString* serviceCode;

@property (retain, nonatomic) NSString* track3String;

@property (retain, nonatomic) NSString* encryptedTrack1;
@property (retain, nonatomic) NSString* encryptedTrack2;
@property (retain, nonatomic) NSString* encryptedTrack3;

/** This will take track data, either masked, or unmasked, and will parse out the properties of LavuCardDataProtocol.  This selector just sets data with the exception of whether the overall data is encrypted.
 */
- (void) parseTracks: (NSString*) tracks123;
@end
