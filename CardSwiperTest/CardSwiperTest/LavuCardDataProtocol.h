//
//  LavuCardDataProtocol.h
//  CardSwiperTest
//
//  Created by Theodore Schnepper on 10/10/13.
//  Copyright (c) 2013 Lavu, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol LavuCardDataProtocol <NSObject>

- (NSString*) totalTrack;
- (void) setTotalTrack: (NSString*) totalTrack;

- (NSString*) track1String;
- (void) setTrack1String: (NSString*) track1String;
- (NSString*) formatCode;
- (void) setFormatCode: (NSString*) formatCode;
- (NSString*) name;
- (void) setName: (NSString*) name;
- (NSString*) track1Discretionary;
- (void) setTrack1Discretionary: (NSString*) track1Discretionary;

- (NSString*) track2String;
- (void) setTrack2String: (NSString*) track2String;
- (NSString*) track2Discretionary;
- (void) setTrack2Discretionary: (NSString*) track2Discretionary;
- (NSString*) LRC;
- (void) setLRC: (NSString*) LRC;

- (NSString*) PAN;
- (void) setPAN: (NSString*) PAN;
- (NSString*) expiration;
- (void) setExpiration: (NSString*) expiration;
- (NSString*) serviceCode;
- (void) setServiceCode: (NSString*) serviceCode;

- (NSString*) track3String;
- (void) setTrack3String: (NSString*) track3String;

- (NSString*) toLavuGatewayString;
- (BOOL) isEncrypted;
@end
