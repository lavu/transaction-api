//
//  BlueBambooSipeData.h
//  CardSwiperTest
//
//  Created by Theodore Schnepper on 10/11/13.
//  Copyright (c) 2013 Lavu, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LavuNullSwipeData.h"

@interface LavuBlueBambooSwipeData : LavuNullSwipeData
- (id) initWithSwipeBuffer: (uint8_t*) buffer forLength: (unsigned int) length;

@end
