//
//  LavuMagTeckIDynamoSwipper.m
//  CardSwiperTest
//
//  Created by Theodore Schnepper on 10/8/13.
//  Copyright (c) 2013 Lavu, Inc. All rights reserved.
//

#import "LavuMagTekIDynamoSwiper.h"

@implementation LavuMagTekIDynamoSwiper
- (id) init {
    self = [super init];
    [self setDeviceType: MAGTEKIDYNAMO];
    return self;
}

+ (LavuCardSwiperType) getSwipperType {
    return LavuCardSwiperTypeAppleConnector;
}

+ (NSString*) deviceName {
    return @"iDynamo";
}

+ (NSString*) imageName {
    return @"magtek_idynamo.jpg";
}

@end
