//
//  LavuInfinitePeripheralsInfineaTabSwipper.m
//  CardSwiperTest
//
//  Created by Theodore Schnepper on 10/25/13.
//  Copyright (c) 2013 Lavu, Inc. All rights reserved.
//

#import "LavuInfinitePeripheralsInfineaTabSwiper.h"

@implementation LavuInfinitePeripheralsInfineaTabSwiper

- (id) init {
    self = [super init];
    return self;
}

+ (NSString*) getName {
    return @"LINEAPad2L";
}
+ (NSString*) getProtocol {
    return @"com.datecs.linea.pro.msr";
}

+ (NSString*) deviceName {
    return @"Infinea Tab";
}

+ (NSString*) imageName {
    return @"infinite_peripherals_infinea_tab.png";
}

@end
