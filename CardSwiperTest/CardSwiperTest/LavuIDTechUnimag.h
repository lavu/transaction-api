//
//  IDTechUnimag.h
//  CardSwiperTest
//
//  Created by Theodore Schnepper on 10/15/13.
//  Copyright (c) 2013 Lavu, Inc. All rights reserved.
//

#import "LavuIDTechSwiper.h"
#import "uniMag.h"
#import "IDTechCardSwipe.h"



@interface LavuIDTechUnimag : LavuIDTechSwiper {
    uniMag* unimagLib;
    BOOL shouldPrepareForSwipeWhenReady;
    BOOL shouldReListenAfterFirstConnection;

    SEL currentTarget;
    id objToPassSelector;
}

#pragma mark - Physical Device
- (void) physicalDeviceAttached: (NSNotification*) notification;
- (void) physicalDeviceDeattached: (NSNotification*) notification;
#pragma mark  - Device Connection
- (void) connectionInsufficientPower: (NSNotification*) notification;
- (void) connectionPowering: (NSNotification*) notification;
- (void) connectionTimeout: (NSNotification*) notification;
- (void) connectionDeviceConnected: (NSNotification*) notification;
- (void) connectionDeviceDisconnected: (NSNotification*) notification;
#pragma mark  - Swipe Information
- (void) swipeRecieved: (NSNotification*) notification;
- (void) swipeTimeout: (NSNotification*) notification;
- (void) swipeProcessingData: (NSNotification*) notification;
- (void) swipeInvalid: (NSNotification*) notification;
- (void) swipeRecievedData: (NSNotification*) notification;
#pragma mark  - Connection Information
- (void) commandSending: (NSNotification*) notification;
- (void) commandTimeout: (NSNotification*) notification;
- (void) commandRecieved: (NSNotification*) notification;
#pragma mark  - Misc Information
- (void) miscSystemMessage: (NSNotification*) notification;

@end
