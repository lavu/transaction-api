//
//  AppDelegate.m
//  CardSwiperTest
//
//  Created by Richard Gieske on 10/4/13.
//  Copyright (c) 2013 Lavu, Inc. All rights reserved.
//

#import "AppDelegate.h"
#import "ViewController.h"

@implementation AppDelegate

@synthesize externalDeviceHub;
@synthesize applicationJustLaunched;
@synthesize nowSendingSwipeData;
@synthesize nowShowingSignatureShade;
@synthesize ccAmountToCheck;

- (void) gotSwipe: (NSNotification*) notification {
    NSObject<LavuCardDataProtocol>* cardData = [notification object];
    [self performSelectorOnMainThread:@selector(rLog:) withObject:[cardData toLavuGatewayString] waitUntilDone:FALSE];
}

#pragma mark - Lavu Swipper Delegate
- (void) didConnect: (BOOL) connectStatus forSwipper: (NSObject<LavuSwiperProtocol>*) swipper {
    //ViewController* controller = (ViewController *)[self window].rootViewController;
    if( connectStatus ){
        [self rLog: @"Connection Succeeded"];
        //[controller cardIcon: @"up"];
        [swipper startListening];
    } else {
        //[controller cardIcon: @"error"];
        //[self rLog: @"Connection Failed"];
    }
}

- (void) didDisconnect: (BOOL) connectStatus forSwipper: (NSObject<LavuSwiperProtocol>*) swipper {
    ViewController* controller = (ViewController *)[self window].rootViewController;
    if( connectStatus ){
        [controller cardIcon: @"down"];
    } else {
        //[controller cardIcon: @"up"];
    }
}

- (void) didStartListening: (BOOL) listenStatus forSwipper: (NSObject<LavuSwiperProtocol>*) swipper {
    //ViewController* controller = (ViewController *)[self window].rootViewController;
    if( listenStatus ){
        [self rLog: @"Listen Succeeded"];
        [swipper prepareForSwipe];
        //[controller cardIcon: @"up"];
    } else {
        [self rLog: @"Listen Failed"];
        //[controller cardIcon: @"error"];
    }
}

- (void) didStopListening: (BOOL) listenStatus forSwipper: (NSObject<LavuSwiperProtocol>*) swipper {
    ViewController* controller = (ViewController *)[self window].rootViewController;
    if( listenStatus ){
        [controller cardIcon: @"down"];
    } else {
        //[controller cardIcon: @"error"];
    }
}

- (void) didPrepareForSwipe: (BOOL) prepareStatus forSwipper: (NSObject<LavuSwiperProtocol>*) swipper {
    ViewController* controller = (ViewController *)[self window].rootViewController;
    if( prepareStatus ) {
        [self rLog: @"Prepare Succeeded"];
        [controller cardIcon: @"up"];
    } else {
        [self rLog: @"Prepare Failed"];
        [controller cardIcon: @"error"];
    }
}


#pragma mark - App Delegate
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {

    self.applicationJustLaunched = YES;

    return YES;
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    
    [self rLog:@"applicationDidBecomeActive"];
    [[NSNotificationCenter defaultCenter] addObserver: self selector:@selector(gotSwipe:) name: LavuCardSwipeDataNotification object: NULL];
    [self performSelector:@selector(appIsNowActive) withObject:nil afterDelay:0.001];
}

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application {
    
    [self rLog:@"applicationDidReceiveMemoryWarning"];
}

- (void)applicationWillResignActive:(UIApplication *)application {
    [[NSNotificationCenter defaultCenter] removeObserver: self name: LavuCardSwipeDataNotification object: NULL];
    [self rLog:@"applicationWillResignActive"];
    
    [self.externalDeviceHub appWillResignActive];

    ViewController *viewController = (ViewController *)self.window.rootViewController;
    [viewController appWillResignActive];
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    
    [self rLog:@"applicationDidEnterBackground"];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    
    [self rLog:@"applicationWillTerminate"];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UITextFieldTextDidBeginEditingNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UITextViewTextDidBeginEditingNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
}

- (void)onScreenConsole:(NSString *)logString {
    
    ViewController *viewController = (ViewController *)self.window.rootViewController;
    [viewController onScreenConsole:logString];
}

#pragma mark POS Lavu AppDelegate Methods

- (void)appIsNowActive {
    
    // actual method does a bit more
    if (self.applicationJustLaunched) {
        
        self.applicationJustLaunched = NO;
        
        ExternalDeviceHub *edHub = [[ExternalDeviceHub alloc] init];
        self.externalDeviceHub = edHub;
        [edHub release];
        
        //[self.externalDeviceHub performSelector:@selector(initializeUniMag) withObject:nil afterDelay:1];
    }
}

#pragma mark POS Lavu Shared Methods (smai)

- (void)dcLog:(NSString *)logString {
    
    // actual method send logString to on screen debug console
    NSLog(@"%@", logString);
    
    [self onScreenConsole:logString];
}

- (void)rLog:(NSString *)logString {
    
    // actual method only sends logString to console when 'Save device console snapshot upon app launch:' location setting is not disabled
    NSLog(@"%@", logString);

    [self onScreenConsole:logString];
}

- (void)standardAlert:(NSString *)message withTitle:(NSString *)title {
    
    // actual method looks a lot like this
 	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:@"Continue" otherButtonTitles:nil];
	[alert show];
	[alert release];
}

- (void)standardAlert:(NSString *)message withTitle:(NSString *)title delegate:(id)delegate {
    
    // actual method looks a lot like this
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:delegate cancelButtonTitle:@"Continue" otherButtonTitles:nil];
	[alert show];
	[alert release];
}

- (NSString *)ttal:(NSString *)thisString {
    
    // actual method returns language pack translation
    return thisString;
}

#pragma mark

- (void)dealloc {
    
    [ccAmountToCheck release];
    
    [super dealloc];
}

@end
