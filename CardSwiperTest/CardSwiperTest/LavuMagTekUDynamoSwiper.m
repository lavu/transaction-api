//
//  LavuMagTeckUDynamoSwipper.m
//  CardSwiperTest
//
//  Created by Theodore Schnepper on 10/8/13.
//  Copyright (c) 2013 Lavu, Inc. All rights reserved.
//

#import "LavuMagTekUDynamoSwiper.h"

@implementation LavuMagTekUDynamoSwiper

- (id) init {
    self = [super init];
    [self setDeviceType: MAGTEKAUDIOREADER];
    return self;
}

+ (LavuCardSwiperType) getSwipperType {
    return LavuCardSwiperTypeAudioJack;
}

+ (NSString*) deviceName {
    return @"uDynamo";
}

+ (NSString*) imageName {
    return @"magtek_udynamo.jpg";
}
@end
