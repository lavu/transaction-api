//
//  LavuInfinitePeripherals.h
//  CardSwiperTest
//
//  Created by Theodore Schnepper on 10/25/13.
//  Copyright (c) 2013 Lavu, Inc. All rights reserved.
//

#import "LavuEAAccessorySwiper.h"
#import "DTDevices.h"
#import "LavuInfinitePeripheralsSwipeData.h"
#import "AppDelegate.h"

@interface LavuInfinitePeripheralsSwiper : LavuEAAccessorySwiper <DTDeviceDelegate> {
    DTDevices* _dtDevices;
    BOOL _isListening;
    AppDelegate* appDelegate;
    
    BOOL _msrPresent;
    BOOL _rfidPresent;
    BOOL _barcodePresent;
}

@end
