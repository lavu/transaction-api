//
//  LavuCardSwipeData.m
//  CardSwiperTest
//
//  Created by Theodore Schnepper on 10/8/13.
//  Copyright (c) 2013 Lavu, Inc. All rights reserved.
//

#import "LavuMagTekCardSwipeData.h"

@implementation LavuMagTekCardSwipeData
@synthesize responseType;
@synthesize trackStatus;
@synthesize cardStatus;
@synthesize encryptionStatus;
@synthesize batteryLevel;
@synthesize swipeCount;

@synthesize magnePrintEncrypted;
@synthesize magnePrintStatus;
@synthesize sessionID;

@synthesize PANLength;
@synthesize deviceSerialNumber;
@synthesize TLVCardIIN;
@synthesize MagTekSN;
@synthesize firmwarePartNumber;
@synthesize TLVVersion;
@synthesize deviceModelName;
@synthesize capabilityMSR;
@synthesize capabilityTracks;
@synthesize capabilityEncryption;

- (NSString*) toLavuGatewayString {
    NSMutableArray *magArray = [[[NSMutableArray alloc] init] autorelease];
    [magArray addObject: [NSString stringWithFormat: @"%@%@%@", [self track1String], [self track2String], [self track3String]]];
    [magArray addObject: [self encryptionStatus]];
    [magArray addObject: [self encryptedTrack1]];
    [magArray addObject: [self encryptedTrack2]];
    [magArray addObject: [self encryptedTrack3]];
    [magArray addObject: [self magnePrintStatus]];
    [magArray addObject: [self magnePrintEncrypted]];
    [magArray addObject: [self deviceSerialNumber]];
    [magArray addObject: [self sessionID]];
    @try{
        [magArray addObject: [self MagTekSN]];
    } @catch( NSException* e ){
        NSLog(@"%@", e);
    }
    NSString *swipeString = [magArray componentsJoinedByString:@"|"];
    return swipeString;
}
@end
