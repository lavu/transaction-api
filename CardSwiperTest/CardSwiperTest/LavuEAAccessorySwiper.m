	//
//  BlueBambooSwipper.m
//  CardSwiperTest
//
//  Created by Theodore Schnepper on 10/10/13.
//  Copyright (c) 2013 Lavu, Inc. All rights reserved.
//

#import "LavuEAAccessorySwiper.h"

@implementation LavuEAAccessorySwiper

- (id) init {
    self = [super init];
    [[NSNotificationCenter defaultCenter] addObserver: self selector:@selector(deviceConnected:) name: EAAccessoryDidConnectNotification object: NULL];
    [[NSNotificationCenter defaultCenter] addObserver: self selector:@selector(deviceDisconnected:) name: EAAccessoryDidDisconnectNotification object: NULL];
    [[EAAccessoryManager sharedAccessoryManager] registerForLocalNotifications];
    [self checkForDevice];
    return self;
}

- (void) dealloc {
    if(referencedAccessory){
        [referencedAccessory release];
    }
    
    if(session){
        [session release];
    }
    [super dealloc];
}

- (void) checkForDevice {
    NSArray* devices = [[EAAccessoryManager sharedAccessoryManager] connectedAccessories];
    for( int i = 0; i < [devices count]; i++ ){
        EAAccessory* accessory = [devices objectAtIndex: i];
        
        AppDelegate* appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        
        NSString* str = [NSString stringWithFormat: @"Name: %@, Manufacturer: %@, %@", [accessory name], [accessory manufacturer], [accessory protocolStrings]];
        NSLog(@"%@",str);
        [appDelegate performSelectorOnMainThread: @selector(rLog:) withObject: str waitUntilDone:FALSE];
        
        if( [[accessory manufacturer] isEqualToString: [[self class] getManufacturer]] && [[accessory name] isEqualToString: [[self class] getName]] && [[accessory protocolStrings] count] ){
            for( int j = 0; j < [[accessory protocolStrings] count]; j++ ){
                NSString* protocolString = [[accessory protocolStrings] objectAtIndex: j];
                if( [protocolString isEqualToString: [[self class] getProtocol]]){
                    if(referencedAccessory){
                        [referencedAccessory release];
                    }
                    referencedAccessory = [accessory retain];
                    //break;
                }
            }
        }
    }
}


+ (NSString*) getName {
    return @"";
}
+ (NSString*) getProtocol {
    return @"";
}
+ (NSString*) getManufacturer {
    return @"";
}

- (void) deviceConnected:(NSNotification *) notification {
#ifdef DEBUG
    NSLog(@"EAAccessory Connected");
#endif
    [self checkForDevice];
}

- (void) deviceDisconnected:(NSNotification *) notification {
#ifdef DEBUG
    NSLog(@"EAAccessory Disconnected");
#endif
    
}

- (void) stream: (NSStream *) stream handleEvent:(NSStreamEvent) streamEvent {

    if( streamEvent  == NSStreamEventNone ){
        return;
    }
    
    if( (streamEvent & NSStreamEventOpenCompleted) == NSStreamEventOpenCompleted ){
        ;//Swell
    }
    
    if( (streamEvent & NSStreamEventHasBytesAvailable) == NSStreamEventHasBytesAvailable ){
        if( stream == [session inputStream] ){
            //READ ALL DEM BYTES
            [self handleReadStream: [session inputStream]];
        }
    }
    
    if( (streamEvent & NSStreamEventHasSpaceAvailable) == NSStreamEventHasSpaceAvailable ){
        ;
    }
    
    if( (streamEvent & NSStreamEventErrorOccurred) == NSStreamEventErrorOccurred ){
        ;
    }
    
    if( (streamEvent & NSStreamEventEndEncountered) == NSStreamEventEndEncountered ){
        ;
    }
}

- (void) handleReadStream: (NSInputStream*) inputStream {
    //It does so much work! (To be extended by other devices)
}

- (void) connect {
    if( referencedAccessory && [referencedAccessory isConnected] ) {
        if(!session){
            session = [[EASession alloc] initWithAccessory: referencedAccessory forProtocol: [[self class] getProtocol]];
        }
    }
    if(_delegate){
        [_delegate didConnect: session != NULL forSwipper: self];
    }
}

- (BOOL) isConnected {
    return referencedAccessory && [referencedAccessory isConnected] && session;
}

- (void) disconnect {
    if(session){
        if( [self isListening] ){
            [self stopListening];
        }
        [session release];
        session = NULL;
    }
    
    if(_delegate){
        [_delegate didDisconnect: TRUE forSwipper: self];
    }
}

- (void) startListening {
    if(!session){
        if(_delegate){
            [_delegate didConnect: FALSE forSwipper: self];
            return;
        }
    }
    
    BOOL result = TRUE;
    
    NSStreamStatus inputStreamStatus = [[session inputStream] streamStatus];
    NSStreamStatus outputStreamStatus = [[session inputStream] streamStatus];
    
    //First Round, Connect if we have to.
    switch( inputStreamStatus ){
        case NSStreamStatusClosed :{
        }case NSStreamStatusNotOpen: {
            [[session inputStream] setDelegate: self];
            [[session inputStream] scheduleInRunLoop: [NSRunLoop currentRunLoop] forMode: NSDefaultRunLoopMode];
            [[session inputStream] open];
            break;
        } case NSStreamStatusOpening : {
        } case NSStreamStatusOpen : {
        } case NSStreamStatusReading : {
        } case NSStreamStatusWriting : {
            break;
        } case NSStreamStatusError : {
        } default : {
            result = result && FALSE;
        }
    }
    
    switch( outputStreamStatus ){
        case NSStreamStatusClosed :{
        }case NSStreamStatusNotOpen: {
            [[session outputStream] setDelegate: self];
            [[session outputStream] scheduleInRunLoop: [NSRunLoop currentRunLoop] forMode: NSDefaultRunLoopMode];
            [[session outputStream] open];
            break;
        } case NSStreamStatusOpening : {
        } case NSStreamStatusOpen : {
        } case NSStreamStatusReading : {
        } case NSStreamStatusWriting : {
            break;
        } case NSStreamStatusError : {
        } default : {
            result = result && FALSE;
        }
    }
    
    inputStreamStatus = [[session inputStream] streamStatus];
    outputStreamStatus = [[session inputStream] streamStatus];
    
    // SECOND ROUND TO ENSURE EVERYTHING WORKS
    switch( inputStreamStatus ){
        case NSStreamStatusOpening : {
        } case NSStreamStatusOpen : {
        } case NSStreamStatusReading : {
        } case NSStreamStatusWriting : {
            break;
        } case NSStreamStatusClosed :{
        } case NSStreamStatusNotOpen: {
        } case NSStreamStatusError : {
        } default : {
            result = result && FALSE;
        }
    }
    
    switch( outputStreamStatus ){
        case NSStreamStatusOpening : {
        } case NSStreamStatusOpen : {
        } case NSStreamStatusReading : {
        } case NSStreamStatusWriting : {
            break;
        } case NSStreamStatusClosed :{
        } case NSStreamStatusNotOpen: {
        } case NSStreamStatusError : {
        } default : {
            result = result && FALSE;
        }
    }
    
    if( result ){
    } else {
        [[session inputStream] setDelegate: NULL];
        [[session inputStream] close];
        
        [[session outputStream] setDelegate: NULL];
        [[session outputStream] close];
    }
    
    if(_delegate){
        [_delegate didStartListening:result forSwipper: self];
    }
}

- (BOOL) isListening {
    if( session ){
        NSStreamStatus inputStreamStatus = [[session inputStream] streamStatus];
        return inputStreamStatus == NSStreamStatusOpen || inputStreamStatus == NSStreamStatusOpening || inputStreamStatus == NSStreamStatusReading || inputStreamStatus == NSStreamStatusWriting;
    }
    return FALSE;
}

- (void) stopListening {
    if( session ){
        NSStreamStatus inputStreamStatus = [[session inputStream] streamStatus];
        if( inputStreamStatus == NSStreamStatusClosed ){
        } else {
            [[session inputStream] close];
        }
        
        NSStreamStatus outputStreamStatus = [[session outputStream] streamStatus];
        if( outputStreamStatus == NSStreamStatusClosed ){
        } else {
            [[session outputStream] close];
        }
        
        inputStreamStatus = [[session inputStream] streamStatus];
        outputStreamStatus = [[session outputStream] streamStatus];
        if(_delegate){
            [_delegate didStopListening: inputStreamStatus == NSStreamStatusClosed && outputStreamStatus == NSStreamStatusClosed forSwipper: self];
        }
        return;
    }
    if(_delegate){
        [_delegate didStopListening: TRUE forSwipper: self];
    }
}

- (void) prepareForSwipe {
    if(_delegate){
        [_delegate didPrepareForSwipe: TRUE forSwipper: self];
    }
}

+ (LavuCardSwiperType) getSwipperType {
    return LavuCardSwiperTypeAppleConnector;
}
@end
