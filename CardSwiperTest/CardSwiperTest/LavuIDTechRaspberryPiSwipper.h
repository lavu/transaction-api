//
//  LavuRaspberryPiSwipper.h
//  CardSwiperTest
//
//  Created by Theodore Schnepper on 10/24/13.
//  Copyright (c) 2013 Lavu, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LavuNullSwiper.h"
#import "IDTechCardSwipe.h"

enum {
    LavuRaspberryPiCommandUpdatePostStatus = 1,
    LavuRaspberryPiCommandPostStatusCheck = 2,
    LavuRaspberryPiCommandGetPost = 3,
    LavuRaspberryPiCommandCreatePost = 4,
    LavuRaspberryPiCommandPairDevices = 5,
    LavuRaspberryPiCommandUnPair = 6,
    LavuRaspberryPiCommandResetDB = 7,
    LavuRaspberryPiCommandUnpairAll = 8,
    LavuRaspberryPiCommandIsPaired = 9
} LavuRaspberryPiCommands;

@interface LavuIDTechRaspberryPiSwipper : LavuNullSwiper {
    const NSString* LavuRaspberryPiURLPath;
    NSTimeInterval LavuRaspberryPiConnectionTimeoutDuration;
    NSTimeInterval LavuRaspberryPiSwipeTimeoutDuration;
    
    int _ttl;
    BOOL _isConnected;
    BOOL _isListening;
    
    NSString* _networkAddress;
    BOOL _isSSL;
}

- (void) reportDidPrepareForSwipeSUCCESS;
- (void) reportDidPrepareForSwipeFAILURE;

@end
