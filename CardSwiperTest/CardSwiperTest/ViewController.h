//
//  ViewController.h
//  CardSwiperTest
//
//  Created by Richard Gieske on 10/4/13.
//  Copyright (c) 2013 Lavu, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AppDelegate;

@interface ViewController : UIViewController <UIScrollViewDelegate> {
    
    AppDelegate *appDelegate;
    
    float consoleOutputHeight;
}

@property (nonatomic, retain) IBOutlet UILabel *connectedDevices;
@property (nonatomic, retain) IBOutlet UILabel *lastSwipeReader;
@property (nonatomic, retain) IBOutlet UILabel *lastSwipeString;
@property (nonatomic, retain) IBOutlet UILabel *lastSwipeHex;

@property (nonatomic, retain) IBOutlet UIImageView *readerStatusIcon;
@property (nonatomic, retain) IBOutlet UIActivityIndicatorView *readerActivityIndicator;

@property (nonatomic, retain) IBOutlet UIButton *startStopButton;

@property (readwrite, assign) BOOL isListening;

@property (nonatomic, retain) IBOutlet UIScrollView *consoleScrollview;

- (IBAction)reinitialize;
- (IBAction)startStopListening;
- (NSString *)getCurrentDateTime:(NSString *)dateTimeFormat forceUS:(BOOL)forceUS;
- (void)onScreenConsole:(NSString *)logString;
- (IBAction)clearConsole;
- (void)appWillResignActive;

- (void)cardIcon:(NSString *)state;

@end
