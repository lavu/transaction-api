//
//  IDTechCardSwipe.m
//  CardSwiperTest
//
//  Created by Theodore Schnepper on 10/17/13.
//  Copyright (c) 2013 Lavu, Inc. All rights reserved.
//

#import "IDTechCardSwipe.h"

@implementation IDTechCardSwipe


- (id) initWithData: (NSData*) swipeData {
    self = [self init];
    NSString* swipeStr;
    
    const char* bytes = [swipeData bytes];
    if( bytes[0] == 0x02 ){
        _encrypted = TRUE;
        [self setEncryptedTrack1: @""];
        [self setEncryptedTrack2: @""];
        [self setEncryptedTrack3: @""];
        
        if( '\x80' != bytes[3] ){ //Should Always Be True
            return self;
        }
        
        //int totalLength = (bytes[2] << 8) + bytes[1];
        //char trackStatus = bytes[4];
        int track1UnencryptedLength = bytes[5];
        int track2UnencryptedLength = bytes[6];
        int track3UnencryptedLength = bytes[7];
        int encryptedTrack1Length = track1UnencryptedLength;
        if( track1UnencryptedLength & 0x07 ){ // Round to the next byte
            encryptedTrack1Length -= track1UnencryptedLength & 0x07;
            encryptedTrack1Length += 0x08;
        }
        int encryptedTrack2Length = track2UnencryptedLength;
        if( track2UnencryptedLength & 0x07 ){
            encryptedTrack2Length -= track2UnencryptedLength & 0x07;
            encryptedTrack2Length += 0x08;
        }
        int encryptedTrack3Length = track3UnencryptedLength;
        if( track3UnencryptedLength & 0x07 ){
            encryptedTrack3Length -= track3UnencryptedLength & 0x07;
            encryptedTrack3Length += 0x08;
        }
        
        char fieldByte1 = bytes[8];
        char fieldByte2 = bytes[9];
        int i = 10;
        //Masked Tracks
        if( (fieldByte1 & IDTechTrack1ClearMaskDataPresent) == IDTechTrack1ClearMaskDataPresent ){ // Masked Track 1 Available
            [self setTrack1String: [[[NSString alloc] initWithData: [swipeData subdataWithRange: NSMakeRange( i, track1UnencryptedLength)] encoding: NSUTF8StringEncoding] autorelease]];
            i += track1UnencryptedLength;
        }
        if( (fieldByte1 & IDTechTrack2ClearMaskDataPresent) == IDTechTrack2ClearMaskDataPresent ){ // Masked Track 2 Available
            [self setTrack2String: [[[NSString alloc] initWithData: [swipeData subdataWithRange: NSMakeRange( i, track2UnencryptedLength)] encoding: NSUTF8StringEncoding] autorelease]];
            i += track2UnencryptedLength;
        }
        if( (fieldByte1 & IDTechTrack3ClearMaskDataPresent) == IDTechTrack3ClearMaskDataPresent ){ // Masked Track 3 Available
            [self setTrack3String: [[[NSString alloc] initWithData: [swipeData subdataWithRange: NSMakeRange( i, track3UnencryptedLength)] encoding: NSUTF8StringEncoding] autorelease]];
            i += track3UnencryptedLength;
        }
        
        //Encrypted Tracks
        if( (fieldByte2 & IDTechTrack1EncryptedDataPresent ) == IDTechTrack1EncryptedDataPresent ){
            [self setEncryptedTrack1: [[[NSString alloc] initWithData: [swipeData subdataWithRange: NSMakeRange( i, encryptedTrack1Length)] encoding: NSUTF8StringEncoding] autorelease]];
            i += encryptedTrack1Length;
        }
        
        if( (fieldByte2 & IDTechtrack2EncryptedDataPresent ) == IDTechtrack2EncryptedDataPresent ){
            [self setEncryptedTrack2: [[[NSString alloc] initWithData: [swipeData subdataWithRange: NSMakeRange( i, encryptedTrack2Length)] encoding: NSUTF8StringEncoding] autorelease]];
            i += encryptedTrack2Length;
        }
        
        if( (fieldByte2 & IDTechtrack3EncryptedDataPresent ) == IDTechtrack3EncryptedDataPresent ){
            [self setEncryptedTrack3: [[[NSString alloc] initWithData: [swipeData subdataWithRange: NSMakeRange( i, encryptedTrack3Length)] encoding: NSUTF8StringEncoding] autorelease]];
            i += encryptedTrack3Length;
        }
        
        //Serial Number
        if( (fieldByte1 & IDTechSerialNumberIsAvailable) == IDTechSerialNumberIsAvailable ){
            
            i += 10;
        }
        //KSN
        if( (fieldByte2 & IDTechKSNPresent) == IDTechKSNPresent ){
            i += 10;
        }
        
        //LRC
        i += 1;
        //CheckSum
        i += 1;
        //ETX
        i += 1;
        swipeStr = [NSString stringWithFormat: @"%@%@%@", [self track1String], [self track2String], [self track3String]];
    } else {
        swipeStr = [[[NSString alloc] initWithData: swipeData encoding: NSASCIIStringEncoding] autorelease];
    }
    [self parseTracks: swipeStr];
    
    return self;
}

- (BOOL) isEncrypted {
    return _encrypted;
}

- (NSString*) toLavuGatewayString {
    //if(_encrypted){
    //    return [NSString stringWithFormat: @"%@", [self totalTrack]];
    //} else {
        return [NSString stringWithFormat: @"%@%@%@", [self track1String], [self track2String], [self track3String]];
    //}
}

@end
