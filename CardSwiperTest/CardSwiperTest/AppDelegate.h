//
//  AppDelegate.h
//  CardSwiperTest
//
//  Created by Richard Gieske on 10/4/13.
//  Copyright (c) 2013 Lavu, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ExternalDeviceHub.h"
#import "LavuSwiperProtocol.h"
#import "LavuSwiperDelegate.h"
#import "LavuCardDataProtocol.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate, LavuSwiperDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (retain, nonatomic) NSObject<LavuSwiperProtocol>* swipper;

// POS Lavu AppDelegate Properties

@property (nonatomic, retain) ExternalDeviceHub *externalDeviceHub;

// POS Lavu Shared Properties (smai)

@property (readwrite, assign) BOOL applicationJustLaunched;
@property (readwrite, assign) BOOL nowSendingSwipeData;
@property (readwrite, assign) BOOL nowShowingSignatureShade;
@property (nonatomic, retain) NSString *ccAmountToCheck;

// POS Lavu Shared Methods

- (void)dcLog:(NSString *)logString;
- (void)rLog:(NSString *)logString;
- (void)standardAlert:(NSString *)message withTitle:(NSString *)title;
- (void)standardAlert:(NSString *)message withTitle:(NSString *)title delegate:(id)delegate;
- (NSString *)ttal:(NSString *)thisString;

- (void) gotSwipe: (NSNotification*) notification;

@end
