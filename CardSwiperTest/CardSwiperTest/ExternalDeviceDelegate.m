//
//  ExternalDeviceDelegate.m
//  POSLavu
//
//  Created by Richard Gieske on 3/28/13.
//
//

#import "ExternalDeviceDelegate.h"
#import "AppDelegate.h"
#import "ExternalDeviceHub.h"

@implementation ExternalDeviceDelegate

@synthesize protocolString, deviceName, accessory;
@synthesize hasCardReader, hasChipAndPIN, hasScanner, hasPrinter, connected;
@synthesize track1String, track2String, track3String, cardSwipeString, cardSwipeDataString;
@synthesize printString;

- (id)init:(NSString *)reader protocol:(NSString *)protocol accessory:(EAAccessory *)accessory_ {
    
    self = [super init];
    if (self) {
        
        appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        smai = appDelegate;
        edHub = appDelegate.externalDeviceHub;
        
        [smai rLog:[NSString stringWithFormat:@"init ExternalDeviceDelegate: %@ %@", reader, protocol]];
        
        self.deviceName = reader;
        self.protocolString = protocol;
        self.accessory = accessory_;
        
        self.hasCardReader = ([[NSArray arrayWithObjects:@"bluebamboo_p25i", @"bluebamboo_p200", @"idtech_imag", @"idtech_unimag", @"paysaber", nil] containsObject:self.deviceName] || ([protocol rangeOfString:@"com.datecs.linea.pro.msr"].location != NSNotFound));
        self.hasChipAndPIN = [self.deviceName isEqualToString:@"bluebamboo_p200"];
        self.hasScanner = ([protocol rangeOfString:@"com.datecs.linea.pro.bar"].location != NSNotFound);
        self.hasPrinter = [[NSArray arrayWithObjects:@"bluebamboo_p25i", @"bluebamboo_p200", @"tsp650ii", nil] containsObject:self.deviceName];
        
        [self dcLog:[NSString stringWithFormat:@"hasCardReader: %i", self.hasCardReader]];
        [self dcLog:[NSString stringWithFormat:@"hasScanner: %i", self.hasScanner]];
        
        if (self.accessory) {
            [self.accessory setDelegate:self];
            self.connected = YES;
        }
        
        cardReadFailed = NO;
        self.track1String = @"";
        self.track2String = @"";
        self.track3String = @"";
        self.cardSwipeString = @"";
        self.cardSwipeDataString = @"";
        
        testStringWriteAttempts = 0;
        printMode = 0;
        self.printString = @"";
        printBytesToWrite = 0;
        printBytesWritten = 0;
        
        interceptPrinterSettings = [[NSMutableArray alloc] init];
        
        if ([self.deviceName isEqualToString:@"idtech_unimag"]) self.connected = YES;
        else if ([self.deviceName isEqualToString:@"linea"]) {
            
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(batteryDidChange) name:UIDeviceBatteryStateDidChangeNotification object:nil];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(batteryDidChange) name:UIDeviceBatteryLevelDidChangeNotification object:nil];
            
            [self connectLineaPro];
            
        } else if ([self.deviceName isEqualToString:@"tsp650ii"]) {
            
            printMode = 1;
            self.printString = @"Hello Lavu!";
            printBytesToWrite = [self.printString length];
            printBytesWritten = 0;
            
            [self startSession];
            
        } else if ([edHub shouldListen]) [self startListening];
        
        if (self.hasCardReader) {
            //if ([smai.activePOSview isEqualToString:@"diningRoom"]) [appDelegate.diningRoomView performSelectorOnMainThread:@selector(showTabSearchButton) withObject:nil waitUntilDone:NO];
            [edHub performSelectorOnMainThread:@selector(cardIcon:) withObject:@"up" waitUntilDone:NO];
        }
    }
    
    return self;
}

- (void)dcLog:(NSString *)message {
    
    [appDelegate performSelectorOnMainThread:@selector(dcLog:) withObject:message waitUntilDone:NO];
}

- (void)startListening {
    
    [smai rLog:@"startListening (edd)"];
    
    if ([self.deviceName isEqualToString:@"bluebamboo_p25i"]) [self startBlueBamboo];
    else if (![self.deviceName isEqualToString:@"linea"]) [self startSession];
}

- (void)stopListening {
    
    if (![self.deviceName isEqualToString:@"linea"]) [self closeStreams];
}

- (void)startSession {
    
    if (self.accessory && self.accessory.connected) {
        
        [smai rLog:[NSString stringWithFormat:@"DEVICE CONNECTED: %@ %@", self.deviceName, self.protocolString]];
        
        [smai rLog:[NSString stringWithFormat:@"connected: %i - connectionID: %i", self.accessory.connected, self.accessory.connectionID]];
        
        [smai rLog:[NSString stringWithFormat:@"Xsession: %@", session]];
        
        if (session == nil) {
            [smai rLog:@"Xsession alloc"];
            session = [[EASession alloc] initWithAccessory:self.accessory forProtocol:self.protocolString];
        }
        
        [smai rLog:[NSString stringWithFormat:@"Xsession: %@", session]];
        
        if (session) {
            
            [smai rLog:@"Xsession is valid"];
            
            self.connected = YES;
            if (self.hasCardReader) {
                //if ([smai.activePOSview isEqualToString:@"diningRoom"]) [appDelegate.diningRoomView performSelectorOnMainThread:@selector(showTabSearchButton) withObject:nil waitUntilDone:NO];
                [edHub performSelectorOnMainThread:@selector(cardIcon:) withObject:@"up" waitUntilDone:NO];
            }
            [self openStreams];
            
        } else {
            
            [smai rLog:@"Xsession is not valid"];
            [self dcLog:@"Xsession is not valid"];
            
            self.connected = NO;
            if (self.hasCardReader && ![edHub hasCardReader]) {
                //if ([smai.activePOSview isEqualToString:@"diningRoom"]) [appDelegate.diningRoomView performSelectorOnMainThread:@selector(hideTabSearchButton) withObject:nil waitUntilDone:NO];
                [edHub performSelectorOnMainThread:@selector(cardIcon:) withObject:@"error" waitUntilDone:NO];
            }
        }
        
    } else {
        
        [smai rLog:[NSString stringWithFormat:@"ERROR: %@ failed to connect", self.deviceName]];
        [self dcLog:[NSString stringWithFormat:@"ERROR: %@ failed to connect", self.deviceName]];
        
        self.connected = NO;
    }
}

- (void)openStreams {
    
    [smai rLog:@"openStreams"];
	//[self dcLog:@"openStreams"];
    
    [[session inputStream] setDelegate:self];
    [[session inputStream] scheduleInRunLoop:[NSRunLoop mainRunLoop] forMode:NSDefaultRunLoopMode];
    [[session inputStream] open];
    [[session outputStream] setDelegate:self];
    [[session outputStream] scheduleInRunLoop:[NSRunLoop mainRunLoop] forMode:NSDefaultRunLoopMode];
    [[session outputStream] open];
    
    [smai rLog:[NSString stringWithFormat:@"outputStream delegate: %@", [[session outputStream] delegate]]];
    [smai rLog:[NSString stringWithFormat:@"inputStream delegate: %@", [[session inputStream] delegate]]];
    
    [smai rLog:[NSString stringWithFormat:@"mainRunLoop mode: %@", [[NSRunLoop mainRunLoop] currentMode]]];
    //[self dcLog:[NSString stringWithFormat:@"mainRunLoop mode: %@", [[NSRunLoop mainRunLoop] currentMode]]];
    [smai rLog:[NSString stringWithFormat:@"Stream Status: Input: %i %@  Output: %i %@", [[session inputStream] streamStatus], [[session inputStream] streamError], [[session outputStream] streamStatus], [[session outputStream] streamError]]];
    //[self dcLog:[NSString stringWithFormat:@"Stream Status: Input: %i %@  Output: %i %@", [[session inputStream] streamStatus], [[session inputStream] streamError], [[session outputStream] streamStatus], [[session outputStream] streamError]]];
    
    if ([self.deviceName rangeOfString:@"bluebamboo"].location != NSNotFound) {
        testStringWritten = NO;
        [smai rLog:[NSString stringWithFormat:@"testStringWritten: %i", testStringWritten]];
        //[self dcLog:[NSString stringWithFormat:@"testStringWritten: %i", testStringWritten]];
        [NSTimer scheduledTimerWithTimeInterval:1.5 target:self selector:@selector(checkBlueBambooWrite) userInfo:nil repeats:NO];
    }
    
    if (([[session inputStream] streamStatus] != NSStreamStatusOpen) || ![[session inputStream] delegate]) {
        [smai rLog:@"STREAM ERROR DETECTED"];
        //[self dcLog:@"STREAM ERROR DETECTED"];
        self.connected = NO;
        if (self.hasCardReader && ![edHub hasCardReader]) {
            //if ([smai.activePOSview isEqualToString:@"diningRoom"]) [appDelegate.diningRoomView performSelectorOnMainThread:@selector(hideTabSearchButton) withObject:nil waitUntilDone:NO];
            [edHub performSelectorOnMainThread:@selector(cardIcon:) withObject:@"error" waitUntilDone:NO];
        }
    }
}

- (void)closeStreams {
    
    [smai rLog:@"closeStreams"];
	[self dcLog:@"closeStreams"];
    
    if (session) {
        [self dcLog:[NSString stringWithFormat:@"session: %i", [session retainCount]]];
        [[session inputStream] close];
        [[session inputStream] removeFromRunLoop:[NSRunLoop mainRunLoop] forMode:NSDefaultRunLoopMode];
        [[session inputStream] setDelegate:nil];
        [[session outputStream] close];
        [[session outputStream] removeFromRunLoop:[NSRunLoop mainRunLoop] forMode:NSDefaultRunLoopMode];
        [[session outputStream] setDelegate:nil];
        [session release];
        session = nil;
    }
}

- (void)stream:(NSStream *)theStream handleEvent:(NSStreamEvent)streamEvent {
    
    [smai rLog:[NSString stringWithFormat:@"STREAM EVENT - device: %@, nowSendingSwipeData: %i, nowShowingSignatureShade: %i, switcher: %i", self.deviceName, smai.nowSendingSwipeData, smai.nowShowingSignatureShade, switcher]];
	//[self dcLog:[NSString stringWithFormat:@"STREAM EVENT - nowSendingSwipeData: %i,  nowShowingSignatureShade: %i, switcher: %i", smai.nowSendingSwipeData, smai.nowShowingSignatureShade, switcher]];
    [smai rLog:[NSString stringWithFormat:@"ccAmountToCheck: %@", smai.ccAmountToCheck]];
    //[self dcLog:[NSString stringWithFormat:@"ccAmountToCheck: %@", smai.ccAmountToCheck]];
    
    switch (streamEvent) {
            
        case NSStreamEventNone:
            
            [smai rLog:@"NSStreamEventNone"];
            break;
            
        case NSStreamEventOpenCompleted:
            
            [smai rLog:@"NSStreamEventOpenCompleted"];
            self.cardSwipeString = @"";
            break;
            
        case NSStreamEventHasBytesAvailable:
            
            [smai rLog:@"NSStreamEventHasBytesAvailable"];
            
            if (!smai.nowSendingSwipeData && !smai.nowShowingSignatureShade) {
                
                self.track1String = @"";
                self.track2String = @"";
                self.track3String = @"";
                
                uint8_t buf[1024];
                unsigned int len = 0;
                len = [(NSInputStream *)theStream read:buf maxLength:1024];
                
                //NSLog(@"buffer: %s", buf);
                //NSLog(@"len: %i", len);
                
                //if ([self.deviceName isEqualToString:@"bluebamboo_p25i"]) {
                if ([self.deviceName rangeOfString:@"bluebamboo"].location != NSNotFound) {
                    
                    if (printMode > 0) {
                        
                        if (buf[4]==0x03 && buf[5]==0x00) [smai rLog:@"Successful write..."];
                        else [smai rLog:@"Possible write error..."];
                        
                        [smai rLog:[NSString stringWithFormat:@"printBytesWritten: %i, printBytesToWrite: %i", printBytesWritten, printBytesToWrite]];
                        
                        if (printBytesWritten >= printBytesToWrite) {
                            printMode = 0;
                            //[printJobSender BBprintIsDone:1];
                        }
                        
                        if ([edHub shouldListen]) [self startBlueBamboo];
                        
                    } else {
                        
                        BOOL operationFailed = YES;
                        BOOL cardReadGood = NO;
                        
                        if (len > 0) {
                            
                            //if (buf[4]==0x84 && buf[5]==0x04) {
                            
                            //operationFailed = NO;
                            //}
                            
                            if (buf[4]==0x84 && buf[5]==0x00) {
                                
                                operationFailed = NO;
                                
                                for (int i = 6; i < len; i++) {
                                    
                                    //get first track data
                                    if (buf[i] == 0x31) {
                                        
                                        i++;
                                        NSString *stringLength = @"";
                                        int inttext = 0;
                                        int track1length = 0;
                                        stringLength = [NSString stringWithFormat:@"%x", buf[i]];
                                        inttext = ([stringLength intValue] - 30) * 1000;
                                        track1length = track1length + inttext;
                                        i++;
                                        stringLength = [NSString stringWithFormat:@"%x", buf[i]];
                                        inttext=([stringLength intValue]-30) * 100;
                                        track1length = track1length + inttext;
                                        i++;
                                        stringLength = [NSString stringWithFormat:@"%x", buf[i]];
                                        inttext = ([stringLength intValue] - 30) * 10;
                                        track1length = track1length + inttext;
                                        i++;
                                        stringLength = [NSString stringWithFormat:@"%x", buf[i]];
                                        inttext = ([stringLength intValue] - 30) * 1;
                                        track1length = track1length + inttext;
                                        i++;
                                        
                                        if (track1length > 0) {
                                            
                                            cardReadGood = YES;
                                            
                                            unsigned char buffer[track1length];
                                            NSMutableData *data = [[NSMutableData alloc] init];
                                            
                                            for (int j = 0; j < track1length; j++) {
                                                buffer[j]=buf[i];
                                                i++;
                                            }
                                            [data appendBytes:buffer length:track1length];
                                            NSString *stringtest = [[NSString alloc] initWithData:(NSData *)data encoding:NSASCIIStringEncoding];
                                            
                                            NSString *crr = self.track1String;
                                            self.track1String = [crr stringByAppendingString:stringtest];
                                            [data release];
                                            [stringtest release];
                                            
                                        } else if (track1length == 0) self.track1String = @"";
                                    }
                                    
                                    //get track 2 data
                                    if (buf[i] == 0x32) {
                                        
                                        i++;
                                        NSString *stringLength = @"";
                                        int inttext=0;
                                        int track2length=0;
                                        
                                        stringLength = [NSString stringWithFormat:@"%x:",buf[i]];
                                        inttext = ([stringLength intValue] - 30) * 1000;
                                        track2length = track2length + inttext;
                                        i++;
                                        stringLength = [NSString stringWithFormat:@"%x:",buf[i]];
                                        inttext = ([stringLength intValue] - 30) * 100;
                                        track2length = track2length + inttext;
                                        i++;
                                        stringLength = [NSString stringWithFormat:@"%x:",buf[i]];
                                        inttext = ([stringLength intValue] - 30) * 10;
                                        track2length = track2length + inttext;
                                        i++;
                                        stringLength = [NSString stringWithFormat:@"%x:",buf[i]];
                                        inttext = ([stringLength intValue] - 30) * 1;
                                        track2length = track2length + inttext;
                                        i++;
                                        
                                        if (track2length > 0) {
                                            
                                            cardReadGood = YES;
                                            
                                            unsigned char buffer[track2length];
                                            NSMutableData *data = [[NSMutableData alloc] init];
                                            for (int j = 0;j < track2length; j++) {
                                                buffer[j]= buf[i];
                                                i++;
                                            }
                                            [data appendBytes:buffer length:track2length];
                                            NSString *stringtest = [[NSString alloc] initWithData:(NSData *)data encoding:NSASCIIStringEncoding];
                                            
                                            NSString *crr = self.track2String;
                                            self.track2String = [crr stringByAppendingString:stringtest];
                                            [data release];
                                            [stringtest release];
                                            
                                        } else if (track2length == 0) self.track2String = @"";
                                    }
                                    
                                    //get track 3 data
                                    if (buf[i] == 0x33) {
                                        
                                        i++;
                                        NSString *stringLength = @"";
                                        int inttext = 0;
                                        int track3length = 0;
                                        stringLength = [NSString stringWithFormat:@"%x:",buf[i]];
                                        inttext = ([stringLength intValue] - 30) * 1000;
                                        track3length = track3length + inttext;
                                        i++;
                                        stringLength = [NSString stringWithFormat:@"%x:",buf[i]];
                                        inttext = ([stringLength intValue] - 30) * 100;
                                        track3length = track3length + inttext;
                                        i++;
                                        stringLength = [NSString stringWithFormat:@"%x:",buf[i]];
                                        inttext = ([stringLength intValue] - 30) * 10;
                                        track3length = track3length + inttext;
                                        i++;
                                        stringLength = [NSString stringWithFormat:@"%x:",buf[i]];
                                        inttext = ([stringLength intValue] - 30) * 1;
                                        track3length = track3length + inttext;
                                        i++;
                                        
                                        if (track3length > 0) {
                                            
                                            cardReadGood = YES;
                                            
                                            unsigned char buffer[track3length];
                                            NSMutableData *data = [[NSMutableData alloc] init];
                                            for (int j = 0; j < track3length; j++) {
                                                buffer[j]= buf[i];
                                                i++;
                                            }
                                            [data appendBytes:buffer length:track3length];
                                            NSString *stringtest = [[NSString alloc] initWithData:(NSData *)data encoding:NSASCIIStringEncoding];
                                            
                                            NSString *crr = self.track3String;
                                            self.track3String = [crr stringByAppendingString:stringtest];
                                            [data release];
                                            [stringtest release];
                                            
                                        } else if (track3length == 0) self.track3String = @"";
                                        
                                        break;
                                    }
                                }
                                
                                self.cardSwipeString = [NSString stringWithFormat:@"%@%@%@", self.track1String, self.track2String, self.track3String];
                                if ([self.cardSwipeString length] < 10) operationFailed = YES;
                                
                            } else operationFailed = YES;
                            
                        } else operationFailed = YES;
                        
                        if (cardReadGood) [edHub processSwipe:self.cardSwipeString dataString:self.cardSwipeDataString reader:self.deviceName encrypted:NO];
                        else if (operationFailed) {
                            cardReadFailed = YES;
                            [smai standardAlert:[smai ttal:@"Please try again."] withTitle:[smai ttal:@"Card Read Failed"] delegate:self];
                        }
                        
                        if (operationFailed || cardReadGood) [self startBlueBamboo];
                    }
                    
                } else if ([self.deviceName isEqualToString:@"tsp650ii"]) {
                    
                    NSData *data = [NSData dataWithBytes:buf length:len];
                    [smai rLog:[NSString stringWithFormat:@"data: %@", data]];
                    
                    NSString *string = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
                    [smai rLog:[NSString stringWithFormat:@"string: %@", string]];
                    [string release];
                    
                    if (printMode > 0) {
                        
                        [smai rLog:[NSString stringWithFormat:@"printBytesWritten: %i, printBytesToWrite: %i", printBytesWritten, printBytesToWrite]];
                        
                        if (printBytesWritten >= printBytesToWrite) {
                            printMode = 0;
                            //[printJobSender BBprintIsDone:1];
                        }
                    }
                    
                } else {
                    
                    if (len > 0) {
                        
                        NSData *data = [NSData dataWithBytes:buf length:len];
                        [self dcLog:[NSString stringWithFormat:@"data: %@", data]];
                        
                        NSString *magString = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
                        [smai rLog:[NSString stringWithFormat:@"string: %@", magString]];
                        [self dcLog:[NSString stringWithFormat:@"string: %@", magString]];
                        
                        BOOL encrypted = NO;
                        self.cardSwipeString = magString;
                        
                        if ([magString length] > 0) {
                            if (![[magString substringToIndex:1] isEqualToString:@"%"]) {
                                encrypted = YES;
                                NSString *dataString = [NSString stringWithFormat:@"%@", data];
                                dataString = [dataString stringByReplacingOccurrencesOfString:@"<" withString:@""];
                                dataString = [dataString stringByReplacingOccurrencesOfString:@">" withString:@""];
                                self.cardSwipeDataString = [dataString stringByReplacingOccurrencesOfString:@" " withString:@""];
                                
                                NSRange range = [magString rangeOfString:@"%*"];
                                if (range.location != NSNotFound) self.cardSwipeString = [magString substringFromIndex:(range.location + 2)];
                            }
                        }
                        
                        [self dcLog:[NSString stringWithFormat:@"magString: %@", magString]];
                        
                        [edHub processSwipe:self.cardSwipeString dataString:self.cardSwipeDataString reader:self.deviceName encrypted:encrypted];
                        
                        [magString release];
                        
                    } else {
                        
                        NSError *error = [(NSInputStream *)theStream streamError];
                        [smai rLog:[NSString stringWithFormat:@"Error -- %@", [error localizedDescription]]];
                        error = nil;
                    }
                }
                
            } else [smai rLog:@"Ignoring available bytes"];
            
        case NSStreamEventHasSpaceAvailable:
            
            [smai rLog:@"NSStreamEventHasSpaceAvailable"];
            
            //if ([self.deviceName isEqualToString:@"bluebamboo_p25i"] && switcher) {
            if (([self.deviceName rangeOfString:@"bluebamboo"].location != NSNotFound) && switcher) {
                switcher = NO;
                [self writeToBlueBamboo];
            }
            
            if ([self.deviceName isEqualToString:@"tsp650ii"]) {
                
                [self writeToStarTSP650II];
            }
            
            break;
            
        case NSStreamEventErrorOccurred:
            
            [smai rLog:@"NSStreamEventErrorOccurred"];
            //[self dcLog:@"NSStreamEventErrorOccurred"];
            
            if ([self.deviceName rangeOfString:@"bluebamboo"].location != NSNotFound) {
                [self startBlueBamboo];
                if (printMode == 0) {
                    cardReadFailed = YES;
                    [smai standardAlert:[smai ttal:@"NSStream Error"] withTitle:[smai ttal:@"ERROR:"] delegate:self];
                }
            } else {
                [self closeStreams];
                [self startSession];
            }
            
            break;
            
        default:
            
            [smai rLog:@"Unknown NSStream event"];
            break;
    }
}

#pragma mark BlueBamboo

- (void)startBlueBamboo {
    
    //[NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(startBlueBambooExec) userInfo:nil repeats:NO];
    [self performSelector:@selector(startBlueBambooExec) withObject:nil afterDelay:0.5];
}

- (void)startBlueBambooExec {
	
	[smai rLog:@"startBlueBambooExec"];
	//[self dcLog:@"startBlueBambooExec"];
	switcher = YES;
    
	[self closeStreams];
    [self startSession];
}

- (void)writeToBlueBamboo {
	
    [smai rLog:@"writeToBlueBamboo"];
	//[self dcLog:@"writeToBlueBamboo"];
    
	[smai rLog:[NSString stringWithFormat:@"mainRunLoop mode: %@", [[NSRunLoop mainRunLoop] currentMode]]];
	//[self dcLog:[NSString stringWithFormat:@"mainRunLoop mode: %@", [[NSRunLoop mainRunLoop] currentMode]]];
    [smai rLog:[NSString stringWithFormat:@"Stream Status: Input: %i %@  Output: %i %@", [[session inputStream] streamStatus], [[session inputStream] streamError], [[session outputStream] streamStatus], [[session outputStream] streamError]]];
	//[self dcLog:[NSString stringWithFormat:@"Stream Status: Input: %i %@  Output: %i %@", [[Xsession inputStream] streamStatus], [[Xsession inputStream] streamError], [[Xsession outputStream] streamStatus], [[Xsession outputStream] streamError]]];
    
	// send read MSR commands 55 66 77 88 is Commands flag and 48 is MSR command 14 is hex value of timeout. unit is second
    
    if (printMode == 1) {
        
        int writeLen = (printBytesToWrite - printBytesWritten);
        if (writeLen > 2048) writeLen = 2048;
        [smai rLog:[NSString stringWithFormat:@"writeLen: %i", writeLen]];
        
        unsigned char buffer[(writeLen + 5)];
        buffer[0] = 0X55; buffer[1]=0x66; buffer[2]=0x77; buffer[3]=0x88; buffer[4]=0x44;
        for (int i = printBytesWritten; i < (printBytesWritten + writeLen); i++) {
            buffer[(i + 5)] = [self.printString characterAtIndex:i];
            //NSLog(@"%i : %c - %i", i, buffer[(i + 5)], buffer[(i + 5)]);
        }
        
        //NSLog(@"buffer: %s", buffer);
        
        int bytesWritten = [[session outputStream] write:(const uint8_t *)buffer maxLength:(writeLen + 5)];
        printBytesWritten += (bytesWritten - 5);
        
        [smai rLog:[NSString stringWithFormat:@"bytesWritten: %i", (bytesWritten - 5)]];
        [smai rLog:[NSString stringWithFormat:@"printBytesWritten: %i", printBytesWritten]];
        
    } else {
        
        unsigned char buffer[6];
        buffer[0] = 0x55; buffer[1]=0x66; buffer[2]=0x77; buffer[3]=0x88; buffer[4]=0x48; buffer[5]=0x00;
        [[session outputStream] write:(const uint8_t *)buffer maxLength:6];
    }
    
	[smai rLog:[NSString stringWithFormat:@"mainRunLoop mode: %@", [[NSRunLoop mainRunLoop] currentMode]]];
	//[self dcLog:[NSString stringWithFormat:@"mainRunLoop mode: %@", [[NSRunLoop mainRunLoop] currentMode]]];
    [smai rLog:[NSString stringWithFormat:@"Stream Status: Input: %i %@  Output: %i %@", [[session inputStream] streamStatus], [[session inputStream] streamError], [[session outputStream] streamStatus], [[session outputStream] streamError]]];
	//[self dcLog:[NSString stringWithFormat:@"Stream Status: Input: %i %@  Output: %i %@", [[Xsession inputStream] streamStatus], [[Xsession inputStream] streamError], [[Xsession outputStream] streamStatus], [[Xsession outputStream] streamError]]];
    
	testStringWritten = YES;
    testStringWriteAttempts = 0;
	[smai rLog:[NSString stringWithFormat:@"testStringWritten: %i", testStringWritten]];
	//[self dcLog:[NSString stringWithFormat:@"testStringWritten: %i", testStringWritten]];
}

- (void)checkBlueBambooWrite {
	
	[smai rLog:[NSString stringWithFormat:@"checkBlueBambooWrite: %i %i", testStringWritten, testStringWriteAttempts]];
	//[self dcLog:[NSString stringWithFormat:@"checkBlueBambooWrite: %i %i", testStringWritten, testStringWriteAttempts]];
	if (!testStringWritten) {
        testStringWriteAttempts++;
        if (testStringWriteAttempts >= 10) {
            if (printMode > 0) {
                printMode = 0;
                //[printJobSender BBprintIsDone:0];
                if ([edHub shouldListen]) [self startBlueBamboo];
            } else {
                [smai standardAlert:@"A connection cannot be established. Please try disconnecting and reconnecting the device." withTitle:@"Card Reader Error"];
                self.connected = NO;
                if (self.hasCardReader && ![edHub hasCardReader]) {
                    //if ([smai.activePOSview isEqualToString:@"diningRoom"]) [appDelegate.diningRoomView performSelectorOnMainThread:@selector(hideTabSearchButton) withObject:nil waitUntilDone:NO];
                    [edHub performSelectorOnMainThread:@selector(cardIcon:) withObject:@"error" waitUntilDone:NO];
                }
            }
        } else [self startBlueBamboo];
	}
}

- (void)BBprintText:(NSString *)prntStrng sender:(id)sender {
    
    [smai rLog:@"BBprintText"];
    
    printMode = 1;
    //printJobSender = (PrintHandler *)sender;
    
    self.printString = [NSString stringWithString:prntStrng];
    //self.printString = [NSString stringWithFormat:@"Hello from POSLavu!%cHello from POSLavu!%cHello from POSLavu!%c%cHello from POSLavu!", 10, 10, 10, 10];
    printBytesToWrite = [self.printString length];
    printBytesWritten = 0;
    
    [self startBlueBamboo];
}

#pragma mark TSP650II

- (void)writeToStarTSP650II {
    
    [smai rLog:@"writeToStarTSP650II"];
    
    if (printMode == 1) {
        
        int writeLen = (printBytesToWrite - printBytesWritten);
        if (writeLen > 2048) writeLen = 2048;
        [smai rLog:[NSString stringWithFormat:@"writeLen: %i", writeLen]];
        
        unsigned char buffer[writeLen];
        for (int i = printBytesWritten; i < (printBytesWritten + writeLen); i++) {
            buffer[i] = [self.printString characterAtIndex:i];
        }
        
        //NSLog(@"buffer: %s", buffer);
        
        int bytesWritten = [[session outputStream] write:(const uint8_t *)buffer maxLength:writeLen];
        printBytesWritten += bytesWritten;
        
        [smai rLog:[NSString stringWithFormat:@"bytesWritten: %i", bytesWritten]];
        [smai rLog:[NSString stringWithFormat:@"printBytesWritten: %i", printBytesWritten]];
    }
}

#pragma mark Linea Pro

- (void)connectLineaPro { // Inifinite Peripherals devices
    
    [smai rLog:@"connectLineaPro"];
    [self dcLog:@"connectLineaPro"];
    
    dtDevice = [DTDevices sharedDevice];
	[dtDevice addDelegate:self];
	[dtDevice connect];
}

- (void)disconnectLineaPro {
    
    [smai rLog:@"disconnectLineaPro"];
    [self dcLog:@"disconnectLineaPro"];
    
    [dtDevice removeDelegate:self];
    [dtDevice disconnect];
}

- (void)connectionState:(int)state {
	
	[smai rLog:[NSString stringWithFormat:@"connectionState: %i", state]];
	[self dcLog:[NSString stringWithFormat:@"connectionState: %i", state]];
    
    switch (state) {
            
        case CONN_DISCONNECTED:
            
            [self dcLog:@"CONN_DISCONNECTED"];
            
            [[UIApplication sharedApplication] setIdleTimerDisabled:NO];
            self.connected = NO;
            
            if (self.hasCardReader && ![edHub hasCardReader]) {
                //if ([smai.activePOSview isEqualToString:@"diningRoom"]) [appDelegate.diningRoomView performSelectorOnMainThread:@selector(hideTabSearchButton) withObject:nil waitUntilDone:NO];
                [edHub performSelectorOnMainThread:@selector(cardIcon:) withObject:@"down" waitUntilDone:NO];
            }
            if (self.hasScanner && ![edHub hasScanner]) {
                //if ([smai.activePOSview isEqualToString:@"order"]) [appDelegate.orderView performSelectorOnMainThread:@selector(scannerDidDisconnect) withObject:nil waitUntilDone:NO];
            }
            
            break;
            
        case CONN_CONNECTING:
            
            [self dcLog:@"CONN_CONNECTING"];
            
            [[UIApplication sharedApplication] setIdleTimerDisabled:NO];
            self.connected = NO;
            
            if (self.hasCardReader && ![edHub hasCardReader]) {
                //if ([smai.activePOSview isEqualToString:@"diningRoom"]) [appDelegate.diningRoomView performSelectorOnMainThread:@selector(hideTabSearchButton) withObject:nil waitUntilDone:NO];
                [edHub performSelectorOnMainThread:@selector(cardIcon:) withObject:@"error" waitUntilDone:NO];
            }
            if (self.hasScanner && ![edHub hasScanner]) {
                //if ([smai.activePOSview isEqualToString:@"order"]) [appDelegate.orderView scannerDidDisconnect];
            }
            
            break;
            
        case CONN_CONNECTED:
            
            [self dcLog:@"CONN_CONNECTED"];
            
            [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
            self.connected = YES;
            [smai rLog:[NSString stringWithFormat:@"linea device: %@", [dtDevice deviceName]]];
            [self dcLog:[NSString stringWithFormat:@"linea device: %@", [dtDevice deviceName]]];
            
            NSError *error = nil;
            [dtDevice msSetCardDataMode:MS_PROCESSED_CARD_DATA error:&error];
            [smai rLog:[NSString stringWithFormat:@"dtDevice msSetCardDataMode - error: %@", [error localizedDescription]]];
            [self dcLog:[NSString stringWithFormat:@"dtDevice msSetCardDataMode - error: %@", [error localizedDescription]]];
            
            if (self.hasCardReader) {
                //if ([smai.activePOSview isEqualToString:@"diningRoom"]) [appDelegate.diningRoomView performSelectorOnMainThread:@selector(showTabSearchButton) withObject:nil waitUntilDone:NO];
                [edHub performSelectorOnMainThread:@selector(cardIcon:) withObject:@"up" waitUntilDone:NO];
            }
            if (self.hasScanner && ![edHub hasScanner]) {
                //if ([smai.activePOSview isEqualToString:@"order"]) [appDelegate.orderView performSelectorOnMainThread:@selector(scannerDidConnect) withObject:nil waitUntilDone:NO];
            }
            
            break;
    }
}

- (void)batteryDidChange {
    
    NSError *error = nil;
    BOOL charging = NO;
	
    if ((dtDevice.connstate == CONN_CONNECTED) && ([dtDevice getSupportedFeature:FEAT_BATTERY_CHARGING error:nil] == FEAT_SUPPORTED)) {
        
        [dtDevice getCharging:&charging error:&error];
        [smai rLog:[NSString stringWithFormat:@"dtDevice getCharging: %i - %@", charging, [error localizedDescription]]];
        [self dcLog:[NSString stringWithFormat:@"dtDevice getCharging: %i - %@", charging, [error localizedDescription]]];
		
        if (charging && ([UIDevice currentDevice].batteryState == UIDeviceBatteryStateFull)) {
            
            [dtDevice setCharging:NO error:&error];
            [smai rLog:[NSString stringWithFormat:@"dtDevice setCharging: NO - %@", [error localizedDescription]]];
            [self dcLog:[NSString stringWithFormat:@"dtDevice setCharging: NO - %@", [error localizedDescription]]];
            
        } else if ([UIDevice currentDevice].batteryLevel < 0.8f) {
            
            [dtDevice setCharging:YES error:&error];
            [smai rLog:[NSString stringWithFormat:@"dtDevice getCharging: YES - %@", [error localizedDescription]]];
            [self dcLog:[NSString stringWithFormat:@"dtDevice getCharging: YES - %@", [error localizedDescription]]];
        }
	}
}

- (void)magneticCardData:(NSString *)track1 track2:(NSString *)track2 track3:(NSString *)track3 {
	
    [smai rLog:[NSString stringWithFormat:@"magneticCardData, nowSendingSwipeData: %i", smai.nowSendingSwipeData]];
    [appDelegate dcLog:[NSString stringWithFormat:@"magneticCardData, nowSendingSwipeData: %i", smai.nowSendingSwipeData]];
    
    //NSLog(@"track1: %@", track1);
    //NSLog(@"track2: %@", track2);
    //NSLog(@"track3: %@", track3);
    
	if (!smai.nowSendingSwipeData) {
        
        self.track1String = @"ERROR?";
        self.track2String = @"ERROR?";
        self.track3String = @"ERROR";
        if (track1 != nil) { self.track1String = track1; }
        if (track2 != nil) { self.track2String = track2; }
        if (track3 != nil) { self.track3String = track3; }
        self.cardSwipeString = [NSString stringWithFormat:@"%@%@%@", self.track1String, self.track2String, self.track3String];
        [edHub processSwipe:self.cardSwipeString dataString:@"" reader:self.deviceName encrypted:NO];
    }
}

- (void)scanDown {
    
    NSError *error = nil;
    [dtDevice barcodeStartScan:&error];
    [smai rLog:[NSString stringWithFormat:@"dtDevice barcodeStartScan: %@", [error localizedDescription]]];
}

- (void)scanUp {
    
    NSError *error = nil;
    [dtDevice barcodeStopScan:&error];
    [smai rLog:[NSString stringWithFormat:@"dtDevice barcodeStopScan: %@", [error localizedDescription]]];
}

- (void)barcodeData:(NSString *)barcode type:(int)type {
    
    //NSLog(@"barcodeData: %@ type: %i", barcode, type);
    //[appDelegate dcLog:[NSString stringWithFormat:@"barcodeData: %@ type: %i", barcode, type]];
    
    edHub.lastScanScannerName = self.deviceName;
    edHub.lastScanString = barcode;
    
    //if ([smai.activePOSview isEqualToString:@"order"]) [appDelegate.orderView barcodeData:barcode type:type];
}

#pragma mark

- (void)dealloc {
    
    [smai rLog:[NSString stringWithFormat:@"dealloc ExternalDeviceDelegate: %@ %@", self.deviceName, self.protocolString]];
    
    [self closeStreams];
    
    if ([self.deviceName isEqualToString:@"linea"]) {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIDeviceBatteryStateDidChangeNotification object:nil];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIDeviceBatteryLevelDidChangeNotification object:nil];
    }
    
    [interceptPrinterSettings release];
    
    [deviceName release];
    [protocolString release];
    [accessory release];
    [track1String release];
    [track2String release];
    [track3String release];
    [cardSwipeString release];
    [cardSwipeDataString release];
    [printString release];
    
    [super dealloc];
}

@end
