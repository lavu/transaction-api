//
//  LavuSwipper.h
//  CardSwiperTest
//
//  Created by Theodore Schnepper on 10/8/13.
//  Copyright (c) 2013 Lavu, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LavuSwiperDelegate.h"

static NSString* LavuCardSwipeDataNotification __unused = @"LavuCardSwipeDataNotification";

typedef enum {
    LavuCardSwiperTypeAudioJack = 1,
    LavuCardSwiperTypeAppleConnector = 2,
    LavuCardSwiperTypeBluetooth = 3,
    LavuCardSwiperTypeNetworkSwiper = 4,
    LavuCardSwiperType3rdParty = 10,
    LavuCardSwiperTypeUNKNOWN = 0
} LavuCardSwiperType;

typedef enum {
    LavuCardSwiperStatusConnected,
    LavuCardSwiperStatusDisconnected,
    LavuCardSwiperStatusListening,
    LavuCardSwiperStatusError
} LavuCardSwiperStatus;

/** The LavuSwipper protocol specifies a wrapper for any potential Swipper we
 */
@protocol LavuSwiperProtocol <NSObject>
///---------------------------------------------------------------------------------------
/// Device Connection Section
///---------------------------------------------------------------------------------------
#pragma mark -
#pragma mark Device Connection Section

 /** Attempts to connect to the device, if neccessary.  Should return whether the action was successful or not.
  This method will call the registered LavuSwipperDelegate upon completion of the connection status via the selector @selector(didConnect: forSwipper:)
 */
- (void) connect;
 /** Returns whether the device is currently 'Connected' or Not
 @return Returns `TRUE` if the device is currently believed to be connected, `FALSE` otherwise
 */
- (BOOL) isConnected;
 /** Attempts to disconnect from the device, if neccessary.  Should return whether the action was successful or not.
  This method will call the registered LavuSwipperDelegate upon completion of the disconnection status via the selector @selector(didDisonnect: forSwipper:)
  */
- (void) disconnect;

///---------------------------------------------------------------------------------------
/// Device Listening Section
///---------------------------------------------------------------------------------------
#pragma mark -
#pragma mark Device Listening Section
 /** Will attempt to start listening to the device for input.  Essentially this is a helpful prompt for the device to tell it when it should expect to recieve any input/swipes.
  This method will call the registered LavuSwipperDelegate upon completion of the connection status via the selector @selector(didStartListening: forSwipper:)
  */
- (void) startListening;

 /** Returns whether the device is currently in a state of 'listening' or not.
  @return Returns `TRUE` if the device is currently in a 'listening' state, `FALSE` otherwise
  */
- (BOOL) isListening;

 /** Will attempt to stop listening to the device for input.
  This method will call the registered LavuSwipperDelegate upon completion of the connection status via the selector @selector(didStopListening: forSwipper:)
  */
- (void) stopListening;

///---------------------------------------------------------------------------------------
/// Device Listening Section
///---------------------------------------------------------------------------------------
#pragma mark -
#pragma mark Device Swipe Preparation
/** Returns whether the device is successfully ready for a card Swipe/input.
 
 Will notify the device that it should get in a state where it needs to prepare for a swipe.  This is mainly provided as a convenience method for any swipper that may need it.  In most cases, it's unlikely that it will be needed.  However it exists here just in case.
 
 This method will call the registered LavuSwipperDelegate upon completion of the connection status via the selector @selector(didPrepareForSwipe: forSwipper:)
 */
- (void) prepareForSwipe;

///---------------------------------------------------------------------------------------
/// Device Type
///---------------------------------------------------------------------------------------
#pragma mark -
#pragma mark Device Swipe Preparation
/** Returns the type of the device.  Please note that this is different than the device itself, meaning that this is not it's model, but specifically how the swipper communicates with the iPad itself.  Can be useful debuging information.
    @return Returns the Swipper type of the device from the enum type LavuCardSwipperType
 */
+ (LavuCardSwiperType) getSwipperType;

/** Attempts to set the Delegate for this swipper.  The delegate will be informed whenever the swipper performs and action successfully or unsuccessfully in accordance with what the selectors are specified to having report.
 @return Returns TRUE upon successfully being set, FALSE otherwise.
 */
- (BOOL) setDelegate: (id<LavuSwiperDelegate>) delegate;

/** Convenience selector for retrieving the device's Manufacturer as a String.
 @return Returns an NSString representation of the device's Manufacturer;
 */
+ (NSString*) deviceManufacturer;

/** Convenience selector for retrieving the device's name as a String.
 @return Returns an NSString representation of the device's name.
 */
+ (NSString*) deviceName;

/** Convenience selector for retrieving the device's imagename as a String.  This is an image to reference so it can be loaded later.  Probably shouldn't be here.
 @return Returns an NSString representation of the device's imagename.
 */
+ (NSString*) imageName;
@end
