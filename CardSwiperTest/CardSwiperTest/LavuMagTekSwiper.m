//
//  LavuMagTekSwipper.m
//  CardSwiperTest
//
//  Created by Theodore Schnepper on 10/8/13.
//  Copyright (c) 2013 Lavu, Inc. All rights reserved.
//

#import "LavuMagTekSwiper.h"

@implementation LavuMagTekSwiper
- (id) init {
    self = [super init];
    if(! mtSCRAlib ){
        mtSCRAlib = [[MTSCRA alloc] init];
    }
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(trackDataReady:) name: MagTekTrackDataReadyNotification object: NULL];
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(connectionStatusChange:) name: MagTekConnectionStatusChangeNotification object: NULL];
    [self setDeviceProtocol: (NSString*)MagTekDeviceProtocolString];
    return self;
}

- (void) dealloc {
    //[mtSCRAlib release];
    [[NSNotificationCenter defaultCenter] removeObserver: self name: MagTekTrackDataReadyNotification object: NULL];
    [[NSNotificationCenter defaultCenter] removeObserver: self name: MagTekConnectionStatusChangeNotification object: NULL];
    [super dealloc];
}


- (void) trackDataReady:(NSNotification *)notification {
    NSNumber* status = [[notification userInfo] valueForKey: @"status" ];
    switch( [status intValue] ){
        case TRANS_STATUS_OK :{
            //Done Reading
            NSString* pstrTrackDecodeStatus = [mtSCRAlib getTrackDecodeStatus];
            BOOL trackReadError = FALSE;
            if(pstrTrackDecodeStatus && [pstrTrackDecodeStatus length] >= 6) {
                trackReadError = trackReadError || [[pstrTrackDecodeStatus substringWithRange: NSMakeRange(0, 2)] intValue] == 1;
                trackReadError = trackReadError || [[pstrTrackDecodeStatus substringWithRange: NSMakeRange(2, 2)] intValue] == 1;
                trackReadError = trackReadError || [[pstrTrackDecodeStatus substringWithRange: NSMakeRange(4, 2)] intValue] == 1;
            } else {
                trackReadError = TRUE;
            }
            
            //Even with an Error, we should be able to read track data
            
            LavuMagTekCardSwipeData* cardData = [[[LavuMagTekCardSwipeData alloc] init] autorelease];
            [cardData setResponseType: [mtSCRAlib getResponseType]];
            [cardData setTrackStatus: [mtSCRAlib getTrackDecodeStatus]];
            [cardData setCardStatus: [mtSCRAlib getCardStatus]];
            [cardData setEncryptionStatus: [mtSCRAlib getEncryptionStatus]];
            [cardData setBatteryLevel: [mtSCRAlib getBatteryLevel]];
            [cardData setSwipeCount: [mtSCRAlib getSwipeCount]];
            [cardData setTrack1String: [mtSCRAlib getTrack1Masked]];
            [cardData setTrack2String: [mtSCRAlib getTrack2Masked]];
            [cardData setTrack3String: [mtSCRAlib getTrack3Masked]];
            [cardData setEncryptedTrack1: [mtSCRAlib getTrack1]];
            [cardData setEncryptedTrack2: [mtSCRAlib getTrack2]];
            [cardData setEncryptedTrack3: [mtSCRAlib getTrack3]];
            [cardData setMagnePrintEncrypted: [mtSCRAlib getMagnePrint]];
            [cardData setMagnePrintStatus: [mtSCRAlib getMagnePrintStatus]];
            [cardData setSessionID: [mtSCRAlib getSessionID]];
            [cardData setPANLength: [mtSCRAlib getCardPANLength]];
            [cardData setDeviceSerialNumber: [mtSCRAlib getDeviceSerial]];
            [cardData setMagTekSN: [mtSCRAlib getKSN]];
            [cardData setDeviceSerialNumber: [mtSCRAlib getMagTekDeviceSerial]];
            [cardData setFirmwarePartNumber: [mtSCRAlib getFirmware]];
            [cardData setTLVVersion: [mtSCRAlib getTLVVersion]];
            [cardData setDeviceModelName: [mtSCRAlib getDeviceName]];
            [cardData setCapabilityMSR: [mtSCRAlib getCapMSR]];
            [cardData setCapabilityTracks: [mtSCRAlib getCapTracks]];
            [cardData setCapabilityEncryption: [mtSCRAlib getCapMagStripeEncryption]];
            [cardData parseTracks: [NSString stringWithFormat: @"%@%@%@", [cardData track1String], [cardData track2String], [cardData track3String]]];
            
            //[mtSCRAlib clearBuffers];
            [[NSNotificationCenter defaultCenter] postNotificationName: (NSString*)LavuCardSwipeDataNotification object: cardData userInfo: NULL];
            break;
        } case TRANS_STATUS_START : {
            //Now Reading
            break;
        } case TRANS_STATUS_ERROR : {
        } default: {
            if( mtSCRAlib != NULL ){
                NSLog(@"Some Bullshit Happened With the Magtek Library");
            }
            break;
        }
    }
}

- (void) connectionStatusChange:(NSNotification *)notification {
    NSLog(@"Connection Status: %@", notification);
}

- (void) setDeviceType:(UInt32)deviceType {
    [mtSCRAlib setDeviceType: deviceType];
}

- (void) setDeviceProtocol:(NSString *)protocolString {
    [mtSCRAlib setDeviceProtocolString: protocolString];
}


- (void) connect {
    BOOL result = FALSE;
    if( [mtSCRAlib openDevice] ){
        result = [mtSCRAlib isDeviceConnected];
        [mtSCRAlib closeDevice];
        //result = TRUE;
    }
    if(_delegate){
        [_delegate didConnect: result forSwipper: self];
    }
}

- (BOOL) isConnected {
    return [mtSCRAlib isDeviceConnected] && [mtSCRAlib isDeviceOpened];
}

- (void) disconnect {
    
    if( [self isListening] ){
        [self stopListening];
    }
    
    BOOL result = [mtSCRAlib isDeviceConnected] && [mtSCRAlib isDeviceOpened];
    
    if(_delegate){
        [_delegate didDisconnect: result forSwipper: self];
    }
}

- (void) startListening {
    BOOL result = FALSE;
    if([mtSCRAlib isDeviceConnected] && [mtSCRAlib openDevice]){
        [mtSCRAlib clearBuffers];
        result = TRUE;
    }
    if(_delegate){
        [_delegate didStartListening: result forSwipper: self];
    }
}

- (BOOL) isListening {
    return [mtSCRAlib isDeviceConnected] && [mtSCRAlib isDeviceOpened];
}

- (void) stopListening {
    BOOL result = FALSE;
    if([mtSCRAlib closeDevice] ){
        [mtSCRAlib clearBuffers];
        result = TRUE;
    }
    if(_delegate){
        [_delegate didStopListening: result forSwipper: self];
    }
}

- (void) prepareForSwipe {
    if(_delegate){
        [_delegate didPrepareForSwipe: TRUE forSwipper: self];
    }
}

+ (LavuCardSwiperType) getSwipperType {
    return LavuCardSwiperTypeUNKNOWN;
}

+ (NSString*) deviceManufacturer {
    return @"MagTek";
}
@end
