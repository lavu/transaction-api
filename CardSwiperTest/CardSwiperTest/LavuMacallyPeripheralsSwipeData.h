//
//  LavuMacallyPeripheralsSwipeData.h
//  CardSwiperTest
//
//  Created by Theodore Schnepper on 11/4/13.
//  Copyright (c) 2013 Lavu, Inc. All rights reserved.
//

#import "LavuNullSwipeData.h"

@interface LavuMacallyPeripheralsSwipeData : LavuNullSwipeData

- (id) initWithData: (NSData*) swipeData;

@end
