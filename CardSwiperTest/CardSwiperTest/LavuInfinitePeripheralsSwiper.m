//
//  LavuInfinitePeripherals.m
//  CardSwiperTest
//
//  Created by Theodore Schnepper on 10/25/13.
//  Copyright (c) 2013 Lavu, Inc. All rights reserved.
//

#import "LavuInfinitePeripheralsSwiper.h"

@implementation LavuInfinitePeripheralsSwiper
#pragma mark - Initilization and Deallocation
- (id) init {
    self = [super init];
    
    _msrPresent = FALSE;
    _rfidPresent = FALSE;
    _barcodePresent = FALSE;
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    _dtDevices = [DTDevices sharedDevice];
    [_dtDevices addDelegate: self];
    [_dtDevices retain];
    _isListening = FALSE;
    return self;
}

+ (NSString*) getManufacturer {
    return @"DATECS";
}

- (void) dealloc {
    if([self isConnected]){
        [self disconnect];
    }
    if( _dtDevices ){
        [_dtDevices removeDelegate: self];
        [_dtDevices release];
    }
    [super dealloc];
}

#pragma mark - Infinite Peripherals DTDeviceDelegate
-(void)connectionState:(int)state {
    [appDelegate performSelectorOnMainThread:@selector(rLog:) withObject:[NSString stringWithFormat:@"connectionState: %d for %@:%@", state, [_dtDevices deviceModel], [_dtDevices deviceName]] waitUntilDone:false];
    //NSLog(@"connectionState: %d", state);
    if( CONN_CONNECTED == state ){
        if(_delegate){
            [_delegate didConnect: TRUE forSwipper: self];
        }
    } else if ( CONN_DISCONNECTED == state ){
        if(_delegate){
            [_delegate didDisconnect: TRUE forSwipper: self];
        }
    } else {
        //[_delegate didConnect: TRUE forSwipper: self];
        //[self checkForDevice];
    }
}

-(void)deviceButtonPressed:(int)which {
   [appDelegate performSelectorOnMainThread:@selector(rLog:) withObject:[NSString stringWithFormat:@"deviceButtonPressed: %d", which] waitUntilDone:false];
    NSLog(@"deviceButtonPressed: %d", which);
}

-(void)deviceButtonReleased:(int)which {
    NSLog(@"deviceButtonReleased: %d", which);
}

-(void)barcodeData:(NSString *)barcode type:(int)type {
    [appDelegate performSelectorOnMainThread:@selector(rLog:) withObject:[NSString stringWithFormat:@"barcodeData| barcode: %@, type: %d", barcode, type] waitUntilDone:false];
    NSLog(@"barcodeData| barcode: %@, type: %d", barcode, type);
}

-(void)barcodeData:(NSString *)barcode isotype:(NSString *)isotype {
    [appDelegate performSelectorOnMainThread:@selector(rLog:) withObject:[NSString stringWithFormat:@"barcodeData| barcode: %@, isotype: %@", barcode, isotype] waitUntilDone:false];
    NSLog(@"barcodeData| barcode: %@, isotype: %@", barcode, isotype);
}

-(void)magneticCardData:(NSString *)track1 track2:(NSString *)track2 track3:(NSString *)track3 {
    [appDelegate performSelectorOnMainThread:@selector(rLog:) withObject:[NSString stringWithFormat:@"magneticCardData| track1: %@, track2: %@, track3: %@", track1, track2, track3] waitUntilDone:false];
    NSLog(@"magneticCardData| track1: %@, track2: %@, track3: %@", track1, track2, track3);
    
    LavuInfinitePeripheralsSwipeData* swipeData = [[[LavuInfinitePeripheralsSwipeData alloc] init] autorelease];
    NSString* swipeString = @"";
    if( track1 ){
        swipeString = [swipeString stringByAppendingString: track1];
    }
    if( track2 ){
        swipeString = [swipeString stringByAppendingString: track2];
    }
    if( track3 ){
        swipeString = [swipeString stringByAppendingString: track3];
    }
    [swipeData parseTracks: swipeString];
    [[NSNotificationCenter defaultCenter] postNotificationName: LavuCardSwipeDataNotification object: swipeData userInfo: NULL];
}

-(void)magneticCardEncryptedData:(int)encryption tracks:(int)tracks data:(NSData *)data {
   [appDelegate performSelectorOnMainThread:@selector(rLog:) withObject:[NSString stringWithFormat:@"magneticCardEncryptedData| encryption: %d, tracks: %d, data: %@", encryption, tracks, data] waitUntilDone:false];
    NSLog(@"magneticCardEncryptedData| encryption: %d, tracks: %d, data: %@", encryption, tracks, data);
}

-(void)magneticCardRawData:(NSData *)tracks {
   [appDelegate performSelectorOnMainThread:@selector(rLog:) withObject:[NSString stringWithFormat:@"magneticCardRawData: %@", tracks] waitUntilDone:false];
    NSLog(@"magneticCardRawData: %@", tracks);
}

-(void)magneticCardEncryptedRawData:(int)encryption data:(NSData *)data {
    [appDelegate performSelectorOnMainThread:@selector(rLog:) withObject:[NSString stringWithFormat:@"magneticCardEncryptedRawData| encryption: %d, data: %@", encryption, data] waitUntilDone:false];
    NSLog(@"magneticCardEncryptedRawData| encryption: %d, data: %@", encryption, data);
}

-(void)firmwareUpdateProgress:(int)phase percent:(int)percent {
    NSLog(@"firmwareUpdateProgress| phase: %d, percent: %d", phase, percent);
}

-(void)bluetoothDiscoverComplete:(BOOL)success {
    NSLog(@"bluetoothDiscoverComplete: %hhd", success);
}

-(void)bluetoothDeviceDiscovered:(NSString *)btAddress name:(NSString *)btName {
    NSLog(@"bluetoothDeviceDiscovered| Name: %@, Address: %@", btName, btAddress);
}

-(void)bluetoothDeviceConnected:(NSString *)btAddress {
    NSLog(@"bluetoothDeviceConnected: %@", btAddress);
}

-(void)bluetoothDeviceDisconnected:(NSString *)btAddress {
    NSLog(@"bluetoothDeviceDisconnected: %@", btAddress);
}

-(void)magneticJISCardData:(NSString *)data {
    [appDelegate performSelectorOnMainThread:@selector(rLog:) withObject:[NSString stringWithFormat:@"magneticJISCardData: %@", data] waitUntilDone:false];
    NSLog(@"magneticJISCardData: %@", data);
}

-(void)rfCardDetected:(int)cardIndex info:(DTRFCardInfo *)info {
    [appDelegate performSelectorOnMainThread:@selector(rLog:) withObject:[NSString stringWithFormat:@"rfCardDetected| cardIndex: %d, info: %@", cardIndex, info] waitUntilDone:false];
    NSLog(@"rfCardDetected| cardIndex: %d, info: %@", cardIndex, info);
}

-(void)rfCardRemoved:(int)cardIndex {
    [appDelegate performSelectorOnMainThread:@selector(rLog:) withObject:[NSString stringWithFormat:@"rfCardRemoved: %d", cardIndex] waitUntilDone:false];
    NSLog(@"rfCardRemoved: %d", cardIndex);
}

-(void)deviceFeatureSupported:(int)feature value:(int)value {
    NSString* enabled = @"";
    if(FEAT_SUPPORTED == value){
        enabled = @"Enabled";
    } else {
        enabled = @"Disabled";
    }
    
    switch( feature ){
        case FEAT_BARCODE: {
            
            if( value & BARCODE_CODE ){
                _barcodePresent =  true;
                [appDelegate performSelectorOnMainThread:@selector(rLog:) withObject:@"BarCode with Code Engine Supported" waitUntilDone:false];
            }
            
            if( value & BARCODE_INTERMEC ){
                _barcodePresent = true;
                [appDelegate performSelectorOnMainThread:@selector(rLog:) withObject:@"BarCode with Intermec Engine Supported" waitUntilDone:false];
            }
            
            if( value & BARCODE_NEWLAND ){
                _barcodePresent = true;
                [appDelegate performSelectorOnMainThread:@selector(rLog:) withObject:@"BarCode with Newland Engine Supported" waitUntilDone:false];
            }
            
            if( value & BARCODE_OPTICON ){
                _barcodePresent = true;
                [appDelegate performSelectorOnMainThread:@selector(rLog:) withObject:@"BarCode with Opticon Engine Supported" waitUntilDone:false];
            }
            
            if( value == FEAT_UNSUPPORTED ){
                _barcodePresent = false;
                [appDelegate performSelectorOnMainThread:@selector(rLog:) withObject:@"BarCode Diabled" waitUntilDone:false];
            }
            
            break;
        } case FEAT_BATTERY_CHARGING: {
            [appDelegate performSelectorOnMainThread:@selector(rLog:) withObject:[NSString stringWithFormat:@"Battery Charging %@", enabled] waitUntilDone:false];
            NSLog( @"Battery Charging %@", enabled );
            break;
        } case FEAT_BLUETOOTH : {
            
            if( (value & BLUETOOTH_CLIENT) == BLUETOOTH_CLIENT ){
                [appDelegate performSelectorOnMainThread:@selector(rLog:) withObject:[NSString stringWithFormat:@"Bluetooth Enabled, can accept Outgoing."] waitUntilDone:false];
            }
            
            if( (value & BLUETOOTH_HOST) == BLUETOOTH_HOST ){
                [appDelegate performSelectorOnMainThread:@selector(rLog:) withObject:[NSString stringWithFormat:@"Bluetooth Enabled, can accept incoming."] waitUntilDone:false];
            }
            
            if ( value == FEAT_UNSUPPORTED ) {
                [appDelegate performSelectorOnMainThread:@selector(rLog:) withObject:[NSString stringWithFormat:@"Bluetooth Disabled"] waitUntilDone:false];
            }
            break;
        } case FEAT_EMVL2_KERNEL : {
            [appDelegate performSelectorOnMainThread:@selector(rLog:) withObject:[NSString stringWithFormat:@"Emvl2 Kernal %@", enabled] waitUntilDone:false];
            NSLog( @"Emvl2 Kernal %@", enabled );
            break;
        } case FEAT_EXTERNAL_SERIAL_PORT : {
            [appDelegate performSelectorOnMainThread:@selector(rLog:) withObject:[NSString stringWithFormat:@"External Serial Port %@", enabled] waitUntilDone:false];
            NSLog( @"External Serial Port %@", enabled );
            break;
        } case FEAT_LEDS : {
            [appDelegate performSelectorOnMainThread:@selector(rLog:) withObject:[NSString stringWithFormat:@"LEDs %@", enabled] waitUntilDone:false];
            NSLog( @"LEDs %@", enabled );
            break;
        } case FEAT_MAX : {
            [appDelegate performSelectorOnMainThread:@selector(rLog:) withObject:[NSString stringWithFormat:@"MAX %@", enabled] waitUntilDone:false];
            NSLog( @"MAX %@", enabled );
            break;
        } case FEAT_MSR : { //card swiper
            if( (value & MSR_ENCRYPTED) == MSR_ENCRYPTED ){
                [appDelegate performSelectorOnMainThread:@selector(rLog:) withObject:@"Encrypted Card Swiper Supported" waitUntilDone:false];
                _msrPresent = true;
            }
            
            if( (value & MSR_PLAIN) == MSR_PLAIN ){
                [appDelegate performSelectorOnMainThread:@selector(rLog:) withObject:@"Unencrypted Card Swiper Supported" waitUntilDone:false];
                _msrPresent = true;
            }
            
            if( (value & MSR_PLAIN_WITH_ENCRYPTION) == MSR_PLAIN_WITH_ENCRYPTION ){
                [appDelegate performSelectorOnMainThread:@selector(rLog:) withObject:@"Encrypted and Unencrypted Card Swiper Supported" waitUntilDone:false];
                _msrPresent = true;
            }
            
            if( (value & MSR_VOLTAGE) == MSR_VOLTAGE ){
                [appDelegate performSelectorOnMainThread:@selector(rLog:) withObject:@"Volate? Card Swiper Supported" waitUntilDone:false];
                _msrPresent = true;
            }
            
            if( value == FEAT_UNSUPPORTED ){
                [appDelegate performSelectorOnMainThread:@selector(rLog:) withObject:@"Card Swiper Not Enabled." waitUntilDone:false];
                _msrPresent = false;
            }
            break;
        } case FEAT_PIN_ENTRY : {
            [appDelegate performSelectorOnMainThread:@selector(rLog:) withObject:[NSString stringWithFormat:@"PIN Entry %@", enabled] waitUntilDone:false];
            NSLog( @"PIN Entry %@", enabled );
            break;
        } case FEAT_PRINTING : {
            [appDelegate performSelectorOnMainThread:@selector(rLog:) withObject:[NSString stringWithFormat:@"Printing %@", enabled] waitUntilDone:false];
            NSLog( @"Printing %@", enabled );
            break;
        } case FEAT_RF_READER : {
            if(value == FEAT_SUPPORTED);
            _rfidPresent = (value == FEAT_SUPPORTED);
            [appDelegate performSelectorOnMainThread:@selector(rLog:) withObject:[NSString stringWithFormat:@"RF Reader %@, %x", enabled, value] waitUntilDone:false];
            NSLog( @"RF Reader %@", enabled );
            break;
        } case FEAT_SMARTCARD : {
            [appDelegate performSelectorOnMainThread:@selector(rLog:) withObject:[NSString stringWithFormat:@"Smartcard %@", enabled] waitUntilDone:false];
            NSLog( @"Smartcard %@", enabled );
            break;
        } case FEAT_SPEAKER : {
            [appDelegate performSelectorOnMainThread:@selector(rLog:) withObject:[NSString stringWithFormat:@"Speaker %@", enabled] waitUntilDone:false];
            NSLog( @"Speaker %@", enabled );
            break;
        } case FEAT_VIBRATION : {
            [appDelegate performSelectorOnMainThread:@selector(rLog:) withObject:[NSString stringWithFormat:@"Vibration %@", enabled] waitUntilDone:false];
            NSLog( @"Vibration %@", enabled );
            break;
        }
    }
    //NSLog(@"deviceFeatureSupported| feature: %d, value: %d", feature, value);
}

-(void)smartCardInserted:(SC_SLOTS)slot {
    [appDelegate performSelectorOnMainThread:@selector(rLog:) withObject:[NSString stringWithFormat:@"smartCardInserted: %u", slot] waitUntilDone:false];
    NSLog(@"smartCardInserted: %u", slot);
}

-(void)smartCardRemoved:(SC_SLOTS)slot {
    [appDelegate performSelectorOnMainThread:@selector(rLog:) withObject:[NSString stringWithFormat:@"smartCardRemoved: %u", slot] waitUntilDone:false];
    NSLog(@"smartCardRemoved: %u", slot);
}

-(void)PINEntryCompleteWithError:(NSError *)error {
    [appDelegate performSelectorOnMainThread:@selector(rLog:) withObject:[NSString stringWithFormat:@"PINEntryCompleteWithError: %@", error] waitUntilDone:false];
    NSLog(@"PINEntryCompleteWithError: %@", error);
}

-(void)paperStatus:(BOOL)present {
    [appDelegate performSelectorOnMainThread:@selector(rLog:) withObject:[NSString stringWithFormat:@"paperStatus: %hhd", present] waitUntilDone:false];
    NSLog(@"paperStatus: %hhd", present);
}


#pragma mark - Infinite Peripherals Behaviors

- (void) connect {
    //if([_dtDevices isPresent: DEVICE_TYPE_LINEA]){
        [_dtDevices connect];
    //} else {
    //    if(_delegate){
    //        [_delegate didConnect: false forSwipper:self];
    //    }
    //}
}

- (BOOL) isConnected {
    return [_dtDevices isPresent: DEVICE_TYPE_LINEA] && [_dtDevices connstate] == CONN_CONNECTED;
}

- (void) disconnect {
    if( [self isListening] ){
        [self stopListening];
    }
    [_dtDevices disconnect];
}



- (void) checkAndStartMSR {
    NSError* error;
    if( _msrPresent ){
        if([_dtDevices msEnable: &error]){
            [appDelegate performSelectorOnMainThread:@selector(rLog:) withObject:[NSString stringWithFormat:@"MSR Enable Success: %@", error] waitUntilDone:false];
        } else {
            [appDelegate performSelectorOnMainThread:@selector(rLog:) withObject:[NSString stringWithFormat:@"MSR Enable Error: %@", error] waitUntilDone:false];
        }
    } else {
        [appDelegate performSelectorOnMainThread:@selector(rLog:) withObject:[NSString stringWithFormat:@"MSR Not Present"] waitUntilDone:false];
    }
}

- (void) checkAndStartBarCode {
    NSError* error;
    if(_barcodePresent){
        if([_dtDevices barcodeStartScan: &error]){
            [appDelegate performSelectorOnMainThread:@selector(rLog:) withObject:[NSString stringWithFormat:@"Barcode Start Success: %@", error] waitUntilDone:false];
        } else {
            [appDelegate performSelectorOnMainThread:@selector(rLog:) withObject:[NSString stringWithFormat:@"Barcode Start Error: %@", error] waitUntilDone:false];
        }
    } else {
        [appDelegate performSelectorOnMainThread:@selector(rLog:) withObject:[NSString stringWithFormat:@"Barcode Not Present"] waitUntilDone:false];
    }
}

- (void) checkAndStartRFReader {
    NSError* error;
    if(_rfidPresent){
        int all_supported_cards = CARD_SUPPORT_FELICA | CARD_SUPPORT_ISO15 | CARD_SUPPORT_JEWEL /*| CARD_SUPPORT_NFC*/ | CARD_SUPPORT_STSRI | CARD_SUPPORT_TYPE_A | CARD_SUPPORT_TYPE_B;
        if([_dtDevices rfInit: all_supported_cards error: &error]){
            [appDelegate performSelectorOnMainThread:@selector(rLog:) withObject:[NSString stringWithFormat:@"RF Start Success: %@", error] waitUntilDone:false];
        } else {
            [appDelegate performSelectorOnMainThread:@selector(rLog:) withObject:[NSString stringWithFormat:@"RF Start Error: %@", error] waitUntilDone:false];
        }
    } else {
        [appDelegate performSelectorOnMainThread:@selector(rLog:) withObject:[NSString stringWithFormat:@"RF Not Present"] waitUntilDone:false];
    }
}


- (void) startListening {
    if(![self isConnected]){
        if(_delegate){
            [_delegate didStartListening: FALSE forSwipper: self];
        }
        return;
    }
    
    //NSError* error = [[[NSError alloc] init] autorelease];
    BOOL result = true;
    [self performSelectorOnMainThread:@selector(checkAndStartMSR) withObject:nil waitUntilDone: true];
    [self performSelectorOnMainThread:@selector(checkAndStartBarCode) withObject:nil waitUntilDone: true];
    [self performSelectorOnMainThread:@selector(checkAndStartRFReader) withObject:nil waitUntilDone: true];
    //[self performSelector:@selector(checkAndStartMSR) withObject: self afterDelay:0.0];
    //[self performSelector:@selector(checkAndStartBarCode) withObject: self afterDelay:0.5];
    //[self performSelector:@selector(checkAndStartRFReader) withObject: self afterDelay:1.0];
    //[self checkAndStartMSR];
    //[self checkAndStartBarCode];
    //[self checkAndStartRFReader];
    
    // Magnetic Stripe Reader
    /*{
        if( _msrPresent ){
            if([_dtDevices msEnable: &error]){
                [appDelegate performSelectorOnMainThread:@selector(rLog:) withObject:[NSString stringWithFormat:@"MSR Enable Success: %@", error] waitUntilDone:false];
            } else {
                [appDelegate performSelectorOnMainThread:@selector(rLog:) withObject:[NSString stringWithFormat:@"MSR Enable Error: %@", error] waitUntilDone:false];
                result = result && false;
            }
        }
    }
    
    //Barcode Scanner
    {
        
        if(_barcodePresent){
            if([_dtDevices barcodeStartScan: &error]){
                [appDelegate performSelectorOnMainThread:@selector(rLog:) withObject:[NSString stringWithFormat:@"Barcode Start Success: %@", error] waitUntilDone:false];
            } else {
                [appDelegate performSelectorOnMainThread:@selector(rLog:) withObject:[NSString stringWithFormat:@"Barcode Start Error: %@", error] waitUntilDone:false];
                result = result && false;
            }
        }
    }
    
    //RF Reader
    {
        if(_rfidPresent){
            int all_supported_cards = CARD_SUPPORT_FELICA | CARD_SUPPORT_ISO15 | CARD_SUPPORT_JEWEL | CARD_SUPPORT_NFC | CARD_SUPPORT_STSRI | CARD_SUPPORT_TYPE_A | CARD_SUPPORT_TYPE_B;
            if([_dtDevices rfInit: all_supported_cards error: &error]){
                [appDelegate performSelectorOnMainThread:@selector(rLog:) withObject:[NSString stringWithFormat:@"RF Start Success: %@", error] waitUntilDone:false];
            } else {
                [appDelegate performSelectorOnMainThread:@selector(rLog:) withObject:[NSString stringWithFormat:@"RF Start Error: %@", error] waitUntilDone:false];
                result = result && false;
            }
        }
    }*/
    
    _isListening = result;
    
    if(_delegate){
        [_delegate didStartListening: result forSwipper: self];
    }
}

- (BOOL) isListening {
    return [_dtDevices isPresent: DEVICE_TYPE_LINEA] && [_dtDevices connstate] == CONN_CONNECTED && _isListening;
}

- (void) stopListening {
    NSError* error = [[[NSError alloc] init] autorelease];
    BOOL result = [_dtDevices msDisable: &error];
    if(error && [error code]){
        NSLog(@"Start Listening Error: %@", error);
        if(_delegate){
            [_delegate didStopListening: FALSE forSwipper: self];
        }
    } else {
        _isListening = result;
        [_delegate didStopListening: result forSwipper: self];
    }
    [_dtDevices barcodeStopScan: NULL];
    [_dtDevices rfClose: NULL];
}

- (void) prepareForSwipe {
    [_delegate didPrepareForSwipe: [self isConnected] && [self isListening] forSwipper: self];
}

#pragma mark - LavuSwipperProtocal Overrides
+ (LavuCardSwiperType) getSwipperType {
    return LavuCardSwiperTypeAppleConnector;
}

+ (NSString*) deviceManufacturer {
    return @"Infinite Peripherals";
}

@end
