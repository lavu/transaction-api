//
//  BlueBambooSwipper.h
//  CardSwiperTest
//
//  Created by Theodore Schnepper on 10/10/13.
//  Copyright (c) 2013 Lavu, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ExternalAccessory/ExternalAccessory.h>
#import "LavuNullSwiper.h"
#import "AppDelegate.h"

@interface LavuEAAccessorySwiper : LavuNullSwiper<NSStreamDelegate> {
    EAAccessory* referencedAccessory;
    EASession* session;
}
+ (NSString*) getName;
+ (NSString*) getProtocol;
+ (NSString*) getManufacturer;

/** The checkForDevice method will iterate through all connected EAAccessories looking for the device that matches across the protocol, manufacturer, and the device name.  Upon finding a device that matches the requirements, this device will be stored as the device that is represented by this class.
 */
- (void) checkForDevice;

/** This method is called via the NSNotification Center upon a new device being connected.
 */
- (void) deviceConnected: (NSNotification*) notification;

/**  This method is called via the NSNotification Center upon an existing device being disconnected.
 */
- (void) deviceDisconnected: (NSNotification*) notification;

/** In the event of an inputStream having data, the stream is passed to this function for custom data parsing/handling.
 */
- (void) handleReadStream: (NSInputStream*) inputStream;

@end
