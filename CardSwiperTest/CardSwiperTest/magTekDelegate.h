//
//  uDynamoDelegate.h
//  POSLavu
//
//  Created by Richard Gieske on 1/31/13.
//
//

#import <Foundation/Foundation.h>
#import "MTSCRA.h"

@class AppDelegate;
@class ExternalDeviceHub;

@interface magTekDelegate : NSObject {
    
    AppDelegate *appDelegate;
    AppDelegate *smai;
    ExternalDeviceHub *edHub;
    
    BOOL initializing;
    
    BOOL processingDataEvent;
    
    //NSTimer *devConnStatusTimer;
    //BOOL lastConnectStatus;
    //BOOL lastOpenedStatus;
}

@property (nonatomic, retain) MTSCRA *mtSCRALib;

@property (nonatomic, retain) NSString *deviceName;
@property (nonatomic, retain) NSString *protocolString;

@property (readwrite, assign) BOOL hasCardReader;
@property (readwrite, assign) BOOL hasChipAndPIN;
@property (readwrite, assign) BOOL hasScanner;
@property (readwrite, assign) BOOL hasPrinter;
@property (readwrite, assign) BOOL connected;

@property (nonatomic, retain) NSString *controlVar;
@property (nonatomic, retain) NSTimer *processTimer;

//@property (nonatomic, retain) NSTimer *devConnStatusTimer;

- (id)init:(NSString *)reader protocol:(NSString *)protocol;

- (void)openDevice:(NSString *)mode;
- (void)closeDevice;
- (void)startProcessTimer:(int)interval;
- (void)stopProcessTimer;
- (void)checkProcess;
- (void)trackDataReady:(NSNotification *)notification;
- (void)onDataEvent:(id)status;
- (void)unsetProcessingDataEventFlag;
//- (void)startDevConnStatusTimer;
//- (void)stopDevConnStatusTimer;
//- (void)devConnStatusCheck;
- (void)devConnStatusChange;
- (BOOL)isConnected;

@end
