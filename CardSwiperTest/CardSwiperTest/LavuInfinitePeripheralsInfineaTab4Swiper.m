//
//  LavuInfinitePeripheralsInfineaTab4Swiper.m
//  CardSwiperTest
//
//  Created by Theodore Schnepper on 11/6/13.
//  Copyright (c) 2013 Lavu, Inc. All rights reserved.
//

#import "LavuInfinitePeripheralsInfineaTab4Swiper.h"

@implementation LavuInfinitePeripheralsInfineaTab4Swiper
- (id) init {
    self = [super init];
    return self;
}

+ (NSString*) getName {
    return @"LIINEATab4";
}
+ (NSString*) getProtocol {
    return @"com.datecs.linea.pro.bar";
}

+ (NSString*) deviceName {
    return @"Infinea Tab 4";
}

+ (NSString*) imageName {
    return @"infinite_peripherals_infinea_tab.png";
}
@end
