//
//  BlueBamboop25iSwipper.m
//  CardSwiperTest
//
//  Created by Theodore Schnepper on 10/11/13.
//  Copyright (c) 2013 Lavu, Inc. All rights reserved.
//

#import "LavuBlueBamboop25iSwiper.h"

@implementation LavuBlueBamboop25iSwiper

- (id) init {
    self = [super init];
    return self;
}

+ (NSString*) getName {
    return @"P25i";
}
+ (NSString*) getProtocol{
    return @"com.bluebamboo.p25i";
}

+ (NSString*) deviceName {
    return @"p25i";
}

+ (NSString*) imageName {
    return @"blue_bamboo_p25i.png";
}
@end
