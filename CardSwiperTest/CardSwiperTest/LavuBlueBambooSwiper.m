//
//  BlueBamboop25iSwipper.m
//  CardSwiperTest
//
//  Created by Theodore Schnepper on 10/11/13.
//  Copyright (c) 2013 Lavu, Inc. All rights reserved.
//

#import "LavuBlueBambooSwiper.h"

@implementation LavuBlueBambooSwiper

- (id) init {
    self = [super init];
    return self;
}

+ (NSString*) getManufacturer {
    return @"BlueBamboo";
}

- (void) prepareForSwipe {
    
    if(!session) {
        if(_delegate){
            [_delegate didPrepareForSwipe:FALSE forSwipper:self];
        }
    }
    
    _prepareForSwipeWhenReady = TRUE;
    //if( [[session outputStream] hasSpaceAvailable] ){
    //    //This byte sequence corresponds to 'Start Listening For a Swipe' on the Blue Bamboo Devices
    //    unsigned const char buffer[6] = { 0x55, 0x66, 0x77, 0x88, 0x48, 0x00 }; // Same as "Ufw H"
    //    return [[session outputStream] write: buffer maxLength: 6] == 6;
    //}
    //return FALSE;
}

- (void) stream: (NSStream *) stream handleEvent:(NSStreamEvent) streamEvent {
    if( (streamEvent & NSStreamEventHasSpaceAvailable) == NSStreamEventHasSpaceAvailable && stream == [session outputStream] && _prepareForSwipeWhenReady){
        _prepareForSwipeWhenReady = FALSE;
        unsigned const char buffer[6] = { 0x55, 0x66, 0x77, 0x88, 0x48, 0x00 }; // Same as "Ufw H"
        BOOL result = [[session outputStream] write: buffer maxLength: 6] == 6;
        if(_delegate){
            [_delegate didPrepareForSwipe:result forSwipper: self];
        }
    }
    [super stream: stream handleEvent: streamEvent];
}

- (void) handleReadStream: (NSInputStream*) inputStream {
    uint8_t buffer[1024];
    for( int i = 0; i < 1024; i++ ){
        buffer[i] = 0;
    }
    unsigned int len = [inputStream read: buffer maxLength: 1024];
    LavuBlueBambooSwipeData* swipeData = [[LavuBlueBambooSwipeData alloc] initWithSwipeBuffer: buffer forLength: len];
    
    if( [[swipeData track1String] length] || [[swipeData track2String] length] || [[swipeData track3String] length]){
        [[NSNotificationCenter defaultCenter] postNotificationName: LavuCardSwipeDataNotification object: swipeData userInfo: NULL];
    }
    
    [swipeData release];
}

+ (LavuCardSwiperType) getSwipperType {
    return LavuCardSwiperTypeBluetooth;
}

+ (NSString*) deviceManufacturer {
    return @"Blue Bamboo";
}

@end
