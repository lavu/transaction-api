//
//  LavuRaspberryPiSwipper.m
//  CardSwiperTest
//
//  Created by Theodore Schnepper on 10/24/13.
//  Copyright (c) 2013 Lavu, Inc. All rights reserved.
//

#import "LavuIDTechRaspberryPiSwipper.h"

@implementation LavuIDTechRaspberryPiSwipper

- (id) init {
    LavuRaspberryPiURLPath = @"local/lls_swipe/lls_swipe_posting.php";
    LavuRaspberryPiConnectionTimeoutDuration = 5.0;
    LavuRaspberryPiSwipeTimeoutDuration = 300.0f;
    self = [super init];
    _isSSL = FALSE;
    _isConnected = FALSE;
    _isListening = FALSE;
    _networkAddress = @"10.0.1.150";
    return self;
}

- (void) dealloc {
    [super dealloc];
}

#pragma mark - Spin That Shit

- (void) reportDidPrepareForSwipeSUCCESS {
    if(_delegate){
        [_delegate didPrepareForSwipe: TRUE forSwipper: self];
    }
}

- (void) reportDidPrepareForSwipeFAILURE {
    if(_delegate){
        [_delegate didPrepareForSwipe: FALSE forSwipper: self];
    }
}

- (void) spinForDuration {
    if(![self isConnected]){
        if(_delegate){
            [self performSelectorOnMainThread: @selector(reportDidPrepareForSwipeFAILURE) withObject: NULL waitUntilDone:TRUE];
        }
        return;
    }
    if(_delegate){
        [self performSelectorOnMainThread: @selector(reportDidPrepareForSwipeSUCCESS) withObject: NULL waitUntilDone:TRUE];
    }
    NSUUID* uuid = [[UIDevice currentDevice] identifierForVendor];
    NSString* protocolString;
    if(_isSSL){
        protocolString = @"https://";
    } else {
        protocolString = @"http://";
    }
    BOOL hasData = FALSE;
    NSURL* connectURL = [NSURL URLWithString: [NSString stringWithFormat: @"%@%@/%@", protocolString, _networkAddress, LavuRaspberryPiURLPath]];
    {
        NSString* caseStr = [NSString stringWithFormat:@"%d", LavuRaspberryPiCommandPostStatusCheck];
        NSString* requestParams = [NSString stringWithFormat: @"%@=%@&%@=%@", @"UDID", [uuid UUIDString], @"Case", caseStr];
        NSMutableURLRequest* urlRequest = [NSMutableURLRequest requestWithURL: connectURL cachePolicy: NSURLCacheStorageNotAllowed timeoutInterval:LavuRaspberryPiConnectionTimeoutDuration];
        [urlRequest setHTTPMethod: @"POST"];
        [urlRequest setHTTPBody: [requestParams dataUsingEncoding: NSASCIIStringEncoding]];
        
        NSDate* startData = [NSDate date];
        
        while( [self isConnected] ){
            if( [startData timeIntervalSinceNow] < -LavuRaspberryPiSwipeTimeoutDuration ){
                hasData = FALSE;
                break;
            }
            [NSThread sleepForTimeInterval: 0.1];
            NSError* error = NULL;
            NSURLResponse* response = NULL;
            NSData* responseData = [NSURLConnection sendSynchronousRequest: urlRequest returningResponse: &response error: &error];
            
            if( error || [responseData length] < 1){
                hasData = FALSE;
                break;
            } else {
                // 0 Device Isn't in Use
                // 1 Device is Ready, No Data
                // 2 Device is Receiving Data
                // 3 Device is Ready and has Data Ready
                const char * bytes = [responseData bytes];
                if( bytes[0] == '\x33' ) {
                    hasData = TRUE;
                    break;
                } else {
                    continue;
                }
            }
        }
    }
    
    //Should have Data? Let's Fetch it
    {
        if(!hasData){
            [self disconnect];
            return;
        }
        
        NSString* caseStr = [NSString stringWithFormat:@"%d", LavuRaspberryPiCommandGetPost];
        NSString* requestParams = [NSString stringWithFormat: @"%@=%@&%@=%@", @"UDID", [uuid UUIDString], @"Case", caseStr];
        NSMutableURLRequest* urlRequest = [NSMutableURLRequest requestWithURL: connectURL cachePolicy: NSURLCacheStorageNotAllowed timeoutInterval:LavuRaspberryPiConnectionTimeoutDuration];
        [urlRequest setHTTPMethod: @"POST"];
        [urlRequest setHTTPBody: [requestParams dataUsingEncoding: NSASCIIStringEncoding]];
        
        NSError* error = NULL;
        NSURLResponse* response = NULL;
        NSData* responseData = [NSURLConnection sendSynchronousRequest: urlRequest returningResponse: &response error: &error];
        if(error || [responseData length] < 1){
            [self disconnect];
            return;
        } else {
            NSLog( @"%@", [[[NSString alloc] initWithData: responseData encoding: NSASCIIStringEncoding] autorelease]);
            [[NSNotificationCenter defaultCenter] postNotificationName: LavuCardSwipeDataNotification object: [[[IDTechCardSwipe alloc] initWithData: responseData] autorelease] userInfo: NULL];
            [self disconnect];
            
        }
    }
}

#pragma mark - LavuCardSwipperProtocol Overrides

- (void) connect {
    NSString* protocolString;
    if(_isSSL){
        protocolString = @"https://";
    } else {
        protocolString = @"http://";
    }
    NSURL* connectURL = [NSURL URLWithString: [NSString stringWithFormat: @"%@%@/%@", protocolString, _networkAddress, LavuRaspberryPiURLPath]];
    NSMutableURLRequest* urlRequest = [NSMutableURLRequest requestWithURL: connectURL cachePolicy: NSURLCacheStorageNotAllowed timeoutInterval:LavuRaspberryPiConnectionTimeoutDuration];
    [urlRequest setHTTPMethod: @"POST"];
    //[urlRequest setValue: @"application/json" forHTTPHeaderField: @"Content-Type"];
    
    NSUUID* uuid = [[UIDevice currentDevice] identifierForVendor];
    NSString* caseStr = [NSString stringWithFormat:@"%d", LavuRaspberryPiCommandIsPaired];
    
    NSString* requestParams = [NSString stringWithFormat: @"%@=%@&%@=%@", @"UDID", [uuid UUIDString], @"Case", caseStr];
    [urlRequest setHTTPBody: [requestParams dataUsingEncoding: NSASCIIStringEncoding]];
    
    //[urlRequest setValue: [uuid UUIDString] forKey:@"UDID"];
    //[urlRequest setValue: [NSString stringWithFormat:@"%d", LavuRaspberryPiCommandIsPaired] forKey:@"Case"];
    
    NSError* error = NULL;
    NSURLResponse* response = NULL;
    NSData* responseData = [NSURLConnection sendSynchronousRequest: urlRequest returningResponse: &response error: &error];
    
    if( error ){
        if(_delegate){
            [_delegate didConnect: FALSE forSwipper: self];
        }
    } else {
        if( [responseData length] >= 1 ){
            const char * bytes = [responseData bytes];
            if( bytes[0] == '\x31' ){
                if(_delegate){
                    _isConnected = TRUE;
                    
                }
            } else {
                if(_delegate){
                    _isConnected = FALSE;
                }
            }
        }
    }
    [_delegate didConnect: _isConnected forSwipper: self];
}

- (BOOL) isConnected {
    return _isConnected;
}

- (void) disconnect {
    if( [self isListening] ){
        [self stopListening];
    }
    
    _isConnected = FALSE;
    if(_delegate){
        [_delegate didDisconnect: TRUE forSwipper: self];
    }
}

- (void) startListening {
    if(_delegate){
        [_delegate didStartListening:[self isConnected] forSwipper: self];
    }
}

- (BOOL) isListening {
    return [self isConnected];
}

- (void) stopListening {
    if(_delegate){
        [_delegate didStopListening: TRUE forSwipper: self];
    }
}


- (void) prepareForSwipe {
    [NSThread detachNewThreadSelector: @selector(spinForDuration) toTarget: self withObject:NULL];
}


+ (LavuCardSwiperType) getSwipperType {
    return LavuCardSwiperTypeNetworkSwiper;
}

+ (NSString*) deviceManufacturer {
    return @"Lavu - ID Tech";
}

+ (NSString*) deviceName {
    return @"Pi Swipper";
}
@end
