//
//  LavuNullSwipper.m
//  CardSwiperTest
//
//  Created by Theodore Schnepper on 10/15/13.
//  Copyright (c) 2013 Lavu, Inc. All rights reserved.
//

#import "LavuNullSwiper.h"

@implementation LavuNullSwiper
- (id) init {
    self = [super init];
    return self;
}

- (void) dealloc {
    if( _delegate && _delegate != (id<LavuSwiperDelegate>)self){
        [_delegate release];
    }
    
    if([self isListening] ){
        [self stopListening];
    }
    
    if( [self isConnected]){
        [self disconnect];
    }
    [super dealloc];
}

- (void) connect {
    if(_delegate){
        [_delegate didConnect: FALSE forSwipper: self];
    }
}
- (BOOL) isConnected {
    return FALSE;
}
- (void) disconnect {
    if(_delegate){
        [_delegate didDisconnect: FALSE forSwipper: self];
    }
}
- (void) startListening {
    if(_delegate){
        [_delegate didStartListening: FALSE forSwipper: self];
    }
}
- (BOOL) isListening {
    return FALSE;
}
- (void) stopListening {
    if(_delegate){
        [_delegate didStopListening: FALSE forSwipper: self];
    }
}
- (void) prepareForSwipe {
    if(_delegate){
        [_delegate didPrepareForSwipe: FALSE forSwipper: self];
    }
}
+ (LavuCardSwiperType) getSwipperType {
    return LavuCardSwiperTypeUNKNOWN;
}

- (BOOL) setDelegate:(id<LavuSwiperDelegate>) delegate {
    if( [delegate conformsToProtocol: @protocol(LavuSwiperDelegate)] ) {
        if(_delegate){
            [_delegate release];
        }
        _delegate = [delegate retain];
        return TRUE;
    }
    return FALSE;
}

+ (NSString*) deviceManufacturer {
    return NULL;
}

+ (NSString*) deviceName {
    return NULL;
}

+ (NSString*) imageName {
    return NULL;
}

@end
