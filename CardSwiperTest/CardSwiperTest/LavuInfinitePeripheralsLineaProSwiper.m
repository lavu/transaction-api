//
//  LavuInfinitePeripheralsLineaPro4.m
//  CardSwiperTest
//
//  Created by Theodore Schnepper on 10/28/13.
//  Copyright (c) 2013 Lavu, Inc. All rights reserved.
//

#import "LavuInfinitePeripheralsLineaProSwiper.h"

@implementation LavuInfinitePeripheralsLineaProSwiper
- (id) init {
    self = [super init];
    return self;
}

+ (NSString*) getName {
    return @"LINEAPro4L";
}
+ (NSString*) getProtocol {
    return @"com.datecs.linea.pro.msr";
}

+ (NSString*) deviceName {
    return @"Linea Pro";
}
@end
