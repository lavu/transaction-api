//
//  ExternalDeviceHub.h
//  POSLavu
//
//  Created by Richard Gieske on 3/28/13.
//
//

#import <Foundation/Foundation.h>
#import "uniMag.h"
#import "MTSCRA.h"

@class AppDelegate;
@class ViewController;

@interface ExternalDeviceHub : NSObject <UIAlertViewDelegate> {
    
    AppDelegate *appDelegate;
    AppDelegate *smai;
    
    uniMag *uniReader;

    NSMutableDictionary *connectedDevices;
    
    BOOL doubleMagTekWarningIsShowing;
}

@property (nonatomic, retain) MTSCRA *mtSCRALib;

@property (nonatomic, retain) NSTimer *deviceCheckTimer;

@property (nonatomic, retain) NSString *lastSwipeReaderName;
@property (nonatomic, retain) NSString *lastSwipeString;
@property (nonatomic, retain) NSString *lastSwipeDataString;
@property (readwrite, assign) BOOL lastSwipeIsEncrypted;

@property (nonatomic, retain) NSString *lastScanScannerName;
@property (nonatomic, retain) NSString *lastScanString;

@property (nonatomic, retain) UIWebView *componentListeningForCardSwipe;
@property (nonatomic, retain) UIWebView *componentListeningForScan;

- (id)init;

- (void)startDeviceCheckTimer;
- (void)stopDeviceCheckTimer;
- (void)appDidBecomeActive;
- (void)appWillResignActive;
- (void)deinitializeAll;
- (void)checkExternalDevices;
- (void)checkExternalDevicesInBackground;
- (BOOL)shouldListen;
- (void)startListening;
- (void)stopListening;
- (void)startReaderActivityIndicator;
- (void)stopReaderActivityIndicator;
- (void)processSwipe:(NSString *)swipeString dataString:(NSString *)dataString reader:(NSString *)reader encrypted:(BOOL)encrypted;
- (void)clearSwipeData;
- (BOOL)hasCardReader;
- (void)changeTabSearchButtonState:(NSString *)state;
- (void)cardIcon:(NSString *)state;
- (BOOL)hasScanner;
- (void)scanDown;
- (void)scanUp;
- (BOOL)deviceIsConnected:(NSString *)deviceName;

// BlueBamboo

//- (void)initializeBlueBamboo:(NSDictionary *)info;
- (void)BBprintText:(NSString *)prntStrng sender:(id)sender;

// UniMag

- (void)addUniMagObservers;
- (void)removeUniMagObservers;
- (void)initializeUniMag;
- (void)deinitializeUniMag;
- (void)uniAccessoryAttach:(NSNotification *)notification;
- (void)uniInsufficientPower:(NSNotification *)notification;
- (void)uniPowering:(NSNotification *)notification;
- (void)uniTimeout:(NSNotification *)notification;
- (void)uniAccessoryConnected:(NSNotification *)notification;
- (void)uniPostConnect;
- (void)uniAccessoryDisconnected:(NSNotification *)notification;
- (void)uniSwipe:(NSNotification *)notification;
- (void)uniTimeoutSwipe:(NSNotification *)notification;
- (void)uniDataProcessing:(NSNotification *)notification;
- (void)uniInvalidSwipe:(NSNotification *)notification;
- (void)accessoryData:(NSNotification *)notification;
- (void)uniCommandSending:(NSNotification *)notification;
- (void)uniCommandTimeout:(NSNotification *)notification;
- (void)accessoryCommand:(NSNotification *)notification;
- (void)uniSystemMessage:(NSNotification *)notification;
- (void)resetUniMagSwipe;
- (void)requestUniMagSwipe;
- (void)cancelUniMagSwipe;

// magTek

- (void)initializeMagTek:(NSDictionary *)info;
- (void)deinitializeMagTek:(NSString *)reader;
- (void)magTekConnectionFailed:(NSString *)mode reader:(NSString *)reader;
- (void)doubleMagTekWarning:(NSString *)reader;

@end
