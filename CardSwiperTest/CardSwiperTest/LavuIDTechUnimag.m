//
//  IDTechUnimag.m
//  CardSwiperTest
//
//  Created by Theodore Schnepper on 10/15/13.
//  Copyright (c) 2013 Lavu, Inc. All rights reserved.
//

#import "LavuIDTechUnimag.h"

@implementation LavuIDTechUnimag

#pragma mark - init and dealloc
- (id) init {
    self = [super init];
    //Notification Registration
    {
        [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector( physicalDeviceAttached: ) name: uniMagAttachmentNotification object: NULL];
        [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector( physicalDeviceDeattached: ) name: uniMagDetachmentNotification object: NULL];
    
        [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector( connectionInsufficientPower: ) name: uniMagInsufficientPowerNotification object: NULL];
        [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector( connectionPowering: ) name: uniMagPoweringNotification object: NULL];
        [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector( connectionTimeout: ) name: uniMagTimeoutNotification object: NULL];
        [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector( connectionDeviceConnected: ) name: uniMagDidConnectNotification object: NULL];
        [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector( connectionDeviceDisconnected: ) name: uniMagDidDisconnectNotification object: NULL];
    
        [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector( swipeRecieved: ) name: uniMagSwipeNotification object: NULL];
        [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector( swipeTimeout: ) name: uniMagTimeoutSwipeNotification object: NULL];
        [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector( swipeProcessingData: ) name: uniMagDataProcessingNotification object: NULL];
        [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector( swipeInvalid: ) name: uniMagInvalidSwipeNotification object: NULL];
        [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector( swipeRecievedData: ) name: uniMagDidReceiveDataNotification object: NULL];
    
        [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector( commandSending: ) name: uniMagCmdSendingNotification object: NULL];
        [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector( commandTimeout: ) name: uniMagCommandTimeoutNotification object: NULL];
        [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector( commandRecieved: ) name: uniMagDidReceiveCmdNotification object: NULL];
    
        [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector( miscSystemMessage: ) name: uniMagSystemMessageNotification object: NULL];
    }
    unimagLib = [[uniMag alloc] init];
    [unimagLib setAutoAdjustVolume: TRUE];
    [unimagLib setSwipeTimeoutDuration: 300];
    [unimagLib setCmdTimeoutDuration: 60];
    return self;
}

- (void) dealloc {
    [super dealloc];
    [unimagLib release];
    //Notification Removal
    {
        [[NSNotificationCenter defaultCenter] removeObserver: self name: uniMagAttachmentNotification object: NULL];
        [[NSNotificationCenter defaultCenter] removeObserver: self name: uniMagDetachmentNotification object: NULL];
        
        [[NSNotificationCenter defaultCenter] removeObserver: self name: uniMagInsufficientPowerNotification object: NULL];
        [[NSNotificationCenter defaultCenter] removeObserver: self name: uniMagPoweringNotification object: NULL];
        [[NSNotificationCenter defaultCenter] removeObserver: self name: uniMagTimeoutNotification object: NULL];
        [[NSNotificationCenter defaultCenter] removeObserver: self name: uniMagDidConnectNotification object: NULL];
        [[NSNotificationCenter defaultCenter] removeObserver: self name: uniMagDidDisconnectNotification object: NULL];
        
        [[NSNotificationCenter defaultCenter] removeObserver: self name: uniMagSwipeNotification object: NULL];
        [[NSNotificationCenter defaultCenter] removeObserver: self name: uniMagTimeoutSwipeNotification object: NULL];
        [[NSNotificationCenter defaultCenter] removeObserver: self name: uniMagDataProcessingNotification object: NULL];
        [[NSNotificationCenter defaultCenter] removeObserver: self name: uniMagInvalidSwipeNotification object: NULL];
        [[NSNotificationCenter defaultCenter] removeObserver: self name: uniMagDidReceiveDataNotification object: NULL];
        
        [[NSNotificationCenter defaultCenter] removeObserver: self name: uniMagCmdSendingNotification object: NULL];
        [[NSNotificationCenter defaultCenter] removeObserver: self name: uniMagCommandTimeoutNotification object: NULL];
        [[NSNotificationCenter defaultCenter] removeObserver: self name: uniMagDidReceiveCmdNotification object: NULL];
        
        [[NSNotificationCenter defaultCenter] removeObserver: self name: uniMagSystemMessageNotification object: NULL];
    }
}
#pragma mark - Physical Device
- (void) physicalDeviceAttached: (NSNotification*) notification {
    NSDictionary* dict = [notification userInfo];
    NSLog(@"Device Attached: %@", dict);
    //[self connect];
}

- (void) physicalDeviceDeattached: (NSNotification*) notification {
    NSDictionary* dict = [notification userInfo];
    NSLog(@"Device Deattached: %@", dict);
    if(_delegate){
        [_delegate didDisconnect: TRUE forSwipper: self];
    }
}

#pragma mark - Helpful Quck Reporting

- (void) report {
    if( currentTarget ) {
        [self performSelector: currentTarget withObject: objToPassSelector afterDelay: 0.0f];
        currentTarget = NULL;
        objToPassSelector = NULL;
    }
}

- (void) sendConnectStatus: (BOOL) success {
    if(_delegate){
        [_delegate didConnect: success forSwipper: self];
    }
}

- (void) sendDisconnectStatus: (BOOL) success {
    if(_delegate){
        [_delegate didDisconnect: success forSwipper: self];
    }
}

- (void) sendStartListenStatus: (BOOL) success {
    if(_delegate ){
        [_delegate didStartListening: success forSwipper: self];
    }
}

- (void) sendStopListenStatus: (BOOL) success {
    if(_delegate){
        [_delegate didStopListening: success forSwipper: self];
    }
}

- (void) sendDidPrepareForSwipeStatus: (BOOL) success {
    if(_delegate){
        [_delegate didPrepareForSwipe: success forSwipper: self];
    }
}

#pragma mark  - Device Connection
- (void) connectionInsufficientPower: (NSNotification*) notification {
    [self sendStartListenStatus: FALSE];
}

- (void) connectionPowering: (NSNotification*) notification {
    //All Good
}

- (void) connectionTimeout: (NSNotification*) notification {
    [self sendStartListenStatus: FALSE];
}

- (void) connectionDeviceConnected: (NSNotification*) notification {
    [unimagLib sendCommandClearBuffer];
    [self sendStartListenStatus: TRUE];
}

- (void) connectionDeviceDisconnected: (NSNotification*) notification {
    [self sendStopListenStatus: TRUE];
}

#pragma mark  - Swipe Information
BOOL reportSuccess = FALSE;
- (void) swipeRecieved: (NSNotification*) notification {
    //Doesn't Do us any good to know this, really
    if( !shouldReListenAfterFirstConnection && reportSuccess ){
        [self sendDidPrepareForSwipeStatus: TRUE];
        reportSuccess = FALSE;
    }
}

- (void) swipeTimeout: (NSNotification*) notification {
    [self stopListening]; // Timeout... we could either prepare for a swipe again, or die
}

- (void) swipeProcessingData: (NSNotification*) notification {
    //Nothing really useful here
 
}

- (void) swipeInvalid: (NSNotification*) notification {
    //After the inital prepare, we are always guaranteed to get this once.  Stupid unimag swipper.
    //if(shouldReListenAfterFirstConnection){
    NSLog(@"Invalid %@", [notification object]);
    NSLog(@"Invalid %@", [[NSString alloc] initWithData:[notification object] encoding: NSASCIIStringEncoding]);
        shouldReListenAfterFirstConnection = FALSE;
        reportSuccess = TRUE;
        [unimagLib requestSwipe];
    //}
}

- (void) swipeRecievedData: (NSNotification*) notification {
    id obj = [notification object];
    [[NSNotificationCenter defaultCenter] postNotificationName: LavuCardSwipeDataNotification object: [[[IDTechCardSwipe alloc] initWithData: obj] autorelease] userInfo: NULL];
    [self stopListening];
}

#pragma mark  - Connection Information
- (void) commandSending: (NSNotification*) notification {
    //We are sending a command, I guess
}

- (void) finishCommandProcessing {
    if(shouldPrepareForSwipeWhenReady){
        shouldPrepareForSwipeWhenReady = FALSE;
        UmRet status = [unimagLib requestSwipe];
        BOOL result = FALSE;
        switch( status ){
            case UMRET_SUCCESS: {
                result = TRUE;
                break;
            } case UMRET_LOW_VOLUME: {
            } case UMRET_NO_READER: {
            } case UMRET_SDK_BUSY: {
            } case UMRET_NOT_CONNECTED: {
            } default:
                result = FALSE;
        }
    }
}

- (void) commandTimeout: (NSNotification*) notification {
    //Time out... hooray... which command?
    [self finishCommandProcessing];
}

- (void) commandRecieved: (NSNotification*) notification {
    // Wish I knew what the command was.  It's likely that I'm only sending clear buffer commands, so that should be the only command.
    // Otherwisse, command tracking would be neccessary.
    [self finishCommandProcessing];
}
#pragma mark  - Misc Information
- (void) miscSystemMessage: (NSNotification*) notification {
    NSDictionary* dict = [notification userInfo];
    NSLog(@"Misc Info: %@", dict);
}

# pragma mark - Overwritting inheritence
- (void) connect {
    [self sendConnectStatus: [unimagLib isReaderAttached]];
}

- (BOOL) isConnected {
    return [unimagLib isReaderAttached] && [unimagLib getConnectionStatus];
}

- (void) disconnect {
    if( [self isListening] ){
        [self stopListening];
    } else {
        [self sendDisconnectStatus: TRUE];
    }
}

- (void) startListening {
    if(! [unimagLib isReaderAttached] ){
        [self sendStartListenStatus: FALSE];
        return;
    }
    
    if( [unimagLib getConnectionStatus] ){
        [self sendStartListenStatus: TRUE];
        return;
    }
    
    BOOL result = FALSE;
    UmRet status = [unimagLib startUniMag: TRUE];
    switch( status ){
        case UMRET_ALREADY_CONNECTED: {
        } case UMRET_SUCCESS: {
            result = TRUE;
            break;
        } case UMRET_NO_READER: {
        } case UMRET_SDK_BUSY: {
        } default:
            result = FALSE;
    }
    
    if(!result){
        [self sendStartListenStatus: FALSE];
    } else {
        shouldReListenAfterFirstConnection = TRUE;
    }
}

- (BOOL) isListening {
    return [unimagLib isReaderAttached] && [unimagLib getConnectionStatus];
}

- (void) stopListening {
    if(! [unimagLib isReaderAttached] ){
        [self sendStopListenStatus: TRUE];
        return;
    }
    
    if(! [unimagLib getConnectionStatus] ){
        [self sendStopListenStatus: TRUE];
        return;
    }
    
    BOOL result = FALSE;
    UmRet status = [unimagLib startUniMag: FALSE];
    switch( status ){
        case UMRET_SUCCESS: {
            result = TRUE;
            break;
        } case UMRET_NO_READER: {
        } case UMRET_SDK_BUSY: {
        } case UMRET_ALREADY_CONNECTED: { //Wat
        } default:
            result = FALSE;
    }
    
    if(!result){
        [self sendStopListenStatus: result];
    }
}

- (void) prepareForSwipe {
    if( ![unimagLib isReaderAttached] || ![unimagLib getConnectionStatus] ){
        [self sendDidPrepareForSwipeStatus: FALSE];
        return;
    }
    
    UmTask task = [unimagLib getRunningTask];
    if( UMTASK_NONE == task ){
        [unimagLib requestSwipe];
    } else {
        shouldPrepareForSwipeWhenReady = TRUE;
    }
}

+ (LavuCardSwiperType) getSwipperType {
    return LavuCardSwiperTypeAudioJack;
}

+ (NSString*) deviceName {
    return @"Unimag";
}

+ (NSString*) imageName {
    return @"unimag_221x220.png";
}

@end
