//
//  uDynamoDelegate.m
//  POSLavu
//
//  Created by Richard Gieske on 1/31/13.
//
//

#import "magTekDelegate.h"
#import "AppDelegate.h"
#import "ExternalDeviceHub.h"

@implementation magTekDelegate

@synthesize mtSCRALib;
@synthesize deviceName, protocolString;
@synthesize hasCardReader, hasChipAndPIN, hasScanner, hasPrinter, connected;
@synthesize controlVar, processTimer;
//@synthesize devConnStatusTimer;

- (id)init:(NSString *)reader protocol:(NSString *)protocol {
    
    self = [super init];
    if (self) {
        
        appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        smai = appDelegate;
        edHub = appDelegate.externalDeviceHub;
        
        [smai rLog:[NSString stringWithFormat:@"init magTekDelegate: %@", reader]];
        
        self.mtSCRALib = [edHub mtSCRALib];
        [self.mtSCRALib listenForEvents:(TRANS_EVENT_START|TRANS_EVENT_OK|TRANS_EVENT_ERROR)];
        
        self.deviceName = reader;
        self.protocolString = protocol;
        self.hasCardReader = YES;
        self.hasChipAndPIN = NO;
        self.hasScanner = NO;
        self.hasPrinter = NO;
        self.connected = NO;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(trackDataReady:) name:@"trackDataReadyNotification" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(devConnStatusChange) name:@"devConnectionNotification" object:nil];
        
        if ([reader isEqualToString:@"udynamo"]) [self.mtSCRALib setDeviceType:MAGTEKAUDIOREADER];
        else if ([reader isEqualToString:@"idynamo"]) {
            [self.mtSCRALib setDeviceType:MAGTEKIDYNAMO];
            [self.mtSCRALib setDeviceProtocolString:(protocol)];
        }
        
        [self openDevice:@"initializing"];
    }
    
    return self;
}

- (void)openDevice:(NSString *)mode {
    
    [smai rLog:[NSString stringWithFormat:@"magTek openDevice: %@", mode]];
    
    [edHub startReaderActivityIndicator];
    
    [self.mtSCRALib openDevice];
    self.controlVar = mode;
    [self startProcessTimer:1];
}

- (void)closeDevice {
    
    [smai rLog:@"magTek closeDevice"];
    
    if ([mtSCRALib isDeviceOpened]) [self.mtSCRALib closeDevice];
    [self.mtSCRALib clearBuffers];
    [edHub stopReaderActivityIndicator];
}

- (void)startProcessTimer:(int)interval {
    
    [self stopProcessTimer];
    self.processTimer = [NSTimer scheduledTimerWithTimeInterval:interval target:self selector:@selector(checkProcess) userInfo:nil repeats:NO];
}

- (void)stopProcessTimer {
    
    if (self.processTimer && [self.processTimer isKindOfClass:[NSTimer class]]) [self.processTimer invalidate];
    initializing = NO;
}

- (void)checkProcess {
    
    if ([self.controlVar isEqualToString:@"initializing"] || [self.controlVar isEqualToString:@"listening"]) {
        
        [edHub stopReaderActivityIndicator];
        
        if ([mtSCRALib isDeviceOpened]) {
            self.connected = YES;
            [smai rLog:@"magTek device is opened"];
            [edHub cardIcon:@"up"];
            if ([self.controlVar isEqualToString:@"initializing"]) {
                //lastConnectStatus = [mtSCRALib isDeviceConnected];
                //lastOpenedStatus = [mtSCRALib isDeviceOpened];
                //[self startDevConnStatusTimer];
                [edHub changeTabSearchButtonState:@"1"];
                if (![edHub shouldListen] && [self.deviceName isEqualToString:@"udynamo"]) [self closeDevice];
            }
        } else if ([mtSCRALib isDeviceConnected]) {
            [smai rLog:@"magTek device is connected"];
            [edHub cardIcon:@"up"];
        } else {
            if (![edHub hasCardReader]) [edHub cardIcon:@"error"];
            [edHub magTekConnectionFailed:self.controlVar reader:self.deviceName];
        }
        
        self.controlVar = @"";
    }
}

- (void)trackDataReady:(NSNotification *)notification {
    
    NSNumber *status = [[notification userInfo] valueForKey:@"status"];
    
    [self performSelectorOnMainThread:@selector(onDataEvent:) withObject:status waitUntilDone:NO]; // must perform onDataEvent: on Main Thread
}

- (void)onDataEvent:(id)status {
    
    [smai rLog:@"magTek onDataEvent"];
    
	switch ([status intValue]) {
            
        case TRANS_STATUS_OK:
            
            [smai rLog:@"magTek TRANS_STATUS_OK"];
            
            if (!processingDataEvent) {
                
                processingDataEvent = YES;
                [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(unsetProcessingDataEventFlag) userInfo:nil repeats:NO];
                
                [edHub stopReaderActivityIndicator];
                
                BOOL bTrackError = NO;
                NSString * pstrTrackDecodeStatus = [self.mtSCRALib getTrackDecodeStatus];
                //[self logData];
                
                NSString *maskedTracks = [self.mtSCRALib getMaskedTracks];
                NSString *encryptionStatus = [self.mtSCRALib getEncryptionStatus];
                NSString *track1 = [self.mtSCRALib getTrack1];
                NSString *track2 = [self.mtSCRALib getTrack2];
                NSString *track3 = [self.mtSCRALib getTrack3];
                NSString *magnePrintStatus = [self.mtSCRALib getMagnePrintStatus];
                NSString *magnePrint = [self.mtSCRALib getMagnePrint];
                NSString *deviceSerial = [self.mtSCRALib getDeviceSerial];
                NSString *sessionID = [self.mtSCRALib getSessionID];
                NSString *ksn = [self.mtSCRALib getKSN];
                
                if (maskedTracks == nil) maskedTracks = @"";
                if (encryptionStatus == nil) encryptionStatus = @"";
                if (track1 == nil) track1 = @"";
                if (track2 == nil) track2 = @"";
                if (track3 == nil) track3 = @"";
                if (magnePrintStatus == nil) magnePrintStatus = @"";
                if (magnePrint == nil) magnePrint = @"";
                if (deviceSerial == nil) deviceSerial = @"";
                if (sessionID == nil) sessionID = @"";
                if (ksn == nil) ksn = @"";
                
                NSMutableArray *magArray = [[NSMutableArray alloc] init];
                [magArray addObject:maskedTracks];
                [magArray addObject:encryptionStatus];
                [magArray addObject:track1];
                [magArray addObject:track2];
                [magArray addObject:track3];
                [magArray addObject:magnePrintStatus];
                [magArray addObject:magnePrint];
                [magArray addObject:deviceSerial];
                [magArray addObject:sessionID];
                [magArray addObject:ksn];
                NSString *swipeString = [magArray componentsJoinedByString:@"|"];
                
                //NSLog(@"magArray: %@", magArray);
                //NSLog(@"swipeString: %@", swipeString);
                
                [magArray release];
                
                @try {
                    
                    if (pstrTrackDecodeStatus) {
                        
                        if (pstrTrackDecodeStatus.length >= 6) {
                            
                            NSString * pStrTrack1Status = [pstrTrackDecodeStatus substringWithRange:NSMakeRange(0, 2)];
                            NSString * pStrTrack2Status = [pstrTrackDecodeStatus substringWithRange:NSMakeRange(2, 2)];
                            NSString * pStrTrack3Status = [pstrTrackDecodeStatus substringWithRange:NSMakeRange(4, 2)];
                            if (pStrTrack1Status && pStrTrack2Status) { // && pStrTrack3Status
                                if (([pStrTrack1Status compare:@"01"] == NSOrderedSame) && ([pStrTrack2Status compare:@"01"] == NSOrderedSame)) bTrackError = YES;
                                //if ([pStrTrack3Status compare:@"01"] == NSOrderedSame) bTrackError = YES;
                            }
                            [smai rLog:[NSString stringWithFormat:@"Track1.Status = %@", pStrTrack1Status]];
                            [smai rLog:[NSString stringWithFormat:@"Track2.Status = %@", pStrTrack2Status]];
                            [smai rLog:[NSString stringWithFormat:@"Track3.Status = %@", pStrTrack3Status]];
                        }
                    }
                }
                
                @catch (NSException * e) { }
                
                [appDelegate dcLog:[NSString stringWithFormat:@"swipeString: %@, reader: %@", swipeString, self.deviceName]];
                
                if (!bTrackError) [edHub processSwipe:swipeString dataString:@"" reader:self.deviceName encrypted:YES];
                else [smai standardAlert:[smai ttal:@"Please try again."] withTitle:[smai ttal:@"Card Read Failed"]];
            }
            
            break;
            
        case TRANS_STATUS_START:
            
            [smai rLog:@"magTek TRANS_STATUS_START"];
            
            [edHub startReaderActivityIndicator];
            
            break;
            
        case TRANS_STATUS_ERROR:
            
            if (self.mtSCRALib != NULL) {
                
                [smai rLog:@"magTek TRANS_STATUS_ERROR"];
            }
            
            [edHub stopReaderActivityIndicator];
            
            break;
            
        default:
            
            break;
    }
}

- (void)unsetProcessingDataEventFlag {
    
    processingDataEvent = NO;
}

/*- (void)startDevConnStatusTimer {

    [self stopDevConnStatusTimer];
    self.devConnStatusTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(devConnStatusCheck) userInfo:nil repeats:NO];
}
 
- (void)stopDevConnStatusTimer {
 
    if (self.devConnStatusTimer && [self.devConnStatusTimer isKindOfClass:[NSTimer class]]) [self.devConnStatusTimer invalidate];
}
 
- (void)devConnStatusCheck {
 
    [appDelegate dcLog:[NSString stringWithFormat:@"%i %i %i %i - %@ - %@", [mtSCRALib isDeviceConnected], lastConnectStatus, [mtSCRALib isDeviceOpened], lastOpenedStatus, [mtSCRALib getDeviceStatus], [mtSCRALib getDeviceName]]];
 
    if (([mtSCRALib isDeviceConnected] != lastConnectStatus) || ([mtSCRALib isDeviceOpened] != lastOpenedStatus)) {
        NSLog(@"devConnStatusCheck triggering devConnStatusChange");
        [appDelegate dcLog:@"devConnStatusCheck triggering devConnStatusChange"];
        [self devConnStatusChange];
    } else [self startDevConnStatusTimer];
}*/

- (void)devConnStatusChange {
    
    [smai rLog:@"devConnStatusChange"];
    
    //NSLog(@"magTek connected: %i", [mtSCRALib isDeviceConnected]);
    //NSLog(@"magTek open: %i", [mtSCRALib isDeviceOpened]);
    
    //lastConnectStatus = [mtSCRALib isDeviceConnected];
    //lastOpenedStatus = [mtSCRALib isDeviceOpened];
    
    BOOL closeSelf = NO;
    if ([mtSCRALib isDeviceConnected]) {
        [edHub changeTabSearchButtonState:@"1"];
        if ([mtSCRALib isDeviceOpened]) [edHub cardIcon:@"up"];
    } else {
        self.connected = NO;
        if (![edHub hasCardReader]) {
            [edHub changeTabSearchButtonState:@"0"];
            [edHub cardIcon:@"down"];
        }
        closeSelf = YES;
    }
    
    [edHub stopReaderActivityIndicator];
    
    [smai rLog:[NSString stringWithFormat:@"magTek connected (2): %i", [mtSCRALib isDeviceConnected]]];
    [smai rLog:[NSString stringWithFormat:@"magTek open (2): %i", [mtSCRALib isDeviceOpened]]];
    
    if (closeSelf) {
        //[self stopDevConnStatusTimer];
        [edHub deinitializeMagTek:self.deviceName];
    } //else [self startDevConnStatusTimer];
}

- (BOOL)isConnected {
    
    return ([mtSCRALib isDeviceConnected] && [mtSCRALib isDeviceOpened]);
}

- (void)dealloc {
    
    [smai rLog:@"dealloc magTekDelegate"];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"trackDataReadyNotification" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"devConnectionNotification" object:nil];
    
    [self stopProcessTimer];
    //[self stopDevConnStatusTimer];
    [self closeDevice];
    [[self mtSCRALib] release];
    
    [deviceName release];
    [protocolString release];
    [controlVar release];
    [processTimer release];
    //[devConnStatusTimer release];
    
    [super dealloc];
}

@end
