//
//  IDTechCardSwipe.h
//  CardSwiperTest
//
//  Created by Theodore Schnepper on 10/17/13.
//  Copyright (c) 2013 Lavu, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LavuNullSwipeData.h"

enum {
    IDTechtrack1DecodedSuccessfully   = 1 << 0,
    IDTechtrack2DecodedSuccessfully   = 1 << 1,
    IDTechtrack3DecodedSuccessfully   = 1 << 2,
    IDTechtrack1SamplingDataPresent   = 1 << 3,
    IDTechtrack2SamplingDataPresent   = 1 << 4,
    IDTechtrack3SamplingDataPresent   = 1 << 5,
    IDTechtrackStatusReserved1        = 1 << 6,
    IDTechtrackStatusReserved2        = 1 << 7
} IDTechTrackStatus;

enum {
    IDTechTrack1ClearMaskDataPresent  = 1 << 0,
    IDTechTrack2ClearMaskDataPresent  = 1 << 1,
    IDTechTrack3ClearMaskDataPresent  = 1 << 2,
    IDTechFieldByte1NotUsed1          = 1 << 3,
    IDTechFieldByte1TDESEncryption1   = 1 << 4,
    IDTechFieldByte1TDESEncryption2   = 1 << 5,
    IDTechFieldByte1NotUsed2          = 1 << 6,
    IDTechSerialNumberIsAvailable     = 1 << 7
} IDTechFieldByte1;

enum {
    IDTechTrack1EncryptedDataPresent = 1 << 0,
    IDTechtrack2EncryptedDataPresent = 1 << 1,
    IDTechtrack3EncryptedDataPresent = 1 << 2,
    IDTechFieldByte2NotUsed1          = 1 << 3,
    IDTechFieldByte2NotUsed2          = 1 << 4,
    IDTechFieldByte2NotUsed3          = 1 << 5,
    IDTechFieldByte2NotUsed4          = 1 << 6,
    IDTechKSNPresent                  = 1 << 7
} IDTechFieldByte2;

@interface IDTechCardSwipe : LavuNullSwipeData
- (id) initWithData: (NSData*) swipeData;
@end
