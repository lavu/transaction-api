//
//  ExternalDeviceDelegate.h
//  POSLavu
//
//  Created by Richard Gieske on 3/28/13.
//
//

#import <Foundation/Foundation.h>
#import <ExternalAccessory/ExternalAccessory.h>
#import "DTDevices.h"

@class AppDelegate;
@class SharedMethodsAndInfo;
@class ExternalDeviceHub;
//@class PrintHandler;

@interface ExternalDeviceDelegate : NSObject <NSStreamDelegate, EAAccessoryDelegate> {
    
    AppDelegate *appDelegate;
    AppDelegate *smai;
    ExternalDeviceHub *edHub;
    
    DTDevices *dtDevice;
    
    EASession *session;
    
    BOOL cardReadFailed;
    
    BOOL testStringWritten;
    int testStringWriteAttempts;
    BOOL switcher;
    
    int printMode;
    //PrintHandler *printJobSender;
    int printBytesToWrite;
    int printBytesWritten;
    NSMutableArray *interceptPrinterSettings;
}

@property (nonatomic, retain) NSString *deviceName;
@property (nonatomic, retain) NSString *protocolString;
@property (nonatomic, retain) EAAccessory *accessory;

@property (readwrite, assign) BOOL hasCardReader;
@property (readwrite, assign) BOOL hasChipAndPIN;
@property (readwrite, assign) BOOL hasScanner;
@property (readwrite, assign) BOOL hasPrinter;
@property (readwrite, assign) BOOL connected;

@property (nonatomic, retain)NSString *track1String;
@property (nonatomic, retain)NSString *track2String;
@property (nonatomic, retain)NSString *track3String;
@property (nonatomic, retain)NSString *cardSwipeString;
@property (nonatomic, retain)NSString *cardSwipeDataString;

@property (nonatomic, retain) NSString *printString;

- (id)init:(NSString *)reader protocol:(NSString *)protocol accessory:(EAAccessory *)accessory_;

- (void)dcLog:(NSString *)message;
- (void)startListening;
- (void)stopListening;
- (void)startSession;
- (void)openStreams;
- (void)closeStreams;

// BlueBamboo

- (void)startBlueBamboo;
- (void)startBlueBambooExec;
- (void)writeToBlueBamboo;
- (void)checkBlueBambooWrite;
- (void)BBprintText:(NSString *)prntStrng sender:(id)sender;

// TSP650II

- (void)writeToStarTSP650II;

// Linea Pro

- (void)connectLineaPro;
- (void)disconnectLineaPro;
- (void)connectionState:(int)state;
- (void)batteryDidChange;
- (void)magneticCardData:(NSString *)track1 track2:(NSString *)track2 track3:(NSString *)track3;
- (void)scanDown;
- (void)scanUp;
- (void)barcodeData:(NSString *)barcode type:(int)type;

//

@end
