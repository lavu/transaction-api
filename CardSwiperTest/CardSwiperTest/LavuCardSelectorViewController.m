//
//  LavuCardSelectorViewController.m
//  CardSwiperTest
//
//  Created by Theodore Schnepper on 10/18/13.
//  Copyright (c) 2013 Lavu, Inc. All rights reserved.
//

#import "LavuCardSelectorViewController.h"

@interface LavuCardSelectorViewController ()

@end

@implementation LavuCardSelectorViewController

#pragma mark - Standard View Controller Implementations
- (id) initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder: aDecoder];
    if (self) {
        // Custom initialization
        [self deviceListView];
        
        int numClasses = objc_getClassList(NULL, 0);
        Class *classes = NULL;
        classes = malloc(sizeof(Class) * numClasses);
        numClasses = objc_getClassList( classes,  numClasses );
        swipperList = [[NSMutableArray alloc] init];
        
        for( NSInteger i = 0; i < numClasses; i++ ){
            Class baseClass = classes[i];
            if( baseClass == [LavuNullSwiper class]){
                continue;
            }
            
            Class superClass = baseClass;
            
            do
            {
                superClass = class_getSuperclass(superClass);
            } while(superClass && superClass != [LavuNullSwiper class]);
            
            if (superClass == nil)
            {
                continue;
            }
            
            if( [baseClass deviceManufacturer] && [baseClass deviceName])
            {
                //NSMutableDictionary* device1 = [[[NSMutableDictionary alloc] init] autorelease];
                //[device1 setObject: @"" forKey:@"imagename"];
                //[device1 setObject: baseClass forKey:@"class"];
                [swipperList addObject: baseClass];
            }
        }
        
        free( classes );
        /*{
            //LavuMagTekIDynamoSwipper
            NSMutableDictionary* device1 = [[[NSMutableDictionary alloc] init] autorelease];
            [device1 setObject: @"magtek_idynamo.jpg" forKey:@"imagename"];
            [device1 setObject: [LavuMagTekIDynamoSwipper class] forKey:@"class"];
            [swipperList addObject: [device1 copy]];
        }
        {
            NSMutableDictionary* device1 = [[[NSMutableDictionary alloc] init] autorelease];
            [device1 setObject: @"magtek_udynamo.jpg" forKey:@"imagename"];
            [device1 setObject: [LavuMagTekUDynamoSwipper class] forKey:@"class"];
            [swipperList addObject: [device1 copy]];
        }
        
        {
            NSMutableDictionary* device1 = [[[NSMutableDictionary alloc] init] autorelease];
            [device1 setObject: @"unimag_221x220.png" forKey:@"imagename"];
            [device1 setObject: [LavuIDTechUnimag class] forKey:@"class"];
            [swipperList addObject: [device1 copy]];
        }
        
        {
            NSMutableDictionary* device1 = [[[NSMutableDictionary alloc] init] autorelease];
            [device1 setObject: @"idtech_imag_pro.png" forKey:@"imagename"];
            [device1 setObject: [LavuIDTechIMagProSwipper class] forKey:@"class"];
            [swipperList addObject: [device1 copy]];
        }
        
        {
            NSMutableDictionary* device1 = [[[NSMutableDictionary alloc] init] autorelease];
            [device1 setObject: @"blue_bamboo_p25i.png" forKey:@"imagename"];
            [device1 setObject: [LavuBlueBamboop25iSwipper class] forKey:@"class"];
            [swipperList addObject: [device1 copy]];
        }
        
        {
            NSMutableDictionary* device1 = [[[NSMutableDictionary alloc] init] autorelease];
            [device1 setObject: @"" forKey:@"imagename"];
            [device1 setObject: [LavuRaspberryPiSwipper class] forKey:@"class"];
            [swipperList addObject: [device1 copy]];
        }
        
        {
            NSMutableDictionary* device1 = [[[NSMutableDictionary alloc] init] autorelease];
            [device1 setObject: @"infinite_peripherals_infinea_tab.png" forKey:@"imagename"];
            [device1 setObject: [LavuInfinitePeripheralsInfineaTab2LSwipper class] forKey:@"class"];
            [swipperList addObject: [device1 copy]];
        }
        
        {
            NSMutableDictionary* device1 = [[[NSMutableDictionary alloc] init] autorelease];
            [device1 setObject: @"infinite_peripherals_infinea_tab.png" forKey:@"imagename"];
            [device1 setObject: [LavuInfinitePeripheralsInfineaTab4LSwipper class] forKey:@"class"];
            [swipperList addObject: [device1 copy]];
        }*/
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [_deviceListView release];
    [swipperList release];
    [super dealloc];
}
- (void)viewDidUnload {
    [self setDeviceListView:nil];
    [super viewDidUnload];
}

#pragma mark - UITableViewDelegate Selectors
#pragma mark Managing Selections
- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    return indexPath;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if( [indexPath length] > 1 ){
        int index = [indexPath indexAtPosition: 1];
        Class device = [swipperList objectAtIndex: index];
        NSObject<LavuSwiperProtocol>* swipper = [[device alloc] init];
        [[swipper class] getSwipperType];
        [appDelegate setSwipper: swipper];
        [swipper setDelegate: appDelegate];
        [swipper release];
    }
}

//- (NSIndexPath *)tableView:(UITableView *)tableView willDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
//    return indexPath;
//}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    if( appDelegate && [appDelegate swipper] ){
        [appDelegate setSwipper: NULL];
    }
}
/*
#pragma mark Configuring Rows for the Table View
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 1.0f;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 1.0f;
}

- (NSInteger)tableView:(UITableView *)tableView indentationLevelForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 0;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"Here");
    
}

#pragma mark Managing Accessory Views
- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"Here");
}



#pragma mark Modifying the Header and Footer of Sections
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return NULL;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return NULL;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 1.0f;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForHeaderInSection:(NSInteger)section {
    return 1.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 1.0f;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForFooterInSection:(NSInteger)section {
    return 1.0f;
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section {
    NSLog(@"Here");
}

- (void)tableView:(UITableView *)tableView willDisplayFooterView:(UIView *)view forSection:(NSInteger)section {
    NSLog(@"Here");
}

#pragma mark Editing Table Rows
- (void)tableView:(UITableView *)tableView willBeginEditingRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"Here");
}

- (void)tableView:(UITableView *)tableView didEndEditingRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"Here");
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewCellEditingStyleNone;
}

- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath {
        return @"";
}

- (BOOL)tableView:(UITableView *)tableView shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath {
    return FALSE;
}

#pragma mark Reording Table Rows
- (NSIndexPath *)tableView:(UITableView *)tableView targetIndexPathForMoveFromRowAtIndexPath:(NSIndexPath *)sourceIndexPath toProposedIndexPath:(NSIndexPath *)proposedDestinationIndexPath {
    return proposedDestinationIndexPath;
}

#pragma mark Tracking the Removal of Views
- (void)tableView:(UITableView *)tableView didEndDisplayingCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"Here");
}

- (void)tableView:(UITableView *)tableView didEndDisplayingHeaderView:(UIView *)view forSection:(NSInteger)section {
    NSLog(@"Here");
}

- (void)tableView:(UITableView *)tableView didEndDisplayingFooterView:(UIView *)view forSection:(NSInteger)section {
    NSLog(@"Here");
}
#pragma Copying and Pasting Row Content
- (BOOL)tableView:(UITableView *)tableView shouldShowMenuForRowAtIndexPath:(NSIndexPath *)indexPath {
    return FALSE;
}

- (BOOL)tableView:(UITableView *)tableView canPerformAction:(SEL)action forRowAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
    return FALSE;
}

- (void)tableView:(UITableView *)tableView performAction:(SEL)action forRowAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
    NSLog(@"Here");
}
#pragma Manage Table View Highlighting
- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath {
    return FALSE;
}

- (void)tableView:(UITableView *)tableView didHighlightRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"Here");
}

- (void)tableView:(UITableView *)tableView didUnhighlightRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"Here");
}
*/
#pragma mark - UITableViewDataSource
#pragma mark Configuring a Table View
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier: @"DeviceCell"];
    if( [indexPath length] > 1 ){
        int index = [indexPath indexAtPosition: 1];
        
        Class device = [swipperList objectAtIndex: index];
        UIImageView* imageView = (UIImageView*)[[cell contentView] viewWithTag: 1];
        UILabel* titleLabel = (UILabel*)[[cell contentView] viewWithTag: 2];
        UILabel* manufacturerLabel = (UILabel*)[[cell contentView] viewWithTag: 3];
        //UILabel* descriptionLabel = (UILabel*)[[cell contentView] viewWithTag: 4];

        //[imageView setBackgroundColor: [UIColor colorWithPatternImage: [UIImage imageWithContentsOfFile: [device imageName]]]];
        //[imageView setNeedsDisplay];
        UIImage* image = [UIImage imageNamed: [device imageName]];
        [imageView setImage: image];
        
        [titleLabel setText: [device deviceName]];
        [manufacturerLabel setText: [device deviceManufacturer]];
        //[descriptionLabel setText: [deviceDetails objectForKey: @"description"]];
    }
    
    return cell;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [swipperList count];
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    return NULL;
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index {
    return 0;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return @"Devices";
}
- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section {
    return NULL;
}

#pragma mark Inserting or Deleting Table Rows
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return FALSE;
}
#pragma mark Reordering Table Rows
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    return TRUE;
}

- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
    
}
@end
