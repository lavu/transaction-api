//
//  ViewController.m
//  CardSwiperTest
//
//  Created by Richard Gieske on 10/4/13.
//  Copyright (c) 2013 Lavu, Inc. All rights reserved.
//

#import "ViewController.h"
#import "AppDelegate.h"

@implementation ViewController

@synthesize connectedDevices;
@synthesize lastSwipeReader;
@synthesize lastSwipeString;
@synthesize lastSwipeHex;
@synthesize readerStatusIcon;
@synthesize readerActivityIndicator;
@synthesize startStopButton;
@synthesize isListening;
@synthesize consoleScrollview;

- (void)viewDidLoad {
    
    [super viewDidLoad];
	appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [[UIApplication sharedApplication] setStatusBarHidden: TRUE];
}

- (IBAction)reinitialize {
    [appDelegate rLog:@"reinitialize"];
    [self performSegueWithIdentifier: @"ChooseCardType" sender: self ];
    //if (isListening) [self startStopListening];
    
    //[appDelegate.externalDeviceHub deinitializeMagTek:@"idynamo"];
    //[appDelegate.externalDeviceHub deinitializeMagTek:@"udynamo"];
    //[appDelegate.externalDeviceHub deinitializeUniMag];
    //[appDelegate.externalDeviceHub deinitializeAll];
    
    //[appDelegate.externalDeviceHub performSelector:@selector(initializeUniMag) withObject:nil afterDelay:1];
}

- (IBAction)startStopListening {
    if(![[appDelegate swipper] isConnected])
        [[appDelegate swipper] connect];
    else
        [[appDelegate swipper] disconnect];
    /*if (isListening) {
        [appDelegate rLog:@"stop listening"];
        isListening = NO;
        [appDelegate.externalDeviceHub stopListening];
        [self.startStopButton setTitle:@"Start Listening" forState:UIControlStateNormal];
    } else {
        [appDelegate rLog:@"start listening"];
        isListening = YES;
        [appDelegate.externalDeviceHub startListening];
        [self.startStopButton setTitle:@"Stop Listening" forState:UIControlStateNormal];
    }*/
}

- (NSString *)getCurrentDateTime:(NSString *)dateTimeFormat forceUS:(BOOL)forceUS {
    
	NSDateFormatter *tf = [[NSDateFormatter alloc] init];
	[tf setDateFormat:dateTimeFormat];
	if (forceUS) {
        NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
        [tf setLocale:usLocale];
        [usLocale release];
    }
    NSString *dateString = [tf stringFromDate:[NSDate date]];
	[tf release];
	return dateString;
}

- (void)onScreenConsole:(NSString *)logString {
    
    UILabel *dateTime = [[UILabel alloc] initWithFrame:CGRectMake(0, consoleOutputHeight, 105, 14)];
    dateTime.textColor = [UIColor darkGrayColor];
    dateTime.backgroundColor = [UIColor clearColor];
    dateTime.textAlignment = UITextAlignmentLeft;
    dateTime.lineBreakMode = UILineBreakModeWordWrap;
    dateTime.numberOfLines = 1;
    [dateTime setFont:[UIFont fontWithName:@"Helvetica" size:9]];
    dateTime.text = [self getCurrentDateTime:@"yyyy-MM-dd HH:mm:ss.SSS - " forceUS:YES];
    [self.consoleScrollview addSubview:dateTime];
    [dateTime release];
    
    NSString *logLine = [NSString stringWithFormat:@"                                       %@", logString];
    
    CGSize labelSize = [logLine sizeWithFont:[UIFont fontWithName:@"Helvetica" size:10] constrainedToSize:CGSizeMake(484, 4096) lineBreakMode:NSLineBreakByWordWrapping];
    
    UILabel *newLine = [[UILabel alloc] initWithFrame:CGRectMake(0, consoleOutputHeight, 484, labelSize.height)];
    newLine.textColor = [UIColor blackColor];
    newLine.backgroundColor = [UIColor clearColor];
    newLine.textAlignment = UITextAlignmentLeft;
    newLine.lineBreakMode = UILineBreakModeWordWrap;
    newLine.numberOfLines = 0;
    [newLine setFont:[UIFont fontWithName:@"Helvetica" size:10]];
    newLine.text = logLine;
    [self.consoleScrollview addSubview:newLine];
    [newLine release];
    
    consoleOutputHeight += (labelSize.height + 5);
    
    self.consoleScrollview.contentSize = CGSizeMake(484, consoleOutputHeight);
    if (consoleOutputHeight > 688) [self.consoleScrollview scrollRectToVisible:CGRectMake(0, (consoleOutputHeight - 688), 484, 688) animated:YES];
}

- (IBAction)clearConsole {
    
    for (UILabel *label in [self.consoleScrollview subviews]) {
		[label removeFromSuperview];
	}
	self.consoleScrollview.contentSize = CGSizeMake(484, 0);
	[self.consoleScrollview scrollRectToVisible:CGRectMake(0, 0, 484, 688) animated:YES];
    consoleOutputHeight = 0;
}

- (void)appWillResignActive {
    
    if (isListening) [self startStopListening];
}

- (void)didReceiveMemoryWarning {

    [super didReceiveMemoryWarning];
}

- (void)dealloc {
    
    [connectedDevices release];
    [lastSwipeReader release];
    [lastSwipeString release];
    [lastSwipeHex release];
    [readerStatusIcon release];
    [readerActivityIndicator release];
    [startStopButton release];
    [consoleScrollview release];
    
    [super dealloc];
}

- (void)cardIcon:(NSString *)state {
    
    //[smai rLog:[NSString stringWithFormat:@"cardIcon: %@", state]];
    
    //UIImage *image = [UIImage imageNamed:[NSString stringWithFormat:@"cc_%@.png", state]];
    //[smai.checkOutPanel.readerStatusIcon setImage:image];
    
    ViewController *viewController = self;
    viewController.readerStatusIcon.image = [UIImage imageNamed:[NSString stringWithFormat:@"cc_%@.png", state]];
}

@end
