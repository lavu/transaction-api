//
//  BlueBambooSipeData.m
//  CardSwiperTest
//
//  Created by Theodore Schnepper on 10/11/13.
//  Copyright (c) 2013 Lavu, Inc. All rights reserved.
//

#import "LavuBlueBambooSwipeData.h"

@implementation LavuBlueBambooSwipeData

- (NSString*) getTrackWithBuffer: (uint8_t*) buffer startingWithChar: (uint8_t) valid withMaxLength: (unsigned int) maxLength{
    int i = 0;
    int trackLength = 0;
    if( buffer[i++] == valid ){
        for( int j = 0; j < 4; j++ ){
            trackLength *= 10;
            trackLength += buffer[i++] - 48;
        }
        if( trackLength >= 0 && trackLength <= 9999 && ([[self track1String] length] + [[self track2String] length] + [[self track3String] length] + trackLength < maxLength )){
            //Assume it's valid
            NSData* bufferData = [NSData dataWithBytes: &buffer[i] length:trackLength];
            NSString* result = [[[NSString alloc] initWithData: bufferData encoding: NSASCIIStringEncoding] autorelease];
            return result;
        }
    }
    return @"";
}

- (id) init {
    self = [super init];
    
    return self;
}

- (id) initWithSwipeBuffer: (uint8_t*) buffer forLength: (unsigned int) length {
    self = [self init];
        
        //BOOL operationFailed = YES;
        //BOOL cardReadGood = NO;
        
    if (length > 0) {
        
        //if (buf[4]==0x84 && buf[5]==0x04) {
        
        //operationFailed = NO;
        //}
        
        if (buffer[4]==0x84 && buffer[5]==0x00) {
            
            //operationFailed = NO;
            
            for (int i = 6; i < length; i++) {
                
                //get first track data
                /*if (buffer[i] == 0x31) {
                    
                    i++;
                    NSString *stringLength = @"";
                    int inttext = 0;
                    int track1length = 0;
                    stringLength = [NSString stringWithFormat:@"%x", buffer[i]];
                    inttext = ([stringLength intValue] - 30) * 1000;
                    track1length = track1length + inttext;
                    i++;
                    stringLength = [NSString stringWithFormat:@"%x", buffer[i]];
                    inttext=([stringLength intValue]-30) * 100;
                    track1length = track1length + inttext;
                    i++;
                    stringLength = [NSString stringWithFormat:@"%x", buffer[i]];
                    inttext = ([stringLength intValue] - 30) * 10;
                    track1length = track1length + inttext;
                    i++;
                    stringLength = [NSString stringWithFormat:@"%x", buffer[i]];
                    inttext = ([stringLength intValue] - 30) * 1;
                    track1length = track1length + inttext;
                    i++;
                    
                    if (track1length > 0) {
                        
                        cardReadGood = YES;
                        
                        unsigned char track1buffer[track1length];
                        NSMutableData *data = [[NSMutableData alloc] init];
                        
                        for (int j = 0; j < track1length; j++) {
                            track1buffer[j]=buffer[i];
                            i++;
                        }
                        [data appendBytes:track1buffer length:track1length];
                        NSString *stringtest = [[NSString alloc] initWithData:(NSData *)data encoding:NSASCIIStringEncoding];
                        
                        NSString *crr = [self track1];
                        [self setTrack1: [crr stringByAppendingString:stringtest]];
                        [data release];
                        [stringtest release];
                        
                    } else if (track1length == 0) {
                        [self setTrack1: @""];
                    }
                }
                
                //get track 2 data
                if (buffer[i] == 0x32) {
                    
                    i++;
                    NSString *stringLength = @"";
                    int inttext=0;
                    int track2length=0;
                    
                    stringLength = [NSString stringWithFormat:@"%x:",buffer[i]];
                    inttext = ([stringLength intValue] - 30) * 1000;
                    track2length = track2length + inttext;
                    i++;
                    stringLength = [NSString stringWithFormat:@"%x:",buffer[i]];
                    inttext = ([stringLength intValue] - 30) * 100;
                    track2length = track2length + inttext;
                    i++;
                    stringLength = [NSString stringWithFormat:@"%x:",buffer[i]];
                    inttext = ([stringLength intValue] - 30) * 10;
                    track2length = track2length + inttext;
                    i++;
                    stringLength = [NSString stringWithFormat:@"%x:",buffer[i]];
                    inttext = ([stringLength intValue] - 30) * 1;
                    track2length = track2length + inttext;
                    i++;
                    
                    if (track2length > 0) {
                        
                        cardReadGood = YES;
                        
                        unsigned char track2buffer[track2length];
                        NSMutableData *data = [[NSMutableData alloc] init];
                        for (int j = 0;j < track2length; j++) {
                            track2buffer[j]= buffer[i];
                            i++;
                        }
                        [data appendBytes:buffer length:track2length];
                        NSString *stringtest = [[NSString alloc] initWithData:(NSData *)data encoding:NSASCIIStringEncoding];
                        
                        NSString *crr = [self track2];
                        [self setTrack2: [crr stringByAppendingString:stringtest]];
                        [data release];
                        [stringtest release];
                        
                    } else if (track2length == 0) {
                        [self setTrack2: @""];
                    }
                }
                
                //get track 3 data
                if (buffer[i] == 0x33) {
                    
                    i++;
                    NSString *stringLength = @"";
                    int inttext = 0;
                    int track3length = 0;
                    stringLength = [NSString stringWithFormat:@"%x:",buffer[i]];
                    inttext = ([stringLength intValue] - 30) * 1000;
                    track3length = track3length + inttext;
                    i++;
                    stringLength = [NSString stringWithFormat:@"%x:",buffer[i]];
                    inttext = ([stringLength intValue] - 30) * 100;
                    track3length = track3length + inttext;
                    i++;
                    stringLength = [NSString stringWithFormat:@"%x:",buffer[i]];
                    inttext = ([stringLength intValue] - 30) * 10;
                    track3length = track3length + inttext;
                    i++;
                    stringLength = [NSString stringWithFormat:@"%x:",buffer[i]];
                    inttext = ([stringLength intValue] - 30) * 1;
                    track3length = track3length + inttext;
                    i++;
                    
                    if (track3length > 0) {
                        
                        cardReadGood = YES;
                        
                        unsigned char track3buffer[track3length];
                        NSMutableData *data = [[NSMutableData alloc] init];
                        for (int j = 0; j < track3length; j++) {
                            track3buffer[j]= buffer[i];
                            i++;
                        }
                        [data appendBytes:track3buffer length:track3length];
                        NSString *stringtest = [[NSString alloc] initWithData:(NSData *)data encoding:NSASCIIStringEncoding];
                        
                        NSString *crr = [self track3];
                        [self setTrack3: [crr stringByAppendingString:stringtest]];
                        [data release];
                        [stringtest release];
                        
                    } else if (track3length == 0) {
                        [self setTrack3: @""];
                    }
                    
                    break;
                }*/
                
                NSString* track1Data = [self getTrackWithBuffer: &buffer[i] startingWithChar: '1' withMaxLength: length];
                [self setTrack1String: [[self track1String] stringByAppendingString: track1Data]];
                i += [track1Data length];
                NSString* track2Data = [self getTrackWithBuffer: &buffer[i] startingWithChar: '2' withMaxLength: length];
                [self setTrack2String: [[self track2String] stringByAppendingString: track2Data]];
                i += [track2Data length];
                NSString* track3Data = [self getTrackWithBuffer: &buffer[i] startingWithChar: '3' withMaxLength: length];
                [self setTrack3String: [[self track3String] stringByAppendingString: track3Data]];
                i += [track3Data length];
            }
        }
    }
    [self setTotalTrack: [NSString stringWithFormat: @"%@%@%@", [self track1String], [self track2String], [self track3String]]];
    [self parseTracks: [self totalTrack]];
    return self;
}

- (NSString*) toLavuGatewayString {
    return [self totalTrack];
}
@end
