//
//  LavuIMagProSwipper.h
//  CardSwiperTest
//
//  Created by Theodore Schnepper on 10/21/13.
//  Copyright (c) 2013 Lavu, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LavuEAAccessorySwiper.h"
#import "IDTechCardSwipe.h"

@interface LavuIDTechIMagProSwiper : LavuEAAccessorySwiper

@end
