//
//  LavuIMagProSwipper.m
//  CardSwiperTest
//
//  Created by Theodore Schnepper on 10/21/13.
//  Copyright (c) 2013 Lavu, Inc. All rights reserved.
//

#import "LavuIDTechIMagProSwiper.h"

@implementation LavuIDTechIMagProSwiper

- (id) init {
    self = [super init];
    return self;
}

+ (NSString*) getName {
    return @"ID TECH iMag";
}

+ (NSString*) getProtocol {
    return @"com.idtechproducts.reader";
}
+ (NSString*) getManufacturer {
    return @"ID TECH";
}

- (void) handleReadStream: (NSInputStream*) inputStream {
    uint8_t buffer[1024];
    for( int i = 0; i < 1024; i++ ){
        buffer[i] = 0;
    }
    unsigned int len = [inputStream read: buffer maxLength: 1024];
    NSData* data = [NSData dataWithBytes: buffer length: len];
    [[NSNotificationCenter defaultCenter] postNotificationName: LavuCardSwipeDataNotification object: [[[IDTechCardSwipe alloc] initWithData: data] autorelease] userInfo: NULL];
}

+ (NSString*) deviceManufacturer {
    return @"ID Tech";
}

+ (NSString*) deviceName {
    return @"iMag Pro";
}

+ (NSString*) imageName {
    return @"idtech_imag_pro.png";
}

@end
