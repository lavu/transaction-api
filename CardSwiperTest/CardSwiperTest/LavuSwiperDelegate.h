//
//  LavuSwipperDelegateProtocol.h
//  CardSwiperTest
//
//  Created by Theodore Schnepper on 10/15/13.
//  Copyright (c) 2013 Lavu, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol LavuSwiperDelegate <NSObject>

/** the didConnect method notifies the delegate the status of calling the connect method on the LavuSwiperProtocol object.
 */
- (void) didConnect: (BOOL) connectStatus forSwipper: (NSObject*) swipper;

/** the didDisconnect method notifies the delegate the status of calling the connect method on the LavuSwiperProtocol object.
 */
- (void) didDisconnect: (BOOL) connectStatus forSwipper: (NSObject*) swipper;

/** the didStartListening method notifies the delegate the status of calling the connect method on the LavuSwiperProtocol object.
 */
- (void) didStartListening: (BOOL) listenStatus forSwipper: (NSObject*) swipper;

/** the didStopListening method notifies the delegate the status of calling the connect method on the LavuSwiperProtocol object.
 */
- (void) didStopListening: (BOOL) listenStatus forSwipper: (NSObject*) swipper;

/** the didPrepareForSwipe method notifies the delegate the status of calling the connect method on the LavuSwiperProtocol object.
 */
- (void) didPrepareForSwipe: (BOOL) prepareStatus forSwipper: (NSObject*) swipper;
@end
