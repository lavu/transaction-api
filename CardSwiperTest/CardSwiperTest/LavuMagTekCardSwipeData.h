//
//  LavuCardSwipeData.h
//  CardSwiperTest
//
//  Created by Theodore Schnepper on 10/8/13.
//  Copyright (c) 2013 Lavu, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LavuNullSwipeData.h"

@interface LavuMagTekCardSwipeData : LavuNullSwipeData
@property (retain, nonatomic) NSString* responseType;
@property (retain, nonatomic) NSString* trackStatus;
@property (retain, nonatomic) NSString* cardStatus;
@property (retain, nonatomic) NSString* encryptionStatus;
@property (assign, nonatomic) long batteryLevel;
@property (assign, nonatomic) int swipeCount;


@property (retain, nonatomic) NSString* magnePrintEncrypted;
@property (retain, nonatomic) NSString* magnePrintStatus;
@property (retain, nonatomic) NSString* sessionID;

@property (assign, nonatomic) int PANLength;
@property (retain, nonatomic) NSString* deviceSerialNumber;
@property (retain, nonatomic) NSString* TLVCardIIN;
@property (retain, nonatomic) NSString* MagTekSN;
@property (retain, nonatomic) NSString* firmwarePartNumber;
@property (retain, nonatomic) NSString* TLVVersion;
@property (retain, nonatomic) NSString* deviceModelName;
@property (retain, nonatomic) NSString* capabilityMSR;
@property (retain, nonatomic) NSString* capabilityTracks;
@property (retain, nonatomic) NSString* capabilityEncryption;
@end
