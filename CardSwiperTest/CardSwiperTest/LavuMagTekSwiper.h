//
//  LavuMagTekSwipper.h
//  CardSwiperTest
//
//  Created by Theodore Schnepper on 10/8/13.
//  Copyright (c) 2013 Lavu, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LavuNullSwiper.h"
#import "MTSCRA.h"
#import "LavuMagTekCardSwipeData.h"

static NSString* MagTekTrackDataReadyNotification __unused = @"trackDataReadyNotification";
static NSString* MagTekConnectionStatusChangeNotification __unused = @"devConnectionNotification";
static NSString* MagTekDeviceProtocolString __unused = @"com.magtek.idynamo";
static MTSCRA* mtSCRAlib __unused;
@interface LavuMagTekSwiper : LavuNullSwiper {
    
}

- (void) setDeviceProtocol: (NSString*) protocolString;
- (void) setDeviceType: (UInt32) deviceType;

- (void) trackDataReady: (NSNotification*) notification;
- (void) connectionStatusChange: (NSNotification*) notification;

@end