//
//  LavuCardSelectorViewController.h
//  CardSwiperTest
//
//  Created by Theodore Schnepper on 10/18/13.
//  Copyright (c) 2013 Lavu, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "LavuMagTekIDynamoSwipper.h"
//#import "LavuMagTekUDynamoSwipper.h"
//#import "LavuBlueBamboop25iSwipper.h"
//#import "LavuIDTechUnimag.h"
//#import "LavuIDTechIMagProSwipper.h"
//#import "LavuRaspberryPiSwipper.h"
//#import "LavuInfinitePeripheralsInfineaTab2LSwipper.h"
//#import "LavuInfinitePeripheralsInfineaTab4LSwipper.h"
#import "LavuNullSwiper.h"
#import "AppDelegate.h"

#import "objc/runtime.h"

@interface LavuCardSelectorViewController : UITableViewController <UITableViewDelegate, UITableViewDataSource> {
    NSMutableArray* swipperList;
    AppDelegate* appDelegate;
}
@property (retain, nonatomic) IBOutlet UITableView *deviceListView;

@end
