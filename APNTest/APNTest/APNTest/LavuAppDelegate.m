//
//  LavuAppDelegate.m
//  APNTest
//
//  Created by Theodore Schnepper on 3/12/14.
//  Copyright (c) 2014 Theodore Schnepper. All rights reserved.
//

#import "LavuAppDelegate.h"

@implementation LavuAppDelegate

#pragma mark - Remote Notification Delegate

- (void) application:(UIApplication *) application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    //    const void *devTokenBytes = [deviceToken bytes];
    [[LavuAPNManager sharedInstance] setApnIdentifier: deviceToken];
    [[LavuAPNManager sharedInstance] setGroupIdentifier: @"group1"];
    [[LavuAPNManager sharedInstance] setDeviceIdentifier:[[NSUUID UUID] UUIDString]];
    NSString* tokenString = [[NSString alloc] initWithData:deviceToken encoding:NSUTF8StringEncoding];
    NSLog( @"Token String: %@", tokenString );
    NSLog( @"Token Bytes: %@", deviceToken );
    
    [[LavuAPNManager sharedInstance] registerForAppAPN];
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    NSLog( @"Error: %@", error.description );
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    NSLog( @"didReceiveRemoteNotification: %@", userInfo );
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult result))handler {
    NSLog( @"didReceiveRemoteNotification:fetchCompletionHandler: %@, %@", userInfo, handler );
    if( userInfo[@"aps"] ){
        NSDictionary* aps = userInfo[@"aps"];
        if( [application applicationState] == UIApplicationStateActive ){
            UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"Test Notification" message:aps[@"alert"] delegate:self cancelButtonTitle:@"ok" otherButtonTitles: nil];
            [alertView show];
        } else {
            UILocalNotification* notification = [[UILocalNotification alloc] init];
            [notification setUserInfo: userInfo];
            [notification setFireDate: [NSDate date]];
            [notification setAlertBody:aps[@"alert"]];
            [notification setAlertAction: @"Test Notification"];
            if( aps[@"badge"] ){
                [notification setApplicationIconBadgeNumber:[aps[@"badge"] integerValue]];
            }
            if( aps[@"sound"] ){
                [notification setSoundName:aps[@"sound"]];
            } else {
                [notification setSoundName: UILocalNotificationDefaultSoundName];
            }
            [application presentLocalNotificationNow: notification];
        }
    }
}

#pragma mark - Application Delegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
//    [[self locationManager] startRangingBeaconsInRegion:<#(CLBeaconRegion *)#>];
    [[UIApplication sharedApplication] registerForRemoteNotificationTypes: UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound ];
    
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    application.applicationIconBadgeNumber = 0;
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
