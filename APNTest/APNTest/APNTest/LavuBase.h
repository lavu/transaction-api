//
//  LavuBase.h
//  APNTest
//
//  Created by Theodore Schnepper on 3/21/14.
//  Copyright (c) 2014 Theodore Schnepper. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LavuBase : NSObject
- (BOOL) setValueUsing: (SEL) selector ifKeyExists:(NSString*) key usingDetails: (NSDictionary*) dict;
- (NSNumber*) ID;
@end
