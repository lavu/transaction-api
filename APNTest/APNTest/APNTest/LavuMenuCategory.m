//
//  LavuMenuCategory.m
//  APNTest
//
//  Created by Theodore Schnepper on 3/21/14.
//  Copyright (c) 2014 Theodore Schnepper. All rights reserved.
//

#import "LavuMenuCategory.h"

@implementation LavuMenuCategory
@synthesize ID                      = _ID;
@synthesize menuGroupID             = _menuGroupID;
@synthesize name                    = _name;
@synthesize imagePath               = _imagePath;
@synthesize description             = _description;
@synthesize lastModified            = _lastModified;
@synthesize printerIdentifier       = _printerIdentifier;
@synthesize modifierListID          = _modifierListID;
@synthesize forcedModifierGroupID   = _forcedModifierGroupID;
@synthesize forcedModifierListID    = _forcedModifierListID;
@synthesize printOrder              = _printOrder;
@synthesize superGroupID            = _superGroupID;
@synthesize taxInclusion            = _taxInclusion;
@synthesize lavutogoDisplay         = _lavutogoDisplay;
@synthesize taxProfileID            = _taxProfileID;
@synthesize chainReportingGroup     = _chainReportingGroup;
@synthesize priceTierProfileID      = _priceTierProfileID;

- (void) setForcedModifierGroupOrListID: (NSString*) idDetails {
    if( [idDetails characterAtIndex: 0] == 'c' || [idDetails characterAtIndex: 0] == 'C' || [idDetails isEqualToString: @""] ){
        [self setForcedModifierListID: [NSNumber numberWithInteger: 0]];
        [self setForcedModifierGroupID: [NSNumber numberWithInteger: 0]];
    } else if( [idDetails characterAtIndex: 0] == 'l' ){
        //list
        [self setForcedModifierListID: [NSNumber numberWithInteger: [[idDetails substringFromIndex: 1] integerValue]]];
        [self setForcedModifierGroupID: [NSNumber numberWithInteger: 0]];
    } else {
        //group
        if( [idDetails characterAtIndex: 0] >= '0' && [idDetails characterAtIndex: 0] <= '9' ) {
            //Just a Number
            [self setForcedModifierGroupID: [NSNumber numberWithInteger: [idDetails integerValue]]];
        } else {
            [self setForcedModifierGroupID: [NSNumber numberWithInteger: [[idDetails substringFromIndex: 1] integerValue]]];
        }
        [self setForcedModifierListID: [NSNumber numberWithInteger: 0]];
    }
}

+ (id) lavuMenuCategory: (NSDictionary*) details {
    LavuMenuCategory* result = [[LavuMenuCategory alloc] init];
    
    if(! [result setValueUsing: @selector(setID:) ifKeyExists:@"id" usingDetails: details]){
        return nil;
    }
    [result setValueUsing: @selector(setMenuGroupID:) ifKeyExists:@"group_id" usingDetails: details];
    [result setValueUsing: @selector(setName:) ifKeyExists:@"name" usingDetails: details];
    [result setValueUsing: @selector(setImagePath:) ifKeyExists:@"image" usingDetails: details];
    [result setValueUsing: @selector(setDescription:) ifKeyExists:@"description" usingDetails: details];
//    [result setValueUsing: @selector(setActive:) ifKeyExists:@"active" usingDetails: details];
//    [result setValueUsing: @selector(setOrder:) ifKeyExists:@"_order" usingDetails: details];
//    [result setValueUsing: @selector(setPrint:) ifKeyExists:@"print" usingDetails: details];
    if( ![[NSNull null] isEqual: details[@"last_modified_date"]] ){
        NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"%Y-%m-%d %H:%M:%S"];
        [result setLastModified: [formatter dateFromString: details[@"last_modified_date"]]];
    }
    [result setValueUsing: @selector(setPrinterIdentifier:) ifKeyExists:@"printer" usingDetails: details];
    [result setValueUsing: @selector(setModifierListID:) ifKeyExists:@"modifier_list_id" usingDetails: details];
//    [result setValueUsing: @selector(setDeleted:) ifKeyExists:@"_deleted" usingDetails: details];
//    [result setValueUsing: @selector(setApplyTaxRate:) ifKeyExists:@"apply_taxrate" usingDetails: details];
//    [result setValueUsing: @selector(setCustomTaxRate:) ifKeyExists:@"custom_taxrate" usingDetails: details];
    [result setValueUsing: @selector(setForcedModifierGroupOrListID:) ifKeyExists:@"forced_modifier_group_id" usingDetails: details];
    [result setValueUsing: @selector(setPrintOrder:) ifKeyExists:@"print_order" usingDetails: details];
    [result setValueUsing: @selector(setSuperGroupID:) ifKeyExists:@"super_group_id" usingDetails: details];
    [result setValueUsing: @selector(setTaxInclusion:) ifKeyExists:@"tax_inclusion" usingDetails: details];
    [result setValueUsing: @selector(setLavutogoDisplay:) ifKeyExists:@"ltd_display" usingDetails: details];
//    [result setValueUsing: @selector(setHappyHour:) ifKeyExists:@"happyhour" usingDetails: details];
    [result setValueUsing: @selector(setTaxProfileID:) ifKeyExists:@"tax_profile_id" usingDetails: details];
    [result setValueUsing: @selector(setChainReportingGroup:) ifKeyExists:@"chain_reporting_group" usingDetails: details];
    [result setValueUsing: @selector(setPriceTierProfileID:) ifKeyExists:@"price_tier_profile_id" usingDetails: details];
    return result;
}


@end
