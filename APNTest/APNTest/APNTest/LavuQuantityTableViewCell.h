//
//  LavuQuantityTableViewCell.h
//  APNTest
//
//  Created by Theodore Schnepper on 3/31/14.
//  Copyright (c) 2014 Theodore Schnepper. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LavuQuantityChangedProtocol.h"

@interface LavuQuantityTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *amountLabel;
@property (weak, nonatomic) IBOutlet UIStepper *stepper;
@property (weak, nonatomic) id<LavuQuantityChangedProtocol> proto;

- (IBAction)amountChange:(id)sender forEvent:(UIEvent *)event;
@end
