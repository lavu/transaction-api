//
//  LavuMenu.m
//  APNTest
//
//  Created by Theodore Schnepper on 3/21/14.
//  Copyright (c) 2014 Theodore Schnepper. All rights reserved.
//

#import "LavuMenu.h"

@implementation LavuMenu {
    NSArray* _items;
    NSArray* _categories;
    NSArray* _lavuMenuGroups;
    NSArray* _lavuForcedModifiers;
    NSArray* _lavuForcedModifierLists;
    NSArray* _lavuForcedModifierGroups;
    NSArray* _lavuModifiers;
    NSArray* _lavuModifierCategories;
    NSArray* _lavuIngredients;
}

@synthesize ID = _ID;

- (id) init {
    self = [super init];
    if( self ){
        _items = [[NSArray alloc] init];
        _categories = [[NSArray alloc] init];
        _lavuMenuGroups = [[NSArray alloc] init];
        _lavuForcedModifiers = [[NSArray alloc] init];
        _lavuForcedModifierLists = [[NSArray alloc] init];
        _lavuForcedModifierGroups = [[NSArray alloc] init];
        _lavuModifiers = [[NSArray alloc] init];
        _lavuModifierCategories = [[NSArray alloc] init];
        _lavuIngredients = [[NSArray alloc] init];
    }
    return self;
}


+ (id) lavuMenu: (NSDictionary*) details {
    LavuMenu* result = [[LavuMenu alloc] init];
    
    if(! [result setValueUsing: @selector(setID:) ifKeyExists:@"id" usingDetails: details]){
        return nil;
    }
    return result;
}

- (NSArray*) items {
    return _items;
}

- (NSArray*) categories {
    return _categories;
}
- (NSArray*) lavuMenuGroups {
    return _lavuMenuGroups;
}
- (NSArray*) lavuForcedModifiers{
    return _lavuForcedModifiers;
}
- (NSArray*) lavuForcedModifierLists{
    return _lavuForcedModifierLists;
}
- (NSArray*) lavuForcedModifierGroups{
    return _lavuForcedModifierGroups;
}
- (NSArray*) lavuModifiers {
    return _lavuModifiers;
}
- (NSArray*) lavuModifierCategories {
    return _lavuModifierCategories;
}

- (NSArray*) lavuIngredients {
    return _lavuIngredients;
}

- (id) checkArray: (NSArray*) array forID: (NSNumber*) numID {
    for( int i = 0; i < array.count; i++ ){
        if( [[array[i] ID] isEqual: numID] ){
            return array[i];
        }
    }
    return nil;
}

- (id) lavuMenuItemFromID: (NSNumber*) menuItemID {
    return [self checkArray: _items forID: menuItemID];
}

- (id) lavuMenuCategoryFromID: (NSNumber*) menuCategoryID {
    return [self checkArray: _categories forID: menuCategoryID];
}
- (id) lavuMenuGroupFromID: (NSNumber*) menuGroupID {
    return [self checkArray: _lavuMenuGroups forID: menuGroupID];
}
- (id) lavuForcedModifierFromID: (NSNumber*) lavuForcedModifierID {
    return [self checkArray: _lavuForcedModifiers forID: lavuForcedModifierID];
}
- (id) lavuForcedModifierListFromID: (NSNumber*) lavuForcedModifierListID {
    return [self checkArray: _lavuForcedModifierLists forID: lavuForcedModifierListID];
}
- (id) lavuForcedModifierGroupFromID: (NSNumber*) lavuForcedModifierGroupID {
    return [self checkArray: _lavuForcedModifierGroups forID: lavuForcedModifierGroupID];
}
- (id) lavuModifierFromID: (NSNumber*) lavuModifierID {
    return [self checkArray: _lavuModifiers forID: lavuModifierID];
}
- (id) lavuModifierCategoryFromID: (NSNumber*) lavuModifierCategoryID {
    return [self checkArray: _lavuModifierCategories forID: lavuModifierCategoryID];
}

- (id) lavuIngredientsFromID:(NSNumber *)lavuIngredientID {
    return [self checkArray: _lavuIngredients forID: lavuIngredientID];
}

- (void) loadAllDetails: (NSDictionary*) details {
    {
        NSArray* menu_groups_array = details[@"groups"];
        NSMutableArray* menu_groups = [_lavuMenuGroups mutableCopy];
        for( int i = 0; i < menu_groups_array.count; i++ ){
            LavuMenuGroup* lavu_menu_group = [LavuMenuGroup lavuMenuGroup: menu_groups_array[i]];
            if( [menu_groups containsObject: lavu_menu_group] ){
                [menu_groups removeObject: lavu_menu_group];
            }
            
            [menu_groups addObject: lavu_menu_group];
        }
        _lavuMenuGroups = [menu_groups copy];
    }
    
    {
        NSArray* menu_categories_array = details[@"categories"];
        NSMutableArray* menu_categories = [_categories mutableCopy];
        for( int i = 0; i < menu_categories_array.count; i++ ){
            LavuMenuCategory* lavu_menu_category = [LavuMenuCategory lavuMenuCategory: menu_categories_array[i]];
            if( [menu_categories containsObject: lavu_menu_category] ){
                [menu_categories removeObject: lavu_menu_category];
            }
            
            [menu_categories addObject: lavu_menu_category];
        }
        _categories = [menu_categories copy];
    }
    
    {
        NSArray* menu_items_array = details[@"items"];
        NSMutableArray* menu_items = [_items mutableCopy];
        for( int i = 0; i < menu_items_array.count; i++ ){
            LavuMenuItem* lavu_menu_item = [LavuMenuItem lavuMenuItem: menu_items_array[i]];
            if( [menu_items containsObject: lavu_menu_item] ){
                [menu_items removeObject: lavu_menu_item];
            }
            
            [menu_items addObject: lavu_menu_item];
        }
        _items = [menu_items copy];
    }
    
    {
        NSArray* menu_ingredients_array = details[@"ingredients"];
        NSMutableArray* menu_ingredients = [_lavuIngredients mutableCopy];
        for( int i = 0; i < menu_ingredients_array.count; i++ ){
            LavuIngredient* lavu_menu_category = [LavuIngredient lavuIngredients: menu_ingredients_array[i]];
            if( [menu_ingredients containsObject: lavu_menu_category] ){
                [menu_ingredients removeObject: lavu_menu_category];
            }
            
            [menu_ingredients addObject: lavu_menu_category];
        }
        _lavuIngredients = [menu_ingredients copy];
    }
    
    {
        NSArray* menu_modifier_categories_array = details[@"modifier_categories"];
        NSMutableArray* menu_modifier_categories = [_lavuModifierCategories mutableCopy];
        for( int i = 0; i < menu_modifier_categories_array.count; i++ ){
            LavuModifierCategory* lavu_menu_category = [LavuModifierCategory lavuModifierCategory: menu_modifier_categories_array[i]];
            if( [menu_modifier_categories containsObject: lavu_menu_category] ){
                [menu_modifier_categories removeObject: lavu_menu_category];
            }
            
            [menu_modifier_categories addObject: menu_modifier_categories];
        }
        _lavuModifierCategories = [menu_modifier_categories copy];
    }
    
    {
        NSArray* menu_modifiers_array = details[@"modifiers"];
        NSMutableArray* menu_modifiers = [_lavuModifiers mutableCopy];
        for( int i = 0; i < menu_modifiers_array.count; i++ ){
            LavuModifier* lavu_modifier = [LavuModifier lavuModifier: menu_modifiers_array[i]];
            if( [menu_modifiers containsObject: lavu_modifier] ){
                [menu_modifiers removeObject: lavu_modifier];
            }
            
            [menu_modifiers addObject: lavu_modifier];
        }
        _lavuModifiers = [menu_modifiers copy];
    }
    
    {
        NSArray* menu_forced_modifier_groups_array = details[@"forced_modifier_groups"];
        NSMutableArray* menu_forced_modifier_groups = [_lavuForcedModifierGroups mutableCopy];
        for( int i = 0; i < menu_forced_modifier_groups_array.count; i++ ){
            LavuForcedModifierGroup* lavu_forced_modifier_group = [LavuForcedModifierGroup lavuForcedModifierGroup: menu_forced_modifier_groups_array[i]];
            if( [menu_forced_modifier_groups containsObject: lavu_forced_modifier_group] ){
                [menu_forced_modifier_groups removeObject: lavu_forced_modifier_group];
            }
            
            [menu_forced_modifier_groups addObject: lavu_forced_modifier_group];
        }
        _lavuForcedModifierGroups = [menu_forced_modifier_groups copy];
    }
    
    {
        NSArray* menu_forced_modifiers_lists = details[@"forced_modifier_lists"];
        NSMutableArray* menu_forced_modifier_lists = [_lavuForcedModifierLists mutableCopy];
        for( int i = 0; i < menu_forced_modifiers_lists.count; i++ ){
            LavuForcedModifierList* lavu_forced_modifier_list = [LavuForcedModifierList lavuForcedModifierList: menu_forced_modifiers_lists[i]];
            if( [menu_forced_modifier_lists containsObject: lavu_forced_modifier_list] ){
                [menu_forced_modifier_lists removeObject: lavu_forced_modifier_list];
            }
            
            [menu_forced_modifier_lists addObject: lavu_forced_modifier_list];
        }
        _lavuForcedModifierLists = [menu_forced_modifier_lists copy];
    }
    
    {
        NSArray* menu_forced_modifiers_array = details[@"forced_modifiers"];
        NSMutableArray* menu_forced_modifiers = [_lavuForcedModifiers mutableCopy];
        for( int i = 0; i < menu_forced_modifiers_array.count; i++ ){
            LavuForcedModifier* lavu_forced_modifier = [LavuForcedModifier lavuForcedModifier: menu_forced_modifiers_array[i]];
            if( [menu_forced_modifiers containsObject: lavu_forced_modifier] ){
                [menu_forced_modifiers removeObject: lavu_forced_modifier];
            }
            
            [menu_forced_modifiers addObject: lavu_forced_modifier];
        }
        _lavuForcedModifiers = [menu_forced_modifiers copy];
    }
    
    
//    details[@"items"];
    
}

@end
