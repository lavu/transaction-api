//
//  LavuOrderScreenItemTableViewCell.m
//  APNTest
//
//  Created by Theodore Schnepper on 4/3/14.
//  Copyright (c) 2014 Theodore Schnepper. All rights reserved.
//

#import "LavuOrderScreenItemTableViewCell.h"

@implementation LavuOrderScreenItemTableViewCell
@synthesize quantityLabel = _quantityLabel;
@synthesize ItemNameLabel = _ItemNameLabel;
@synthesize itemSubtotalLabel = _itemSubtotalLabel;
@synthesize modifierDetailsLabel = _modifierDetailsLabel;
@synthesize delegate = _delegate;
@synthesize leftSwipeRecognizer = _leftSwipeRecognizer;
@synthesize rightSwipeRecognizer = _rightSwipeRecognizer;
@synthesize itemDecision = _itemDecision;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) setItemDecision: (LavuItemDecision*) decision withLocation: (LavuLocation*) location {
    if( _leftSwipeRecognizer ){
        [[self contentView] removeGestureRecognizer: _leftSwipeRecognizer];
        _leftSwipeRecognizer = nil;
    }
    _leftSwipeRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget: self action:@selector(receivedLeftSwipeGesture:)];
    [_leftSwipeRecognizer setDirection: UISwipeGestureRecognizerDirectionLeft];
    [[self contentView] addGestureRecognizer: _leftSwipeRecognizer];
    
    if( _rightSwipeRecognizer ){
        [[self contentView] removeGestureRecognizer: _rightSwipeRecognizer];
        _rightSwipeRecognizer = nil;
    }
    _rightSwipeRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget: self action:@selector(receivedRightSwipeGesture:)];
    [_rightSwipeRecognizer setDirection: UISwipeGestureRecognizerDirectionRight];
    [[self contentView] addGestureRecognizer: _rightSwipeRecognizer];
    
    _itemDecision = decision;
    [_quantityLabel setText: [[decision quantity] stringValue]];
    [_ItemNameLabel setText: [[decision item] name]];
    [_itemSubtotalLabel setText: [[location formatter] stringForObjectValue: [decision calculateSubtotal]]];
    
    [_modifierDetailsLabel setText: [LavuOrderScreenItemTableViewCell optionsStringFromDecision: decision]];
}

+ (NSString*) optionsStringFromDecision: (LavuItemDecision*) decision {
    NSString* forcedModifiersString = @"";
    {
        NSMutableArray* forcedModifierTitles = [NSMutableArray array];
        NSEnumerator* enumerator = [[decision forcedModifiersDecisions] enumerator];
        LavuForcedModifierDecision* forcedModifierDecision;
        while( forcedModifierDecision = [enumerator nextObject] ){
            [forcedModifierTitles addObject:
             [NSString stringWithFormat: @"- %@", [[forcedModifierDecision forcedModifier] title]]];
        }
        
        forcedModifiersString = [forcedModifierTitles componentsJoinedByString: @"\n"];
    }
    NSString* optionalModifiersString = @"";
    {
        NSMutableArray* optionalModifiersTitles = [NSMutableArray array];
        [[decision removedOptionalModifiers] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL* stop){
            [optionalModifiersTitles addObject: [NSString stringWithFormat:@"No %@", [(LavuModifier*)obj title]]];
        }];
        
        [[decision addedOptionalModifiers] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL* stop){
            [optionalModifiersTitles addObject: [NSString stringWithFormat:@"Plus %@", [(LavuModifier*)obj title]]];
        }];
        optionalModifiersString = [optionalModifiersTitles componentsJoinedByString: @", "];
    }
    
    NSArray* array = @[forcedModifiersString, optionalModifiersString];
    return [array componentsJoinedByString: @"\n"];
}

+ (CGRect) calculateHeightForString: (NSString*) stringToMeasure withWidth: (CGFloat) width {
    NSMutableParagraphStyle* paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.baseWritingDirection = NSWritingDirectionNatural;
    paragraphStyle.alignment = NSTextAlignmentLeft;
    //    UIFont* font = [UIFont fontWithName:@"Helvetica Neue" size:12.0];
    UIFont* font = [UIFont systemFontOfSize: 12.0];
    UIColor* color = [UIColor grayColor];
    //Helvetica Neue 12.0
    
    
    NSAttributedString* str = [[NSAttributedString alloc] initWithString: stringToMeasure attributes:@{ NSFontAttributeName: font, NSForegroundColorAttributeName: color }];
    UITextView* textView = [[UITextView alloc] init];
    [textView setAttributedText: str];
    [textView sizeToFit];
    textView.layoutManager.allowsNonContiguousLayout = false;
    
    CGSize size = [textView sizeThatFits:CGSizeMake(width, FLT_MAX)];
    
    
    return CGRectMake(0.0, 0.0, size.width, size.height);
}

+ (CGRect) calculateHeightForDecision: (LavuItemDecision*) decision withLocation: (LavuLocation*) location withWidth: (CGFloat) width {
    NSString* stringToMeasure = [LavuOrderScreenItemTableViewCell optionsStringFromDecision: decision];
    
    CGRect __block result;
    result.size.height = 0;
    result.size.width = 0;
    NSArray* stringParts = [stringToMeasure componentsSeparatedByString:@"\n"];
    [stringParts enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL* stop){
        CGRect temp = [LavuOrderScreenItemTableViewCell calculateHeightForString: obj withWidth: width];
        result.size.width = MAX( result.size.width, temp.size.width );
        result.size.height += temp.size.height;
    }];
    return result;
}

- (IBAction) receivedLeftSwipeGesture: (UIGestureRecognizer *)gestureRecognizer {
    if( _delegate && _itemDecision ){
        [_delegate removeItemDecision: _itemDecision];
    }    
}
- (IBAction) receivedRightSwipeGesture: (UIGestureRecognizer *)gestureRecognizer {
    if( _delegate && _itemDecision ){
        [_delegate editItemDecision: _itemDecision];
    }
}

@end
