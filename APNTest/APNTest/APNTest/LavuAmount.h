//
//  LavuCurrencyAmount.h
//  APNTest
//
//  Created by Theodore Schnepper on 3/28/14.
//  Copyright (c) 2014 Theodore Schnepper. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LavuAmount : NSObject
@property (readonly) int64_t baseUnit;
@property (readonly) uint decimalPlaces;

- (id) initWithString: (NSString*) numberStr;
- (id) initWithFloat: (float) number;
- (id) initWithInteger: (NSInteger) number;
- (id) initWithDouble: (double) number;

- (LavuAmount*) convertDecimalPlaces:(uint)decimalPlaces;

- (LavuAmount*) add: (LavuAmount*) amount;
- (LavuAmount*) subtract: (LavuAmount*) amount;
- (LavuAmount*) multiply: (LavuAmount*) amount;
- (LavuAmount*) divide: (LavuAmount*) amount;

- (long double) toLongDouble;
- (long int) toLongInt;
@end
