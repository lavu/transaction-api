//
//  LavuMenuTableViewController.m
//  APNTest
//
//  Created by Theodore Schnepper on 3/20/14.
//  Copyright (c) 2014 Theodore Schnepper. All rights reserved.
//

#import "LavuMenuTableViewController.h"

@interface LavuMenuTableViewController ()

@end

@implementation LavuMenuTableViewController

@synthesize majorMinor = _majorMinor;
@synthesize currentLocation = _currentLocation;
@synthesize currentMenu = _currentMenu;
@synthesize selectedItem = _selectedItem;

- (void) setMajorMinor:(NSIndexPath *)majorMinor {
    NSURL *theURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://admin.poslavu.com/beacon_service/request/MenuData/%ld/%ld.json", (long)[majorMinor section], (long)[majorMinor row]]];
//    NSURLRequestReloadIgnoringLocalAndRemoteCacheData
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:theURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:10.0f];
    [theRequest setHTTPMethod:@"GET"];
    
    NSError *theError = NULL;
    NSURLResponse *theResponse = NULL;
    NSData *theResponseData = [NSURLConnection sendSynchronousRequest:theRequest returningResponse:&theResponse error:&theError];
    if( !theResponseData ){
//        [[self navigationController] popToRootViewControllerAnimated:true];
//        [[self navigationController] popViewControllerAnimated: true];
        UILabel* label = [[UILabel alloc] init];
        [[self tableView] addSubview: label];
        return;
    }
    NSDictionary* data = [NSJSONSerialization JSONObjectWithData:theResponseData options:NSJSONReadingAllowFragments error:&theError];
//    NSLog( @"Data: %@", data );
    _majorMinor = majorMinor;
    
    NSArray* allLocations = data[@"locations"];
    NSDictionary* locationDetails = allLocations[0];
    
    LavuLocation* loc = [LavuLocation lavuLocation: locationDetails];
//    [loc setID: locationDetails[@"id"]];
    LavuMenu* menu = [[LavuMenu alloc] init];
    [menu setID: locationDetails[@"menu_id"]];
    NSArray* allPrinters = data[@"printers"];
    NSMutableArray* printers = [NSMutableArray array];
    [allPrinters enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL* stop){
        [printers addObject: [LavuPrinterBase lavuPrinterBaseWithDetails: obj]];
    }];
    
    [loc setPrinters: [printers copy]];
    [menu loadAllDetails: data];
    [self setCurrentLocation: loc];
    [self setCurrentMenu: menu];
    [[self tableView] reloadData];
}

#pragma mark - Default Template
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return MAX([[_currentMenu categories] count], 1);
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    NSArray* categories = [_currentMenu categories];
    if( categories == nil || [categories count] == 0 ){
        return @"Attempt to Retrieve Data Failed, Or The Provided Menu is Empty. Please Check Your Connection.  To Retry, go back and reselect an Item";
    }
    if( [categories count] <= section ){
        return @"";
    }
    
    return [categories[section] name];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    int count = 0;
    NSArray* categories = [_currentMenu categories];
    if( [categories count] <= section ){
        return 0;
    }
    NSNumber* categoryID = [categories[section] ID];
    NSArray* items = [_currentMenu items];
    for( int i = 0; i < items.count; i++ ){
        if( [[items[i] menuCategoryID] isEqualToValue: categoryID] ){
            count++;
        }
    }
    return count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@LAVU_MENU_NAVIGATION_MENU_ITEM_CELL_IDENTIFIER forIndexPath:indexPath];
    NSArray* categories = [_currentMenu categories];
    NSNumber* categoryID = [categories[[indexPath section]] ID];
    NSArray* items = [_currentMenu items];
    NSEnumerator* itemEnumerator = [items objectEnumerator];
    LavuMenuItem* item;
    int it = 0;
    while( item = [itemEnumerator nextObject] ){
        if( [[item menuCategoryID] isEqual: categoryID] ){
            if( it++ == [indexPath row]){
                break;//found it
            }
        }
    }
    
    if( item ){
//        UIImage* menuItemImage  = (UIImage*)[cell viewWithTag: LAVU_MENU_NAVIGATION_MENU_ITEM_IMAGE_TAG];
        UILabel* menuItemLabel = (UILabel*)[cell viewWithTag: LAVU_MENU_NAVIGATION_MENU_ITEM_LABEL_TAG];
        UILabel* priceLabel = (UILabel*)[cell viewWithTag: LAVU_MENU_NAVIGATION_MENU_PRICE_LABEL_TAG];
        UILabel* ingredientLabel = (UILabel*)[cell viewWithTag: LAVU_MENU_NAVIGATION_MENU_INGREDIENT_LABEL_TAG];
        
        [menuItemLabel setText: [item name]];
        NSEnumerator* ingredientIDs = [[item ingredients] keyEnumerator];
        NSNumber* ingredientID;
        NSMutableArray* ingredientList = [[NSArray array] mutableCopy];
        while( ingredientID = [ingredientIDs nextObject] ){
            LavuIngredient* ingredient = [_currentMenu lavuIngredientsFromID: ingredientID];
            [ingredientList addObject: [ingredient title]];
        }
        
        
        [priceLabel setText: [[_currentLocation formatter] stringForObjectValue: [item price]]];
        
        NSStringDrawingContext* stringDrawingContext = [[NSStringDrawingContext alloc] init];
        NSString* allIngredientsString = [ingredientList componentsJoinedByString: @", "];
        
        CGRect measuredStringHeight = [allIngredientsString boundingRectWithSize:CGSizeMake(280.0f, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes: @{} context: stringDrawingContext];
        
        //NSColor
        //NSFont
        //NSParagraphStyle
        //NSShadow
        
        //34 height padding
        CGRect ingredientFrame = [ingredientLabel frame];
        [ingredientLabel setNumberOfLines: 0];
        
        ingredientFrame.size.height = measuredStringHeight.size.height;
        CGRect cellFrame = [cell frame];
        cellFrame.size.height = 40.0f + 43.0f + ingredientFrame.size.height;
        [ingredientLabel setPreferredMaxLayoutWidth: 280.0f];
        [ingredientLabel setFrame: ingredientFrame];
        [cell setFrame: cellFrame];
        [ingredientLabel setText: allIngredientsString];
        [ingredientLabel setNeedsDisplay];
        [cell setNeedsDisplay];
    }
    // Configure the cell...
    
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [self tableView: tableView heightForRowAtIndexPath: indexPath];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSArray* categories = [_currentMenu categories];
    NSNumber* categoryID = [categories[[indexPath section]] ID];
    NSArray* items = [_currentMenu items];
    NSEnumerator* itemEnumerator = [items objectEnumerator];
    LavuMenuItem* item;
    int it = 0;
    while( item = [itemEnumerator nextObject] ){
        if( [[item menuCategoryID] isEqual: categoryID] ){
            if( it++ == [indexPath row]){
                break;//found it
            }
        }
    }
    
    if( item ){
        NSEnumerator* ingredientIDs = [[item ingredients] keyEnumerator];
        NSNumber* ingredientID;
        NSMutableArray* ingredientList = [[NSArray array] mutableCopy];
        while( ingredientID = [ingredientIDs nextObject] ){
            LavuIngredient* ingredient = [_currentMenu lavuIngredientsFromID: ingredientID];
            [ingredientList addObject: [ingredient title]];
        }
        NSStringDrawingContext* stringDrawingContext = [[NSStringDrawingContext alloc] init];
        NSString* allIngredientsString = [ingredientList componentsJoinedByString: @", "];
        
        CGRect measuredStringHeight = [allIngredientsString boundingRectWithSize:CGSizeMake(280.0f, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes: @{} context: stringDrawingContext];
        
        return 40.0f + 43.0f + measuredStringHeight.size.height;
    }
    return 0.0f;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSArray* categories = [_currentMenu categories];
    NSNumber* categoryID = [categories[[indexPath section]] ID];
    NSArray* items = [_currentMenu items];
    NSEnumerator* itemEnumerator = [items objectEnumerator];
    LavuMenuItem* item;
    int it = 0;
    while( item = [itemEnumerator nextObject] ){
        if( [[item menuCategoryID] isEqual: categoryID] ){
            if( it++ == [indexPath row]){
                break;//found it
            }
        }
    }
    
    if( item ){
        _selectedItem = item;
        [self performSegueWithIdentifier:@LAVU_MENU_TO_INGREDIENTS_SEGUE sender:self];
    }
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    id destination = [segue destinationViewController];
    if( [destination respondsToSelector: @selector(setCurrentLocation:)] ){
        [destination setCurrentLocation: [self currentLocation]];
    }
    if( [destination respondsToSelector: @selector(setCurrentMenu:)] ){
        [destination setCurrentMenu: [self currentMenu]];
    }
    if( [destination respondsToSelector: @selector(setCurrentItem:andDecisions:)] ){
        [destination setCurrentItem: [self selectedItem] andDecisions: nil];
    }
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}


@end
