//
//  LavuMenuItem.h
//  APNTest
//
//  Created by Theodore Schnepper on 3/21/14.
//  Copyright (c) 2014 Theodore Schnepper. All rights reserved.
//

#import "LavuBase.h"
#import "LavuAmount.h"

@interface LavuMenuItem : LavuBase
@property (retain, nonatomic) NSNumber* ID;
@property (retain, nonatomic) NSNumber* menuCategoryID;
@property (retain, nonatomic) NSNumber* menuID;
@property (retain, nonatomic) NSString* name;
@property (retain, nonatomic) LavuAmount* price;
@property (retain, nonatomic) NSString* description;
@property (retain, nonatomic) NSString* imagePath;
@property (retain, nonatomic) NSString* imagePath2;
@property (retain, nonatomic) NSString* imagePath3;
@property (retain, nonatomic) NSString* option1;
@property (retain, nonatomic) NSString* option2;
@property (retain, nonatomic) NSString* option3;
@property (retain, nonatomic) NSNumber* active;
@property (retain, nonatomic) NSNumber* order;
@property (retain, nonatomic) NSNumber* print;
@property (retain, nonatomic) NSNumber* quickItem;
@property (retain, nonatomic) NSDate* lastModified;
@property (retain, nonatomic) NSNumber* printer;
@property (retain, nonatomic) NSString* applyTaxRate;
@property (retain, nonatomic) NSString* customTaxRate;
@property (retain, nonatomic) NSNumber* modifierListID;
@property (retain, nonatomic) NSNumber* forcedModifierGroupID;
@property (retain, nonatomic) NSNumber* forcedModifierListID;
@property (retain, nonatomic) NSString* misc;
@property (retain, nonatomic) NSDictionary* ingredients;
@property (retain, nonatomic) NSNumber* openItem;
@property (retain, nonatomic) NSString* hiddenValue;
@property (retain, nonatomic) NSString* hiddenValue2;
@property (retain, nonatomic) NSString* hiddenValue3;
@property (retain, nonatomic) NSNumber* allowDeposit;
@property (retain, nonatomic) NSString* UPC;
@property (retain, nonatomic) NSNumber* inventoryCount;
@property (retain, nonatomic) NSNumber* showInApp;
@property (retain, nonatomic) NSNumber* superGroupID;
@property (retain, nonatomic) NSNumber* taxInclusion;
@property (retain, nonatomic) NSNumber* lavutogoDisplay;
@property (retain, nonatomic) NSNumber* happyHour;
@property (retain, nonatomic) NSNumber* taxProfileID;
@property (retain, nonatomic) NSNumber* chainReportingGroup;
@property (retain, nonatomic) NSNumber* track86Count;
@property (retain, nonatomic) NSNumber* priceTierProfileID;

- (void) setPriceFromNumber:(NSNumber *)price;
+ (id) lavuMenuItem: (NSDictionary*) details;
@end
