//
//  main.m
//  APNTest
//
//  Created by Theodore Schnepper on 3/12/14.
//  Copyright (c) 2014 Theodore Schnepper. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "LavuAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([LavuAppDelegate class]));
    }
}
