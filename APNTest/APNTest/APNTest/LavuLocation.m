//
//  LavuLocation.m
//  APNTest
//
//  Created by Theodore Schnepper on 3/21/14.
//  Copyright (c) 2014 Theodore Schnepper. All rights reserved.
//

#import "LavuLocation.h"

@implementation LavuLocation {
    NSArray* menus;
    NSArray* printers;
    NSArray* ingredients;
}
static NSArray* _locations;

@synthesize ID = _ID;
@synthesize restaurantID = _restaurantID;
@synthesize menuID = _menuID;
@synthesize title = _title;
@synthesize address = _address;
@synthesize city = _city;
@synthesize state = _state;
@synthesize country = _country;
@synthesize zip = _zip;
@synthesize phone = _phone;
@synthesize website = _website;
@synthesize email = _email;
@synthesize manager = _manager;
@synthesize netPath = _netPath;
@synthesize netPathPrint = _netPathPrint;
@synthesize useNetPath = _useNetPath;
@synthesize timezone = _timezone;
@synthesize monetarySymbol = _monetarySymbol;
@synthesize leftOrRight = _leftOrRight;
@synthesize decimalCharacter = _decimalCharacter;
@synthesize thousandsCharacter = _thousandsCharacter;
@synthesize decimalPlaces = _decimalPlaces;
@synthesize taxInclusion = _taxInclusion;
@synthesize roundingFactor = _roundingFactor;
@synthesize roundUpOrDown = _roundUpOrDown;
@synthesize useDirectPrinting = _useDirectPrinting;
@synthesize printers = _printers;

- (id) init {
    self = [super init];
    if( self ){
        menus = [[NSArray alloc] init];
        _printers = [[NSArray alloc] init];
        ingredients = [[NSArray alloc] init];
        if( ![LavuLocation lavuLocations] ){
            [LavuLocation setLavuLocations: [[NSArray alloc] init]];
        }
    }
    return self;
}

+ (void) setLavuLocations: (NSArray*) lavuLocations {
    _locations = lavuLocations;
}
+ (NSArray*) lavuLocations {
    return _locations;
}

+ (id) lavuLocation: (NSDictionary*) details {
    LavuLocation* result = [[LavuLocation alloc] init];
    if(! [result setValueUsing: @selector(setID:) ifKeyExists:@"id" usingDetails: details]){
        return nil;
    }
    if(! [result setValueUsing: @selector(setMenuID:) ifKeyExists:@"menu_id" usingDetails: details]){
        return nil;
    }
    [result setValueUsing: @selector(setRestaurantID:) ifKeyExists:@"rest_id" usingDetails: details];
    [result setValueUsing: @selector(setAddress:) ifKeyExists:@"address" usingDetails: details];
    [result setValueUsing: @selector(setCity:) ifKeyExists:@"city" usingDetails: details];
    [result setValueUsing: @selector(setState:) ifKeyExists:@"state" usingDetails: details];
    [result setValueUsing: @selector(setCountry:) ifKeyExists:@"country" usingDetails: details];
    [result setValueUsing: @selector(setZip:) ifKeyExists:@"zip" usingDetails: details];
    [result setValueUsing: @selector(setPhone:) ifKeyExists:@"phone" usingDetails: details];
    [result setValueUsing: @selector(setWebsite:) ifKeyExists:@"website" usingDetails: details];
    [result setValueUsing: @selector(setEmail:) ifKeyExists:@"email" usingDetails: details];
    [result setValueUsing: @selector(setManager:) ifKeyExists:@"manager" usingDetails: details];
    [result setValueUsing: @selector(setNetPath:) ifKeyExists:@"net_path" usingDetails: details];
    [result setValueUsing: @selector(setNetPathPrint:) ifKeyExists:@"net_path_print" usingDetails: details];
    [result setValueUsing: @selector(setUseNetPath:) ifKeyExists:@"use_net_path" usingDetails: details];
    [result setValueUsing: @selector(setTimeZone:) ifKeyExists:@"timezone" usingDetails: details];
    [result setValueUsing: @selector(setMonetarySymbol:) ifKeyExists:@"monitary_symbol" usingDetails: details];
    [result setValueUsing: @selector(setLeftOrRightWithString:) ifKeyExists:@"left_or_right" usingDetails: details];
    [result setValueUsing: @selector(setDecimalCharacter:) ifKeyExists:@"decimal_char" usingDetails: details];
    [result setValueUsing: @selector(setThousandsCharacter:) ifKeyExists:@"thousands_char" usingDetails: details];
    [result setValueUsing: @selector(setDecimalPlaces:) ifKeyExists:@"disable_decimal" usingDetails: details];
    [result setValueUsing: @selector(setTaxInclusion:) ifKeyExists:@"tax_inclusion" usingDetails: details];
    [result setValueUsing: @selector(setRoundingFactor:) ifKeyExists:@"rounding_factor" usingDetails: details];
    [result setValueUsing: @selector(setRoundUpOrDown:) ifKeyExists:@"round_up_or_down" usingDetails: details];
    [result setValueUsing: @selector(setUseDirectPrinting:) ifKeyExists:@"use_direct_printing" usingDetails: details];
    
    [result setFormatter: [[LavuMonetaryFormatter alloc] init]];
    [[result formatter] setMonetarySymbol: [result monetarySymbol]];
    [[result formatter] setLeftOrRight: [result leftOrRight]];
    [[result formatter] setDecimalCharacter: [result decimalCharacter]];
    [[result formatter] setThousandsCharacter: [result thousandsCharacter]];
    [[result formatter] setDecimalPlaces: [result decimalPlaces]];

    return result;
}

- (void) setLeftOrRightWithString:(NSString*)leftOrRight {
    if( [[leftOrRight lowercaseString] isEqualToString: @"left"] ){
        _leftOrRight = LavuMonetaryPositionLeft;
    } else {
        _leftOrRight = LavuMonetaryPositionRight;
    }
}

- (NSArray*) lavuMenus {
    return menus;
}

- (NSArray*) lavuIngredients {
    return ingredients;
}

- (id) lavuMenuFromID: (NSNumber*) menuID {
    for( int i = 0; i < menus.count; i++ ){
        if( [[menus[i] ID] isEqual: menuID] ){
            return menus[i];
        }
    }
    return nil;
}

- (id) lavuIngredientFromID: (NSNumber*) ingredientID {
    for( int i = 0; i < ingredients.count; i++ ){
        if( [[ingredients[i] ID] isEqual: ingredientID] ){
            return ingredients[i];
        }
    }
    return nil;
}

- (id) printerFromIdentifier: (NSString*) identifier {
    for( int i = 0; i < [_printers count]; i++ ){
        if( [[_printers[i] printerID] isEqualToString: identifier] ){
            return _printers[i];
        }
    }
    return nil;
}

@end
