//
//  LavuModifierCategory.m
//  APNTest
//
//  Created by Theodore Schnepper on 3/21/14.
//  Copyright (c) 2014 Theodore Schnepper. All rights reserved.
//

#import "LavuModifierCategory.h"

@implementation LavuModifierCategory
@synthesize ID = _ID;
@synthesize title = _title;
@synthesize menuID = _menuID;

+ (id) lavuModifierCategory: (NSDictionary*) details {
    LavuModifierCategory* result = [[LavuModifierCategory alloc] init];
    if(! [result setValueUsing: @selector(setID:) ifKeyExists:@"id" usingDetails: details]){
        return nil;
    }
    [result setValueUsing: @selector(setTitle:) ifKeyExists:@"title" usingDetails: details];
    [result setValueUsing: @selector(setMenuID:) ifKeyExists:@"menu_id" usingDetails: details];

    return result;
}

@end
