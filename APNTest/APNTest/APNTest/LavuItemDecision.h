//
//  LavuItemDecision.h
//  APNTest
//
//  Created by Theodore Schnepper on 3/28/14.
//  Copyright (c) 2014 Theodore Schnepper. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LavuMenuItem.h"
#import "LavuForcedModifierDecision.h"
#import "LavuModifier.h"
@interface LavuItemDecision : NSObject
@property (retain, nonatomic) LavuMenuItem* item;
@property (retain, nonatomic) LavuForcedModifierDecisionTreeRoot* forcedModifiersDecisions;
@property (retain, nonatomic) NSArray* addedOptionalModifiers;
@property (retain, nonatomic) NSArray* removedOptionalModifiers;
@property (retain, nonatomic) NSNumber* quantity;

- (void) addAddedOptionalModifier: (LavuModifier*) modifier;
- (void) removeAddedOptionalModifier: (LavuModifier*) modifier;
- (void) addRemovedOptionalModifier: (LavuModifier*) modifier;
- (void) removeRemovedOptionalModifier: (LavuModifier*) modifier;

- (LavuAmount*) calculateSubtotal;
@end
