//
//  LavuTrackViewController.m
//  APNTest
//
//  Created by Theodore Schnepper on 3/14/14.
//  Copyright (c) 2014 Theodore Schnepper. All rights reserved.
//

#import "LavuTrackViewController.h"

@interface LavuTrackViewController ()

@end

@implementation LavuTrackViewController

#pragma mark - Beacon Delgate Garbage
- (void)locationManager:(CLLocationManager *)manager didEnterRegion:(CLRegion *)region {
    [self.locationManager startRangingBeaconsInRegion:self.beaconRegion];
}

-(void)locationManager:(CLLocationManager *)manager didExitRegion:(CLRegion *)region {
    [self.locationManager stopRangingBeaconsInRegion:self.beaconRegion];
    self.beaconFoundLabel.text = @"No";
}

- (void)locationManager:(CLLocationManager *)manager didStartMonitoringForRegion:(CLRegion *)region {
    [self.locationManager startRangingBeaconsInRegion:self.beaconRegion];
}

-(void)locationManager:(CLLocationManager *)manager didRangeBeacons:(NSArray *)beacons inRegion:(CLBeaconRegion *)region {
    CLBeacon *beacon = [[CLBeacon alloc] init];
    beacon = [beacons lastObject];
    
    if( [[UIApplication sharedApplication] applicationState] == UIApplicationStateActive){
        self.beaconFoundLabel.text = @"Yes";
        self.proximityUUIDLabel.text = beacon.proximityUUID.UUIDString;
        self.majorLabel.text = [NSString stringWithFormat:@"%@", beacon.major];
        self.minorLabel.text = [NSString stringWithFormat:@"%@", beacon.minor];
        self.accuracyLabel.text = [NSString stringWithFormat:@"%f", beacon.accuracy];
        if (beacon.proximity == CLProximityUnknown) {
            self.distanceLabel.text = @"Unknown Proximity";
            self.beaconFoundLabel.text = @"No";
        } else if (beacon.proximity == CLProximityImmediate) {
            self.distanceLabel.text = @"Immediate";
        } else if (beacon.proximity == CLProximityNear) {
            self.distanceLabel.text = @"Near";
        } else if (beacon.proximity == CLProximityFar) {
            self.distanceLabel.text = @"Far";
        }
        self.rssiLabel.text = [NSString stringWithFormat:@"%li", (long)beacon.rssi];
    } else {
        UILocalNotification* notification = [[UILocalNotification alloc] init];
        [notification setFireDate: [NSDate date]];
        [notification setAlertBody:@"You are Within Range of a Beacon!"];
        [notification setAlertAction: @"Lavu Range Notification"];
        [notification setApplicationIconBadgeNumber: [beacons count]];
        [notification setSoundName: @"bingbong.aiff"];
        
        //[application scheduleLocalNotification: notification];
        [[UIApplication sharedApplication] presentLocalNotificationNow:notification];
    }
}


#pragma mark - Custom Initialization
- (void)initRegion {
    NSUUID *uuid = [[NSUUID alloc] initWithUUIDString:@"EE71F129-8E70-4684-AE33-8A545EEECBD4"];
    self.beaconRegion = [[CLBeaconRegion alloc] initWithProximityUUID:uuid identifier:@"com.POSLavu.APNTest"];
    [self.locationManager startMonitoringForRegion:self.beaconRegion];
}

#pragma mark - default

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    [self initRegion];
    
//    [self locationManager:self.locationManager didStartMonitoringForRegion:self.beaconRegion];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
