//
//  LavuQuantityChanged.h
//  APNTest
//
//  Created by Theodore Schnepper on 3/31/14.
//  Copyright (c) 2014 Theodore Schnepper. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol LavuQuantityChangedProtocol <NSObject>
- (IBAction) quantityChangedTo: (NSNumber*) number;
@end
