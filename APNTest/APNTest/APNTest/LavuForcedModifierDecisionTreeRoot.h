//
//  LavuForcedModifierDecisionTreeRoot.h
//  APNTest
//
//  Created by Theodore Schnepper on 4/1/14.
//  Copyright (c) 2014 Theodore Schnepper. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LavuForcedModifierDecisionTreeRoot : NSObject
@property (retain, nonatomic) NSArray* children;
@property (retain, nonatomic) LavuForcedModifierDecisionTreeRoot* parent;

- (BOOL) isRoot;

- (NSNumber*) totalChildrenInTree;
- (id) objectAtOffset: (NSInteger) offset;
- (NSNumber*) calculateIndexForItem: (id) item;
- (NSEnumerator*) enumerator;
@end

@interface LavuForcedModifierDecisionTreeEnumerator : NSEnumerator
- (id) initWithArray: (NSArray*) array;
@end