//
//  LavuMonitaryFormatter.m
//  APNTest
//
//  Created by Theodore Schnepper on 4/1/14.
//  Copyright (c) 2014 Theodore Schnepper. All rights reserved.
//

#import "LavuMonetaryFormatter.h"

@implementation LavuMonetaryFormatter
@synthesize monetarySymbol = _monetarySymbol;
@synthesize leftOrRight = _leftOrRight;
@synthesize decimalCharacter = _decimalCharacter;
@synthesize thousandsCharacter = _thousandsCharacter;
@synthesize decimalPlaces = _decimalPlaces;


- (NSString*) fillWithThousandsSeparator: (NSString*) preDecimal {
    NSString* temp = [preDecimal copy];
    NSString* result = @"";
    
    if( [preDecimal length] % 3 ) {
        if( [preDecimal length] % 3 != [preDecimal length] ){
            result = [temp substringWithRange:(NSRange){ 0, [preDecimal length] % 3 }];
            result = [result stringByAppendingString: _thousandsCharacter];
            temp = [temp substringFromIndex: [preDecimal length] % 3 ];
        } else {
            return temp;
        }
    }
    while( [temp length] > 0 ) {
        result = [result stringByAppendingString: [temp substringWithRange: (NSRange){ 0, 3 }]];
//        temp = [temp substringWithRange: (NSRange){ 0, 3 }];
        temp = [temp substringFromIndex: 3];
        if( [temp length] ){
            result = [result stringByAppendingString: _thousandsCharacter];
        }
    }
    return result;
}

- (NSString *)stringForObjectValue:(id)anObject {
    if( !anObject ){
        return @"";
    }
    if( ![anObject isKindOfClass: [LavuAmount class]] ){
        return @"";
    }
    
    LavuAmount* amount = anObject;
    NSString* str = [NSString stringWithFormat: @"%lli", [amount baseUnit]];
    NSInteger decimalBeginsPosition = [str length] - MIN( [str length], [amount decimalPlaces]);
    NSInteger totalStringLength = (decimalBeginsPosition + MAX( (NSInteger)[amount decimalPlaces], [_decimalPlaces integerValue] ));
    str = [str stringByPaddingToLength:totalStringLength withString:@"0" startingAtIndex:0];
    NSString* preDecimal = [str substringWithRange: (NSRange){ 0, decimalBeginsPosition }];
    NSString* postDecimal = [str substringFromIndex: decimalBeginsPosition];
    preDecimal = [self fillWithThousandsSeparator: preDecimal];
    
    NSString* leftStr = @"";
    NSString* rightStr = @"";
    
    if( _leftOrRight == LavuMonetaryPositionLeft ){
        leftStr = _monetarySymbol;
    } else {
        rightStr = _monetarySymbol;
    }
    
    return [NSString stringWithFormat:@"%@%@%@%@%@", leftStr, preDecimal, _decimalCharacter, [postDecimal substringWithRange: (NSRange){ 0, [_decimalPlaces integerValue]}], rightStr];
}

- (NSAttributedString *)attributedStringForObjectValue:(id)anObject withDefaultAttributes:(NSDictionary *)attributes {
    return [[NSAttributedString alloc] initWithString:[self stringForObjectValue: anObject]];
}

- (NSString *)editingStringForObjectValue:(id)anObject {
    return [self stringForObjectValue: anObject];
}

- (BOOL)getObjectValue:(id *)anObject forString:(NSString *)string errorDescription:(NSString **)error {
    return false;
}
@end
