//
//  LavuMainMenuTableViewController.h
//  APNTest
//
//  Created by Theodore Schnepper on 3/19/14.
//  Copyright (c) 2014 Theodore Schnepper. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "LavuMenuTableViewController.h"

#define LAVU_MENU_SECTION_ORDERS 0
#define LAVU_MENU_SECTION_LOCATIONS 1
#define LAVU_MENU_SECTIONS_COUNT 2

#define LAVU_MENU_ORDERS_SECTION_CURRENT_ORDER 0
#define LAVU_MENU_ORDERS_SECTION_ORDER_HISTORY 1
#define LAVU_MENU_ORDERS_SECTION_ROWS_COUNT 2

#define LAVU_MAIN_TO_MENU_SEGUE_IDENTIFIER "show_menu"
#define LAVU_MAIN_TO_ORDER_SEGUE_IDENTIFIER "goto_order"

#define LAVU_MAIN_MENU_NAVIGATION_ITEM_CELL_IDENTIFIER "navigation_item"
#define LAVU_MAIN_MENU_NAVIGATION_ITEM_IMAGE_TAG 1
#define LAVU_MAIN_MENU_NAVIGATION_ITEM_LABEL_TAG 2

@interface LavuMainMenuTableViewController : UITableViewController <CLLocationManagerDelegate>

@property (strong, nonatomic) CLBeaconRegion *beaconRegion;
@property (strong, nonatomic) CLLocationManager *locationManager;
@property (strong, nonatomic) NSMutableArray *trackedBeacons;
@property (strong, nonatomic) NSMutableArray *trackedLocations;
@property (retain, nonatomic) NSIndexPath* selection;

@property (strong, nonatomic) NSMutableArray *currentLocations;

@property (strong, nonatomic) NSMutableDictionary *beaconLocationMappings; // (major, minor) -> (rest_id, loc_id, menu_group_id, [category_id])
@property (retain, nonatomic) NSMutableDictionary* mappingData; //NSIndexPath(rest_id, loc_id, menu_group_id, [category_id]) -> mapping

@end
