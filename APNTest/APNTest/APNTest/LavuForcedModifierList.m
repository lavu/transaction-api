//
//  LavuForcedModifierList.m
//  APNTest
//
//  Created by Theodore Schnepper on 3/21/14.
//  Copyright (c) 2014 Theodore Schnepper. All rights reserved.
//

#import "LavuForcedModifierList.h"

@implementation LavuForcedModifierList
@synthesize ID = _ID;
@synthesize title = _title;
@synthesize menuID = _menuID;
@synthesize type = _type;

+ (id) lavuForcedModifierList: (NSDictionary*) details {
    LavuForcedModifierList* result = [[LavuForcedModifierList alloc] init];
    if(! [result setValueUsing: @selector(setID:) ifKeyExists:@"id" usingDetails: details]){
        return nil;
    }
//    [result setValueUsing: @selector(setDescription:) ifKeyExists:@"description" usingDetails: details];
    [result setValueUsing: @selector(setTitle:) ifKeyExists:@"title" usingDetails: details];
    [result setValueUsing: @selector(setType:) ifKeyExists:@"type" usingDetails: details];
//    [result setValueUsing: @selector(setDeleted:) ifKeyExists:@"_deleted" usingDetails: details];
    [result setValueUsing: @selector(setMenuID:) ifKeyExists:@"menu_id" usingDetails: details];
//    [result setValueUsing: @selector(setChainReportingGroup:) ifKeyExists:@"chain_reporting_group" usingDetails: details];
    return result;

}
@end
