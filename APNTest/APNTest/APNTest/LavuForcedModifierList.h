//
//  LavuForcedModifierList.h
//  APNTest
//
//  Created by Theodore Schnepper on 3/21/14.
//  Copyright (c) 2014 Theodore Schnepper. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LavuBase.h"

typedef enum {
    LavuForcedModifierListTypeCheckList,
    LavuForcedModifierListTypeChoice
} LavuForcedModifierListTypes;

@interface LavuForcedModifierList : LavuBase
@property (retain, nonatomic) NSNumber* ID;
@property (retain, nonatomic) NSString* title;
@property (retain, nonatomic) NSNumber* menuID;
@property (nonatomic) LavuForcedModifierListTypes type;

+ (id) lavuForcedModifierList: (NSDictionary*) details;
@end
