//
//  LavuMenuGroup.m
//  APNTest
//
//  Created by Theodore Schnepper on 3/21/14.
//  Copyright (c) 2014 Theodore Schnepper. All rights reserved.
//

#import "LavuMenuGroup.h"

@implementation LavuMenuGroup
@synthesize ID = _ID;
@synthesize name = _name;
@synthesize menuID = _menuID;
@synthesize order = _order;

+ (id) lavuMenuGroup: (NSDictionary*) details {
    LavuMenuGroup* result = [[LavuMenuGroup alloc] init];
    if(! [result setValueUsing: @selector(setID:) ifKeyExists:@"id" usingDetails: details]){
        return nil;
    }
    [result setValueUsing: @selector(setMenuID:) ifKeyExists:@"menu_id" usingDetails: details];
    [result setValueUsing: @selector(setName:) ifKeyExists:@"group_name" usingDetails: details];
    [result setValueUsing: @selector(setOrder:) ifKeyExists:@"_order" usingDetails: details];
//    [result setValueUsing: @selector(setOrderBy:) ifKeyExists:@"orderby" usingDetails: details];
//    [result setValueUsing: @selector(setDeleted:) ifKeyExists:@"_deleted" usingDetails: details];
//    [result setValueUsing: @selector(setchainReportingGroup:) ifKeyExists:@"chain_reporting_group" usingDetails: details];
    return result;
}
@end
