//
//  LavuAPNManager.h
//  APNTest
//
//  Created by Theodore Schnepper on 4/3/14.
//  Copyright (c) 2014 Theodore Schnepper. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Communicator.h"

@interface LavuAPNManager : NSObject
@property (retain, nonatomic) NSString* groupIdentifier;
@property (retain, nonatomic) NSString* deviceIdentifier;
@property (retain, nonatomic) NSData* apnIdentifier;
@property (retain, nonatomic) Communicator* comm;
+ (instancetype) sharedInstance;
- (void) registerForAppAPN;
@end
