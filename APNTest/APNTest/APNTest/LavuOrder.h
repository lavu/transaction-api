//
//  LavuOrder.h
//  APNTest
//
//  Created by Theodore Schnepper on 4/2/14.
//  Copyright (c) 2014 Theodore Schnepper. All rights reserved.
//

#import "LavuBase.h"
#import "LavuItemDecision.h"
#import "LavuLocation.h"

@interface LavuOrder : LavuBase
@property (retain, nonatomic) NSString* orderID;
@property (retain, nonatomic) NSString* ioid;
@property (retain, nonatomic) NSNumber* guests;
@property (retain, nonatomic) NSNumber* courses;
@property (retain, nonatomic) NSNumber* checks;
@property (retain, nonatomic) NSDate* opened;
@property (retain, nonatomic) NSDate* closed;
@property (retain, nonatomic) NSString* orderTag;
@property (retain, nonatomic) NSString* serverName;
@property (retain, nonatomic) NSString* tableName;
@property (retain, nonatomic) LavuLocation* location;
@property BOOL sentToKitchen;

@property (retain, nonatomic) NSSet* items;

- (void) addItem: (LavuItemDecision*) item;
- (void) removeItem: (LavuItemDecision*) item;
+ (id) currentOrder;
- (LavuAmount*) calculateSubtotal;
- (LavuAmount*) calculateTax;
- (LavuAmount*) calculateTotal;
@end
