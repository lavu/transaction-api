//
//  LavuForcedModifierGroup.h
//  APNTest
//
//  Created by Theodore Schnepper on 3/21/14.
//  Copyright (c) 2014 Theodore Schnepper. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LavuBase.h"

@interface LavuForcedModifierGroup : LavuBase
@property (retain, nonatomic) NSNumber* ID;
@property (retain, nonatomic) NSString* title;
@property (retain, nonatomic) NSNumber* menuID;
@property (retain, nonatomic) NSArray* forcedModifierLists;

+ (id) lavuForcedModifierGroup: (NSDictionary*) details;
@end
