//
//  LavuPrinterFormatting.h
//  APNTest
//
//  Created by Theodore Schnepper on 4/7/14.
//  Copyright (c) 2014 Theodore Schnepper. All rights reserved.
//

#import "LavuBase.h"

@interface LavuPrinterFormatting : LavuBase
@property (retain, nonatomic) NSNumber* charactersPerLine;
@property (retain, nonatomic) NSNumber* charactersPerLineLargeFont;
@property (retain, nonatomic) NSNumber* lineSpacing;
@property BOOL individualTicketPerItem;

+ (LavuPrinterFormatting*) lavuPrinterFormmatingWithDetails: (NSDictionary*) dict;
@end
