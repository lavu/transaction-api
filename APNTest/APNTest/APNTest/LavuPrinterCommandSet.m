//
//  LavuPrinterCommandSet.m
//  APNTest
//
//  Created by Theodore Schnepper on 4/2/14.
//  Copyright (c) 2014 Theodore Schnepper. All rights reserved.
//

#import "LavuPrinterCommandSet.h"

@implementation LavuPrinterCommandSet
- (NSInteger)versionForClassName:(NSString *)className {
    return 1;
}

- (NSInteger) commandSetIdentifier {
    @throw [NSException exceptionWithName:NSInternalInconsistencyException reason:[NSString stringWithFormat:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)] userInfo:nil];
}

- (NSString*) commandSetName {
    @throw [NSException exceptionWithName:NSInternalInconsistencyException reason:[NSString stringWithFormat:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)] userInfo:nil];
}

- (NSData*) generatePrintData: (id) order usingFormatting: (LavuPrinterFormatting*) formatting {
    @throw [NSException exceptionWithName:NSInternalInconsistencyException reason:[NSString stringWithFormat:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)] userInfo:nil];
}

@end
