//
//  LavuForcedModifierDecisionTreeRoot.m
//  APNTest
//
//  Created by Theodore Schnepper on 4/1/14.
//  Copyright (c) 2014 Theodore Schnepper. All rights reserved.
//

#import "LavuForcedModifierDecisionTreeRoot.h"
#import "LavuForcedModifierDecision.h"

@implementation LavuForcedModifierDecisionTreeRoot {
    int treeCount;
}
@synthesize children = _children;
@synthesize parent = _parent;

- (id) init {
    self = [super init];
    if( self ){
        _children = [NSArray array];
        _parent = nil;
        treeCount = 0;
    }
    return self;
}

- (BOOL) isRoot {
    return _parent == nil;
}

- (void) setChildren:(NSArray *)children {
    treeCount = 0;
    if( _parent ){
        [_parent setChildren: [_parent children]];
    }
    [self totalChildrenInTree];
    _children = children;
}

- (NSNumber*) totalChildrenInTree {
    if( treeCount ){
        return [NSNumber numberWithInt: treeCount];
    }
    int __block total = 0;
    if( _children ){
        total += [_children count];
        [_children enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            total += [[obj totalChildrenInTree] integerValue];
        }];
    }
    
    return [NSNumber numberWithInt: total];
}

- (id) objectAtOffset: (NSInteger) offset {
    NSInteger currentOffset = offset;
    if( currentOffset == 0 && ![self isRoot]){
        return self;
    } else if( ![self isRoot] ){
        currentOffset--;
    }
    
    if( _children ){
        for( int i = 0; i < [_children count]; i++ ){
            if( [[_children[i] totalChildrenInTree] integerValue] + 1 > currentOffset ){
                return [_children[i] objectAtOffset: currentOffset];
            } else {
                currentOffset -= ([[_children[i] totalChildrenInTree] integerValue] + 1);
            }
        }
    }
    
    return nil;
}

- (int) _calculateIndexForItem: (id) item withAcc: (int) acc {
    if( self == item ){
        return acc;
    }
    for( int i = 0; i < [_children count]; i++ ){
        int res = 0;
        if( [self isRoot] ){
            res = [_children[i] _calculateIndexForItem: item withAcc: acc];
        } else {
            res = [_children[i] _calculateIndexForItem: item withAcc: acc+1];
        }
        if( res == -1 ){
            acc += [[_children[i] totalChildrenInTree] integerValue]+1;
        } else {
            return res;
        }
    }
    
    return -1;
}

- (NSNumber*) calculateIndexForItem: (id) item {
    return [NSNumber numberWithInt: [self _calculateIndexForItem: item withAcc: 0]];
}

- (void) buildArray: (NSMutableArray*) array {
    if( ![self isRoot] ){
        [array addObject: self];
    }
    
    [_children enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop){
        [obj buildArray: array];
    }];
}

- (NSEnumerator*) enumerator {
    NSMutableArray* array = [NSMutableArray array];
    [self buildArray: array];
    
    return [[LavuForcedModifierDecisionTreeEnumerator alloc] initWithArray: array];
}
@end

@implementation LavuForcedModifierDecisionTreeEnumerator {
    NSArray* _array;
    int _index;
}
- (id) initWithArray: (NSArray*) array {
    self = [super init];
    if( self ){
        _array = array;
        _index = 0;
    }
    return self;
}

- (NSArray *)allObjects {
    return [_array copy];
}

- (id)nextObject {
    if( _index >= [_array count] ){
        return nil;
    }
    
    return _array[_index++];
}
@end
