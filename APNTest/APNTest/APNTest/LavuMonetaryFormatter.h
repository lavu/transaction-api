//
//  LavuMonitaryFormatter.h
//  APNTest
//
//  Created by Theodore Schnepper on 4/1/14.
//  Copyright (c) 2014 Theodore Schnepper. All rights reserved.
//


#import <Foundation/Foundation.h>
#import "LavuAmount.h"
typedef enum {
    LavuMonetaryPositionLeft,
    LavuMonetaryPositionRight
} LavuMonetaryPosition;

@interface LavuMonetaryFormatter : NSFormatter
@property (retain, nonatomic) NSString* monetarySymbol;
@property LavuMonetaryPosition leftOrRight;
@property (retain, nonatomic) NSString* decimalCharacter;
@property (retain, nonatomic) NSString* thousandsCharacter;
@property (retain, nonatomic) NSNumber* decimalPlaces;
@end
