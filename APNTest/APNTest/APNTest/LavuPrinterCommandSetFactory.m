//
//  LavuPrinterCommandSetFactory.m
//  APNTest
//
//  Created by Theodore Schnepper on 4/7/14.
//  Copyright (c) 2014 Theodore Schnepper. All rights reserved.
//

#import "LavuPrinterCommandSetFactory.h"

@implementation LavuPrinterCommandSetFactory
+ (LavuPrinterCommandSet*) createPrinterCommandSetWithInteger: (NSInteger) commandSetIdentifier {
    switch ( commandSetIdentifier ) {
        case LAVU_PRINTER_COMMAND_SET_LAVU_KDS:
            return [[LavuPrinterCommandSetKDSLavu alloc] init];
        default:
            return nil;
    }
}
@end
