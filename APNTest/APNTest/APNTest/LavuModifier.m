//
//  LavuModifier.m
//  APNTest
//
//  Created by Theodore Schnepper on 3/21/14.
//  Copyright (c) 2014 Theodore Schnepper. All rights reserved.
//

#import "LavuModifier.h"

@implementation LavuModifier
@synthesize ID = _ID;
@synthesize title = _title;
@synthesize lavuModifierCategoryID = _lavuModifierCategoryID;
@synthesize cost = _cost;
@synthesize ingredients = _ingredients;

- (id) init {
    self = [super init];
    if( self ){
        _ingredients = [NSDictionary dictionary];
    }
    return self;
}

- (void) setCostFromNumber:(NSNumber *)cost{
    _cost = [[LavuAmount alloc] initWithDouble: [cost doubleValue]];
}

+ (id) lavuModifier: (NSDictionary*) details {
    LavuModifier* result = [[LavuModifier alloc] init];
    if(! [result setValueUsing: @selector(setID:) ifKeyExists:@"id" usingDetails: details]){
        return nil;
    }
    [result setValueUsing: @selector(setTitle:) ifKeyExists:@"title" usingDetails: details];
    [result setValueUsing: @selector(setLavuModifierCategoryID:) ifKeyExists:@"category" usingDetails: details];
    [result setValueUsing: @selector(setCostFromNumber:) ifKeyExists:@"cost" usingDetails: details];
//    [result setValueUsing: @selector(setDeleted:) ifKeyExists:@"_deleted" usingDetails: details];
//    [result setValueUsing: @selector(setOrder:) ifKeyExists:@"_order" usingDetails: details];
    if( ![[NSNull null] isEqual: details[@"ingredients"]] && ![details[@"ingredients"] isEqual: @""] ){
        // id x qty, ...
        NSMutableDictionary* dict = [[result ingredients] mutableCopy];
        NSArray* ingredient_parts = [details[@"ingredients"] componentsSeparatedByString: @","];
        for( int i = 0; i < ingredient_parts.count; i++ ){
            NSString* ingredient_details = ingredient_parts[i];
            NSArray* ingredients = [ingredient_details componentsSeparatedByString: @"x"];
            if( ingredients.count < 2 ){
                continue;
            }
            dict[[NSNumber numberWithInt: [ingredients[0] intValue]]] = [NSNumber numberWithInt: [ingredients[1] intValue]];
        }
        [result setIngredients: dict];
    }
    //    [result setValueUsing: @selector(setChainReportingGroup:) ifKeyExists:@"chain_reporting_group" usingDetails: details];
    return result;
}
@end
