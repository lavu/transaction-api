//
//  LavuForcedModifier.m
//  APNTest
//
//  Created by Theodore Schnepper on 3/21/14.
//  Copyright (c) 2014 Theodore Schnepper. All rights reserved.
//

#import "LavuForcedModifier.h"

@implementation LavuForcedModifier
@synthesize ID = _ID;
@synthesize title = _title;
@synthesize listID = _listID;
@synthesize cost = _cost;
@synthesize forcedModifierGroupDetour = _forcedModifierGroupDetour;
@synthesize forcedModifierListDetour = _forcedModifierListDetour;
@synthesize ingredients = _ingredients;
@synthesize extra = _extra;
@synthesize extra2 = _extra2;
@synthesize extra3 = _extra3;
@synthesize extra4 = _extra4;
@synthesize extra5 = _extra5;

- (void) setCostFromNumber:(NSNumber *)cost{
    _cost = [[LavuAmount alloc] initWithDouble: [cost doubleValue]];
}

- (void) setForcedModifierGroupOrListIDDetour: (NSString*) idDetails {
    if( [idDetails isEqualToString: @""] ){
        return;
    }
    if( [idDetails characterAtIndex: 0] == 'c' || [idDetails characterAtIndex: 0] == 'C' || [idDetails isEqualToString: @""] ){
        [self setForcedModifierListDetour: [NSNumber numberWithInteger: 0]];
        [self setForcedModifierGroupDetour: [NSNumber numberWithInteger: 0]];
    } else if( [idDetails characterAtIndex: 0] == 'l' ){
        //list
        [self setForcedModifierListDetour: [NSNumber numberWithInteger: [[idDetails substringFromIndex: 1] integerValue]]];
        [self setForcedModifierGroupDetour: [NSNumber numberWithInteger: 0]];
    } else {
        //group
        if( [idDetails characterAtIndex: 0] >= '0' && [idDetails characterAtIndex: 0] <= '9' ) {
            //Just a Number
            [self setForcedModifierGroupDetour: [NSNumber numberWithInteger: [idDetails integerValue]]];
        } else {
            [self setForcedModifierGroupDetour: [NSNumber numberWithInteger: [[idDetails substringFromIndex: 1] integerValue]]];
        }
        [self setForcedModifierListDetour: [NSNumber numberWithInteger: 0]];
    }
}

+ (id) lavuForcedModifier: (NSDictionary*) details {
    LavuForcedModifier* result = [[LavuForcedModifier alloc] init];
    if(! [result setValueUsing: @selector(setID:) ifKeyExists:@"id" usingDetails: details]){
        return nil;
    }
    [result setValueUsing: @selector(setTitle:) ifKeyExists:@"title" usingDetails: details];
    [result setValueUsing: @selector(setCostFromNumber:) ifKeyExists:@"cost" usingDetails: details];
    [result setValueUsing: @selector(setListID:) ifKeyExists:@"list_id" usingDetails: details];
//    [result setValueUsing: @selector(setOrder:) ifKeyExists:@"_order" usingDetails: details];
//    [result setValueUsing: @selector(setDeleted:) ifKeyExists:@"_deleted" usingDetails: details];
    [result setValueUsing: @selector(setForcedModifierGroupOrListIDDetour:) ifKeyExists:@"detour" usingDetails: details];
    [result setValueUsing: @selector(setExtra:) ifKeyExists:@"extra" usingDetails: details];
    [result setValueUsing: @selector(setExtra2:) ifKeyExists:@"extra2" usingDetails: details];
    [result setValueUsing: @selector(setExtra3:) ifKeyExists:@"extra3" usingDetails: details];
    [result setValueUsing: @selector(setExtra4:) ifKeyExists:@"extra4" usingDetails: details];
    [result setValueUsing: @selector(setExtra5:) ifKeyExists:@"extra5" usingDetails: details];
//    [result setValueUsing: @selector(setChainReportingGroup:) ifKeyExists:@"chain_reporting_group" usingDetails: details];
    return result;
}
@end
