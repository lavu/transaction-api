//
//  LavuModifierCategory.h
//  APNTest
//
//  Created by Theodore Schnepper on 3/21/14.
//  Copyright (c) 2014 Theodore Schnepper. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LavuBase.h"

@interface LavuModifierCategory : LavuBase
@property (retain, nonatomic) NSNumber* ID;
@property (retain, nonatomic) NSString* title;
@property (retain, nonatomic) NSNumber* menuID;

+ (id) lavuModifierCategory: (NSDictionary*) details;
@end
