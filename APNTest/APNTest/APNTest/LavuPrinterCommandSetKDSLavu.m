//
//  LavuPrinterCommandSetKDSLavu.m
//  APNTest
//
//  Created by Theodore Schnepper on 4/2/14.
//  Copyright (c) 2014 Theodore Schnepper. All rights reserved.
//

#import "LavuPrinterCommandSetKDSLavu.h"

@implementation LavuPrinterCommandSetKDSLavu

- (NSDictionary*) dictionaryFromLavuItemDecision: (LavuItemDecision*) decision {
    //             "course" = 1;
    //             "item" = "Pepperoni Pizza";
    //             "notes" = "";
    //             "options" = "Pizza\n - Hand Tossed\n - Marinara";
    //             "order_id" = "1-2";
    //             "quantity" = 1;
    //             "seat" = 1;
    //             "sent" = 0;
    //             "special" = "";
    NSMutableDictionary* dict = [NSMutableDictionary dictionary];
    dict[@"course"] = [NSNumber numberWithInt: 1];
    dict[@"item"] = [[decision item] name];
    dict[@"notes"] = @"";
    dict[@"quantity"] = [decision quantity];
    dict[@"seat"] = [NSNumber numberWithInt: 1];
    dict[@"course"] = [NSNumber numberWithInt: 1];
    
    //options
    NSMutableArray* special = [NSMutableArray array];
    
    
    [[decision addedOptionalModifiers] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL* stop){
        [special addObject: [NSString stringWithFormat:@"Plus %@", [(LavuModifier*)obj title]]];
    }];
    
    [[decision removedOptionalModifiers] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL* stop){
        [special addObject: [NSString stringWithFormat:@"No %@", [(LavuModifier*)obj title]]];
    }];
    dict[@"special"] = [special componentsJoinedByString: @", "];
    
    NSMutableArray* options = [NSMutableArray array];
    NSEnumerator* enumerator = [[decision forcedModifiersDecisions] enumerator];
    LavuForcedModifierDecision* forcedModifierDecision;
    while( forcedModifierDecision = [enumerator nextObject] ){
        [options addObject: [[forcedModifierDecision forcedModifier] title]];
    }
    
    dict[@"options"] = [options componentsJoinedByString: @"\n - "];
    //special
    return [dict copy];
}

- (NSData*) generatePrintData: (id) order usingFormatting: (LavuPrinterFormatting*) formatting {
    if( !order ){
        return nil;
    }
    if( ![order isKindOfClass: [LavuOrder class]] ){
        return nil;
    }
    
    LavuOrder* lavuOrder = order;
    NSMutableDictionary* dict = [NSMutableDictionary dictionary];
    NSLocale* usLocale = [NSLocale localeWithLocaleIdentifier:@"en_US"];
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle: NSDateFormatterFullStyle];
    [dateFormatter setTimeStyle: NSDateFormatterFullStyle];
    [dateFormatter setLocale: usLocale];
    [dateFormatter setDateFormat:@"yyyy'-'MM'-'dd' 'HH':'mm':'ss"];
    dict[@"datetime"] = [dateFormatter stringFromDate: [NSDate date]];
    dict[@"guests"] = [lavuOrder guests];
    dict[@"max_courses"] = [lavuOrder courses];
    dict[@"order_id"] = [lavuOrder orderID];
    dict[@"order_tag"] = [lavuOrder orderTag];
    dict[@"resend"] = [lavuOrder sentToKitchen] ? [NSNumber numberWithInt: 1] : [NSNumber numberWithInt: 0];
    dict[@"server_name"] = [lavuOrder serverName];
    dict[@"tablename"] = [lavuOrder tableName];
    if( [[LavuAPNManager sharedInstance] deviceIdentifier] ){
        dict[@"apn_identifier"] = [[LavuAPNManager sharedInstance] deviceIdentifier];
    }
    
    
    NSMutableArray* array = [NSMutableArray array];
    [[lavuOrder items] enumerateObjectsUsingBlock:^(id obj, BOOL* stop){
        [array addObject: [self dictionaryFromLavuItemDecision: obj]];
    }];
    
    dict[@"items"] = [array copy];
    
    
    //    {
    //        "datetime" = "2014-04-02 16:24:28";
    //        "guests" = 1;
    //        "items" =
    //		(
    //         {
    //             "course" = 1;
    //             "item" = "Pepperoni Pizza";
    //             "notes" = "";
    //             "options" = "Pizza\n - Hand Tossed\n - Marinara";
    //             "order_id" = "1-2";
    //             "quantity" = 1;
    //             "seat" = 1;
    //             "sent" = 0;
    //             "special" = "";
    //         }
    //         );
    //        "max_course" = 1;
    //        "order_id" = "1-2";
    //        "order_tag" = "Dine In";
    //        "resend" = 0;
    //        "server_name" = Apple;
    //        "tablename" = "Quick Serve";
    //    }
    
    NSString* errorDescription;
    NSData* serialized = [NSPropertyListSerialization dataFromPropertyList:[dict copy] format:NSPropertyListBinaryFormat_v1_0 errorDescription:&errorDescription];
    if( errorDescription ){
        //error
        return nil;
    }
    return serialized;
}
@end
