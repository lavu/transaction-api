//
//  LavuOptionalModifierStateChange.h
//  APNTest
//
//  Created by Theodore Schnepper on 3/31/14.
//  Copyright (c) 2014 Theodore Schnepper. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LavuModifier.h"

@protocol LavuOptionalModifierStateChangeProtocol <NSObject>
- (IBAction) modifier: (LavuModifier*) modifier enabledByDefault: (BOOL) enabled toggledTo: (BOOL) state atIndexPath: (NSIndexPath*) indexPath;
@end
