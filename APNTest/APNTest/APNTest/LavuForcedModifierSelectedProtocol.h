//
//  LavuForcedModifierOptionSelected.h
//  APNTest
//
//  Created by Theodore Schnepper on 3/31/14.
//  Copyright (c) 2014 Theodore Schnepper. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LavuForcedModifier.h"
#import "LavuForcedModifierDecision.h"

@protocol LavuForcedModifierSelectedProtocol <NSObject>
- (IBAction) selectedForcedModifier: (LavuForcedModifier*) forcedModifier forDecision: (LavuForcedModifierDecision*) decision;
@end
