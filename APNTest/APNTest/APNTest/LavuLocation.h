//
//  LavuLocation.h
//  APNTest
//
//  Created by Theodore Schnepper on 3/21/14.
//  Copyright (c) 2014 Theodore Schnepper. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LavuBase.h"
#import "LavuMenu.h"
#import "LavuMonetaryFormatter.h"

@interface LavuLocation : LavuBase
@property (retain, nonatomic) NSNumber* ID;
@property (retain, nonatomic) NSNumber* restaurantID;

@property (retain, nonatomic) NSNumber* menuID;

@property (retain, nonatomic) NSString* title;
@property (retain, nonatomic) NSString* address;
@property (retain, nonatomic) NSString* city;
@property (retain, nonatomic) NSString* state;
@property (retain, nonatomic) NSString* country;
@property (retain, nonatomic) NSNumber* zip;

@property (retain, nonatomic) NSString* phone;
@property (retain, nonatomic) NSString* website;
@property (retain, nonatomic) NSString* email;
@property (retain, nonatomic) NSString* manager;

@property (retain, nonatomic) NSString* netPath;
@property (retain, nonatomic) NSString* netPathPrint;
@property BOOL useNetPath;

@property (retain, nonatomic) NSString* timezone;

@property (retain, nonatomic) NSString* monetarySymbol;
@property LavuMonetaryPosition leftOrRight;
@property (retain, nonatomic) NSString* decimalCharacter;
@property (retain, nonatomic) NSString* thousandsCharacter;
@property (retain, nonatomic) NSNumber* decimalPlaces;
@property BOOL taxInclusion;

@property (retain, nonatomic) NSString* roundingFactor;
@property (retain, nonatomic) NSString* roundUpOrDown;

@property BOOL useDirectPrinting;

@property (retain, nonatomic) LavuMonetaryFormatter* formatter;
@property (retain, nonatomic) NSArray* printers;

+ (NSArray*) lavuLocations;
+ (void) setLavuLocations: (NSArray*) lavuLocations;
//+ (id) lavuLocationFromID: (NSNumber*) locationID;
+ (id) lavuLocation: (NSDictionary*) details;

- (void) setLeftOrRightWithString:(NSString*)leftOrRight;

- (NSArray*) lavuMenus;
- (NSArray*) lavuIngredients;

- (id) lavuMenuFromID: (NSNumber*) menuID;
- (id) lavuIngredientFromID: (NSNumber*) ingredientID;
- (id) printerFromIdentifier: (NSString*) identifier;
@end
