//
//  LavuModifier.h
//  APNTest
//
//  Created by Theodore Schnepper on 3/21/14.
//  Copyright (c) 2014 Theodore Schnepper. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LavuBase.h"
#import "LavuAmount.h"

@interface LavuModifier : LavuBase
@property (retain, nonatomic) NSNumber* ID;
@property (retain, nonatomic) NSString* title;
@property (retain, nonatomic) NSNumber* lavuModifierCategoryID;
@property (retain, nonatomic) LavuAmount* cost;
@property (retain, nonatomic) NSDictionary* ingredients; //LavuIngredient -> Ingredient Quantity
- (void) setCostFromNumber:(NSNumber *)cost;
+ (id) lavuModifier: (NSDictionary*) details;
@end
