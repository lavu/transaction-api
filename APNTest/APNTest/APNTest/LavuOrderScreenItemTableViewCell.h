//
//  LavuOrderScreenItemTableViewCell.h
//  APNTest
//
//  Created by Theodore Schnepper on 4/3/14.
//  Copyright (c) 2014 Theodore Schnepper. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LavuItemDecision.h"
#import "LavuLocation.h"
#import "LavuItemModificationProtocol.h"
#import "LavuPrinterBase.h"
#import <CoreText/CoreText.h>

@interface LavuOrderScreenItemTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *quantityLabel;
@property (weak, nonatomic) IBOutlet UILabel *ItemNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *itemSubtotalLabel;
@property (weak, nonatomic) IBOutlet UILabel *modifierDetailsLabel;
@property (weak, nonatomic) id<LavuItemModificationProtocol> delegate;
@property (retain, nonatomic) UISwipeGestureRecognizer* leftSwipeRecognizer;
@property (retain, nonatomic) UISwipeGestureRecognizer* rightSwipeRecognizer;

@property (retain, nonatomic) LavuItemDecision* itemDecision;

- (void) setItemDecision: (LavuItemDecision*) decision withLocation: (LavuLocation*) location;
+ (CGRect) calculateHeightForDecision: (LavuItemDecision*) decision withLocation: (LavuLocation*) location withWidth: (CGFloat) width ;

- (IBAction) receivedLeftSwipeGesture: (UIGestureRecognizer *)gestureRecognizer;
- (IBAction) receivedRightSwipeGesture: (UIGestureRecognizer *)gestureRecognizer;
@end
