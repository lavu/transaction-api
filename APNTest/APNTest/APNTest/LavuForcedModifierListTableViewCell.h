//
//  LavuForcedModifierListTableViewCell.h
//  APNTest
//
//  Created by Theodore Schnepper on 3/27/14.
//  Copyright (c) 2014 Theodore Schnepper. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LavuForcedModifier.h"
#import "LavuForcedModifierSelectedProtocol.h"
#import "LavuForcedModifierDecision.h"

@interface LavuForcedModifierListTableViewCell : UITableViewCell <UIPickerViewDataSource>
@property (retain, nonatomic) NSArray* forcedModifiers;
@property (retain, nonatomic) LavuForcedModifier* currentValue;
@property (weak, nonatomic) id<LavuForcedModifierSelectedProtocol> proto;
@property (retain, nonatomic) LavuForcedModifierDecision* forcedModifierDecision;
@property (weak, nonatomic) IBOutlet UILabel *forcedModifierTitle;

- (BOOL) addForcedModifier: (LavuForcedModifier*) forcedModifier;
- (BOOL) removeForcedModifier: (LavuForcedModifier*) forcedModifier;
@end
