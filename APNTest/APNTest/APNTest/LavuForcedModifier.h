//
//  LavuForcedModifier.h
//  APNTest
//
//  Created by Theodore Schnepper on 3/21/14.
//  Copyright (c) 2014 Theodore Schnepper. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LavuBase.h"
#import "LavuAmount.h"

@interface LavuForcedModifier : LavuBase
@property (retain, nonatomic) NSNumber* ID;
@property (retain, nonatomic) NSString* title;
@property (retain, nonatomic) NSNumber* listID;
@property (retain, nonatomic) LavuAmount* cost;
@property (retain, nonatomic) NSNumber* forcedModifierGroupDetour;
@property (retain, nonatomic) NSNumber* forcedModifierListDetour;
@property (retain, nonatomic) NSDictionary* ingredients; // ingredientID -> ingredient Quantity
@property (retain, nonatomic) NSString* extra;
@property (retain, nonatomic) NSString* extra2;
@property (retain, nonatomic) NSString* extra3;
@property (retain, nonatomic) NSString* extra4;
@property (retain, nonatomic) NSString* extra5;

- (void) setCostFromNumber:(NSNumber *)cost;
- (void) setForcedModifierGroupOrListIDDetour: (NSString*) idDetails;
+ (id) lavuForcedModifier: (NSDictionary*) details;
@end
