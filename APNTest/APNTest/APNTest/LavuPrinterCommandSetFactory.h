//
//  LavuPrinterCommandSetFactory.h
//  APNTest
//
//  Created by Theodore Schnepper on 4/7/14.
//  Copyright (c) 2014 Theodore Schnepper. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LavuPrinterCommandSet.h"
#import "LavuPrinterCommandSetKDSLavu.h"

@interface LavuPrinterCommandSetFactory : NSObject
+ (LavuPrinterCommandSet*) createPrinterCommandSetWithInteger: (NSInteger) commandSetIdentifier;
@end
