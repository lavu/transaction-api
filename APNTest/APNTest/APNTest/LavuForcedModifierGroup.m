//
//  LavuForcedModifierGroup.m
//  APNTest
//
//  Created by Theodore Schnepper on 3/21/14.
//  Copyright (c) 2014 Theodore Schnepper. All rights reserved.
//

#import "LavuForcedModifierGroup.h"

@implementation LavuForcedModifierGroup
@synthesize ID = _ID;
@synthesize title = _title;
@synthesize menuID = _menuID;
@synthesize forcedModifierLists = _forcedModifierLists;

- (id) init {
    self = [super init];
    if( self ){
        _forcedModifierLists = [[NSArray alloc] init];
    }
    return self;
}

+ (id) lavuForcedModifierGroup: (NSDictionary*) details {
    LavuForcedModifierGroup* result = [[LavuForcedModifierGroup alloc] init];
    if(! [result setValueUsing: @selector(setID:) ifKeyExists:@"id" usingDetails: details]){
        return nil;
    }
    [result setValueUsing: @selector(setMenuID:) ifKeyExists:@"menu_id" usingDetails: details];
    [result setValueUsing: @selector(setTitle:) ifKeyExists:@"title" usingDetails: details];
    
    if( ![[NSNull null] isEqual: details[@"include_lists"]] ){
        NSString* include_lists = details[@"include_lists"];
        NSArray* include_id_strs = [include_lists componentsSeparatedByString: @"|"];
        NSMutableArray* temp = [[result forcedModifierLists] mutableCopy];
        for( int i = 0; i < include_id_strs.count; i++ ){
            NSNumber* include_id = [NSNumber numberWithInt: [include_id_strs[i] intValue]];
            if( ![temp containsObject: include_id ] ){
                [temp addObject: include_id];
            }
        }
        [result setForcedModifierLists: temp];
    }
    
//    [result setValueUsing: @selector(setDeleted:) ifKeyExists:@"_deleted" usingDetails: details];
//    [result setValueUsing: @selector(setChainReportingGroup:) ifKeyExists:@"chain_reporting_group" usingDetails: details];
    return result;
}
@end
