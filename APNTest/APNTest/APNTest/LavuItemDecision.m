//
//  LavuItemDecision.m
//  APNTest
//
//  Created by Theodore Schnepper on 3/28/14.
//  Copyright (c) 2014 Theodore Schnepper. All rights reserved.
//

#import "LavuItemDecision.h"

@implementation LavuItemDecision
@synthesize item = _item;
@synthesize forcedModifiersDecisions = _forcedModifiersDecisions;
@synthesize addedOptionalModifiers = _addedOptionalModifiers;
@synthesize removedOptionalModifiers = _removedOptionalModifiers;
@synthesize quantity = _quantity;

- (id) init {
    self = [super init];
    if( self ){
        _forcedModifiersDecisions = [[LavuForcedModifierDecisionTreeRoot alloc] init];
        _addedOptionalModifiers = [NSArray array];
        _removedOptionalModifiers = [NSArray array];
    }
    
    return self;
}

- (NSArray*) addItemToArray: (NSArray*) array object: (NSObject*) object {
    if( [array containsObject: object] ){
        return array;
    }
    
    NSMutableArray* temp = [array mutableCopy];
    [temp addObject: object];
    return [temp copy];
}

- (NSArray*) removeItemFromArray: (NSArray*) array object: (NSObject*) object {
    if( ![array containsObject: object] ){
        return array;
    }
    
    NSMutableArray* temp = [array mutableCopy];
    [temp removeObject: object];
    return [temp copy];
}

- (void) addAddedOptionalModifier: (LavuModifier*) modifier {
    _addedOptionalModifiers = [self addItemToArray: _addedOptionalModifiers  object:modifier];
}

- (void) removeAddedOptionalModifier: (LavuModifier*) modifier {
    _addedOptionalModifiers = [self removeItemFromArray: _addedOptionalModifiers object: modifier];
}
- (void) addRemovedOptionalModifier: (LavuModifier*) modifier {
    _removedOptionalModifiers = [self addItemToArray: _removedOptionalModifiers  object:modifier];
}
- (void) removeRemovedOptionalModifier: (LavuModifier*) modifier {
    _removedOptionalModifiers = [self removeItemFromArray: _removedOptionalModifiers  object:modifier];
}

- (LavuAmount*) calculateSubtotal {
    LavuAmount* __block result = [[LavuAmount alloc] init];
    result = [result add: [_item price]];
//    NSEnumerator* test;
    [_addedOptionalModifiers enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL* stop){
        result = [result add: [obj cost]];
    }];
    
//    [_removedOptionalModifiers enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL* stop){
//        result = [result subtract: [obj cost]];
//    }];
    
    NSEnumerator* enumerator = [_forcedModifiersDecisions enumerator];
    LavuForcedModifierDecision* decision;
    while( decision = [enumerator nextObject] ){
        if( [decision forcedModifier] ){
            result = [result add: [[decision forcedModifier] cost]];
        }
    }
    return [result multiply: [[LavuAmount alloc] initWithInteger: [_quantity integerValue]]];
}
@end
