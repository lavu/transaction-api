//
//  LavuMenuItem.m
//  APNTest
//
//  Created by Theodore Schnepper on 3/21/14.
//  Copyright (c) 2014 Theodore Schnepper. All rights reserved.
//

#import "LavuMenuItem.h"

@implementation LavuMenuItem
@synthesize ID                      = _ID;
@synthesize menuCategoryID          = _menuCategoryID;
@synthesize menuID                  = _menuID;
@synthesize name                    = _name;
@synthesize price                   = _price;
@synthesize description             = _description;
@synthesize imagePath               = _imagePath;
@synthesize imagePath2              = _imagePath2;
@synthesize imagePath3              = _imagePath3;
@synthesize option1                 = _option1;
@synthesize option2                 = _option2;
@synthesize option3                 = _option3;
@synthesize active                  = _active;
@synthesize order                   = _order;
@synthesize print                   = _print;
@synthesize quickItem               = _quickItem;
@synthesize lastModified            = _lastModified;
@synthesize printer                 = _printer;
@synthesize applyTaxRate            = _applyTaxRate;
@synthesize customTaxRate           = _customTaxRate;
@synthesize modifierListID          = _modifierListID;
@synthesize forcedModifierGroupID   = _forcedModifierGroupID;
@synthesize forcedModifierListID    = _forcedModifierListID;
@synthesize misc                    = _misc;
@synthesize ingredients             = _ingredients;
@synthesize openItem                = _openItem;
@synthesize hiddenValue             = _hiddenValue;
@synthesize hiddenValue2            = _hiddenValue2;
@synthesize hiddenValue3            = _hiddenValue3;
@synthesize allowDeposit            = _allowDeposit;
@synthesize UPC                     = _UPC;
@synthesize inventoryCount          = _inventoryCount;
@synthesize showInApp               = _showInApp;
@synthesize superGroupID            = _superGroupID;
@synthesize taxInclusion            = _taxInclusion;
@synthesize lavutogoDisplay         = _lavutogoDisplay;
@synthesize happyHour               = _happyHour;
@synthesize taxProfileID            = _taxProfileID;
@synthesize chainReportingGroup     = _chainReportingGroup;
@synthesize track86Count            = _track86Count;
@synthesize priceTierProfileID      = _priceTierProfileID;

- (id) init {
    self = [super init];
    if( self ){
        _ingredients = [[NSDictionary alloc] init];
    }
    return self;
}

- (void) setPriceFromNumber:(NSNumber *)price {
    _price = [[LavuAmount alloc] initWithDouble: [price doubleValue]];
}

- (void) setForcedModifierGroupOrListID: (NSString*) idDetails {
    if( [idDetails characterAtIndex: 0] == 'c' || [idDetails characterAtIndex: 0] == 'C' || [idDetails isEqualToString: @""] ){
        [self setForcedModifierListID: [NSNumber numberWithInteger: 0]];
        [self setForcedModifierGroupID: [NSNumber numberWithInteger: 0]];
    } else if( [idDetails characterAtIndex: 0] == 'l' ){
        //list
        [self setForcedModifierListID: [NSNumber numberWithInteger: [[idDetails substringFromIndex: 1] integerValue]]];
        [self setForcedModifierGroupID: [NSNumber numberWithInteger: 0]];
    } else {
        //group
        if( [idDetails characterAtIndex: 0] >= '0' && [idDetails characterAtIndex: 0] <= '9' ) {
            //Just a Number
            [self setForcedModifierGroupID: [NSNumber numberWithInteger: [idDetails integerValue]]];
        } else {
            [self setForcedModifierGroupID: [NSNumber numberWithInteger: [[idDetails substringFromIndex: 1] integerValue]]];
        }
        [self setForcedModifierListID: [NSNumber numberWithInteger: 0]];
    }
}

+ (id) lavuMenuItem: (NSDictionary*) details {
    LavuMenuItem* result = [[LavuMenuItem alloc] init];
    if(! [result setValueUsing: @selector(setID:) ifKeyExists:@"id" usingDetails: details]){
        return nil;
    }
    [result setValueUsing: @selector(setMenuCategoryID:) ifKeyExists:@"category_id" usingDetails: details];
    [result setValueUsing: @selector(setMenuID:) ifKeyExists:@"menu_id" usingDetails: details];
    [result setValueUsing: @selector(setName:) ifKeyExists:@"name" usingDetails: details];
    [result setValueUsing: @selector(setPriceFromNumber:) ifKeyExists:@"price" usingDetails: details];
    [result setValueUsing: @selector(setDescription:) ifKeyExists:@"description" usingDetails: details];
    [result setValueUsing: @selector(setImagePath:) ifKeyExists:@"image" usingDetails: details];
    [result setValueUsing: @selector(setImagePath2:) ifKeyExists:@"image2" usingDetails: details];
    [result setValueUsing: @selector(setImagePath3:) ifKeyExists:@"image3" usingDetails: details];
    [result setValueUsing: @selector(setOption1:) ifKeyExists:@"options1" usingDetails: details];
    [result setValueUsing: @selector(setOption3:) ifKeyExists:@"options2" usingDetails: details];
    [result setValueUsing: @selector(setOption1:) ifKeyExists:@"options3" usingDetails: details];
    [result setValueUsing: @selector(setActive:) ifKeyExists:@"active" usingDetails: details];
    [result setValueUsing: @selector(setOrder:) ifKeyExists:@"_order" usingDetails: details];
    [result setValueUsing: @selector(setPrint:) ifKeyExists:@"print" usingDetails: details];
    [result setValueUsing: @selector(setQuickItem:) ifKeyExists:@" quick_item" usingDetails: details];
//    [result setValueUsing: @selector(setLastModified:) ifKeyExists:@"last_modified" usingDetails: details];
    if( ![[NSNull null] isEqual: details[@"last_modified_date"]] ){
        NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"%Y-%m-%d %H:%M:%S"];
        [result setLastModified: [formatter dateFromString: details[@"last_modified_date"]]];
    }
    [result setValueUsing: @selector(setPrinter:) ifKeyExists:@"printer" usingDetails: details];
    [result setValueUsing: @selector(setApplyTaxRate:) ifKeyExists:@"apply_taxrate" usingDetails: details];
    [result setValueUsing: @selector(setCustomTaxRate:) ifKeyExists:@"custom_taxrate" usingDetails: details];
    [result setValueUsing: @selector(setModifierListID:) ifKeyExists:@"modifier_list_id" usingDetails: details];
    [result setValueUsing: @selector(setForcedModifierGroupOrListID:) ifKeyExists:@"forced_modifier_group_id" usingDetails: details];
    [result setValueUsing: @selector(setMisc:) ifKeyExists:@"misc_content" usingDetails: details];
//    [result setValueUsing: @selector(setIngredients:) ifKeyExists:@"ingredients" usingDetails: details];
    if( ![[NSNull null] isEqual: details[@"ingredients"]] && ![details[@"ingredients"] isEqual: @""] ){
        // id x qty, ...
        NSMutableDictionary* dict = [[result ingredients] mutableCopy];
        NSArray* ingredient_parts = [details[@"ingredients"] componentsSeparatedByString: @","];
        for( int i = 0; i < ingredient_parts.count; i++ ){
            NSString* ingredient_details = ingredient_parts[i];
            NSArray* ingredients = [ingredient_details componentsSeparatedByString: @"x"];
            if( ingredients.count < 2 ){
                continue;
            }
            dict[[NSNumber numberWithInt: [ingredients[0] intValue]]] = [NSNumber numberWithInt: [ingredients[1] intValue]];
        }
        [result setIngredients: dict];
    }
    [result setValueUsing: @selector(setOpenItem:) ifKeyExists:@"open_item" usingDetails: details];
    [result setValueUsing: @selector(setHiddenValue:) ifKeyExists:@"hidden_value" usingDetails: details];
    [result setValueUsing: @selector(setHiddenValue2:) ifKeyExists:@"hidden_value2" usingDetails: details];
    [result setValueUsing: @selector(setHiddenValue3:) ifKeyExists:@"hidden_value3" usingDetails: details];
    [result setValueUsing: @selector(setAllowDeposit:) ifKeyExists:@"allow_deposit" usingDetails: details];
    [result setValueUsing: @selector(setUPC:) ifKeyExists:@"UPC" usingDetails: details];
    [result setValueUsing: @selector(setInventoryCount:) ifKeyExists:@"inv_count" usingDetails: details];
    [result setValueUsing: @selector(setShowInApp:) ifKeyExists:@"how_in_app" usingDetails: details];
    [result setValueUsing: @selector(setSuperGroupID:) ifKeyExists:@"super_group_id" usingDetails: details];
    [result setValueUsing: @selector(setTaxInclusion:) ifKeyExists:@"tax_inclusion" usingDetails: details];
    [result setValueUsing: @selector(setLavutogoDisplay:) ifKeyExists:@"ltg_display" usingDetails: details];
    [result setValueUsing: @selector(setHappyHour:) ifKeyExists:@"happy_hour" usingDetails: details];
    [result setValueUsing: @selector(setTaxProfileID:) ifKeyExists:@"tax_profile_id" usingDetails: details];
    [result setValueUsing: @selector(setChainReportingGroup:) ifKeyExists:@"chain_reporting_group" usingDetails: details];
    [result setValueUsing: @selector(setTrack86Count:) ifKeyExists:@"track_86_count" usingDetails: details];
    [result setValueUsing: @selector(setPriceTierProfileID:) ifKeyExists:@"price_tier_profile_id" usingDetails: details];
    return result;
}
@end
