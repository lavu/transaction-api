//
//  LavuOptionalModifierTableViewCell.m
//  APNTest
//
//  Created by Theodore Schnepper on 3/31/14.
//  Copyright (c) 2014 Theodore Schnepper. All rights reserved.
//

#import "LavuOptionalModifierTableViewCell.h"

@implementation LavuOptionalModifierTableViewCell
@synthesize optionLabel = _optionLabel;
@synthesize optionToggle = _optionToggle;
@synthesize modifier = _modifier;
@synthesize enabledByDefault = _enabledByDefault;
@synthesize proto = _proto;
@synthesize pathToObject = _pathToObject;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)toggled:(id)sender {
    
    if( _proto ){
        
        [_proto modifier: _modifier enabledByDefault: _enabledByDefault toggledTo: [_optionToggle isOn] atIndexPath: _pathToObject];
    }
}
@end
