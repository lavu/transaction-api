//
//  LavuMenuTableViewController.h
//  APNTest
//
//  Created by Theodore Schnepper on 3/20/14.
//  Copyright (c) 2014 Theodore Schnepper. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LavuOptionsPickerTableViewController.h"
#import "LavuLocation.h"
#import "LavuAmount.h"

#define LAVU_MENU_TO_INGREDIENTS_SEGUE "show_ingredients"

#define LAVU_MENU_NAVIGATION_MENU_ITEM_CELL_IDENTIFIER "menu_item"
#define LAVU_MENU_NAVIGATION_MENU_ITEM_IMAGE_TAG 1
#define LAVU_MENU_NAVIGATION_MENU_ITEM_LABEL_TAG 2
#define LAVU_MENU_NAVIGATION_MENU_PRICE_LABEL_TAG 3
#define LAVU_MENU_NAVIGATION_MENU_INGREDIENT_LABEL_TAG 4

@interface LavuMenuTableViewController : UITableViewController
@property (strong, nonatomic, setter=setMajorMinor:) NSIndexPath* majorMinor;
@property (strong, nonatomic) LavuLocation* currentLocation;
@property (strong, nonatomic) LavuMenu* currentMenu;

@property (strong, nonatomic) LavuMenuItem* selectedItem;
- (void) setMajorMinor:(NSIndexPath *)majorMinor;
@end
