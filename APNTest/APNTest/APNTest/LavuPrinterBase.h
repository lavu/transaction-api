//
//  LavuPrinterBase.h
//  APNTest
//
//  Created by Theodore Schnepper on 4/2/14.
//  Copyright (c) 2014 Theodore Schnepper. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LavuBase.h"
#import "LavuPrinterCommandSetFactory.h"
#import "LavuPrinterFormatting.h"

@interface LavuPrinterBase : LavuBase
@property (retain, nonatomic) NSString* printerID;
@property (retain, nonatomic) NSString* name;
@property (retain, nonatomic) NSURL* address;
@property (retain, nonatomic) NSString* ipAddress;
@property (retain, nonatomic) NSNumber* port;

@property BOOL skipPing;
@property BOOL eTBFlag;
@property NSTimeInterval writePauseDuration;

@property (retain, nonatomic) LavuPrinterCommandSet* lavuPrinterCommandSet;
@property (retain, nonatomic) LavuPrinterFormatting* lavuPrinterFormatting;

//@property (retain, nonatomic) NSNumber* commandSet; // This will be the extension abstraction
//@property (retain, nonatomic) NSString* imageCapabilities; // Another need for abstraction, probably a pass-off...
//@property BOOL statusCheck; Further Down the Abstraction layer (only applies to STAR printers)

//Hand off Data Generation Based on CommandSet and ImageCapabilities information
//Hand off statusCheck?

+ (instancetype) lavuPrinterBaseWithDetails: (NSDictionary*) dict;

- (void) setLavuPrinterCommandSetWithInteger: (NSNumber*) value;
- (void) setLavuPrinterFormattingUsingDetails: (NSDictionary*) dict;

- (NSData*) generatePrintData: (id) order;
@end
