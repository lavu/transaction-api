//
//  LavuAPNManager.m
//  APNTest
//
//  Created by Theodore Schnepper on 4/3/14.
//  Copyright (c) 2014 Theodore Schnepper. All rights reserved.
//

#import "LavuAPNManager.h"

@implementation LavuAPNManager
@synthesize deviceIdentifier = _deviceIdentifier;
@synthesize groupIdentifier = _groupIdentifier;
@synthesize apnIdentifier = _apnIdentifier;

static LavuAPNManager* _LavuAPNManager = nil;

+ (instancetype) sharedInstance {
    if( !_LavuAPNManager ){
        _LavuAPNManager = [[LavuAPNManager alloc] init];
    }
    return _LavuAPNManager;
}

- (void) registerForAppAPN {
    self.comm = [[Communicator alloc] init];
    self.comm->host = @"tcp://apn.lavu";
    self.comm->port = 12345;
    [self.comm setup];
    
    NSData* nameData = [_deviceIdentifier dataUsingEncoding: NSUTF8StringEncoding];
    NSData* groupnameData = [_groupIdentifier dataUsingEncoding: NSUTF8StringEncoding];
    
    const char nameLength[] = { [nameData length] };
    const char groupnameLength[] = { [groupnameData length] };
    
    NSMutableData *allData = [[NSMutableData alloc] init];
    [allData appendBytes: nameLength length:sizeof(nameLength)];
    [allData appendBytes: groupnameLength length:sizeof(groupnameLength)];
    [allData appendData: nameData];
    [allData appendData: groupnameData];
    [allData appendData: _apnIdentifier];
    
    [self.comm writeOut: allData closeAfterwards: true];
//    [self.comm close];
}
@end
