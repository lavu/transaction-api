//
//  LavuPrinterCommandSet.h
//  APNTest
//
//  Created by Theodore Schnepper on 4/2/14.
//  Copyright (c) 2014 Theodore Schnepper. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LavuPrinterFormatting.h"
#define LAVU_PRINTER_COMMAND_SET_STAR 01
#define LAVU_PRINTER_COMMAND_SET_EPSON 02
#define LAVU_PRINTER_COMMAND_SET_PARTNER 03
#define LAVU_PRINTER_COMMAND_SET_IMPACT_EPSON 04
#define LAVU_PRINTER_COMMAND_SET_BTP_M280 05
#define LAVU_PRINTER_COMMAND_SET_BIXOLON 06
#define LAVU_PRINTER_COMMAND_SET_IMPACT_EPSON_BASIC 07
#define LAVU_PRINTER_COMMAND_SET_EPSON_BASIC 08
#define LAVU_PRINTER_COMMAND_SET_BIXOLON_BASIC 09
#define LAVU_PRINTER_COMMAND_SET_STAR_2 10
#define LAVU_PRINTER_COMMAND_SET_BIXOLON_NO_DRAWER 11
#define LAVU_PRINTER_COMMAND_SET_LAVU_KDS 12
#define LAVU_PRINTER_COMMAND_SET_STAR_IMPACT 13
#define LAVU_PRINTER_COMMAND_SET_STAR_BASIC 14
#define LAVU_PRINTER_COMMAND_SET_TSP650 15
#define LAVU_PRINTER_COMMAND_SET_SP700 16
#define LAVU_PRINTER_COMMAND_SET_EPSON_2 17
#define LAVU_PRINTER_COMMAND_SET_EPSON_TM_T20 18
#define LAVU_PRINTER_COMMAND_SET_LAVU_KDS_PRO 19

@interface LavuPrinterCommandSet : NSObject
- (NSInteger) commandSetIdentifier;
- (NSString*) commandSetName;

- (NSData*) generatePrintData: (id) order usingFormatting: (LavuPrinterFormatting*) formatting;

@end
