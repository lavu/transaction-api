//
//  LavuOrder.m
//  APNTest
//
//  Created by Theodore Schnepper on 4/2/14.
//  Copyright (c) 2014 Theodore Schnepper. All rights reserved.
//

#import "LavuOrder.h"

@implementation LavuOrder
static LavuOrder* currentOrder;
@synthesize orderID = _orderID;
@synthesize ioid = _ioid;
@synthesize guests = _guests;
@synthesize courses = _courses;
@synthesize checks = _checks;
@synthesize opened = _opened;
@synthesize closed = _closed;
@synthesize orderTag = _orderTag;
@synthesize serverName = _serverName;
@synthesize tableName = _tableName;
@synthesize sentToKitchen = _sentToKitchen;
@synthesize location = _location;

@synthesize items = _items;

- (id) init {
    self = [super init];
    if( self ){
        _opened = [NSDate date];
        _sentToKitchen = false;
        _guests = [NSNumber numberWithInt: 1];
        _courses = [NSNumber numberWithInt: 1];
        _checks = [NSNumber numberWithInt: 1];
        _ioid = @"1234"; //Extremely random number
        _orderID = @"4-1"; //Equally random order-id
        _orderTag = @"Pick-Up";
        _tableName = @"QuickServe";
        _serverName = @"Self-Serve";
        _items = [NSSet set];
    }
    return self;
}

- (void) addItem: (LavuItemDecision*) item {
    if( !item ){
        return;
    }
    if( [[item quantity] isEqualToNumber: [NSNumber numberWithInt: 0]] ){
        [self removeItem: item];
        return;
    } else {
        NSMutableSet* set = [_items mutableCopy];
        [set addObject: item];
        _items = [set copy];
    }
}
- (void) removeItem: (LavuItemDecision*) item {
    if( !item ){
        return;
    }
    
    NSMutableSet* set = [_items mutableCopy];
    [set removeObject: item];
    _items = [set copy];
}
+ (id) currentOrder {
    if( !currentOrder ){
        currentOrder = [[LavuOrder alloc] init];
    }
    return currentOrder;
}

- (LavuAmount*) calculateSubtotal {
    LavuAmount* __block result = [[LavuAmount alloc] initWithInteger:0];
    [_items enumerateObjectsUsingBlock:^(id obj, BOOL* stop){
        result = [result add: [obj calculateSubtotal]];
    }];
    return result;
}

- (LavuAmount*) calculateTax {
    return [[LavuAmount alloc] initWithInteger: 0];
}
- (LavuAmount*) calculateTotal {
    return [[self calculateSubtotal] add: [self calculateTax]];
}

@end
