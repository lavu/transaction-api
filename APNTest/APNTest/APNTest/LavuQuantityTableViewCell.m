//
//  LavuQuantityTableViewCell.m
//  APNTest
//
//  Created by Theodore Schnepper on 3/31/14.
//  Copyright (c) 2014 Theodore Schnepper. All rights reserved.
//

#import "LavuQuantityTableViewCell.h"

@implementation LavuQuantityTableViewCell
@synthesize titleLabel = _titleLabel;
@synthesize amountLabel = _amountLabel;
@synthesize stepper = _stepper;
@synthesize proto = _proto;


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
//    [super setSelected:se bnmlected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)amountChange:(id)sender forEvent:(UIEvent *)event {
    if(_proto){
        [_proto quantityChangedTo:[NSNumber numberWithDouble: [_stepper value]]];
    }
    
    [_amountLabel setText: [NSString stringWithFormat: @"%i", (int) [_stepper value]]];
}
@end
