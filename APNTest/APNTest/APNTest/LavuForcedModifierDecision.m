//
//  LavuForcedModifierDecision.m
//  APNTest
//
//  Created by Theodore Schnepper on 3/28/14.
//  Copyright (c) 2014 Theodore Schnepper. All rights reserved.
//

#import "LavuForcedModifierDecision.h"

@implementation LavuForcedModifierDecision
@synthesize currentMenu = _currentMenu;
@synthesize forcedModifier = _forcedModifier;
@synthesize forcedModifierList = _forcedModifierList;

- (id) initWithMenu: (LavuMenu*) currentMenu ForcedModifierList: (LavuForcedModifierList*) list {
    self = [super init];
    if( self ){
        _currentMenu = currentMenu;
        _forcedModifierList = list;
    }
    return self;
}

- (id) initWithMenu: (LavuMenu*) currentMenu ForcedModifierList: (LavuForcedModifierList*) list AndForcedModifier: (LavuForcedModifier*) forcedModifier {
    self = [self initWithMenu: currentMenu ForcedModifierList: list];
    if( self ){
        [self setForcedModifier: forcedModifier];
    }
    return self;
}

- (void) setForcedModifier:(LavuForcedModifier *)forcedModifier {
    [self setChildren: nil]; //Clear the Previous Fork
    if( forcedModifier ){
        _forcedModifier = forcedModifier;
        if( [[forcedModifier forcedModifierGroupDetour] integerValue] ){
            LavuForcedModifierGroup* group = [_currentMenu lavuForcedModifierGroupFromID:[forcedModifier forcedModifierGroupDetour]];
            NSMutableArray* decisions = [NSMutableArray array];
            [[group forcedModifierLists] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop){
                LavuForcedModifierDecision* decision = [[LavuForcedModifierDecision alloc] initWithMenu: _currentMenu ForcedModifierList:[_currentMenu lavuForcedModifierListFromID: obj]];
                [decision setParent: self];
                [decisions addObject:decision];
            }];
            [self setChildren: [decisions copy]];
        } else if ( [[forcedModifier forcedModifierListDetour] integerValue] ) {
            NSMutableArray* decisions = [NSMutableArray array];
            LavuForcedModifierDecision* decision = [[LavuForcedModifierDecision alloc] initWithMenu: _currentMenu ForcedModifierList:[_currentMenu lavuForcedModifierListFromID: [forcedModifier forcedModifierListDetour]]];
            [decisions addObject:decision];
            [decision setParent: self];
            [self setChildren: [decisions copy]];
        }
    }
}

- (BOOL) hasDecision {
    return _forcedModifier != nil;
}
@end