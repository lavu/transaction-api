//
//  LavuMenu.h
//  APNTest
//
//  Created by Theodore Schnepper on 3/21/14.
//  Copyright (c) 2014 Theodore Schnepper. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LavuBase.h"
#import "LavuMenuGroup.h"
#import "LavuMenuCategory.h"
#import "LavuMenuItem.h"
#import "LavuModifierCategory.h"
#import "LavuModifier.h"
#import "LavuForcedModifierGroup.h"
#import "LavuForcedModifierList.h"
#import "LavuForcedModifier.h"
#import "LavuIngredient.h"

@interface LavuMenu : LavuBase
@property (retain, nonatomic) NSNumber* ID;

+ (id) lavuMenu: (NSDictionary*) details;

- (NSArray*) items;
- (NSArray*) categories;
- (NSArray*) lavuMenuGroups;
- (NSArray*) lavuForcedModifiers;
- (NSArray*) lavuForcedModifierLists;
- (NSArray*) lavuForcedModifierGroups;
- (NSArray*) lavuModifiers;
- (NSArray*) lavuModifierCategories;
- (NSArray*) lavuIngredients;

- (id) lavuMenuItemFromID: (NSNumber*) menuItemID;
- (id) lavuMenuCategoryFromID: (NSNumber*) menuCategoryID;
- (id) lavuMenuGroupFromID: (NSNumber*) menuGroupID;
- (id) lavuForcedModifierFromID: (NSNumber*) lavuForcedModifierID;
- (id) lavuForcedModifierListFromID: (NSNumber*) lavuForcedModifierListID;
- (id) lavuForcedModifierGroupFromID: (NSNumber*) lavuForcedModifierGroupID;
- (id) lavuModifierFromID: (NSNumber*) lavuModifierID;
- (id) lavuModifierCategoryFromID: (NSNumber*) lavuModifierCategoryID;
- (id) lavuIngredientsFromID: (NSNumber*) lavuIngredientID;

- (void) loadAllDetails: (NSDictionary*) details;
@end
