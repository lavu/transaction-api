#import "Communicator.h"
@implementation Communicator

- (void)setup {
	NSURL *url = [NSURL URLWithString:host];
	
	NSLog(@"Setting up connection to %@ : %i", [url absoluteString], port);
	
	CFStreamCreatePairWithSocketToHost(kCFAllocatorDefault, (__bridge CFStringRef)[url host], port, &readStream, &writeStream);
	
	if(!CFWriteStreamOpen(writeStream)) {
		NSLog(@"Error, writeStream not open");
		
		return;
	}
	[self open];
	
	NSLog(@"Status of outputStream: %lu", (long)[outputStream streamStatus]);
    NSLog(@"Status of inputStream: %lu", (long)[inputStream streamStatus]);
	
	return;
}

- (void)open {
	NSLog(@"Opening streams.");
	
	inputStream = (__bridge NSInputStream *)readStream;
	outputStream = (__bridge NSOutputStream *)writeStream;
	
//	[inputStream retain];
//	[outputStream retain];
	
	[inputStream setDelegate:self];
	[outputStream setDelegate:self];
	
	[inputStream scheduleInRunLoop:[NSRunLoop mainRunLoop] forMode:NSDefaultRunLoopMode];
	[outputStream scheduleInRunLoop:[NSRunLoop mainRunLoop] forMode:NSDefaultRunLoopMode];
	
	[inputStream open];
	[outputStream open];
}

- (void)close {
	NSLog(@"Closing streams.");
	
	[inputStream close];
	[outputStream close];
	
	[inputStream removeFromRunLoop:[NSRunLoop mainRunLoop] forMode:NSDefaultRunLoopMode];
	[outputStream removeFromRunLoop:[NSRunLoop mainRunLoop] forMode:NSDefaultRunLoopMode];
	
	[inputStream setDelegate:nil];
	[outputStream setDelegate:nil];
	
//	[inputStream release];
//	[outputStream release];
	
	inputStream = nil;
	outputStream = nil;
}

- (void)stream:(NSStream *)stream handleEvent:(NSStreamEvent)event {
	NSLog(@"Stream triggered.");
	
	switch(event) {
		case NSStreamEventHasSpaceAvailable: {
			if(stream == outputStream) {
				NSLog(@"outputStream is ready.");
                if( outputBuffer && [outputBuffer count] && [outputStream streamStatus] == NSStreamStatusOpen && [outputStream hasSpaceAvailable]){
                    for( int i = 0; i < [outputBuffer count]; i++ ){
                        NSLog(@"Writing: %@", outputBuffer[i] );
                        [self write: (uint8_t *)[outputBuffer[i] bytes] length: (int)[outputBuffer[i] length]];
                    }
                    
                    while( [outputBuffer count] ){
                        [outputBuffer removeObjectAtIndex: 0];
                    }
                    
                    if( closeAfterWrite ){
                        [self close];
                    }
                }
			}
			break;
		}
		case NSStreamEventHasBytesAvailable: {
			if(stream == inputStream) {
				NSLog(@"inputStream is ready.");
				
				uint8_t buf[1024];
				unsigned int len = 0;
				
				len = (unsigned int)[inputStream read:buf maxLength:(NSUInteger)1024];
				
				if(len > 0) {
					NSMutableData* data=[[NSMutableData alloc] initWithLength:0];
					
					[data appendBytes: (const void *)buf length:len];
					
//					NSString *s = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
					
					[self readIn:data];
					
//					[data release];
				}
			}
			break;
		}
        case NSStreamEventErrorOccurred : {
            if( stream == inputStream ){
                NSLog(@"Input Stream Error: %@", [stream streamError]);
            } else if( stream == outputStream ){
                NSLog(@"Output Stream Error: %@", [stream streamError]);
            }
        }
		default: {
			NSLog(@"Stream is sending an Event: %lu", (long)event);
			
			break;
		}
	}
}

- (void)readIn:(NSData *)s {
	NSLog(@"Reading in the following:");
	NSLog(@"%@", s);
}

- (void) write: (uint8_t *) bytes length: (int) len {
    NSLog(@"Size of Bytes: %d", len);
    [outputStream write:bytes maxLength:len];
}

- (void)writeOut:(NSData *)s {
	if( [outputStream hasSpaceAvailable] && [outputStream streamStatus] == NSStreamStatusOpen ) {
        uint8_t *buf = (uint8_t *)[s bytes];
        [self write: buf length: (int)[s length]];
    } else {
        if(!outputBuffer){
            outputBuffer = [[NSMutableArray alloc] init];
            [outputBuffer addObject: s];
        }
    }
	NSLog(@"Writing out the following:");
	NSLog(@"%@", s);
}

- (void)writeOut:(NSData *)s closeAfterwards: (BOOL) shouldClose {
    closeAfterWrite = shouldClose;
    [self writeOut: s];
}

@end