//
//  LavuTransmitViewController.m
//  APNTest
//
//  Created by Theodore Schnepper on 3/14/14.
//  Copyright (c) 2014 Theodore Schnepper. All rights reserved.
//

#import "LavuTransmitViewController.h"

@interface LavuTransmitViewController ()

@end

@implementation LavuTransmitViewController

#pragma mark - Bluetooth Delegate Gabarge
-(void)peripheralManagerDidUpdateState:(CBPeripheralManager *)peripheral {
    if (peripheral.state == CBPeripheralManagerStatePoweredOn) {
        NSLog(@"Powered On");
//        [self.peripheralManager startAdvertising:self.beaconPeripheralData];
    } else if (peripheral.state == CBPeripheralManagerStatePoweredOff) {
        NSLog(@"Powered Off");
        if( [self.peripheralManager isAdvertising]){
            [self.peripheralManager stopAdvertising];
        }
    }
}

#pragma mark - Default
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    NSUUID* uuid = [[NSUUID alloc] initWithUUIDString: @"EE71F129-8E70-4684-AE33-8A545EEECBD4"];
    self.beaconRegion = [[CLBeaconRegion alloc] initWithProximityUUID:uuid major:1 minor:1 identifier:@"com.POSLavu.APNTest"];
    [self setLabels];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void) setLabels {
    [[self proximityUUIDLabel] setText: [[[self beaconRegion] proximityUUID] UUIDString]];
    [[self majorLabel] setText: [NSString stringWithFormat:@"%@", [[self beaconRegion] major]]];
    [[self minorLabel] setText: [NSString stringWithFormat:@"%@", [[self beaconRegion] minor]]];
    [[self identityLabel] setText: [[self beaconRegion] identifier]];
    
}

- (IBAction)toggleTransmit:(id)sender forEvent:(UIEvent *)event {
    UISwitch* switchButton = (UISwitch*)sender;
    if ([switchButton isOn]) {
        if( ! self.peripheralManager ){
            self.beaconPeripheralData = [self.beaconRegion peripheralDataWithMeasuredPower: nil];
            self.peripheralManager = [[CBPeripheralManager alloc] initWithDelegate:self queue:nil options:nil];
        }
        
        if( [self.peripheralManager state] == CBPeripheralManagerStatePoweredOn ) {
            [self.peripheralManager startAdvertising:self.beaconPeripheralData];
        } else {
            [switchButton setOn: false];
        }
    } else {
        if( [self.peripheralManager isAdvertising]){
            [self.peripheralManager stopAdvertising];
        }
    }
}
@end
