//
//  LavuIngredient.h
//  APNTest
//
//  Created by Theodore Schnepper on 3/21/14.
//  Copyright (c) 2014 Theodore Schnepper. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LavuBase.h"

@interface LavuIngredient : LavuBase
@property (retain, nonatomic) NSNumber* ID;
@property (retain, nonatomic) NSString* title;
@property (retain, nonatomic) NSString* unit;
@property (retain, nonatomic) NSNumber* quantity;
@property (retain, nonatomic) NSNumber* high;
@property (retain, nonatomic) NSNumber* low;
@property (retain, nonatomic) NSNumber* ingredientCategory;
@property (retain, nonatomic) NSNumber* cost;
@property (retain, nonatomic) NSNumber* locationID;
@property (retain, nonatomic) NSNumber* chainReportingGroup;

+ (id) lavuIngredients: (NSDictionary*) details;
@end
