//
//  LavuOptionsPickerTableViewController.m
//  APNTest
//
//  Created by Theodore Schnepper on 3/24/14.
//  Copyright (c) 2014 Theodore Schnepper. All rights reserved.
//

#import "LavuOptionsPickerTableViewController.h"

@implementation LavuOptionsPickerTableViewController
@synthesize currentItem = _currentItem;
@synthesize currentLocation = _currentLocation;
@synthesize currentMenu = _currentMenu;
@synthesize decisions = _decisions;

BOOL showAll;

NSArray* currentlySelectedOptions;
NSArray* allOptions;


- (void) setCurrentItem:(LavuMenuItem *)currentItem andDecisions: (LavuItemDecision*) decisions{
    showAll = false;
    if( decisions == nil ){
        _decisions = [[LavuItemDecision alloc] init];
        [_decisions setItem: currentItem];
        [_decisions setQuantity: [NSNumber numberWithInt: 1]];
    } else {
        _decisions = decisions;
    }
    if( currentItem && _currentMenu ){
        NSMutableArray* tempAllOptions = [[NSMutableArray alloc] init];
        NSMutableArray* tempSelectedOptions = [[NSMutableArray alloc] init];
        
        LavuMenuCategory* category = [_currentMenu lavuMenuCategoryFromID: [currentItem menuCategoryID]];
        NSNumber* forcedModifiersCount = [[_decisions forcedModifiersDecisions] totalChildrenInTree];
        if( [forcedModifiersCount isEqualToNumber: [NSNumber numberWithInt:0]] ){
            NSMutableArray* tempModifiersListArray = [NSMutableArray array];
            if( [[currentItem forcedModifierGroupID] isEqual: [NSNumber numberWithInt:0]] && [[currentItem forcedModifierListID] isEqual: [NSNumber numberWithInt:0]]){
                //Category
                if( ![[category forcedModifierGroupID] isEqual: [NSNumber numberWithInt:0]] ){
                    //Category Group
                    LavuForcedModifierGroup* forcedModiferGroup = [_currentMenu lavuForcedModifierGroupFromID: [category forcedModifierGroupID]];
                    [[forcedModiferGroup forcedModifierLists] enumerateObjectsUsingBlock:^(id forcedModifierListID, NSUInteger index, BOOL* stop2){
                        [tempModifiersListArray addObject: [_currentMenu lavuForcedModifierListFromID: forcedModifierListID]];
                    }];
                } else if( ![[category forcedModifierListID] isEqual: [NSNumber numberWithInt:0]] ){
                    //Category list
                    [tempModifiersListArray addObject: [_currentMenu lavuForcedModifierListFromID: [category forcedModifierListID]]];
                }
            } else {
                //Item
                if( ![[currentItem forcedModifierGroupID] isEqual: [NSNumber numberWithInt:0]] ){
                    //Group
                    LavuForcedModifierGroup* forcedModiferGroup = [_currentMenu lavuForcedModifierGroupFromID: [category forcedModifierGroupID]];
                    [[forcedModiferGroup forcedModifierLists] enumerateObjectsUsingBlock:^(id forcedModifierListID, NSUInteger index, BOOL* stop2){
                        [tempModifiersListArray addObject: [_currentMenu lavuForcedModifierListFromID: forcedModifierListID]];
                    }];
                } else {
                    //list
                    [tempModifiersListArray addObject: [_currentMenu lavuForcedModifierListFromID: [currentItem forcedModifierListID]]];
                }
            }
            NSMutableArray* decisionArray = [NSMutableArray array];
            [tempModifiersListArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop ){
                LavuForcedModifierDecision* decision = [[LavuForcedModifierDecision alloc] initWithMenu: _currentMenu ForcedModifierList: obj];
                [decisionArray addObject: decision];
                [decision setParent: [_decisions forcedModifiersDecisions]];
            }];
            
            [[_decisions forcedModifiersDecisions] setChildren: [decisionArray copy]];
        }
        
        NSArray* modifiers = [_currentMenu lavuModifiers];
        
        for( int i = 0; i < modifiers.count; i++ ){
            LavuModifier* mod = modifiers[i];
            if( [[mod lavuModifierCategoryID] isEqual: [currentItem modifierListID]] || ([[currentItem modifierListID] isEqual: [NSNumber numberWithInt:0]] && category && [[mod lavuModifierCategoryID] isEqual: [category modifierListID]]) ){
                [tempAllOptions addObject: modifiers[i]];
            }
        }
        
        allOptions = [tempAllOptions copy];
        
        for( int i = 0; i < [allOptions count]; i++ ){
            LavuModifier* mod = allOptions[i];
            NSDictionary* modifierIngredients = [mod ingredients];
            if( modifierIngredients.count != 1 ){
                continue;
            }
            NSEnumerator* ingredientIDs = [[currentItem ingredients] keyEnumerator];
            NSNumber* ingredientID;
            while( ingredientID = [ingredientIDs nextObject] ){
                if( modifierIngredients[ingredientID] != nil ){
                    [tempSelectedOptions addObject: mod];
                }
            }
        }
        currentlySelectedOptions = [tempSelectedOptions copy];
        [[self navigationItem] setTitle: [NSString stringWithFormat: @"%@ - %@", [category name],[currentItem name] ]];
    }
    _currentItem = currentItem;
    [[self tableView] reloadData];
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [self tableView: tableView heightForRowAtIndexPath: indexPath];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger section = [indexPath section];
    if( section == LAVU_OPTIONS_PICKER_SECTION_FORCED_MODIFIERS ){
        LavuForcedModifierDecision* decision = [[_decisions forcedModifiersDecisions] objectAtOffset: indexPath.row];
        if( decision && [[decision forcedModifierList] type] == LavuForcedModifierListTypeCheckList ){
            return 80.0f;
        } else {
            NSNumber* listID = [[decision forcedModifierList] ID];
            NSArray* lavuForcedModifiers = [_currentMenu lavuForcedModifiers];
            NSMutableArray* currentList = [[NSArray array] mutableCopy];
            [lavuForcedModifiers enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL* stop){
                LavuForcedModifier* forcedModifier = obj;
                if( [[forcedModifier listID] isEqual: listID] ){
                    [currentList addObject: obj];
                }
            }];
            if( [currentList count] > 4 ){
                return 190.0f;
            } else {
                return 75.0f;
            }
        }
        
    } else {
        return 50.0f;
    }
    return 150.0f;
}

#pragma mark - Table view data source

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    switch( section ){
        case LAVU_OPTIONS_PICKER_SECTION_FORCED_MODIFIERS: {
            return @"Forced Modifiers";
        } case LAVU_OPTIONS_PICKER_SECTION_OPTIONAL_INCLUDED: {
            if( [currentlySelectedOptions count] == 0 ){
                return @"";
            }
            return @"Includes";
        } case LAVU_OPTIONS_PICKER_SECTION_OPTIONAL_OTHERS: {
            if( [allOptions count] - [currentlySelectedOptions count] == 0 ){
                return @"";
            }
            return @"Extras";
        } case LAVU_OPTIONS_PICKER_SECTION_QUANTITY: {
            return @"Quantity";
        } case LAVU_OPTIONS_PICKER_SECTION_PRICING: {
            return @"Pricing";
        } case LAVU_OPTIONS_PICKER_SECTION_CONFIRM: {
            return @"Confirm";
        } default : {
            return @"";
        }
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return LAVU_OPTIONS_PICKER_SECTION_CONFIRM + 1; //Forced, Comes With, Options, Quantity, Subtotal, Confirm
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    switch( section ){
        case LAVU_OPTIONS_PICKER_SECTION_FORCED_MODIFIERS: {
            return [[[_decisions forcedModifiersDecisions] totalChildrenInTree] integerValue];
        } case LAVU_OPTIONS_PICKER_SECTION_OPTIONAL_INCLUDED: {
            if( currentlySelectedOptions ){
                return [currentlySelectedOptions count];
            }
            return 0;
        } case LAVU_OPTIONS_PICKER_SECTION_OPTIONAL_OTHERS: {
            if( allOptions ){
                if( currentlySelectedOptions ){
                    if( showAll ){
                        return [allOptions count] - [currentlySelectedOptions count];
                    } else {
                        if( [allOptions count] - [currentlySelectedOptions count] <= LAVUMAXOPTIONALPREVIEWCOUNT ){
                            showAll = true;
                        }
                        return MIN([allOptions count] - [currentlySelectedOptions count], LAVUMAXOPTIONALPREVIEWCOUNT);
                    }
                }
                
                if( showAll ){
                    return [allOptions count];
                } else {
                    if( [allOptions count] <= LAVUMAXOPTIONALPREVIEWCOUNT ){
                        showAll = true;
                    }
                    return MIN([allOptions count], LAVUMAXOPTIONALPREVIEWCOUNT);
                }
            }
            return 0;
        } case LAVU_OPTIONS_PICKER_SECTION_QUANTITY : {
        } case LAVU_OPTIONS_PICKER_SECTION_PRICING : {
        } case LAVU_OPTIONS_PICKER_SECTION_CONFIRM : {
            return 1;
        } default: {
            return 0;
        }
    }
}

#pragma mark - cell creation helper selectors
#pragma region test
- (UITableViewCell*) createForcedModifiersCellForTableView: (UITableView*) tableView atIndexPath: (NSIndexPath *)indexPath {
    UITableViewCell *cell;
    LavuForcedModifierDecision* decision = [[_decisions forcedModifiersDecisions] objectAtOffset: [indexPath row]];
    
    if( [[decision forcedModifierList] type] == LavuForcedModifierListTypeCheckList ){
        cell = [tableView dequeueReusableCellWithIdentifier:@LAVU_OPTIONS_NAVIGATION_OPTIONAL_MODIFIER_CELL_IDENTIFIER forIndexPath:indexPath];
        return cell;
    } else {
        cell = [tableView dequeueReusableCellWithIdentifier:@LAVU_OPTIONS_NAVIGATION_FORCED_MODIFIER_CELL_IDENTIFIER forIndexPath:indexPath];
        LavuForcedModifierListTableViewCell* actualCell = (LavuForcedModifierListTableViewCell*) cell;
        
        [[actualCell forcedModifierTitle] setText: [[decision forcedModifierList] title]];
        NSNumber* listID = [[decision forcedModifierList] ID];
        NSArray* lavuForcedModifiers = [_currentMenu lavuForcedModifiers];
        NSMutableArray* currentList = [[NSArray array] mutableCopy];
        [lavuForcedModifiers enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL* stop){
            LavuForcedModifier* forcedModifier = obj;
            if( [[forcedModifier listID] isEqual: listID] ){
                [currentList addObject: obj];
            }
        }];
        [actualCell setCurrentValue: [decision forcedModifier]];
        [actualCell setForcedModifiers: [currentList copy]];
        [actualCell setProto: self];
        //            [actualCell setPathToObject: indexPath];
        [actualCell setForcedModifierDecision: decision];
        return actualCell;
    }
}

- (UITableViewCell*) createOptionalIncludedModifierCellForTableView: (UITableView*) tableView atIndexPath: (NSIndexPath *)indexPath {
    UITableViewCell *cell;
    cell = [tableView dequeueReusableCellWithIdentifier:@LAVU_OPTIONS_NAVIGATION_OPTIONAL_MODIFIER_CELL_IDENTIFIER forIndexPath:indexPath];
    LavuOptionalModifierTableViewCell* actualCell = (LavuOptionalModifierTableViewCell*) cell;
    LavuModifier* mod = currentlySelectedOptions[indexPath.row];
//    [[actualCell priceLabel] setText: [[_currentLocation formatter] stringForObjectValue: [mod cost]]];
    [[actualCell priceLabel] setText: @""];
    UILabel* modifierLabel = (UILabel*) [cell viewWithTag: LAVU_OPTIONS_NAVIGATION_MODIFIER_LABEL_TAG];
    UISwitch* modifierSwitch = (UISwitch*) [cell viewWithTag: LAVU_OPTIONS_NAVIGATION_MODIFIER_SWITCH_TAG];
    [modifierLabel setText: [mod title]];
    if( [[_decisions removedOptionalModifiers] containsObject: mod] ){
        [modifierSwitch setOn: false];
    } else {
        [modifierSwitch setOn: true];
    }
    [actualCell setModifier: mod];
    [actualCell setEnabledByDefault: true];
    [actualCell setProto: self];
    [actualCell setPathToObject: indexPath];
    return actualCell;
}

- (UITableViewCell*) createOptionalExtraModifierCellForTableView: (UITableView*) tableView atIndexPath: (NSIndexPath *)indexPath {
    UITableViewCell *cell;
    int it = 0;
    
    if( indexPath.row == LAVUMAXOPTIONALPREVIEWCOUNT - 1 && !showAll ){
        cell = [tableView dequeueReusableCellWithIdentifier:@LAVU_OPTIONS_NAVIGATION_LOAD_MORE_CELL_IDENTIFIER forIndexPath:indexPath];
        return cell;
    } else {
        cell = [tableView dequeueReusableCellWithIdentifier:@LAVU_OPTIONS_NAVIGATION_OPTIONAL_MODIFIER_CELL_IDENTIFIER forIndexPath:indexPath];
        LavuOptionalModifierTableViewCell* actualCell = (LavuOptionalModifierTableViewCell*) cell;
        for( int i = 0; i < allOptions.count; i++ ){
            if( [currentlySelectedOptions containsObject: allOptions[i]] ){
                continue;
            }
            
            if( it++ == indexPath.row ){
                LavuModifier* mod = allOptions[i];
                [[actualCell priceLabel] setText: [[_currentLocation formatter] stringForObjectValue: [mod cost]]];
                UILabel* modifierLabel = (UILabel*) [cell viewWithTag: LAVU_OPTIONS_NAVIGATION_MODIFIER_LABEL_TAG];
                UISwitch* modifierSwitch = (UISwitch*) [cell viewWithTag: LAVU_OPTIONS_NAVIGATION_MODIFIER_SWITCH_TAG];
                [modifierLabel setText: [mod title]];
                if( [[_decisions addedOptionalModifiers] containsObject: mod] ){
                    [modifierSwitch setOn: true];
                } else {
                    [modifierSwitch setOn: false];
                }
                [actualCell setModifier: mod];
                [actualCell setEnabledByDefault: false];
                [actualCell setProto: self];
                [actualCell setPathToObject: indexPath];
                return actualCell;
            }
        }
        return cell;
    }
}

- (UITableViewCell*) createQuantityCellForTableView: (UITableView*) tableView atIndexPath: (NSIndexPath *)indexPath {
    LavuQuantityTableViewCell* actualCell = (LavuQuantityTableViewCell*) [tableView dequeueReusableCellWithIdentifier:@LAVU_OPTIONS_NAVIGATION_QUANTITY_CELL_IDENTIFIER forIndexPath:indexPath];
    [actualCell setProto: self];
    return actualCell;
}

- (UITableViewCell*) createPricingCellForTableView: (UITableView*) tableView atIndexPath: (NSIndexPath *)indexPath {
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@LAVU_OPTIONS_NAVIGATION_PRICING_CELL_IDENTIFIER forIndexPath:indexPath];
    UILabel* priceDisplay = (UILabel*)[cell viewWithTag: LAVU_OPTIONS_NAVIGATION_PRICE_LABEL_TAG];
    [priceDisplay setText: [[_currentLocation formatter] stringForObjectValue: [_decisions calculateSubtotal]]];
    return cell;
}

- (UITableViewCell*) createConfirmCellForTableView: (UITableView*) tableView atIndexPath: (NSIndexPath *)indexPath {
    return [tableView dequeueReusableCellWithIdentifier:@LAVU_OPTIONS_NAVIGATION_CONFIRM_CELL_IDENTIFIER forIndexPath:indexPath];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger section = [indexPath section];
    UITableViewCell *cell;
    
    switch( section ){
        case LAVU_OPTIONS_PICKER_SECTION_FORCED_MODIFIERS: {
            return [self createForcedModifiersCellForTableView: tableView atIndexPath: indexPath];
        } case LAVU_OPTIONS_PICKER_SECTION_OPTIONAL_INCLUDED: {
            return [self createOptionalIncludedModifierCellForTableView: tableView atIndexPath: indexPath];
        } case LAVU_OPTIONS_PICKER_SECTION_OPTIONAL_OTHERS: {
            return [self createOptionalExtraModifierCellForTableView: tableView atIndexPath: indexPath];
        } case LAVU_OPTIONS_PICKER_SECTION_QUANTITY: {
            return [self createQuantityCellForTableView: tableView atIndexPath: indexPath];
        } case LAVU_OPTIONS_PICKER_SECTION_PRICING: {
            return [self createPricingCellForTableView: tableView atIndexPath: indexPath];
        } case LAVU_OPTIONS_PICKER_SECTION_CONFIRM: {
            return [self createConfirmCellForTableView: tableView atIndexPath: indexPath];
        } default : {
            return cell;
        }
    }
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Actions

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if( [indexPath section] == LAVU_OPTIONS_PICKER_SECTION_OPTIONAL_OTHERS && indexPath.row == LAVUMAXOPTIONALPREVIEWCOUNT - 1 && !showAll ){
        showAll = true;
        [[self tableView] reloadSections:[NSIndexSet indexSetWithIndex: LAVU_OPTIONS_PICKER_SECTION_OPTIONAL_OTHERS] withRowAnimation:UITableViewRowAnimationFade];
    } else if( [indexPath section] == LAVU_OPTIONS_PICKER_SECTION_CONFIRM ){
        LavuOrder* order = [LavuOrder currentOrder];
        [order addItem: _decisions];
        [order setLocation: _currentLocation];
        [self performSegueWithIdentifier:@LAVU_INGREDIENTS_TO_ORDER_SEGUE sender:self];
    }
}

- (IBAction) modifier: (LavuModifier*) modifier enabledByDefault: (BOOL) enabled toggledTo: (BOOL) state atIndexPath: (NSIndexPath*) indexPath {
    if( enabled ){
        if( state ){
            [_decisions removeRemovedOptionalModifier: modifier];
        } else {
            [_decisions addRemovedOptionalModifier: modifier];
        }
    } else {
        if( state ){
            [_decisions addAddedOptionalModifier: modifier];
        } else {
            [_decisions removeAddedOptionalModifier: modifier];
        }
    }
    [[self tableView] reloadSections: [NSIndexSet indexSetWithIndex: LAVU_OPTIONS_PICKER_SECTION_PRICING] withRowAnimation: UITableViewRowAnimationNone];
}

- (IBAction) selectedForcedModifier: (LavuForcedModifier*) forcedModifier forDecision: (LavuForcedModifierDecision*) decision {   
    BOOL refresh = [decision forcedModifier] != forcedModifier;
    NSInteger row = [[[_decisions forcedModifiersDecisions] calculateIndexForItem: decision] integerValue];
    
    NSInteger originalChildrenCount = [[decision totalChildrenInTree] integerValue];
    [decision setForcedModifier: forcedModifier];
    if( refresh ){
        if( originalChildrenCount ){
            NSMutableArray* temp = [NSMutableArray array];
            for( int i = 0; i < originalChildrenCount; i++ ){
                const NSUInteger indexPath[] = { LAVU_OPTIONS_PICKER_SECTION_FORCED_MODIFIERS, i + row + 1 };
                [temp addObject: [NSIndexPath indexPathWithIndexes: indexPath length: 2]];
            }
            [[self tableView] deleteRowsAtIndexPaths:[temp copy] withRowAnimation:UITableViewRowAnimationFade];
        }
        
        NSUInteger totalChildren = [[decision children] count];
        
        NSMutableArray* temp = [NSMutableArray array];
        for( int i = 0; i < totalChildren; i++ ){
            const NSUInteger indexPath[] = { LAVU_OPTIONS_PICKER_SECTION_FORCED_MODIFIERS, i + row + 1 };
            [temp addObject: [NSIndexPath indexPathWithIndexes: indexPath length: 2]];
        }
        [[self tableView] insertRowsAtIndexPaths:[temp copy] withRowAnimation:UITableViewRowAnimationFade];
//        [[self tableView] reloadSections: [NSIndexSet indexSetWithIndex: LAVU_OPTIONS_PICKER_SECTION_FORCED_MODIFIERS] withRowAnimation: UITableViewRowAnimationMiddle];
    }
    [[self tableView] reloadSections: [NSIndexSet indexSetWithIndex: LAVU_OPTIONS_PICKER_SECTION_PRICING] withRowAnimation: UITableViewRowAnimationNone];
}

- (IBAction) quantityChangedTo: (NSNumber*) number {
    [_decisions setQuantity: number];
    [[self tableView] reloadSections: [NSIndexSet indexSetWithIndex: LAVU_OPTIONS_PICKER_SECTION_PRICING] withRowAnimation: UITableViewRowAnimationNone];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    id destination = [segue destinationViewController];
    if( [destination respondsToSelector: @selector(setOrder:)] ){
        [destination setOrder: [LavuOrder currentOrder]];
    }
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
@end
