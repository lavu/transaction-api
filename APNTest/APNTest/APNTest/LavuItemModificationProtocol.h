//
//  LavuItemModificationProtocol.h
//  APNTest
//
//  Created by Theodore Schnepper on 4/3/14.
//  Copyright (c) 2014 Theodore Schnepper. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LavuItemDecision.h"

@protocol LavuItemModificationProtocol <NSObject>
- (void) removeItemDecision: (LavuItemDecision*) itemDecision;
- (void) editItemDecision: (LavuItemDecision*) itemDecision;
@end
