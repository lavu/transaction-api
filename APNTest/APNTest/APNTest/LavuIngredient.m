//
//  LavuIngredient.m
//  APNTest
//
//  Created by Theodore Schnepper on 3/21/14.
//  Copyright (c) 2014 Theodore Schnepper. All rights reserved.
//

#import "LavuIngredient.h"

@implementation LavuIngredient
@synthesize ID = _ID;
@synthesize title = _title;
@synthesize unit = _unit;
@synthesize quantity = _quantity;
@synthesize high = _high;
@synthesize low = _low;
@synthesize ingredientCategory = _ingredientCategory;
@synthesize cost = _cost;
@synthesize locationID = _locationID;
@synthesize chainReportingGroup = _chainReportingGroup;

+ (id) lavuIngredients: (NSDictionary*) details {
    LavuIngredient* result = [[LavuIngredient alloc] init];
    if(! [result setValueUsing: @selector(setID:) ifKeyExists:@"id" usingDetails: details]){
        return nil;
    }
    [result setValueUsing: @selector(setTitle:) ifKeyExists:@"title" usingDetails: details];
    [result setValueUsing: @selector(setQuantity:) ifKeyExists:@"qty" usingDetails: details];
    [result setValueUsing: @selector(setUnit:) ifKeyExists:@"unit" usingDetails: details];
    [result setValueUsing: @selector(setLow:) ifKeyExists:@"low" usingDetails: details];
    [result setValueUsing: @selector(setHigh:) ifKeyExists:@"high" usingDetails: details];
    [result setValueUsing: @selector(setIngredientCategory:) ifKeyExists:@"category" usingDetails: details];
    [result setValueUsing: @selector(setCost:) ifKeyExists:@"cost" usingDetails: details];
    [result setValueUsing: @selector(setLocationID:) ifKeyExists:@"loc_id" usingDetails: details];
    return result;
}
@end
