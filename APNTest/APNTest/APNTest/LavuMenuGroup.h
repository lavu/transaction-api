//
//  LavuMenuGroup.h
//  APNTest
//
//  Created by Theodore Schnepper on 3/21/14.
//  Copyright (c) 2014 Theodore Schnepper. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LavuBase.h"

@interface LavuMenuGroup : LavuBase
@property (retain, nonatomic) NSNumber* ID;
@property (retain, nonatomic) NSString* name;
@property (retain, nonatomic) NSNumber* menuID;
@property (retain, nonatomic) NSNumber* order;

+ (id) lavuMenuGroup: (NSDictionary*) details;

@end
