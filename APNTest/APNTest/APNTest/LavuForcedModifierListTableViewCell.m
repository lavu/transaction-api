//
//  LavuForcedModifierListTableViewCell.m
//  APNTest
//
//  Created by Theodore Schnepper on 3/27/14.
//  Copyright (c) 2014 Theodore Schnepper. All rights reserved.
//

#import "LavuForcedModifierListTableViewCell.h"

@implementation LavuForcedModifierListTableViewCell
@synthesize forcedModifiers = _forcedModifiers;
@synthesize currentValue = _currentValue;
@synthesize proto = _proto;
@synthesize forcedModifierDecision = _forcedModifierDecision;

#pragma mark - Default Implemenation Functions
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        _forcedModifiers = @[];
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
//    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - UIPickerViewDataSource
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return [_forcedModifiers count];
}

#pragma mark - UIPickerViewDelegate
//- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component {
//    return 20.0f;
//}

//- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
//    return [[self contentView] frame].size.width;
//}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    LavuForcedModifier* forcedModifier = _forcedModifiers[row];
    return [forcedModifier title];
}

#pragma mark - Data Modification/View Updates
- (void) makePicker {
    CGRect frame = [[self contentView] frame];
    frame.origin.y = 29.0f;
    UIPickerView* picker = [[UIPickerView alloc] initWithFrame: frame];
    [picker setAutoresizesSubviews: true];
    [picker setDelegate: (id<UIPickerViewDelegate>)self];
    [picker setDataSource: self];
    [picker reloadAllComponents];
    
    [[self contentView] addSubview: picker];
}

- (void) makeSegmentedControl {
    NSMutableArray* titleArray = [[NSArray array] mutableCopy];
    [_forcedModifiers enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL* stop){
        LavuForcedModifier* forcedModifier = obj;
        [titleArray addObject: [forcedModifier title]];
    }];
    
    UISegmentedControl* control  = [[UISegmentedControl alloc] initWithItems: titleArray];
    
    CGRect controlFrame = [control frame];
    CGRect contentFrame = [[self contentView] frame];
    contentFrame.size.height = 75.0f;
    controlFrame.size.width = contentFrame.size.width - 12.5f;
    controlFrame.origin.y = (((contentFrame.size.height - 29.0f) - controlFrame.size.height) / 2.0f) + 29.0f;
    controlFrame.origin.x = (contentFrame.size.width - controlFrame.size.width) / 2.0f;
    [control setFrame: controlFrame];
    [[self contentView] addSubview: control];
    
    [control addTarget: self action:@selector(segmentedControlAction:withEvent:) forControlEvents: UIControlEventValueChanged];
}

- (void) makeToggle {
    
}

- (void) updateView {
    [[[self contentView] subviews] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL* stop){
        if( ![obj isKindOfClass: [UILabel class]]){
            [(UIView*)obj removeFromSuperview];
        }
    }];
    
    if( [_forcedModifiers count] > 4 ){
        //Do a Picker
        [self makePicker];
    } else if([_forcedModifiers count] > 1 ){
        //Do a SegmentedControl
        [self makeSegmentedControl];
    } else if( [_forcedModifiers count] == 1 ){
        //Do a Toggle
        [self makeToggle];
    } else {
        //Hell if I know what to do at this point.
    }
}

- (void) setForcedModifiers:(NSArray *)forcedModifiers {
    if( forcedModifiers == nil ){
        return;
    }
    
    _forcedModifiers = forcedModifiers;
    [self updateView];
}

- (void) setForcedModifierDecision:(LavuForcedModifierDecision *)forcedModifierDecision {
    _forcedModifierDecision = forcedModifierDecision;
    if( _forcedModifierDecision && [_forcedModifierDecision forcedModifier] ){
        NSInteger index = [_forcedModifiers indexOfObject: [_forcedModifierDecision forcedModifier]];
        id view = [[self contentView] subviews][1];
        if( [view isKindOfClass: [UIPickerView class]] ){
            UIPickerView* picker = view;
            if( index < [picker numberOfRowsInComponent: 0] ){
                [picker selectRow:index inComponent:0 animated:true];
            }
        } else if ( [view isKindOfClass: [UISegmentedControl class]] ){
            UISegmentedControl* control = view;
            if( index < [control numberOfSegments] ){
                [control setSelectedSegmentIndex: index];
                [control setSelected: true];
            }
        }
    }
}

- (BOOL) addForcedModifier: (LavuForcedModifier*) forcedModifier {
    if( forcedModifier == nil ){
        return false;
    }
    
    if( [_forcedModifiers containsObject: forcedModifier] ){
        return false;
    }
    NSMutableArray* tempForcedModifiersArray = [_forcedModifiers mutableCopy];
    [tempForcedModifiersArray addObject: forcedModifier];
    _forcedModifiers = [tempForcedModifiersArray copy];
    [self updateView];
    return true;
}
- (BOOL) removeForcedModifier: (LavuForcedModifier*) forcedModifier {
    if( forcedModifier == nil ){
        return false;
    }
    
    if( ![_forcedModifiers containsObject: forcedModifier] ){
        return false;
    }
    
    NSMutableArray* tempForcedModifiersArray = [_forcedModifiers mutableCopy];
    [tempForcedModifiersArray removeObject: forcedModifier];
    _forcedModifiers = [tempForcedModifiersArray copy];
    [self updateView];
    return true;
}

#pragma mark - Actions

- (IBAction) segmentedControlAction: (id) element withEvent: (UIEvent*) event {
    if( _proto ){
        UISegmentedControl* control = element;
        [_proto selectedForcedModifier: _forcedModifiers[[control selectedSegmentIndex]] forDecision: _forcedModifierDecision];
    }
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    if(_proto){
        [_proto selectedForcedModifier: _forcedModifiers[row] forDecision: _forcedModifierDecision];
    }
}

@end
