//
//  LavuPrinterBase.m
//  APNTest
//
//  Created by Theodore Schnepper on 4/2/14.
//  Copyright (c) 2014 Theodore Schnepper. All rights reserved.
//

#import "LavuPrinterBase.h"

@implementation LavuPrinterBase
@synthesize printerID = _printerID;
@synthesize name = _name;
@synthesize address = _address;
@synthesize ipAddress = _ipAddress;
@synthesize port = _port;

@synthesize skipPing = _skipPing;
@synthesize eTBFlag = _eTBFlag;
@synthesize writePauseDuration = _writePauseDuration;

@synthesize lavuPrinterCommandSet = _lavuPrinterCommandSet;
@synthesize lavuPrinterFormatting = _lavuPrinterFormatting;

+ (instancetype) lavuPrinterBaseWithDetails: (NSDictionary*) dict {
    LavuPrinterBase* printer = [[LavuPrinterBase alloc] init];
    
    [printer setValueUsing:@selector(setPrinterID:) ifKeyExists:@"setting" usingDetails:dict];
    [printer setValueUsing:@selector(setName:) ifKeyExists:@"value2" usingDetails:dict];
    [printer setValueUsing:@selector(setIpAddress:) ifKeyExists:@"value3" usingDetails:dict];
    [printer setValueUsing:@selector(setPort:) ifKeyExists:@"value5" usingDetails:dict];
    [printer setValueUsing:@selector(setLavuPrinterCommandSetWithInteger:) ifKeyExists:@"value6" usingDetails:dict];
//    [printer setValueUsing:@selector(setImageCapability:) ifKeyExists:@"value10" usingDetails:dict];
    [printer setValueUsing:@selector(setSkipPing:) ifKeyExists:@"value13" usingDetails:dict];
//    [printer setValueUsing:@selector(setStatusCheck:) ifKeyExists:@"value11" usingDetails:dict];
    [printer setValueUsing:@selector(setETBFlag:) ifKeyExists:@"value12" usingDetails:dict];
    [printer setValueUsing:@selector(setWritePauseDuration:) ifKeyExists:@"value14" usingDetails:dict];
    [printer setLavuPrinterFormattingUsingDetails: dict];
    
    return printer;
}

- (void) setLavuPrinterCommandSetWithInteger: (NSNumber*) value {
    _lavuPrinterCommandSet = [LavuPrinterCommandSetFactory createPrinterCommandSetWithInteger: [value integerValue]];
}

- (void) setLavuPrinterFormattingUsingDetails: (NSDictionary*) dict {
    _lavuPrinterFormatting = [LavuPrinterFormatting lavuPrinterFormmatingWithDetails: dict];
}

- (NSData*) generatePrintData: (id) order {
    return [_lavuPrinterCommandSet generatePrintData: order usingFormatting: _lavuPrinterFormatting];
}
@end
