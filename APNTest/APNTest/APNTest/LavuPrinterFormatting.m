//
//  LavuPrinterFormatting.m
//  APNTest
//
//  Created by Theodore Schnepper on 4/7/14.
//  Copyright (c) 2014 Theodore Schnepper. All rights reserved.
//

#import "LavuPrinterFormatting.h"

@implementation LavuPrinterFormatting
@synthesize charactersPerLine = _charactersPerLine;
@synthesize charactersPerLineLargeFont = _charactersPerLineLargeFont;
@synthesize lineSpacing = _lineSpacing;
@synthesize individualTicketPerItem = _individualTicketPerItem;

+ (LavuPrinterFormatting*) lavuPrinterFormmatingWithDetails: (NSDictionary*) dict {
    LavuPrinterFormatting* result = [[LavuPrinterFormatting alloc] init];
    [result setValueUsing:@selector(setCharactersPerLine:) ifKeyExists:@"value7" usingDetails:dict];
    [result setValueUsing:@selector(setCharactersPerLineLargeFont:) ifKeyExists:@"value8" usingDetails:dict];
    [result setValueUsing:@selector(setLineSpacing:) ifKeyExists:@"value9" usingDetails:dict];
    [result setValueUsing:@selector(setIndividualTicketPerItem:) ifKeyExists:@"value_long" usingDetails:dict];
    
    return result;
}
@end
