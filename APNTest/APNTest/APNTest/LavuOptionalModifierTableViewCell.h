//
//  LavuOptionalModifierTableViewCell.h
//  APNTest
//
//  Created by Theodore Schnepper on 3/31/14.
//  Copyright (c) 2014 Theodore Schnepper. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LavuModifier.h"
#import "LavuOptionalModifierStateChangeProtocol.h"
#import "LavuPrinterBase.h"

@interface LavuOptionalModifierTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UISwitch *optionToggle;
@property (weak, nonatomic) IBOutlet UILabel *optionLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (retain, nonatomic) LavuModifier* modifier;
@property BOOL enabledByDefault;
@property (weak, nonatomic) id<LavuOptionalModifierStateChangeProtocol> proto;
@property (retain, nonatomic) NSIndexPath* pathToObject;

- (IBAction)toggled:(id)sender;
@end
