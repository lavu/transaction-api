//
//  LavuMainMenuTableViewController.m
//  APNTest
//
//  Created by Theodore Schnepper on 3/19/14.
//  Copyright (c) 2014 Theodore Schnepper. All rights reserved.
//

#import "LavuMainMenuTableViewController.h"
#import "LavuPrinterCommandSet.h"

@interface LavuMainMenuTableViewController ()

@end

@implementation LavuMainMenuTableViewController
@synthesize beaconRegion = _beaconRegion;
@synthesize locationManager = _locationManager;
@synthesize trackedBeacons = _trackedBeacons;
@synthesize trackedLocations = _trackedLocations;
@synthesize selection = _selection;
@synthesize currentLocations = _currentLocations;
@synthesize beaconLocationMappings = _beaconLocationMappings; // (major, minor) -> (rest_id, loc_id, menu_group_id, [category_id])
@synthesize mappingData = _mappingData; //NSIndexPath(major, minor) -> mapping

#pragma mark - Beacon Delgate Garbage
- (void)locationManager:(CLLocationManager *)manager didEnterRegion:(CLRegion *)region {
    if( [[UIApplication sharedApplication] applicationState] != UIApplicationStateActive ){
        UILocalNotification* notification = [[UILocalNotification alloc] init];
        [notification setFireDate: [NSDate date]];
        [notification setAlertBody:[NSString stringWithFormat:@"You are in Range of a Location!"]];
        [notification setAlertAction: @"Lavu Local Range Notification"];
        [notification setApplicationIconBadgeNumber: 1];
        [notification setSoundName: @"bingbong.aiff"];
        
        //[application scheduleLocalNotification: notification];
        [[UIApplication sharedApplication] presentLocalNotificationNow:notification];
    }
    [_locationManager startRangingBeaconsInRegion:self.beaconRegion];
}

-(void)locationManager:(CLLocationManager *)manager didExitRegion:(CLRegion *)region {
    [_locationManager stopRangingBeaconsInRegion:self.beaconRegion];
}

- (void)locationManager:(CLLocationManager *)manager didStartMonitoringForRegion:(CLRegion *)region {
    [_locationManager startRangingBeaconsInRegion:self.beaconRegion];
}

-(void)locationManager:(CLLocationManager *)manager didRangeBeacons:(NSArray *)beacons inRegion:(CLBeaconRegion *)region {
//    [@"Testing like a boss" encodeWithCoder: [[LavuPrinterCommandSet alloc] init]];
    CLBeacon *beacon = nil;
    NSMutableArray* convertedBeacons = [[NSMutableArray alloc] init];
    for( int i = 0; i < beacons.count; i++ ){
        beacon = [beacons objectAtIndex: i];
        const NSUInteger identifier[] = { [[beacon major] unsignedIntValue],[[beacon minor] unsignedIntValue] };
        //        NSString* identifier = [NSString stringWithFormat:@"%d-%d", , ];
        if( beacon.proximity != CLProximityUnknown  ){
            
            [convertedBeacons addObject: [[NSIndexPath alloc] initWithIndexes:identifier length:2]];
        }
    }
    
    BOOL added = FALSE;
    BOOL removed = FALSE;
    for( int i = 0; i < convertedBeacons.count; i++ ){
        if( ![_trackedBeacons containsObject: [convertedBeacons objectAtIndex: i]] ) {
            [_trackedBeacons addObject: [convertedBeacons objectAtIndex: i]];
            added = TRUE;
        }
    }
    
    NSMutableArray* newArray = [[NSMutableArray alloc] init];
    for( int i=0; i < _trackedBeacons.count; i++ ){
        if( [convertedBeacons containsObject: [_trackedBeacons objectAtIndex: i]] ){
            [newArray addObject:[_trackedBeacons objectAtIndex: i]];
        } else {
            removed = TRUE;
        }
    }
    
    if( (added || removed)){
        _trackedBeacons = newArray;
        
        NSURL *theURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://admin.poslavu.com/beacon_service/request/Locations/%d.json", [[[beacons lastObject] major] unsignedIntValue]]];
        
        NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:theURL cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:10.0f];
        [theRequest setHTTPMethod:@"GET"];

        NSError *theError = NULL;
        NSURLResponse *theResponse = NULL;
        NSData *theResponseData = [NSURLConnection sendSynchronousRequest:theRequest returningResponse:&theResponse error:&theError];
        if(!theResponseData){
            _trackedBeacons = [[NSArray array] mutableCopy];
            return;
        }
        NSArray* data = [NSJSONSerialization JSONObjectWithData:theResponseData options:NSJSONReadingAllowFragments error:&theError];
//        NSLog( @"Data: %@", data );

        for( int i = 0; i < data.count; i++ ){
            NSDictionary* mappingData = [data objectAtIndex: i];
            const NSUInteger index_pieces[] = { [mappingData[@"major"] unsignedIntegerValue], [mappingData[@"minor"] unsignedIntegerValue] };
            NSIndexPath* index = [[NSIndexPath alloc] initWithIndexes:index_pieces length:2];
            NSIndexPath* locationPath = nil;
            if( ![mappingData[@"category_id"] isKindOfClass: [NSNull class]] ){
                const NSUInteger location_index[] = { [mappingData[@"rest_id"] unsignedIntegerValue],  [mappingData[@"loc_id"] unsignedIntegerValue], [mappingData[@"menu_group_id"] unsignedIntegerValue],  [mappingData[@"category_id"] unsignedIntegerValue]  };
                locationPath = [[NSIndexPath alloc] initWithIndexes: location_index length:4 ];
            } else {
                const NSUInteger location_index[] = { [mappingData[@"rest_id"] unsignedIntegerValue],  [mappingData[@"loc_id"] unsignedIntegerValue], [mappingData[@"menu_group_id"] unsignedIntegerValue] };
                locationPath = [[NSIndexPath alloc] initWithIndexes: location_index length:3 ];
            }
            _beaconLocationMappings[index] = locationPath;
            if( ![_trackedLocations containsObject: locationPath]){
                [_trackedLocations addObject: locationPath];
            }
            [_mappingData setObject:mappingData forKey:locationPath];
        }
        
        NSMutableArray* tempArray = [[NSMutableArray alloc] init];
        for( int i = 0; i < _trackedBeacons.count; i++ ){
            if( _beaconLocationMappings && _trackedBeacons && ![tempArray containsObject: _beaconLocationMappings[_trackedBeacons[i]]] ){
                [tempArray addObject: _beaconLocationMappings[_trackedBeacons[i]]];
            }
        }
        _currentLocations = tempArray;
        [[self tableView] reloadSections:[NSIndexSet indexSetWithIndex: LAVU_MENU_SECTION_LOCATIONS] withRowAnimation: UITableViewRowAnimationFade];
    }
}

#pragma mark - Custom Initialization
- (void)initRegion {
    _trackedBeacons = [[NSMutableArray alloc] init];
    
    NSUUID *uuid = [[NSUUID alloc] initWithUUIDString:@"EE71F129-8E70-4684-AE33-8A545EEECBD4"];
    self.beaconRegion = [[CLBeaconRegion alloc] initWithProximityUUID:uuid identifier:@"com.POSLavu.APNTest"];
    [_locationManager startMonitoringForRegion:self.beaconRegion];
}

#pragma mark UITableView

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.delegate = self;
    [self initRegion];
    
    if( ! _mappingData){
        [self setMappingData: [[NSMutableDictionary alloc] init]];
    }
    
    if( ! _beaconLocationMappings){
        [self setBeaconLocationMappings: [[NSMutableDictionary alloc] init]];
    }
    
    if( !_trackedLocations ){
        [self setTrackedLocations: [[NSMutableArray alloc] init]];
    }
    
    if( !_currentLocations ){
        [self setCurrentLocations: [[NSMutableArray alloc] init]];
    }
    
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return LAVU_MENU_SECTIONS_COUNT;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    switch( section ){
        case LAVU_MENU_SECTION_ORDERS :
            return LAVU_MENU_ORDERS_SECTION_ROWS_COUNT;
        case LAVU_MENU_SECTION_LOCATIONS :{
            return _currentLocations.count;
        }
        default:
            return 0;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@LAVU_MAIN_MENU_NAVIGATION_ITEM_CELL_IDENTIFIER forIndexPath:indexPath];
    // Configure the cell...
    switch( [indexPath section] ){
        case LAVU_MENU_SECTION_ORDERS: {
            if( [indexPath row] == LAVU_MENU_ORDERS_SECTION_CURRENT_ORDER ){
                UILabel* cellTitle = (UILabel*)[cell viewWithTag: LAVU_MAIN_MENU_NAVIGATION_ITEM_LABEL_TAG];
                if( [(LavuOrder*)[LavuOrder currentOrder] location] ){
                    [cellTitle setText:@"Current Order"];
                } else {
                    [cellTitle setText:@"No Active Order"];
                }
            } else if( [indexPath row] == LAVU_MENU_ORDERS_SECTION_ORDER_HISTORY ){
                UILabel* cellTitle = (UILabel*)[cell viewWithTag: LAVU_MAIN_MENU_NAVIGATION_ITEM_LABEL_TAG];
                [cellTitle setText:@"Order History"];
            }
            return cell;
        } case LAVU_MENU_SECTION_LOCATIONS: {
            UILabel* cellTitle = (UILabel*)[cell viewWithTag: LAVU_MAIN_MENU_NAVIGATION_ITEM_LABEL_TAG];
            
            if( _mappingData && [_mappingData objectForKey: _currentLocations[[indexPath row]]]){
                NSDictionary* mapping = [_mappingData objectForKey: _currentLocations[[indexPath row]]];
                [cellTitle setText: [NSString stringWithFormat:@"%@ - %@", mapping[@"company_name"], mapping[@"name"]]];
            }
            return cell;
        } default: {
            return cell;
        }
    }
    return cell;
}

//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
//    return 20.0f;
//}
//
//- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForHeaderInSection:(NSInteger)section {
//    return 20.0f;
//}

//- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
//    UILabel* label = [[UILabel alloc] initWithFrame: CGRectMake(0.0f, 0.0f, [[self view] frame].size.width, 20.0f)];
//    switch( section ){
//        case 0: {
//            [label setText:@"Orders"];
//            break;
//        } case 1 : {
//            [label setText:@"Locations"];
//            break;
//        } default:;
//    }
//    return label;
//}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    switch( section ){
        case LAVU_MENU_SECTION_ORDERS: {
            return @"Orders";
        } case LAVU_MENU_SECTION_LOCATIONS : {
            return @"Locations";
        } default:
            return @"";
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    switch( [indexPath section] ){
        case LAVU_MENU_SECTION_LOCATIONS: {
            [self setSelection: _currentLocations[[indexPath row]]];
            [self performSegueWithIdentifier:@LAVU_MAIN_TO_MENU_SEGUE_IDENTIFIER sender:self];
            break;
        } case LAVU_MENU_SECTION_ORDERS: {
            switch ( [indexPath row] ) {
                case LAVU_MENU_ORDERS_SECTION_CURRENT_ORDER: {
                    if( [(LavuOrder*)[LavuOrder currentOrder] location] ){
                        [self performSegueWithIdentifier:@LAVU_MAIN_TO_ORDER_SEGUE_IDENTIFIER sender:self];
                    }
                    break;
                } default: { }
            }
            break;
        } default: { }
    }
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    NSString* segueIdentifier = [segue identifier];
    
    if( [segueIdentifier isEqualToString: @LAVU_MAIN_TO_ORDER_SEGUE_IDENTIFIER] ){
        LavuOrderScreenTableViewController* destination = [segue destinationViewController];
        [destination setOrder: [LavuOrder currentOrder]];
    } else if( [segueIdentifier isEqualToString: @LAVU_MAIN_TO_MENU_SEGUE_IDENTIFIER] ) {
        LavuMenuTableViewController* destination = [segue destinationViewController];
        if( !self.selection ){
            return;
        }
        NSEnumerator* enumerator = [_beaconLocationMappings keyEnumerator];
        
        NSIndexPath* key = nil;
        while( key = [enumerator nextObject] ){
            if( [_beaconLocationMappings[key] isEqual: _selection] ){
                [destination setMajorMinor:key];
                [destination setTitle:[NSString stringWithFormat:@"%@ - %@", _mappingData[_selection][@"company_name"], _mappingData[_selection][@"name"]]];
                break;
            }
        }
    }
    /*if( self.selection && _mappingData[_beaconLocationMappings[_selection]] ){
        NSDictionary* mapping = _mappingData[_beaconLocationMappings[_selection]];
        NSString* title = [NSString stringWithFormat:@"%@ - %@", mapping[@"company_name"], mapping[@"name"]];
        [[destination navigationItem] setTitle: title];
        
        [destination setMajorMinor:self.selection];
    }*/
}


@end
