//
//  LavuOptionsPickerTableViewController.h
//  APNTest
//
//  Created by Theodore Schnepper on 3/24/14.
//  Copyright (c) 2014 Theodore Schnepper. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LavuLocation.h"
#import "LavuForcedModifierListTableViewCell.h"
#import "LavuOptionalModifierTableViewCell.h"
#import "LavuQuantityTableViewCell.h"
#import "LavuOrderScreenTableViewController.h"

#import "LavuOrder.h"
#import "LavuItemDecision.h"
#import "LavuOptionalModifierStateChangeProtocol.h"
#import "LavuForcedModifierSelectedProtocol.h"
#import "LavuQuantityChangedProtocol.h"


#define LAVUMAXOPTIONALPREVIEWCOUNT 2

#define LAVU_OPTIONS_PICKER_SECTION_FORCED_MODIFIERS 0
#define LAVU_OPTIONS_PICKER_SECTION_OPTIONAL_INCLUDED 1
#define LAVU_OPTIONS_PICKER_SECTION_OPTIONAL_OTHERS 2
#define LAVU_OPTIONS_PICKER_SECTION_QUANTITY 3
#define LAVU_OPTIONS_PICKER_SECTION_PRICING 4
#define LAVU_OPTIONS_PICKER_SECTION_CONFIRM 5

#define LAVU_OPTIONS_NAVIGATION_OPTIONAL_MODIFIER_CELL_IDENTIFIER "menu_option"
#define LAVU_OPTIONS_NAVIGATION_FORCED_MODIFIER_CELL_IDENTIFIER "forced_modifier_list"
#define LAVU_OPTIONS_NAVIGATION_LOAD_MORE_CELL_IDENTIFIER "load_more"
#define LAVU_OPTIONS_NAVIGATION_QUANTITY_CELL_IDENTIFIER "quantity"
#define LAVU_OPTIONS_NAVIGATION_PRICING_CELL_IDENTIFIER "pricing"
#define LAVU_OPTIONS_NAVIGATION_CONFIRM_CELL_IDENTIFIER "menu_options_confirm"
#define LAVU_OPTIONS_NAVIGATION_MODIFIER_LABEL_TAG 1
#define LAVU_OPTIONS_NAVIGATION_MODIFIER_SWITCH_TAG 2
#define LAVU_OPTIONS_NAVIGATION_PRICE_LABEL_TAG 2

#define LAVU_INGREDIENTS_TO_ORDER_SEGUE "goto_order"

@interface LavuOptionsPickerTableViewController : UITableViewController <LavuOptionalModifierStateChangeProtocol, LavuForcedModifierSelectedProtocol, LavuQuantityChangedProtocol>
@property (strong, nonatomic) LavuLocation* currentLocation;
@property (strong, nonatomic) LavuMenu* currentMenu;
@property (strong, nonatomic) LavuItemDecision* decisions;

@property (strong, nonatomic) LavuMenuItem* currentItem;

- (void) setCurrentItem:(LavuMenuItem *)currentItem andDecisions: (LavuItemDecision*) decisions;
@end
