//
//  LavuCurrencyAmount.m
//  APNTest
//
//  Created by Theodore Schnepper on 3/28/14.
//  Copyright (c) 2014 Theodore Schnepper. All rights reserved.
//

#import "LavuAmount.h"

@implementation LavuAmount
@synthesize baseUnit = _baseUnit;
@synthesize decimalPlaces = _decimalPlaces;

- (id) initWithbaseUnit: (int64_t) baseUnit decimalPlaces: (uint) decimalPlaces {
    self = [super init];
    if( self ){
        _baseUnit = baseUnit;
        _decimalPlaces = decimalPlaces;
    }
    return self;
}

- (id) initWithString: (NSString*) numberStr {
    NSArray* stringPieces = [numberStr componentsSeparatedByString: @"."];
    NSString* preDecimal = stringPieces[0];
        
    if( [stringPieces count] > 1 ){
        NSString* postDecimal = stringPieces[1];
        return [[self initWithbaseUnit: [[NSString stringWithFormat:@"%@%@", preDecimal, postDecimal] integerValue] decimalPlaces:(uint)[postDecimal length]] normalize];
    } else {
        return [self initWithbaseUnit: [preDecimal integerValue] decimalPlaces: 0];
    }
}

- (id) copy {
    return [[LavuAmount alloc] initWithbaseUnit: _baseUnit decimalPlaces: _decimalPlaces];
}

- (id) initWithFloat: (float) number {
    return [self initWithDouble: number];
}
- (id) initWithInteger: (NSInteger) number {
    return [self initWithbaseUnit: number decimalPlaces:0];
}

- (id) initWithDouble: (double) number {
    return [self initWithString: [NSString stringWithFormat:@"%F", number]];
}

- (LavuAmount*) convertDecimalPlaces:(uint)decimalPlaces {
    long int adjustment = -(((long int)_decimalPlaces) - ((long int)decimalPlaces));
    if( !adjustment ){
        return [self copy];
    }
    int64_t compound = 1;
    for( uint i = 0; i < (uint)adjustment; i++ ){
        compound *= 10;
    }
    
    uint64_t resultBaseUnit = 0;
    if( adjustment < 0 ){
        resultBaseUnit = _baseUnit / compound;
    } else {
        resultBaseUnit = _baseUnit * compound;
    }
    return [[LavuAmount alloc] initWithbaseUnit:resultBaseUnit decimalPlaces:decimalPlaces];
}

- (LavuAmount*) normalize {
    if( _baseUnit == 0 || _decimalPlaces == 0 ){
        return self;
    }
    int inc = 0;
    int64_t compound = 1;
    while( _baseUnit % compound == 0 ){
        compound *= 10;
        inc++;
    }
    
    return [self convertDecimalPlaces: _decimalPlaces - (inc-1)];
}

- (LavuAmount*) add: (LavuAmount*) amount {
    uint decimalPlaces = MAX( _decimalPlaces, [amount decimalPlaces] );
    LavuAmount* temp = [self copy];
    amount = [amount convertDecimalPlaces: decimalPlaces];
    temp = [temp convertDecimalPlaces: decimalPlaces];
    
    return [[[LavuAmount alloc] initWithbaseUnit: ([temp baseUnit] + [amount baseUnit]) decimalPlaces: decimalPlaces] normalize];
}
- (LavuAmount*) subtract: (LavuAmount*) amount {
    return [[self add: [[LavuAmount alloc] initWithbaseUnit:-[amount baseUnit] decimalPlaces: [amount decimalPlaces]]] normalize];
}
- (LavuAmount*) multiply: (LavuAmount*) amount {
    LavuAmount* temp = [self copy];
    uint decimalPlaces = [temp decimalPlaces] + [amount decimalPlaces];
    
    return [[[LavuAmount alloc] initWithbaseUnit:([temp baseUnit] * [amount baseUnit]) decimalPlaces: decimalPlaces] normalize];
}
- (LavuAmount*) divide: (LavuAmount*) amount {
    return self;
}

- (BOOL) isEqual:(id)object {
    if( !object ){
        return false;
    }
    
    if( ![object isKindOfClass: [LavuAmount class]] ){
        return false;
    }
    
    return [[self normalize] baseUnit] == [[object normalize] baseUnit];
}

- (long double) toLongDouble {
    return ((long double)_baseUnit)*powl(10.0f, -((long double)_decimalPlaces));

}
- (long int) toLongInt {
    uint compound = 1;
    for( uint i = 0; i < _decimalPlaces; i++ ){
        compound *= 10;
    }
    return (long int)(_baseUnit / compound );
}

@end
