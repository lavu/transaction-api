//
//  LavuOrderScreenTableViewController.h
//  APNTest
//
//  Created by Theodore Schnepper on 4/3/14.
//  Copyright (c) 2014 Theodore Schnepper. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LavuOrder.h"
#import "LavuOrderScreenItemTableViewCell.h"
#import "LavuItemModificationProtocol.h"
#import "LavuPrinterCommandSetKDSLavu.h"
#import "Communicator.h"

#define LAVU_ORDER_SCREEN_SECTION_HEADER 0
#define LAVU_ORDER_SCREEN_SECTION_ITEMS 1
#define LAVU_ORDER_SCREEN_SECTION_PRICING 2
#define LAVU_ORDER_SCREEN_SECTION_CONTROLS 3
#define LAVU_ORDER_SCREEN_SECTIONS_COUNT 4

#define LAVU_ORDER_SCREEN_PRICING_SECTION_ROW_SUBTOTAL 0
#define LAVU_ORDER_SCREEN_PRICING_SECTION_ROW_TAX 1
#define LAVU_ORDER_SCREEN_PRICING_SECTION_ROW_TOTAL 2
#define LAVU_ORDER_SCREEN_PRICING_SECTION_ROWS_COUNT 3

#define LAVU_ORDER_SCREEN_CONTROL_SECTION_ROW_ADD_MORE 0
#define LAVU_ORDER_SCREEN_CONTROL_SECTION_ROW_SEND 1
#define LAVU_ORDER_SCREEN_CONTROL_SECTION_ROWS_COUNT 2

#define LAVU_ORDER_SCREEN_HEADER_SECTION_ROWS_COUNT 0

#define LAVU_ORDER_SCREEN_NAVIGATION_PRICING_TITLE_LABEL_TAG 1
#define LAVU_ORDER_SCREEN_NAVIGATION_PRICING_AMOUNT_LABEL_TAG 2
#define LAVU_ORDER_SCREEN_NAVIGATION_CONTROL_TITLE_LABEL_TAG 1

@interface LavuOrderScreenTableViewController : UITableViewController <LavuItemModificationProtocol>
@property (retain, nonatomic) LavuOrder* order;
@property (retain, nonatomic) Communicator* comm;
@end
