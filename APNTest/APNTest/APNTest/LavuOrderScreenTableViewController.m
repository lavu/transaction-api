//
//  LavuOrderScreenTableViewController.m
//  APNTest
//
//  Created by Theodore Schnepper on 4/3/14.
//  Copyright (c) 2014 Theodore Schnepper. All rights reserved.
//

#import "LavuOrderScreenTableViewController.h"

@interface LavuOrderScreenTableViewController ()

@end

@implementation LavuOrderScreenTableViewController
@synthesize order = _order;
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
        _order = [LavuOrder currentOrder];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - setter
- (void) setOrder:(LavuOrder *)order {
    _order = order;
    [[self tableView] reloadData];
}

#pragma mark - Table view data source
- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [self tableView: tableView heightForRowAtIndexPath: indexPath];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch ( [indexPath section] ){
        case LAVU_ORDER_SCREEN_SECTION_ITEMS:{
            NSArray* items = [[[_order items] objectEnumerator] allObjects];
            CGRect sizing = [LavuOrderScreenItemTableViewCell calculateHeightForDecision:items[[indexPath row]] withLocation:[_order location] withWidth: [[self tableView] frame].size.width - 111.0f];
            sizing.size.height *= 1.1;
            return sizing.size.height + 50.0;
            
        } default: {
            return 50.0f;
        }
    }
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return LAVU_ORDER_SCREEN_SECTIONS_COUNT;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch( section ){
        case LAVU_ORDER_SCREEN_SECTION_HEADER:{
            return LAVU_ORDER_SCREEN_HEADER_SECTION_ROWS_COUNT;
        } case LAVU_ORDER_SCREEN_SECTION_ITEMS: {
            return [[_order items] count];
        } case LAVU_ORDER_SCREEN_SECTION_PRICING: {
            return LAVU_ORDER_SCREEN_PRICING_SECTION_ROWS_COUNT;
        } case LAVU_ORDER_SCREEN_SECTION_CONTROLS: {
            return LAVU_ORDER_SCREEN_CONTROL_SECTION_ROWS_COUNT;
        } default:{
            return 0;
        }
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger section = [indexPath section];
    switch( section ){
        case LAVU_ORDER_SCREEN_SECTION_HEADER:{
        } case LAVU_ORDER_SCREEN_SECTION_ITEMS: {
            LavuOrderScreenItemTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"order_item" forIndexPath:indexPath];
            NSArray* items = [[[_order items] objectEnumerator] allObjects];
            [cell setItemDecision: items[[indexPath row]] withLocation: [_order location]];
            [cell setDelegate: self];
            return cell;
        } case LAVU_ORDER_SCREEN_SECTION_PRICING: {
            UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"pricing" forIndexPath:indexPath];
            UILabel* pricingTitleLabel = (UILabel*)[cell viewWithTag: LAVU_ORDER_SCREEN_NAVIGATION_PRICING_TITLE_LABEL_TAG];
            UILabel* pricingAmountLabel = (UILabel*)[cell viewWithTag: LAVU_ORDER_SCREEN_NAVIGATION_PRICING_AMOUNT_LABEL_TAG];
            switch( [indexPath row] ){
                case LAVU_ORDER_SCREEN_PRICING_SECTION_ROW_SUBTOTAL:{
                    [pricingTitleLabel setText: @"Subtotal"];
                    [pricingAmountLabel setText: [[[_order location] formatter] stringForObjectValue: [_order calculateSubtotal]]];
                    break;
                } case LAVU_ORDER_SCREEN_PRICING_SECTION_ROW_TAX:{
                    [pricingTitleLabel setText: @"Tax"];
                    [pricingAmountLabel setText: [[[_order location] formatter] stringForObjectValue: [_order calculateTax]]];
                    break;
                } case LAVU_ORDER_SCREEN_PRICING_SECTION_ROW_TOTAL: {
                    [pricingTitleLabel setText: @"Total"];
                    [pricingAmountLabel setText: [[[_order location] formatter] stringForObjectValue: [_order calculateTotal]]];
                    break;
                } default: {
                    [pricingTitleLabel setText: @""];
                    break;
                }
            }
            return cell;
            
        } case LAVU_ORDER_SCREEN_SECTION_CONTROLS: {
            UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"control" forIndexPath:indexPath];
            UILabel* controlTitle = (UILabel*)[cell viewWithTag: LAVU_ORDER_SCREEN_NAVIGATION_CONTROL_TITLE_LABEL_TAG];
            switch( [indexPath row] ){
                case LAVU_ORDER_SCREEN_CONTROL_SECTION_ROW_ADD_MORE: {
                    [controlTitle setText: @"Add Another Item"];
                    break;
                } case LAVU_ORDER_SCREEN_CONTROL_SECTION_ROW_SEND: {
                    [controlTitle setText: @"Send Order"];
                    break;
                } default: {
                    [controlTitle setText: @""];
                    break;
                }
            }
            return cell;
        } default:{
            return nil;
        }
    }
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - LavuItemModificationProtocol

- (void) removeItemDecision: (LavuItemDecision*) itemDecision {
    NSArray* items = [[[_order items] objectEnumerator] allObjects];
    NSUInteger row = [items indexOfObject: itemDecision];
    
    [_order removeItem: itemDecision];
    const NSUInteger indexes[] = { LAVU_ORDER_SCREEN_SECTION_ITEMS, row };
    [[self tableView] deleteRowsAtIndexPaths:@[ [NSIndexPath indexPathWithIndexes:indexes length:2] ] withRowAnimation:UITableViewRowAnimationFade];
}
- (void) setCurrentItem:(LavuMenuItem *)currentItem andDecisions: (LavuItemDecision*) decisions {}
- (void) editItemDecision: (LavuItemDecision*) itemDecision {
    NSArray* controllers = [[self navigationController] viewControllers];
    id itemEditor = controllers[2];

    if( [itemEditor respondsToSelector: @selector(setCurrentItem:andDecisions:)]){
        [itemEditor setCurrentItem: [itemDecision item] andDecisions: itemDecision];
    }
    [[self navigationController] popToViewController:itemEditor animated:true];
//    [self performSegueWithIdentifier:@"edit_item" sender:self];
}
#pragma mark - Actions
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    switch( [indexPath section] ){
        case LAVU_ORDER_SCREEN_SECTION_CONTROLS:{
            switch( [indexPath row] ){
                case LAVU_ORDER_SCREEN_CONTROL_SECTION_ROW_ADD_MORE : {
                    NSArray* controllers = [[self navigationController] viewControllers];
                    
                    [[self navigationController] popToViewController:controllers[1] animated:true];
                    break;
                } case LAVU_ORDER_SCREEN_CONTROL_SECTION_ROW_SEND : {
                    NSArray* printers = [[_order location] printers];
                    [printers enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL* stop){
                        LavuPrinterBase* printer = (LavuPrinterBase*)obj;
                        NSMutableData* dataToSend = [[printer generatePrintData: _order] mutableCopy];
                        [dataToSend appendBytes:"\r\n" length:2];
                        self.comm = [[Communicator alloc] init];
                        self.comm->host = [NSString stringWithFormat:@"tcp://%@", [printer ipAddress]];
                        self.comm->port = (int)[[printer port] integerValue];
                        [self.comm setup];
                        
                        [self.comm writeOut: [dataToSend copy] closeAfterwards: true];
                    }];
                    
//                    [test close];
                    [[self navigationController] popToRootViewControllerAnimated: true];
                    break;
                } default: {
                    return;
                }
            }
        } default: {
            return;
        }
    }
}
@end
