//
//  LavuAppDelegate.h
//  APNTest
//
//  Created by Theodore Schnepper on 3/12/14.
//  Copyright (c) 2014 Theodore Schnepper. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Communicator.h"
#import "LavuAPNManager.h"
#import <CoreLocation/CoreLocation.h>
#import <CoreBluetooth/CoreBluetooth.h>

@interface LavuAppDelegate : UIResponder <UIApplicationDelegate, NSStreamDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
