#import <Foundation/Foundation.h>

@interface Communicator : NSObject <NSStreamDelegate> {
@public
	
	NSString *host;
	int port;
    BOOL closeAfterWrite;
@private
    CFReadStreamRef readStream;
    CFWriteStreamRef writeStream;
    
    NSInputStream *inputStream;
    NSOutputStream *outputStream;
    
    NSMutableArray *outputBuffer;
}

- (void)setup;
- (void)open;
- (void)close;
- (void)stream:(NSStream *)stream handleEvent:(NSStreamEvent)event;
- (void)readIn:(NSData *)s;
- (void)writeOut:(NSData *)s;
- (void)writeOut:(NSData *)s closeAfterwards: (BOOL) shouldClose;

@end