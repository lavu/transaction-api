//
//  LavuMenuCategory.h
//  APNTest
//
//  Created by Theodore Schnepper on 3/21/14.
//  Copyright (c) 2014 Theodore Schnepper. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LavuBase.h"

@interface LavuMenuCategory : LavuBase
@property (retain, nonatomic) NSNumber* ID;
@property (retain, nonatomic) NSNumber* menuGroupID;
@property (retain, nonatomic) NSString* name;
@property (retain, nonatomic) NSString* imagePath;
@property (retain, nonatomic) NSString* description;

@property (retain, nonatomic) NSDate* lastModified;
@property (retain, nonatomic) NSString* printerIdentifier;
@property (retain, nonatomic) NSNumber* modifierListID;
//@property (retain, nonatomic) NSString* applyTaxRate;
//@property (retain, nonatomic) NSString* customTaxRate;
@property (retain, nonatomic) NSNumber* forcedModifierGroupID;
@property (retain, nonatomic) NSNumber* forcedModifierListID;
@property (retain, nonatomic) NSNumber* printOrder;
@property (retain, nonatomic) NSNumber* superGroupID;
@property (retain, nonatomic) NSNumber* taxInclusion;
@property (retain ,nonatomic) NSNumber* lavutogoDisplay;
//@property (retain, nonatomic) NSString* happyHour;
@property (retain, nonatomic) NSNumber* taxProfileID;
@property (retain, nonatomic) NSNumber* chainReportingGroup;
@property (retain, nonatomic) NSNumber* priceTierProfileID;

+ (id) lavuMenuCategory: (NSDictionary*) details;
@end
