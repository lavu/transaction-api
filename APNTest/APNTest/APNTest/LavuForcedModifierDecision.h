//
//  LavuForcedModifierDecision.h
//  APNTest
//
//  Created by Theodore Schnepper on 3/28/14.
//  Copyright (c) 2014 Theodore Schnepper. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LavuForcedModifier.h"
#import "LavuForcedModifierList.h"
#import "LavuForcedModifierDecisionTreeRoot.h"
#import "LavuMenu.h"
@interface LavuForcedModifierDecision : LavuForcedModifierDecisionTreeRoot
@property (retain, nonatomic) LavuMenu* currentMenu;
@property (retain, nonatomic) LavuForcedModifier* forcedModifier;
@property (retain, nonatomic) LavuForcedModifierList* forcedModifierList;

- (id) initWithMenu: (LavuMenu*) currentMenu ForcedModifierList: (LavuForcedModifierList*) list;
- (id) initWithMenu: (LavuMenu*) currentMenu ForcedModifierList: (LavuForcedModifierList*) list AndForcedModifier: (LavuForcedModifier*) forcedModifier;

- (BOOL) hasDecision;
@end
