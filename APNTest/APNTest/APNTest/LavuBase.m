//
//  LavuBase.m
//  APNTest
//
//  Created by Theodore Schnepper on 3/21/14.
//  Copyright (c) 2014 Theodore Schnepper. All rights reserved.
//

#import "LavuBase.h"

@implementation LavuBase

- (NSNumber*) ID {
    @throw [NSException exceptionWithName:NSInternalInconsistencyException reason:[NSString stringWithFormat:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)] userInfo:nil];
}

- (BOOL) setValueUsing: (SEL) selector ifKeyExists:(NSString*) key usingDetails: (NSDictionary*) dict {
    if( ![[NSNull null] isEqual: dict[key]] && [self respondsToSelector: selector]){
        IMP imp = [self methodForSelector: selector];
        void (*func)(id, SEL, id) = (void*) imp;
        func(self, selector, dict[key]);
        return true;
    }
    return false;
}

- (BOOL) isEqual:(id)object {
    if( [object respondsToSelector: @selector(ID)] && [self respondsToSelector: @selector(ID)] ){
        return [[self ID] isEqual: [object ID]];
    }
    return self == object;
}
@end
