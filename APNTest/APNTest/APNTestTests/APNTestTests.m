//
//  APNTestTests.m
//  APNTestTests
//
//  Created by Theodore Schnepper on 3/12/14.
//  Copyright (c) 2014 Theodore Schnepper. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "LavuAmount.h"

@interface APNTestTests : XCTestCase

@end

@implementation APNTestTests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}
//    XCTFail(@"No implementation for \"%s\"", __PRETTY_FUNCTION__);
- (void)testCurrency
{
    {
        double value1 = 1.01;
        LavuAmount* test = [[LavuAmount alloc] initWithDouble: value1];
        NSLog(@"%@", test);

        if( [test toLongDouble] != value1 ){
                        XCTFail(@"Double Parsing Test Failed \"%s\"", __PRETTY_FUNCTION__);
        }
    }
    
    {
        double value1 = 0.33;
        double value2 = 0.02;
        double value3 = 0.35;
        
        LavuAmount* amount1 = [[LavuAmount alloc] initWithDouble: value1];
        LavuAmount* amount2 = [[LavuAmount alloc] initWithDouble: value2];
        LavuAmount* amount3 = [[LavuAmount alloc] initWithDouble: value3];
        
        if( ![[amount1 add: amount2] isEqual: amount3]){
            XCTFail(@"Double Addition Test Failed \"%s\"", __PRETTY_FUNCTION__);
        }
        
    }
    
    {
        double value1 = 0.33;
        double value2 = 0.02;
        double value3 = 0.0066;
//        double value4 = value1 * value2;
        
        LavuAmount* amount1 = [[LavuAmount alloc] initWithDouble: value1];
        LavuAmount* amount2 = [[LavuAmount alloc] initWithDouble: value2];
        LavuAmount* amount3 = [[LavuAmount alloc] initWithDouble: value3];
        
        if( ![[amount1 multiply: amount2] isEqual: amount3]){
            XCTFail(@"Double Multiplication Test Failed \"%s\"", __PRETTY_FUNCTION__);
        }
        
    }
    
    {
        double value1 = 0.09;
        double value2 = 0.015;
        double value3 = 0.00135;
//        double value4 = value1 * value2;
        
        LavuAmount* amount1 = [[LavuAmount alloc] initWithDouble: value1];
        LavuAmount* amount2 = [[LavuAmount alloc] initWithDouble: value2];
        LavuAmount* amount3 = [[LavuAmount alloc] initWithDouble: value3];
//        long double value5 = [[amount1 multiply: amount2] toLongDouble];
//        long double value6 = [amount3 toLongDouble];
        if( ![[amount1 multiply: amount2] isEqual: amount3]){
            XCTFail(@"Double Multiplication Test Failed \"%s\"", __PRETTY_FUNCTION__);
        }
        
    }
}

@end
