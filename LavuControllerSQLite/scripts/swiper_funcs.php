<?php
date_default_timezone_set("America/Denver");
error_reporting(E_ALL);
function getIDString($devID){
	$ip = getIPs();
	$ip_octets = explode('.',$ip);
	$padded_ip = '';
	foreach ($ip_octets as $octet) {
		$padded_ip = $padded_ip.str_pad($octet, 3,'0',STR_PAD_LEFT);
	}
	return $padded_ip.$devID;
}

function getIPs() {
	//preg_match_all('/inet'.($withV6 ? '6?' : '').' addr:?([^ ]+)/', `ifconfig`, $ips);
	//return $ips[1][0];
	$stuff = '/bin/sh /usr/local/share/lavu/scripts/get_my_ip.sh';
	exec($stuff,$execReturn);
	$hidrawList = array_unique($execReturn);
	if(empty($hidrawList)){
		error_log(__FUNCTION__."WTF BLANK IP");
		return rand(1111,9999);
	}else{
		return $hidrawList[0];
	}
}

function sendStatus($case,$device,$code,$Data){
	echo date('Y-m-d H:i:s')." Sending\n";
	$toSend = array();
	if(0 == $case){return;}
	$toSend['Case'] = $case;
	if($device != ''){$toSend['Device'] = $device;}
	if($code != ''){$toSend['Code'] = $code;}
	if($Data != ''){$toSend['Data'] = $Data;}
	$myCurl = curl_init();
	$default_address = 'localhost';
	//require_once(dirname(__FILE__)."/../web/lavu_controller/utils.php");
	//$netpath = getServer();
	//if( strstr($netpath,'lavu.com') || strstr($netpath,'lavutogo.com') ){
	//	$address = $default_address;
	//}else{
		$address = $netpath;
	//}
	$curl_addr = $address.'/local/lls_swipe/lls_swipe_posting.php';
	echo "$curl_addr \n";
	curl_setopt($myCurl, CURLOPT_URL, $curl_addr );
	curl_setopt($myCurl, CURLOPT_POST, 1);
	curl_setopt($myCurl, CURLOPT_POSTFIELDS, $toSend);
	curl_setopt($myCurl, CURLOPT_RETURNTRANSFER, 1);
	$response = curl_exec($myCurl);
	echo $response;
	return;
}

function sendMagStripe($inStripe,$myID){
	echo date('Y-m-d H:i:s')." Sending\n";
	sendStatus(4,$myID,3,$inStripe);
}

function setDeviceType($deviceType,$myID){
	sendStatus(15,$myID,$deviceType);
}
?>
