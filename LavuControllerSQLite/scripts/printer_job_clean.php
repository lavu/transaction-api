<?php
//
// PRINTERS
//
$PRINTERDB = new SQLite3(__DIR__.'/../db/.printers.sqlite');
$PRINTERDB->busyTimeout(2000);
$PRINTERDB->query('DELETE FROM `local_print_queue` WHERE `status` = "sent"');
$PRINTERDB->query('VACUUM');
$PRINTERDB->close();
?>