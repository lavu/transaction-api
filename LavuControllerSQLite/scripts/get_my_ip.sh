#! /bin/sh

/sbin/ifconfig | grep inet | awk -F':' '{print $2}' | awk '{print $1}' | grep -v '127.0.0.1'
