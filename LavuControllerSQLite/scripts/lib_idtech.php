<?php

if ( !function_exists( 'hex2bin' ) ) {
    function hex2bin( $str ) {
        $sbin = "";
        $len = strlen( $str );
        for ( $i = 0; $i < $len; $i += 2 ) {
            $sbin .= pack( "H*", substr( $str, $i, 2 ) );
        }
        return $sbin;
    }
}

class IDTechLLS
{
	public $StatusBits = '';
	public $Track1 = '';
	public $Track1Hash = '';
	public $Track2 = '';
	public $Track2Hash = '';
	public $TrackLengths = '';
	public $CryptoLength = '';
	public $Crypto = '';
	public $KSN = '';
	public $Footer = '';
	public $Remaining = '';

	public function printAll()
	{
		echo "Track Two: $this->Track2\n";
		echo "Crypto: $this->Crypto\n";
		echo "KSN: $this->KSN\n";
		$this->printContents($this->Remaining);
	}

	public function getEncryptedInfoBlock()
	{

		$encrypted_info = array();
		$encrypted_info['track2'] = $this->Crypto;
		$encrypted_info['ksn'] = $this->KSN;
		return $encrypted_info;
	}

	public function setMagData($inputMagData)
	{
		//error_log(__FILE__." ".__FUNCTION__." $inputMagData");
		//$this->setHexMagData( bin2hex($inputMagData) );
		$this->setHexMagData($inputMagData);
	}

	public function setHexMagData($inputMagData)
	{
		$inputMagData = strtoupper($inputMagData);
		if("000000000000000000" == substr($inputMagData,0,18)){
			$cardInfo = $this->badMacParse($inputMagData);
		}else{
			$cardInfo = $this->fastParse($inputMagData);
		}
		$this->Track1 = $cardInfo['Track1Masked'];
		$this->Track2 = $cardInfo['Track2Masked'] ;
		$this->Crypto = $cardInfo['Crypto'];
		$this->KSN = $cardInfo['KSN'];
	}

	public function GetMaskedCardNumber(){
		$maskedTrack2 = hex2bin($this->Track2);
		$theoRegex = '/;([0-9\*]{1,19})=([\d\*]{4}|=)([\d\*]{3}|=)([\d\*]*)?\?/';
		preg_match($theoRegex, $maskedTrack2, $matches);
		if(empty($matches)){
			error_log(__FILE__." ".__FUNCTION__." Could Not Parse Masked Track2");
		}
		if(empty($matches[1])){
			error_log(__FILE__." ".__FUNCTION__." Could Not Parse Masked Card Number");
		}
		$stuff = str_replace('*', '0', $matches[1]);
		return $stuff;
	}

	function fastParse($magStripe)
	{
		$cardInfo = array();
		$offset = 0;
		$cardInfo['CardTypeByte'] = substr($magStripe, 6,2);
		$cardInfo['Track1LenHex'] = substr($magStripe, 10,2);
		$cardInfo['Track2LenHex'] = substr($magStripe,12,2);
		if( 1 > hexdec($cardInfo['Track2LenHex']) )
		{
			error_log(__FILE__." ".__FUNCTION__." Could Not Get Track 2 (Required)");
			exit();
		}
		$cardInfo['Track3LenHex'] = substr($magStripe,14,2);
		$T1L = hexdec($cardInfo['Track1LenHex']);
		$T2L = hexdec($cardInfo['Track2LenHex']);
		$T3L = hexdec($cardInfo['Track3LenHex']);
		if(hexdec($cardInfo['CardTypeByte']) & hexdec('80')){
			$offset = 20;
			$cardInfo['Track1Masked'] = substr($magStripe,$offset,$T1L * 2);
			$offset += $T1L * 2;
			$cardInfo['Track2Masked'] = substr($magStripe,$offset,$T2L * 2);
			$offset += $T2L * 2;
			$offset += $T3L * 2;
			$offset += $this->roundUpToEights($T1L) * 2;
			$cardInfo['Crypto'] = substr($magStripe,$offset,$this->roundUpToEights($T2L) * 2);
			$offset += $this->roundUpToEights($T2L) * 2;
			$offset += $this->roundUpToEights($T3L) * 2;
		}else{
			error_log(__FILE__." ".__FUNCTION__."Something Bad Happened. $inputMagData");
		}

		if(0 < $T1L){
			$offset += 40;
		}
		$cardInfo['Track2Hash'] = substr($magStripe,$offset,40);
		$offset += 40;
		if(0 < $T3L){
			$offset += 40;
		}
		$cardInfo['KSN'] = substr($magStripe,$offset,20);
		return $cardInfo;
	}

	function roundUpToEights($int)
	{
		$var = $int % 8;
		if($var != 0)
		{
			$var = 8 - $var;
			$int += $var;
		}
		return $int;
	}

	function printContents($contents)
	{
		$myStringArray = str_split($contents,50);
		$output = '';
		foreach ($myStringArray as $value) {
			$value2 = preg_replace('/[\x00-\x1F\x80-\xFF]/', '.', $value);
			$output .= $value2."\n";
		}
		echo "------------------ Raw Contents ------------------\n".$output."\n";
		$output = '';
		foreach ($myStringArray as $value) {
			$output .= bin2hex($value)."\n";
		}
		echo "------------------ HEX Contents ------------------\n".$output."\n";
	}

	function badMacParse($magStripe)
	{
		//echo "$myID : badMacParse\n";
		$cardInfo = array();
		$temp = substr($magStripe, 18);
		$cardInfo['CardTypeByte'] = substr($temp, 0,2);
		$temp = substr($temp, 2);

		//
		// Getting Track 1
		//
		if( '25' == substr($temp,0,2) ){
			//echo hex2bin(substr($temp,0,120))."\n";
			$magLength = strlen($temp);
			for($i = 2;$i < ($magLength - 1);$i++){
				if('3F' == substr($temp,$i,2)){
					$cardInfo['Track1Masked'] = substr($temp,0,$i+2);
					$cardInfo['Track1Length'] = strlen($cardInfo['Track1Masked']) / 2;
					$temp = substr($temp,$i+2);
					//echo hex2bin($cardInfo['Track1Masked'])."\n";
					break;
				}
			}
		}

		//
		// Getting Track 2
		//
		if( '3B' == substr($temp,0,2) ){
			//echo hex2bin(substr($temp,0,120))."\n";
			$magLength = strlen($temp);
			for($i = 2;$i < ($magLength - 1);$i++){
				if('3F' == substr($temp,$i,2)){
					$cardInfo['Track2Masked'] = substr($temp,0,$i+2);
					$cardInfo['Track2Length'] = strlen($cardInfo['Track2Masked']) / 2;
					$temp = substr($temp,$i+2);
					//echo hex2bin($cardInfo['Track2Masked'])."\n";
					break;
				}
			}
		}

		if(isset($cardInfo['Track1Length'])){
			$cardInfo['Track1Crypto'] = substr($temp,0,$this->roundUpToEights($cardInfo['Track1Length']) * 2);
			$temp = substr($temp,$this->roundUpToEights($cardInfo['Track1Length']) * 2 );
		}
		if(isset($cardInfo['Track2Length'])){
			$cardInfo['Track2Crypto'] = substr($temp,0,$this->roundUpToEights($cardInfo['Track2Length']) * 2);
			$cardInfo['Crypto'] = $cardInfo['Track2Crypto'];
			$temp = substr($temp,$this->roundUpToEights($cardInfo['Track2Length']) * 2 );
		}
		if(isset($cardInfo['Track1Length'])){
			$cardInfo['Track1Hash'] = substr($temp,0,40);
			$temp = substr($temp,40);
		}
		if(isset($cardInfo['Track2Length'])){
			$cardInfo['Track2Hash'] = substr($temp,0,40);
			$temp = substr($temp,40);
		}
		$cardInfo['KSN'] = substr($temp,0,20);
		//var_export($cardInfo);
		//echo "\n\n";
		return $cardInfo;
	}

	function idtechParse2($magStripe){
		$test = substr($magStripe, 6,2);
		if(hexdec($test) & hexdec('80')){
			//error_log(__FILE__." ".__FUNCTION__." IDTECH ENHANCED");
			return idtechParse_E($magStripe);
		}else{
			//error_log(__FILE__." ".__FUNCTION__." IDTECH ORIGINAL");
			return idtechParse_O($magStripe);
		}
	}

	function idtechParse_O($magStripe){
		$cardInfo = array();
		$offset = 0;
		$cardInfo['STX'] = substr($magStripe, 0,2);
		$cardInfo['LENL'] = substr($magStripe, 2,2);
		$cardInfo['LENH'] = substr($magStripe,4,2);
		$cardInfo['Length'] = hexdec($cardInfo['LENH'].$cardInfo['LENL']);
		$cardInfo['Card Encoding Type'] = substr($magStripe,6,2);
		$cardInfo['Track Status'] = substr($magStripe,8,2);
		$cardInfo['Track 1 Unencrypted Length'] = hexdec(substr($magStripe,10,2));
		$cardInfo['Track 2 Unencrypted Length'] = hexdec(substr($magStripe,12,2));
		$cardInfo['Track 3 Unencrypted Length'] = hexdec(substr($magStripe,14,2));
		$offset = 16;
		if(0 < $cardInfo['Track 1 Unencrypted Length']){
			$cardInfo['Track 1 Masked'] = hex2bin(substr($magStripe,$offset,$cardInfo['Track 1 Unencrypted Length']*2));
			$cardInfo['Track1Masked'] = substr($magStripe,$offset,$cardInfo['Track 1 Unencrypted Length']*2);
			$offset += $cardInfo['Track 1 Unencrypted Length']*2;
		}
		if(0 < $cardInfo['Track 2 Unencrypted Length']){
			$cardInfo['Track 2 Masked'] = hex2bin(substr($magStripe,$offset,$cardInfo['Track 2 Unencrypted Length']*2));
			$cardInfo['Track2Masked'] = substr($magStripe,$offset,$cardInfo['Track 2 Unencrypted Length']*2);
			$offset += $cardInfo['Track 2 Unencrypted Length']*2;
		}
		if(0 < $cardInfo['Track 3 Unencrypted Length']){
			$cardInfo['Track 3 Masked'] = hex2bin(substr($magStripe,$offset,$cardInfo['Track 3 Unencrypted Length']*2));
			$cardInfo['Track3Masked'] = substr($magStripe,$offset,$cardInfo['Track 3 Unencrypted Length']*2);
			$offset += $cardInfo['Track 3 Unencrypted Length']*2;
		}
		if('00' == $cardInfo['Card Encoding Type']){
			$tempSize = $cardInfo['Track 1 Unencrypted Length'] + $cardInfo['Track 2 Unencrypted Length'] + $cardInfo['Track 3 Unencrypted Length'];
			$cryptoSize = $this->roundUpToEights($tempSize);
			$cardInfo['Encrypted Data'] = substr($magStripe,$offset,$cryptoSize*2);
			$offset += $cryptoSize*2;
			if(0 < $cardInfo['Track 1 Unencrypted Length']){
				$cardInfo['Track 1 Hash'] = substr($magStripe,$offset,40);
				$offset += 40;
			}
			if(0 < $cardInfo['Track 2 Unencrypted Length']){
				$cardInfo['Track 2 Hash'] = substr($magStripe,$offset,40);
				$offset += 40;
			}
			if(0 < $cardInfo['Track 3 Unencrypted Length']){
				$cardInfo['Track 3 Hash'] = substr($magStripe,$offset,40);
				$offset += 40;
			}
			$cardInfo['KSN'] = substr($magStripe,$offset,20);
			$offset += 20;
		}
		$cardInfo['CheckLRC'] = substr($magStripe,$offset,2);
		$offset += 2;
		$cardInfo['CheckSum'] = substr($magStripe,$offset,2);
		$offset += 2;
		$cardInfo['ETX'] = substr($magStripe,$offset,2);
		$cardInfo['Crypto'] = $cardInfo['Encrypted Data'];
		return $cardInfo;
	}

	function idtechParse_E($magStripe){
		$cardInfo = array();
		$offset = 0;
		$cardInfo['STX'] = substr($magStripe, 0,2);
		$cardInfo['LENL'] = substr($magStripe, 2,2);
		$cardInfo['LENH'] = substr($magStripe,4,2);
		$cardInfo['Length'] = hexdec($cardInfo['LENH'].$cardInfo['LENL']);
		$cardInfo['Card Encoding Type'] = substr($magStripe,6,2);
		$cardInfo['Track Status'] = substr($magStripe,8,2);
		$cardInfo['Track 1 Unencrypted Length'] = hexdec(substr($magStripe,10,2));
		$cardInfo['Track 2 Unencrypted Length'] = hexdec(substr($magStripe,12,2));
		$cardInfo['Track 3 Unencrypted Length'] = hexdec(substr($magStripe,14,2));
		$cardInfo['Mask Status'] = substr($magStripe,16,2);
		$cardInfo['Encrypted Status'] = substr($magStripe,18,2);
		$offset = 20;
		if(0 < $cardInfo['Track 1 Unencrypted Length']){
			$cardInfo['Track 1 Masked'] = hex2bin(substr($magStripe,$offset,$cardInfo['Track 1 Unencrypted Length']*2));
			$cardInfo['Track1Masked'] = substr($magStripe,$offset,$cardInfo['Track 1 Unencrypted Length']*2);
			$offset += $cardInfo['Track 1 Unencrypted Length']*2;
		}
		if(0 < $cardInfo['Track 2 Unencrypted Length']){
			$cardInfo['Track 2 Masked'] = hex2bin(substr($magStripe,$offset,$cardInfo['Track 2 Unencrypted Length']*2));
			$cardInfo['Track2Masked'] = substr($magStripe,$offset,$cardInfo['Track 1 Unencrypted Length']*2);
			$offset += $cardInfo['Track 2 Unencrypted Length']*2;
		}
		if(0 < $cardInfo['Track 3 Unencrypted Length']){
			$cardInfo['Track 3 Masked'] = hex2bin(substr($magStripe,$offset,$cardInfo['Track 3 Unencrypted Length']*2));
			$cardInfo['Track3Masked'] = substr($magStripe,$offset,$cardInfo['Track 1 Unencrypted Length']*2);
			$offset += $cardInfo['Track 3 Unencrypted Length']*2;
		}	
		if('80' == $cardInfo['Card Encoding Type']){
			if(0 < $cardInfo['Track 1 Unencrypted Length']){
				$cryptoSize = $this->roundUpToEights($cardInfo['Track 1 Unencrypted Length']);
				$cardInfo['Track 1 Encrypted'] = substr($magStripe,$offset,$cryptoSize*2);
				$offset += $cryptoSize*2;
				$cardInfo['Crypto'] = $cardInfo['Track 1 Encrypted'];
			}

			if(0 < $cardInfo['Track 2 Unencrypted Length']){
				$cryptoSize = $this->roundUpToEights($cardInfo['Track 2 Unencrypted Length']);
				$cardInfo['Track 2 Encrypted'] = substr($magStripe,$offset,$cryptoSize*2);
				$offset += $cryptoSize*2;
				$cardInfo['Crypto'] = $cardInfo['Track 2 Encrypted'];
			}

			if(0 < $cardInfo['Track 3 Unencrypted Length']){
				$cryptoSize = $this->roundUpToEights($cardInfo['Track 3 Unencrypted Length']);
				$cardInfo['Track 3 Encrypted'] = substr($magStripe,$offset,$cryptoSize*2);
				$offset += $cryptoSize*2;
			}

			if(0 < $cardInfo['Track 1 Unencrypted Length']){
				$cardInfo['Track 1 Hash'] = substr($magStripe,$offset,40);
				$offset += 40;
			}
			if(0 < $cardInfo['Track 2 Unencrypted Length']){
				$cardInfo['Track 2 Hash'] = substr($magStripe,$offset,40);
				$offset += 40;
			}
			if(0 < $cardInfo['Track 3 Unencrypted Length']){
				$cardInfo['Track 3 Hash'] = substr($magStripe,$offset,40);
				$offset += 40;
			}
		}
		$SerialOff = $offset;
		$cardInfo['Serial Number'] = substr($magStripe,$offset,20);
		$offset += 20;
		if('80' == $cardInfo['Card Encoding Type']){
			$cardInfo['KSN'] = substr($magStripe,$offset,20);
			$offset += 20;
		}
		$cardInfo['CheckLRC'] = substr($magStripe,$offset,2);
		$offset += 2;
		$cardInfo['CheckSum'] = substr($magStripe,$offset,2);
		$offset += 2;
		$cardInfo['ETX'] = substr($magStripe,$offset,2);
		if( ('00' == $cardInfo['CheckLRC']) && ('00' == $cardInfo['CheckSum']) && ('00' == $cardInfo['ETX']) ){
			$offset = $SerialOff;
			if('80' == $cardInfo['Card Encoding Type']){
				$cardInfo['KSN'] = substr($magStripe,$offset,20);
				$offset += 20;
			}
			$cardInfo['CheckLRC'] = substr($magStripe,$offset,2);
			$offset += 2;
			$cardInfo['CheckSum'] = substr($magStripe,$offset,2);
			$offset += 2;
			$cardInfo['ETX'] = substr($magStripe,$offset,2);
		}
		return $cardInfo;
	}


}

?>