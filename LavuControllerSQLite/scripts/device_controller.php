<?php
hidrawProcess();
hiddevProcess();
poledisplayProcess();
echo "\n\n";

function poledisplayProcess(){
	$stuff = 'dmesg | grep ttyUSB0';
	exec($stuff,$execReturn);
	$hidrawList = array_unique($execReturn);
	if(count($hidrawList) > 0) {
		if(file_exists('/dev/ttyUSB0')){
			$commandBuild = 'php '.__DIR__.'/pole_display.php';
			if(checkAndRelaunch($commandBuild)){
				echo " Good\n";
			}else{
				echo " Bad\n";
			}
		}else{
			echo "$entry : Does Not Exist\n";
		}
	}
}

function hidrawProcess(){
	$stuff = 'dmesg | grep hidraw | grep RFID| awk -F\':\' \'{print $4}\' |  awk -F\',\' \'{print $2}\'';
	exec($stuff,$execReturn);
	$hidrawList = array_unique($execReturn);
	foreach ($hidrawList as $entry) {
		if(file_exists('/dev/'.$entry)){
			echo "$entry : Exists\n";
			$idNumber = str_replace("hidraw","",$entry);
			$commandBuild = "php ".__DIR__."/rfid.php ".$idNumber;
			if(checkAndRelaunch($commandBuild)){
				echo " Good\n";
			}else{
				echo " Bad\n";
			}
		}else{
			echo "$entry : Does Not Exist\n";
		}
	}
}

function deviceTypeDetection(){
	$stuff = 'dmesg | grep hiddev | grep MSR215E ';
	exec($stuff,$execReturn);
	if(!empty($execReturn)){
		return "heartlandmsr";
	}
	$stuff = 'dmesg | grep hiddev | grep \'ID TECH\' ';
	exec($stuff,$execReturn);
	if(!empty($execReturn)){
		return "idtech";
	}
}

function hiddevProcess(){
	$devType = deviceTypeDetection();
	$stuff = 'ls /dev/usb | grep hiddev | awk \'{print $1}\'';
	exec($stuff,$execReturn);
	$hiddevList = array_unique($execReturn);
	foreach ($hiddevList as $entry) {
		if(file_exists('/dev/usb/'.$entry)){
			echo "$entry : Exists\n";
			$idNumber = str_replace("hiddev","",$entry);
			if('heartlandmsr' == $devType){
				$commandBuild = "php ".__DIR__."/swiper_heartland.php ".$idNumber;
			}elseif('idtech' == $devType){
				$commandBuild = "php ".__DIR__."/swiper_idtech.php ".$idNumber;
			}else{
				$commandBuild = "php ".__DIR__."/swiper.php ".$idNumber;	
			}
			
			if(checkAndRelaunch($commandBuild)){
				echo " Good\n";
			}else{
				echo " Bad\n";
			}
		}else{
			echo "$entry : Does Not Exist\n";
		}
	}
}

function checkAndRelaunch($command){
	echo "Command: $command";
	$execString  = "ps aux | grep \"";
	$execString .= $command;
	$execString .= "\" | grep -v grep";
	$execReturn	= array();
	exec($execString,$execReturn);
	if(empty($execReturn)){
		$execString  = "sh -c \"";
		$execString .= "$command";
		$execString .= "  >/dev/null 2>&1 &\" ";
		exec($execString);
		return false;
	}
	return true;
}

?>
