#! /bin/bash

if [ $1 -eq 1 ]; then
	TEST=$(ps aux | grep "printer_queues_manager.sh 0" | grep -v grep | wc -c)
	if [ $TEST -eq 0 ]; then
		echo "Restarting Queue Manager"
		nohup sh /usr/local/share/lavu/scripts/printer_queues_manager.sh 0 >/dev/null 2>&1 &
	fi
	exit
fi

COMMAND='php /usr/local/share/lavu/web/lavu_controller/print_queues.php'
$COMMAND listen 30 >/dev/null 2>&1 &
QUEUE01=$!

while [ true ]; do
	TEST=$(ps -p $QUEUE01 | grep $QUEUE01 | wc -c)
	if [ $TEST -eq 0 ]; then
		$COMMAND listen 30 >/dev/null 2>&1 &
		QUEUE01=$!
		echo "Restarting Queues"
	fi
	sleep 1
done
