<?php
//
// Lavu Controller: Socket Interface
//
require_once(__DIR__.'/../web/lavu_controller/utils.php');
$MyServer = new LavuControllerSocketServer();
$MyServer->listener();

class LavuControllerSocketServer {
	public $myPort = 6888;
	private $myDB = '';

	function __construct() {
		$this->myDB = getLocalDBSwipe();
	}

	public function listener() {
		$sock = socket_create_listen($this->myPort);
		if(false === $sock){die(__FILE__." ".__FUNCTION__." socket_create_listen failed");}
		while($connected = socket_accept($sock) ) {
			$this->buffer = '';
			socket_recv ( $connected , $this->buffer , 1024, null);
			if($this->isJson($this->buffer) ) {
				$this->jsonHandler($connected);
			}
			socket_close( $connected );
		}
	}

	//
	// Choose correct function based on sent JSON and return JSON encoded information
	//
	private function jsonHandler($conn) {
		if(empty($this->buffer['Lavu'])){return;}
		if(empty($this->buffer['Mode'])){return;}
		if(empty($this->buffer['DeviceType'])){return;}
		switch ($this->buffer['Mode']) {
			case 'SetStatus':
				if(empty($this->buffer['Status'])){return;}
				$this->setStatus($this->buffer['DeviceType'],$this->buffer['Status']);
				$retVal['Success'] = true;
				$toSend = json_encode($retVal);
			break;
			case 'SetData':
				if(empty($this->buffer['Data'])){return;}
				$this->setData($this->buffer['DeviceType'],$this->buffer['Data']);
				$retVal['Success'] = true;
				$toSend = json_encode($retVal);
			break;
			case 'GetStatusOrData':
				$retVal = $this->getData($this->buffer['DeviceType']);
				$toSend = json_encode($retVal);
			break;
			default:
				$retVal['Success'] = false;
				$toSend = json_encode($retVal);
				return;
			break;
		}
		return socket_send ( $conn , $toSend , strlen($toSend) , MSG_EOF );
	}

	private function setStatus($DeviceType,$NewStatus) {
		$statement = $myDB->prepare('REPLACE INTO `local_post_data_temp` (BoundDevice,DeviceKey,DataStatus) VALUES (:ident, :ident, :newStatus)');
		$statement->bindParam('ident',$DeviceType);
		$statement->bindParam(':newStatus',$NewStatus);
		return $statement->execute();
	}

	private function setData($DeviceType,$NewData) {
		$statement = $myDB->prepare('REPLACE INTO `local_post_data_temp` (BoundDevice,DeviceKey,DataStatus,PostData) VALUES (:ident, :ident, 3, :newData)');
		$statement->bindParam('ident',$DeviceType);
		$statement->bindParam(':newData',$NewStatus);
		return $statement->execute();
	}

	private function getData($DeviceType) {
		$tempArray = $this->getInfo($DeviceType,true);
		if(empty($tempArray) ) {
			$tempArray = array();
			$tempArray['DataStatus'] = 1;
		}
		return $tempArray;
	}

	private function getInfo($DeviceType,$timelocked = false) {
		if($timelocked){
			//Timelocked prevents old swipe data from being returned.
			$statement = $this->myDB->prepare('SELECT `DataStatus`,`DeviceType`,`PostData` FROM `local_post_data_temp` WHERE `DeviceType`=:ident AND `UpdateTime` > datetime("now","-3 seconds")');
		}else{
			$statement = $this->myDB->prepare('SELECT `DataStatus`,`DeviceType`,`PostData` FROM `local_post_data_temp` WHERE `DeviceType`=:ident');	
		}
		$statement->bindParam('ident',$DeviceType);
		$result = $statement->execute();
		$tempArray = $result->fetchArray(SQLITE3_ASSOC);
		$this->clearData($DeviceType);
		return $tempArray;
	}

	private function clearData($DeviceType) {
		$statement = $myDB->prepare('UPDATE `local_post_data_temp` SET `DataStatus` = 1 , `PostData` = "" WHERE `DeviceType`=:ident');
		$statement->bindParam('ident',$DeviceType);
		return $statement->execute();
	}

	private function isJson($string) {
		json_decode($string);
		return (json_last_error() == JSON_ERROR_NONE);
	}
}

?>
