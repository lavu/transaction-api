<?php
ini_set("display_errors", 1);
error_reporting(E_ALL);
$GLOBALS['LAVUPATH'] = '/usr/local/share/';
$RELEASETYPE='Release';
$SYSTEMTYPE='LC';
if(($argc > 1) && ($argv[1] == 'force')){
	ForceUpdate($SYSTEMTYPE,$RELEASETYPE);
}else{
	CheckForUpdate($SYSTEMTYPE,$RELEASETYPE);
}

function ForceUpdate($Device = '',$Type = 'Release'){
	$Server = GetCurrentVersionFromServer($Device,$Type);
	if(empty($Server)){
		die("Could Not Contact Version Server");
	}
	RunUpdater($Device,$Type,$Server);
}

function CheckForUpdate($Device = '',$Type = 'Release'){
	echo "CheckForUpdate\n";
	$Curr = GetCurrentVersion();
	echo "Current : $Curr \n";
	$Server = GetCurrentVersionFromServer($Device,$Type);
	echo "Latest : $Server \n";
	if(empty($Server)){return;}
	if($Server['Version'] <= $Curr){
		echo 'Up to date';
		return;
	}
	RunUpdater($Device,$Type,$Server);
}

function RunUpdater($Device = '',$Type = 'Release',$Server){
	echo "RunUpdater ".$Server['Filename']."\n";
	$fn = $Server['Filename'];
	GetFiles($fn);
	if(1 != CheckFile($fn)){
		RemoveFiles($fn);
		return;
	}

	GetFiles($fn.'.preinstall');
	if(1 != CheckFile($fn.'.preinstall')){
		RemoveFiles($fn);
		RemoveFiles($fn.'.preinstall');
		return;
	}

	GetFiles($fn.'.install');
	if(1 != CheckFile($fn.'.install')){
		RemoveFiles($fn);
		RemoveFiles($fn.'.preinstall');
		RemoveFiles($fn.'.install');
		return;
	}

	GetFiles($fn.'.postinstall');
	if(1 != CheckFile($fn.'.postinstall')){
		RemoveFiles($fn);
		RemoveFiles($fn.'.preinstall');
		RemoveFiles($fn.'.install');
		RemoveFiles($fn.'.postinstall');
		return;
	}
	RunPreInstall($fn);
	RunInstall($fn);
	RunPostInstall($fn);
	CleanupFiles($fn);
	KillSwipers();
}

function KillSwipers(){
	$command ="pgrep php";
	$outputArray = array();
	exec($command,$outputArray);
	$myPID = getmypid();
	foreach ($outputArray as $outputPID) {
		if($myPID != $outputPID){
			$killMe ="kill $outputPID";
			exec($killMe,$outputArray);
		}
	}
}

function RunPreInstall($fn){
	echo "RunPreInstall $fn\n";
	$fullfn = $GLOBALS['LAVUPATH'].'lavutemp/'.$fn;
	$command ="sh ".$fullfn.".preinstall";
	$outputArray = array();
	exec($command,$outputArray);
	if('Pre Install Successful' != end($outputArray)){
		echo "Pre Install Failure\n";
		print_r($outputArray);
		CleanupFiles($fn);
		die("Pre Install Failure\n");
	}
}

function RunInstall($fn){
	echo "RunInstall $fn\n";
	$fullfn = $GLOBALS['LAVUPATH'].'lavutemp/'.$fn;
	$command ="sh ".$fullfn.".install $fn";
	$outputArray = array();
	exec($command,$outputArray);
	if('Install Successful' != end($outputArray)){
		echo "Install Failure\n";
		print_r($outputArray);
		CleanupFiles($fn);
		die("Install Failure\n");
	}
}

function RunPostInstall($fn){
	echo "RunPostInstall $fn\n";
	$fullfn = $GLOBALS['LAVUPATH'].'lavutemp/'.$fn;
	$command ="sh ".$fullfn.".postinstall";
	$outputArray = array();
	exec($command,$outputArray);
	if('Post Install Successful' != end($outputArray)){
		echo "Post Install Failure\n";
		print_r($outputArray);
		CleanupFiles($fn);
		die("Post Install Failure\n");
	}
}

function CleanupFiles($fn){
	echo "CleanupFiles $fn\n";
	RemoveFiles($fn);
	RemoveFiles($fn.'.preinstall');
	RemoveFiles($fn.'.install');
	RemoveFiles($fn.'.postinstall');
}

function GetCurrentVersion(){
	echo "GetCurrentVersion\n";
	$arrayPost["CMD"] = 'version';
	$ch = curl_init();
	$version_url = "localhost/challenge.php";
	curl_setopt($ch, CURLOPT_URL, $version_url);
	curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
	curl_setopt($ch, CURLOPT_POSTFIELDS,  $arrayPost);
	curl_setopt($ch, CURLOPT_VERBOSE, false);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	$output = curl_exec($ch);
	$output = trim($output);
	if(empty($output)){
		return '0';
	}
	return $output;
}

function GetCurrentVersionFromServer($Device = '',$Type = 'Release'){
	echo "GetCurrentVersionFromServer\n";
	$ch = curl_init();
	$version_url = "https://admin.poslavu.com/private_api/lavu_controller/version.php";
	curl_setopt($ch, CURLOPT_URL, $version_url);
	$arrayPost = array();
	$arrayPost["Device"] = $Device;
	$arrayPost["Type"] = $Type;
	$arrayPost["Code"] = "ea6QANSk16YBfLHDTz46jqpL1W2Nya";
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
	curl_setopt($ch, CURLOPT_POSTFIELDS,  $arrayPost);
	curl_setopt($ch, CURLOPT_VERBOSE, false);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	$output = curl_exec($ch);
	$output = trim($output);
	echo $output."\n";
	if(empty($output)){
		return FALSE;
	}
	$retArray = json_decode($output,true);
	return $retArray;
}

function GetFiles($filename){
	echo "GetFiles $filename\n";
	$repo_url = 'https://admin.poslavu.com/lavu_controller_repo/';
	$download = fopen($repo_url.$filename,'r');
	$local_file = fopen($GLOBALS['LAVUPATH'].'lavutemp/'.$filename,'w');
	$len = stream_copy_to_stream($download,$local_file);
	fclose($download);
	fclose($local_file);

	$download = fopen($repo_url.$filename.'.hash','r');
	$local_file = fopen($GLOBALS['LAVUPATH'].'lavutemp/'.$filename.'.hash','w');
	$len = stream_copy_to_stream($download,$local_file);
	fclose($download);
	fclose($local_file);

	$download = fopen($repo_url.$filename.'.hash.sig','r');
	$local_file = fopen($GLOBALS['LAVUPATH'].'lavutemp/'.$filename.'.hash.sig','w');
	$len = stream_copy_to_stream($download,$local_file);
	fclose($download);
	fclose($local_file);
}

function RemoveFiles($filename){
	echo "RemoveFiles $filename\n";
	unlink($GLOBALS['LAVUPATH'].'lavutemp/'.$filename);
	unlink($GLOBALS['LAVUPATH'].'lavutemp/'.$filename.'.hash');
	unlink($GLOBALS['LAVUPATH'].'lavutemp/'.$filename.'.hash.sig');
}

function CheckFile($filename){
	echo "CheckFile $filename\n";
	if(empty($filename)){
		error_log("Empty Filename");
		return '-1';
	}
	$pubkey_file 	= fopen($GLOBALS['LAVUPATH'].'certs/poslavu.pub','r');
	$signature_file = fopen($GLOBALS['LAVUPATH'].'lavutemp/'.$filename.'.hash.sig','r');
	$hash_file 		= fopen($GLOBALS['LAVUPATH'].'lavutemp/'.$filename.'.hash','r');
	$pubkey 		= fread($pubkey_file,10000);
	$signed_text 	= fread($signature_file,10000);
	$hash_text 		= fread($hash_file,10000);
	if(1 != openssl_verify ( $hash_text , $signed_text , $pubkey)){
		return '-2';
	}
	$file_sha512_sum = hash_file('sha512',$GLOBALS['LAVUPATH'].'lavutemp/'.$filename);
	if($file_sha512_sum != $hash_text){
		return '-3';
	}
	return '1';
}

?>