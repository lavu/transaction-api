<?php
error_reporting(E_ALL);
date_default_timezone_set('America/Denver');
$foo = new PoleDisplay();
$foo->clearDisplay();
$foo->putString = 'Lavu Pole Display';
$foo->printString();
$foo->listener();


class PoleDisplay{
	private $displayHandle;
	private $emptyString;
	public $putString;
	public $count;

	function __construct(){
		$this->displayHandle = fopen('/dev/ttyUSB0','w');
		if(false === $this->displayHandle){
			trigger_error('Could Not Open Display');
		}
		$this->emptyString = "\r                                        ";
		$count = 0;
	}

	public function listener() {
		$sock = socket_create_listen(9142);
		if(false === $sock){die(__FILE__." ".__FUNCTION__." socket_create_listen failed");}
		while($connected = socket_accept($sock) ) {
			$this->clearDisplay();
			$this->putString = '';
			socket_recv ( $connected , $this->putString , 4096, null);
			socket_close( $connected );
			$this->printString();
		}
	}

	public function clearDisplay(){
		return fwrite($this->displayHandle,$this->emptyString);
	}

	public function testDisplay(){
		return fwrite($this->displayHandle,"\rabcdefghijklmnopqrstuvwxyz1234567890:?!@");
	}

	public function countDisplay(){
		$this->clearDisplay();
		$this->putString = "Count ".rand(0,1000)."\n{$this->count}";
		$this->count++;
		$this->printString();
	}

	public function dateDisplay(){
		$this->putString = date('c');
		$this->printString();
	}

	public function printString(){
		$this->padString();
		return fwrite($this->displayHandle,$this->putString);
	}

	public function padString(){
		$newlineCount = substr_count($this->putString, "\n");
		if($newlineCount > 0){
			$workingArray = explode("\n", $this->putString);
			foreach ($workingArray as $key => $value) {
				if(strlen($value) < 20){
					$workingArray[$key] = $value."\n\r";
				}else{
					$workingArray[$key] = substr($value, 0,20);
				}
			}
			$this->putString = $workingArray[0].$workingArray[1];
		}else{
			if(strlen($this->putString) < 40){
				if(strlen($this->putString) < 20){
					$this->putString = $this->putString."\n\n";
				}else{
					$this->putString = $this->putString."\n";
				}
			}else{
				$this->putString = substr($this->putString, 0,40);
			}
		}
		$this->putString = "\r".$this->putString;
	}
}

