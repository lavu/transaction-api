<?php
error_reporting(E_ALL);
//
// SWIPER DB STUFF
//
$SWIPERDB = new SQLite3(__DIR__.'/../db/.swipers.sqlite');
$SWIPERDB->busyTimeout(2000);
@$SWIPERDB->query('DROP TABLE "local_post_data_temp";');
$Query1 = <<<'SWIPEHEREDOC'
CREATE TABLE `local_post_data_temp` (
	`DeviceKey` INTEGER NOT NULL,
	`DataStatus` INTEGER DEFAULT NULL,
	`DeviceType` INTEGER DEFAULT 0,
	`PostData` BLOB DEFAULT '',
	`UpdateTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`BoundDevice` INTEGER DEFAULT NULL,
	PRIMARY KEY (`DeviceKey`)
);
SWIPEHEREDOC;
$SWIPERDB->query($Query1);
$SWIPERDB->close();
//
// END SWIPER DB STUFF
//

//
// SWIPER DB STUFF
//
$CONFIGDB = new SQLite3(__DIR__.'/../db/.config.sqlite');
$CONFIGDB->busyTimeout(2000);
@$CONFIGDB->query('DROP TABLE "config";');
$Query1 = <<<'CONFIGHEREDOC'
CREATE TABLE `config` (
        `ConfigKey` TEXT NOT NULL,
	`ConfigValue` TEXT NOT NULL,
        PRIMARY KEY (`ConfigKey`)
);
CONFIGHEREDOC;
$CONFIGDB->query($Query1);
$CONFIGDB->close();
//
// END SWIPER DB STUFF
//

//
// PRINTDB STUFF
//
$PRINTDB = new SQLite3(__DIR__.'/../db/.printers.sqlite');
@$PRINTDB->query('DROP TABLE "local_print_queue";');
@$PRINTDB->query('DROP TABLE "local_printer_status";');
@$PRINTDB->query('DROP TABLE "printer_types";');
$Query2 = <<<'QUEUEHEREDOC'
CREATE TABLE `local_print_queue` (
	`printer_ipaddress` varchar(255) NOT NULL DEFAULT '',
	`printer_port` varchar(255) NOT NULL DEFAULT '',
	`source_ipaddress` varchar(255) NOT NULL DEFAULT '',
	`printer_type` varchar(255) NOT NULL DEFAULT '',
	`datetime` varchar(255) NOT NULL DEFAULT '',
	`ts` varchar(255) NOT NULL DEFAULT '',
	`contents` longtext NOT NULL,
	`actions` longtext NOT NULL,
	`status` varchar(255) NOT NULL DEFAULT '',
	`sent_ts` varchar(255) NOT NULL DEFAULT '',
	`message` varchar(255) NOT NULL DEFAULT '',
	`udid` varchar(255) NOT NULL DEFAULT '',
	`designator` varchar(255) NOT NULL DEFAULT '',
	`printer_name` varchar(255) NOT NULL DEFAULT '',
	`image_capability` varchar(255) NOT NULL DEFAULT '',
	`alert_sent` varchar(255) NOT NULL DEFAULT '',
	`message_history` varchar(255) NOT NULL DEFAULT '',
	`order_id` varchar(255) NOT NULL DEFAULT '',
	`id` INTEGER PRIMARY KEY AUTOINCREMENT,
	`job_status` varchar(11) NOT NULL DEFAULT ''
);
QUEUEHEREDOC;
$PRINTDB->query($Query2);

$Query3 = <<<'STATUSHEREDOC'
CREATE TABLE `local_printer_status` (
	`ipaddress` varchar(255) UNIQUE NOT NULL DEFAULT '',
	`port` varchar(255) NOT NULL DEFAULT '',
	`last_print_ts` varchar(255) NOT NULL DEFAULT '',
	`last_status` varchar(255) NOT NULL DEFAULT '',
	`id` INTEGER PRIMARY KEY AUTOINCREMENT
);
STATUSHEREDOC;
$PRINTDB->query($Query3);

$Query4 = <<<'TYPEHEREDOC'
CREATE TABLE `printer_types` (
	`designator` varchar(255) PRIMARY KEY DEFAULT '',
	`printer_type` varchar(255) NOT NULL DEFAULT '',
	`printer_ipaddress` varchar(255) NOT NULL DEFAULT '',
	`printer_port` varchar(255) NOT NULL DEFAULT '',
	`image_capability` varchar(255) NOT NULL DEFAULT ''
);
TYPEHEREDOC;
$PRINTDB->query($Query4);
$PRINTDB->close();
//
// END PRINTDB STUFF
//


?>
