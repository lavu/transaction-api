<?php
require_once(__DIR__.'/swiper_funcs.php');
$devID = 0;
if( (!empty($argv[1])) && is_numeric($argv[1]) ){
	$devID = $argv[1];
}
$hiddev = "/dev/usb/hiddev".$devID;
$string = getIDString($devID).'1';
heartlandReader($hiddev,$string);

function heartlandReader($hiddev,$myID){
	$magReader = fopen($hiddev,"r");
	sendStatus('1',$myID,'0','');
	$readCounter = 0;
	$magStripe = '';
	while($myBytes = fread($magReader,8)){
		$myHex = strtoupper(bin2hex($myBytes));
		$head = substr($myHex,0,2);
		$data = substr($myHex,8,2);
		if('20' == $head){
			$readCounter = 0;
			$magStripe = '';
		}
		$magStripe .= $data;
		$readCounter++;
		if( 1714 == $readCounter){
			sendMagStripe($magStripe,$myID);
		}
	}
	sendStatus('6',$myID,'0','');
	echo "Removed device: $myID\n";
}
?>