<?php
//
// Swipers
//
$SWIPERDB = new SQLite3(__DIR__.'/../db/.swipers.sqlite');
$SWIPERDB->busyTimeout(2000);
$SWIPERDB->query('DELETE FROM `local_post_data_temp`');
$SWIPERDB->query('VACUUM');
$SWIPERDB->close();
//
// PRINTERS
//
$PRINTERDB = new SQLite3(__DIR__.'/../db/.printers.sqlite');
$PRINTERDB->busyTimeout(2000);
$PRINTERDB->query('DELETE FROM `local_print_queue`');
$PRINTERDB->query('VACUUM');
$PRINTERDB->close();
?>