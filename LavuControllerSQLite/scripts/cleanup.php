<?php
//
// Swipers
//
$SWIPERDB = new SQLite3(__DIR__.'/../db/.swipers.sqlite');
$SWIPERDB->busyTimeout(2000);
$SWIPERDB->query('DELETE FROM `local_post_data_temp` WHERE `BoundDevice` IS NULL');
$SWIPERDB->query('UPDATE `local_post_data_temp` SET `PostData` = "" AND DataStatus = 0 WHERE `UpdateTime` < datetime("now","-1 minutes")');
$SWIPERDB->query('VACUUM');
$SWIPERDB->close();
//
// PRINTERS
//
$PRINTERDB = new SQLite3(__DIR__.'/../db/.printers.sqlite');
$PRINTERDB->busyTimeout(2000);
$PRINTERDB->query('DELETE FROM `local_print_queue` WHERE `status` = "sent"');
$PRINTERDB->query('VACUUM');
$PRINTERDB->close();
?>