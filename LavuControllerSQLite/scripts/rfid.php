<?php
require_once(__DIR__.'/swiper_funcs.php');
error_reporting(E_ALL);
$devID = 0;
if( (!empty($argv[1])) && is_numeric($argv[1]) ){
	$devID = $argv[1];
}
$hiddev = "/dev/hidraw".$devID;
$string = getIDString($devID).'2';
rfidReader($hiddev,$string);

function rfidReader($hiddev,$myID){
	$magReader = fopen($hiddev,"r");
	sendStatus('1',$myID,'0','');
	setDeviceType(1,$myID);
	$readCounter = 0;
	$magStripe = '';
	while($myBytes = fread($magReader,8)){
		$myHex = strtoupper(bin2hex($myBytes));
		$head = substr($myHex,4,2);
		if('00' != $head){
			$magStripe .= rfidTranslate($head);
		}
		if('28' == $head){
			echo "Send: $magStripe \n";
			setDeviceType(1,$myID);
			sendMagStripe($magStripe,$myID);
			setDeviceType(1,$myID);
			$magStripe = '';
			$readCounter = 0;
		}
		if( 537 == $readCounter){
			$magStripe = '';
			$readCounter = 0;
		}
	}
	sendStatus('6',$myID,'0','');
	echo "Removed device: $myID\n";
}

function rfidTranslate($hex){
	if($hex == '1E'){return '1';}
	if($hex == '1F'){return '2';}
	if($hex == '20'){return '3';}
	if($hex == '21'){return '4';}
	if($hex == '22'){return '5';}
	if($hex == '23'){return '6';}
	if($hex == '24'){return '7';}
	if($hex == '25'){return '8';}
	if($hex == '26'){return '9';}
	if($hex == '27'){return '0';}
	//if($hex == '28'){return "\n";}
	return '';
}
?>