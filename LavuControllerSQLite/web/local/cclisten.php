<!doctype html>

<script src='\\ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js' type='text/javascript'></script>

<html>
	<head>
		<title>CC Retriever</title> <meta charset="utf-8" />
	</head>
	<body>
		<div id="Main">
			<div id="Border">
				<div id="Close"><img onclick="DoCommandCancel()" src="../images/lls_swiper/close_x.svg" width="100%" /></div>
				<div id="Header"><img src="../images/lls_swiper/one_moment.svg" /></div>
			</div>
			<div id="Status">No Javascript</div>
		</div>
		<div style='display:none'>
		</div>
	</body>
</html>

<script type="text/javascript">
	//Strings!
	var TryAgainButton = '<br><img onclick="AjaxGetPage()" src="../images/lls_swiper/try_again.svg"></img>';
	var PairStringHeader = '<img src="../images/lls_swiper/pairing_mode_text.svg"></img>';
	var PairString = '<img id="Smaller" src="../images/lls_swiper/step_1_swipe.svg" id="shrink"></img><br>';
	PairString += '<img onclick="Pairmode()" src="../images/lls_swiper/step_2_pair.svg"></img><br>';
	var NotPaired = '<img src="../images/lls_swiper/not_paired_text.svg"></img><br>';
	var PairingText = '<img src="../images/lls_swiper/waiting_text.svg"></img><br>';
	var DeclinedText = '<img src="../images/lls_swiper/Declined.svg"></img><br>';
	var waitForPairing = '<img src="../images/lls_swiper/waiting_pairing_text.svg"></img><br>';
	var PleaseSwipeHead = '<img src="../images/lls_swiper/waiting_text.svg"></img><br>';
	var PleaseSwipe = '<img src="../images/lls_swiper/please_swipe.svg"></img><br>';
	// End Strings!

	var postJSON = <?php echo json_encode($_REQUEST); ?>;

	document.getElementById("Header").innerHTML=PleaseSwipeHead;
	document.getElementById("Status").innerHTML=PleaseSwipe;

	setTimeout(function(){AjaxGetPage();},500);

	function AjaxStateResponder(responseText){
		document.getElementById("Status").innerHTML="Response";

		var myResponseText = responseText.trim();
		if("Not Paired" == myResponseText){
			document.getElementById("Header").innerHTML= PairStringHeader ;
			document.getElementById("Status").innerHTML= PairString;
			return;
		}
		if( ("" == myResponseText)||("Timeout Error" == myResponseText) ){
			document.getElementById("Header").innerHTML= "";
			document.getElementById("Status").innerHTML= TryAgainButton;
			return;
		}
		document.getElementById("Status").innerHTML="Success";

		var splitResponse = myResponseText.split("|");

		var approved = false;

		if( (3 < splitResponse.length) && (1 == splitResponse[0]) ){
			if(("APPROVAL" == splitResponse[3]) || ("Approved" == splitResponse[3])){
				CCResponseParse(splitResponse);
			}else{
				document.getElementById("Header").innerHTML= 'Card Declined';
				document.getElementById("Status").innerHTML= DeclinedText;
			}
			return;
		}
		if(0 == splitResponse[0]){
			document.getElementById("Header").innerHTML= "Bad Response";
			if("Duplicate Transaction" == splitResponse[1]){
				document.getElementById("Header").innerHTML= "Duplicate";
				return;
			}
		}
		document.getElementById("Status").innerHTML = "<textarea rows='15' cols='60'>" + responseText + "</textarea>"+TryAgainButton;
	}

	function CCResponseParse(splitResp){
		// To Be Checked AND/OR Fixed
		//RefNumber:o:Last4:o:Approved:o:AuthCode:o:CardType:o:none:o:none
		var info = splitResp[1]+":o:"+splitResp[2]+":o:"+"Approved"+":o:"+splitResp[4]+":o:"+splitResp[5]+":o:"+""+":o:"+"";
		document.location = "_DO:cmd=Approved&info="+info+":o:"+"";
	}

	function DoCommandCancel(){
		document.location = "_DO:cmd=Cancel";
	}

	function PairModeResponder(){
		if (this.readyState==4 && this.status==200){
			var myResponseText = this.responseText.trim();
			if("" == myResponseText){
				document.getElementById("Status").innerHTML= NotPaired+PairString;
				return;
			}
			document.getElementById("Status").innerHTML =  "<br>" + this.responseText + "<br><button class=\"DeviceButton\" onclick=\"Pairmode()\" >Refresh</button><br>";
		}
	}

	function PairResponder(){
		document.getElementById("Status").innerHTML="Working";
		if (this.readyState==4 && this.status==200){
			var myResponseText = this.responseText.trim();
			if( ("Fail" == myResponseText) || ("" == myResponseText) ){
				document.getElementById("Status").innerHTML='Error Trying Again';
				Pairmode();
				return;
			}
			if("Succeed" == myResponseText){
				document.getElementById("Status").innerHTML="Succeeded Pairing" + this.responseText;
				AjaxGetPage();
				return;
			}
		}
	}

	function Pairmode(){
		document.getElementById("Status").innerHTML=waitForPairing;
		var xmlhttp = new XMLHttpRequest();
		xmlhttp.onreadystatechange=PairModeResponder;
		xmlhttp.open("POST","/local/lls_swipe/lls_swipe_pairing.php",true);
		var myPostString = "UDID=" + postJSON.UDID;
		xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		xmlhttp.send(myPostString);
	}

	function Pair(mySwiper){
		document.getElementById("Status").innerHTML="<br>Clicked..";
		var xmlhttp = new XMLHttpRequest();
		xmlhttp.onreadystatechange=PairResponder;
		xmlhttp.open("POST","/local/lls_swipe/lls_swipe_posting.php",true);
		var myPostString = "Case=5&UDID=" + postJSON.UDID + "&Device="+mySwiper ;
		xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		xmlhttp.send(myPostString);

		document.getElementById("Header").innerHTML=PairStringHeader;
		document.getElementById("Status").innerHTML=PairingText;
	}

	function AjaxGetPage(){
		document.getElementById("Header").innerHTML=PleaseSwipeHead;
		document.getElementById("Status").innerHTML=PleaseSwipe;
		var myPost = $.post(
			"/local/lls_swipe/lls_swipe.php",
			postJSON
			);
		myPost.done(AjaxStateResponder);
		return;
	}
</script>

<style>
	html, body, div, span, applet, object, iframe,
	h1, h2, h3, h4, h5, h6, p, blockquote, pre,
	a, abbr, acronym, address, big, cite, code,
	del, dfn, em, font, img, ins, kbd, q, s, samp,
	small, strike, strong, sub, sup, tt, var,
	dl, dt, dd, ol, ul, li,
	fieldset, form, label, legend,
	table, caption, tbody, tfoot, thead, tr, th, td {
		margin: 0;
		padding: 0;
		border: 0;
		outline: 0;
		vertical-align: baseline;
	}

	.DeviceButton {
		font-family:Arial;
		font-size:24px;
		background-color:#aecd37;
		width: 80%;
	}

	#Divider{
		width:100%;
		z-index: -1;
	}

	#Close img{
		width:100%;
	}

	#Close{
		width:10%;
		display:inline-block;
	}

	#Header img{
		width:45%;
	}

	#Header{
		width:85%;
		text-align:right;
		display:inline-block
	}

	#Status img{
		width: 100%;
	}

	#Status img.Smaller{
		width: 60%;
	}

	#Status{
		font-family:Arial;
		font-size:24px;
		color:#999999;
		text-align:left;
	}

	#Main{
		width:100%;
		margin:0px auto;
		text-align:left;
		padding:0px;
	}

	#Border{
		border-bottom-style: solid;
		border-bottom-width: 4px;
		border-bottom-color:#eee;
	}

	body{
		margin:0px 0px; 
		padding:0px;
		width:100%;
		text-align:center;
	}
</style>
