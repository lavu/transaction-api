<?php
require_once(__DIR__."/utils.php");
error_reporting(E_ALL);

class Printers
{
	public $printerArray;

	public function Printers(){
		$this->getPrinters();
	}

	public function getPrinters(){
		$settings = readLocalSettings();
		if(empty($settings)){die("Failed To Read Settings");}
		$this->getPrintersFromRemote($settings['dataname'],$settings['database']);
		$temp = $this->getPrintersFromLocal();
		if(empty($temp)){
			error_log('No Printer Information');
			exit;
		}
		$this->printerArray = $temp;
	}

	public function updatePrintersFromRemote(){
		$settings = readLocalSettings();
		if(empty($settings)){die("0|Failed To Read Settings");}
		$this->getPrintersFromRemote($settings['dataname'],$settings['database']);
	}

	function getPrintersFromRemote($dataName,$databaseName){
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "http://admin.poslavu.com/private_api/pi_printer/printers.php");
		$arrayPost = array();
		$arrayPost["DATA_NAME"] = $dataName;
		$arrayPost["DB_NAME"] = $databaseName;
		$arrayPost["code"] = "ijH2aBBFn019R12zYVg0rzKqdSI4RExA3Hgr0E5KLW2mD50MTQ1o5p3lu6b8Gbe";
		if(!empty($_REQUEST['loc_id']) ){ $arrayPost["loc_id"] = $_REQUEST['loc_id' ];}
		error_log(var_export($arrayPost,1) );
		curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
		curl_setopt($ch, CURLOPT_POSTFIELDS,  $arrayPost);
		curl_setopt($ch, CURLOPT_VERBOSE, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$output = curl_exec($ch);
		error_log("Curl Response: $output");
		#curl_close($ch);
		$output = trim($output);
		if(empty($output)){
			error_log("CURL ERROR: ".curl_error($ch));
			error_log(__FUNCTION__." No printer information response from admin");
			return FALSE;
		}
		$temp = json_decode($output,true);
		$this->setLocalPrinters($temp);
		return $temp;
 	}

	function getPrintersFromLocal(){
		$myDB = getLocalDBPrint();
		if(empty($myDB)){return FALSE;}
		$result = $myDB->query('SELECT * from `printer_types`;');
		if(empty($result)){return;}
		$newPrinterArray = array();
		while($temp = $result->fetchArray(SQLITE3_ASSOC)){
			$newPrinterArray[] = $temp;
		}
		return $newPrinterArray;
	}

	function setLocalPrinters($input){
		$myDB = getLocalDBPrint();
		if(empty($myDB)){return;}
		if(empty($input)){return;}
		$myDB->query('DELETE FROM `printer_types`;');
		foreach ($input as $printer) {
			if( empty($printer['value6']) || empty($printer['setting']) ){
				error_log('Recieved Bad Information from Remote DB');
			}else{
				$statement = $myDB->prepare('
					REPLACE INTO `printer_types`
					VALUES (:setting,:value6,:value3,:value5,:value10);
				');
				$statement->bindParam('setting',	$printer['setting']	);
				$statement->bindParam('value6',		$printer['value6']	);
				$statement->bindParam('value3',		$printer['value3']	);
				$statement->bindParam('value5',		$printer['value5']	);
				$statement->bindParam('value10',	$printer['value10']	);
				$statement->execute();
				$statement->close();
			}
		}
	}

	function resetLocalPrintJobs(){
		$myDB = getLocalDBPrint();
		if(empty($myDB)){return;}
		$myDB->query('DELETE FROM `local_print_queue`');
	}
}

?>