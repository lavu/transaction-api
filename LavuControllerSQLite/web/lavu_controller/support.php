<script src='//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js' type='text/javascript'></script>
<html>
<head>
	<title>Lavu Controller Support Page</title>
	<style>
	/* "auto" makes the left and right margins center the wrapper */
		#wrap{
			width: 900px;
		    margin-left: auto;
    		margin-right: auto;
		}

		table,th, td{
			border:1px solid black;
			border-collapse:collapse;
			padding:5px;
		}

		button{
			font-family:Arial;
			background-color:#aecd37;
			//width: 100%;
		}
	</style>
</head>
<body>
<div id="wrap">

<?php
	ini_set('display_errors','1');
	error_reporting(E_ALL);
	require_once(__DIR__."/utils.php");

	printSwiperTable();
	printPrinterTable();
	echo "<a href='/PrinterConfigurationPage/'>Printer / Network Configuration</a>";

	function getPostInfo(){
		$myDB = getLocalDBSwipe();
		$statement = $myDB->prepare('SELECT DeviceKey,DeviceKey,BoundDevice,UpdateTime,DeviceType FROM `local_post_data_temp` ');
		$result = $statement->execute();
		$resultArray = array();
		while($tempArray = $result->fetchArray(SQLITE3_ASSOC)){
			$resultArray[] = $tempArray;
		}
		$statement->close();
		return($resultArray);
	}

	function getPrintInfo(){
		$myDB = getLocalDBPrint();
		$statement = $myDB->prepare('SELECT * FROM `local_print_queue` ');
		$result = $statement->execute();
		$resultArray = array();
		while($tempArray = $result->fetchArray(SQLITE3_ASSOC)){
			$resultArray[] = $tempArray;
		}
		$statement->close();
		return($resultArray);
	}

	function getPrinterInfo(){
		$myDB = getLocalDBPrint();
		$statement = $myDB->prepare('SELECT * from `printer_types`');
		$result = $statement->execute();
		$resultArray = array();
		while($tempArray = $result->fetchArray(SQLITE3_ASSOC)){
			$resultArray[] = $tempArray;
		}
		$statement->close();
		return($resultArray);
	}

	function printSwiperTable(){
		echo "<table>";
		echo "<tr>";
		echo "<td>Device ID</td>";
		echo "<td>Device Type</td>";
		echo "<td>Bound iPad</td>";
		echo "<td>Last Updated</td>";
		echo "<td><button type='button' onclick='UnbindAll()'>Unbind All</button></td>";
		echo "<td><button type='button' onclick='RemoveAll()'>Remove All</button></td>";
		echo "</tr>";
		$info =  getPostInfo();
		foreach ($info as $device) {
			echo "<tr>";
			echo "<td>".$device["DeviceKey"]."</td>";
			echo "<td>".$device["DeviceType"]."</td>";
			echo "<td>".$device["BoundDevice"]."</td>";
			echo "<td>".$device["UpdateTime"]."</td>";
			if(empty($device["BoundDevice"])){
				echo "<td></td>";
			}else{
				echo "<td><button type='button' onclick='Unbind(".$device["DeviceKey"].")'>Unbind Device</button></td>";
			}
			echo "<td><button type='button' onclick='Remove(".$device["DeviceKey"].")'>Remove Device</button></td>";
			echo "</tr>";
		}
		echo "</table><br>";
	}

	function printPrinterTable(){
		echo "<table>";
		echo "<tr>";
		echo "<td>Printer Name</td>";
		echo "<td>IP</td>";
		echo "<td>Port</td>";
		echo "<td>Printer Type</td>";
		echo "<td>Imaging Type</td>";
		echo "</tr>";
		$info =  getPrinterInfo();
		foreach ($info as $device) {
			echo "<tr>";
			echo "<td>".$device["designator"]."</td>";
			echo "<td>".$device["printer_ipaddress"]."</td>";
			echo "<td>".$device["printer_port"]."</td>";
			echo "<td>".$device["printer_type"]."</td>";
			echo "<td>".$device["image_capability"]."</td>";
			echo "</tr>";
		}
		echo "<tr><td><button type='button' onclick='RefreshPrinters()'>Reload Printer Config</button></td></tr>";
		echo "</table>";
	}

	echo '<pre>';
	//print_r(getPrintInfo());
	echo '</pre>';

?>


</div>
</body>
</html>

<script type="text/javascript">

function UnbindAll()
{
	Posting(8,0);
}

function RemoveAll()
{
	Posting(7,0);
}

function Unbind(DeviceKey)
{
	Posting(6,DeviceKey);
}

function Remove(DeviceKey)
{
	alert("Remove: Not Implimented");
}

function RefreshPrinters()
{
	postOBJ = {};
        postOBJ.CMD = 'configure';
        var myPost = $.post("/challenge.php",postOBJ);
        myPost.done(PostResponder);
        return;
}

function Posting(Case,DeviceKey)
{
	var postOBJ = {};
	postOBJ.Case = Case;
	if(DeviceKey != 0){postOBJ.Device = DeviceKey;}
	var myPost = $.post("/local/lls_swipe/lls_swipe_posting.php",postOBJ);
	myPost.done(PostResponder);
	return;
}

function PostResponder(responseText)
{
	var myResponseText = responseText.trim();
	alert("Command Executed. Response from server:"+responseText+" ");
	return;
}

</script>

