<?php
require_once(__DIR__."/utils.php");
error_reporting(E_ALL);

function postUtils(){
	$fooBar = new SwiperFunctionality;
	$fooBar->postUtils();
}

class SwiperFunctionality{
	private $myDB;

	function __construct(){
		$this->myDB = getLocalDBSwipe();
	}

	function postUtils(){
		// Effective Main for swipe functions
		// Switch Statement using a post variable to call one of the available functions
		if(empty($_REQUEST["Case"]) ){return;}	
		if(empty($this->myDB) ){return;}
		$myCase = $_REQUEST["Case"];
		$myFunctions = array();
		$myFunctions[1]  = 'setPostStatus';
		$myFunctions[2]  = 'getPostStatus';
		//$myFunctions[3]  = 'getPostData';//Depricated
		$myFunctions[4]  = 'setPostData';
		$myFunctions[5]  = 'setPairing';
		$myFunctions[6]  = 'unPair';
		$myFunctions[7]  = 'wipeDB';
		$myFunctions[8]  = 'unPairAll';
		$myFunctions[9]  = 'isPaired';
		$myFunctions[10] = 'isPaired';
		$myFunctions[11] = 'cleanupDB';
		$myFunctions[12] = 'jsonPairingList';
		//$myFunctions[13] = 'jsonGetPost'; //Depricated
		$myFunctions[15] = 'setDeviceType';
		$myFunctions[16] = 'jsonGetDevicesInfo';
		if(empty($myFunctions[$myCase]) ){return;}
		echo $this->$myFunctions[$myCase]();
		$this->myDB->close();
	}
	//
	// Mode 1
	//
	public function setPostStatus($newStatus = 0){
		if(isset($_REQUEST["Code"] ) ) {
			$newStatus = $_REQUEST["Code"];
		}
		if(isset($_REQUEST["Device"] ) ) {
			if(!$this->DeviceKeyExists($_REQUEST["Device"])){
				$this->CreateDeviceKey($_REQUEST["Device"]);
			}
			return $this->UpdatePostStatus($_REQUEST["Device"],$newStatus);
		}
		return 1;
	}
	//
	// Mode 2
	//
	public function getPostStatus(){
		if(!empty($_REQUEST["pair_mode"]) ) {
			if(isset($_REQUEST["device_type"]) ) {
				return $this->getPostStatus_DEVICETYPE($_REQUEST["device_type"]);
			}
		}
		if(!empty($_REQUEST["send_data"]) ) {
			return $this->getPostStatus_SENDDATA($_REQUEST["UDID"]);
		}
		return $this->getPostStatusFromUDID($_REQUEST["UDID"]);
	}

	private function getPostStatusFromUDID($UDID){
		$ident = $this->UdidToIdentifier($UDID);
		if(false === $ident){return 0;}
		$statement = $this->myDB->prepare('SELECT `DataStatus` FROM `local_post_data_temp` WHERE `BoundDevice` = :boundDev');
		$statement->bindParam('boundDev',$ident);
		$result = $statement->execute();
		$tempArray = $result->fetchArray(SQLITE3_ASSOC);
		$result->finalize();
		$statement->close();
		if(empty($tempArray)){return 0;}
		return $tempArray['DataStatus'];
	}

	private function getPostStatus_SENDDATA($UDID){
		$status = $this->getPostStatusFromUDID($UDID);
		if($status != 3){return $status;}
		$ident = $this->UdidToIdentifier($UDID);
		if(false === $ident){return 0;}
		$statement = $this->myDB->prepare('SELECT `DataStatus`,`PostData`,`DeviceKey` FROM `local_post_data_temp` WHERE `BoundDevice`=:boundDev AND `DataStatus` = 3 AND `UpdateTime` > datetime("now","-3 seconds")');
		$statement->bindParam('boundDev',$ident);
		$result = $statement->execute();
		$tempArray = $result->fetchArray(SQLITE3_ASSOC);
		$result->finalize();
		if(empty($tempArray)){return 0;}
		$jsonArray = array();
		$jsonArray['status'] = $tempArray['DataStatus'];
		$jsonArray['type'] = $DeviceType;
		$jsonArray['data'] = $tempArray['PostData'];
		$statement->close();
		$this->UpdatePostStatus($tempArray['DeviceKey'],1); // Clear Data that has been retrieved
		return json_encode($jsonArray);	
	}

	private function getPostStatus_DEVICETYPE($DeviceType){
		$status = $this->getPostStatusFromDeviceType($DeviceType);
		if($status != 3 ){return $status;}
		return $this->getPostDataFromDeviceType($DeviceType);
	}

	private function getPostStatusFromDeviceType($DeviceType){
		$temp = $this->DeviceTypeExists($DeviceType);
		if(empty( $temp ) ) {return 0;}
		$statement = $this->myDB->prepare('SELECT MAX(`DataStatus`) as `MaxDataStatus` FROM `local_post_data_temp` WHERE `DeviceType` = :devKey');
		$statement->bindParam('devKey',$DeviceType);
		$result = $statement->execute();
		$tempArray = $result->fetchArray(SQLITE3_ASSOC);
		$result->finalize();
		$statement->close();
		if(empty($tempArray)){return 0;}
		return $tempArray['MaxDataStatus'];
	}

	private function getPostDataFromDeviceType($DeviceType){
		$temp = $this->DeviceTypeExists($DeviceType);
		if(empty( $temp ) ) {return 0;}
		$statement = $this->myDB->prepare('SELECT `DataStatus`,`PostData`,`DeviceKey` FROM `local_post_data_temp` WHERE `DeviceType`=:devKey AND `DataStatus` = 3 AND `UpdateTime` > datetime("now","-3 seconds")');
		$statement->bindParam('devKey',$DeviceType);
		$result = $statement->execute();
		$tempArray = $result->fetchArray(SQLITE3_ASSOC);
		$result->finalize();
		if(empty($tempArray) ){
			return 1;
		}
		$jsonArray = array();
		$jsonArray['status'] = $tempArray['DataStatus'];
		$jsonArray['type'] = $DeviceType;
		$jsonArray['data'] = $tempArray['PostData'];
		$statement->close();
		$this->UpdatePostStatus($tempArray['DeviceKey'],1); // Clear Data that has been retrieved
		return json_encode($jsonArray);
	}
	//
	// Mode 4 : Set Post Data
	//
	function setPostData($myDB){
		if( ($_REQUEST["Data"] === NULL) || ($_REQUEST["Device"] === NULL) ){return 0;}
		if(!$this->DeviceKeyExists($_REQUEST["Device"])){
				$this->CreateDeviceKey($_REQUEST["Device"]);
		}
		$this->UpdatePostData($_REQUEST["Device"],$_REQUEST["Data"]);
		return "Posted\n";
	}
	//
	// Mode 5
	//
	public function setPairing(){
		$ident = $this->UdidToIdentifier($_REQUEST["UDID"]);
		$devKey = $_REQUEST["Device"];
		if(($ident === NULL) || ($devKey === NULL) || ($ident === false) ){return;}
		$statement = $this->myDB->prepare('UPDATE `local_post_data_temp` SET `BoundDevice` = :ident , `UpdateTime` = CURRENT_TIMESTAMP WHERE `DeviceKey` = :devKey');
		$statement->bindParam('devKey',$devKey);
		$statement->bindParam('ident',$ident);
		$statement->execute();
		$statement->close();
		return "Succeed";
	}
	//
	// Mode 6 unPair
	//
	public function unPair(){
		$DeviceKey = $_REQUEST['Device'];
		if(empty($DeviceKey) && $DeviceKey !== 0){return;}
		$statement = '';
		$statement = $this->myDB->prepare('UPDATE `local_post_data_temp` SET `BoundDevice` = NULL , `UpdateTime` = CURRENT_TIMESTAMP WHERE `DeviceKey` = :devKey');
		$statement->bindParam('devKey',$DeviceKey);
		$statement->execute();
		$statement->close();
		return 1;
	}
	//
	// Mode 7 wipeDB
	//
	public function wipeDB(){
		$this->myDB->query('DELETE FROM `local_post_data_temp`');
		$this->myDB->query('VACUUM');
	}
	//
	// Mode 8 unPairAll
	//
	public function unPairAll(){
		$this->myDB->query('UPDATE `local_post_data_temp` SET `BoundDevice` = NULL , `UpdateTime` = CURRENT_TIMESTAMP');
		return "Success";
	}
	//
	// Mode 9 isPaired
	// Mode 10 isPaired
	//
	public function isPaired(){
		if(!empty($_REQUEST["pair_mode"]) ){return 1;}
		if(empty($_REQUEST["UDID"]) ){return 0;}
		$ident = $this->UdidToIdentifier($_REQUEST["UDID"]);
		if($ident === false){return 0;}
		$statement = $this->myDB->prepare('SELECT DeviceKey FROM `local_post_data_temp` WHERE `BoundDevice`=:ident');
		$statement->bindParam('ident',$ident);
		$result = $statement->execute();
		$tempArray = $result->fetchArray(SQLITE3_NUM);
		$result->finalize();
		$statement->close();
		if(!empty($tempArray)){return 1;}
		return 0;
	}
	//
	// Mode 11 cleanupDB
	//
	public function cleanupDB(){
		$this->myDB->query('DELETE FROM `local_post_data_temp` WHERE `BoundDevice` IS NULL');
		$this->myDB->query('UPDATE `local_post_data_temp` SET `PostData` = "" AND DataStatus = 0');
	}
	//
	// Mode 12 jsonPairingList
	//
	public function jsonPairingList(){
		$statement = $this->myDB->prepare('SELECT `DeviceKey`,`PostData` FROM `local_post_data_temp` WHERE (`BoundDevice` IS NULL AND `DataStatus` = 3 ) ');
		$returnArray = array();
		$result = $statement->execute();
		while($tempArray = $result->fetchArray(SQLITE3_NUM)){
			$returnArray[] = $tempArray;
		}
		$result->finalize();
		$statement->close();
		return json_encode($returnArray);
	}
	//
	// Mode 15 setDeviceType
	//
	public function setDeviceType(){
		if(!isset($_REQUEST['Device']) ){return 0;}
		$DeviceKey = $_REQUEST['Device'];
		if(!isset($_REQUEST['Code']) ){return 0;}
		$NewDeviceType = $_REQUEST["Code"];
		$this->UpdateDeviceType($DeviceKey,$NewDeviceType);
	}
	//
	// Mode 16
	//
	public function jsonGetDevicesInfo(){
		$statement = $this->myDB->prepare('SELECT `DeviceKey`,`DataStatus`,`DeviceType`,LENGTH(`PostData`),`UpdateTime`,`BoundDevice` FROM `local_post_data_temp`');
		$returnArray = array();
		$result = $statement->execute();
		while($tempArray = $result->fetchArray(SQLITE3_ASSOC)){
			$returnArray[] = $tempArray;
		}
		$result->finalize();
		$statement->close();
		return json_encode($returnArray);
	}
	//
	// Internal Methods
	//
	private function CreateDeviceKey($DeviceKey){
		$statement = $this->myDB->prepare('INSERT INTO `local_post_data_temp` (`DeviceKey`,`DataStatus`,`DeviceType`,`PostData`) VALUES (:devKey,1,0,NULL)');
		$statement->bindParam('devKey',$DeviceKey);
		$statement->execute();
		$statement->close();
		return 1;
	}

	private function UpdatePostStatus($DeviceKey,$newStatus){
		$statement = $this->myDB->prepare('UPDATE `local_post_data_temp` SET `DataStatus` = :newData , `UpdateTime` = CURRENT_TIMESTAMP WHERE `DeviceKey` = :devKey');
		$statement->bindParam('newData',$newStatus);
		$statement->bindParam('devKey',$DeviceKey);
		$statement->execute();
		$statement->close();
		return 1;
	}

	private function UpdatePostData($DeviceKey,$newData){
		$statement = $this->myDB->prepare('UPDATE `local_post_data_temp` SET `PostData` = :newData , `DataStatus` = "3" , `UpdateTime` = CURRENT_TIMESTAMP WHERE `DeviceKey` = :devKey');
		$statement->bindParam('newData',$newData);
		$statement->bindParam('devKey',$DeviceKey);
		$statement->execute();
		$statement->close();
		return 1;
	}

	private function UpdateDeviceType($DeviceKey,$newData){
		$statement = $this->myDB->prepare('UPDATE `local_post_data_temp` SET `DeviceType` = :newData , `UpdateTime` = CURRENT_TIMESTAMP WHERE `DeviceKey` = :devKey');
		$statement->bindParam('newData',$newData);
		$statement->bindParam('devKey',$DeviceKey);
		$statement->execute();
		$statement->close();
		return 1;
	}

	private function DeviceKeyExists($DeviceKey){
		$statement = $this->myDB->prepare('SELECT COUNT(*) FROM  `local_post_data_temp` WHERE `DeviceKey` = :devKey');
		$statement->bindParam('devKey',$DeviceKey);
		$result = $statement->execute();
		$tempArray = $result->fetchArray(SQLITE3_NUM);
		$result->finalize();
		$statement->close();
		if(empty($tempArray) ){return false;}
		return $tempArray[0];
	}

	private function DeviceTypeExists($DeviceKey){
		$statement = $this->myDB->prepare('SELECT COUNT(*) FROM  `local_post_data_temp` WHERE `DeviceType` = :devKey');
		$statement->bindParam('devKey',$DeviceKey);
		$result = $statement->execute();
		$tempArray = $result->fetchArray(SQLITE3_NUM);
		$result->finalize();
		$statement->close();
		if(empty($tempArray) ){return false;}
		return $tempArray[0];
	}

	private function BoundDeviceExists($BoundDevice){
		$statement = $this->myDB->prepare('SELECT `DeviceKey` FROM  `local_post_data_temp` WHERE `BoundDevice` = :boundDev');
		$statement->bindParam('boundDev',$BoundDevice);
		$result = $statement->execute();
		$tempArray = $result->fetchArray(SQLITE3_NUM);
		$result->finalize();
		$statement->close();
		if(empty($tempArray) ){return false;}
		return $tempArray[0];
	}

	private function UdidToIdentifier($rawUDID){
		if(empty($rawUDID) ){return false;}
		$myUdid = hexdec(substr($rawUDID,-8) );
		return $myUdid;
	}
}

?>