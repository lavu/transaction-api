<?php
require_once(__DIR__."/utils.php");
$myTime = time();
while(0 != getTogoJob()){}
$myTime = time() - $myTime;
echo "Completed in $myTime seconds\n";
function getTogoJob(){
	$settings = readLocalSettings();
	if(empty($settings) ){
		error_log(__FUNCTION__." Unable To Open Setting");
	}
	$postVars = array();
	$postVars['code'] = 'TESTING';
	$postVars['cmd'] = 'GET';
	$postVars['DATA_NAME'] = $settings['dataname'];
	$postVars['DB_NAME'] = $settings['database'];
	$myCurl = curl_init();
	curl_setopt($myCurl, CURLOPT_URL, 'https://admin.poslavu.com/private_api/pi_printer/togo_printer.php');
	curl_setopt($myCurl, CURLOPT_POST, 1);
	curl_setopt($myCurl, CURLOPT_POSTFIELDS, $postVars);
	curl_setopt($myCurl, CURLOPT_RETURNTRANSFER, 1);
	$response = curl_exec($myCurl);
	if(empty($response)){return 0;}
	$myObject = json_decode($response);
	if(empty($myObject)){return 0;}
	if(empty($myObject->orderID)){return 0;}
	foreach($myObject->printJobArray as $printerID => $printJob){
		if(!empty($printJob->printerInfo) && !empty($printJob->printerInfo->type)){
			printOrder($myObject,$printerID,$printJob->printerInfo->type);
		}
	}
	return setSent($postVars,$myObject->orderID);
}

function printOrder($myOrder,$printerID,$printerType){
	//choose printer type
	if(empty($printerType)){
		return;
	}
	$functionList['1']  = 'printStar';
	$functionList['10'] = 'printStar';
	$functionList['13'] = 'printStar';
	$functionList['14'] = 'printStar';
	$functionList['15'] = 'printStar';
	$functionList['16'] = 'printStar';
	$functionList['2']  = 'printEpson';
	$functionList['4']  = 'printEpson';
	$functionList['7']  = 'printEpson';
	$functionList['8']  = 'printEpson';
	$functionList['17'] = 'printEpson';
	$functionList['18'] = 'printEpson';
	$functionList['19'] = 'printKDS';
	if(isset($functionList[$printerType]))
	{
		$functionList[$printerType]($myOrder,$printerID);
	}else{
		error_log("Invalid Printer Type: $printerType");
	}
}

function printStar($myOrder,$printerID){
	$toPrint = array();
	$toPrint['print_string'] = formatStar($myOrder,$printerID);
	$toPrint['ip'] = $myOrder->printJobArray->$printerID->printerInfo->ip;
	$toPrint['port'] = $myOrder->printJobArray->$printerID->printerInfo->port;
	$toPrint['designator'] = $printerID;
	$toPrint['printer_name'] = $printerID;
	$toPrint['order_id'] = $myOrder->orderID;
	$toPrint['image_capability'] = '0';
	$toPrint['m'] = '60';
	insertToDB($toPrint);
}

function setSent($postVars,$orderID){
	$postVars['cmd'] = 'SET';
	$postVars['orderID'] = $orderID;
	$myCurl = curl_init();
	curl_setopt($myCurl, CURLOPT_URL, 'https://admin.poslavu.com/private_api/lavu_controller/togo_printer.php');
	curl_setopt($myCurl, CURLOPT_POST, 1);
	curl_setopt($myCurl, CURLOPT_POSTFIELDS, $postVars);
	curl_setopt($myCurl, CURLOPT_RETURNTRANSFER, 1);
	$response = curl_exec($myCurl);
	if(empty($response)){error_log("OOPS :  Lavu togo response blank");exit;}
	return 1;
}

function insertToDB($toPrint){
	$myCurl = curl_init();
	curl_setopt($myCurl, CURLOPT_URL, 'localhost/lib/printer_stub.php');
	curl_setopt($myCurl, CURLOPT_POST, 1);
	curl_setopt($myCurl, CURLOPT_POSTFIELDS, $toPrint);
	curl_setopt($myCurl, CURLOPT_RETURNTRANSFER, 1);
	$response = curl_exec($myCurl);
	return;
}

function formatStar($myOrder,$printerID,$printerMaxWidth = 42){
	$modsInRed = $myOrder->fontStyle->modifiers_in_red;
	$fontStyle = $myOrder->fontStyle->kitchen_ticket_font_size;
	$printerMaxWidth = 42;
	$dividerLine = str_repeat("-", $printerMaxWidth) . "\n";
	$formattedString = "\x1b@"; //Reset Printer Settings
	$formattedString.= "\x1b0"; //3mm line spacing
	if ('Emphasize' == $fontStyle) {
		$formattedString.= "\x1bE"; //Emphasized Printing
	}
	if ('Double Height' == $fontStyle) {
		$formattedString.= "\x1bh1"; //Double Height
		$formattedString.= "\x1bE"; //Emphasized Printing
	}
	$formattedString.= $dividerLine;
	$formattedString.= CenterString($printerMaxWidth, "LAVU TO GO ORDER");
	$formattedString.= CenterString($printerMaxWidth, "Order # " . $myOrder->orderID); //Order Number
	$formattedString.= CenterString($printerMaxWidth, "Name: " . $myOrder->name); //Name On Order
	$formattedString.= CenterString($printerMaxWidth, "Pickup Time");
	$formattedString.= CenterString($printerMaxWidth, $myOrder->togoTime); //Pickup Time
	$formattedString.= $dividerLine;
	foreach ($myOrder->printJobArray->$printerID->items as $itemObj){
		//Printing Items and Item Modifiers
		if (!empty($itemObj)){
			$formattedString.= $itemObj->quantity . " ";
			$formattedString.= $itemObj->item . "\n";
			if (1 == $modsInRed) {
				$formattedString.= "\x1b\x34";
			}
			if (!empty($itemObj->options)) {
				$formattedString.= " - " . $itemObj->options . "\n";
			}
			if (!empty($itemObj->special)) {
				$formattedString.= " * " . $itemObj->special . "\n";
			}
			if (!empty($itemObj->notes)) {
				$formattedString.= " ** " . $itemObj->notes . "\n";
			}
			if (1 == $modsInRed) {
				$formattedString.= "\x1b\x35";
			}
		}
	}
	$formattedString.= $dividerLine;
	$formattedString.= " \n \n"; //Footer?
	$formattedString.= $dividerLine;
	$formattedString.= "\x1Bd2"; //Cut Command
	return ($formattedString);
}

function printEpson($myOrder,$printerID){
	$toPrint = array();
	$toPrint['print_string'] = formatEpson($myOrder,$printerID);
	$toPrint['ip'] = $myOrder->printJobArray[$printerID]->printerInfo['ip'];
	$toPrint['port'] = $myOrder->printJobArray[$printerID]->printerInfo['port'];
	$toPrint['designator'] = $printerID;
	$toPrint['printer_name'] = $printerID;
	$toPrint['order_id'] = $myOrder->orderID;
	$toPrint['image_capability'] = '0';
	$toPrint['m'] = '60';
	insertToDB($toPrint);
}

function formatEpson($myOrder,$printerID,$printerMaxWidth = 48){
	$fontStyle = $myOrder->fontStyle['kitchen_ticket_font_size'];
	$dividerLine = str_repeat("-", $printerMaxWidth) . "\n";
	$formattedString = "";
	$formattedString.= "\x1b\x40"; //Initialize/Reset Printer
	if ('Emphasize' == $fontStyle) {
		$formattedString.= "\x1b\x21\x08"; //Emphasized Printing
	}
	if ('Double Height' == $fontStyle) {
		$formattedString.= "\x1b\x21\x18"; //Double Height
	}
	$formattedString.= $dividerLine;
	$formattedString.= CenterString($printerMaxWidth, "LAVU TO GO ORDER");
	$formattedString.= CenterString($printerMaxWidth, "Order # " . $myOrder->orderID); //Order Number
	$formattedString.= CenterString($printerMaxWidth, "Name: " . $myOrder->name); //Name On Order
	$formattedString.= CenterString($printerMaxWidth, "Pickup Time");
	$formattedString.= CenterString($printerMaxWidth, $myOrder->togoTime); //Pickup Time
	$formattedString.= $dividerLine;
	foreach ($myOrder->printJobArray[$printerID]->items as $item) //Printing Items and Item Modifiers
	{
		$formattedString.= $item['quantity'] . " " . $item['item'] . "\n";
		if (!empty($item['options'])) {
			$formattedString.= " - " . $item['options'] . "\n";
		}
		if (!empty($item['special'])) {
			$formattedString.= " * " . $item['special'] . "\n";
		}
		if (!empty($item['notes'])) {
			$formattedString.= " ** " . $item['notes'] . "\n";
		}
	}
	$formattedString.= $dividerLine;
	$formattedString.= " \n \n"; //Footer?
	$formattedString.= $dividerLine;
	$formattedString.= "\x1d\x56\x42\x00\n"; //Autofeed-Autocutter New
	return $formattedString;
}

function centerString($printerMaxWidth, $myString){
	//CenterString returns a formatted string ending with a newline
	//Trim any spurious spaces from input
	$myString = trim($myString);
	//Figure out how much space we left to print on the line
	$emptySpace = $printerMaxWidth - strlen($myString);
	if ($emptySpace > 0) { //if we have empty space then pad with spaces to center the text and return it
		return (str_pad($myString,$printerMaxWidth,' ',STR_PAD_BOTH)."\n");
	}
	if (0 == $emptySpace) { //If the line is perfectly filled then return with a newline
		return $myString . "\n";
	} //If the line is too long to print then split it in half
	$tempString = wordwrap($myString, (int)((strlen($myString) / 2) + 1), "\n", true);
	$stringArray = explode("\n", $tempString); // then make an array of the two lines
	$myString = ""; //clear mystring
	foreach ($stringArray as $split) { //center the new strings and return them.
		$myString.= centerString($printerMaxWidth, $split);
	}
	return $myString;
}

?>