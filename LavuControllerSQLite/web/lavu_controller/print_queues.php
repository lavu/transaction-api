<?php
error_reporting(E_ALL);
require_once(__DIR__."/utils.php");

if(isset($_REQUEST['MATDEBUG'])){
	echo "<pre>\n";
	allPrintMain();
	echo "</pre>\n";
}

if(isset($argv) && count($argv) > 1){
	commandLineControl($argv);
}

if(isset($_REQUEST['CMD']) && $_REQUEST['CMD'] == 'test'){
	testPrintAll();
}

function testPrintAll(){
	echo "1"; //NEEDS TO BE HERE!
	$myDB = getLocalDBPrint();
	if(empty($_REQUEST['more_info'] ) ){
		$statement = $myDB->prepare('
			 SELECT DISTINCT `printer_ipaddress` AS "ip",
			 `printer_port` AS "port",
			 `printer_type` AS "type"
			 FROM `printer_types`'
			 );
		$myResult = $statement->execute();
		while($myPrinter = $myResult->fetchArray(SQLITE3_ASSOC)){
			$tempString = $_REQUEST['info'];
			$tempString = str_replace("[printer_ip]", $myPrinter['ip'], $tempString);
			$tempString = str_replace("[printer_port]", $myPrinter['port'], $tempString);
			if($tempString == "test"){
				$tempString = "PiPrintTest\nPrinter IP:".$myPrinter['ip']."\nPrinter Port:".$myPrinter['port']."\n\n\n\n\n\n\n\n\n\n\n\n\x1bd\x01";
			}
			easyPrintJob($myPrinter,$tempString);
		}
	}else{
		//explode array and create print job for every printer
		$myPrinterArray = explode("|",$_REQUEST['more_info']);
		foreach ($myPrinterArray as $printerStuff){
			$printerInfo = explode(":",$printerStuff);
			$myPrinter = array();
			$myPrinter['name'] = '';
			$myPrinter['ip'] = $printerInfo[0];
			$myPrinter['port'] = $printerInfo[1];
			$tempString = $_REQUEST['info'];
			$tempString = str_replace("[printer_ip]", $myPrinter['ip'], $tempString);
			$tempString = str_replace("[printer_port]", $myPrinter['port'], $tempString);
			easyPrintJob($myPrinter,$tempString);
		}
	}
}

function easyPrintJob($PrinterData,$PrinterMessage){
	$errorno = '';
	$errorstr = '';
	$mySocket = @fsockopen( $PrinterData['ip'], $PrinterData['port'], $errorno, $errorstr, 1.0 );
	if ( !$mySocket ) {
		error_log("Could Not Open Printer: ".$PrinterData['ip'].":".$PrinterData['port']);
		return 0;
	}
	stream_set_timeout( $mySocket, 1 );
	fwrite( $mySocket,$PrinterMessage);
	fclose( $mySocket );
	return 1;
}

function commandLineControl($argv,$myTime = ''){
	if(!empty($argv[3])){
		$printerName = trim($argv[3]);
		specificPrintMain($printerName);
	}else{
		allPrintMain();
	}
	if(is_numeric($argv[2])){
		if(empty($myTime)){
			$myTime = time();
		}
		$myDiffTime = time() - $myTime;
		if($argv[2] > $myDiffTime){
			usleep(800000);
			setAllQueueTS();
			commandLineControl($argv,$myTime);
		}
	}
}

function allPrintMain(){
	$myDB = getLocalDBPrint();
	$myPrinterList = getActivePrinters($myDB);
	foreach($myPrinterList as $myPrinter){
		specificPrintMain($myPrinter,$myDB);
	}
	if(empty($myPrinterList)){return;}
	updateQueueTime($myDB);
	$myDB->close();
}

function specificPrintMain($myPrinter,$myDB = ''){
	if(empty($myDB)){
		$myDB = getLocalDBPrint();
	}
	$jobArray = array();
	//$jobArray[] = compileAllPrintJobsForPrinter($myPrinter,$myDB);
	$jobArray = getPrintJobs($myDB,$myPrinter);
	if(empty($jobArray)){return null;}
	echo "$myPrinter Job Count: ".count($jobArray)."\n";
	$printerType = getPrinterType($myDB,$myPrinter);
	sendArrayToPrinter($myDB,$jobArray,$printerType);
}

function sendArrayToPrinter($myDB,$jobArray,$printerType){
	$myDB;
	if(empty($jobArray)){
		return;
	}
	$functionList[1]  = 'sendStarJob';
	$functionList[10] = 'sendStarJob';
	$functionList[13] = 'sendStarJob';
	$functionList[14] = 'sendStarJob';
	$functionList[15] = 'sendStarJob';
	$functionList[16] = 'sendStarJob';
	$functionList[2]  = 'sendEpsonJob';
	$functionList[4]  = 'sendEpsonJob';
	$functionList[7]  = 'sendEpsonJob';
	$functionList[8]  = 'sendEpsonJob';
	$functionList[17] = 'sendEpsonJob';
	$functionList[18] = 'sendEpsonJob';
	$functionList[19] = 'sendKDSJob';
	if(isset($functionList[$printerType])){
		foreach ($jobArray as $printJob){
			echo "PrintIP: {$printJob['printer_ipaddress']}\n";
			$functionList[$printerType]($myDB,$printJob);
		}
	}else{
		error_log("Invalid Printer Type: $printerType");
	}
	return;
}

function sendEpsonJob($myDB,$printJob)
{
	$sendStatus = array();
	$sendStatus['sent'] = false;
	$test = sendEpsonPrintJob($printJob['printer_ipaddress'], $printJob['printer_port'], $printJob['contents'],$printJob['designator'] );
	if($test){
		setQueueStatus($printJob['designator'],$myDB,'OK');
		setQueueTS($printJob['designator'],$myDB);
		if(empty($printJob['ids'])){
			echo "Epson Print Succeeded:".$printJob['id']."\n";
			setJobPrinted($myDB,$printJob['id']);
		}else{
			foreach ($printJob['ids'] as $id) {
				setJobPrinted($myDB,$id);
			}
		}
		return true;
	}else{
		echo "Epson Print Failed   :"."\n";
		return false;
	}
}

function sendStarJob($myDB,$printJob){
	$sendStatus = array();
	$sendStatus['sent'] = false;
	$test = sendStarPrintJob($printJob['printer_ipaddress'], $printJob['printer_port'], $printJob['contents'],$printJob['designator'] );
	if($test){
		setQueueStatus($printJob['designator'],$myDB,'OK');
		setQueueTS($printJob['designator'],$myDB);
		if(empty($printJob['ids'])){
			echo "Star Print Succeeded :".$printJob['id']."\n";
			setJobPrinted($myDB,$printJob['id']);
		}else{
			foreach ($printJob['ids'] as $id) {
				setJobPrinted($myDB,$id);
			}
		}
		return true;
	}else{
		echo "Star Print Failed"."\n";
		return false;
	}
}

function sendKDSJob($myDB,$printJob){
	$sendStatus = array();
	$sendStatus['sent'] = true;
	$printerStatus = array();
	$printerStatus['message'] = "OK";
	$printerStatus['message_long'] = "OK";
	$sendStatus['printer'] = $printerStatus;
	setJobPrinted($myDB,$printJob['id']);
	return $sendStatus;
}

function setJobPrinted($myDB,$printID,$message = "OK"){
	$statement = $myDB->prepare("
		UPDATE `local_print_queue`
		SET `status`='sent',`sent_ts`=:myTime, `message`=:message
		WHERE `id`=:printID
	");
	$myTime = time();
	$statement->bindParam('myTime',$myTime);
	$statement->bindParam('message',$message);
	$statement->bindParam('printID',$printID);
	$statement->execute();
	return;
}

function getActivePrinters($myDB){
	$myPrinters = array();
	$statement = $myDB->prepare('
	SELECT DISTINCT `designator`
	FROM `local_print_queue`
	WHERE `status` != "sent"
	');
	$result = $statement->execute();
	while ($pName = $result->fetchArray(SQLITE3_NUM)) {
		$myPrinters[] = $pName[0];
	}
	return $myPrinters;
}

function getPrintJobs($myDB,$printerName){
	$statement = $myDB->prepare('
		SELECT *
		FROM `local_print_queue`
		WHERE `designator` = :printerName
		AND `status` != "sent"
	');
	$statement->bindParam('printerName',$printerName);
	$result = $statement->execute();
	$resultArray = array();
	while($resultTemp = $result->fetchArray(SQLITE3_ASSOC)){
		if(!empty($resultTemp)){
			$resultArray[] = $resultTemp;
		}
	}
	if(empty($resultArray)){
		return array();
	}
	return $resultArray;
}

function sendEpsonPrintJob($ipaddress, $port, $contents,$printerName ){
	$errorno = '';
	$errorstr = '';
	$mySocket = fsockopen( $ipaddress, $port, $errorno, $errorstr, 1.0 );
	if ( !$mySocket ) {
		setQueueStatus($printerName,NULL,'offline');
		usleep( 5000 );
		return 0;
	}
	stream_set_timeout( $mySocket, 1 );
	fwrite( $mySocket,"\x10\x04\x01" );
	$status = fgetc( $mySocket );
	if ( empty( $status ) ) {
		setQueueStatus($printerName,NULL,'offline');
		fclose( $mySocket );
		return 0;
	}
	$parsedContent = parseImageData( $contents );
	$contents = $parsedContent[0];
	$write_contents = $contents;
	$max_bytes = 16000;
	$finished = false;
	while ( !$finished ) {
		$bytes_left = strlen( $write_contents );
		$send_bytes = ($bytes_left > $max_bytes) ? $max_bytes : $bytes_left;
		$written = fwrite( $mySocket, $write_contents, $send_bytes );
		if ( $written===false ){//If write failed then fail out.
				fclose( $mySocket );
				usleep(25000);
				return $job_printed;
		}
		if ( $written < strlen( $write_contents ) ){
			$write_contents = substr( $write_contents, $written );
			usleep( 25000 );
		}else{
			$finished = true; // actually done printing
			$job_printed = true;
		}
	}

	fwrite( $mySocket,"\x10\x04\x01");
	$status = fgetc( $mySocket );
	if ( empty( $status ) ) {
		setQueueStatus($printerName,NULL,'offline');
		fclose( $mySocket );
		return 0;
	}
	fclose( $mySocket );
	return 1;
}

function sendStarPrintJob( $ipaddress, $port, $contents,$printerName  ){
	$p_status = checkPrinterStatus( $ipaddress, $port,$printerName );
	if(!$p_status['success']){
		return false;
	}
	$errorno = '';
	$errorstr = '';
	$mySocket = @fsockopen( $ipaddress, $port, $errorno, $errorstr, 1.0 );
	if ( !$mySocket ) {
		error_log( "Socket Open Fail: $ipaddress:$port" );
		usleep( 25000 );
		return 0;
	}
	stream_set_blocking( $mySocket, 1 );
	stream_set_timeout( $mySocket, 1 );
	$parsedContent = parseImageData( $contents );
	$contents = $parsedContent[0];
	$write_contents = $contents . "\r\n";
	$max_bytes = 2500;
	$finished = false;
	while ( !$finished ) {
		$bytes_left = strlen( $write_contents );
		$send_bytes = ($bytes_left > $max_bytes) ? $max_bytes : $bytes_left;
		$written = fwrite( $mySocket, $write_contents, $send_bytes );
		if ( $written===false ){//If write failed then fail out.
				echo "Failed Write\n";
				fclose( $mySocket );
				return $job_printed;
		}
		if ( $written < strlen( $write_contents ) ){
			$write_contents = substr( $write_contents, $written );
		}else{
			$finished = true; // actually done printing
			$job_printed = true;
		}
		usleep( 25000 );
	}
	echo "Full Write\n";
	$job_printed = true;
	fflush( $mySocket );
	fclose( $mySocket );
	usleep( 300000 );
	$p_status = checkPrinterStatus( $ipaddress, $port,$printerName );
	if(!$p_status['success']){
		return false;
	}
	return $job_printed;
}

function checkPrinterStatus( $ipaddress, $port ,$printerName){
	$p_return = array();
	$p_return['success'] = false;
	$p_return['error'] = false;
	$p_return['paper_low'] = false;
	$p_return['error_msg'] = "unknown";
	$p_return['error_msg_long'] = "An unknown error occurred.";
	$p_return['raw_response'] = '';
	$status_check_str = chr( 27 ) . chr( 6 ) . chr( 1 );
	$raw_response = "";
	$buf = "";
	$sock = fsockopen($ipaddress, $port);
	if ( !$sock ) {
		error_log( "Socket Open Fail: $ipaddress:$port (status temp)" );
		$p_return['error'] = true;
		usleep(25000);
		return $p_return;
	}
	stream_set_timeout( $sock, 1 );
	fwrite( $sock, $status_check_str );
	$buf = fread( $sock, 9);
	fclose($sock);
	usleep(25000);
	if ( empty($buf) ){
		$p_return['error'] = true;
		return $p_return;
	}
	$stat_list = array();
	for ( $i=0; $i<strlen( $buf );$i++ ) {
		$chr = substr( $buf, $i, 1 );
		$stat_line = str_pad( decbin( ord( $chr ) ), 8, "0", STR_PAD_LEFT );
		$stat_list[] = $stat_line;
		if ( $raw_response!="" ){
			$raw_response .= " ";
		}
		$raw_response .= $stat_line;
	}
	$p_return['raw_response'] = $raw_response;
	$cover_open = substr( $stat_list[2], 2, 1 );
	$offline    = substr( $stat_list[2], 4, 1 );
	$paper_out  = substr( $stat_list[5], 4, 1 );
	$paper_low  = substr( $stat_list[5], 5, 1 );
	if ( $cover_open ){
		$p_return['error_msg'] 			= "cover_open";
		$p_return['error_msg_long'] 	= "The cover is open.";
		$p_return['error'] 				= true;
	}elseif ( $paper_out ) {
		$p_return['error_msg'] 			= "no_paper";
		$p_return['error_msg_long'] 	= "There is no paper.";
		$p_return['error'] 				= true;
	}elseif ( $offline ) {
		$p_return['error_msg'] 			= "offline";
		$p_return['error_msg_long'] 	= "The printer is offline.";
		$p_return['error'] 				= true;
	}elseif ( $paper_low ) {
		$p_return['error_msg'] 			= "low_paper";
		$p_return['error_msg_long'] 	= "The paper is getting low.";
		$p_return['success'] 			= true;
	}else{
		$p_return['error_msg'] 			= "OK";
		$p_return['error_msg_long'] 	= "OK";
		$p_return['success'] 			= true;
	}
	setQueueStatus($printerName,'',$p_return['error_msg']);
	return $p_return;
}

function parseImageData($contents){
	$has_image_data = false;
	if (strstr($contents, "<IMAGE_START>")) {
		$has_image_data = true;
		$print_parts = explode("<IMAGE_START>", str_replace(" ", "", $contents));
		$temp_string = $print_parts[0];
		for ($i = 1; $i < count($print_parts); $i++) {
			$mixed_parts = explode("<IMAGE_END>", $print_parts[$i]);
			$hex = $mixed_parts[0];
			for ($h = 0; $h < strlen($hex); $h += 2) {
				$temp_string .= chr(hexdec($hex[$h].$hex[$h+1]));
			}
			$temp_string .= $mixed_parts[1];
		}
		$contents = $temp_string;
	}
	return array($contents, $has_image_data);
}

function getPrinterType($myDB,$printerName){
	//For Pi / NonLLS
	$statement = $myDB->prepare('SELECT `printer_type` from `printer_types` WHERE `designator`=:printerName');
	$statement->bindParam('printerName',$printerName);
	$result = $statement->execute();
	$resultArray = $result->fetchArray(SQLITE3_NUM);
	if(empty($resultArray)){
		return NULL;
	}
	if(empty($resultArray[0])){
		return NULL;
	}
	return $resultArray[0];
}

//@codingStandardsIgnoreStart
function add_to_queue($vars){
	return(addToQueue($vars));
}
//@codingStandardsIgnoreEnd

function addToQueue($vars){ //inputs a print job into database (local_print_queue)
	$myDB = getLocalDBPrint();
	error_log(print_r($vars,1));
	$actionsTemp = '';
	$statement = $myDB->prepare('
			INSERT INTO `local_print_queue`
			(
				`printer_ipaddress`,
				`printer_port`,
				`source_ipaddress`,
				`datetime`,
				`ts`,
				`contents`,
				`status`,
				`sent_ts`,
				`message`,
				`udid`,
				`designator`,
				`printer_name`,
				`image_capability`,
				`alert_sent`,
				`message_history`,
				`actions`,
				`order_id`
			)
			VALUES
			(
				:printer_ipaddress,
				:printer_port,
				:source_ipaddress,
				:datetime,
				:ts,
				:contents,
				:status,
				:sent_ts,
				:message,
				:udid,
				:designator,
				:printer_name,
				:image_capability,
				:alert_sent,
				:message_history,
				:actions,
				:order_id
			)
		');
	$statement->bindParam('printer_ipaddress',	$vars['printer_ipaddress']);
	$statement->bindParam('printer_port',		$vars['printer_port']);
	$statement->bindParam('source_ipaddress',	$vars['source_ipaddress']);
	$statement->bindParam('datetime',			$vars['datetime']);
	$statement->bindParam('ts',					$vars['ts']);
	$statement->bindParam('contents',			$vars['contents']);
	$statement->bindParam('status',				$vars['status']);
	$statement->bindParam('sent_ts',			$vars['sent_ts']);
	$statement->bindParam('message',			$vars['message']);
	$statement->bindParam('udid',				$vars['udid']);
	$statement->bindParam('designator',			$vars['designator']);
	$statement->bindParam('printer_name',		$vars['printer_name']);
	$statement->bindParam('image_capability',	$vars['image_capability']);
	$statement->bindParam('alert_sent',			$vars['alert_sent']);
	$statement->bindParam('message_history',	$vars['message_history']);
	$statement->bindParam('actions',			$actionsTemp);
	$statement->bindParam('order_id',			$vars['order_id']);
	$statement->execute();
	$statement->close();
	$statement2 = $myDB->prepare('SELECT `id` FROM `local_print_queue` WHERE (`ts` = :ts AND `contents`= :contents)');
	$statement2->bindParam('ts',		$vars['ts']);
	$statement2->bindParam('contents',	$vars['contents']);
	$print_id = false;
	$result = $statement2->execute();
	$resultArray = $result->fetchArray(SQLITE3_NUM);
	$print_id = $resultArray[0];
	$statement2->close();
	return array($print_id,'OK',time());
}

function setQueueStatus($Queue,$localDB = '',$status = ''){
	if(empty($Queue)){return;}
	if(empty($localDB)){
		$localDB = getLocalDBPrint();
	}
	$statement = $localDB->prepare('
			REPLACE INTO `local_printer_status`
			(`ipaddress`,`last_status`)
			VALUES
			(:Queue,:status)
			'
		);
	$statement->bindParam('Queue',$Queue);
	$statement->bindParam('status',$status);
	$statement->execute();
	return;
}

function setQueueTS($designator,$localDB = ''){
	if(empty($designator)){return;}
	if(empty($localDB)){
		$localDB = getLocalDBPrint();
	}
	$statement = $localDB->prepare('
		REPLACE INTO `local_printer_status`
		(`ipaddress`,`last_print_ts`)
		VALUES
		(:designator,:myTime)
		');
	$myTime = time();
	$statement->bindParam('Queue',$Queue);
	$statement->bindParam('myTime',$myTime);
	$statement->execute();
	return;
}

function setAllQueueTS($localDB = ''){
	if(empty($localDB)){
		$localDB = getLocalDBPrint();
	}
	$statement = $localDB->prepare('
			UPDATE `local_printer_status`
			SET `last_print_ts`=:myTime
			'
		);
	$myTime = time();
	$statement->bindParam('myTime',$myTime);
	$statement->execute();
	return;
}

function getQueueStatus($designator,$localDB = ''){
	if(empty($designator)){return;}
	if(empty($localDB)){
		$localDB = getLocalDBPrint();
	}
	$statement = $localDB->prepare('
			SELECT `last_print_ts`,`last_status`
			FROM `local_printer_status`
			WHERE `ipaddress`=:designator
		');
	$statement->bindParam('designator',$designator);
	$result = $statement->execute();
	$resultArray = $result->fetchArray(SQLITE3_NUM);
	$last_print_ts = $resultArray[0];
	$last_print_status = $resultArray[1];
	$result2 = array(true,'OK',$last_print_ts);
	if($last_print_status != 'OK' ){
		error_log(__FUNCTION__."| $designator : $last_print_status");
		$result2[0]=false;
		$result2[1]=$last_print_status;
	}
	return $result2;
}

function updateQueueTime($localDB){
	$statement = $localDB->prepare('
			UPDATE `local_printer_status`
			SET `last_print_ts` = :myTime
			WHERE `last_status` = "OK"
		');
	$myTime = time();
	$statement->bindParam('myTime',$myTime);
	$statement->execute();
}

?>