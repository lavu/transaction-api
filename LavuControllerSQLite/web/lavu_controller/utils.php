<?php

if ( !function_exists( 'hex2bin' ) ) {
	function hex2bin( $str ) {
		$sbin = "";
		$len = strlen( $str );
		for ( $i = 0; $i < $len; $i += 2 ) {
			$sbin .= pack( "H*", substr( $str, $i, 2 ) );
		}
		return $sbin;
	}
}

function hex4bin( $str ) {
	$sbin = "";
	$len = strlen( $str );
	for ( $i = 0; $i < $len; $i += 2 ) {
		$sbin .= pack( "H*", substr( $str, $i, 2 ) );
	}
	return $sbin;
}

function getLocalDBPrint(){
	$DBPath = __DIR__.'/../../db/.printers.sqlite';
	$DBPath = realpath($DBPath);
	$myDB = new SQLite3($DBPath);
	$myDB->busyTimeout(2000);
	return $myDB;
}

function getLocalDBSwipe(){
	$DBPath = __DIR__.'/../../db/.swipers.sqlite';
	$DBPath = realpath($DBPath);
	$myDB = new SQLite3($DBPath);
	$myDB->busyTimeout(2000);
	return $myDB;
}

function getTimeZone(){
	$default = "UTC";
	$result = false;
	if($result){
		return $result;
	}
	return $default;
}

function getVersion(){
	return('5');
}


function getServer(){
	$settings = readLocalSettings();
	if(empty($settings) || empty($settings['net_path'])){
		return('https://admin.poslavu.com/');
	}
	return($settings['net_path']);
}

function debugWriteVar($var){
	return;
	$filePointer = fopen(__DIR__.'/../../db/debugging.txt',"a");
	if($filePointer === FALSE){
		error_log("debugWriteVar: fopen failure");
		return;
	}
	fwrite($filePointer,var_export($var,1));
	fclose($filePointer);
	return;
}

function readLocalSettings(){ //reads the /local/settings file and creates/returns an associative array for it.
	$settings;
	$fname = __DIR__.'/../../db/settings.txt';
	$settingsFileHandle = fopen($fname, "r");
	if($settingsFileHandle === FALSE){
		error_log("readLocalSettings: fopen failure");
		return;
	}
	$stringData = fread($settingsFileHandle, filesize($fname));
	if ($stringData === false){
		error_log("readLocalSettings: read failure");
		return;
	}
	fclose($settingsFileHandle);
	$dataArray = explode("\n", $stringData);
	foreach ($dataArray as $currentString) {
		$parts = array_map('trim', explode("=", $currentString, 2));
		if ($parts[0] != '') {
			$settings[$parts[0]] = $parts[1];
		}
	}
	return $settings;
}

?>