<?php //Challenge for Pi Printer
require_once(__DIR__."/lavu_controller/utils.php");
error_log("Challenge Hit");
if(!empty( $_REQUEST['CMD'] ) ){

	if($_REQUEST['CMD'] == 'identify'){
		$identityString = "LavuPiPrinter"."|";
		$dataname = getDataName();
		echo $identityString.$dataname;
	}
	if($_REQUEST['CMD'] == 'configure'){
		configurePiPrint();
	}
	if($_REQUEST['CMD'] == 'test'){
		include(dirname(__FILE__)."/lavu_controller/print_queues.php");
	}
	if($_REQUEST['CMD'] == 'version'){
		echo getVersion();
	}
}

function configurePiPrint(){
	if(empty($_REQUEST['dataname'])){echo "0|No dataname"; return;}
	$settings = array();
	$settings['dataname'] = $_REQUEST['dataname'];
	$settings['database'] = "poslavu_".$_REQUEST['dataname']."_db";
	$settings['net_path'] = $_REQUEST['net_path'];
	saveSettings($settings);
	require_once(__DIR__."/lavu_controller/printer_setup.php");
	$myPrinters = new Printers();
	$myPrinters->resetLocalPrintJobs();
	echo "1|Success";
}

function saveSettings($settings){
	if(is_array($settings)){
		$settingsFile = fopen(__DIR__.'/../db/settings.txt', 'w');
		if(!$settingsFile){
			echo "0|Could not open/edit settings file";
			return;
		}
		foreach ($settings as $key => $value) {
			fwrite($settingsFile,$key." = ".$value."\n");
		}
		fclose($settingsFile);
	}
}


function getDataName(){
	$settings = readLocalSettings();
	if(empty($settings)){return '';}
	if(empty($settings['dataname'])){return '';}
	return($settings['dataname']);
}


?>
