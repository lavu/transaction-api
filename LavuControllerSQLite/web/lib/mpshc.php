<?php
error_log('mpshc');
function determineDeviceIdentifier($req) {
	$identifier = "";
	if (isset($req['UDID'])){
		$identifier = $req['UDID'];
	}elseif (isset($req['UUID'])){
		$identifier = $req['UUID'];
	}elseif (isset($req['MAC'])){
		$identifier = $req['MAC'];
	}
	return $identifier;
}

function createJSTags($js){
return <<<JAVASCURPTS
<script type="text/javascript">
	{$js}
</script>
JAVASCURPTS;
}

$gateway = (isset($_REQUEST['gateway']))?$_REQUEST['gateway']:"";
$host_mode = (isset($_REQUEST['host_mode']))?$_REQUEST['host_mode']:"";
$custom_payment_info = (isset($_REQUEST['custom_payment_info']))?$_REQUEST['custom_payment_info']:"";
$mode = (isset($_REQUEST['m']))?$_REQUEST['m']:"complete";
if ($mode == "init") {
	$data_name         = (isset($_REQUEST['data_name']))?$_REQUEST['data_name']:false;
	$loc_id            = (isset($_REQUEST['loc_id']))?$_REQUEST['loc_id']:false;
	$order_id          = (isset($_REQUEST['order_id']))?$_REQUEST['order_id']:false;
	$check             = (isset($_REQUEST['check']))?$_REQUEST['check']:"1";
	$card_amount       = (isset($_REQUEST['card_amount']))?$_REQUEST['card_amount']:false;
	$transtype         = (isset($_REQUEST['transtype']))?$_REQUEST['transtype']:"Sale";
	$register          = (isset($_REQUEST['register']))?$_REQUEST['register']:"";
	$register_name     = (isset($_REQUEST['register_name']))?$_REQUEST['register_name']:"";
	$server_id         = (isset($_REQUEST['server_id']))?$_REQUEST['server_id']:0;
	$server_name       = (isset($_REQUEST['server_name']))?$_REQUEST['server_name']:0;
	$ccIdent           = (isset($_REQUEST['cc']))?$_REQUEST['cc']:"1234567890";
	$poslavu_version   = (isset($_REQUEST['version']))?$_REQUEST['version']:"2.0";
	$poslavu_build     = (isset($_REQUEST['build']))?$_REQUEST['build']:"20110214-1";
	$device_time       = (isset($_REQUEST['device_time']))?$_REQUEST['device_time']:date("Y-m-d H:i:s", time());
	$device_udid       = determineDeviceIdentifier($_REQUEST);
	$for_deposit       = (isset($_REQUEST['for_deposit']))?$_REQUEST['for_deposit']:"0";
	$is_deposit        = (isset($_REQUEST['is_deposit']))?$_REQUEST['is_deposit']:"0";
	$bg_color          = (!empty($_REQUEST['bc']))?$_REQUEST['bc']:"#E7E7E7";
	$font_color        = (!empty($_REQUEST['fc']))?$_REQUEST['fc']:"#000000";
	$internal_order_id = (isset($_POST['ioid']))?$_POST['ioid']:"";
	$internal_tx_id    = (isset($_POST['internal_id']))?$_POST['internal_id']:"";
	if (isset($_REQUEST['inner_content'])) {
		if($host_mode=="general") {
			$mpshc_referral = true;
			require_once(__DIR__.'/hosted_payment.php');
			exit();
		}
	}else{
		echo "<html>";
		echo "<body style='background-color: transparent; margin-left: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; -webkit-tap-highlight-color:rgba(0,0,0,0);'>";
		echo "<center>";
		echo "<iframe name='the_iframe' frameborder='0' width='450px' height='450px' scrolling='no' src=''></iframe>";
		echo "<form name='the_form' method='post' action='mpshc.php' target='the_iframe'>";
		echo "<input type='hidden' name='gateway' value='$gateway'>";
		echo "<input type='hidden' name='host_mode' value='$host_mode'>";
		echo "<input type='hidden' name='custom_payment_info' value=\"".str_replace("\"","&quot;",$custom_payment_info)."\">";
		echo "<input type='hidden' name='inner_content' value='1'>";
		echo "<input type='hidden' name='m' value='init'>";
		echo "<input type='hidden' name='data_name' value='$data_name'>";
		echo "<input type='hidden' name='loc_id' value='$loc_id'>";
		echo "<input type='hidden' name='order_id' value='$order_id'>";
		echo "<input type='hidden' name='check' value='$check'>";
		echo "<input type='hidden' name='card_amount' value='$card_amount'>";
		echo "<input type='hidden' name='transtype' value='$transtype'>";
		echo "<input type='hidden' name='register' value='$register'>";
		echo "<input type='hidden' name='register_name' value='".str_replace("'", "\'", $register_name)."'>";
		echo "<input type='hidden' name='server_id' value='$server_id'>";
		echo "<input type='hidden' name='server_name' value='".str_replace("'", "\'", $server_name)."'>";
		echo "<input type='hidden' name='version' value='$poslavu_version'>";
		echo "<input type='hidden' name='build' value='$poslavu_build'>";
		echo "<input type='hidden' name='device_time' value='$device_time'>";
		echo "<input type='hidden' name='UDID' value='$device_udid'>";
		echo "<input type='hidden' name='for_deposit' value='$for_deposit'>";
		echo "<input type='hidden' name='is_deposit' value='$is_deposit'>";
		echo "<input type='hidden' name='bc' value='$bg_color'>";
		echo "<input type='hidden' name='cc' value='$ccIdent'>";
		echo "<input type='hidden' name='fc' value='$font_color'>";
		echo "<input type='hidden' name='ioid' value='$internal_order_id'>";
		echo "<input type='hidden' name='internal_id' value='$internal_tx_id'>";
		echo "</form>";
		echo "</center>";
		echo createJSTags("the_form.submit();");
		echo "</body>";
		echo "</html>";
		return;
	}
}
?>