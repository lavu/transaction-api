<?php
error_log('printer stub');
if($_REQUEST['m'] != 60){
	error_log(var_export($_REQUEST,1));
	return;
}
require_once(dirname(__FILE__)."/../lavu_controller/print_queues.php");
date_default_timezone_set('UTC');
$vars = array(
		"printer_ipaddress"=>$_REQUEST['ip'],
		"printer_port"=>$_REQUEST['port'],
		"source_ipaddress"=>$_SERVER['REMOTE_ADDR'],
		"datetime"=>date("Y-m-d H:i:s"),
		"ts"=>time(),
		"contents"=>$_REQUEST['print_string'],
		"status"=>"new",
		"sent_ts"=>"",
		"message"=>"",
		"udid"=>"",
		"designator"=>$_REQUEST['designator'],
		"printer_name"=>$_REQUEST['printer_name'],
		"image_capability"=>$_REQUEST['image_capability'],
		"alert_sent"=>"",
		"message_history"=>"",
		"order_id"=>$_REQUEST['order_id']
	);
$result = addToQueue($vars);

if (!$result[0]){
	$result[1] = "db_error";
}else{
	$result = getQueueStatus($_REQUEST['designator']);
}
$post = $result[1]."|".$result[2];
echo $post;
?>
